﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOClassLibrary
{
    public class FilterFunctie
    {
        public decimal ID { get; set; }
        public decimal Contact_ID { get; set; }
        public string Voornaam { get; set; }
        public string VoornaamFonetisch { get; set; }
        public string Achternaam { get; set; }
        public string AchternaamFonetisch { get; set; }
        public DateTime? Geboortedatum { get; set; }
        public string Aanspreking { get; set; }
        public string Contactnotities { get; set; }
        public Nullable<bool> Contactactief { get; set; }
        public decimal Bedrijf_ID { get; set; }
        public string Btwnummer { get; set; }
        public string Bedrijfsnaam { get; set; }
        public string BedrijfsTelefoon { get; set; }
        public string BedrijfsFax { get; set; }
        public string BedrijfsEmail { get; set; }
        public string BedrijfsAdres { get; set; }
        public string BedrijfsPostcode { get; set; }
        public string BedrijfsPlaats { get; set; }
        public string BedrijfsProvincie { get; set; } = "";
        public string Land { get; set; }
        public string Website { get; set; } = "";
        public string TaalNaam { get; set; }
        public string SecundaireTaalNaam { get; set; }
        public decimal Niveau1_ID { get; set; }
        public string Niveau1Naam { get; set; }
        public Nullable<decimal> Niveau2_ID { get; set; }
        public string Niveau2Naam { get; set; }
        public Nullable<decimal> Niveau3_ID { get; set; }
        public string Niveau3Naam { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }
        public string Mobiel { get; set; }
        public Nullable<decimal> Erkenning_ID { get; set; }
        public Nullable<bool> Mail { get; set; }
        public string MailAsText
        {
            get
            {
                return Mail == true ? "Ja" : "Nee";
            }
        }
        public Nullable<bool> Fax { get; set; }
        public string FaxAsText
        {
            get
            {
                return Fax == true ? "Ja" : "Nee";
            }
        }

        public string ContactactiefAsString
        {
            get { return Contactactief == true ? "Ja" : "Nee"; }
        }
        public Nullable<System.DateTime> Startdatum { get; set; }
        public Nullable<System.DateTime> Einddatum { get; set; }
        public Nullable<bool> Actief { get; set; }
        public string Aangemaakt_door { get; set; }
        public Nullable<System.DateTime> Aangemaakt { get; set; }
        public string Gewijzigd_door { get; set; }
        public Nullable<System.DateTime> Gewijzigd { get; set; }
        public string Oorspronkelijke_Titel { get; set; }
        public string Erkenningsnummer { get; set; }
        public string Attesttype { get; set; }
        public string Type { get; set; }
        public Nullable<decimal> Attesttype_ID { get; set; }
        public string Bedrijfsnotities { get; set; }
        public Nullable<bool> Bedrijfactief { get; set; }
        public string BedrijfactiefAsString
        {
            get { return Bedrijfactief == true ? "Ja" : "Nee"; }
        }
        public Nullable<System.DateTime> ContactAangemaakt { get; set; }
        public string ContactAangemaakt_door { get; set; }
        public Nullable<System.DateTime> ContactGewijzigd { get; set; }
        public string ContactGewijzigd_door { get; set; }
        public byte[] Modified { get; set; }
        public Nullable<bool> Functieactief { get; set; }
        public string FunctieActiefAsString
        {
            get { return Functieactief == true ? "Ja" : "Nee"; }
        }

        public FilterFunctie()
        {

        }
    }
}

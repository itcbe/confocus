﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOClassLibrary
{
    public static class SQLQuery
    {
        public static string FunctiesQuery()
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT Functies.ID");
            query.Append(",Contact.ID as ContactID");
            query.Append(",Contact.Voornaam");
            query.Append(",Contact.Achternaam");
            query.Append(",Contact.Geboortedatum");
            query.Append(",Contact.Aanspreking");
            query.Append(",Functies.Type");
            query.Append(",Functies.Email");
            query.Append(",Functies.Telefoon");
            query.Append(",Taal.Naam as TaalNaam");
            query.Append(",Niveau1.Naam as Niveau1Naam");
            query.Append(",Niveau2.Naam as Niveau2Naam");
            query.Append(",Niveau3.Naam as Niveau3Naam");
            query.Append(",Bedrijf.Naam as BedrijfNaam");
            query.Append(",Bedrijf.Telefoon as Btelefoon");
            query.Append(",Bedrijf.Fax as Bfax");
            query.Append(",Bedrijf.Email as BEmail");
            query.Append(",Bedrijf.Adres");
            query.Append(",Bedrijf.Postcode");
            query.Append(",Bedrijf.Plaats");
            query.Append(",Contact.Notities");
            query.Append(",Functies.Mail");
            query.Append(",Functies.Fax");
            query.Append(",Functies.Gewijzigd");
            query.Append(",Functies.Gewijzigd_door");
            query.Append(",Functies.Aangemaakt");
            query.Append(",Functies.Aangemaakt_door");
            query.Append("FROM Functies");
            query.Append(",Niveau1");
            query.Append(",Niveau2");
            query.Append(",Niveau3");
            query.Append(",Contact");
            query.Append(",Bedrijf");
            query.Append(",Taal");
            query.Append("WHERE Functies.Niveau1_ID = Niveau1.ID");
            query.Append("and Functies.Niveau2_ID = Niveau2.ID");
            query.Append("and Functies.Niveau3_ID = Niveau3.ID");
            query.Append("and Functies.Contact_ID = Contact.ID");
            query.Append("and Functies.Bedrijf_ID = Bedrijf.ID");
            query.Append("and Contact.Taal_ID = Taal.ID");
            query.Append("and Functies.Actief = 1");
            return query.ToString();
        }
    }
}

﻿using System;

namespace ADOClassLibrary
{
    public class InschrijvingSessie
    {
        public decimal ID { get; set; }
        public decimal Seminarie_ID { get; set; }
        public string SeminarieNaam { get; set; }
        public string Naam { get; set; }
        public string Status { get; set; }
        public Nullable<DateTime> Datum { get; set; }
        public bool Actief { get; set; }
        public string LocatieNaam { get; set; }
        public string NaamDatumLocatie
        {
            get
            {
                return Naam + ": " + (Datum == null ? "" : ((DateTime)Datum).ToShortDateString()) + "  " + LocatieNaam;
            }
        }

    }
}
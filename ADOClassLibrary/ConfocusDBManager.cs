﻿using ConfocusClassLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOClassLibrary
{
    public class ConfocusDBManager
    {
        private static ConnectionStringSettings conConfocusSetting = ConfigurationManager.ConnectionStrings["Confocus"];
        private static DbProviderFactory factory = DbProviderFactories.GetFactory(conConfocusSetting.ProviderName);

        public DbConnection GetConnection()
        {
            var conConfocus = factory.CreateConnection();
            conConfocus.ConnectionString = conConfocusSetting.ConnectionString;
            return conConfocus;
        }

        public List<FilterFunctie> GetFuncties()
        {
            try
            {
                List<FilterFunctie> functies = new List<FilterFunctie>();
                using (var conConfocus = this.GetConnection())
                {
                    using (var comFuncties = conConfocus.CreateCommand())
                    {
                        comFuncties.CommandType = System.Data.CommandType.Text;
                        comFuncties.CommandText = "select * from Functies";

                        conConfocus.Open();
                        using (var rdrFuncties = comFuncties.ExecuteReader())
                        {
                            Int32 idPos = rdrFuncties.GetOrdinal("ID");
                            Int32 ContactIDPos = rdrFuncties.GetOrdinal("Contact_ID");
                            Int32 bedrijfIDPos = rdrFuncties.GetOrdinal("Bedrijf_ID");
                            Int32 n1Pos = rdrFuncties.GetOrdinal("Niveau1_ID");
                            Int32 n2Pos = rdrFuncties.GetOrdinal("Niveau2_ID");
                            Int32 n3Pos = rdrFuncties.GetOrdinal("Niveau3_ID");
                            Int32 emailPos = rdrFuncties.GetOrdinal("Email");
                            Int32 telefoonPos = rdrFuncties.GetOrdinal("Telefoon");
                            Int32 mobielPos = rdrFuncties.GetOrdinal("Mobiel");
                            Int32 erkenningIDPos = rdrFuncties.GetOrdinal("Erkenning_ID");
                            Int32 mailPos = rdrFuncties.GetOrdinal("Mail");
                            Int32 faxPos = rdrFuncties.GetOrdinal("Fax");
                            Int32 startdatumPos = rdrFuncties.GetOrdinal("Startdatum");
                            Int32 einddatumPos = rdrFuncties.GetOrdinal("Einddatum");
                            Int32 actiefPos = rdrFuncties.GetOrdinal("Actief");
                            Int32 aangemaaktDoorPos = rdrFuncties.GetOrdinal("Aangemaakt_door");
                            Int32 aangemaaktPos = rdrFuncties.GetOrdinal("Aangemaakt");
                            Int32 gewijzigdDoorPos = rdrFuncties.GetOrdinal("Gewijzigd_door");
                            Int32 gewijzigdPos = rdrFuncties.GetOrdinal("Gewijzigd");
                            Int32 titelPos = rdrFuncties.GetOrdinal("Oorspronkelijke_Titel");
                            Int32 erkenningnummerPos = rdrFuncties.GetOrdinal("Erkenningsnummer");
                            Int32 attesttypePos = rdrFuncties.GetOrdinal("Attesttype");
                            Int32 typePos = rdrFuncties.GetOrdinal("Type");
                            Int32 attestIDPos = rdrFuncties.GetOrdinal("Attesttype_ID");
                            Int32 modifiedPos = rdrFuncties.GetOrdinal("Modified");

                            while (rdrFuncties.Read())
                            {
                                FilterFunctie functie = new FilterFunctie();
                                functie.ID = rdrFuncties.GetDecimal(idPos);
                                functie.Contact_ID = rdrFuncties.GetDecimal(ContactIDPos);
                                functie.Bedrijf_ID = rdrFuncties.GetDecimal(bedrijfIDPos);
                                functie.Niveau1_ID = rdrFuncties.GetDecimal(n1Pos);
                                functie.Niveau2_ID = rdrFuncties.IsDBNull(n2Pos) ? 0 : rdrFuncties.GetDecimal(n2Pos);
                                functie.Niveau3_ID = rdrFuncties.IsDBNull(n3Pos) ? 0 : rdrFuncties.GetDecimal(n3Pos);
                                functie.Email = rdrFuncties.IsDBNull(emailPos) ? "" : rdrFuncties.GetString(emailPos);
                                functie.Telefoon = rdrFuncties.IsDBNull(telefoonPos) ? "" : rdrFuncties.GetString(telefoonPos);
                                functie.Mobiel = rdrFuncties.IsDBNull(mobielPos) ? "" : rdrFuncties.GetString(mobielPos);
                                functie.Erkenningsnummer = rdrFuncties.IsDBNull(erkenningnummerPos) ? "" : rdrFuncties.GetString(erkenningnummerPos);
                                functie.Erkenning_ID = rdrFuncties.IsDBNull(erkenningIDPos) ? 0 : rdrFuncties.GetDecimal(erkenningIDPos);
                                functie.Mail = rdrFuncties.IsDBNull(mailPos) ? false : rdrFuncties.GetBoolean(mailPos);
                                functie.Fax = rdrFuncties.IsDBNull(faxPos) ? false : rdrFuncties.GetBoolean(faxPos);
                                if (!rdrFuncties.IsDBNull(startdatumPos))
                                    functie.Startdatum = rdrFuncties.GetDateTime(startdatumPos);
                                if (!rdrFuncties.IsDBNull(einddatumPos))
                                    functie.Einddatum = rdrFuncties.GetDateTime(einddatumPos);
                                functie.Actief = rdrFuncties.IsDBNull(actiefPos) ? false : rdrFuncties.GetBoolean(actiefPos);
                                functie.Aangemaakt_door = rdrFuncties.IsDBNull(aangemaaktDoorPos) ? "" : rdrFuncties.GetString(aangemaaktDoorPos);
                                if (!rdrFuncties.IsDBNull(aangemaaktPos))
                                    functie.Aangemaakt = rdrFuncties.GetDateTime(aangemaaktPos);
                                functie.Gewijzigd_door = rdrFuncties.IsDBNull(gewijzigdDoorPos) ? "" : rdrFuncties.GetString(gewijzigdDoorPos);
                                if (!rdrFuncties.IsDBNull(gewijzigdPos))
                                    functie.Gewijzigd = rdrFuncties.GetDateTime(gewijzigdPos);
                                functie.Oorspronkelijke_Titel = rdrFuncties.IsDBNull(titelPos) ? "" : rdrFuncties.GetString(titelPos);
                                functie.Attesttype = rdrFuncties.IsDBNull(attesttypePos) ? "" : rdrFuncties.GetString(attesttypePos);
                                functie.Type = rdrFuncties.IsDBNull(typePos) ? "" : rdrFuncties.GetString(typePos);
                                functie.Attesttype_ID = rdrFuncties.IsDBNull(attestIDPos) ? 0 : rdrFuncties.GetDecimal(attestIDPos);
                                functies.Add(functie);
                            }
                        }
                    }
                }
                return functies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FilterFunctie> GetFilterFuncties(ContactSearchFilter filter)
        {
            try
            {
                List<FilterFunctie> functies = new List<FilterFunctie>();
                using (var conConfocus = this.GetConnection())
                {
                    using (var comFuncties = conConfocus.CreateCommand())
                    {
                        comFuncties.CommandType = System.Data.CommandType.StoredProcedure;
                        comFuncties.CommandText = "GetFunctions";
                        if (!string.IsNullOrEmpty(filter.Voornaam))
                        {
                            var parVoornaam = comFuncties.CreateParameter();
                            parVoornaam.ParameterName = "@voornaam";
                            parVoornaam.Value = filter.Voornaam;
                            comFuncties.Parameters.Add(parVoornaam);
                        }
                        if (!string.IsNullOrEmpty(filter.Achternaam))
                        {
                            var parAchternaam = comFuncties.CreateParameter();
                            parAchternaam.ParameterName = "@Achternaam";
                            parAchternaam.Value = filter.Achternaam;
                            comFuncties.Parameters.Add(parAchternaam);
                        }
                        if (!string.IsNullOrEmpty(filter.Email))
                        {
                            var parEmail = comFuncties.CreateParameter();
                            parEmail.ParameterName = "@Email";
                            parEmail.Value = filter.Email;
                            comFuncties.Parameters.Add(parEmail);
                        }
                        if (!string.IsNullOrEmpty(filter.BedrijfsNaam))
                        {
                            var parBedrijf = comFuncties.CreateParameter();
                            parBedrijf.ParameterName = "@Bedrijfsnaam";
                            parBedrijf.Value = filter.BedrijfsNaam;
                            comFuncties.Parameters.Add(parBedrijf);
                        }
                        if (!string.IsNullOrEmpty(filter.ContactType))
                        {
                            var parContactType = comFuncties.CreateParameter();
                            parContactType.ParameterName = "@ContactType";
                            parContactType.Value = filter.ContactType;
                            comFuncties.Parameters.Add(parContactType);
                        }
                        if (!string.IsNullOrEmpty(filter.Niveau1Naam))
                        {
                            var parNiveau1 = comFuncties.CreateParameter();
                            parNiveau1.ParameterName = "@Niveau1naam";
                            parNiveau1.Value = filter.Niveau1Naam;
                            comFuncties.Parameters.Add(parNiveau1);
                        }
                        if (!string.IsNullOrEmpty(filter.Niveau2Naam))
                        {
                            var parNiveau2 = comFuncties.CreateParameter();
                            parNiveau2.ParameterName = "@Niveau2naam";
                            parNiveau2.Value = filter.Niveau2Naam;
                            comFuncties.Parameters.Add(parNiveau2);
                        }
                        if (!string.IsNullOrEmpty(filter.Niveau3Naam))
                        {
                            var parNiveau3 = comFuncties.CreateParameter();
                            parNiveau3.ParameterName = "@Niveau3naam";
                            parNiveau3.Value = filter.Niveau3Naam;
                            comFuncties.Parameters.Add(parNiveau3);
                        }
                        if (!string.IsNullOrEmpty(filter.Gemeente))
                        {
                            var parGemeente = comFuncties.CreateParameter();
                            parGemeente.ParameterName = "@Gemeente";
                            parGemeente.Value = filter.Gemeente;
                            comFuncties.Parameters.Add(parGemeente);
                        }
                        if (!string.IsNullOrEmpty(filter.Provincie))
                        {
                            var parProvincie = comFuncties.CreateParameter();
                            parProvincie.ParameterName = "@Provincie";
                            parProvincie.Value = filter.Provincie;
                            comFuncties.Parameters.Add(parProvincie);
                        }
                        if (filter.Aangemaakt != null)
                        {
                            var parAangemaakt = comFuncties.CreateParameter();
                            parAangemaakt.ParameterName = "@Aangemaakt";
                            parAangemaakt.Value = filter.Aangemaakt;
                            comFuncties.Parameters.Add(parAangemaakt);
                        }
                        if (!string.IsNullOrEmpty(filter.Door))
                        {
                            var parAangemaaktDoor = comFuncties.CreateParameter();
                            parAangemaaktDoor.ParameterName = "@AangemaaktDoor";
                            parAangemaaktDoor.Value = filter.Door;
                            comFuncties.Parameters.Add(parAangemaaktDoor);
                        }
                        if (!string.IsNullOrEmpty(filter.Faxnummer))
                        {
                            var parFax = comFuncties.CreateParameter();
                            parFax.ParameterName = "@Faxnummer";
                            parFax.Value = filter.Faxnummer;
                            comFuncties.Parameters.Add(parFax);
                        }
                        if (!string.IsNullOrEmpty(filter.TaalNaam))
                        {
                            var parTaal = comFuncties.CreateParameter();
                            parTaal.ParameterName = "@Taalnaam";
                            parTaal.Value = filter.TaalNaam;
                            comFuncties.Parameters.Add(parTaal);
                        }


                        conConfocus.Open();
                        using (var rdrFuncties = comFuncties.ExecuteReader())
                        {
                            Int32 idPos = rdrFuncties.GetOrdinal("ID");
                            Int32 ContactIDPos = rdrFuncties.GetOrdinal("ContactID");
                            Int32 ContactVoornaamPos = rdrFuncties.GetOrdinal("Voornaam");
                            Int32 ContactAchternaamPos = rdrFuncties.GetOrdinal("Achternaam");
                            Int32 ContactGeboortedatumPos = rdrFuncties.GetOrdinal("Geboortedatum");
                            Int32 ContactGeslachtPos = rdrFuncties.GetOrdinal("Aanspreking");
                            Int32 ContactNotitiePos = rdrFuncties.GetOrdinal("Notities");
                            Int32 ContactAangemaaktPos = rdrFuncties.GetOrdinal("CAangemaakt");
                            Int32 ContactAangemaaktDoorPos = rdrFuncties.GetOrdinal("CAangemaaktD");
                            Int32 ContactGewijzigdPos = rdrFuncties.GetOrdinal("CGewijzigd");
                            Int32 ContactGewijzigdDoorPos = rdrFuncties.GetOrdinal("CGewijzigdD");
                            Int32 ContactActiefPos = rdrFuncties.GetOrdinal("CActief");
                            Int32 TaalPos = rdrFuncties.GetOrdinal("TaalNaam");
                            Int32 SecundaireTaalPos = rdrFuncties.GetOrdinal("SecTaalNaam");
                            Int32 niveau1Pos = rdrFuncties.GetOrdinal("Niveau1Naam");
                            Int32 niveau2Pos = rdrFuncties.GetOrdinal("Niveau2Naam");
                            Int32 niveau3Pos = rdrFuncties.GetOrdinal("Niveau3Naam");
                            Int32 bedrijfPos = rdrFuncties.GetOrdinal("BedrijfNaam");
                            Int32 btwnummerPos = rdrFuncties.GetOrdinal("Btwnummer");
                            Int32 startdatumPos = rdrFuncties.GetOrdinal("Startdatum");
                            Int32 einddatumPos = rdrFuncties.GetOrdinal("Einddatum");
                            Int32 titelPos = rdrFuncties.GetOrdinal("Oorspronkelijke_Titel");
                            Int32 BedrijfsIDPos = rdrFuncties.GetOrdinal("BID");
                            Int32 bedrijfTelefoonPos = rdrFuncties.GetOrdinal("Btelefoon");
                            Int32 bedrijfFaxPos = rdrFuncties.GetOrdinal("Bfax");
                            Int32 bedrijfEmailPos = rdrFuncties.GetOrdinal("BEmail");
                            Int32 websitePos = rdrFuncties.GetOrdinal("Website");
                            Int32 bedrijfNotitiePos = rdrFuncties.GetOrdinal("BNotities");
                            Int32 bedrijfAdresPos = rdrFuncties.GetOrdinal("Adres");
                            Int32 bedrijfPostcodePos = rdrFuncties.GetOrdinal("Postcode");
                            Int32 bedrijfPlaatsPos = rdrFuncties.GetOrdinal("Plaats");
                            Int32 bedrijfslandPos = rdrFuncties.GetOrdinal("Land");
                            Int32 bedrijfActiefPos = rdrFuncties.GetOrdinal("BActief");
                            Int32 emailPos = rdrFuncties.GetOrdinal("Email");
                            Int32 telefoonPos = rdrFuncties.GetOrdinal("Telefoon");
                            Int32 mailPos = rdrFuncties.GetOrdinal("Mail");
                            Int32 faxPos = rdrFuncties.GetOrdinal("Fax");
                            Int32 AttesttyePos = rdrFuncties.GetOrdinal("Attesttype");
                            Int32 ErkenningsnummerPos = rdrFuncties.GetOrdinal("Erkenningsnummer");
                            Int32 aangemaaktDoorPos = rdrFuncties.GetOrdinal("Aangemaakt_door");
                            Int32 aangemaaktPos = rdrFuncties.GetOrdinal("Aangemaakt");
                            Int32 gewijzigdDoorPos = rdrFuncties.GetOrdinal("Gewijzigd_door");
                            Int32 gewijzigdPos = rdrFuncties.GetOrdinal("Gewijzigd");
                            Int32 typePos = rdrFuncties.GetOrdinal("Type");
                            Int32 mobielPos = rdrFuncties.GetOrdinal("Mobiel");
                            Int32 functieIdPos = rdrFuncties.GetOrdinal("FunctieID");
                            Int32 functieActiefPos = rdrFuncties.GetOrdinal("FActief");


                            while (rdrFuncties.Read())
                            {
                                FilterFunctie functie = new FilterFunctie();
                                functie.ID = rdrFuncties.GetDecimal(idPos);
                                functie.Contact_ID = rdrFuncties.GetDecimal(ContactIDPos);
                                functie.Voornaam = rdrFuncties.GetString(ContactVoornaamPos);
                                functie.Achternaam = rdrFuncties.GetString(ContactAchternaamPos);
                                if (!rdrFuncties.IsDBNull(ContactGeboortedatumPos))
                                    functie.Geboortedatum = rdrFuncties.GetDateTime(ContactGeboortedatumPos);
                                functie.TaalNaam = rdrFuncties.IsDBNull(TaalPos) ? "" : rdrFuncties.GetString(TaalPos);
                                functie.SecundaireTaalNaam = rdrFuncties.IsDBNull(SecundaireTaalPos) ? "" : rdrFuncties.GetString(SecundaireTaalPos);
                                functie.Aanspreking = rdrFuncties.IsDBNull(ContactGeslachtPos) ? "" : rdrFuncties.GetString(ContactGeslachtPos);
                                functie.Contactnotities = rdrFuncties.IsDBNull(ContactNotitiePos) ? "" : rdrFuncties.GetString(ContactNotitiePos);
                                if (!rdrFuncties.IsDBNull(ContactAangemaaktPos))
                                    functie.ContactAangemaakt = rdrFuncties.GetDateTime(ContactAangemaaktPos);
                                functie.ContactAangemaakt_door = rdrFuncties.IsDBNull(ContactAangemaaktDoorPos) ? "" : rdrFuncties.GetString(ContactAangemaaktDoorPos);
                                if (!rdrFuncties.IsDBNull(ContactGewijzigdPos))
                                    functie.ContactGewijzigd = rdrFuncties.GetDateTime(ContactGewijzigdPos);
                                functie.ContactGewijzigd_door = rdrFuncties.IsDBNull(ContactGewijzigdDoorPos) ? "" : rdrFuncties.GetString(ContactGewijzigdDoorPos);
                                functie.Contactactief = rdrFuncties.GetBoolean(ContactActiefPos);
                                functie.Niveau1Naam = rdrFuncties.IsDBNull(niveau1Pos) ? "" : rdrFuncties.GetString(niveau1Pos);
                                functie.Niveau2Naam = rdrFuncties.IsDBNull(niveau2Pos) ? "" : rdrFuncties.GetString(niveau2Pos);
                                functie.Niveau3Naam = rdrFuncties.IsDBNull(niveau3Pos) ? "" : rdrFuncties.GetString(niveau3Pos);
                                functie.Attesttype = rdrFuncties.IsDBNull(AttesttyePos) ? "" : rdrFuncties.GetString(AttesttyePos);
                                functie.Erkenningsnummer = rdrFuncties.IsDBNull(ErkenningsnummerPos) ? "" : rdrFuncties.GetString(ErkenningsnummerPos);
                                functie.Bedrijfsnaam = rdrFuncties.IsDBNull(bedrijfPos) ? "" : rdrFuncties.GetString(bedrijfPos);
                                functie.Btwnummer = rdrFuncties.IsDBNull(btwnummerPos) ? "" : rdrFuncties.GetString(btwnummerPos);
                                functie.BedrijfsTelefoon = rdrFuncties.IsDBNull(bedrijfTelefoonPos) ? "" : rdrFuncties.GetString(bedrijfTelefoonPos);
                                functie.BedrijfsFax = rdrFuncties.IsDBNull(bedrijfFaxPos) ? "" : rdrFuncties.GetString(bedrijfFaxPos);
                                functie.BedrijfsEmail = rdrFuncties.IsDBNull(bedrijfEmailPos) ? "" : rdrFuncties.GetString(bedrijfEmailPos);
                                functie.Website = rdrFuncties.IsDBNull(websitePos) ? "" : rdrFuncties.GetString(websitePos);
                                functie.BedrijfsAdres = rdrFuncties.IsDBNull(bedrijfAdresPos) ? "" : rdrFuncties.GetString(bedrijfAdresPos);
                                functie.BedrijfsPostcode = rdrFuncties.IsDBNull(bedrijfPostcodePos) ? "" : rdrFuncties.GetString(bedrijfPostcodePos);
                                functie.BedrijfsPlaats = rdrFuncties.IsDBNull(bedrijfPlaatsPos) ? "" : rdrFuncties.GetString(bedrijfPlaatsPos);
                                functie.Land = rdrFuncties.IsDBNull(bedrijfslandPos) ? "" : rdrFuncties.GetString(bedrijfslandPos);
                                functie.Bedrijfsnotities = rdrFuncties.IsDBNull(bedrijfNotitiePos) ? "" : rdrFuncties.GetString(bedrijfNotitiePos);
                                functie.Bedrijfactief = rdrFuncties.IsDBNull(bedrijfActiefPos) ? false : rdrFuncties.GetBoolean(bedrijfActiefPos);
                                functie.Email = rdrFuncties.IsDBNull(emailPos) ? "" : rdrFuncties.GetString(emailPos);
                                functie.Telefoon = rdrFuncties.IsDBNull(telefoonPos) ? "" : rdrFuncties.GetString(telefoonPos);
                                functie.Mail = rdrFuncties.IsDBNull(mailPos) ? false : rdrFuncties.GetBoolean(mailPos);
                                functie.Fax = rdrFuncties.IsDBNull(faxPos) ? false : rdrFuncties.GetBoolean(faxPos);
                                functie.Mobiel = rdrFuncties.IsDBNull(mobielPos) ? "" : rdrFuncties.GetString(mobielPos);
                                functie.Functieactief = rdrFuncties.IsDBNull(functieActiefPos) ? false : rdrFuncties.GetBoolean(functieActiefPos);
                                if (!rdrFuncties.IsDBNull(startdatumPos))
                                    functie.Startdatum = rdrFuncties.GetDateTime(startdatumPos);
                                if (!rdrFuncties.IsDBNull(einddatumPos))
                                    functie.Einddatum = rdrFuncties.GetDateTime(einddatumPos);
                                functie.Aangemaakt_door = rdrFuncties.IsDBNull(aangemaaktDoorPos) ? "" : rdrFuncties.GetString(aangemaaktDoorPos);
                                if (!rdrFuncties.IsDBNull(aangemaaktPos))
                                    functie.Aangemaakt = rdrFuncties.GetDateTime(aangemaaktPos);
                                functie.Gewijzigd_door = rdrFuncties.IsDBNull(gewijzigdDoorPos) ? "" : rdrFuncties.GetString(gewijzigdDoorPos);
                                if (!rdrFuncties.IsDBNull(gewijzigdPos))
                                    functie.Gewijzigd = rdrFuncties.GetDateTime(gewijzigdPos);
                                functie.Type = rdrFuncties.IsDBNull(typePos) ? "" : rdrFuncties.GetString(typePos);
                                functie.Oorspronkelijke_Titel = rdrFuncties.IsDBNull(titelPos) ? "" : rdrFuncties.GetString(titelPos);
                                if (functie.Einddatum == null || functie.Einddatum >= DateTime.Today)
                                    functies.Add(functie);
                            }
                        }
                    }
                }
                return functies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InschrijvingSessie> GetInschrijvingSessies()
        {
            try
            {
                List<InschrijvingSessie> sessies = new List<InschrijvingSessie>();
                using (var conConfocus = this.GetConnection())
                {
                    using (var comSessies = conConfocus.CreateCommand())
                    {
                        comSessies.CommandType = System.Data.CommandType.StoredProcedure;
                        comSessies.CommandText = "GetInschrijvingSessies";
                        conConfocus.Open();
                        using (var rdrSessies = comSessies.ExecuteReader())
                        {
                            Int32 IDPos = rdrSessies.GetOrdinal("ID");
                            Int32 SemIDPos = rdrSessies.GetOrdinal("Seminarie_ID");
                            Int32 SemTitelPos = rdrSessies.GetOrdinal("Seminarie");
                            Int32 NaamPos = rdrSessies.GetOrdinal("Naam");
                            Int32 StatusPos = rdrSessies.GetOrdinal("Status");
                            Int32 DatumPos = rdrSessies.GetOrdinal("Datum");
                            Int32 ActiefPos = rdrSessies.GetOrdinal("Actief");
                            Int32 LocatiePos = rdrSessies.GetOrdinal("Locatie");

                            while (rdrSessies.Read())
                            {
                                InschrijvingSessie sessie = new InschrijvingSessie();
                                sessie.ID = rdrSessies.GetDecimal(IDPos);
                                sessie.Seminarie_ID = rdrSessies.IsDBNull(SemIDPos) ? 0 : rdrSessies.GetDecimal(SemIDPos);
                                sessie.SeminarieNaam = rdrSessies.IsDBNull(SemTitelPos) ? "" : rdrSessies.GetString(SemTitelPos);
                                sessie.Naam = rdrSessies.GetString(NaamPos);
                                sessie.Status = rdrSessies.IsDBNull(StatusPos) ? "" : rdrSessies.GetString(StatusPos);
                                if (!rdrSessies.IsDBNull(DatumPos))
                                    sessie.Datum = rdrSessies.GetDateTime(DatumPos);
                                sessie.Actief = rdrSessies.GetBoolean(ActiefPos);
                                sessie.LocatieNaam = rdrSessies.IsDBNull(LocatiePos) ? "" : rdrSessies.GetString(LocatiePos);
                                sessies.Add(sessie);
                            }
                        }
                    }
                }
                return sessies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

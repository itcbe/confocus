﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ConfocusDBLibrary;
using System.Windows.Controls;
using ConfocusClassLibrary;
using Outlook = Microsoft.Office.Interop;
using ITCLibrary;

namespace ConfocusWPF
{
    public partial class Aanwezigheden
    {
        // Functions regarding to mailing Speakers

        private void LoadSpeakers()
        {
            try
            {
                Seminarie seminarie = (Seminarie)sprekerSeminarieComboBox.SelectedItem;
                Sessie sessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                if (sprekerSessieComboBox.SelectedIndex <= 0)
                    sprekers = new SessieSprekerServices().GetSprekersBySeminarieID(seminarie.ID);
                else
                    sprekers = new SessieSprekerServices().GetSprekersBySessieID(sessie.ID);
                List<Sessie_Spreker> uniekeSprekers = new List<Sessie_Spreker>();
                foreach (Sessie_Spreker spreker in sprekers)
                {
                    bool gevonden = false;
                    foreach (Sessie_Spreker item in uniekeSprekers)
                    {
                        if (spreker.Spreker.Contact_ID == item.Spreker.Contact_ID)
                        {
                            gevonden = true;
                            break;
                        }
                    }
                    if (!gevonden)
                        uniekeSprekers.Add(spreker);
                }

                sessie_SprekerViewSource.Source = uniekeSprekers;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van de sprekers! " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
            
        }
        private void sprekerMailButton_Click(object sender, RoutedEventArgs e)
        {
            string template = sprekerAttestTemplateComboBox.Text;
            switch (template)
            {
                //case "Attest":
                //    SendSprekerAttesten();
                //    break;
                case "Annulatie":
                    SendSprekerAnnulatie();
                    break;
                case "Blanco":
                    SendSprekerBlanco();
                    break;
                case "Praktische afspraken":
                    SendAfspraken();
                    break;
                case "Samenvatting evaluatie en attest":
                    SendSamenvatting();
                    break;
                default:
                    break;
            }
        }

        private void SendSprekerAnnulatie()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                foreach (Sessie_Spreker spreker in sprekerDataGrid.Items)
                {
                    sprekers.Add(spreker);
                }
                Sessie sessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                    sessie = null;
                foreach (Sessie_Spreker spreker in sprekers)
                {

                    string mailbody = StelSprekerMailBodyOpPlanning(sessie, spreker);
                    TaalServices tService = new TaalServices();
                    Taal taal = tService.GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID);
                    string Mailto = spreker.Spreker.Email;
                    if (!string.IsNullOrEmpty(Mailto))
                    {
                        try
                        {
                            //ConfocusClassLibrary.Attachment attest = GetSprekerAanwezigheidsAttest(spreker);
                            Outlook.Outlook.Application oApp = new Outlook.Outlook.Application();
                            Outlook.Outlook._MailItem oMailItem = (Outlook.Outlook._MailItem)oApp.CreateItem(Outlook.Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            //oMailItem.Attachments.Add(attest.Link, Outlook.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                            if (taal.Code == "FR")
                                if (sessie != null)
                                    oMailItem.Subject = "Annulation de formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Annulation de formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            else
                                if (sessie != null)
                                oMailItem.Subject = "Annulatie Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                            else
                                oMailItem.Subject = "Annulatie Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            oMailItem.To = Mailto;
                            oMailItem.HTMLBody = mailbody;
                            oMailItem.Importance = Outlook.Outlook.OlImportance.olImportanceHigh;
                            oMailItem.Display(false);
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(spreker.Spreker.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }

            }
            catch (System.Exception ex)
            {

                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SendSamenvatting()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                foreach (Sessie_Spreker spreker in sprekerDataGrid.Items)
                {
                    sprekers.Add(spreker);
                }
                Sessie sessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                    sessie = null;
                foreach (Sessie_Spreker spreker in sprekers)
                {

                    string mailbody = StelSprekerMailBodyOpPlanning(sessie, spreker);
                    TaalServices tService = new TaalServices();
                    Taal taal = tService.GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID);
                    string Mailto = spreker.Spreker.Email;
                    if (!string.IsNullOrEmpty(Mailto))
                    {
                        try
                        {
                            ConfocusClassLibrary.Attachment attest = GetSprekerAanwezigheidsAttest(spreker);
                            Outlook.Outlook.Application oApp = new Outlook.Outlook.Application();
                            Outlook.Outlook._MailItem oMailItem = (Outlook.Outlook._MailItem)oApp.CreateItem(Outlook.Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            oMailItem.Attachments.Add(attest.Link, Outlook.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                            if (taal.Code == "FR")
                                if (sessie != null)
                                    oMailItem.Subject = "Evaluation sommaire á la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Evaluation sommaire á la formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            else
                                if (sessie != null)
                                oMailItem.Subject = "Samenvatting evaluatie: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                            else
                                oMailItem.Subject = "Samenvatting evaluatie: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            oMailItem.To = Mailto;
                            oMailItem.HTMLBody = mailbody;
                            oMailItem.Importance = Outlook.Outlook.OlImportance.olImportanceHigh;
                            oMailItem.Display(false);

                            try
                            {
                                spreker.AttestVerstuurd = true;
                                new SessieSprekerServices().Update(spreker);
                            }
                            catch (Exception spEx)
                            {
                                ConfocusMessageBox.Show(spEx.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(spreker.Spreker.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }

            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SendAfspraken()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                foreach (Sessie_Spreker spreker in sprekerDataGrid.Items)
                {
                    sprekers.Add(spreker);
                }
                Sessie sessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                    sessie = null;
                foreach (Sessie_Spreker spreker in sprekers)
                {

                    string mailbody = StelSprekerMailBodyOpPlanning(sessie, spreker);
                    TaalServices tService = new TaalServices();
                    Taal taal = tService.GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID);
                    string Mailto = spreker.Spreker.Email;
                    if (!string.IsNullOrEmpty(Mailto))
                    {
                        try
                        {
                            Outlook.Outlook.Application oApp = new Outlook.Outlook.Application();
                            Outlook.Outlook._MailItem oMailItem = (Outlook.Outlook._MailItem)oApp.CreateItem(Outlook.Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            //oMailItem.Attachments.Add(attest.Link, Outlook.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                            if (taal.Code == "FR")
                                if (sessie != null)
                                    oMailItem.Subject = "Modalités pratiques á la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Modalités pratiques á la formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            else
                                if (sessie != null)
                                oMailItem.Subject = "Praktische afspraken " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                            else
                                oMailItem.Subject = "Praktische afspraken " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            oMailItem.To = Mailto;
                            oMailItem.HTMLBody = mailbody;
                            oMailItem.Importance = Outlook.Outlook.OlImportance.olImportanceHigh;
                            oMailItem.Display(false);
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(spreker.Spreker.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }

            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void SendSprekerBlanco()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                foreach (Sessie_Spreker spreker in sprekerDataGrid.Items)
                {
                    sprekers.Add(spreker);
                }
                Sessie sessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                    sessie = null;
                string MailtoNl = "";
                string MailtoFr = "";
                List<string> mailsnl = new List<string>();
                List<string> mailsfr = new List<string>();
                int aantalMailadressenNl = 0;
                int aantalMailadressenFr = 0;
                foreach (Sessie_Spreker spreker in sprekers)
                {
                    Spreker mySpreker = new SprekerServices().GetSprekerById(spreker.Spreker_ID);
                    if (mySpreker.Email != "")
                    {
                        if (mySpreker.Contact.Taal.Code == "FR")
                        {
                            if (aantalMailadressenFr == 10)
                            {
                                aantalMailadressenFr = 0;
                                mailsfr.Add(MailtoFr);
                                MailtoFr = mySpreker.Email + "; ";
                                aantalMailadressenFr++;
                            }
                            else
                            {
                                MailtoFr += mySpreker.Email + "; ";
                                aantalMailadressenFr++;
                            }
                        }
                        else
                        {
                            if (aantalMailadressenNl == 10)
                            {
                                aantalMailadressenNl = 0;
                                mailsnl.Add(MailtoNl);
                                MailtoNl = mySpreker.Email + "; ";
                                aantalMailadressenNl++;
                            }
                            else
                            {
                                MailtoNl += mySpreker.Email + "; ";
                                aantalMailadressenNl++;
                            }
                        }
                    }
                }
                if (!mailsfr.Contains(MailtoFr))
                    mailsfr.Add(MailtoFr);
                if (!mailsnl.Contains(MailtoNl))
                    mailsnl.Add(MailtoNl);
                foreach (string mailtonl in mailsnl)
                {
                    if (!String.IsNullOrEmpty(mailtonl))
                    {
                        try
                        {
                            Outlook.Outlook.Application oApp = new Outlook.Outlook.Application();
                            Outlook.Outlook._MailItem oMailItem = (Outlook.Outlook._MailItem)oApp.CreateItem(Outlook.Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            if (sessie != null)
                                oMailItem.Subject = "Studiedag " + sessie.SeminarieTitel + ": " + sessie.Naam + " " + ((DateTime)sessie.Datum).ToShortDateString();
                            else
                                oMailItem.Subject = "Studiedag " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;

                            oMailItem.BCC = mailtonl;
                            oMailItem.HTMLBody = "";
                            oMailItem.Display(true);
                            //oMailItem.Send();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                    }

                }
                foreach (string mailtofr in mailsfr)
                {
                    if (!string.IsNullOrEmpty(mailtofr))
                    {
                        try
                        {
                            Outlook.Outlook.Application oApp = new Outlook.Outlook.Application();
                            Outlook.Outlook._MailItem oMailItem = (Outlook.Outlook._MailItem)oApp.CreateItem(Outlook.Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            if (sessie != null)
                                oMailItem.Subject = "La formation Confocus " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + ")";
                            else
                                oMailItem.Subject = "La formation Confocus " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;

                            oMailItem.BCC = mailtofr;
                            oMailItem.HTMLBody = "";
                            oMailItem.Display(true);
                            //oMailItem.Send();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SendSprekerAttesten()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                foreach (Sessie_Spreker spreker in sprekerDataGrid.Items)
                {
                    sprekers.Add(spreker);
                }
                Sessie sessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                    sessie = null;
                foreach (Sessie_Spreker spreker in sprekers)
                {

                    string mailbody = StelSprekerMailBodyOpPlanning(sessie, spreker);
                    TaalServices tService = new TaalServices();
                    Taal taal = tService.GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID);
                    string Mailto = spreker.Spreker.Email;
                    if (!string.IsNullOrEmpty(Mailto))
                    {
                        try
                        {
                            ConfocusClassLibrary.Attachment attest = GetSprekerAanwezigheidsAttest(spreker);
                            if (attest != null)
                            {
                                Outlook.Outlook.Application oApp = new Outlook.Outlook.Application();
                                Outlook.Outlook._MailItem oMailItem = (Outlook.Outlook._MailItem)oApp.CreateItem(Outlook.Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                oMailItem.Attachments.Add(attest.Link, Outlook.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                                if (taal.Code == "FR")
                                    if (sessie != null)
                                        oMailItem.Subject = "Votre présence á la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                    else
                                        oMailItem.Subject = "Votre présence á la formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                else
                                    if (sprekerSessieComboBox.SelectedIndex > 0)
                                    oMailItem.Subject = "Aanwezigheidsattest: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Aanwezigheidsattest: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.To = Mailto;
                                oMailItem.HTMLBody = mailbody;
                                oMailItem.Importance = Outlook.Outlook.OlImportance.olImportanceHigh;
                                oMailItem.Display(false);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(spreker.Spreker.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }

            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private Attachment GetSprekerAanwezigheidsAttest(Sessie_Spreker spreker)
        {
            if (spreker != null)
            {
                try
                {
                    object oMissing = System.Reflection.Missing.Value;
                    object template = GetSprekerTemplate(spreker);
                    string filename = spreker.Spreker.Contact.VolledigeNaam + GetCleanDate((DateTime)spreker.Sessie.Datum) + "_";
                    if (spreker.Spreker.AttestType_ID != null)
                    {
                        Attesttype_Services aService = new Attesttype_Services();
                        AttestType type = aService.GetByID((decimal)spreker.Spreker.AttestType_ID);
                        filename += type.Naam + ".pdf";
                    }
                    //else if (!string.IsNullOrEmpty(deelnemer.Attesttype))
                    //{
                    //    filename += deelnemer.Attesttype + ".pdf";
                    //}
                    else
                    {
                        filename += "administratieattest.pdf";
                    }

                    string path = _dataFolder + @"\Aanwezigheids Attesten\Sprekers\" + filename;
                    Sessie sessie = new SessieServices().GetSessieByID((decimal)spreker.Sessie_ID);
                    Seminarie_ErkenningenService sService = new Seminarie_ErkenningenService();
                    List<Seminarie_Erkenningen> semErkenningen = sService.GetBySeminarieID((decimal)sessie.Seminarie_ID);

                    WordDocument doc;
                    try
                    {
                        doc = new WordDocument(template, "Staand");
                    }
                    catch (System.Exception ex)
                    {
                        throw new System.Exception("Er is geen template beschikbaar voor deze erkenning!");
                    }
                    //doc.save(path);
                    List<String> fields = new List<string>();
                    //1
                    fields.Add(spreker.Spreker.Contact.Voornaam);
                    //2
                    fields.Add(spreker.Spreker.Contact.Achternaam);
                    //3
                    fields.Add(spreker.Spreker.Oorspronkelijke_Titel);
                    //4
                    string bedrijfsnaam = "";
                    try
                    {
                        bedrijfsnaam = spreker.Spreker.Bedrijf.Naam;
                    }
                    catch (Exception)
                    {

                        bedrijfsnaam = new BedrijfServices().GetBedrijfByID((decimal)spreker.Spreker.Bedrijf_ID).Naam;
                    }
                    fields.Add(bedrijfsnaam);
                    //5
                    fields.Add(((Seminarie)seminarieComboBox.SelectedItem).Titel);
                    //6
                    if (sessie != null)
                        fields.Add(sessie.Naam);
                    else
                        fields.Add("");
                    //7
                    if (sessie.Locatie != null)
                        fields.Add(sessie.Locatie.Naam);
                    else
                        fields.Add("");
                    //8
                    fields.Add(((DateTime)sessie.Datum).ToShortDateString());
                    //9
                    fields.Add(sessie.Beginuur);
                    //10
                    fields.Add(sessie.Einduur);

                    //fields.Add(dagtype);
                    //11
                    fields.Add("");

                    string erknummersessie = "";
                    //string erknummercontact = "";
                    string erkAantal = "";
                    foreach (Seminarie_Erkenningen erk in semErkenningen)
                    {
                        if (erk.Sessie_ID == sessie.ID && spreker.Spreker.Attesttype == erk.Erkenning.Attest)
                        {
                            erknummersessie = erk.Nummer;
                            break;
                        }
                        else
                        {
                            if (spreker.Spreker.Attesttype == erk.Erkenning.Naam)
                            {
                                erknummersessie = erk.Nummer;
                            }
                        }

                    }
                    //12
                    fields.Add(spreker.Spreker.Erkenningsnummer);
                    //13
                    fields.Add(erknummersessie);
                    //14
                    if (erkAantal != "")
                        fields.Add(erkAantal);
                    else
                        fields.Add("");
                    //15
                    fields.Add(DateTime.Now.ToShortDateString());
                    //16
                    fields.Add("");
                    //17
                    fields.Add(spreker.Spreker.Type);
                    //18
                    fields.Add("");
                    //19
                    fields.Add("");
                    //20
                    fields.Add("");
                    //21
                    fields.Add(spreker.Uren);
                    doc.Activate();
                    doc.ChangeFields(fields);
                    doc.SaveAsPDF(path);
                    //doc.save(path);
                    doc.quit();

                    ConfocusClassLibrary.Attachment attachment = new ConfocusClassLibrary.Attachment();
                    attachment.Naam = "Aanwezigheids attest";
                    attachment.Link = path;

                    return attachment;

                }
                catch (System.Exception ex)
                {
                    throw new System.Exception(ex.Message);
                }
            }
            else
                return null;
        }

        private object GetSprekerTemplate(Sessie_Spreker spreker)
        {
            string template = _dataFolder +@"\Templates\";
            string filename = "";
            string taal = new TaalServices().GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID).Code;
            AttestType attest = null;
            if (spreker.Spreker.AttestType_ID != null)
            {
                Attesttype_Services aService = new Attesttype_Services();
                attest = aService.GetByID((decimal)spreker.Spreker.AttestType_ID);
            }
            //string contactAttest = deelnemer.Functies.Contact.Attesttype;
            if (attest != null)
            {
                if (attest.Naam != "Geen")
                {
                    List<AttestType> attesten = new Attesttype_Services().FindActive();
                    foreach (var myattest in attesten)
                    {
                        if (myattest.Naam == attest.Naam)
                        {
                            if (sessieComboBox.SelectedIndex < 1)
                                filename = myattest.Naam + "_Spreker_RECHTSDAG_" + taal + ".docx";
                            else
                                filename = myattest.Naam + "_Spreker_" + taal + ".docx";
                            break;
                        }
                    }
                    if (filename == "")
                        filename = "Administratief_Spreker_" + taal + ".docx";
                }
                else
                    filename = "Administratief_Spreker" + taal + ".docx";
            }
            else
            {
                filename = "Administratief_" + taal + ".docx";
            }

            return template + filename;
        }

        private string StelSprekerMailBodyOpPlanning(Sessie sessie, Sessie_Spreker spreker)
        {
            Sessie fullSessie = new SessieServices().GetSessieByID((decimal)spreker.Sessie_ID);
            if (fullSessie != null || fullSessie.ID != 0)
            {
                string body = "";

                string filename = GetCorrectSprekerFilename(spreker);
                string naam = spreker.Spreker.Contact.Achternaam;
                string aanspreking = GetSprekerAanspreking(spreker);
                string verantwoordelijke = GetSessieVerantwoordelijke(fullSessie.Verantwoordelijke_Sessie_ID);
                string verantwoordelijkeEmail = GetSessieVerantwoordelijkeEmail(fullSessie.Verantwoordelijke_Sessie_ID);
                int aantaldeelnemers = new InschrijvingenService().GetAantalEffectieveInschrijvingenFromSessie(fullSessie.ID);
                string adres = fullSessie.Locatie.FullAddress;

                string agenda = SprekerEditor.ContentHtml;
                body = System.IO.File.ReadAllText(filename);
                body = body.Replace("#aanspreking#", aanspreking);
                body = body.Replace("#naam#", naam);
                body = body.Replace("#achternaam_spreker#", naam);
                body = body.Replace("#beginuur_sessie#", fullSessie.Beginuur);
                body = body.Replace("#einduur_sessie#", fullSessie.Einduur);
                body = body.Replace("#url_sessie#", fullSessie.URL);
                body = body.Replace("#naam_sessie#", fullSessie.Naam);
                body = body.Replace("#datum_sessie#", ((DateTime)fullSessie.Datum).ToShortDateString());
                // body = body.Replace("#Paswoord#", paswoord);
                //body = body.Replace("#nieuwe_datum#", nieuweDatum);
                //body = body.Replace("#nieuwe_locatie#", nieuweLocatie);
                //body = body.Replace("#Periode#", "Voorjaar 2016");

                body = body.Replace("#Extra#", agenda);
                body = body.Replace("#locatie_sessie#", fullSessie.Locatie.Naam);
                body = body.Replace("#adres_locatie#", adres);
                body = body.Replace("#naam_seminarie#", fullSessie.SeminarieTitel);
                body = body.Replace("#seminarie#", fullSessie.SeminarieTitel);
                body = body.Replace("#naam_sessieverantwoordelijke#", verantwoordelijke);
                body = body.Replace("#e-mailverantwoordelijke​sessie#", verantwoordelijkeEmail);
                body = body.Replace("#aantal_inschrijvingen_sessie#", aantaldeelnemers.ToString());

                return body;
            }
            else
            {
                Seminarie seminarie = (Seminarie)seminarieComboBox.SelectedItem;
                List<Sessie> zalen = new List<Sessie>();
                string planning = "";
                if (seminarie != null)
                {
                    planning += "<ul>";
                    zalen = new SessieServices().GetSessiesBySeminarieID(seminarie.ID);
                    foreach (Sessie zaal in zalen)
                    {
                        if (spreker.Sessie_ID == zaal.ID)
                        {
                            planning += "<li>" + zaal.Beginuur + " tot " + zaal.Einduur + " - " + zaal.Naam + "</li>";
                            
                        }
                    }
                    planning += "</ul>";
                }


                
                string body = "";

                string filename = GetCorrectSprekerFilename(spreker);
                string naam = spreker.Spreker.Contact.Achternaam;
                string aanspreking = GetSprekerAanspreking(spreker);
                //string verantwoordelijke = GetSessieVerantwoordelijke(fullSessie.Verantwoordelijke_Sessie_ID);
                //string verantwoordelijkeEmail = GetSessieVerantwoordelijkeEmail(fullSessie.Verantwoordelijke_Sessie_ID);
                //int aantaldeelnemers = new InschrijvingenService().GetAantalEffectieveInschrijvingenFromSessie(fullSessie.ID);
                //string adres = fullSessie.Locatie.FullAddress;


                string agenda = SprekerEditor.ContentHtml;
                body = System.IO.File.ReadAllText(filename);
                body = body.Replace("#aanspreking#", aanspreking);
                body = body.Replace("#naam#", naam);
                body = body.Replace("#achternaam_spreker#", naam);
                body = body.Replace("#planning#", planning);
                //body = body.Replace("#beginuur_sessie#", fullSessie.Beginuur);
                //body = body.Replace("#einduur_sessie#", fullSessie.Einduur);
                //body = body.Replace("#url_sessie#", fullSessie.URL);
                //body = body.Replace("#naam_sessie#", fullSessie.Naam);
                //body = body.Replace("#datum_sessie#", ((DateTime)fullSessie.Datum).ToShortDateString());
                // body = body.Replace("#Paswoord#", paswoord);
                //body = body.Replace("#nieuwe_datum#", nieuweDatum);
                //body = body.Replace("#nieuwe_locatie#", nieuweLocatie);
                //body = body.Replace("#Periode#", "Voorjaar 2016");

                body = body.Replace("#Extra#", agenda);
                //body = body.Replace("#locatie_sessie#", fullSessie.Locatie.Naam);
                //body = body.Replace("#adres_locatie#", adres);
                //body = body.Replace("#naam_seminarie#", fullSessie.SeminarieTitel);
                //body = body.Replace("#seminarie#", fullSessie.SeminarieTitel);
                //body = body.Replace("#naam_sessieverantwoordelijke#", verantwoordelijke);
                //body = body.Replace("#e-mailverantwoordelijke​sessie#", verantwoordelijkeEmail);
                //body = body.Replace("#aantal_inschrijvingen_sessie#", aantaldeelnemers.ToString());

                return body;
                throw new System.Exception(ErrorMessages.SessieIsNull);
            }
        }

        private string GetSprekerAanspreking(Sessie_Spreker spreker)
        {
            var geslacht = spreker.Spreker.Contact.Aanspreking;
            AansprekingServices aService = new AansprekingServices();
            Aanspreking aanspreking = aService.GetByLanguage((decimal)spreker.Spreker.Contact.Taal_ID);

            if (geslacht.ToLower() == "v")
                return aanspreking.Aanspreking_vrouw;
            else
                return aanspreking.Aanspreking_man;
        }

        private string GetCorrectSprekerFilename(Sessie_Spreker spreker)
        {
            String name = sprekerAttestTemplateComboBox.Text;
            String language = new TaalServices().GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID).Code;
            name += " " + language;
            AttestTemplate template = new AttestTemplate_Service().GetByName(name)[0];
            String filename = template.Uri;
            return filename;
        }

        private void sprekerFilterButton_Click(object sender, RoutedEventArgs e)
        {
            LoadSpeakers();
        }

        private void ExtraSprekerAttestButton_Click(object sender, RoutedEventArgs e)
        {
            Sessie_Spreker spreker = (Sessie_Spreker)sprekerDataGrid.SelectedItem;
            bool rechtsdag = false;
            if (sessieComboBox.SelectedIndex < 1)
                rechtsdag = true;
            ExtraAttestWindow EA = new ExtraAttestWindow(spreker.ID, "Spreker", rechtsdag, currentUser);
            EA.Show();
            EA.Activate();
        }

        private void sessieSprekerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadSpeakers();
        }

        private void sprekerSeminarieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sprekerSeminarieComboBox.SelectedIndex > 0)
            {
                Seminarie seminarie = (Seminarie)sprekerSeminarieComboBox.SelectedItem;
                SessieServices sesService = new SessieServices();
                List<Sessie> sessies = sesService.GetSessiesBySeminarieID(seminarie.ID);
                Sessie firstSessie = new Sessie();
                firstSessie.Naam = "Alle deelnemers";
                sessies.Insert(0, firstSessie);
                sprekerSessieComboBox.ItemsSource = sessies;
                sprekerSessieComboBox.DisplayMemberPath = "NaamDatumLocatie";
            }
        }
    }
}

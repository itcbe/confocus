﻿using ConfocusClassLibrary;
using data = ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Windows.Threading;
using System.Threading;
using System.Threading.Tasks;

namespace ConfocusWPF
{
    public partial class NieuweSeminarie
    {

        #region Sessie functies

        private int sessieindex = 0;
        private int indexBeforeSave = 0;
        private data.Sessie SessieBeforeSave;

        private void BindSessie(data.Sessie sessie)
        {
            if (sessie != null)
            {
                try
                {
                    naamTextBox.Text = sessie.Naam;
                    if (sessie.Locatie_ID != null)
                    {
                        data.LocatieServices lService = new data.LocatieServices();
                        data.Locatie locatie = lService.GetLocatieByID((decimal)sessie.Locatie_ID);
                        if (locatie != null)
                            locatieComboBox1.Text = locatie.Naam;
                    }
                    else
                        locatieComboBox1.SelectedIndex = -1;
                    if (sessie.Datum != null)
                        datumDatePicker.SelectedDate = sessie.Datum;
                    else
                        datumDatePicker.SelectedDate = null;

                    if (sessie.Sorteerdatum != null)
                        verborgenDatumDatePicker.SelectedDate = sessie.Sorteerdatum;
                    else
                        verborgenDatumDatePicker.SelectedDate = null;
                    beginuurTextBox.Text = sessie.Beginuur;
                    einduurTextBox.Text = sessie.Einduur;
                    statusComboBox.Text = sessie.Status;
                    status_infoTextBox.Text = sessie.Status_info;
                    //zaalTextBox.Text = sessie.Zaal;
                    uRLTextBox.Text = sessie.URL;
                    standaard_prijsTextBox.Text = sessie.Standaard_prijs;
                    prijs_Non_ProfitTextBox.Text = sessie.Prijs_Non_Profit;
                    prijs_Profit_PriveTextBox.Text = sessie.Prijs_Profit_Prive;
                    targetTextBox.Text = sessie.Target;
                    kortingsprijsTextBox.Text = sessie.Kortingsprijs;
                    //opstelling_zaalTextBox.Text = sessie.Opstelling_zaal;
                    //if (sessie.Erkenning_ID != null)
                    //{
                    //    data.ErkenningServices eService = new data.ErkenningServices();
                    //    data.Erkenning erkenning = eService.GetErkenningByID(sessie.Erkenning_ID);
                    //    if (erkenning != null)
                    //        erkenningComboBox.Text = erkenning.Naam;
                    //}
                    //else
                    //    erkenningComboBox.SelectedIndex = -1;
                    //sessieErkenningsnummerTextBox.Text = sessie.Erkenningsnummer;
                    catering_LunchTextBox.Text = sessie.Catering_Lunch;
                    if (!string.IsNullOrEmpty(sessie.Status_Locatie))
                        status_LocatieComboBox.Text = sessie.Status_Locatie;
                    else
                        status_LocatieComboBox.SelectedIndex = 0;
                    status_DatumTextBox.Text = sessie.Status_Datum;
                    kMO_Port_UrenTextBox.Text = sessie.KMO_Port_Uren;

                    if (sessie.Verantwoordelijke_Terplaatse_ID != null)
                    {
                        data.GebruikerService gService = new data.GebruikerService();
                        data.Gebruiker gebruiker = gService.GetUserByID(sessie.Verantwoordelijke_Terplaatse_ID);
                        if (gebruiker != null)
                            terplaatseComboBox.Text = gebruiker.Naam;
                    }
                    else
                        terplaatseComboBox.SelectedIndex = -1;
                    if (sessie.Verantwoordelijke_Sessie_ID != null)
                    {
                        data.GebruikerService gService = new data.GebruikerService();
                        data.Gebruiker gebruiker = gService.GetUserByID(sessie.Verantwoordelijke_Sessie_ID);
                        if (gebruiker != null)
                            sessieVerantwoordelijkeComboBox.Text = gebruiker.Naam;
                    }
                    else
                        sessieVerantwoordelijkeComboBox.SelectedIndex = -1;
                    //if (sessie.Beamer != null)
                    //    beamerComboBox.Text = sessie.Beamer;
                    //else
                    //    beamerComboBox.SelectedIndex = -1;
                    //if (sessie.Laptop != null)
                    //    laptopTextBox.Text = sessie.Laptop;
                    //else
                    //    laptopTextBox.SelectedIndex = -1;
                    opmerkingTextBox.Text = sessie.Opmerking;
                    //sessieErkenningsnummerTextBox.Text = sessie.Erkenningsnummer;
                    actiefCheckBox2.IsChecked = sessie.Actief;
                    IsChanged = false;

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nBindSessie module.", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void BindSessieAsync(data.Sessie sessie)
        {
            if (sessie != null)
            {
                try
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { naamTextBox.SetValue(TextBox.TextProperty, sessie.Naam); }, null);
                    //naamTextBox.Text = sessie.Naam;
                    if (sessie.Locatie_ID != null)
                    {
                        data.LocatieServices lService = new data.LocatieServices();
                        data.Locatie locatie = lService.GetLocatieByID((decimal)sessie.Locatie_ID);
                        if (locatie != null)
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { locatieComboBox1.SetValue(ComboBox.TextProperty, locatie.Naam); }, null);
                        //locatieComboBox1.Text = locatie.Naam;
                    }
                    else
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { locatieComboBox1.SetValue(ComboBox.SelectedIndexProperty, -1); }, null);
                    //locatieComboBox1.SelectedIndex = -1;
                    if (sessie.Datum != null)
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { datumDatePicker.SetValue(DatePicker.SelectedDateProperty, sessie.Datum); }, null); //datumDatePicker.SelectedDate = sessie.Datum;
                    else
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { datumDatePicker.SetValue(DatePicker.SelectedDateProperty, null); }, null); //datumDatePicker.SelectedDate = null;

                    if (sessie.Sorteerdatum != null)
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { verborgenDatumDatePicker.SetValue(DatePicker.SelectedDateProperty, sessie.Datum); }, null); //verborgenDatumDatePicker.SelectedDate = sessie.Sorteerdatum;
                    else
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { verborgenDatumDatePicker.SetValue(DatePicker.SelectedDateProperty, null); }, null); //verborgenDatumDatePicker.SelectedDate = null;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { beginuurTextBox.SetValue(TextBox.TextProperty, sessie.Beginuur); }, null);
                    //beginuurTextBox.Text = sessie.Beginuur;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { einduurTextBox.SetValue(TextBox.TextProperty, sessie.Einduur); }, null);
                    //einduurTextBox.Text = sessie.Einduur;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { statusComboBox.SetValue(ComboBox.TextProperty, sessie.Status); }, null);
                    //statusComboBox.Text = sessie.Status;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { status_infoTextBox.SetValue(TextBox.TextProperty, sessie.Status_info); }, null);
                    //status_infoTextBox.Text = sessie.Status_info;
                    //zaalTextBox.Text = sessie.Zaal;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { uRLTextBox.SetValue(TextBox.TextProperty, sessie.URL); }, null);
                    //uRLTextBox.Text = sessie.URL;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { standaard_prijsTextBox.SetValue(TextBox.TextProperty, sessie.Standaard_prijs); }, null);
                    //standaard_prijsTextBox.Text = sessie.Standaard_prijs;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { prijs_Non_ProfitTextBox.SetValue(TextBox.TextProperty, sessie.Prijs_Non_Profit); }, null);
                    //prijs_Non_ProfitTextBox.Text = sessie.Prijs_Non_Profit;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { prijs_Profit_PriveTextBox.SetValue(TextBox.TextProperty, sessie.Prijs_Profit_Prive); }, null);
                    //prijs_Profit_PriveTextBox.Text = sessie.Prijs_Profit_Prive;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { targetTextBox.SetValue(TextBox.TextProperty, sessie.Target); }, null);
                    //targetTextBox.Text = sessie.Target;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { kortingsprijsTextBox.SetValue(TextBox.TextProperty, sessie.Kortingsprijs); }, null);
                    //kortingsprijsTextBox.Text = sessie.Kortingsprijs;
                    //opstelling_zaalTextBox.Text = sessie.Opstelling_zaal;
                    //if (sessie.Erkenning_ID != null)
                    //{
                    //    data.ErkenningServices eService = new data.ErkenningServices();
                    //    data.Erkenning erkenning = eService.GetErkenningByID(sessie.Erkenning_ID);
                    //    if (erkenning != null)
                    //        erkenningComboBox.Text = erkenning.Naam;
                    //}
                    //else
                    //    erkenningComboBox.SelectedIndex = -1;
                    //sessieErkenningsnummerTextBox.Text = sessie.Erkenningsnummer;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { catering_LunchTextBox.SetValue(TextBox.TextProperty, sessie.Catering_Lunch); }, null);
                    //catering_LunchTextBox.Text = sessie.Catering_Lunch;
                    if (!string.IsNullOrEmpty(sessie.Status_Locatie))
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { status_LocatieComboBox.SetValue(ComboBox.TextProperty, sessie.Status_Locatie); }, null); //status_LocatieComboBox.Text = sessie.Status_Locatie;
                    else
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { status_LocatieComboBox.SetValue(ComboBox.SelectedIndexProperty, 0); }, null); //status_LocatieComboBox.SelectedIndex = 0;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { status_DatumTextBox.SetValue(TextBox.TextProperty, sessie.Status_Datum); }, null);
                    //status_DatumTextBox.Text = sessie.Status_Datum;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { kMO_Port_UrenTextBox.SetValue(TextBox.TextProperty, sessie.KMO_Port_Uren); }, null);
                    //kMO_Port_UrenTextBox.Text = sessie.KMO_Port_Uren;

                    if (sessie.Verantwoordelijke_Terplaatse_ID != null)
                    {
                        data.GebruikerService gService = new data.GebruikerService();
                        data.Gebruiker gebruiker = gService.GetUserByID(sessie.Verantwoordelijke_Terplaatse_ID);
                        if (gebruiker != null)
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { terplaatseComboBox.SetValue(ComboBox.TextProperty, gebruiker.Naam); }, null); //terplaatseComboBox.Text = gebruiker.Naam;
                    }
                    else
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { terplaatseComboBox.SetValue(ComboBox.SelectedIndexProperty, 0); }, null); //terplaatseComboBox.SelectedIndex = -1;
                    if (sessie.Verantwoordelijke_Sessie_ID != null)
                    {
                        data.GebruikerService gService = new data.GebruikerService();
                        data.Gebruiker gebruiker = gService.GetUserByID(sessie.Verantwoordelijke_Sessie_ID);
                        if (gebruiker != null)
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieVerantwoordelijkeComboBox.SetValue(ComboBox.TextProperty, gebruiker.Naam); }, null); //sessieVerantwoordelijkeComboBox.Text = gebruiker.Naam;
                    }
                    else
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieVerantwoordelijkeComboBox.SetValue(ComboBox.SelectedIndexProperty, 0); }, null); //sessieVerantwoordelijkeComboBox.SelectedIndex = -1;
                    //if (sessie.Beamer != null)
                    //    beamerComboBox.Text = sessie.Beamer;
                    //else
                    //    beamerComboBox.SelectedIndex = -1;
                    //if (sessie.Laptop != null)
                    //    laptopTextBox.Text = sessie.Laptop;
                    //else
                    //    laptopTextBox.SelectedIndex = -1;
                    //opmerkingTextBox.Text = sessie.Opmerking;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { opmerkingTextBox.SetValue(TextBox.TextProperty, sessie.Opmerking); }, null);
                    //actiefCheckBox2.IsChecked = sessie.Actief;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { actiefCheckBox2.SetValue(CheckBox.IsCheckedProperty, sessie.Actief); }, null);
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { IsChanged = false; }, null); // IsChanged = false;
                    //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { IsInit = true; }, null);
                    //sessieDataGrid.SelectedIndex = sessieindex;
                    //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieDataGrid.SetValue(DataGrid.IsEnabledProperty, true); }, null);
                    //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { IsInit = false; }, null);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nBindSessie module.", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }



        private data.Sessie GetSessieFromForm()
        {
            if (IsSessieValid())
            {
                try
                {
                    data.Sessie sessie = new data.Sessie();
                    sessie.Naam = naamTextBox.Text;
                    sessie.Seminarie_ID = currentSeminarie.ID;
                    if (locatieComboBox1.SelectedIndex > -1)
                        sessie.Locatie_ID = ((data.Locatie)locatieComboBox1.SelectedItem).ID;
                    sessie.Beginuur = beginuurTextBox.Text;
                    sessie.Einduur = einduurTextBox.Text;
                    int[] tijdarr = GetTijdFromText(sessie.Einduur);
                    if (datumDatePicker.SelectedDate != null)
                    {
                        DateTime d = (DateTime)datumDatePicker.SelectedDate;
                        sessie.Datum = new DateTime(d.Year, d.Month, d.Day, tijdarr[0], tijdarr[1], 0);
                    }
                    if (verborgenDatumDatePicker.SelectedDate != null)
                    {
                        DateTime d = (DateTime)verborgenDatumDatePicker.SelectedDate;
                        sessie.Sorteerdatum = new DateTime(d.Year, d.Month, d.Day, tijdarr[0], tijdarr[1], 0);//((DateTime)verborgenDatumDatePicker.SelectedDate).AddHours(tijdarr[0]).AddMinutes(tijdarr[1]);
                    }
                    else if (sessie.Datum != null)
                        sessie.Sorteerdatum = sessie.Datum;
                    sessie.Status = statusComboBox.Text;
                    sessie.Status_info = status_infoTextBox.Text;
                    //sessie.Zaal = zaalTextBox.Text;
                    sessie.URL = uRLTextBox.Text;
                    sessie.Standaard_prijs = standaard_prijsTextBox.Text;
                    sessie.Prijs_Non_Profit = prijs_Non_ProfitTextBox.Text;
                    sessie.Prijs_Profit_Prive = prijs_Profit_PriveTextBox.Text;
                    sessie.Kortingsprijs = kortingsprijsTextBox.Text;
                    sessie.Target = targetTextBox.Text;
                    //sessie.Opstelling_zaal = opstelling_zaalTextBox.Text;
                    //if (erkenningComboBox.SelectedIndex > -1)
                    //    sessie.Erkenning_ID = ((data.Erkenning)erkenningComboBox.SelectedItem).ID;
                    sessie.Catering_Lunch = catering_LunchTextBox.Text;
                    if (status_LocatieComboBox.SelectedIndex > 0)
                        sessie.Status_Locatie = status_LocatieComboBox.Text;
                    sessie.Status_Datum = status_DatumTextBox.Text;
                    sessie.KMO_Port_Uren = kMO_Port_UrenTextBox.Text;
                    if (terplaatseComboBox.SelectedIndex > -1)
                        sessie.Verantwoordelijke_Terplaatse_ID = ((data.Gebruiker)terplaatseComboBox.SelectedItem).ID;
                    if (sessieVerantwoordelijkeComboBox.SelectedIndex > -1)
                        sessie.Verantwoordelijke_Sessie_ID = ((data.Gebruiker)sessieVerantwoordelijkeComboBox.SelectedItem).ID;
                    else
                        sessie.Verantwoordelijke_Sessie_ID = currentSeminarie.Verantwoordelijke_ID;
                    //if (beamerComboBox.SelectedIndex > -1)
                    //    sessie.Beamer = beamerComboBox.Text;
                    //if (laptopTextBox.SelectedIndex > -1)
                    //    sessie.Laptop = laptopTextBox.Text;
                    sessie.Opmerking = opmerkingTextBox.Text;
                    //sessie.Erkenningsnummer = sessieErkenningsnummerTextBox.Text;
                    sessie.Actief = actiefCheckBox2.IsChecked == true;
                    return sessie;

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nGetSessieFromForm module.", ConfocusMessageBox.Kleuren.Rood);
                    return null;
                }
            }
            else
                return null;
        }

        private int[] GetTijdFromText(string uur)
        {
            int[] arr = new int[2] { 23, 59 };
            if (!string.IsNullOrEmpty(uur))
            {
                try
                {
                    string justTime = uur.Substring(0, 5);
                    char separator = ':';
                    if (justTime.IndexOf('.') > -1)
                        separator = '.';
                    var timeparts = justTime.Split(separator);
                    int hour; //= 23;
                    int minute; //= 59;
                    if (int.TryParse(timeparts[0], out hour))
                        arr[0] = hour;
                    if (int.TryParse(timeparts[1], out minute))
                        arr[1] = minute;
                }
                catch (Exception)
                {
                    
                }
            }

            return arr;
        }

        private bool IsSessieValid()
        {
            bool valid = true;
            string errortext = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (currentSeminarie == null)
            {
                errortext += "Kies eerst een seminarie op het seminarie tabblad.";
                valid = false;
            }

            if (naamTextBox.Text == "")
            {
                errortext += "Geen naam ingegeven voor de sessie!";
                valid = false;
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

            if (verborgenDatumDatePicker.SelectedDate == null)
            {
                if (datumDatePicker.SelectedDate != null)
                    verborgenDatumDatePicker.SelectedDate = datumDatePicker.SelectedDate;
                else
                {
                    errortext += "\nGeen verborgen datum gekozen!";
                    valid = false;
                }
            }

            if (locatieComboBox1.SelectedIndex < 0)
            {
                errortext += "\nGeen locatie gekozen!";
                valid = false;
            }

            if (currentSeminarie.Type != "Webinar")
            {
                if (String.IsNullOrEmpty(beginuurTextBox.Text))
                {
                    errortext += "\nBeginuur is niet ingevuld!";
                    valid = false;
                    Helper.SetTextBoxInError(beginuurTextBox, errorTemplate);
                }
                else
                    Helper.SetTextBoxToNormal(beginuurTextBox, normalTemplate);

                if (String.IsNullOrEmpty(einduurTextBox.Text))
                {
                    errortext += "\nEinduur is niet ingevuld!";
                    valid = false;
                    Helper.SetTextBoxInError(einduurTextBox, errorTemplate);
                }
                else
                    Helper.SetTextBoxToNormal(einduurTextBox, normalTemplate);
            }

            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }

        private void notitie_factuurTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void NieuweSessieButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsChanged)
            {
                if (MessageBox.Show("U heeft nog onopgeslagen wijzigingen gemaakt. Niet opgeslagen wijzigingen gaan dan verloren.\nWil u toch doorgaan?", "Opgelet", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    ClearSessie();
                }
            }
            else
                ClearSessie();
        }

        private void ClearSessie()
        {
            currentSessie = null;
            naamTextBox.Text = "";
            locatieComboBox1.SelectedIndex = -1;
            datumDatePicker.SelectedDate = null;
            verborgenDatumDatePicker.SelectedDate = null;
            beginuurTextBox.Text = "";
            einduurTextBox.Text = "";
            statusComboBox.SelectedIndex = -1;
            status_infoTextBox.Text = "";
            //zaalTextBox.Text = "";
            uRLTextBox.Text = "www.confocus.be/";
            standaard_prijsTextBox.Text = "";
            prijs_Non_ProfitTextBox.Text = "";
            prijs_Profit_PriveTextBox.Text = "";
            kortingsprijsTextBox.Text = "";
            targetTextBox.Text = "";
            //erkenningComboBox.SelectedIndex = -1;
            //opstelling_zaalTextBox.Text = "";
            catering_LunchTextBox.Text = "";
            status_LocatieComboBox.SelectedIndex = -1;
            status_DatumTextBox.Text = "";
            kMO_Port_UrenTextBox.Text = "";
            if (currentSeminarie != null)
            {
                if (currentSeminarie.Gebruiker != null)
                {
                    terplaatseComboBox.Text = currentSeminarie.Gebruiker.Naam;
                    sessieVerantwoordelijkeComboBox.Text = currentSeminarie.Gebruiker.Naam;
                }
            }
            //beamerComboBox.SelectedIndex = -1;
            //laptopTextBox.SelectedIndex = -1;
            opmerkingTextBox.Text = "";
            //sessieErkenningsnummerTextBox.Text = "";
            actiefCheckBox2.IsChecked = true;
            sessieInschrijvingenTotaalLabel.Content = "0";
            sessieGastenLabel.Content = "0";
            sessieInschrijvingenLabel.Content = "0";

            IsChanged = false;
        }

        private void SessieOpslaanButton_Click(object sender, RoutedEventArgs e)
        {
            if (currentSessie != null)
            {
                SessieBeforeSave = currentSessie;
                //indexBeforeSave = sessieindex;
                SaveSessieChanges(SessieBeforeSave.ID);
                currentSessie = SessieBeforeSave;
                int index = 0;
                foreach (data.Sessie sessie in sessieDataGrid.Items)
                {
                    if (sessie.ID == currentSessie.ID)
                        break;
                    index++;
                }
                //sessieindex = indexBeforeSave;
                sessieDataGrid.SelectedIndex = index;
            }
            else
            {
                SaveNewSessie();
            }
            sessieDataGrid.IsEnabled = true;
            IsChanged = false;
        }

        private void SaveSessieChanges(decimal id)
        {
            data.Sessie mySessie = GetSessieFromForm();
            if (mySessie != null)
            {
                try
                {
                    mySessie.ID = id;
                    mySessie.Modified = currentSessie.Modified;
                    mySessie.Gewijzigd = DateTime.Now;
                    mySessie.Gewijzigd_door = currentUser.Naam;
                    data.SessieServices sService = new data.SessieServices();
                    data.Sessie savedSessie = sService.SaveSessieChanges(mySessie);
                    if (savedSessie != null)
                    {
                        currentSessie = savedSessie;
                        LoadSessionsAsync();
                        BindSessie(savedSessie);
                        IsChanged = false;
                        sessieDataGrid.SelectedIndex = sessieindex;
                        ConfocusMessageBox.Show("Sessie opgeslagen", ConfocusMessageBox.Kleuren.Blauw);
                    }
                    else
                        ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nSaveSessieChanges module.", ConfocusMessageBox.Kleuren.Rood);

                }
            }
        }

        private async void LoadSessionsAsync()
        {
            if (currentSeminarie != null)
            {
                try
                {
                    IsInit = true;
                    data.SessieServices sService = new data.SessieServices();
                    List<data.Sessie> sessies = await Task.Run(() => sService.GetSessiesBySeminarieID(currentSeminarie.ID));

                    IsChanged = false;
                    sessieViewSource.Source = sessies;
                    sessieDataGrid.SelectedIndex = sessieindex;
                    data.Sessie kies = new data.Sessie();
                    List<data.Sessie> combosessies = new List<data.Sessie>();
                    combosessies.AddRange(sessies);
                    kies.Naam = "-- Kies --";
                    combosessies.Insert(0, kies);

                    sprekerSessieComboBox.ItemsSource = combosessies;
                    sprekerSessieComboBox.DisplayMemberPath = "NaamDatumLocatie";
                    erkenningSessieComboBox.ItemsSource = combosessies;
                    erkenningSessieComboBox.DisplayMemberPath = "NaamDatumLocatie";
                    //GetInschrijvingen(sessies);

                    SeminarieSessieTotaalLabel.Content = sessies.Count;
                    IsInit = false;
                    sessieDataGrid.IsEnabled = true;

                }
                catch (Exception ex)
                {

                    ConfocusMessageBox.Show(ex.Message + "\nLoadSessions module.", ConfocusMessageBox.Kleuren.Rood);
                }
            }

        }

        private void GetInschrijvingen(List<data.Sessie> sessies)
        {
            try
            {
                data.InschrijvingenService iService = new data.InschrijvingenService();
                List<data.Inschrijvingen> inschrijvingen = iService.GetInschrijvingBySeminarieID(currentSeminarie.ID);
                HaalInschrijvingenOp();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message + "\nGetInschrijvingen module.", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SaveNewSessie()
        {
            data.Sessie mySessie = GetSessieFromForm();
            if (mySessie != null)
            {
                try
                {
                    mySessie.Aangemaakt = DateTime.Now;
                    mySessie.Aangemaakt_door = currentUser.Naam;
                    data.SessieServices sService = new data.SessieServices();
                    data.Sessie savedSessie = sService.SaveSessie(mySessie);
                    if (savedSessie != null)
                    {
                        currentSessie = savedSessie;
                        LoadSessionsAsync();

                        sessieDataGrid.SelectedItem = savedSessie;
                        //BindSessie(savedSessie);
                        IsChanged = false;
                        ConfocusMessageBox.Show("Sessie opgeslagen", ConfocusMessageBox.Kleuren.Blauw);

                    }
                    else
                        ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }

            }
        }

        private void sessieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsChanged)
            {
                if (!IsInit)
                {
                    e.Handled = true;
                    ConfocusMessageBox.Show(data.ErrorMessages.GewijzigdeDataOpen, ConfocusMessageBox.Kleuren.Rood);
                    IsInit = true;
                    sessieDataGrid.SelectedIndex = sessieindex;
                    IsInit = false;
                    return;
                }
            }
            else
            {
                //DisableSessieSaveControls();                
                data.Sessie mySessie = (data.Sessie)sessieDataGrid.SelectedItem;
                SetSelectedSessie(mySessie);
            }
        }

        private async void SetSelectedSessie(data.Sessie sessie)
        {
            if (sessie != null)
            {
                DisableSessieSaveControls();
                currentSessie = sessie;
                sessieindex = sessieDataGrid.SelectedIndex;
                await Task.Run(() => BindSessieAsync(sessie));
                LoadInschrijvingen();
                ReloadSprekersAsync();
                IsInit = true;
                sessieDataGrid.SelectedIndex = sessieindex;
                //sessieDataGrid.IsEnabled = true;
                IsInit = false;
            }
            IsChanged = false;
            //await Task.Run(() => EnableButtonStatus());
        }

        private async void RemoveAantallebAsync()
        {
            int result = await Task.Run(() => 20 * 5);
            sessieSprekersTotaalLabel.Content = "";
        }

        private void DisableButtonStatus()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SessieOpslaanButton.SetValue(Grid.IsEnabledProperty, false); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { NieuweSessieButton.SetValue(Grid.IsEnabledProperty, false); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SessieVolgendeButton.SetValue(Grid.IsEnabledProperty, false); }, null);
        }

        private void EnableButtonStatus()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SessieOpslaanButton.SetValue(Grid.IsEnabledProperty, true); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { NieuweSessieButton.SetValue(Grid.IsEnabledProperty, true); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SessieVolgendeButton.SetValue(Grid.IsEnabledProperty, true); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieDataGrid.SetValue(DataGrid.IsEnabledProperty, true); }, null);
        }

        private void LoadInschrijvingen()
        {
            try
            {
                data.InschrijvingenService iService = new data.InschrijvingenService();
                List<data.Inschrijvingen> inschrijvingen = iService.GetInschrijvingBySessieID(currentSessie.ID);

                BindInschrijvingen(inschrijvingen);

                inschrijvingTotaalLabel.Content = currentSessie.AantalEchteInschrijvingen;//.AantalInschrijvingen;
                int allinschrijvingen = (from i in inschrijvingen where i.Status == "Inschrijving" || i.Status == "Gast" select i).Count();
                sessieInschrijvingenTotaalLabel.Content = allinschrijvingen;
                //SeminarieSessieTotaalLabel.Content = sessieDataGrid.Items.Count;
                //data.SessieServices sService = new data.SessieServices();
                //int inschrijvingaantal = sService.GetAantalInschrijvingenPerSeminarie(currentSeminarie.ID);
                //SeminarieInschrijvingenTotaalLabel.Content = inschrijvingaantal;
                //sessieInschrijvingenTotaalLabel.Content = currentSessie.AantalInschrijvingen;
                SetSeminarieInfo(currentSeminarie);
                sessieGastenLabel.Content = currentSessie.AantalGasten == "" ? "0" : currentSessie.AantalGasten;
                sessieInschrijvingenLabel.Content = currentSessie.AantalEchteInschrijvingen == "" ? "0" : currentSessie.AantalEchteInschrijvingen;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message + "\nLoadInschrijvingen module", ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void SessieSeminarieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearSessies();
        }

        private void SessieVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            InschrijvingenTab.IsSelected = true;

        }

        private void DisableSessieSaveControls()
        {
            SessieOpslaanButton.IsEnabled = false;
            NieuweSessieButton.IsEnabled = false;
            SessieVolgendeButton.IsEnabled = false;
        }

        private void EnableSessieSaveControls()
        {
            SessieOpslaanButton.IsEnabled = true;
            NieuweSessieButton.IsEnabled = true;
            SessieVolgendeButton.IsEnabled = true;
        }
        #endregion
    }
}

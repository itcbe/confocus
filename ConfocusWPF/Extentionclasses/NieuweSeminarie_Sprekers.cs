﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ConfocusWPF
{
    public partial class NieuweSeminarie
    {
        #region Sprekers tab

        private void sprekerZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekSprekers();
        }

        private void ZoekSprekers()
        {
            ClearSessieSpreker();
            String zoektext = sprekerZoekTextBox.Text;
            SprekerServices sService = new SprekerServices();
            var sprekers = sService.GetContactName(zoektext);
            sprekerComboBox.ItemsSource = sprekers;
            sprekerComboBox.DisplayMemberPath = "SprekerNaamEnTitel";
            saveSprekerButton.IsEnabled = false;
            toevoegButton.IsEnabled = true;
        }

        private void toevoegButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateSpreker())
            {
                Sessie_Spreker spreker = GetSprekerFromForm();
                Spreker laatstGekozenSpreker = (Spreker)sprekerComboBox.SelectedItem;
                if (spreker != null)
                {
                    spreker.AttestVerstuurd = false;
                    SaveSpreker(spreker);
                    ClearSessieSpreker();
                }
                ReloadSprekersAsync();
                int index = 0;
                foreach (Spreker sprek in sprekerComboBox.Items)
                {
                    if (sprek.ID == laatstGekozenSpreker.ID)
                    {
                        break;
                    }
                    index++;
                }
                sprekerComboBox.SelectedIndex = index;
                //sprekerAttesttypeLabel.Content = spreker.Attesttype;
                UrenTextBox.Text = "";
                sprekerZoekTextBox.Text = "";
                toevoegButton.IsEnabled = false;
                if (currentSessie != null)
                    new SessieServices().UpdateSessieTimeStamp(currentSessie.ID, currentUser);
            }
        }

        private void saveSprekerButton_Click(object sender, RoutedEventArgs e)
        {
            if (sessie_SprekerDataGrid.SelectedIndex > -1)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)sessie_SprekerDataGrid.SelectedItem;
                if (spreker.Opmerking != sprekerOpmerkingTextBox.Text)
                    spreker.Opmerking = sprekerOpmerkingTextBox.Text;
                //if (sprekerAttesttypeComboBox.SelectedIndex > -1)
                //{
                //    AttestType type = (AttestType)sprekerAttesttypeComboBox.SelectedItem;
                //    spreker.Attesttype = type.Naam;
                //}
                if (sprekerComboBox.SelectedIndex > -1)
                {
                    Spreker sprek = (Spreker)sprekerComboBox.SelectedItem;
                    spreker.Spreker_ID = sprek.ID;
                }
                if (sprekerSessieComboBox.SelectedIndex > -1)
                {
                    Sessie ses = (Sessie)sprekerSessieComboBox.SelectedItem;
                    spreker.Sessie_ID = ses.ID;
                }
                spreker.Uren = UrenTextBox.Text;
                spreker.AttestVerstuurd = AttestSprekerVerstuurdComboBox.Text == "Ja" ? true : false;
                SessieSprekerServices sService = new SessieSprekerServices();
                try
                {
                    sService.Update(spreker);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                ReloadSprekersAsync();
                int index = 0;
                foreach (Spreker sprek in sprekerComboBox.Items)
                {
                    if (sprek.ID == spreker.ID)
                    {
                        break;
                    }
                    index++;
                }
                sprekerComboBox.SelectedIndex = index;
                sprekerZoekTextBox.Text = "";
                if (currentSessie != null)
                    new SessieServices().UpdateSessieTimeStamp(currentSessie.ID, currentUser);
            }
        }


        private void ReloadSprekers()
        {
            SessieSprekerServices sService = new SessieSprekerServices();
            List<Sessie_Spreker> sprekers = sService.GetSprekersBySeminarieID(currentSeminarie.ID);
            sessie_SprekerDataGrid.CancelEdit(DataGridEditingUnit.Row);
            sessie_SprekerViewSource.GroupDescriptions.Clear();
            sessie_SprekerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("Sessie.NaamDatumLocatie"));
            sessie_SprekerViewSource.Source = sprekers;
                
            int sessieSprekersAantal = sService.GetAantalSprekersFromSessie(currentSessie.ID);
            totaalSprekerLabel.Content = "Totaal sprekers: " + sprekers.Count;
            deelnemerSprekerTotaalLabel.Content = sessieSprekersAantal.ToString();
            sessieSprekersTotaalLabel.Content = sessieSprekersAantal.ToString();
            BindSelectedSessieSpreker();
            toevoegButton.IsEnabled = false;
            saveSprekerButton.IsEnabled = true;
        }

        private async void ReloadSprekersAsync()
        {
            SessieSprekerServices sService = new SessieSprekerServices();
            List<Sessie_Spreker> sprekers = await Task.Run(() => sService.GetSprekersBySeminarieID(currentSeminarie.ID));

            sessie_SprekerDataGrid.CancelEdit(DataGridEditingUnit.Row);
            sessie_SprekerViewSource.GroupDescriptions.Clear();
            sessie_SprekerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("Sessie.NaamDatumLocatie"));
            sessie_SprekerViewSource.Source = sprekers;

            int sessieSprekersAantal = sService.GetAantalSprekersFromSessie(currentSessie.ID);
            totaalSprekerLabel.Content = "Totaal sprekers: " + sprekers.Count;
            deelnemerSprekerTotaalLabel.Content = sessieSprekersAantal.ToString();
            sessieSprekersTotaalLabel.Content = sessieSprekersAantal.ToString();
            BindSelectedSessieSpreker();
            toevoegButton.IsEnabled = false;
            saveSprekerButton.IsEnabled = true;

            await Task.Run(() => EnableButtonStatus());
        }

        private void sprekerComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Spreker spreker = (Spreker)box.SelectedItem;
                if (spreker.AttestType_ID != null)
                {
                    Attesttype_Services aService = new Attesttype_Services();
                    AttestType attest = aService.GetByID((decimal)spreker.AttestType_ID);
                    //sprekerAttesttypeLabel.Content = attest.Naam;
                }
                //else
                //    sprekerAttesttypeLabel.Content = "Geen";
            }
        }

        private void SaveSpreker(Sessie_Spreker spreker)
        {
            try
            {
                SessieSprekerServices sService = new SessieSprekerServices();
                sService.SaveSpreker(spreker);
            }
            catch (Exception ex)
            {
                if (ex.Message == ErrorMessages.SprekerReedsToegevoegd)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Sessie_Spreker GetSprekerFromForm()
        {
            try
            {
                Sessie_Spreker spreker = new Sessie_Spreker();
                if (sprekerSessieComboBox.SelectedIndex > -1)
                {
                    Sessie thisSessie = (Sessie)sprekerSessieComboBox.SelectedItem;
                    spreker.Sessie_ID = thisSessie.ID;
                }
                else
                    spreker.Sessie_ID = currentSessie.ID;
                spreker.Spreker_ID = ((Spreker)sprekerComboBox.SelectedItem).ID;
                //spreker.Status = sprekerStatusComboBox.Text;
                spreker.Opmerking = sprekerOpmerkingTextBox.Text;
                //spreker.Attesttype = sprekerAttesttypeLabel.Content == "Geen" ? "" : sprekerAttesttypeLabel.Content;
                spreker.Uren = UrenTextBox.Text;
                spreker.AttestVerstuurd = AttestSprekerVerstuurdComboBox.Text == "Ja" ? true : false;
                return spreker;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool ValidateSpreker()
        {
            bool valid = true;
            String errortext = "";
            if (!(sprekerComboBox.SelectedItem is Spreker))
            {
                valid = false;
                errortext += "- Er is geen spreker gekozen om toe te voegen!";
            }
            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void sessie_SprekerDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BindSelectedSessieSpreker();
            saveSprekerButton.IsEnabled = true;
        }

        private void BindSelectedSessieSpreker()
        {
            if (sessie_SprekerDataGrid.SelectedIndex > -1)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)sessie_SprekerDataGrid.SelectedItem;
                SessieSprekerServices sService = new SessieSprekerServices();
                Sessie_Spreker fullSpreker = sService.GetById(spreker.ID);
                SprekerServices ssService = new SprekerServices();
                List<Spreker> sprekers = ssService.FindActive();
                //sprekers.Add(fullSpreker.Spreker);
                sprekerComboBox.ItemsSource = sprekers;
                sprekerComboBox.DisplayMemberPath = "SprekerNaamEnTitel";
                foreach (Spreker spr in sprekerComboBox.Items)
                {
                    if (spr.ID == fullSpreker.Spreker_ID)
                    {
                        sprekerComboBox.SelectedItem = spr;
                        break;
                    }
                }
                AttestSprekerVerstuurdComboBox.Text = fullSpreker.StrAttestVerstuurd;
                //sprekerAttesttypeLabel.Content = fullSpreker.Attesttype == "" ? "Geen" : fullSpreker.Attesttype;
                UrenTextBox.Text = fullSpreker.Uren == null ? "" : fullSpreker.Uren;
                foreach (Sessie sessie in sprekerSessieComboBox.Items)
                {
                    if (sessie.ID == fullSpreker.Sessie_ID)
                    {
                        sprekerSessieComboBox.SelectedItem = sessie;
                        break;
                    }
                }
                //sprekerSessieComboBox.Text = fullSpreker.Sessie.NaamDatumLocatie;
                //sprekerStatusComboBox.Text = fullSpreker.Status;
                sprekerOpmerkingTextBox.Text = fullSpreker.Opmerking;
            }
            else
                ClearSessieSpreker();
        }

        private void ClearSessieSpreker()
        {
            sprekerComboBox.ItemsSource = new List<Spreker>();
            sprekerSessieComboBox.SelectedIndex = -1;
            //sprekerAttesttypeLabel.Content = "";
            sprekerOpmerkingTextBox.Text = "";
            UrenTextBox.Text = "";
            AttestSprekerVerstuurdComboBox.SelectedIndex = 1;
        }

        #endregion
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ConfocusWPF
{
    public partial class NieuweSeminarie
    {
        private Seminarie_Doelgroep currentDoelgroep;
        private void SaveDoelgroep(Seminarie_Doelgroep doelgroep)
        {
            try
            {
                Seminarie_DoelgroepServices dsService = new Seminarie_DoelgroepServices();
                doelgroep.Aangemaakt = DateTime.Now;
                doelgroep.Aangemaakt_door = currentUser.Naam;
                currentDoelgroep = dsService.SaveSeminarieDoelgroep(doelgroep);
                SetSeminarieInfo(currentSeminarie);
                if (currentSeminarie != null)
                    new SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void doelgroepToevoegenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Seminarie_Doelgroep doelgroep = GetDoelgroepFromForm();
                SaveDoelgroep(doelgroep);
                ReloadDoelgroepenAsync();
                if (currentSessie != null)
                    new SessieServices().UpdateSessieTimeStamp(currentSessie.ID, currentUser);

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private async void ReloadDoelgroepenAsync()
        {
            if (currentSeminarie != null)
            {
                Seminarie_DoelgroepServices sdService = new Seminarie_DoelgroepServices();
                List<Seminarie_Doelgroep> doelgroepen = await Task.Run(() => sdService.GetDoelgroepenBySeminarieID(currentSeminarie.ID));
                seminarie_DoelgroepViewSource.Source = doelgroepen;
                if (doelgroepen.Contains(currentDoelgroep))
                {
                    seminarie_DoelgroepDataGrid.SelectedItem = currentDoelgroep;
                }
            }
        }

        private Seminarie_Doelgroep GetDoelgroepFromForm()
        {
            bool valid = true;
            String errormessage = "";

            Seminarie_Doelgroep doelgroep = new Seminarie_Doelgroep();
            if (currentSeminarie == null)
            {
                valid = false;
                errormessage += "Er is geen seminarie aangemaakt of gekozen!\n";
            }
            else
                doelgroep.Seminarie_ID = currentSeminarie.ID;
            if (Niveau1ComboBox.SelectedIndex > -1)
                doelgroep.Niveau1_ID = ((Niveau1)Niveau1ComboBox.SelectedItem).ID;
            else
            {
                valid = false;
                errormessage += "Niveau 1 is verplicht in te vullen!";
            }
            if (Niveau2ComboBox.SelectedIndex > 0)
                doelgroep.Niveau2_ID = ((Niveau2)Niveau2ComboBox.SelectedItem).ID;
            if (Niveau3ComboBox.SelectedIndex > 0)
                doelgroep.Niveau3_ID = ((Niveau3)Niveau3ComboBox.SelectedItem).ID;
            doelgroep.Opmerking = DoelgroepOpmerkingTextBox.Text;
            doelgroep.Actief = doelgroepActiefCheckBox.IsChecked == true;
            if (!valid)
                throw new Exception(errormessage);
            return doelgroep;
        }

        private void seminarie_DoelgroepDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seminarie_DoelgroepDataGrid.SelectedIndex > -1)
            {
                Seminarie_Doelgroep doelgroep = (Seminarie_Doelgroep)seminarie_DoelgroepDataGrid.SelectedItem;
                Niveau1ComboBox.Text = doelgroep.Niveau1.Naam;
                if (doelgroep.Niveau2 != null)
                    Niveau2ComboBox.Text = doelgroep.Niveau2.Naam;
                if (doelgroep.Niveau3 != null)
                    Niveau3ComboBox.Text = doelgroep.Niveau3.Naam;
                DoelgroepOpmerkingTextBox.Text = doelgroep.Opmerking == null ? "" : doelgroep.Opmerking;
                doelgroepActiefCheckBox.IsChecked = doelgroep.Actief;
            }
        }

        private void doelgroepWijzigButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Seminarie_Doelgroep current = (Seminarie_Doelgroep)seminarie_DoelgroepDataGrid.SelectedItem;
                Seminarie_Doelgroep doelgroep = GetDoelgroepFromForm();
                doelgroep.ID = current.ID;
                doelgroep.Modified = current.Modified;
                doelgroep.Gewijzigd = DateTime.Now;
                doelgroep.Gewijzigd_door = currentUser.Naam;
                ChangeDoelgroep(doelgroep);
                if (currentSeminarie != null)
                    new SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser);

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ChangeDoelgroep(Seminarie_Doelgroep doelgroep)
        {
            try
            {
                Seminarie_DoelgroepServices dsService = new Seminarie_DoelgroepServices();
                dsService.SaveSeminarieDoelgroepChanges(doelgroep);
                int index = seminarie_DoelgroepDataGrid.SelectedIndex;
                ReloadDoelgroepenAsync();
                seminarie_DoelgroepDataGrid.SelectedIndex = index;
                SetSeminarieInfo(currentSeminarie);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void verwijderDoelgroepButton_Click(object sender, RoutedEventArgs e)
        {
            Seminarie_Doelgroep doelgroep = (Seminarie_Doelgroep)seminarie_DoelgroepDataGrid.SelectedItem;
            if (MessageBox.Show("Wil U het gekozen doelgroep verwijderen uit het seminarie?", "Verwijderen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Seminarie_DoelgroepServices sdService = new Seminarie_DoelgroepServices();
                try
                {
                    if (sdService.Delete(doelgroep))
                    {
                        ConfocusMessageBox.Show("Doelgroep is verwijderd uit het seminarie.", ConfocusMessageBox.Kleuren.Blauw);
                        ReloadDoelgroepenAsync();
                        SetSeminarieInfo(currentSeminarie);
                        if (currentSeminarie != null)
                            new SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser);
                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Doelgroep kon niet verwijderd worden!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }
    }
}

﻿using data = ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Data;
using System.Collections.ObjectModel;

namespace ConfocusWPF
{
    public partial class NieuweSeminarie
    {
        private void inschrijvingenVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            SprekerTab.IsSelected = true;
        }

        private void inschrijvingenTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                switch (box.Name)
                {
                    case "factuurnummerTextBox":
                        inschrijving.Factuurnummer = box.Text;
                        if (box.Text == "")
                            inschrijving.Gefactureerd = false;
                        else
                            inschrijving.Gefactureerd = true;
                        break;
                    case "notitieTextBox":
                        inschrijving.Notitie = box.Text;
                        break;
                    case "notitie_factuurTextBox":
                        inschrijving.Notitie_factuur = box.Text;
                        break;
                }

                UpdateInschrijving(inschrijving);
            }
        }

        private void inschrijvingenDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsInit = true;
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedIndex > -1)
            {
                data.Inschrijvingen inschrijving = (data.Inschrijvingen)grid.SelectedItem;
                if (inschrijving.Via != "")
                    viaComboBox.Text = inschrijving.Via;
                else
                    viaComboBox.SelectedIndex = -1;

                if (inschrijving.Functie_ID != 0)
                    if (inschrijving.Functies.Attesttype_ID != null)
                    {
                        data.Attesttype_Services aService = new data.Attesttype_Services();
                        data.AttestType type = aService.GetByID((decimal)inschrijving.Functies.Attesttype_ID);
                        attesttypeComboBox.Content = type.Naam;
                    }
                    else
                        attesttypeComboBox.Content = "";
                //if (inschrijving.Attesttype != null && inschrijving.Attesttype != "")
                //    attesttypeComboBox.Text = inschrijving.Attesttype;
                //else
                //    attesttypeComboBox.SelectedIndex = -1;
                if (inschrijving.Status != "")
                    statusComboBox1.Text = inschrijving.Status;
                else
                    statusComboBox1.SelectedIndex = -1;
                if (inschrijving.Dagtype != "")
                    dagtypeComboBox.Text = inschrijving.Dagtype;
                else
                    dagtypeComboBox.SelectedIndex = -1;

                //if (inschrijving.Functies != null)
                //    if (inschrijving.Functies.Bedrijf != null)
                //        facturatie_op_IDComboBox.Text = inschrijving;
                //    else
                //        facturatie_op_IDComboBox.Text = "";
                //else
                //    facturatie_op_IDComboBox.Text = "";
                aantalTextBlock.Text = inschrijving.TAantal;
                datum_inschrijvingDatePicker.SelectedDate = inschrijving.Datum_inschrijving;
                datum_inschriijvingTimeTextbox.Text = ((DateTime)inschrijving.Datum_inschrijving).ToShortTimeString();

                aanwezigComboBox.Text = inschrijving.Aanwezig == true ? "Ja" : "Nee";
                gefactureerdCheckBox.Text = inschrijving.Gefactureerd == true ? "Ja" : "Nee";
                mail_ontvangenCheckBox.Text = inschrijving.Mail_ontvangen == true ? "Ja" : "Nee";
                evaluatie_ontvangenComboBox.Text = inschrijving.Evaluatie_ontvangen == true ? "Ja" : "Nee";
                ReminderComboBox.Text = inschrijving.Reminder_Verstuurd == true ? "Ja" : "Nee";
                AnnulatieComboBox.Text = inschrijving.Annulatie_Verstuurd == true ? "Ja" : "Nee";
                VerplaatsingComboBox.Text = inschrijving.Verplaatsing_Verstuurd == true ? "Ja" : "Nee";
                AttestVerstuurdComboBox.Text = inschrijving.Attest_Verstuurd == true ? "Ja" : "Nee";

                inschrijvingTotaalLabel.Content = GetAantalInschrijvingenBySessie(inschrijving.Sessie_ID);
                deelnemerSprekerTotaalLabel.Content = GetAantalSprekersBySessie(inschrijving.Sessie_ID);
            }
            IsInit = false;
        }

        private object GetAantalSprekersBySessie(decimal? sessie_ID)
        {
            if (sessie_ID != null)
            {
                data.SessieSprekerServices sService = new ConfocusDBLibrary.SessieSprekerServices();
                int aantal = sService.GetAantalSprekersFromSessie((decimal)sessie_ID);
                return aantal.ToString();
            }
            else
                return "0";
        }

        private string GetAantalInschrijvingenBySessie(decimal? sessie_ID)
        {
            if (sessie_ID != null)
            {
                data.InschrijvingenService iService = new ConfocusDBLibrary.InschrijvingenService();
                int aantal = iService.GetAantalEffectieveInschrijvingenFromSessie((decimal)sessie_ID);
                return aantal.ToString();
            }
            else
                return "0";
        }

        private void inschrijvingComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInit)
            {
                ComboBox box = (ComboBox)sender;
                SaveComboBoxChanges(box);
            }
        }

        private void SaveComboBoxChanges(ComboBox box)
        {
            if (inschrijvingenDataGrid.SelectedIndex > -1)
            {
                data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                switch (box.Name)
                {
                    case "attesttypeComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Attesttype = ((data.AttestType)box.SelectedItem).Naam;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "viaComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Via = ((ComboBoxItem)box.SelectedItem).Content.ToString();
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "dagtypeComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Dagtype = ((data.DagType)box.SelectedItem).Naam;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "facturatie_op_IDComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Facturatie_op_ID = ((data.Bedrijf)box.SelectedItem).ID;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "statusComboBox1":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Status = ((ComboBoxItem)box.SelectedItem).Content.ToString();
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "aanwezigComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Aanwezig = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "evaluatie_ontvangenComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Evaluatie_ontvangen = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "gefactureerdCheckBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Gefactureerd = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "mail_ontvangenCheckBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Mail_ontvangen = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "ReminderComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Reminder_Verstuurd = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "AnnulatieComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Annulatie_Verstuurd = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "VerplaatsingComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Verplaatsing_Verstuurd = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "AttestVerstuurdComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            inschrijving.Attest_Verstuurd = ((ComboBoxItem)box.SelectedItem).Content.ToString() == "Ja" ? true : false;
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                }
            }
        }

        private void attesttypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            SaveComboBoxChanges(box);
        }

        protected void UpdateInschrijving(data.Inschrijvingen inschrijving)
        {
            inschrijving.Gewijzigd = DateTime.Now;
            inschrijving.Gewijzigd_door = currentUser.Naam;

            data.InschrijvingenService iService = new data.InschrijvingenService();
            try
            {
                iService.SaveInschrijvingCHanges(inschrijving);
                //int index = inschrijvingenDataGrid.SelectedIndex;
                //if (SessieRadioButton.IsChecked == true)
                //    LoadInschrijvingen();
                //else
                //    ReloadInschijvingen();
                //inschrijvingenDataGrid.SelectedIndex = index;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void inschrijvingTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.LineFeed || e.Key == Key.Tab)
            {
                TextBox box = (TextBox)sender;
                SaveInschrijving(box);
            }
        }

        private void inschrijvingTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            SaveInschrijving(box);
        }

        private void SaveInschrijving(TextBox box)
        {
            data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
            if (inschrijving != null)
            {
                switch (box.Name)
                {

                    case "cC_EmailTextBox":
                        inschrijving.CC_Email = box.Text;
                        UpdateInschrijving(inschrijving);
                        break;
                    case "notitieTextBox":
                        inschrijving.Notitie = box.Text;
                        UpdateInschrijving(inschrijving);
                        break;
                    case "aantalTextBlock":
                        if (!string.IsNullOrEmpty(box.Text))
                        {
                            inschrijving.TAantal = box.Text;
                            UpdateInschrijving(inschrijving);
                        }
                        else
                        {
                            inschrijving.TAantal = "";
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "factuurnummerTextBox":
                        if (box.Text.Length > 49)
                        {
                            ConfocusMessageBox.Show("Het factuurnummer is te lang. Maximum 50 tekens toegelaten", ConfocusMessageBox.Kleuren.Rood);
                        }
                        else
                        {
                            inschrijving.Factuurnummer = box.Text;
                            if (!string.IsNullOrEmpty(box.Text))
                            {
                                inschrijving.Gefactureerd = true;
                                gefactureerdCheckBox.Text = "Ja";
                            }
                            else
                            {
                                inschrijving.Gefactureerd = false;
                                gefactureerdCheckBox.Text = "Nee";
                            }
                            UpdateInschrijving(inschrijving);
                        }
                        break;
                    case "attest_opmakenTextBox":
                        inschrijving.Attest_opmaken = box.Text;
                        UpdateInschrijving(inschrijving);
                        break;
                }
            }
        }

        private void datum_inschrijvingDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInit)
            {

                data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                DatePicker datepicker = (DatePicker)sender;
                DateTime d = (DateTime)datepicker.SelectedDate;
                DateTime olddate = (DateTime)inschrijving.Datum_inschrijving;
                d = d.AddHours(olddate.Hour).AddMinutes(olddate.Minute).AddSeconds(olddate.Second);
                inschrijving.Datum_inschrijving = d;
                UpdateInschrijving(inschrijving);
            }

        }

        private void inschrijvingenCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox check = (CheckBox)sender;
            data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
            switch (check.Name)
            {
                case "aanwezigCheckBox":
                    inschrijving.Aanwezig = check.IsChecked == true;
                    UpdateInschrijving(inschrijving);
                    break;
                case "evaluatie_ontvangenCheckBox":
                    inschrijving.Evaluatie_ontvangen = check.IsChecked == true;
                    UpdateInschrijving(inschrijving);
                    break;
                case "gefactureerdCheckBox":
                    inschrijving.Gefactureerd = check.IsChecked == true;
                    UpdateInschrijving(inschrijving);
                    break;
                case "mail_ontvangenCheckBox":
                    inschrijving.Mail_ontvangen = check.IsChecked == true;
                    UpdateInschrijving(inschrijving);
                    break;
                case "actiefCheckBox3":
                    inschrijving.Actief = check.IsChecked == true;
                    //if (inschrijving.Actief == false)
                    //{
                    //    inschrijving.Sessie_ID = 0;
                    //}
                    UpdateInschrijving(inschrijving);
                    int sessieindex = sessieDataGrid.SelectedIndex;
                    LoadSessionsAsync();
                    BindSessie(currentSessie);
                    IsChanged = false;
                    sessieDataGrid.SelectedIndex = sessieindex;
                    break;
            }
        }

        private void datum_inschrijvingDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            try
            {
                DatePicker date = (DatePicker)sender;
                data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                inschrijving.Datum_inschrijving = date.SelectedDate;

                data.InschrijvingenService iService = new data.InschrijvingenService();
                iService.SaveInschrijvingCHanges(inschrijving);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message + "\nCalendarClosed module.", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void bedrijfComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
            if (inschrijving != null)
            {
                data.InschrijvingenService iService = new data.InschrijvingenService();
                switch (box.Name)
                {
                    case "statusComboBox1":
                        inschrijving.Status = box.Text;
                        try
                        {
                            UpdateInschrijving(inschrijving);
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                        break;
                    case "viaComboBox":
                        inschrijving.Via = box.Text;
                        try
                        {
                            UpdateInschrijving(inschrijving);
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                        break;
                    case "bedrijfComboBox":
                        if (box.SelectedIndex > -1)
                        {
                            //inschrijving.Bedrijf_ID = ((Bedrijf)box.SelectedItem).ID;
                            try
                            {
                                UpdateInschrijving(inschrijving);
                            }
                            catch (Exception ex)
                            {
                                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                        break;
                }
            }
        }

        private void inschrijvingDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsInit = true;
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedIndex > -1)
            {
                try
                {
                    data.Inschrijvingen myInschrijving = (data.Inschrijvingen)grid.SelectedItem;
                    //attesttypeComboBox.Text = myInschrijving.Attesttype;
                    statusComboBox1.Text = myInschrijving.Status;
                    viaComboBox.Text = myInschrijving.Via;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nInschrijvingen selectionChanged module.", ConfocusMessageBox.Kleuren.Rood);
                }
            }
            IsInit = false;
        }

        private void inschrijvingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            data.Inschrijvingen current = (data.Inschrijvingen)grid.SelectedItem;
            Inschrijvingen IW = new Inschrijvingen(currentUser, current.ID);
            if (IW.ShowDialog() == true)
            {
                SeminarieReload();
            }
        }

        private void InschrijvingSessieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox box = (ListBox)sender;

            List<data.Sessie> selectedSessies = new List<data.Sessie>();
            foreach (data.Sessie ses in box.Items)
            {
                if (ses.IsSelected == true)
                    selectedSessies.Add(ses);
            }

            if (selectedSessies.Count > 0)
            {
                try
                {
                    data.InschrijvingenService iService = new data.InschrijvingenService();
                    List<data.Inschrijvingen> inschrijvingen = new List<data.Inschrijvingen>();
                    foreach (data.Sessie mySessie in selectedSessies)
                    {
                        var tempInschrijvingen = iService.GetInschrijvingBySessieID(mySessie.ID);
                        foreach (var item in tempInschrijvingen)
                        {
                            inschrijvingen.Add(item as data.Inschrijvingen);
                        }
                    }
                    inschrijvingenViewSource.Source = inschrijvingen;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nInschrijvingenSessie selectionChanged module.", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void HaalInschrijvingenOp()
        {
            IsInit = true;
            data.SessieServices sService = new data.SessieServices();
            List<data.Sessie> sessies = sService.GetSessiesBySeminarieID(currentSeminarie.ID);
            if (sessies.Count > 0)
            {
                currentSessie = sessies[0];
                data.InschrijvingenService iService = new data.InschrijvingenService();
                ObservableCollection<data.Inschrijvingen> inschrijvingen = new ObservableCollection<data.Inschrijvingen>();
                foreach (data.Sessie mySessie in sessies)
                {
                    var tempInschrijvingen = iService.GetInschrijvingBySessieID(mySessie.ID);
                    foreach (var item in tempInschrijvingen)
                    {
                        inschrijvingen.Add(item as data.Inschrijvingen);
                    }
                }
                try
                {
                    BindInschrijvingen(inschrijvingen);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\nHaalInschrijvingenOp module", ConfocusMessageBox.Kleuren.Rood);
                }
                int aantal = currentSessie.AantalInschrijvingen;
                //inschrijvingTotaalLabel.Content = aantal;
                //SeminarieInschrijvingenTotaalLabel.Content = currentSeminarie.AantalInschrijvingen;
                //sessieInschrijvingenTotaalLabel.Content = aantal;
                SetSeminarieInfo(currentSeminarie);

            }
            else
                inschrijvingenViewSource.Source = new List<data.Inschrijvingen>();
            IsInit = false;
        }

        private void BindInschrijvingen(ObservableCollection<data.Inschrijvingen> lijst)
        {
            inschrijvingenDataGrid.CancelEdit(DataGridEditingUnit.Row);
            inschrijvingenViewSource.GroupDescriptions.Clear();
            //inschrijvingenViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SeminarieNaam"));
            inschrijvingenViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SessieNaam"));
            inschrijvingenViewSource.Source = null;
            inschrijvingenViewSource.Source = lijst;
        }

        private void DeelnemerInschrijvingComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            data.Inschrijvingen inschrijving = (data.Inschrijvingen)combo.SelectedItem;
            if (inschrijving != null)
                SetDeelnemers();
        }

        private void nieuweInschrijvingButton_Click(object sender, RoutedEventArgs e)
        {
            Inschrijvingen IF = new Inschrijvingen(currentUser, null);
            if (IF.ShowDialog() == true)
            {
                SeminarieReload();
            }

        }

        private void InschrijvingenFilter_Click(object sender, RoutedEventArgs e)
        {
            IsInit = true;
            if (currentSeminarie != null)
            {
                try
                {
                    data.InschrijvingenService iService = new data.InschrijvingenService();
                    List<data.Inschrijvingen> inschrijvingen = iService.GetInschrijvingBySeminarieID(currentSeminarie.ID);
                    RadioButton btn = (RadioButton)sender;
                    if (btn.Name == "AllesRadioButton")
                    {
                        var opdatum = (from i in inschrijvingen orderby i.Sessie.Locatie.Naam, i.Sessie.Datum select i).ToList();
                        BindInschrijvingen(opdatum);
                    }
                    else if (btn.Name == "GefactureerdRadioButton")
                    {
                        var gefactureerde = (from i in inschrijvingen where !String.IsNullOrEmpty(i.Factuurnummer) orderby i.Sessie.Locatie.Naam, i.Sessie.Datum select i).ToList();
                        BindInschrijvingen(gefactureerde);
                    }
                    else if (btn.Name == "SessieRadioButton")
                    {
                        var sessieInschrijvingen = (from i in inschrijvingen where i.Sessie_ID == currentSessie.ID orderby i.Sessie.Locatie.Naam, i.Sessie.Datum select i).ToList();
                        BindInschrijvingen(sessieInschrijvingen);
                    }
                    else
                    {
                        var nietgefactureerde = (from i in inschrijvingen where String.IsNullOrEmpty(i.Factuurnummer) && i.Status == "Inschrijving" orderby i.Sessie.Locatie.Naam, i.Sessie.Datum select i).ToList();
                        BindInschrijvingen(nietgefactureerde);
                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het filteren!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            IsInit = false;
        }

        private void BindInschrijvingen(List<data.Inschrijvingen> lijst)
        {
            inschrijvingenDataGrid.CancelEdit(DataGridEditingUnit.Row);
            inschrijvingenViewSource.GroupDescriptions.Clear();
            //inschrijvingenViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SeminarieNaam"));
            inschrijvingenViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SessieNaam"));
            inschrijvingenViewSource.Source = null;
            inschrijvingenViewSource.Source = lijst;
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Input;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for WeergaveInputWindow.xaml
    /// </summary>
    public partial class WeergaveInputWindow : Window
    {
        public String Naam = "";

        public WeergaveInputWindow()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            OkButtonHandler();
            
        }

        private void OkButtonHandler()
        {
            if (NaamTextBox.Text != "")
            {
                this.Naam = NaamTextBox.Text;
                DialogResult = true;
            }
            else
            {
                ConfocusMessageBox.Show("Om op te slaan is de naam verplicht!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void NaamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                OkButtonHandler();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            NaamTextBox.Focus();
        }
    }
}

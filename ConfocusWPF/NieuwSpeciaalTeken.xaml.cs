﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwSpeciaalTeken.xaml
    /// </summary>
    public partial class NieuwSpeciaalTeken : Window
    {
        public NieuwSpeciaalTeken()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            speciaal_karakterTextBox.Text = "";
            alternatiefTextBox.Text = "";
            speciaal_karakterTextBox.Focus();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    Speciaal_teken teken = new Speciaal_teken();
                    teken.Speciaal_karakter = speciaal_karakterTextBox.Text;
                    teken.Alternatief = alternatiefTextBox.Text;
                    new Speciale_Tekens_Services().Add(teken);
                    ClearForm();
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Het teken bestaat reeds in de lijst!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private bool IsValid()
        {
            bool valid = true;
            string errormessage = "";
            if (alternatiefTextBox.Text.Length > 1 || speciaal_karakterTextBox.Text.Length > 1)
            {
                valid = false;
                errormessage += "Er is maar 1 karakter toegelaten!\n";
            }
            if (string.IsNullOrEmpty(alternatiefTextBox.Text) || string.IsNullOrEmpty(speciaal_karakterTextBox.Text))
            {
                valid = false;
                errormessage += "Niet alle velden zijn ingevuld!\n";
            }
            if (!valid)
                ConfocusMessageBox.Show(errormessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void ClearForm()
        {
            speciaal_karakterTextBox.Text = "";
            alternatiefTextBox.Text = "";
        }
    }
}

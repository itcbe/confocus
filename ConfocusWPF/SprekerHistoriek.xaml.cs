﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using ITCLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SprekerHistoriek.xaml
    /// </summary>
    public partial class SprekerHistoriek : Window
    {
        private List<Sessie_Spreker> _sprekers = new List<Sessie_Spreker>();
        public SprekerHistoriek(List<Sessie_Spreker> sprekers)
        {
            InitializeComponent();
            _sprekers = sprekers;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource sessie_SprekerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sessie_SprekerViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            sessie_SprekerViewSource.Source = _sprekers;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.DefaultExt = ".csv";
            savedialog.Filter = "Excel|*.csv";

            //WeergaveInputWindow WI = new WeergaveInputWindow();
            if (savedialog.ShowDialog() == true)               
            {
                string fileName = savedialog.FileName;//@"C:/Confocus Data/" + WI.Naam + ".csv";

                try
                {
                    SessieSprekerServices sService = new SessieSprekerServices();

                    CSVDocument doc = new CSVDocument(fileName);
                    List<string> headers = new List<string>()
                {
                    "Seminarienaam",
                    "Sessienaam",
                    "Datum Sessie",
                    "Locatie Sessie",
                    "Status Sessie",
                    "Voornaam",
                    "Achternaam",
                    "Oorspronkelijke titel",
                    "Bedrijf",
                    "Attesttype",
                };
                    doc.AddRow(headers);
                    foreach (Sessie_Spreker item in sessie_SprekerDataGrid.Items)
                    {
                        Sessie_Spreker spreker = sService.GetSessieSprekerByID(item.ID);
                        List<string> deelnemer = new List<string>();
                        deelnemer.Add(spreker.Sessie.SeminarieTitel);
                        deelnemer.Add(spreker.Sessie.NaamDatumLocatie);
                        deelnemer.Add(spreker.Sessie.Datum.ToString());
                        deelnemer.Add(spreker.Sessie.LocatieNaam);
                        deelnemer.Add(spreker.Sessie.Status);

                        try
                        {
                            deelnemer.Add(spreker.Spreker.Contact.Voornaam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(spreker.Spreker.Contact.Achternaam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(spreker.Spreker.Oorspronkelijke_Titel);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(spreker.Spreker.Bedrijf.Naam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(spreker.SprekerAttesttype == null ? "" : spreker.SprekerAttesttype);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }

                        doc.AddRow(deelnemer);
                    }
                    doc.Save();
                    Helper.OpenUrl(fileName);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }
    }
}

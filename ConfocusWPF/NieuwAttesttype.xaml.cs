﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusDBLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwAttesttype.xaml
    /// </summary>
    public partial class NieuwAttesttype : Window
    {

        private Gebruiker currentUser;
        public NieuwAttesttype(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (naamTextBox.Text != "")
            {
                Attesttype_Services aService = new Attesttype_Services();
                AttestType attest = new AttestType();
                attest.Naam = naamTextBox.Text;
                attest.Actief = true;
                attest.Aangemaakt = DateTime.Now;
                attest.Aangemaakt_door = currentUser.Naam;
                try
                {
                    aService.SaveAttesttype(attest);
                    DialogResult = true;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                
            }
        }
    }
}

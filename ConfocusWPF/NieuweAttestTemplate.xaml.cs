﻿using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweAttestTemplate.xaml
    /// </summary>
    public partial class NieuweAttestTemplate : Window
    {
        private Gebruiker currentUser;
        private bool saveEnabled = false;
        private string _dataFolder = "";

        public NieuweAttestTemplate(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            saveButton.IsEnabled = saveEnabled;
            _dataFolder = new Instelling_Service().Find().DataFolder;
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog FD = new OpenFileDialog();
            FD.InitialDirectory = _dataFolder + "Templates";
            if (FD.ShowDialog() == true)
            {
                uriTextBox.Text = FD.FileName;
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            AttestTemplate template = GetTemplateFromForm();
            if (IsTemplateValid(template))
                SaveTemplate(template);
        }

        private void SaveTemplate(AttestTemplate template)
        {
            try
            {
                AttestTemplate_Service aService = new AttestTemplate_Service();
                aService.Add(template);
                DialogResult = true;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);                
            }
        }

        private bool IsTemplateValid(AttestTemplate template)
        {
            bool valid = true;
            string errortext = "";
            if (template.Naam == "")
            {
                valid = false;
                errortext += "Geen Naame ingegeven!\n";
            }
            if (template.Uri == "")
            {
                valid = false;
                errortext += "Geen uri gekozen!\n";
            }
            if (!valid)
            {
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            }

            return valid;
        }

        private AttestTemplate GetTemplateFromForm()
        {
            AttestTemplate template = new AttestTemplate();
            template.Naam = naamTextBox.Text;
            template.Uri = uriTextBox.Text;
            template.Actief = true;
            template.Aangemaakt = DateTime.Now;
            template.Aangemaakt_door = currentUser.Naam;

            return template;
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (!saveEnabled)
            {
                if (naamTextBox.Text != "" && uriTextBox.Text != "")
                {
                    saveEnabled = true;
                }
            }
            else
            {
                if (naamTextBox.Text == "" || uriTextBox.Text == "")
                {
                    saveEnabled = false;
                }
            }

            saveButton.IsEnabled = saveEnabled;
        }

        private void uriTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!saveEnabled)
            {
                if (naamTextBox.Text != "" && uriTextBox.Text != "")
                {
                    saveEnabled = true;
                }
            }
            else
            {
                if (naamTextBox.Text == "" || uriTextBox.Text == "")
                {
                    saveEnabled = false;
                }
            }

            saveButton.IsEnabled = saveEnabled;
        }
    }
}

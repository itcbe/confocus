﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweInschrijving.xaml
    /// </summary>
    public partial class NieuweInschrijving : Window
    {
        private Gebruiker currentUser;
        public NieuweInschrijving(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            CollectionViewSource inschrijvingViewSource = ((CollectionViewSource)(this.FindResource("inschrijvingViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            BedrijfServices bedrijfService = new BedrijfServices();
            List<Bedrijf> bedrijven = bedrijfService.FindActive();
            bedrijfComboBox.ItemsSource = bedrijven;
            bedrijfComboBox.DisplayMemberPath = "NaamEnBtwNummer";

            SeminarieServices sService = new SeminarieServices();
            List<Seminarie> seminaries = sService.FindActive();
            seminarieComboBox.ItemsSource = seminaries;
            seminarieComboBox.DisplayMemberPath = "Naam";
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            ConfocusDBLibrary.Inschrijvingen inschrijving = GetInschrijvingFromForm();
            if (inschrijving != null)
            {
                InschrijvingenService service = new InschrijvingenService();
                ConfocusDBLibrary.Inschrijvingen savedInscrijving = service.SaveInschrijving(inschrijving);
                if (savedInscrijving != null)
                {
                    ConfocusMessageBox.Show("Inschrijving opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                    DialogResult = true;
                }

                else
                    ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private ConfocusDBLibrary.Inschrijvingen GetInschrijvingFromForm()
        {
            if (IsFormValid())
            {
                ConfocusDBLibrary.Inschrijvingen inschrijving = new ConfocusDBLibrary.Inschrijvingen();
                //inschrijving.Bedrijf_ID = ((Bedrijf)bedrijfComboBox.SelectedItem).ID;
                //inschrijving.Seminarie_ID = ((Seminarie)seminarieComboBox.SelectedItem).ID;
                inschrijving.Datum_inschrijving = datum_inschrijvingDatePicker.SelectedDate;
                inschrijving.Status = statusComboBox.Text;
                inschrijving.Via = viaComboBox.Text;
                inschrijving.Notitie = notitieTextBox.Text;
                inschrijving.Gefactureerd = gefactureerdCheckBox.IsChecked == true;
                inschrijving.Factuurnummer = factuurnummerTextBox.Text;
                inschrijving.Notitie_factuur = notitie_factuurTextBox.Text;
                inschrijving.Actief = actiefCheckBox.IsChecked == true;

                inschrijving.Aangemaakt = DateTime.Now;
                inschrijving.Aangemaakt_door = currentUser.Naam;

                return inschrijving;
            }
            else
            {
                return null;
            }
        }

        private bool IsFormValid()
        {
            bool valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));

            if (bedrijfComboBox.SelectedIndex < 0)
            {
                errorMessage += "Er is geen bedrijf gekozen!\n";
                valid = false;
            }

            if (seminarieComboBox.SelectedIndex < 0)
            {
                errorMessage += "Er is geen seminarie gekozen!\n";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);


            return valid;
        }
    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for BewerkContact.xaml
    /// </summary>
    public partial class BewerkContact : Window
    {
        private Functies Functie = new Functies();
        private Gebruiker currentUser;
        private List<Postcodes> postcodes = new List<Postcodes>();

        public BewerkContact(Gebruiker user, Functies functie)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Bewerk contact: Ingelogd als " + currentUser.Naam;
            this.Functie = functie;
        }

        private void SetData()
        {
            try
            {
                //Contact
                Contact myContact = Functie.Contact;
                if (myContact != null)
                {
                    voornaamTextBox.Text = myContact.Voornaam;
                    achternaamTextBox.Text = myContact.Achternaam;
                    if (myContact.Geboortedatum != null)
                    {
                        geboortedatumDatePicker.Text = ((DateTime)myContact.Geboortedatum).ToShortDateString();
                        geboortedatumDatePicker.SelectedDate = (DateTime)myContact.Geboortedatum;
                        geboortedatumDatePicker.DisplayDate = (DateTime)myContact.Geboortedatum;
                    }
                    aansprekingComboBox.Text = myContact.Aanspreking;
                    TaalServices taalservice = new TaalServices();
                    Taal taal = taalservice.GetTaalByID((decimal)Functie.Contact.Taal_ID);
                    if (taal != null)
                        taalComboBox.Text = taal.Naam;
                    if (Functie.Contact.Secundaire_Taal_ID != null)
                    {
                        Taal secundaire = taalservice.GetTaalByID((decimal)Functie.Contact.Secundaire_Taal_ID);
                        if (secundaire != null)
                            secundaireTaalComboBox.Text = secundaire.Naam;
                    }
                    if (Functie.Contact.Erkenning_ID != null)
                    {
                        ErkenningServices eService = new ErkenningServices();
                        Erkenning erkenning = eService.GetErkenningByID(Functie.Contact.Erkenning_ID);
                        if (erkenning != null)
                            contactErkenningComboBox.Text = erkenning.Naam;
                    }
                    typeComboBox.Text = myContact.Type;
                    notitiesTextBox.Text = myContact.Notities;
                    actiefCheckBox.IsChecked = myContact.Actief;
                }

                //Bedrijf
                Bedrijf myBedrijf = Functie.Bedrijf;
                if (myBedrijf != null)
                {
                    idLabel.Content = myBedrijf.ID;
                    naamTextBox.Text = myBedrijf.Naam;
                    ondernemersnummerTextBox.Text = myBedrijf.Ondernemersnummer;
                    adresTextBox.Text = myBedrijf.Adres;
                    postcodeComboBox.Text = myBedrijf.Postcode;
                    plaatsComboBox.Text = myBedrijf.Plaats;
                    provincieComboBox.Text = myBedrijf.Provincie;
                    landTextBox.Text = myBedrijf.Land;
                    telefoonTextBox.Text = myBedrijf.Telefoon;
                    faxTextBox.Text = myBedrijf.Fax;
                    websiteTextBox.Text = myBedrijf.Website;
                    emailTextBox.Text = myBedrijf.Email;
                    notitiesTextBox1.Text = myBedrijf.Notities;
                    actiefCheckBox1.IsChecked = myBedrijf.Actief;
                }

                //Functies
                if (Functie != null)
                {
                    if (Functie.Contact != null)
                        functieContactComboBox.Text = Functie.Contact.VolledigeNaam;
                    if (Functie.Bedrijf != null)
                        FunctieBedrijfComboBox.Text = Functie.Bedrijf.Naam;
                    if (Functie.Niveau1 != null)
                        niveau1ComboBox1.Text = Functie.Niveau1.Naam;
                    if (Functie.Niveau2 != null)
                        niveau2ComboBox2.Text = Functie.Niveau2.Naam;
                    if (Functie.Niveau3 != null)
                        niveau3ComboBox3.Text = Functie.Niveau3.Naam;
                    emailTextBox1.Text = Functie.Email;
                    telefoonTextBox1.Text = Functie.Telefoon;
                    mobielTextBox.Text = Functie.Mobiel;
                    if (Functie.Erkenning != null)
                        erkenningTextBox1.Text = Functie.Erkenning.Naam;
                    if (Functie.Startdatum != null)
                    {
                        startdatumDatePicker.DisplayDate = (DateTime)Functie.Startdatum;
                        startdatumDatePicker.SelectedDate = (DateTime)Functie.Startdatum;
                    }
                    if (Functie.Einddatum != null)
                        einddatumDatePicker.DisplayDate = (DateTime)Functie.Einddatum;
                    if (Functie.Einddatum != null)
                        einddatumDatePicker.SelectedDate = (DateTime)Functie.Einddatum;
                    mailCheckBox.IsChecked = Functie.Mail;
                    faxCheckBox.IsChecked = Functie.Fax;
                    actiefCheckBox2.IsChecked = Functie.Actief;
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TaalServices taalService = new TaalServices();
            List<Taal> talen = taalService.FindAll();
            Taal taal = new Taal();
            taal.Naam = "Kies";
            talen.Insert(0, taal);
            taalComboBox.ItemsSource = talen;
            taalComboBox.DisplayMemberPath = "Naam";
            taalComboBox.SelectedIndex = 0;
            secundaireTaalComboBox.ItemsSource = talen;
            secundaireTaalComboBox.DisplayMemberPath = "Naam";
            secundaireTaalComboBox.SelectedIndex = 0;

            LoadBedrijfCombobox();

            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> N1Lijst = n1Service.FindAll();
            niveau1ComboBox1.ItemsSource = N1Lijst;
            niveau1ComboBox1.DisplayMemberPath = "Naam";

            ErkenningServices erkenningService = new ErkenningServices();
            List<Erkenning> erkenningen = erkenningService.FindAll();
            Erkenning er = new Erkenning();
            er.Naam = "Kies";
            erkenningen.Insert(0, er);
            erkenningTextBox1.ItemsSource = erkenningen;
            erkenningTextBox1.DisplayMemberPath = "Naam";
            erkenningTextBox1.SelectedIndex = 0;
            contactErkenningComboBox.ItemsSource = erkenningen;
            contactErkenningComboBox.DisplayMemberPath = "Naam";
            contactErkenningComboBox.SelectedIndex = 0;

            PostcodeService postcodeService = new PostcodeService();
            postcodes = postcodeService.FindAllPostcodes();
            postcodeComboBox.ItemsSource = postcodes;
            postcodeComboBox.DisplayMemberPath = "Postcode";

            new Thread(delegate() { LoadContactenComboBox(); }).Start();

            startdatumDatePicker.Text = DateTime.Today.ToShortDateString();
            //einddatumDatePicker.Text = DateTime.Today.AddDays(1).ToShortDateString();
            geboortedatumDatePicker.Text = DateTime.Today.AddYears(-25).ToShortDateString();

            //SetData();
        }

        private void LoadContactenComboBox()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
            ContactServices contactService = new ContactServices();
            List<Contact> contacten = contactService.FindActive();

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { functieContactComboBox.SetValue(ComboBox.ItemsSourceProperty, contacten); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { functieContactComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "VolledigeNaam"); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SetData(); }, null);
            //functieContactComboBox.ItemsSource = contacten;
            //functieContactComboBox.DisplayMemberPath = "VolledigeNaam";
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
        }

        private void LoadBedrijfCombobox()
        {
            BedrijfServices bedrijfService = new BedrijfServices();
            List<Bedrijf> bedrijven = bedrijfService.FindActive();
            //BedrijvenComboBox.ItemsSource = bedrijven;
            //BedrijvenComboBox.DisplayMemberPath = "Naam";

            FunctieBedrijfComboBox.ItemsSource = bedrijven;
            FunctieBedrijfComboBox.DisplayMemberPath = "Naam";
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Opslaan van het contact.
            Contact contact = GetContactFromForm();
            if (contact != null)
            {
                try
                {
                    ContactServices service = new ContactServices();
                    service.SaveContactChanges(contact);
                    ConfocusMessageBox.Show("Contact opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Contact niet opgeslagen!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Contact GetContactFromForm()
        {
            if (IsContactFormValid())
            {
                Contact contact = new Contact();
                contact.ID = Functie.Contact_ID;
                contact.Voornaam = voornaamTextBox.Text;
                contact.Achternaam = achternaamTextBox.Text;
                contact.Aanspreking = aansprekingComboBox.Text;
                if (taalComboBox.SelectedIndex > 0)
                    contact.Taal_ID = ((Taal)taalComboBox.SelectedItem).ID;
                if (secundaireTaalComboBox.SelectedIndex > 0)
                    contact.Secundaire_Taal_ID = ((Taal)secundaireTaalComboBox.SelectedItem).ID;
                if (contactErkenningComboBox.SelectedIndex > 0)
                    contact.Erkenning_ID = ((Erkenning)contactErkenningComboBox.SelectedItem).ID;
                contact.Notities = notitiesTextBox.Text;
                contact.Geboortedatum = geboortedatumDatePicker.SelectedDate;
                if (typeComboBox.SelectedIndex > -1)
                    contact.Type = typeComboBox.Text;
                contact.Gewijzigd = DateTime.Now;
                contact.Gewijzigd_door = currentUser.Naam;
                contact.Actief = actiefCheckBox.IsChecked;

                return contact;
            }
            else
                return null;
        }

        private bool IsContactFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (voornaamTextBox.Text == "")
            {
                errorMessage += "De voornaam is niet ingevuld!\n";
                Helper.SetTextBoxInError(voornaamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(voornaamTextBox, normalTemplate);
            if (achternaamTextBox.Text == "")
            {
                errorMessage += "De achternaam is niet ingevuld!\n";
                Helper.SetTextBoxInError(achternaamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(achternaamTextBox, normalTemplate);
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void BedrijvenComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (postcodeComboBox.SelectedIndex > -1)
            {
                Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                var gemeentes = (from p in postcodes
                                 where p.Postcode == postcode.Postcode
                                 orderby p.Gemeente
                                 select p).ToList();

                if (gemeentes.Count > 0)
                {
                    plaatsComboBox.ItemsSource = gemeentes;
                    plaatsComboBox.DisplayMemberPath = "Gemeente";
                    plaatsComboBox.SelectedIndex = 0;
                }
                provincieComboBox.Text = postcode.Provincie;
                landTextBox.Text = postcode.Land;

            }
            else
            {
                provincieComboBox.SelectedIndex = -1;
                landTextBox.Text = "";
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Bedrijf bedrijf = GetBedrijfFromForm();
            if (bedrijf != null)
            {
                try
                {
                    BedrijfServices service = new BedrijfServices();
                    service.SaveBedrijfChanges(bedrijf);
                    ConfocusMessageBox.Show("Bedrijf opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Bedrijf niet opgeslagen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                
            }
        }

        private Bedrijf GetBedrijfFromForm()
        {
            if (IsBedrijfFormValid())
            {
                Bedrijf bedrijf = new Bedrijf();
                bedrijf.ID = Functie.Bedrijf_ID;
                bedrijf.Naam = naamTextBox.Text;
                bedrijf.Ondernemersnummer = ondernemersnummerTextBox.Text;
                bedrijf.Adres = adresTextBox.Text;
                if (postcodeComboBox.SelectedIndex > -1)
                    bedrijf.Postcode = postcodeComboBox.Text;
                if (plaatsComboBox.SelectedIndex > -1)
                    bedrijf.Plaats = plaatsComboBox.Text;
                if (provincieComboBox.SelectedIndex > -1)
                    bedrijf.Provincie = provincieComboBox.Text;
                bedrijf.Land = landTextBox.Text;
                bedrijf.Telefoon = telefoonTextBox.Text;
                bedrijf.Fax = faxTextBox.Text;
                bedrijf.Website = websiteTextBox.Text;
                bedrijf.Email = emailTextBox.Text;
                bedrijf.Notities = notitiesTextBox.Text;
                bedrijf.Actief = actiefCheckBox1.IsChecked == true;
                bedrijf.Gewijzigd = DateTime.Now;
                bedrijf.Gewijzigd_door = currentUser.Naam;

                return bedrijf;
            }
            else
                return null;
        }

        private bool IsBedrijfFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "Geen naam ingevuld!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void niveau1ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > -1)
            {
                Decimal N1ID = ((Niveau1)(combo.SelectedItem)).ID;
                Niveau2Services n2Service = new Niveau2Services();
                List<Niveau2> N2Lijst = n2Service.GetSelected(N1ID);
                niveau2ComboBox2.ItemsSource = N2Lijst;
                niveau2ComboBox2.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > -1)
            {
                Decimal N2ID = ((Niveau2)(combo.SelectedItem)).ID;
                Niveau3Services n3Service = new Niveau3Services();
                List<Niveau3> N3Lijst = n3Service.GetSelected(N2ID);
                niveau3ComboBox3.ItemsSource = N3Lijst;
                niveau3ComboBox3.DisplayMemberPath = "Naam";
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Functies functie = GetFunctieFromForm();
            if (functie != null)
            {
                try
                {
                    FunctieServices service = new FunctieServices();
                    service.SaveFunctieWijzigingen(functie);
                    ConfocusMessageBox.Show("Functie opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Functie niet opgeslagen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Functies GetFunctieFromForm()
        {
            if (IsFunctieFormValid())
            {
                Functies functie = new Functies();
                functie.ID = Functie.ID;
                if (functieContactComboBox.SelectedIndex > -1)
                {
                    Contact contact = (Contact)functieContactComboBox.SelectedItem;
                    functie.Contact_ID = contact.ID;
                    //functie.Contact = contact;
                }
                if (FunctieBedrijfComboBox.SelectedIndex > -1)
                {
                    Bedrijf bedrijf = (Bedrijf)FunctieBedrijfComboBox.SelectedItem;
                    functie.Bedrijf_ID = bedrijf.ID;
                    //functie.Bedrijf = bedrijf;
                }
                if (niveau1ComboBox1.SelectedIndex > -1)
                {
                    functie.Niveau1_ID = ((Niveau1)niveau1ComboBox1.SelectedItem).ID;
                    //functie.Niveau1 = ((Niveau1)niveau1ComboBox1.SelectedItem);
                }
                if (niveau2ComboBox2.SelectedIndex > -1)
                {
                    functie.Niveau2_ID = ((Niveau2)niveau2ComboBox2.SelectedItem).ID;
                    //functie.Niveau2 = ((Niveau2)niveau2ComboBox2.SelectedItem);
                }
                if (niveau3ComboBox3.SelectedIndex > -1)
                {
                    functie.Niveau3_ID = ((Niveau3)niveau3ComboBox3.SelectedItem).ID;
                    //functie.Niveau3 = ((Niveau3)niveau3ComboBox3.SelectedItem);
                }
                if (erkenningTextBox1.SelectedIndex > 0)
                    functie.Erkenning_ID = ((Erkenning)erkenningTextBox1.SelectedItem).ID;
                functie.Email = emailTextBox1.Text;
                functie.Telefoon = telefoonTextBox1.Text;
                functie.Mobiel = mobielTextBox.Text;
                functie.Mail = mailCheckBox.IsChecked == true;
                functie.Fax = faxCheckBox.IsChecked == true;
                functie.Startdatum = startdatumDatePicker.SelectedDate;
                if (einddatumDatePicker.SelectedDate != null)
                    functie.Einddatum = einddatumDatePicker.SelectedDate;
                else
                    functie.Einddatum = null;
                functie.Actief = actiefCheckBox2.IsChecked == true;
                functie.Gewijzigd = DateTime.Now;
                functie.Gewijzigd_door = currentUser.Naam;

                return functie;
            }
            else
                return null;
        }

        private bool IsFunctieFormValid()
        {
            Boolean valid = true;
            string errorMessage = "";

            if (functieContactComboBox.Text == "")
            {
                errorMessage += "Geen contact gekozen!\n";
                valid = false;
            }
            if (FunctieBedrijfComboBox.Text == "")
            {
                errorMessage += "Geen bedrijf gekozen!\n";
                valid = false;
            }
            if (niveau1ComboBox1.Text == "")
            {
                errorMessage += "Geen niveau 1 gekozen!\n";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }
    }
}

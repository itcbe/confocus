﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ImportPostcodes.xaml
    /// </summary>
    public partial class ImportPostcodes : Window
    {
        private CollectionViewSource postcodesViewSource;
        private List<Postcodes> postcodes = new List<Postcodes>();
        private List<Postcodes> gevondenPostcodes = new List<Postcodes>();
        private List<Postcodes> gewijzigdePostcodes = new List<Postcodes>();
        private Boolean IsScroll = true, IsButtonClick = false;

        public ImportPostcodes()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            postcodesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("postcodesViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            PostcodeService service = new PostcodeService();
            postcodes = service.FindAllGemeentes();
            postcodesViewSource.Source = postcodes;

            goUpdate();

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();
        }

        private void SaveWijzigingen()
        {
            if (gewijzigdePostcodes.Count > 0)
            {
                PostcodeService service = new PostcodeService();
                foreach (Postcodes p in gewijzigdePostcodes)
                {
                    service.ChangePostcode(p);
                }
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                gewijzigdePostcodes.Clear();
                int index = postcodesDataGrid.SelectedIndex;
                LoadData();
                if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                    ZoekInLijst();
                postcodesDataGrid.SelectedIndex = index;
            }
            else
                ConfocusMessageBox.Show("Geen wijzigingen gevonden.", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            postcodesViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            postcodesViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            postcodesViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            postcodesViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwePostcode NP = new NieuwePostcode();
            if (NP.ShowDialog() == true)
                LoadData();
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void goUpdate()
        {
            if (postcodesDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (postcodesViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (postcodesViewSource.View.CurrentPosition == postcodesDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                postcodesDataGrid.SelectedIndex = 0;
            if (postcodesDataGrid.Items.Count != 0)
                postcodesDataGrid.ScrollIntoView(postcodesDataGrid.SelectedItem);

        }

        private void Control_LostFocus(object sender, RoutedEventArgs e)
        {
            Postcodes postcode = (Postcodes)postcodesDataGrid.SelectedItem;
            postcode.Postcode = postcodeTextBox.Text;
            postcode.Gemeente = gemeenteTextBox.Text;
            postcode.Provincie = provincieTextBox.Text;
            postcode.Land = landTextBox.Text;
            int index = postcodesDataGrid.SelectedIndex;

            SaveAndReload(postcode, index);

        }

        private void SaveAndReload(Postcodes postcode, int index)
        {
            try
            {
                PostcodeService service = new PostcodeService();
                service.ChangePostcode(postcode);
                LoadData();
                if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                    ZoekInLijst();
                postcodesDataGrid.SelectedIndex = index;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }          
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gewijzigdePostcodes.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveWijzigingen();
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
                ZoekInLijst();
        }

        private void ZoekInLijst()
        {
            gevondenPostcodes.Clear();

            gevondenPostcodes = (from p in postcodes where p.Gemeente.ToLower().Contains(ZoekTextBox.Text.ToLower()) || p.Postcode == ZoekTextBox.Text select p).ToList();

            if (gevondenPostcodes.Count > 0)
            {
                postcodesViewSource.Source = gevondenPostcodes;
            }
            else
            {
                postcodesViewSource.Source = postcodes;
            }
        }

        private void GridTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Postcodes postcode = (Postcodes)postcodesDataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "postcodeTextBox":
                        postcode.Postcode = text.Text;
                        break;
                    case "gemeenteTextBox":
                        postcode.Gemeente = text.Text;
                        break;
                    case "provincieTextBox":
                        postcode.Provincie = text.Text;
                        break;
                    case "landTextBox":
                        postcode.Land = text.Text;
                        break;
                }
                int index = postcodesDataGrid.SelectedIndex;
                SaveAndReload(postcode, index);
            }
        }

    }
}

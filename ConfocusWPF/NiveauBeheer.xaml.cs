﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NiveauBeheer.xaml
    /// </summary>
    public partial class NiveauBeheer : Window
    {
        private CollectionViewSource niveau1ViewSource;
        private CollectionViewSource niveau2ViewSource;
        private CollectionViewSource niveau3ViewSource;
        private List<Niveau1> Niveau1lijst = new List<Niveau1>();
        private List<Niveau1> GevondenN1Lijst = new List<Niveau1>();
        private List<Niveau2> Niveau2lijst = new List<Niveau2>();
        private List<Niveau2> GevondenN2Lijst = new List<Niveau2>();
        private List<Niveau3> Niveau3lijst = new List<Niveau3>();
        private List<Niveau3> GevondenN3Lijst = new List<Niveau3>();
        private List<Niveau1> GewijzigdeN1Lijst = new List<Niveau1>();
        private List<Niveau2> GewijzigdeN2Lijst = new List<Niveau2>();
        private List<Niveau3> GewijzigdeN3Lijst = new List<Niveau3>();
        private Gebruiker currentUser;
        private Boolean IsScroll = false, IsButtonClick = false;

        public NiveauBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Niveau beheer : Ingelogd als " + currentUser.Naam;
        }

        private void LoadData()
        {
            Niveau1Services n1Service = new Niveau1Services();
            Niveau1lijst = n1Service.FindAll();
            Niveau2Services n2Service = new Niveau2Services();
            Niveau2lijst = n2Service.FindAll();
            List<Niveau2> niveau2Combo = n2Service.FindAllComboBox();
            Niveau3Services n3Service = new Niveau3Services();
            Niveau3lijst = n3Service.FindAll();
            naamComboBox.ItemsSource = Niveau1lijst;
            naamComboBox.DisplayMemberPath = "Naam";
            naamComboBox1.ItemsSource = niveau2Combo;
            naamComboBox1.DisplayMemberPath = "Niveau1En2Namen";
            niveau1ViewSource.Source = Niveau1lijst;
            niveau2ViewSource.Source = Niveau2lijst;
            niveau3ViewSource.Source = Niveau3lijst;
            goUpdateN1();
            goUpdateN2();
            goUpdateN3();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            niveau1ViewSource = ((CollectionViewSource)(this.FindResource("niveau1ViewSource")));
            niveau2ViewSource = ((CollectionViewSource)(this.FindResource("niveau2ViewSource")));
            niveau3ViewSource = ((CollectionViewSource)(this.FindResource("niveau3ViewSource")));
            LoadData();
        }

        /* ___________________________________
         * |                                  |
         * |    Niveau 1 Functionaliteiten    |
         * |    --------------------------    |
         * |__________________________________|
         */

        private void niveau1CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void firstN1_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau1ViewSource.View.MoveCurrentToFirst();
            goUpdateN1();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void backN1_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau1ViewSource.View.MoveCurrentToPrevious();
            goUpdateN1();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void nextN1_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau1ViewSource.View.MoveCurrentToNext();
            goUpdateN1();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void lastN1_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau1ViewSource.View.MoveCurrentToLast();
            goUpdateN1();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newN1_Click(object sender, RoutedEventArgs e)
        {
            NieuwNiveau1 NN1 = new NieuwNiveau1(currentUser);
            if (NN1.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void saveN1_Click(object sender, RoutedEventArgs e)
        {
            SaveN1Wijzigingen();
        }

        private void SaveN1Wijzigingen()
        {
            Niveau1Services service = new Niveau1Services();
            foreach (Niveau1 n1 in GewijzigdeN1Lijst)
            {
                n1.Gewijzigd = DateTime.Now;
                n1.Gewijzigd_door = currentUser.Naam;
                service.SaveNiveau1Wijzigingen(n1);
            }
            GewijzigdeN1Lijst.Clear();
            ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void searchN1_Click(object sender, RoutedEventArgs e)
        {
            ZoekN1InLijst();
            
        }

        private void ZoekN1TextBox_KeyUp(object sender, KeyEventArgs e)
        {
                ZoekN1InLijst();
        }

        private void ZoekN1InLijst()
        {
            GevondenN1Lijst.Clear();

            GevondenN1Lijst = (from n1 in Niveau1lijst
                               where n1.Naam.ToLower().Contains(ZoekN1TextBox.Text.ToLower())
                               select n1).ToList();

            if (GevondenN1Lijst.Count > 0)
            {
                niveau1ViewSource.Source = GevondenN1Lijst;
            }
            else
            {
                niveau1ViewSource.Source = Niveau1lijst;
            }
            goUpdateN1();
        }

        private void goUpdateN1()
        {
            if (niveau1DataGrid.Items.Count <= 2)
            {
                backN1.IsEnabled = false;
                firstN1.IsEnabled = false;
                nextN1.IsEnabled = false;
                lastN1.IsEnabled = false;
            }
            else if (niveau1ViewSource.View.CurrentPosition == 0)
            {
                backN1.IsEnabled = false;
                firstN1.IsEnabled = false;
                nextN1.IsEnabled = true;
                lastN1.IsEnabled = true;
            }
            else if (niveau1ViewSource.View.CurrentPosition == niveau1DataGrid.Items.Count - 2)
            {
                backN1.IsEnabled = true;
                firstN1.IsEnabled = true;
                nextN1.IsEnabled = false;
                lastN1.IsEnabled = false;
            }
            else
            {
                backN1.IsEnabled = true;
                firstN1.IsEnabled = true;
                nextN1.IsEnabled = true;
                lastN1.IsEnabled = true;
            }
            if (!IsScroll)
                niveau1DataGrid.SelectedIndex = 0;
            if (niveau1DataGrid.Items.Count != 0)
                niveau1DataGrid.ScrollIntoView(niveau1DataGrid.SelectedItem);
        }

        private void N1Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            String imgnaam = "";
            Image img;
            switch (naam)
            {
                case "firstN1":
                    img = firstN1Image;
                    imgnaam = "first";
                    break;
                case "backN1":
                    img = backN1Image;
                    imgnaam = "back";
                    break;
                case "nextN1":
                    imgnaam = "next";
                    img = nextN1Image;
                    break;
                case "lastN1":
                    img = lastN1Image;
                    imgnaam = "last";
                    break;
                //case "searchN1":
                //    img = searchN1Image;
                //    imgnaam = "search";
                //    break;
                case "newN1":
                    img = newN1Image;
                    imgnaam = "new";
                    break;
                //case "saveN1":
                //    img = saveN1Image;
                //    imgnaam = "save";
                //    break;
                default:
                    img = firstN1Image;
                    imgnaam = "first";
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + imgnaam + "_hover.gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + imgnaam + "_hover.gif"));

        }

        private void N1Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            String imgnaam = "";
            switch (naam)
            {
                case "firstN1":
                    img = firstN1Image;
                    imgnaam = "first";
                    break;
                case "backN1":
                    img = backN1Image;
                    imgnaam = "back";
                    break;
                case "nextN1":
                    img = nextN1Image;
                    imgnaam = "next";
                    break;
                case "lastN1":
                    img = lastN1Image;
                    imgnaam = "last";
                    break;
                //case "searchN1":
                //    img = searchN1Image;
                //    imgnaam = "search";
                //    break;
                case "newN1":
                    img = newN1Image;
                    imgnaam = "new";
                    break;
                //case "saveN1":
                //    img = saveN1Image;
                //    imgnaam = "save";
                //    break;
                default:
                    img = firstN1Image;
                    imgnaam = "first";
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + imgnaam + ".gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + imgnaam + ".gif"));
        }

        private void niveau1DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdateN1();
                IsScroll = false;
            }
        }

        private void controlN1_LostFocus(object sender, RoutedEventArgs e)
        {
            if (niveau1DataGrid.SelectedIndex > -1)
            {
                Niveau1 n1 = (Niveau1)niveau1DataGrid.SelectedItem;
                n1.Naam = naamTextBox.Text;
                n1.Naam_FR = beschrijvingTextBox.Text;
                n1.Actief = actiefCheckBox.IsChecked == true;
                n1.Gewijzigd = DateTime.Now;
                n1.Gewijzigd_door = currentUser.Naam;

                Niveau1Services service = new Niveau1Services();
                try
                {
                    service.SaveNiveau1Wijzigingen(n1);
                    int index = niveau1DataGrid.SelectedIndex;
                    LoadData();
                    niveau1DataGrid.SelectedIndex = index;

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void gridN1Textbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Niveau1 N1 = (Niveau1)niveau1DataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "gridN1NaamTextBox":
                        N1.Naam = text.Text;
                        break;
                    case "gridN1BeschrijvingTextBox":
                        N1.Naam_FR = text.Text;
                        break;
                }
                try
                {
                    Niveau1Services service = new Niveau1Services();
                    service.SaveNiveau1Wijzigingen(N1);
                    int index = niveau1DataGrid.SelectedIndex;
                    LoadData();
                    niveau1DataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }



        /*  __________________________________
         * |                                  |
         * |    Niveau 2 Functionaliteiten    |
         * |__________________________________|
         */


        private void N2Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            String imgnaam = "";
            Image img;
            switch (naam)
            {
                case "firstN2":
                    img = firstN2Image;
                    imgnaam = "first";
                    break;
                case "backN2":
                    img = backN2Image;
                    imgnaam = "back";
                    break;
                case "nextN2":
                    imgnaam = "next";
                    img = nextN2Image;
                    break;
                case "lastN2":
                    img = lastN2Image;
                    imgnaam = "last";
                    break;
                //case "searchN2":
                //    img = searchN2Image;
                //    imgnaam = "search";
                //    break;
                case "newN2":
                    img = newN2Image;
                    imgnaam = "new";
                    break;
                //case "saveN2":
                //    img = saveN2Image;
                //    imgnaam = "save";
                //    break;
                default:
                    img = firstN2Image;
                    imgnaam = "first";
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + imgnaam + "_hover.gif"));

        }

        private void N2Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            String imgnaam = "";
            Image img;
            switch (naam)
            {
                case "firstN2":
                    img = firstN2Image;
                    imgnaam = "first";
                    break;
                case "backN2":
                    img = backN2Image;
                    imgnaam = "back";
                    break;
                case "nextN2":
                    imgnaam = "next";
                    img = nextN2Image;
                    break;
                case "lastN2":
                    img = lastN2Image;
                    imgnaam = "last";
                    break;
                //case "searchN2":
                //    img = searchN2Image;
                //    imgnaam = "search";
                //    break;
                case "newN2":
                    img = newN2Image;
                    imgnaam = "new";
                    break;
                //case "saveN2":
                //    img = saveN2Image;
                //    imgnaam = "save";
                //    break;
                default:
                    img = firstN2Image;
                    imgnaam = "first";
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + imgnaam + ".gif"));

        }

        private void firstN2_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau2ViewSource.View.MoveCurrentToFirst();
            goUpdateN2();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void backN2_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau2ViewSource.View.MoveCurrentToPrevious();
            goUpdateN2();
            IsScroll = false;
            IsButtonClick = false;

        }

        private void nextN2_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau2ViewSource.View.MoveCurrentToNext();
            goUpdateN2();
            IsScroll = false;
            IsButtonClick = false;

        }

        private void lastN2_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau2ViewSource.View.MoveCurrentToLast();
            goUpdateN2();
            IsScroll = false;
            IsButtonClick = false;

        }

        private void newN2_Click(object sender, RoutedEventArgs e)
        {
            NieuwNiveau2 n2 = new NieuwNiveau2(currentUser);
            if (n2.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void saveN2_Click(object sender, RoutedEventArgs e)
        {
            SaveN2Wijzigingen();
            
        }

        private void SaveN2Wijzigingen()
        {
            if (GewijzigdeN2Lijst.Count > 0)
            {
                Niveau2Services service = new Niveau2Services();
                foreach (Niveau2 N2 in GewijzigdeN2Lijst)
                {
                    N2.Gewijzigd = DateTime.Now;
                    N2.Gewijzigd_door = currentUser.Naam;
                    try
                    {
                        service.SaveNiveau2Wijzigingen(N2);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                GewijzigdeN2Lijst.Clear();
                int index = niveau2DataGrid.SelectedIndex;
                LoadData();
                niveau2DataGrid.SelectedIndex = index;
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            }
        }

        private void goUpdateN2()
        {
            if (niveau2DataGrid.Items.Count <= 2)
            {
                backN2.IsEnabled = false;
                firstN2.IsEnabled = false;
                nextN2.IsEnabled = false;
                lastN2.IsEnabled = false;
            }
            else if (niveau2ViewSource.View.CurrentPosition == 0)
            {
                backN2.IsEnabled = false;
                firstN2.IsEnabled = false;
                nextN2.IsEnabled = true;
                lastN2.IsEnabled = true;
            }
            else if (niveau2ViewSource.View.CurrentPosition == niveau2DataGrid.Items.Count - 2)
            {
                backN2.IsEnabled = true;
                firstN2.IsEnabled = true;
                nextN2.IsEnabled = false;
                lastN2.IsEnabled = false;
            }
            else
            {
                backN2.IsEnabled = true;
                firstN2.IsEnabled = true;
                nextN2.IsEnabled = true;
                lastN2.IsEnabled = true;
            }
            if (!IsScroll)
                niveau2DataGrid.SelectedIndex = 0;
            if (niveau2DataGrid.Items.Count != 0)
                niveau2DataGrid.ScrollIntoView(niveau2DataGrid.SelectedItem);
        }

        private void searchN2_Click(object sender, RoutedEventArgs e)
        {
            ZoekN2InLijst();
        }

        private void ZoekN2TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekN2InLijst();
        }

        private void ZoekN2InLijst()
        {
            GevondenN2Lijst.Clear();

            GevondenN2Lijst = (from n2 in Niveau2lijst
                               where n2.Naam.ToLower().Contains(ZoekN2TextBox.Text.ToLower())
                               select n2).ToList();

            if (GevondenN2Lijst.Count > 0)
            {
                niveau2ViewSource.Source = GevondenN2Lijst;
            }
            else
            {
                niveau2ViewSource.Source = Niveau2lijst;
            }
        }


        private void niveau2DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (niveau2DataGrid.Items.Count > 1)
            {
                Niveau2 niv2 = (Niveau2)niveau2DataGrid.SelectedItem;
                naamComboBox.Text = niv2.Niveau1.Naam;
                if (!IsButtonClick)
                {
                    IsScroll = true;
                    goUpdateN2();
                    IsScroll = false;
                }
            }
        }

        private void controlN2_LostFocus(object sender, RoutedEventArgs e)
        {
            registerN2Changes();
        }

        private void naamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            registerN2Changes();          
        }

        private void registerN2Changes()
        {
            if (niveau2DataGrid.Items.Count > 1)
            {
                Niveau2 n2 = (Niveau2)niveau2DataGrid.SelectedItem;
                Niveau1 n1 = (Niveau1)naamComboBox.SelectedItem;
                n2.Niveau1_ID = n1.ID;
                n2.Naam = naamTextBox1.Text;
                n2.Naam_FR = beschrijvingTextBox1.Text;
                n2.Actief = actiefCheckBox1.IsChecked == true;
                n2.Gewijzigd = DateTime.Now;
                n2.Gewijzigd_door = currentUser.Naam;
                Niveau2Services service = new Niveau2Services();
                try
                {
                    service.SaveNiveau2Wijzigingen(n2);
                    int index = niveau2DataGrid.SelectedIndex;
                    LoadData();
                    niveau2DataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan!\n " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void gridN2Textbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Niveau2 N2 = (Niveau2)niveau2DataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "gridN2NaamTextBox":
                        N2.Naam = text.Text;
                        break;
                    case "gridN2BeschrijvingTextBox":
                        N2.Naam_FR = text.Text;
                        break;
                }
                try
                {
                    Niveau2Services service = new Niveau2Services();
                    service.SaveNiveau2Wijzigingen(N2);
                    int index = niveau2DataGrid.SelectedIndex;
                    LoadData();
                    niveau2DataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }



        /*  ___________________________________________
         * |                                           |
         * |        Niveau 3 functionaliteiten         |
         * |___________________________________________|
         * */

        private void N3Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            String imgnaam = "";
            Image img;
            switch (naam)
            {
                case "firstN3":
                    img = firstN3Image;
                    imgnaam = "first";
                    break;
                case "backN3":
                    img = backN3Image;
                    imgnaam = "back";
                    break;
                case "nextN3":
                    imgnaam = "next";
                    img = nextN3Image;
                    break;
                case "lastN3":
                    img = lastN3Image;
                    imgnaam = "last";
                    break;
                //case "searchN3":
                //    img = searchN3Image;
                //    imgnaam = "search";
                //    break;
                case "newN3":
                    img = newN3Image;
                    imgnaam = "new";
                    break;
                //case "saveN3":
                //    img = saveN3Image;
                //    imgnaam = "save";
                //    break;
                default:
                    img = firstN3Image;
                    imgnaam = "first";
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + imgnaam + "_hover.gif"));

        }

        private void N3Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            String imgnaam = "";
            Image img;
            switch (naam)
            {
                case "firstN3":
                    img = firstN3Image;
                    imgnaam = "first";
                    break;
                case "backN3":
                    img = backN3Image;
                    imgnaam = "back";
                    break;
                case "nextN3":
                    imgnaam = "next";
                    img = nextN3Image;
                    break;
                case "lastN3":
                    img = lastN3Image;
                    imgnaam = "last";
                    break;
                //case "searchN3":
                //    img = searchN3Image;
                //    imgnaam = "search";
                //    break;
                case "newN3":
                    img = newN3Image;
                    imgnaam = "new";
                    break;
                //case "saveN3":
                //    img = saveN3Image;
                //    imgnaam = "save";
                //    break;
                default:
                    img = firstN3Image;
                    imgnaam = "first";
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + imgnaam + ".gif"));

        }

        private void firstN3_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau3ViewSource.View.MoveCurrentToFirst();
            goUpdateN3();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void backN3_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau3ViewSource.View.MoveCurrentToPrevious();
            goUpdateN3();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void nextN3_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau3ViewSource.View.MoveCurrentToNext();
            goUpdateN3();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void lastN3_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            niveau3ViewSource.View.MoveCurrentToLast();
            goUpdateN3();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newN3_Click(object sender, RoutedEventArgs e)
        {
            NieuwNiveau3 NN3 = new NieuwNiveau3(currentUser);
            if (NN3.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void saveN3_Click(object sender, RoutedEventArgs e)
        {
            SaveN3Wijzigingen();
            LoadData();
        }

        private void SaveN3Wijzigingen()
        {
            if (niveau3DataGrid.SelectedIndex > -1)
            {
                Niveau3 n3 = (Niveau3)niveau3DataGrid.SelectedItem;
                Niveau2 n2 = (Niveau2)naamComboBox1.SelectedItem;
                n3.Naam = naamTextBox2.Text;
                n3.Naam_FR = beschrijvingTextBox2.Text;
                n3.Niveau2_ID = n2.ID;
                n3.Actief = actiefCheckBox2.IsChecked == true;
                n3.Gewijzigd = DateTime.Now;
                n3.Gewijzigd_door = currentUser.Naam;
                Niveau3Services service = new Niveau3Services();
                try
                {
                    service.SaveNiveauWijzigingen3(n3);
                    int index = niveau3DataGrid.SelectedIndex;
                    LoadData();
                    niveau3DataGrid.SelectedIndex = index;
                    if (niveau3DataGrid.Items.Count > 1)
                    {
                        Niveau3 niv3 = (Niveau3)niveau3DataGrid.SelectedItem;
                        naamComboBox1.Text = niv3.Niveau2.Niveau1En2Namen;
                        if (!IsButtonClick)
                        {
                            IsScroll = true;
                            goUpdateN3();
                            IsScroll = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan!\n " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void searchN3_Click(object sender, RoutedEventArgs e)
        {
            ZoekN3InLijst();
        }

        private void goUpdateN3()
        {
            if (niveau3DataGrid.Items.Count <= 2)
            {
                backN3.IsEnabled = false;
                firstN3.IsEnabled = false;
                nextN3.IsEnabled = false;
                lastN3.IsEnabled = false;
            }
            else if (niveau3ViewSource.View.CurrentPosition == 0)
            {
                backN3.IsEnabled = false;
                firstN3.IsEnabled = false;
                nextN3.IsEnabled = true;
                lastN3.IsEnabled = true;
            }
            else if (niveau3ViewSource.View.CurrentPosition == niveau3DataGrid.Items.Count - 2)
            {
                backN3.IsEnabled = true;
                firstN3.IsEnabled = true;
                nextN3.IsEnabled = false;
                lastN3.IsEnabled = false;
            }
            else
            {
                backN3.IsEnabled = true;
                firstN3.IsEnabled = true;
                nextN3.IsEnabled = true;
                lastN3.IsEnabled = true;
            }
            if (!IsScroll)
                niveau3DataGrid.SelectedIndex = 0;
            if (niveau3DataGrid.Items.Count != 0)
                niveau3DataGrid.ScrollIntoView(niveau3DataGrid.SelectedItem);
        }

        private void niveau3DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (niveau3DataGrid.Items.Count > 1)
            {
                Niveau3 niv3 = (Niveau3)niveau3DataGrid.SelectedItem;
                naamComboBox1.Text = niv3.Niveau2.Niveau1En2Namen;
                if (!IsButtonClick)
                {
                    IsScroll = true;
                    goUpdateN3();
                    IsScroll = false;
                }
            }
        }

        private void controlN3_LostFocus(object sender, RoutedEventArgs e)
        {
            SaveN3Wijzigingen();
        }

        private void controlN3_LostFocus(object sender, EventArgs e)
        {
            SaveN3Wijzigingen();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (GewijzigdeN1Lijst.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen in Niveau1 die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveN1Wijzigingen();
                }
            }
            if (GewijzigdeN2Lijst.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen in Niveau2 die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveN2Wijzigingen();
                }
            }
            if (GewijzigdeN3Lijst.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen in Niveau3 die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveN3Wijzigingen();
                }
            }
        }

        private void ZoekN3TextBox_KeyUp(object sender, KeyEventArgs e)
        {
                ZoekN3InLijst();
        }

        private void ZoekN3InLijst()
        {
            GevondenN3Lijst.Clear();

            GevondenN3Lijst = (from n in Niveau3lijst
                               where n.Naam.ToLower().Contains(ZoekN3TextBox.Text.ToLower())
                               select n).ToList();

            if (GevondenN3Lijst.Count > 0)
            {
                niveau3ViewSource.Source = GevondenN3Lijst;
            }
            else
            {
                niveau3ViewSource.Source = Niveau3lijst;
            }
        }

        private void gridN3Textbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Niveau3 N3 = (Niveau3)niveau3DataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "gridN3NaamTextBox":
                        N3.Naam = text.Text;
                        break;
                    case "gridN3BeschrijvingTextBox":
                        N3.Naam_FR = text.Text;
                        break;
                }
                try
                {
                    Niveau3Services service = new Niveau3Services();
                    service.SaveNiveauWijzigingen3(N3);
                    int index = niveau3DataGrid.SelectedIndex;
                    LoadData();
                    niveau3DataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ITCLibrary;
using System.Windows.Media.Animation;
using System.Collections.ObjectModel;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Gebruiker currentUser;
        private Int32 UserID;
        private CollectionViewSource contactViewSource;
        private Boolean IsScroll = false, IsButtonClick = false, IsUserClick = false;
        private CollectionViewSource functiesViewSource;
        private CollectionViewSource bedrijfViewSource;
        private CollectionViewSource erkenningViewSource;
        private ObservableCollection<Functies> functies = new ObservableCollection<Functies>();
        private List<Functies> gevondenfuncties = new List<Functies>();
        private string _dataFolder;


        public MainWindow(Gebruiker currentuser)
        {
            InitializeComponent();
            currentUser = currentuser;
            GebruikerService service = new GebruikerService();
            //Gebruiker user = service.GetUserByName(currentUser.ID.ToString());
            UserID = currentUser.ID;
            this.Title = "Zoek naar klant/prospect : Ingelogd als " + currentUser.Naam;
            aansprekingColumn.Visibility = System.Windows.Visibility.Hidden;
            //reloadprogressBar.Visibility = System.Windows.Visibility.Hidden;
            SetKolomkoppen();
            ControleerMachtigingen();
            _dataFolder = new Instelling_Service().Find().DataFolder;
        }

        private void ControleerMachtigingen()
        {
            if (currentUser.Rol != 1)
            {
                GebruikersRollenMenuItem.Visibility = Visibility.Collapsed;
                ImportMarketingMenuItem.Visibility = Visibility.Collapsed;
                excel.Visibility = Visibility.Collapsed;
                doorPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                GebruikersRollenMenuItem.Visibility = Visibility.Visible;
                ImportMarketingMenuItem.Visibility = Visibility.Collapsed;
                excel.Visibility = Visibility.Visible;
                doorPanel.Visibility = Visibility.Visible;
            }

        }

        private void SetKolomkoppen()
        {
            try
            {
                GebruikerService service = new GebruikerService();
                String Filename = service.GetConfigFileName(currentUser.Naam);

                if (Filename != "")
                {
                    ContactConfig config = Helper.GetContactConfigFromXml(Filename);

                    if (!config.ID)
                        iDColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        iDColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Voornaam)
                        voornaamColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        voornaamColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Achternaam)
                        achternaamColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        achternaamColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Geboortedatum)
                        geboortedatumColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        geboortedatumColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Aanspreking)
                        aansprekingColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        aansprekingColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.ContactEmail)
                        ContactEmailColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        ContactEmailColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.ContactTelefoon)
                        ContactTelefoonColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        ContactTelefoonColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Type)
                        typeColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        typeColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Taal)
                        taalColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        taalColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Niveau1)
                        niveau1Column.Visibility = System.Windows.Visibility.Hidden;
                    else
                        niveau1Column.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Niveau2)
                        niveau2Column.Visibility = System.Windows.Visibility.Hidden;
                    else
                        niveau2Column.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Niveau3)
                        niveau3Column.Visibility = System.Windows.Visibility.Hidden;
                    else
                        niveau3Column.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Bedrijf)
                        bedrijfColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        bedrijfColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Telefoon)
                        TelefoonColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        TelefoonColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Fax)
                        faxColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        faxColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Email)
                        bedrijfEmailColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        bedrijfEmailColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Adres)
                        adresColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        adresColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Postcode)
                        postcodeColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        postcodeColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Gemeente)
                        GemeenteColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        GemeenteColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Notities)
                        notitiesColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        notitiesColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.NieuwsbriefMail)
                        nieuwsbriefMailColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        nieuwsbriefMailColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.NieuwsbriefFax)
                        nieuwsbriefFaxColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        nieuwsbriefFaxColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Gewijzigd)
                        gewijzigdColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        gewijzigdColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.GewijzigdDoor)
                        gewijzigdDoorColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        gewijzigdDoorColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Aangemaakt)
                        aangemaaktColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        aangemaaktColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.AangemaaktDoor)
                        aangemaaktdoorColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        aangemaaktdoorColumn.Visibility = System.Windows.Visibility.Visible;
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        /* ___________________________________________
         * |                                          |
         * |             Menu aanroepen               |
         * |__________________________________________|
         * */

        private void AansprekingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AansprekingBeheer AB = new AansprekingBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }

        private void BedrijvenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            BedrijfBeheer BB = new BedrijfBeheer(currentUser);
            BB.Show();
            BB.Activate();
        }

        private void ErkenningMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ErkenningBeheer EB = new ErkenningBeheer(currentUser, null);
            EB.Show();
            EB.Activate();
        }

        private void FunctiesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            InschrijvingenService iService = new InschrijvingenService();
            iService.UpdateTaantal();
            //HumanResourceWindow HW = new HumanResourceWindow(currentUser);
            //HW.Show();
            //HW.Activate();
        }

        private void NiveausMenuItem_Click(object sender, RoutedEventArgs e)
        {
            NiveauBeheer NB = new NiveauBeheer(currentUser);
            NB.Show();
            NB.Activate();
        }

        private void SprekerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SprekerBeheer SB = new SprekerBeheer(currentUser);
            SB.Show();
            SB.Activate();
        }

        private void TalenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            TaalBeheer TB = new TaalBeheer(currentUser);
            TB.Show();
            TB.Activate();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Wil je het programma sluiten?", "Afsluiten", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void GebruikerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GebruikerBeheer GB = new GebruikerBeheer(currentUser);
            GB.Show();
            GB.Activate();
        }

        private void ContactMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ContactServices cService = new ContactServices();
            List<Contact> contacten = cService.FindAll();
            foreach (Contact contact in contacten)
            {
                cService.SaveContactChanges(contact);
            }
            //ContactBeheer CB = new ContactBeheer(currentUser);
            //CB.Show();
        }

        private void InportPostcodesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportPostcodes IP = new ImportPostcodes();
            IP.Show();
            IP.Activate();
        }

        private void LocatieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            LocatieBeheer LB = new LocatieBeheer(currentUser);
            LB.Show();
            LB.Activate();
        }

        private void OnderwerpMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnderwerpBeheer OB = new OnderwerpBeheer(currentUser);
            OB.Show();
            OB.Activate();
        }

        private void SeminarieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SeminarieBeheer SB = new SeminarieBeheer(currentUser);
            SB.Show();
            SB.Activate();
        }

        private void GebruikersRollenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GebruikersRolen GR = new GebruikersRolen(currentUser);
            GR.Show();
            GR.Activate();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            contactViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("contactViewSource")));
            functiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functiesViewSource")));
            bedrijfViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("bedrijfViewSource")));
            erkenningViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("erkenningViewSource")));

            LoadData();
        }

        /********************************************
         *               Data ophalen               *
         *******************************************/

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;
            VerbergSprekerGrid();

            LoadWeergaveComboBox();


            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> n1Lijst = n1Service.FindActive();
            Niveau1 n1 = new Niveau1();
            n1.Naam = "Kies";
            n1Lijst.Insert(0, n1);
            niveau1ComboBox.ItemsSource = n1Lijst;
            niveau1ComboBox.DisplayMemberPath = "Naam";
            niveau1ComboBox.SelectedIndex = 0;

            LoadNiveau2En3();

            PostcodeService postService = new PostcodeService();
            List<Postcodes> gemeentes = postService.FindAllGemeentes();
            Postcodes gemeente = new Postcodes();
            gemeente.Postcode = "Kies";
            gemeente.Gemeente = "Kies";
            gemeentes.Insert(0, gemeente);
            LocatieComboBox.ItemsSource = gemeentes;
            LocatieComboBox.DisplayMemberPath = "Gemeente";
            LocatieComboBox.SelectedIndex = 0;

            List<string> emailoptions = new List<string>() { "Kies", "Leeg" };
            emailComboBox.ItemsSource = emailoptions;
            emailComboBox.SelectedIndex = 0;

            TaalServices tService = new TaalServices();
            List<Taal> talen = tService.FindAll();
            Taal taal = new Taal();
            taal.Naam = "Kies";
            talen.Insert(0, taal);
            languageComboBox.ItemsSource = talen;
            languageComboBox.DisplayMemberPath = "Naam";
            languageComboBox.SelectedIndex = 0;

            List<Gebruiker> doorgebruikers = new GebruikerService().GetAvtiveGebruikers();
            Gebruiker dummy = new Gebruiker();
            dummy.Naam = "";
            doorgebruikers.Insert(0, dummy);
            doorComboBox.ItemsSource = doorgebruikers;
            doorComboBox.DisplayMemberPath = "Naam";
            //ReLoadContacten(null);
            //BedrijfServices bedrijfService = new BedrijfServices();
            //List<Bedrijf> bedrijven = bedrijfService.FindAll();
            //Bedrijf bedrijf = new Bedrijf();
            //bedrijf.Naam = "Kies";
            //bedrijven.Insert(0, bedrijf);
            //bedrijvenComboBox.ItemsSource = bedrijven;
            //bedrijvenComboBox.DisplayMemberPath = "Naam";
            //bedrijvenComboBox.SelectedIndex = 0;
        }

        private void ReLoadContacten(Functies functie)
        {
            ActivateFilters(functie);

            //goUpdate();
            //if (functie != null)
            //    contactDataGrid.SelectedItem = functie;
        }

        private void LoadContacten(Functies functie)
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
                FunctieServices functieService = new FunctieServices();
                functies = functieService.FindActive();
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactViewSource.SetValue(CollectionViewSource.SourceProperty, functies); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
                if (functie != null)
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactDataGrid.SetValue(DataGrid.SelectedItemProperty, functie); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate
                {
                    ActivateFilters(null);
                }, null);

            }
            catch (InvalidOperationException ex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ShowError(ex); }, null);
            }
            catch (Exception nex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ShowError(nex); }, null);
            }
        }

        private void ShowError(Exception ex)
        {
            if (ex is InvalidOperationException)
                ConfocusMessageBox.Show("Fout bij het filteren van de contacten!\n" + ((InvalidOperationException)ex).Message, ConfocusMessageBox.Kleuren.Rood);
            else
                ConfocusMessageBox.Show("Fout bij het filteren van de contacten!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
        }

        private void StartAnimation()
        {
            reloadprogressBar.SetValue(ProgressBar.ValueProperty, 0.0);
            Duration duration = new Duration(TimeSpan.FromSeconds(25));
            DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
            reloadprogressBar.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
        }

        private void LoadWeergaveComboBox()
        {
            WeergaveServices weergaveService = new WeergaveServices();
            List<Weergave> weergaven = weergaveService.GetWeergavesByUserId(UserID);
            Weergave gave = new Weergave();
            gave.Naam = "Kies";
            weergaven.Insert(0, gave);
            WeergavenComboBox.ItemsSource = weergaven;
            WeergavenComboBox.DisplayMemberPath = "Naam";
            WeergavenComboBox.SelectedIndex = 0;
        }

        private void LoadNiveau2En3()
        {
            //Niveau2Services n2Service = new Niveau2Services();
            //List<Niveau2> n2Lijst = n2Service.FindAll();
            List<Niveau2> n2lijst = new List<Niveau2>();
            Niveau2 n2 = new Niveau2();
            n2.Naam = "Leeg";
            n2lijst.Add(n2);
            niveau2ComboBox.ItemsSource = n2lijst;
            niveau2ComboBox.DisplayMemberPath = "Naam";

            //Niveau3Services n3Service = new Niveau3Services();
            //List<Niveau3> n3Lijst = n3Service.FindAll();
            List<Niveau3> n3lijst = new List<Niveau3>();
            Niveau3 n3 = new Niveau3();
            n3.Naam = "Leeg";
            n3lijst.Add(n3);
            niveau3ComboBox.ItemsSource = n3lijst;
            niveau3ComboBox.DisplayMemberPath = "Naam";
        }

        /*************************************************
         *              Toolbar functies                 *
         ************************************************/

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "excel":
                    img = excelImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "flex":
                    img = flexImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "excel":
                    img = excelImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "flex":
                    img = flexImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));

        }

        private void goUpdate()
        {
            if (contactDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (contactViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (contactViewSource.View.CurrentPosition == contactDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                contactDataGrid.SelectedIndex = 0;
            if (contactDataGrid.Items.Count != 0)
                contactDataGrid.ScrollIntoView(contactDataGrid.SelectedItem);
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {            
            ActivateFilters(null);
        }

        private void zoekNaamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ActivateFilters(null);
            }
        }


        private void ActivateFilters(Functies functie)
        {
            gevondenfuncties.Clear();
            ContactSearchFilter filter = GetContactFilter();
            taalTextBox.Text = "";
            mailLabel.Content = "";
            faxLabel.Content = "";

            new Thread(delegate () { GetContacten(filter, functie); }).Start();
            //GetContactenAsync(filter, functie);
            SetAantal();
            goUpdate();

        }

        private async void GetContactenAsync(ContactSearchFilter filter, Functies functie)
        {
            LoadingGrid.Visibility = Visibility.Visible;
            FunctieServices functieService = new FunctieServices();
            ObservableCollection<Functies> results = new ObservableCollection<Functies>();
            results = await Task.Run(() => functieService.FindByFilter(filter));
            List<Contact> Unieke = new List<Contact>();
            foreach (Functies func in results)
            {
                if (!Unieke.Contains(func.Contact))
                    Unieke.Add(func.Contact);
            }

            UniekeLabel.Content = Unieke.Count + " unieke contacten.";
            contactViewSource.Source = results;
            LoadingGrid.Visibility = Visibility.Hidden;
            if (functie != null)
                contactDataGrid.SelectedItem = functie;
        }

        private void GetContacten(ContactSearchFilter filter, Functies functie)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
            FunctieServices functieService = new FunctieServices();
            ObservableCollection<Functies> results = new ObservableCollection<Functies>();
            try
            {
                results = functieService.FindByFilter(filter);//new ObservableCollection<Functies>(functieService.GetActiveFunctiesByFaxnumber("003256327321"));//
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() => 
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }));
            }
            List<Contact> Unieke = new List<Contact>();
            foreach (Functies func in results)
            {
                if (!Unieke.Contains(func.Contact))
                    Unieke.Add(func.Contact);
            }

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate
            {
                UniekeLabel.SetValue(Label.ContentProperty, Unieke.Count + " unieke contacten.");
            }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactViewSource.SetValue(CollectionViewSource.SourceProperty, results); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
            if (functie != null)
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactDataGrid.SetValue(DataGrid.SelectedItemProperty, functie); }, null);

            //SetAantal();
        }

        private ContactSearchFilter GetContactFilter()
        {
            ContactSearchFilter filter = new ContactSearchFilter();
            if (zoekVoorNaamTextBox.Text != "")
                filter.Voornaam = DBHelper.Simplify(zoekVoorNaamTextBox.Text);
            if (zoekAchterNaamTextBox.Text != "")
                filter.Achternaam = DBHelper.Simplify(zoekAchterNaamTextBox.Text);
            if (bedrijvenComboBox.SelectedIndex > 0)
            {
                filter.Bedrijf_ID = ((Bedrijf)bedrijvenComboBox.SelectedItem).ID;
            }
            if (emailComboBox.Text != "Kies")
            {
                filter.Email = emailComboBox.Text;
            }
            if (typeComoBox.SelectedIndex > 0)
            {
                filter.ContactType = typeComoBox.Text;
            }
            if (niveau1ComboBox.SelectedIndex > 0)
            {
                filter.Niveau1_ID = ((Niveau1)niveau1ComboBox.SelectedItem).ID;
            }
            if (niveau2ComboBox.SelectedIndex > 0)
            {
                filter.Niveau2_ID = ((Niveau2)niveau2ComboBox.SelectedItem).ID;
            }
            if (niveau3ComboBox.SelectedIndex > 0)
            {
                filter.Niveau3_ID = ((Niveau3)niveau3ComboBox.SelectedItem).ID;
            }
            if (LocatieComboBox.SelectedIndex > 0)
            {
                filter.Gemeente = LocatieComboBox.Text;
            }
            if (ProvincieComboBox.SelectedIndex > 0)
            {
                filter.Provincie = ProvincieComboBox.Text;
            }
            if (aangemaaktDatepicker.SelectedDate != null && aangemaaktDatepicker.SelectedDate > new DateTime(2014, 12, 1))
            {
                filter.Aangemaakt = (DateTime)aangemaaktDatepicker.SelectedDate;
            }
            if (languageComboBox.SelectedIndex > 0)
            {
                filter.Taal_ID = ((Taal)languageComboBox.SelectedItem).ID;
            }
            if (faxnummerTextBox.Text != "")
            {
                filter.Faxnummer = faxnummerTextBox.Text;
            }
            if (doorComboBox.SelectedIndex > 0)
            {
                filter.Door = ((Gebruiker)doorComboBox.SelectedItem).Naam;
            }

            return filter;
        }

        private void contactDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactDataGrid.SelectedItem is Functies)
            {
                try
                {
                    ClearSpreker();
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    if (functie != null)
                    {
                        mailLabel.Content = functie.Mail == true ? "Ja" : "Nee";
                        faxLabel.Content = functie.Fax == true ? "Ja" : "Nee";
                        Contact contact = functie.Contact;

                        if (contact != null)
                        {
                            taalTextBox.Text = "";
                            if (contact.Taal_ID != null)
                            {
                                decimal taalid = (decimal)contact.Taal_ID;
                                TaalServices taalservice = new TaalServices();
                                Taal taal = taalservice.GetTaalByID(taalid);
                                if (taal != null)
                                    taalTextBox.Text = taal.Naam;
                            }
                            SecundaireTaalTextBox.Text = "";
                            if (contact.Secundaire_Taal_ID != null)
                            {
                                decimal taalid = (decimal)contact.Secundaire_Taal_ID;
                                TaalServices taalservice = new TaalServices();
                                Taal taal = taalservice.GetTaalByID(taalid);
                                if (taal != null)
                                    SecundaireTaalTextBox.Text = taal.Naam;
                            }
                        }
                        //contactErkenningTextBox.Text = "";
                        //if (contact.Erkenning_ID != null)
                        //{
                        //    ErkenningServices eService = new ErkenningServices();
                        //    Erkenning erkenning = eService.GetErkenningByID(contact.Erkenning_ID);
                        //    contactErkenningTextBox.Text = erkenning.Naam;
                        //}

                        //if (functie.Erkenning != null)
                        //    ToonErkenningGrid();
                        //else
                        //    VerbergErkenningGrid();

                        //SprekerServices sprekerservice = new SprekerServices();
                        //Spreker spreker = sprekerservice.GetSprekerByContactBedrijf(functie.Contact_ID, functie.Bedrijf_ID);
                        //if (spreker != null)
                        //{
                        //    SetSpreker(spreker);
                        //    ToonSprekerGrid();
                        //}
                        //else
                        //{
                        //    VerbergSprekerGrid();
                        //}

                        // Aangemaakt / Gewijzigd labels invullen
                        if (functie.Contact != null)
                        {
                            if (functie.Contact.Gewijzigd == null)
                            {
                                ContactAangemaakLabelt.Content = "Aangemaakt op " + functie.Contact.Aangemaakt + " door " + functie.Contact.Aangemaakt_door + ".";
                            }
                            else
                            {
                                ContactAangemaakLabelt.Content = "Gewijzigd op " + functie.Contact.Gewijzigd + " door " + functie.Contact.Gewijzigd_door + ".";
                            }

                            if (functie.Gewijzigd == null)
                            {
                                FunctieAangemaaktLabel.Content = "Aangemaakt op " + functie.Aangemaakt + " door " + functie.Aangemaakt_door + ".";
                            }
                            else
                            {
                                FunctieAangemaaktLabel.Content = "Gewijzigd op " + functie.Gewijzigd + " door " + functie.Gewijzigd_door + ".";
                            }
                        }

                    }
                    else
                    {
                        taalTextBox.Text = "";
                        SecundaireTaalTextBox.Text = "";
                        mailLabel.Content = "";
                        faxLabel.Content = "";
                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het tonen van het selecteerde contact!\n " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdate();
                IsScroll = false;
            }
            SetAantal();
        }

        private void VerbergErkenningGrid()
        {
            //erkenningLabel.Visibility = System.Windows.Visibility.Hidden;
            //erkenningGrid.Visibility = System.Windows.Visibility.Hidden;
        }

        private void ToonErkenningGrid()
        {
            //erkenningLabel.Visibility = System.Windows.Visibility.Visible;
            //erkenningGrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void VerbergSprekerGrid()
        {
            //sprekerGrid.Visibility = System.Windows.Visibility.Hidden;
            //sprekerLabel.Visibility = System.Windows.Visibility.Hidden;
        }

        private void ToonSprekerGrid()
        {
            //sprekerGrid.Visibility = System.Windows.Visibility.Visible;
            //sprekerLabel.Visibility = System.Windows.Visibility.Visible;
        }

        private void ClearSpreker()
        {
            //cVTextBox.Text = "";
            //onderwerpTextBox.Text = "";
            //tariefTextBox.Text = "";
            //waarderingTextBox.Text = "";
        }

        private void SetSpreker(Spreker spreker)
        {
            //cVTextBox.Text = spreker.CV;
            //onderwerpTextBox.Text = spreker.Onderwerp;
            //tariefTextBox.Text = spreker.Tarief.ToString();
            //waarderingTextBox.Text = spreker.Waardering.ToString();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            MaakFiltersLeeg();
            WeergavenComboBox.SelectedIndex = 0;
            contactViewSource.Source = functies;
            SetAantal();
        }

        private void MaakFiltersLeeg()
        {
            //WeergavenComboBox.SelectedIndex = 0;
            bedrijvenComboBox.SelectedIndex = 0;
            List<string> emailoptions = new List<string>() { "Kies", "Leeg" };
            emailComboBox.ItemsSource = emailoptions;
            emailComboBox.SelectedIndex = 0;
            typeComoBox.SelectedIndex = 0;
            niveau1ComboBox.SelectedIndex = 0;
            LoadNiveau2En3();
            //niveau2ComboBox.ItemsSource = new List<Niveau2>();
            //niveau3ComboBox.ItemsSource = new List<Niveau3>();
            LocatieComboBox.SelectedIndex = 0;
            ProvincieComboBox.SelectedIndex = 0;
            aangemaaktDatepicker.SelectedDate = null;
            zoekVoorNaamTextBox.Text = "";
            zoekAchterNaamTextBox.Text = "";
            faxnummerTextBox.Text = "";
            languageComboBox.SelectedIndex = 0;
            doorComboBox.SelectedIndex = 0;
        }



        private void SetAantal()
        {
            if (contactDataGrid.Items.Count > 1)
            {
                AantalLabel.Content = (contactDataGrid.SelectedIndex + 1) + " van " + (contactDataGrid.Items.Count) + " contacten.";
            }
            else
                AantalLabel.Content = "0 van 0 contacten.";
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Splash SS = new Splash();
            SS.Show();
            this.Close();
        }

        private void ConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ConfiguratieBeheer CB = new ConfiguratieBeheer(currentUser);
            if (CB.ShowDialog() == true)
            {
                SetKolomkoppen();
            }
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NieuwContact NC = new NieuwContact(currentUser, "Functie");
                if (NC.ShowDialog() == true)
                {
                    Functies functie = NC.SelectedFunctie;
                    ActivateFilters(functie);
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het openen van nieuw contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            if (contactDataGrid.SelectedIndex > -1)
            {
                try
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    //BewerkContact BC = new BewerkContact(Username, functie);
                    NieuwContact BC = new NieuwContact(currentUser, functie);
                    if (BC.ShowDialog() == true)
                    {
                        LoadData();
                    }

                }
                catch (Exception ex)
                {

                    ConfocusMessageBox.Show("Fout bij het laden van bewerkscherm!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void ZoekSprekerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ZoekSprekerScherm ZS = new ZoekSprekerScherm(currentUser);
            ZS.Show();
            ZS.Activate();
            this.Close();
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            contactDataGrid.Cursor = Cursors.Wait;
            //LoadData();
            ReLoadContacten(null);
            contactDataGrid.Cursor = Cursors.Arrow;
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Niveau1 n1 = (Niveau1)box.SelectedItem;

                Niveau2Services service = new Niveau2Services();
                List<Niveau2> n2lijst = service.GetSelected(n1.ID);
                Niveau2 n2 = new Niveau2();
                n2.Naam = "Leeg";
                n2lijst.Insert(0, n2);
                niveau2ComboBox.ItemsSource = n2lijst;
                niveau2ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau2 n2 = (Niveau2)box.SelectedItem;

                Niveau3Services service = new Niveau3Services();
                List<Niveau3> n3lijst = service.GetSelected(n2.ID);
                Niveau3 n3 = new Niveau3();
                n3.Naam = "Leeg";
                n3lijst.Insert(0, n3);
                niveau3ComboBox.ItemsSource = n3lijst;
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void ToExcel(string url)
        {
            int lengte = contactDataGrid.Items.Count;
            String[] headers = GetActiveHeaders();
            String[][] contacten = new String[lengte][];
            int teller = 0;
            foreach (Object item in contactDataGrid.Items)
            {
                if (item is Functies)
                {
                    Functies functie = item as Functies;

                    String[] lid = GetActiveData(functie, headers);
                    contacten[teller] = lid;
                    teller++;
                }
            }

            // Naar CSV
            List<string> csvheaders = new List<string>();
            foreach (string header in headers)
            {
                csvheaders.Add(header);
            }
            CSVDocument doc = new CSVDocument(url);
            doc.AddRow(csvheaders);
            foreach (string[] data in contacten)
            {
                List<string> conta = new List<string>();
                foreach (string waarde in data)
                {
                    conta.Add(waarde);

                }
                doc.AddRow(conta);
            }
            doc.Save();
            System.Diagnostics.Process.Start(url);

            //Naar Excel
            //ExcelDocument Xl = new ExcelDocument();
            //Xl.SetColumnHeaders(headers, 1, true, true, true);
            //Xl.InsertList(contacten, true, 2);
            //object misValue = System.Reflection.Missing.Value;

        }

        private string[] GetActiveData(Functies functie, string[] headers)
        {
            List<String> mySpreker = new List<string>();
            if (headers.Contains("ID"))
                mySpreker.Add(functie.Contact_ID.ToString());
            if (headers.Contains("Voornaam"))
                mySpreker.Add(functie.Contact.Voornaam);
            if (headers.Contains("Achternaam"))
                mySpreker.Add(functie.Contact.Achternaam);
            if (headers.Contains("Geboortedatum"))
            {
                if (functie.Contact.Geboortedatum != null)
                    mySpreker.Add(((DateTime)functie.Contact.Geboortedatum).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Aanspreking"))
                mySpreker.Add(functie.Contact.Aanspreking);
            if (headers.Contains("Type"))
                mySpreker.Add(functie.Type);
            if (headers.Contains("Contact email"))
            {
                string email = functie.Email == null ? "" : functie.Email;
                email = email.Replace('\n', ' ');
                email = email.Replace(';', ',');
                mySpreker.Add(email);
            }
            if (headers.Contains("Contact telefoon"))
                mySpreker.Add(functie.Telefoon);
            if (headers.Contains("Taal"))
                if (functie.Contact.Taal != null)
                    mySpreker.Add(functie.Contact.Taal.Naam);
                else
                    mySpreker.Add("");
            if (headers.Contains("Niveau1"))
                mySpreker.Add(functie.Niveau1.Naam);
            if (headers.Contains("Niveau2"))
            {
                if (functie.Niveau2 != null)
                    mySpreker.Add(functie.Niveau2.Naam);
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Niveau3"))
            {
                if (functie.Niveau3 != null)
                    mySpreker.Add(functie.Niveau3.Naam);
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Bedrijf"))
            {
                string bedrijf = functie.Bedrijf.Naam == null ? "" : functie.Bedrijf.Naam;
                bedrijf = bedrijf.Replace('\n', ' ');
                bedrijf = bedrijf.Replace(';', ',');
                mySpreker.Add(bedrijf);
            }
            if (headers.Contains("Telefoon"))
            {
                string telefoon = functie.Bedrijf.Telefoon == null ? "" : functie.Bedrijf.Telefoon;
                telefoon = telefoon.Replace(';', ',');
                telefoon = telefoon.Replace('\n', ' ');
                mySpreker.Add(telefoon);
            }
            if (headers.Contains("Fax"))
            {
                string fax = functie.Bedrijf.Fax == null ? "" : functie.Bedrijf.Fax;
                fax = fax.Replace(';', ',');
                fax = fax.Replace('\n', ' ');
                mySpreker.Add(fax);
            }
            if (headers.Contains("Email"))
            {
                string email = functie.Bedrijf.Email == null ? "" : functie.Bedrijf.Email;
                email = email.Replace(';', ',');
                email = email.Replace('\n', ' ');
                mySpreker.Add(email);
            }
            if (headers.Contains("Adres"))
            {
                string adres = functie.Bedrijf.Adres == null ? "" : functie.Bedrijf.Adres;
                adres = adres.Replace("\n", " ");
                adres = adres.Replace(";", ",");
                mySpreker.Add(adres);
            }
            if (headers.Contains("Postcode"))
                mySpreker.Add(functie.Bedrijf.Postcode);
            if (headers.Contains("Gemeente"))
                mySpreker.Add(functie.Bedrijf.Plaats);
            if (headers.Contains("Notities"))
            {
                string notitie = functie.Contact.Notities == null ? "" : functie.Contact.Notities;
                notitie = notitie.Replace('\n', ' ');
                notitie = notitie.Replace(';', ',');
                mySpreker.Add(notitie);
            }
            if (headers.Contains("Nieuwsbrief via mail"))
                mySpreker.Add(functie.Mail == true ? "Ja" : "Nee");
            if (headers.Contains("Nieuwsbrief via fax"))
                mySpreker.Add(functie.Fax == true ? "Ja" : "Nee");
            if (headers.Contains("Gewijzigd"))
            {
                if (functie.Contact.Gewijzigd != null)
                    mySpreker.Add(((DateTime)functie.Contact.Gewijzigd).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Gewijzigd door"))
                mySpreker.Add(functie.Contact.Gewijzigd_door);
            if (headers.Contains("Aangemaakt"))
            {
                if (functie.Contact.Aangemaakt != null)
                    mySpreker.Add(((DateTime)functie.Contact.Aangemaakt).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Aangemaakt door"))
                mySpreker.Add(functie.Contact.Aangemaakt_door);

            return mySpreker.ToArray();
        }

        private string[] GetActiveHeaders()
        {
            List<String> myHeaders = new List<string>();
            GebruikerService service = new GebruikerService();
            String Filename = service.GetConfigFileName(currentUser.Naam);

            if (Filename != "")
            {
                ContactConfig config = Helper.GetContactConfigFromXml(Filename);
                if (config.ID)
                    myHeaders.Add("ID");
                if (config.Voornaam)
                    myHeaders.Add("Voornaam");
                if (config.Achternaam)
                    myHeaders.Add("Achternaam");
                if (config.Geboortedatum)
                    myHeaders.Add("Geboortedatum");
                if (config.Aanspreking)
                    myHeaders.Add("Aanspreking");
                if (config.Type)
                    myHeaders.Add("Type");
                if (config.ContactEmail)
                    myHeaders.Add("Contact email");
                if (config.ContactTelefoon)
                    myHeaders.Add("Contact telefoon");
                if (config.Taal)
                    myHeaders.Add("Taal");
                if (config.Niveau1)
                    myHeaders.Add("Niveau1");
                if (config.Niveau2)
                    myHeaders.Add("Niveau2");
                if (config.Niveau3)
                    myHeaders.Add("Niveau3");
                if (config.Bedrijf)
                    myHeaders.Add("Bedrijf");
                if (config.Telefoon)
                    myHeaders.Add("Telefoon");
                if (config.Fax)
                    myHeaders.Add("Fax");
                if (config.Email)
                    myHeaders.Add("Email");
                if (config.Adres)
                    myHeaders.Add("Adres");
                if (config.Postcode)
                    myHeaders.Add("Postcode");
                if (config.Gemeente)
                    myHeaders.Add("Gemeente");
                if (config.Notities)
                    myHeaders.Add("Notities");
                if (config.NieuwsbriefMail)
                    myHeaders.Add("Nieuwsbrief via mail");
                if (config.NieuwsbriefFax)
                    myHeaders.Add("Nieuwsbrief via fax");
                if (config.Gewijzigd)
                    myHeaders.Add("Gewijzigd");
                if (config.GewijzigdDoor)
                    myHeaders.Add("Gewijzigd door");
                if (config.Aangemaakt)
                    myHeaders.Add("Aangemaakt");
                if (config.AangemaaktDoor)
                    myHeaders.Add("Aangemaakt door");
            }
            return myHeaders.ToArray();
        }

        private void excelButton_Click(object sender, RoutedEventArgs e)
        {
            WeergaveInputWindow IW = new WeergaveInputWindow();

            if (IW.ShowDialog() == true)
            {
                string filename = IW.Naam;
                string url = _dataFolder + filename + ".csv";
                Task.Factory.StartNew(() => { ToExcel(url); });
            }
        }

        private void SaveWeergaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (WeergavenComboBox.SelectedIndex > 0)
            {
                Weergave geselecteerde = (Weergave)WeergavenComboBox.SelectedItem;
                Weergave myWeergave = GetWeergaveFromForm();
                if (myWeergave != null)
                {
                    myWeergave.ID = geselecteerde.ID;
                    myWeergave.Naam = geselecteerde.Naam;
                    myWeergave.User_ID = geselecteerde.User_ID;
                    WeergaveServices service = new WeergaveServices();
                    Weergave Saved = service.SaveWeergaveChanges(myWeergave);
                    if (Saved != null)
                    {
                        ConfocusMessageBox.Show("Weergave opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                        LoadWeergaveComboBox();
                    }
                    else
                        ConfocusMessageBox.Show("Fout bij het opslaan van de weergave!", ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                Weergave myWeergave = GetWeergaveFromForm();
                if (myWeergave != null)
                {
                    WeergaveInputWindow WW = new WeergaveInputWindow();
                    if (WW.ShowDialog() == true)
                    {
                        myWeergave.Naam = WW.Naam;
                        if (myWeergave != null)
                        {
                            WeergaveServices service = new WeergaveServices();
                            Weergave Saved = service.SaveWeergave(myWeergave);
                            if (Saved != null)
                            {
                                ConfocusMessageBox.Show("Weergave opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                LoadWeergaveComboBox();
                            }
                            else
                                ConfocusMessageBox.Show("Fout bij het opslaan van de weergave!", ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }
            }
        }

        private Weergave GetWeergaveFromForm()
        {
            try
            {
                Boolean IsLeeg = true;
                Weergave weergave = new Weergave();
                weergave.User_ID = UserID;
                if (bedrijvenComboBox.Text != "")
                    IsLeeg = false;
                if (emailComboBox.Text != "")
                    IsLeeg = false;
                if (typeComoBox.Text != "")
                    IsLeeg = false;
                if (niveau1ComboBox.Text != "")
                    IsLeeg = false;
                if (niveau2ComboBox.Text != "")
                    IsLeeg = false;
                if (niveau3ComboBox.Text != "")
                    IsLeeg = false;
                if (LocatieComboBox.Text != "")
                    IsLeeg = false;
                if (ProvincieComboBox.Text != "")
                    IsLeeg = false;
                if (zoekVoorNaamTextBox.Text != "")
                    IsLeeg = false;
                if (aangemaaktDatepicker.SelectedDate != null)
                    IsLeeg = false;

                weergave.Bedrijf = bedrijvenComboBox.Text;
                weergave.Email = emailComboBox.Text;
                weergave.Type = typeComoBox.Text;
                weergave.Niveau1 = niveau1ComboBox.Text;
                weergave.Niveau2 = niveau2ComboBox.Text;
                weergave.Niveau3 = niveau3ComboBox.Text;
                weergave.Gemeente = LocatieComboBox.Text;
                weergave.Provincie = ProvincieComboBox.Text;
                weergave.Zoeknaam = zoekVoorNaamTextBox.Text;
                weergave.Achternaam = zoekAchterNaamTextBox.Text;
                if (aangemaaktDatepicker.SelectedDate != null)
                    weergave.Datum = aangemaaktDatepicker.SelectedDate;

                if (IsLeeg)
                    return null;
                else
                    return weergave;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void WeergavenComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Weergave gekozenWeergave = (Weergave)box.SelectedItem;
                SetFilters(gekozenWeergave);
            }
            else
            {
                MaakFiltersLeeg();
            }
        }

        private void SetFilters(Weergave weergave)
        {
            try
            {
                MaakFiltersLeeg();
                if (weergave.Bedrijf != "")
                    bedrijvenComboBox.Text = weergave.Bedrijf;
                if (weergave.Email != "")
                    emailComboBox.Text = weergave.Email;
                if (weergave.Type != null)
                    typeComoBox.Text = weergave.Type;
                if (weergave.Niveau1 != "")
                    niveau1ComboBox.Text = weergave.Niveau1;
                if (weergave.Niveau2 != "")
                    niveau2ComboBox.Text = weergave.Niveau2;
                if (weergave.Niveau3 != "")
                    niveau3ComboBox.Text = weergave.Niveau3;
                if (weergave.Gemeente != "")
                    LocatieComboBox.Text = weergave.Gemeente;
                if (weergave.Provincie != "")
                    ProvincieComboBox.Text = weergave.Provincie;
                if (weergave.Zoeknaam != "")
                    zoekVoorNaamTextBox.Text = weergave.Zoeknaam;
                if (!string.IsNullOrEmpty(weergave.Achternaam))
                    zoekAchterNaamTextBox.Text = weergave.Achternaam;
                if (weergave.Datum != null)
                    aangemaaktDatepicker.SelectedDate = weergave.Datum;

                ActivateFilters(null);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van de weergave." + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }


        /******************** Detail wijzigingen Contact ***************************/

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Contact contact = ((Functies)contactDataGrid.SelectedItem).Contact;
                    switch (box.Name)
                    {
                        case "voornaamTextBox":
                            contact.Voornaam = box.Text;
                            break;
                        case "achternaamTextBox":
                            contact.Achternaam = box.Text;
                            break;
                        case "notitiesTextBox1":
                            contact.Notities = box.Text;
                            break;
                        default:
                            break;
                    }

                    SaveContactChanges(contact);
                }
            }
        }

        private void SaveContactChanges(Contact contact)
        {
            if (contact != null)
            {
                contact.Gewijzigd = DateTime.Now;
                contact.Gewijzigd_door = currentUser.Naam;

                try
                {
                    ContactServices sevice = new ContactServices();
                    sevice.SaveContactChanges(contact);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void TextBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (IsUserClick)
            {
                if (contactDataGrid.SelectedIndex > -1)
                {
                    ComboBox box = (ComboBox)sender;
                    Contact contact = ((Functies)contactDataGrid.SelectedItem).Contact;
                    switch (box.Name)
                    {
                        case "aansprekingTextBox":
                            if (box.SelectedIndex > -1)
                                contact.Aanspreking = box.SelectedIndex == 0 ? "M" : "V";
                            break;
                        case "typeComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Type = box.SelectedIndex == 0 ? "Klant" : "Prospect";
                            break;
                        case "taalComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Taal_ID = ((Taal)box.SelectedItem).ID;
                            break;
                        case "SecundaireTaalComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Secundaire_Taal_ID = ((Taal)box.SelectedItem).ID;
                            break;
                        case "contactErkenningComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Erkenning_ID = ((Erkenning)box.SelectedItem).ID;
                            break;
                        default:
                            break;
                    }

                    SaveContactChanges(contact);
                }
                IsUserClick = false;
            }
        }

        private void ComboBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsUserClick = true;
        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {

            if ((sender as ComboBox).SelectedIndex > -1 || (sender as ComboBox).Name == "niveau3DetailComboBox")
                IsUserClick = true;
        }

        private void geboortedatumTextBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsUserClick)
            {
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Contact contact = ((Functies)contactDataGrid.SelectedItem).Contact;
                    DatePicker datum = (DatePicker)sender;
                    contact.Geboortedatum = datum.SelectedDate;
                    SaveContactChanges(contact);
                }
                IsUserClick = false;
            }
        }

        private void geboortedatumTextBox_CalendarOpened(object sender, RoutedEventArgs e)
        {
            IsUserClick = true;
        }

        /******************** Functie detail wijzigingen *****************************/

        //private void niveau1DetailComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    //niveau2DetailComboBox.ItemsSource = new List<Niveau2>();
        //    //niveau3DetailComboBox.ItemsSource = new List<Niveau3>();
        //    ComboBox box = (ComboBox)sender;
        //    Niveau1 n1 = (Niveau1)box.SelectedItem;
        //    if (n1 != null)
        //    {
        //        Niveau2Services n2Service = new Niveau2Services();
        //        List<Niveau2> n2Lijst = n2Service.GetSelected(n1.ID);
        //        niveau2DetailComboBox.ItemsSource = n2Lijst;
        //        niveau2DetailComboBox.DisplayMemberPath = "Naam";

        //        if (IsUserClick)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Functies functie = (Functies)contactDataGrid.SelectedItem;
        //                functie.Niveau1_ID = n1.ID;
        //                functie.Niveau1 = n1;
        //                functie.Niveau2 = null;
        //                functie.Niveau2_ID = null;
        //                functie.Niveau3 = null;
        //                functie.Niveau3_ID = null;

        //                SaveFunctieDetailChanges(functie);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //}

        private void SaveFunctieDetailChanges(Functies functie)
        {
            functie.Gewijzigd = DateTime.Now;
            functie.Gewijzigd_door = currentUser.Naam;
            try
            {
                FunctieServices service = new FunctieServices();
                service.SaveFunctieWijzigingen(functie);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan!<n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void niveau2DetailComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    niveau3DetailComboBox.ItemsSource = new List<Niveau3>();
        //    ComboBox box = (ComboBox)sender;
        //    if (box.SelectedIndex > -1)
        //    {
        //        Niveau2 n2 = (Niveau2)box.SelectedItem;
        //        Niveau3Services n3Service = new Niveau3Services();
        //        List<Niveau3> n3Lijst = n3Service.GetSelected(n2.ID);
        //        niveau3DetailComboBox.ItemsSource = n3Lijst;
        //        niveau3DetailComboBox.DisplayMemberPath = "Naam";

        //        if (IsUserClick)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Functies functie = (Functies)contactDataGrid.SelectedItem;
        //                functie.Niveau2 = n2;
        //                functie.Niveau2_ID = n2.ID;
        //                functie.Niveau3 = null;
        //                functie.Niveau3_ID = null;

        //                SaveFunctieDetailChanges(functie);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //}

        //private void niveau3DetailComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        ComboBox box = (ComboBox)sender;
        //        Niveau3 n3 = (Niveau3)box.SelectedItem;
        //        if (n3 != null)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Functies functie = (Functies)contactDataGrid.SelectedItem;
        //                functie.Niveau3 = n3;
        //                functie.Niveau3_ID = n3.ID;

        //                SaveFunctieDetailChanges(functie);
        //            }
        //        }
        //        IsUserClick = false;
        //    }
        //}

        private void functieTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    switch (box.Name)
                    {
                        case "emailTextBox":
                            functie.Email = box.Text;
                            break;
                        case "telefoonTextBox":
                            functie.Telefoon = box.Text;
                            break;
                        case "mobielTextBox":
                            functie.Mobiel = box.Text;
                            break;
                        case "OorspronkelijkeTextBox":
                            functie.Oorspronkelijke_Titel = box.Text;
                            break;
                        default:
                            break;
                    }

                    SaveFunctieDetailChanges(functie);
                }
            }
        }

        private void functieDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsUserClick)
            {
                DatePicker picker = (DatePicker)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    switch (picker.Name)
                    {
                        case "startdatumTextBox":
                            functie.Startdatum = picker.SelectedDate;
                            break;
                        case "einddatumTextBox":
                            functie.Einddatum = picker.SelectedDate;
                            break;
                        default:
                            break;
                    }

                    SaveFunctieDetailChanges(functie);
                }
                IsUserClick = false;
            }
        }

        private void functieDatePicker_CalendarOpened(object sender, RoutedEventArgs e)
        {
            IsUserClick = true;
        }

        //private void mailCheckBox_Click(object sender, RoutedEventArgs e)
        //{
        //    Functies functie = (Functies)contactDataGrid.SelectedItem;
        //    functie.Mail = mailCheckBox.IsChecked == true;
        //    SaveFunctieDetailChanges(functie);
        //}

        //private void faxCheckBox_Click(object sender, RoutedEventArgs e)
        //{
        //    Functies functie = (Functies)contactDataGrid.SelectedItem;
        //    functie.Fax = faxCheckBox.IsChecked == true;
        //    SaveFunctieDetailChanges(functie);
        //}

        /************************** Bedrijf details wijzigingen ********************************/

        private void bedrijfTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
                    switch (box.Name)
                    {
                        case "naamTextBox3":
                            bedrijf.Naam = box.Text;
                            break;
                        case "ondernemersnummerTextBox":
                            bedrijf.Ondernemersnummer = box.Text;
                            break;
                        case "adresTextBox":
                            bedrijf.Adres = box.Text;
                            break;
                        case "telefoonTextBox1":
                            bedrijf.Telefoon = box.Text;
                            break;
                        case "faxTextBox":
                            bedrijf.Fax = box.Text;
                            break;
                        case "emailTextBox1":
                            bedrijf.Email = box.Text;
                            break;
                        case "websiteTextBox":
                            bedrijf.Website = box.Text;
                            break;
                        case "notitiesTextBox":
                            bedrijf.Notities = box.Text;
                            break;
                        default:
                            break;
                    }

                    SaveBedrijfDetailChanges(bedrijf);
                }
            }
        }

        private void SaveBedrijfDetailChanges(Bedrijf bedrijf)
        {
            bedrijf.Gewijzigd = DateTime.Now;
            bedrijf.Gewijzigd_door = currentUser.Naam;
            try
            {
                BedrijfServices service = new BedrijfServices();
                service.SaveBedrijfChanges(bedrijf);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void bedrijfPostcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    PostcodeService service = new PostcodeService();
        //    Postcodes postcode = (Postcodes)bedrijfPostcodeComboBox.SelectedItem;
        //    if (postcode != null)
        //    {
        //        List<Postcodes> gemeentes = service.GetGemeentesByPostcode(postcode.Postcode);
        //        bedrijfPlaatsComboBox.ItemsSource = gemeentes;
        //        bedrijfPlaatsComboBox.DisplayMemberPath = "Gemeente";
        //        bedrijfPlaatsComboBox.SelectedIndex = 0;
        //        landTextBox.Text = postcode.Land;

        //        //if (bedrijfProvincieComboBox.Items.Contains(postcode.Provincie))
        //            bedrijfProvincieComboBox.Text = postcode.Provincie;
        //        //else
        //            //bedrijfProvincieComboBox.SelectedIndex = -1;

        //        if (IsUserClick)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
        //                bedrijf.Postcode = postcode.Postcode;
        //                bedrijf.Plaats = postcode.Gemeente;
        //                bedrijf.Provincie = postcode.Provincie;
        //                bedrijf.Land = postcode.Land;

        //                SaveBedrijfDetailChanges(bedrijf);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //    else
        //    {
        //        bedrijfPostcodeComboBox.SelectedIndex = -1;
        //        bedrijfPlaatsComboBox.SelectedIndex = -1;
        //        bedrijfProvincieComboBox.SelectedIndex = -1;
        //    }
        //}

        private void BedrijfComboBox_DropDownOpened(object sender, EventArgs e)
        {
            IsUserClick = true;
        }

        //private void bedrijfPlaatsComboBox_SizeChanged(object sender, SizeChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        if (contactDataGrid.SelectedIndex > -1)
        //        {
        //            Postcodes postcode = (Postcodes)bedrijfPlaatsComboBox.SelectedItem;

        //            Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
        //            bedrijf.Plaats = postcode.Gemeente;

        //            SaveBedrijfDetailChanges(bedrijf);
        //        }
        //        IsUserClick = false;
        //    }
        //}

        //private void bedrijfProvincieComboBox_SizeChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        Postcodes postcode = (Postcodes)bedrijfPostcodeComboBox.SelectedItem;
        //        if (bedrijfProvincieComboBox.SelectedIndex > -1)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                String provincie = ((ComboBoxItem)bedrijfProvincieComboBox.SelectedItem).Content.ToString();
        //                Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
        //                bedrijf.Provincie = provincie;

        //                if (postcode.Provincie == null)
        //                {
        //                    postcode.Provincie = provincie;
        //                    PostcodeService postService = new PostcodeService();
        //                    postService.ChangePostcode(postcode);
        //                }


        //                SaveBedrijfDetailChanges(bedrijf);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //}

        private void contactDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                if (contactDataGrid.SelectedIndex < (contactDataGrid.Items.Count - 1))
                {
                    NextButton_Click(contactDataGrid, new RoutedEventArgs());
                }
            }
            else if (e.Key == Key.Up)
            {
                if (contactDataGrid.SelectedIndex > 0)
                {
                    PrevButton_Click(contactDataGrid, new RoutedEventArgs());
                }
            }
        }

        private void OpenSiteButton_Click(object sender, RoutedEventArgs e)
        {
            //if (websiteTextBox.Text != "")
            //{
            //    String url = websiteTextBox.Text;
            //    try
            //    {
            //        System.Diagnostics.Process.Start(url);
            //    }
            //    catch (Exception ex)
            //    {
            //        ConfocusMessageBox.Show("Fout bij het openen van de website!\nControleer of de URL correct is en probeer opnieuw.\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            //    }
            //}
        }

        private void OpenMailButton_Click(object sender, RoutedEventArgs e)
        {
            //if (emailTextBox1.Text != "")
            //{
            //    String url = "mailto:'" + emailTextBox1.Text + "'";
            //    try
            //    {
            //        System.Diagnostics.Process.Start(url);
            //    }
            //    catch (Exception ex)
            //    {
            //        ConfocusMessageBox.Show("Fout bij het openen van het email programma!\nControleer of het adres correct is en probeer opnieuw.\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            //    }
            //}
        }

        private void ImportContactMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel IE = new ImportExcel(currentUser);
            IE.Show();
            IE.Activate();
        }

        private void KalenderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Kalender kalender = new Kalender(currentUser);
            kalender.Show();
            kalender.Activate();
        }

        private void MarketingkanaalMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Markeningkanalen MK = new Markeningkanalen(currentUser);
            MK.Show();
            MK.Activate();
        }

        private void ImportMarketingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportMarketing IM = new ImportMarketing(currentUser);
            IM.Show();
            IM.Activate();
        }

        private void AttesttypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AttesttypeBeheer AB = new AttesttypeBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }

        private void contactDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.Target is System.Windows.Controls.TextBlock
                || e.MouseDevice.Target is System.Windows.Controls.TextBox
                || e.MouseDevice.Target is System.Windows.Controls.Border)
            {
                Functies functie = (Functies)contactDataGrid.SelectedItem;
                NieuwContact BC = new NieuwContact(currentUser, functie);
                if (BC.ShowDialog() == true)
                {
                    try
                    {
                        ReLoadContacten(functie);
                        contactDataGrid.SelectedItem = functie;
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message + "\nCloseNieuwContact module", ConfocusMessageBox.Kleuren.Rood);
                    }
                }

            }
        }

        private void contactDetailHideButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Toon details")
            {
                button.Content = "Verberg details";
                contactDetailRow.Height = new GridLength(300);
            }
            else
            {
                button.Content = "Toon details";
                contactDetailRow.Height = new GridLength(30);
            }
        }

        private void meerButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Meer")
            {
                button.Content = "Minder";
                searchPanel.Height = new GridLength(120);
            }
            else
            {
                button.Content = "Meer";
                searchPanel.Height = new GridLength(65);
            }
        }

        private void aangemaaktDatepicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ActivateFilters(null);
        }

        private void DagTypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DagtypeBeheer DB = new DagtypeBeheer(currentUser);
            DB.Show();
            DB.Activate();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            ShowSearchWindow();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            SpeedtestWindow SW = new SpeedtestWindow();
            SW.Show();
            //FunctieServices fService = new FunctieServices();
            //fService.SetProspect();
        }

        private void FlexmailWeergaveButton_Click(object sender, RoutedEventArgs e)
        {
            List<Functies> functies = new List<Functies>();
            foreach (Functies func in contactDataGrid.Items)
            {
                if (!string.IsNullOrEmpty(func.Email) && !func.ContainsContact(functies))
                    functies.Add(func);
            }
            FlexmailActieWindows FW = new FlexmailActieWindows(functies);
            FW.Show();
            FW.Activate();
        }

        private void ZoekBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZB = new ZoekContactWindow("Bedrijf");
            if (ZB.ShowDialog() == true)
            {
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                Bedrijf bedrijf = new Bedrijf();
                bedrijf.Naam = "Kies";
                bedrijven.Add(bedrijf);
                bedrijven.Add(ZB.bedrijf);
                bedrijvenComboBox.ItemsSource = bedrijven;
                bedrijvenComboBox.DisplayMemberPath = "Naam";
                bedrijvenComboBox.SelectedIndex = 1;
            }
        }

        private void faxnummerTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
                ActivateFilters(null);
        }

        private void SpecialeTekensMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SpecialeKarakters SK = new SpecialeKarakters();
                SK.Show();
                SK.Activate();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ZoekEmailButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekEmailInvoerWindow EW = new ZoekEmailInvoerWindow();
            if (EW.ShowDialog() == true)
            {
                List<string> emailoptions = new List<string>();
                emailoptions.Add("Kies");
                emailoptions.Add("Leeg");
                emailoptions.Add(EW.Emailadres);

                emailComboBox.ItemsSource = emailoptions;
                emailComboBox.SelectedIndex = 2;
            }
        }

        private void WeergaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ContactWeergaveBeheer CW = new ContactWeergaveBeheer(currentUser);
            if (CW.ShowDialog() == true)
            {
                LoadWeergaveComboBox();
            }
        }

        private void XMLCreatorMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //C:\Confocus XML Creator/ConfocusXMLCreator.exe
            Process.Start(@"C:\Confocus XML Creator/ConfocusXMLCreator.exe");
        }

        private void FixMenuItem_Click(object sender, RoutedEventArgs e)
        {
            BedrijfServices bService = new BedrijfServices();
            FunctieServices fservice = new FunctieServices();
            List<Bedrijf> bedrijven = bService.FindActive();
            int counter = 0;
            var faxnummergroup = (from b in bedrijven where !string.IsNullOrEmpty(b.Fax) group b by b.Fax into g orderby g.Key select new { Fax = g.Key, bedrijf = g.ToList() }).ToList();
            foreach (var bedrijvengroep in faxnummergroup)
            {

                foreach (var bedrijf in bedrijvengroep.bedrijf)
                {
                    List<Functies> functions = fservice.GetFunctiesInBedrijf(bedrijf.ID);
                    bool fax = true;
                    foreach (Functies func in functions)
                    {
                        if (func.Fax == false)
                        {
                            fax = false;
                            break;
                        }
                    }
                    if (fax == false)
                    {
                        foreach (Functies func in functions)
                        {
                            if (func.Fax == true)
                            {
                                func.Fax = false;
                                fservice.SaveFunctieWijzigingen(func);
                                counter++;
                            }
                            
                        }
                    }
                }
            }
            MessageBox.Show(counter + " contacten aangepast");
        }

        public void ShowSearchWindow()
        {
            ReplaceEmailWindow RW = new ReplaceEmailWindow(currentUser);
            if (RW.ShowDialog() == true)
            {
                if (contactDataGrid.Items.Count > 0)
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    ActivateFilters(functie);
                }

            }
            //ZoekVervangWindow ZW = new ZoekVervangWindow();
            //if (ZW.ShowDialog() == true)
            //{
            //    ReplaceEmail mail = ZW.replaceEmail;
            //    FunctieServices fService = new FunctieServices();
            //    int aantal = fService.ReplaceEmail(mail);
            //    ConfocusMessageBox.Show(aantal + " emailadressen vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            //    ReLoadContacten(null);
            //    //ActivateFilters();
            //}
        }

        private void AttestTemplateMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AttestTemplateBeheer AB = new AttestTemplateBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }
    }
}

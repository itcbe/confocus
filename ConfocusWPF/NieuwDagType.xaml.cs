﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwDagType.xaml
    /// </summary>
    public partial class NieuwDagType : Window
    {
        private Gebruiker currentUser;

        public NieuwDagType(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            naamTextBox.Focus();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            DagType dagtype = GetdagTypeFromForm();
            if (IsDagTypeValid(dagtype))
            {
                try
                {
                    DagType_Services dService = new DagType_Services();
                    DagType savedDagType = dService.Save(dagtype);
                    DialogResult = true;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan van het dagtype!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private bool IsDagTypeValid(DagType dagtype)
        {

            bool valid = true;
            String errortext = "";
            if (dagtype.Naam == "")
            {
                errortext += "Geen naam ingegeven!";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private DagType GetdagTypeFromForm()
        {
            DagType type = new DagType();
            type.Naam = naamTextBox.Text;
            type.Actief = true;
            type.Aangemaakt = DateTime.Now;
            type.Aangemaakt_door = currentUser.Naam;

            return type;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }


    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ZoekSprekerWindow.xaml
    /// </summary>
    public partial class ZoekSprekerWindow : Window
    {
        public Spreker Spreker { get; set; }
        public ZoekSprekerWindow()
        {
            InitializeComponent();
            VoornaamTextBox.Focus();
        }

        private void NaamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ZoekSpreker();
            }
        }

        private void ZoekSpreker()
        {
            ContactZoekTerm searchterm = new ContactZoekTerm();
            searchterm.Voornaam = VoornaamTextBox.Text;
            searchterm.Achternaam = AchternaamTextBox.Text;
            if (!string.IsNullOrEmpty(searchterm.Voornaam) || !string.IsNullOrEmpty(searchterm.Achternaam))
            {
                SprekerServices sService = new SprekerServices();
                List<Spreker> sprekers = sService.GetSprekersByZoekterm(searchterm);
                resultsListBox.ItemsSource = sprekers;
                resultsListBox.DisplayMemberPath = "NaamEnNiveaus";
                resultLabel.Content = sprekers.Count + " sprekers gevonden.";
            }
        }

        private void zoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekSpreker();
        }

        private void resultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.SelectedIndex > -1)
            {
                this.Spreker = (Spreker)box.SelectedItem;
                DialogResult = true;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwNiveau3.xaml
    /// </summary>
    public partial class NieuwNiveau3 : Window
    {
        private Gebruiker currentUser;

        public NieuwNiveau3(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            naamTextBox.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Niveau2Services service = new Niveau2Services();
            List<Niveau2> N2Lijst = service.FindAllComboBox();
            naamComboBox.ItemsSource = N2Lijst;
            naamComboBox.DisplayMemberPath = "Niveau1En2Namen";
        }

        private void annuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Niveau3 N3 = GetNiveau3FromForm();
            if (N3 != null)
            {
                Niveau3Services service = new Niveau3Services();
                service.SaveNiveau3(N3);
                DialogResult = true;
            }
            
        }

        private Niveau3 GetNiveau3FromForm()
        {
            if (IsFormValid())
            {
                Niveau3 N3 = new Niveau3();
                N3.Naam = naamTextBox.Text;
                N3.Naam_FR = beschrijvingTextBox.Text;
                if (naamComboBox.SelectedIndex > -1)
                    N3.Niveau2_ID = ((Niveau2)naamComboBox.SelectedItem).ID;
                N3.Actief = actiefCheckBox.IsChecked == true;
                N3.Aangemaakt = DateTime.Now;
                N3.Aangemaakt_door = currentUser.Naam;

                return N3;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "Er is geen naam ingegeven!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);
            if (naamComboBox.Text == "")
            {
                errorMessage += "Er is geen niveau 2 gekozen!\n";
                valid = false;
            }
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }
    }
}

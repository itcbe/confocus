﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ReplaceSpeakerEmailWindow.xaml
    /// </summary>
    public partial class ReplaceSpeakerEmailWindow : Window
    {
        private System.Windows.Data.CollectionViewSource sprekerViewSource;
        private string _toReplace = "";
        private string _toSearch = "";
        private Gebruiker currentUser;
        private List<Spreker> verwerkteSprekers = new List<Spreker>();

        public ReplaceSpeakerEmailWindow(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            sprekerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sprekerViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // sprekerViewSource.Source = [generic data source]
            ZoekTextBox.Focus();
        }
        private void SetAantalLabel()
        {
            aantalLabel.Content = (sprekerDataGrid.SelectedIndex + 1) + " van " + (sprekerDataGrid.Items.Count);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            verwerkteSprekers.Clear();
            FilterSprekers(0);
        }

        private void FilterSprekers(int index)
        {
            if (!string.IsNullOrEmpty(ZoekTextBox.Text))
            {
                _toReplace = VervangTextBox.Text;
                _toSearch = ZoekTextBox.Text;
                SprekerServices sService = new SprekerServices();
                List<Spreker> sprekers = sService.GetReplaceEmail(_toSearch);
                List<Spreker> gefilterde = new List<Spreker>();
                foreach (Spreker spreker in sprekers)
                {
                    if (!verwerkteSprekers.Contains(spreker))
                        gefilterde.Add(spreker);
                }
                sprekerViewSource.Source = gefilterde;
                if (index > 0)
                {
                    sprekerDataGrid.SelectedIndex = index;
                    sprekerDataGrid.ScrollIntoView(sprekerDataGrid.SelectedItem);
                }

                emailTextBox1.Focus();
            }
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                if (emailTextBox1.IsFocused == true)
                {
                    emailTextBox.Focus();
                }
                else if (emailTextBox.IsFocused == true)
                {
                    //int index = functiesDataGrid.SelectedIndex;
                    //if (index < functiesDataGrid.Items.Count)
                    //{
                    //functiesDataGrid.SelectedIndex = ++index;
                    verwerkteSprekers.Add((Spreker)sprekerDataGrid.SelectedItem);
                    FilterSprekers(sprekerDataGrid.SelectedIndex);
                    //}

                    emailTextBox1.Focus();
                }
            }
            else if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                e.Handled = true;
                Spreker spreker = (Spreker)sprekerDataGrid.SelectedItem;
                if (emailTextBox1.IsFocused == true)
                {
                    string replaced = ReplaceBedrijfsEmail(spreker, _toReplace);
                    emailTextBox1.Text = replaced;
                    emailTextBox.Focus();
                }
                else if (emailTextBox.IsFocused == true)
                {
                    ReplaceSprekerEmail(spreker, _toReplace);
                    //int index = functiesDataGrid.SelectedIndex;
                    //if (index < functiesDataGrid.Items.Count)
                    //{
                    //functiesDataGrid.SelectedIndex = ++index;
                    FilterSprekers(sprekerDataGrid.SelectedIndex);
                    //functiesDataGrid.ScrollIntoView(functiesDataGrid.SelectedItem);
                    //}

                    emailTextBox1.Focus();
                }
            }
        }

        private string ReplaceSprekerEmail(Spreker spreker, string _toReplace)
        {
            if (spreker != null)
            {
                SprekerServices sService = new SprekerServices();
                string email = spreker.Email.ToLower();
                string replaced = "";
                if (!string.IsNullOrEmpty(email))
                {
                    replaced = email.Replace(_toSearch.ToLower(), _toReplace.ToLower());
                }
                spreker.Email = replaced;
                spreker.Gewijzigd = DateTime.Now;
                spreker.Gewijzigd_door = currentUser.Naam;
                sService.SaveSprekerWijzigingen(spreker);
                return replaced;
            }
            else
                return "";
        }

        private string ReplaceBedrijfsEmail(Spreker spreker, string _toReplace)
        {
            if (spreker != null)
            {
                BedrijfServices bService = new BedrijfServices();
                string email = spreker.Bedrijf.Email.ToLower();
                string replaced = "";
                if (!string.IsNullOrEmpty(email))
                {
                    replaced = email.Replace(_toSearch.ToLower(), _toReplace.ToLower());
                }
                Bedrijf bedrijf = spreker.Bedrijf;
                bedrijf.Email = replaced;
                bedrijf.Gewijzigd = DateTime.Now;
                bedrijf.Gewijzigd_door = currentUser.Naam;
                bService.SaveBedrijfChanges(bedrijf);
                return replaced;
            }
            else
                return "";
        }

        private void VervangTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _toReplace = VervangTextBox.Text;
        }

        private void ZoekTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _toSearch = ZoekTextBox.Text;
        }

        private void ChangeAllButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Spreker spreker in sprekerDataGrid.Items)
                {
                    ReplaceBedrijfsEmail(spreker, _toReplace);
                    ReplaceSprekerEmail(spreker, _toReplace);
                }
                sprekerViewSource.Source = new List<Spreker>();
                ConfocusMessageBox.Show("Alle functies zijn vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ChangeAllCompanyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Spreker spreker in sprekerDataGrid.Items)
                {
                    ReplaceBedrijfsEmail(spreker, _toReplace);
                }
                sprekerViewSource.Source = new List<Spreker>();
                ConfocusMessageBox.Show("Alle functies zijn vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ChangeAllFunctionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                 foreach (Spreker spreker in sprekerDataGrid.Items)
                {
                    //ReplaceBedrijfsEmail(func, _toReplace);
                    ReplaceSprekerEmail(spreker, _toReplace);
                }
                sprekerViewSource.Source = new List<Spreker>();
                ConfocusMessageBox.Show("Alle spreker zijn vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void switchButton_Click(object sender, RoutedEventArgs e)
        {
            string replace = _toReplace;
            string zoek = _toSearch;
            ZoekTextBox.Text = replace;
            VervangTextBox.Text = zoek;
        }

    }
}

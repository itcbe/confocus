﻿using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ImportMarketing.xaml
    /// </summary>
    public partial class ImportMarketing : Window
    {
        private CollectionViewSource marketingkanaalViewSource;
        private List<Marketingkanalen> kanalen = new List<Marketingkanalen>();
        private Gebruiker currentUser;

        public ImportMarketing(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            marketingkanaalViewSource = ((CollectionViewSource)(this.FindResource("marketingkanaalViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // marketingkanaalViewSource.Source = [generic data source]
        }

        private void OpenExcelButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            if (openDialog.ShowDialog() == true)
            {

                String filename = openDialog.FileName;
                ImportMarketingKanalen IPK = new ImportMarketingKanalen(currentUser);
                kanalen = IPK.readExcel(filename);
                marketingkanaalViewSource.Source = kanalen;
            }
        }

        private void SaveListButton_Click(object sender, RoutedEventArgs e)
        {
            if (kanalen.Count > 0)
            {
                MarketingkanalenServices kService = new MarketingkanalenServices();
                foreach (Marketingkanalen kanaal in kanalen)
                {
                    kanaal.Gemaakt = DateTime.Now;
                    kanaal.Gemaakt_door = currentUser.Naam;
                    kanaal.Actief = true;
                    if (!string.IsNullOrEmpty(kanaal.Marketing_kanaal))
                        kService.AddMarketingkanaal(kanaal);
                }
            }
        }
    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for Splash.xaml
    /// </summary>
    public partial class Splash : Window
    {
        private Gebruiker currentUser;

        public Splash()
        {
            InitializeComponent();
            emailTextBox.Focus();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            LogIn();
        }

        private void LogIn()
        {
            if (CheckLogin())
            {
                MainWindow MW = new MainWindow(currentUser);
                MW.Show();
                this.Close();
            }
            else
            {
                ConfocusMessageBox.Show("De log in gegevens zijn niet correct.", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private bool CheckLogin()
        {
            try
            {
                Encriptor encript = new Encriptor();
                GebruikerService service = new GebruikerService();
                String email = emailTextBox.Text;
                String password = encript.EncodePassword(paswoordBox.Password);

                Gebruiker gebruiker = service.GetUserWithEmailAndPassword(email, password);
                if (gebruiker != null)
                {
                    currentUser = gebruiker;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het inloggen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                return false;
            }
            //return true;     
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                LogIn();
        }
    }
}

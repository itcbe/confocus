﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweAanspreking.xaml
    /// </summary>
    public partial class NieuweAanspreking : Window
    {
        private Gebruiker currentUser;

        public NieuweAanspreking(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            TaalServices taalservice = new TaalServices();
            List<Taal> talen = taalservice.FindAll();
            naamComboBox.ItemsSource = talen;
            naamComboBox.DisplayMemberPath = "Naam";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Aanspreking aanspreking = GetAansprekingFromForm();
            if (aanspreking != null)
            {
                AansprekingServices service = new AansprekingServices();
                service.SaveAanspreking(aanspreking);
                DialogResult = true;
            }
        }

        private Aanspreking GetAansprekingFromForm()
        {
            if (IsFormValid())
            {
                Aanspreking aans = new Aanspreking();
                aans.Aanspreking_man = aanspreking_manTextBox.Text;
                aans.Aanspreking_vrouw = aanspreking_vrouwTextBox.Text;
                if (naamComboBox.SelectedIndex > -1)
                    aans.Taal_ID = ((Taal)naamComboBox.SelectedItem).ID;
                aans.Aangemaakt = DateTime.Now;
                aans.Aangemaakt_door = currentUser.Naam;

                return aans;
            }
            else
            {
                return null;
            }
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (aanspreking_manTextBox.Text == "")
            {
                errorMessage += "Aanspreking man niet ingevuld!\n";
                Helper.SetTextBoxInError(aanspreking_manTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(aanspreking_manTextBox, normalTemplate);

            if (aanspreking_vrouwTextBox.Text == "")
            {
                errorMessage += "Aanspreking vrouw niet ingevuld!\n";
                Helper.SetTextBoxInError(aanspreking_vrouwTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(aanspreking_vrouwTextBox, normalTemplate);

            if (naamComboBox.SelectedIndex == -1)
            {
                errorMessage += "Geen taal gekozen!";
                valid = false;
            }
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for HumanResourceWindow.xaml
    /// </summary>
    public partial class HumanResourceWindow : Window
    {
        private Gebruiker _curentUser;
        private List<Functies> BestaandeLijst = new List<Functies>();
        private List<Functies> NieuweLijst = new List<Functies>();
        private System.Windows.Data.CollectionViewSource functiesViewSource;
        private System.Windows.Data.CollectionViewSource functies2ViewSource;

        public HumanResourceWindow(Gebruiker user)
        {
            InitializeComponent();
            _curentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            functiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functiesViewSource")));
            functies2ViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functies2ViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // functiesViewSource.Source = [generic data source]
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ophaalButton_Click(object sender, RoutedEventArgs e)
        {
            BestaandeLijst = new FunctieServices().getHRMFuncties();
            functiesViewSource.Source = BestaandeLijst;
            aantalLabel.Content = BestaandeLijst.Count + " gevonden.";
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            FunctieServices fService = new FunctieServices();
            int nieuweContacten = 0;
            foreach (Functies item in NieuweLijst)
            {
                item.Aangemaakt = DateTime.Now;
                item.Aangemaakt_door = _curentUser.Naam;
                try
                {
                    fService.SaveFunctie(item);
                    nieuweContacten++;
                }
                catch (Exception)
                {
                    continue;
                }
                
            }
            ConfocusMessageBox.Show(nieuweContacten + " toegevoegd.", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void verwerkButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweLijst.Clear();
            foreach (Functies item in BestaandeLijst)
            {
                Functies newFunctie = new Functies();
                newFunctie.Contact_ID = item.Contact_ID;
                newFunctie.Bedrijf_ID = item.Bedrijf_ID;
                newFunctie.Niveau1_ID = 20;

                if (item.Niveau2 != null)
                {
                    if (item.Niveau2.Naam.ToLower().Contains("verantwoordelijke personeel"))
                        newFunctie.Niveau2_ID = 259;
                    else if (item.Niveau2.Naam.ToLower().Contains("verantwoordelijke opleiding"))
                        newFunctie.Niveau2_ID = 260;
                }

                if (item.Niveau3 != null)
                {
                    if (item.Niveau3.Naam.ToLower().Contains("verantwoordelijke personeel"))
                        newFunctie.Niveau2_ID = 259;
                    else if (item.Niveau3.Naam.ToLower().Contains("verantwoordelijke opleiding"))
                        newFunctie.Niveau2_ID = 260;
                }

                newFunctie.Email = item.Email;
                newFunctie.Telefoon = item.Telefoon;
                newFunctie.Mobiel = item.Mobiel;
                newFunctie.Erkenning_ID = item.Erkenning_ID;
                newFunctie.Mail = item.Mail;
                newFunctie.Fax = item.Fax;
                newFunctie.Startdatum = item.Startdatum;
                newFunctie.Einddatum = item.Einddatum;
                newFunctie.Actief = item.Actief;
                newFunctie.Aangemaakt = DateTime.Now;
                newFunctie.Aangemaakt_door = _curentUser.Naam;
                newFunctie.Oorspronkelijke_Titel = item.Oorspronkelijke_Titel;
                newFunctie.Erkenningsnummer = item.Erkenningsnummer;
                newFunctie.Attesttype = item.Attesttype;
                newFunctie.Type = item.Type;
                newFunctie.Attesttype_ID = item.Attesttype_ID;

                NieuweLijst.Add(newFunctie);


            }

            functies2ViewSource.Source = NieuweLijst;
        }
    }
}

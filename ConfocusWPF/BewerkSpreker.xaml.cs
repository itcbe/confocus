﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for BewerkSpreker.xaml
    /// </summary>
    public partial class BewerkSpreker : Window
    {
        private Spreker spreker;
        private Gebruiker currentUser;
        private List<Postcodes> postcodes = new List<Postcodes>();

        public BewerkSpreker(Gebruiker user, Spreker sprek)
        {
            InitializeComponent();
            this.currentUser = user;
            this.spreker = sprek;
            this.Title = "Bewerk spreker. Ingelogd als " + currentUser.Naam;
        }

        private void SetData()
        {
            SprekerServices sService = new SprekerServices();
            Spreker fullSpreker = sService.GetSprekerById(spreker.ID);
            //Contact
            Contact myContact = fullSpreker.Contact;
            if (myContact != null)
            {
                try
                {
                    voornaamTextBox.Text = myContact.Voornaam;
                    achternaamTextBox.Text = myContact.Achternaam;
                    if (myContact.Geboortedatum != null)
                    {
                        geboortedatumDatePicker.Text = ((DateTime)myContact.Geboortedatum).ToShortDateString();
                        geboortedatumDatePicker.SelectedDate = (DateTime)myContact.Geboortedatum;
                        geboortedatumDatePicker.DisplayDate = (DateTime)myContact.Geboortedatum;
                    }
                    aansprekingComboBox.Text = myContact.Aanspreking;
                    TaalServices taalservice = new TaalServices();
                    Taal taal = taalservice.GetTaalByID((decimal)myContact.Taal_ID);
                    if (taal != null)
                        taalComboBox.Text = taal.Naam;
                    if (myContact.Secundaire_Taal_ID != null)
                    {
                        Taal secundaire = taalservice.GetTaalByID((decimal)myContact.Secundaire_Taal_ID);
                        if (secundaire != null)
                            secundaireTaalComboBox.Text = secundaire.Naam;
                    }
                    if (myContact.Erkenning_ID != null)
                    {
                        ErkenningServices eService = new ErkenningServices();
                        Erkenning erkenning = eService.GetErkenningByID(myContact.Erkenning_ID);
                        if (erkenning != null)
                            contactErkenningComboBox.Text = erkenning.Naam;
                    }
                    typeComboBox.Text = myContact.Type;
                    notitiesTextBox.Text = myContact.Notities;
                    actiefCheckBox.IsChecked = myContact.Actief;

                }
                catch (Exception ex)
                {
                    //ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);                  
                }
            }

            //Bedrijf
            Bedrijf myBedrijf = fullSpreker.Bedrijf;
            if (myBedrijf != null)
            {
                idLabel.Content = myBedrijf.ID;
                naamTextBox.Text = myBedrijf.Naam;
                ondernemersnummerTextBox.Text = myBedrijf.Ondernemersnummer;
                adresTextBox.Text = myBedrijf.Adres;
                postcodeComboBox.Text = myBedrijf.Postcode;
                plaatsComboBox.Text = myBedrijf.Plaats;
                provincieComboBox.Text = myBedrijf.Provincie;
                landTextBox.Text = myBedrijf.Land;
                telefoonTextBox.Text = myBedrijf.Telefoon;
                faxTextBox.Text = myBedrijf.Fax;
                websiteTextBox.Text = myBedrijf.Website;
                emailTextBox.Text = myBedrijf.Email;
                notitiesTextBox1.Text = myBedrijf.Notities;
                actiefCheckBox1.IsChecked = myBedrijf.Actief;
            }

            //Spreker
            if (fullSpreker != null)
            {
                if (fullSpreker.Niveau1 != null)
                    niveau1ComboBox1.Text = fullSpreker.Niveau1.Naam;
                if (fullSpreker.Niveau2 != null)
                    niveau2ComboBox2.Text = fullSpreker.Niveau2.Naam;
                if (fullSpreker.Niveau3 != null)
                    niveau3ComboBox3.Text = fullSpreker.Niveau3.Naam;
                tariefTextBox.Text = String.Format("{0:N2}", fullSpreker.Tarief);
                sprekerEmailTextBox.Text = fullSpreker.Email;
                sprekerTelefoonTextBox.Text = fullSpreker.Telefoon;
                sprekerMobielTextBox.Text = fullSpreker.Mobiel;
                //onderwerpTextBox.Text = spreker.Onderwerp;
                waarderingComboBox.Text = fullSpreker.Waardering.ToString();
                CVTextBox.Text = fullSpreker.CV;
                actiefCheckBox2.IsChecked = fullSpreker.Actief;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OpenCVButton_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://confocusbvba.sharepoint.com/cck/_layouts/15/start.aspx#/Gedeelde%20%20documenten/Forms/AllItems.aspx?RootFolder=%2Fcck%2FGedeelde%20%20documenten%2FCV%20%2D%20Spreker&FolderCTID=0x012000AAC3CED665DBCF408B736BCC81C7FFB2&View=%7BCF22F2E5%2D0182%2D4B10%2D863A%2D3B963CCBEF23%7D");

            //OpenFileDialog OpenCV = new OpenFileDialog();
            //if (OpenCV.ShowDialog() == true)
            //{
            //    CVTextBox.Text = OpenCV.FileName;
            //}
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //contact opslaan
            Contact contact = GetContactFromForm();
            if (contact != null)
            {
                try
                {
                    ContactServices service = new ContactServices();
                    service.SaveContactChanges(contact);
                    ConfocusMessageBox.Show("Contact opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Contact niet opgeslagen!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Contact GetContactFromForm()
        {
            if (IsContactFormValid())
            {
                Contact contact = new Contact();
                contact.ID = spreker.Contact_ID;
                contact.Voornaam = voornaamTextBox.Text;
                contact.Achternaam = achternaamTextBox.Text;
                contact.Aanspreking = aansprekingComboBox.Text;
                if (taalComboBox.SelectedIndex > -1)
                    contact.Taal_ID = ((Taal)taalComboBox.SelectedItem).ID;
                if (secundaireTaalComboBox.SelectedIndex > -1)
                    contact.Secundaire_Taal_ID = ((Taal)secundaireTaalComboBox.SelectedItem).ID;
                if (contactErkenningComboBox.SelectedIndex > -1)
                    contact.Erkenning_ID = ((Erkenning)contactErkenningComboBox.SelectedItem).ID;
                contact.Notities = notitiesTextBox.Text;
                contact.Geboortedatum = geboortedatumDatePicker.SelectedDate;
                if (typeComboBox.SelectedIndex > -1)
                    contact.Type = typeComboBox.Text;
                contact.Gewijzigd = DateTime.Now;
                contact.Gewijzigd_door = currentUser.Naam;
                contact.Actief = actiefCheckBox.IsChecked;

                return contact;
            }
            else
                return null;
        }

        private bool IsContactFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (voornaamTextBox.Text == "")
            {
                errorMessage += "De voornaam is niet ingevuld!\n";
                Helper.SetTextBoxInError(voornaamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(voornaamTextBox, normalTemplate);
            if (achternaamTextBox.Text == "")
            {
                errorMessage += "De achternaam is niet ingevuld!\n";
                Helper.SetTextBoxInError(achternaamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(achternaamTextBox, normalTemplate);
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Opslaan bedrijf
            Bedrijf bedrijf = GetBedrijfFromForm();
            if (bedrijf != null)
            {
                try
                {
                    BedrijfServices service = new BedrijfServices();
                    service.SaveBedrijfChanges(bedrijf);
                    ConfocusMessageBox.Show("Bedrijf opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Bedrijf niet opgeslagen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }

            }
        }

        private Bedrijf GetBedrijfFromForm()
        {
            if (IsBedrijfFormValid())
            {
                Bedrijf bedrijf = new Bedrijf();
                bedrijf.ID = (Decimal)spreker.Bedrijf_ID;
                bedrijf.Naam = naamTextBox.Text;
                bedrijf.Ondernemersnummer = ondernemersnummerTextBox.Text;
                bedrijf.Adres = adresTextBox.Text;
                if (postcodeComboBox.SelectedIndex > -1)
                    bedrijf.Postcode = postcodeComboBox.Text;
                if (plaatsComboBox.SelectedIndex > -1)
                    bedrijf.Plaats = plaatsComboBox.Text;
                if (provincieComboBox.SelectedIndex > -1)
                    bedrijf.Provincie = provincieComboBox.Text;
                bedrijf.Land = landTextBox.Text;
                bedrijf.Telefoon = telefoonTextBox.Text;
                bedrijf.Fax = faxTextBox.Text;
                bedrijf.Website = websiteTextBox.Text;
                bedrijf.Email = emailTextBox.Text;
                bedrijf.Notities = notitiesTextBox.Text;
                bedrijf.Actief = actiefCheckBox1.IsChecked == true;
                bedrijf.Gewijzigd = DateTime.Now;
                bedrijf.Gewijzigd_door = currentUser.Naam;

                return bedrijf;
            }
            else
                return null;
        }

        private bool IsBedrijfFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "Geen naam ingevuld!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            //spreker opslaan
            Spreker mySpreker = GetSprekerFromForm();
            if (mySpreker != null)
            {
                SprekerServices service = new SprekerServices();
                Spreker savedSpreker = service.SaveSprekerWijzigingen(mySpreker);
                if (savedSpreker != null)
                {
                    ConfocusMessageBox.Show("Spreker wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                }
                else
                {
                    ConfocusMessageBox.Show("Wijzigingen niet opgeslagen!", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Spreker GetSprekerFromForm()
        {
            if (IsSprekerValid())
            {
                Spreker mySpreker = new Spreker();
                mySpreker.Tarief = tariefTextBox.Text;
                mySpreker.ID = spreker.ID;
                mySpreker.Contact_ID = spreker.Contact_ID;
                mySpreker.Bedrijf_ID = spreker.Bedrijf_ID;
                mySpreker.Niveau1_ID = ((Niveau1)niveau1ComboBox1.SelectedItem).ID;
                if (niveau2ComboBox2.SelectedIndex > -1)
                    mySpreker.Niveau2_ID = ((Niveau2)niveau2ComboBox2.SelectedItem).ID;
                if (niveau3ComboBox3.SelectedIndex > -1)
                    mySpreker.Niveau3_ID = ((Niveau3)niveau3ComboBox3.SelectedItem).ID;
                if (erkenningTextBox1.SelectedIndex > -1)
                    mySpreker.Erkenning_ID = ((Erkenning)erkenningTextBox1.SelectedItem).ID;
                if (waarderingComboBox.SelectedIndex > -1)
                    mySpreker.Waardering = Int32.Parse(waarderingComboBox.Text);
                //mySpreker.Onderwerp = onderwerpTextBox.Text;
                mySpreker.Email = sprekerEmailTextBox.Text;
                mySpreker.Telefoon = sprekerTelefoonTextBox.Text;
                mySpreker.Mobiel = sprekerMobielTextBox.Text;
                mySpreker.CV = CVTextBox.Text;
                mySpreker.Actief = actiefCheckBox2.IsChecked == true;
                mySpreker.Gewijzigd = DateTime.Now;
                mySpreker.Gewijzigd_door = currentUser.Naam;

                return mySpreker;
            }
            else return null;

        }

        private bool IsSprekerValid()
        {
            Boolean valid = true;
            String errortext = "";
            if (niveau1ComboBox1.SelectedIndex == -1)
            {
                errortext += "Geen niveau 1 geselecteerd!\n";
                valid = false;
            }
            if (waarderingComboBox.SelectedIndex > -1)
            {
                Int32 waarde = 0;
                if (!Int32.TryParse(waarderingComboBox.Text, out waarde))
                {
                    errortext += "Waardering is geen geldig getal!\n";
                    valid = false;
                }
            }

            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void niveau1ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (niveau1ComboBox1.SelectedIndex > -1)
            {
                Niveau1 N1 = (Niveau1)niveau1ComboBox1.SelectedItem;
                Niveau2Services service = new Niveau2Services();
                List<Niveau2> N2Lijst = service.GetSelected(N1.ID);
                niveau2ComboBox2.ItemsSource = N2Lijst;
                niveau2ComboBox2.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (niveau2ComboBox2.SelectedIndex > -1)
            {
                Niveau2 N2 = (Niveau2)niveau2ComboBox2.SelectedItem;
                Niveau3Services service = new Niveau3Services();
                List<Niveau3> N3Lijst = service.GetSelected(N2.ID);
                if (N3Lijst.Count > 0)
                {
                    niveau3ComboBox3.ItemsSource = N3Lijst;
                    niveau3ComboBox3.DisplayMemberPath = "Naam";
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Niveau1Services N1Service = new Niveau1Services();
            List<Niveau1> N1Lijst = N1Service.FindAll();
            niveau1ComboBox1.ItemsSource = N1Lijst;
            niveau1ComboBox1.DisplayMemberPath = "Naam";

            TaalServices taalservice = new TaalServices();
            List<Taal> talen = taalservice.FindAll();
            taalComboBox.ItemsSource = talen;
            taalComboBox.DisplayMemberPath = "Naam";
            secundaireTaalComboBox.ItemsSource = talen;
            secundaireTaalComboBox.DisplayMemberPath = "Naam";

            ErkenningServices erkenningService = new ErkenningServices();
            List<Erkenning> erkenningen = erkenningService.FindAll();
            erkenningTextBox1.ItemsSource = erkenningen;
            erkenningTextBox1.DisplayMemberPath = "Naam";
            contactErkenningComboBox.ItemsSource = erkenningen;
            contactErkenningComboBox.DisplayMemberPath = "Naam";

            PostcodeService postcodeService = new PostcodeService();
            postcodes = postcodeService.FindAllPostcodes();
            postcodeComboBox.ItemsSource = postcodes;
            postcodeComboBox.DisplayMemberPath = "Postcode";

            SetData();
        }

        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (postcodeComboBox.SelectedIndex > -1)
            {
                Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                List<Postcodes> gemeentes = (from p in postcodes where p.Postcode == postcode.Postcode select p).ToList();
                plaatsComboBox.ItemsSource = gemeentes;
                plaatsComboBox.DisplayMemberPath = "Gemeente";
                plaatsComboBox.SelectedIndex = 0;
                provincieComboBox.Text = postcode.Provincie;
                landTextBox.Text = postcode.Land;
            }
            else
            {
                provincieComboBox.SelectedIndex = -1;
                landTextBox.Text = "";
            }
        }

        private void PasteCVButton_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsData(DataFormats.Text))
            {
                CVTextBox.Text = Clipboard.GetData(DataFormats.Text) as string;
                Clipboard.Clear();
            }
        }

        private void FileShowTextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            //bool isCorrect = true;

            //if (e.GetDataPresent(DataFormats.FileDrop, true) == true)
            //{
            //    string[] filenames = (string[])e.GetData(DataFormats.FileDrop, true);
            //    foreach (string filename in filenames)
            //    {
            //        if (File.Exists(filename) == false)
            //        {
            //            isCorrect = false;
            //            break;
            //        }
            //        FileInfo info = new FileInfo(filename);
            //        //if (info.Extension != ".txt")
            //        //{
            //        //    isCorrect = false;
            //        //    break;
            //        //}
            //    }
            //}
            //if (isCorrect == true)
            //    e.Effects = DragDropEffects.All;
            //else
            //    e.Effects = DragDropEffects.None;
            //e.Handled = true;

        }

        private void FileShowTextBox_PreviewDrop(object sender, DragEventArgs e)
        {
            //string[] filenames = (string[])e.GetData(DataFormats.FileDrop, true);
            //if (filenames != null)
            //{
            //    foreach (string filename in filenames)
            //        CVTextBox.Text += filename;
            //}
            //e.Handled = true;
        }
    }
}

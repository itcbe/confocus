﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for BedrijfConflictenWindow.xaml
    /// </summary>
    public partial class BedrijfConflictenWindow : Window
    {
        private Gebruiker currentUser;
        private decimal _bedrijfsID;
        private Functies _currentFunctie;
        private Spreker _currentSpreker;
        private bool IsBinding = false;
        private bool IsSpreker = false;
        private bool IsCancel = false;

        public Functies Functie
        {
            get
            {
                return _currentFunctie;
            }
        }


        public BedrijfConflictenWindow(Gebruiker user, decimal bedrijfsid)
        {
            InitializeComponent();
            currentUser = user;
            _bedrijfsID = bedrijfsid;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.Name == "functiesListBox")
            {
                IsBinding = true;
                sprekersListBox.SelectedIndex = -1;
                _currentFunctie = (Functies)box.SelectedItem;
                IsSpreker = false;
                if (_currentFunctie != null)
                {
                    //bedrijvenComboBox.Text = _currentFunctie.Bedrijf.NaamEnBtwNummer;
                    achternaamLabel.Content = _currentFunctie.Contact.Achternaam;
                    voornaamLabel.Content = _currentFunctie.Contact.Voornaam;

                    niveau1ComboBox.Text = _currentFunctie.Niveau1.Naam;
                    if (_currentFunctie.Niveau2 != null)
                        niveau2ComboBox.Text = _currentFunctie.Niveau2.Naam;
                    else
                        niveau2ComboBox.SelectedIndex = -1;
                    if (_currentFunctie.Niveau3 != null)
                        niveau3ComboBox.Text = _currentFunctie.Niveau3.Naam;
                    else
                        niveau3ComboBox.SelectedIndex = -1;
                    emailTextbox.Text = _currentFunctie.Email;
                    telefoonTextBox.Text = _currentFunctie.Telefoon;
                    mobielTextBox.Text = _currentFunctie.Mobiel;
                    erkenningsnummerTextBox.Text = _currentFunctie.Erkenningsnummer;
                    titelTextBox.Text = _currentFunctie.Oorspronkelijke_Titel;
                    attesttypeComboBox.Text = _currentFunctie.Attesttype;

                    waarderingLabelLabel.Visibility = Visibility.Hidden;
                    tariefLabel.Visibility = Visibility.Hidden;
                    tariefTextBox.Visibility = Visibility.Hidden;
                    waarderingLabel.Visibility = Visibility.Hidden;
                }
                IsBinding = false;

            }
            else if (box.Name == "sprekersListBox")
            {
                IsBinding = true;
                functiesListBox.SelectedIndex = -1;
                _currentSpreker = (Spreker)box.SelectedItem;
                IsSpreker = true;
                if (_currentSpreker != null)
                {
                    //bedrijvenComboBox.Text = _currentSpreker.Bedrijf.NaamEnBtwNummer;
                    achternaamLabel.Content = _currentSpreker.Contact.Achternaam;
                    voornaamLabel.Content = _currentSpreker.Contact.Voornaam;

                    niveau1ComboBox.Text = _currentSpreker.Niveau1.Naam;
                    if (_currentSpreker.Niveau2 != null)
                        niveau2ComboBox.Text = _currentSpreker.Niveau2.Naam;
                    else
                        niveau2ComboBox.SelectedIndex = -1;
                    if (_currentSpreker.Niveau3 != null)
                        niveau3ComboBox.Text = _currentSpreker.Niveau3.Naam;
                    else
                        niveau3ComboBox.SelectedIndex = -1;
                    emailTextbox.Text = _currentSpreker.Email;
                    telefoonTextBox.Text = _currentSpreker.Telefoon;
                    mobielTextBox.Text = _currentSpreker.Mobiel;
                    erkenningsnummerTextBox.Text = _currentSpreker.Erkenningsnummer;
                    titelTextBox.Text = _currentSpreker.Oorspronkelijke_Titel;
                    attesttypeComboBox.Text = _currentSpreker.Attesttype;
                    tariefTextBox.Text = _currentSpreker.Tarief;
                    waarderingLabel.Content = _currentSpreker.Waardering.ToString();

                    waarderingLabelLabel.Visibility = Visibility.Visible;
                    tariefLabel.Visibility = Visibility.Visible;
                    tariefTextBox.Visibility = Visibility.Visible;
                    waarderingLabel.Visibility = Visibility.Visible;

                }
                IsBinding = false;

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IsBinding = true;
            LoadData();
            List<Functies> functies = new FunctieServices().GetFunctiesInBedrijf(_bedrijfsID);
            functiesListBox.ItemsSource = functies;
            functiesListBox.DisplayMemberPath = "NaamEnFunctie";
            if (functies.Count > 0)
                functiesListBox.SelectedIndex = 0;

            List<Spreker> sprekers = new SprekerServices().GetSprekersInBedrijf(_bedrijfsID);
            sprekersListBox.ItemsSource = sprekers;
            sprekersListBox.DisplayMemberPath = "SprekerNaamEnTitel";
            if (functiesListBox.Items.Count == 0 && sprekers.Count > 0)
                sprekersListBox.SelectedIndex = 0;
            else
                sprekersListBox.SelectedIndex = -1;

            if (functiesListBox.Items.Count == 0 && sprekersListBox.Items.Count == 0)
            {
                if (MessageBox.Show("Er zijn geen conflicten voor dit bedrijf.", "Geen conflicten", MessageBoxButton.OK, MessageBoxImage.Information) == MessageBoxResult.OK)
                    DialogResult = true;
            }

            IsBinding = false;
        }

        private void LoadData()
        {

            //ReloadBedrijven();

            List<Niveau1> niveaus1 = new Niveau1Services().FindActive();
            niveau1ComboBox.ItemsSource = niveaus1;
            niveau1ComboBox.DisplayMemberPath = "Naam";

            List<AttestType> types = new Attesttype_Services().FindActive();
            attesttypeComboBox.ItemsSource = types;
            attesttypeComboBox.DisplayMemberPath = "Naam";
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau1 n1 = (Niveau1)box.SelectedItem;
                List<Niveau2> niveaus2 = new Niveau2Services().GetSelected(n1.ID);
                niveau2ComboBox.ItemsSource = niveaus2;
                niveau2ComboBox.DisplayMemberPath = "Naam";
                if (!IsBinding)
                    _currentFunctie.Niveau1_ID = n1.ID;

            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau2 n2 = (Niveau2)box.SelectedItem;
                List<Niveau3> niveaus3 = new Niveau3Services().GetSelected(n2.ID);
                niveau3ComboBox.ItemsSource = niveaus3;
                niveau3ComboBox.DisplayMemberPath = "Naam";
                if (!IsBinding)
                    _currentFunctie.Niveau2_ID = n2.ID;
            }
            else
                if (!IsBinding)
                _currentFunctie.Niveau2_ID = null;
        }

        private void bedrijvenComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsBinding)
            {

                ComboBox box = (ComboBox)sender;
                if (box.SelectedIndex > -1)
                {
                    Bedrijf bedrijf = (Bedrijf)box.SelectedItem;
                    functieBedrijfLabel.Content = bedrijf.NaamEnBtwNummer;
                    if (IsSpreker)
                        _currentSpreker.Bedrijf_ID = bedrijf.ID;
                    else
                        _currentFunctie.Bedrijf_ID = bedrijf.ID;
                }
            }

        }

        private void saveContactButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsSpreker)
            {
                if (sprekersListBox.SelectedIndex > -1)
                {
                    Contact contact = _currentSpreker.Contact;
                    contact.Gewijzigd = DateTime.Now;
                    contact.Gewijzigd_door = currentUser.Naam;

                    var cService = new ContactServices();
                    cService.SaveContactChanges(contact);
                }
            }
            else
            {
                if (functiesListBox.SelectedIndex > -1)
                {
                    //Functies functie = (Functies)functiesListBox.SelectedItem;
                    Contact contact = _currentFunctie.Contact;
                    //contact.Attesttype = attesttypeComboBox.Text;
                    contact.Gewijzigd = DateTime.Now;
                    contact.Gewijzigd_door = currentUser.Naam;

                    var cService = new ContactServices();
                    cService.SaveContactChanges(contact);
                }

            }
        }

        private void niveau3ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau3 n3 = (Niveau3)box.SelectedItem;
                if (!IsBinding)
                {
                    if (IsSpreker)
                        _currentSpreker.Niveau3_ID = n3.ID;
                    else
                        _currentFunctie.Niveau3_ID = n3.ID;
                }

            }
            else
            {
                if (!IsBinding)
                {
                    if (IsSpreker)
                        _currentSpreker.Niveau3_ID = null;
                    else
                        _currentFunctie.Niveau3_ID = null;
                }

            }
        }

        private void niveau1ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                if (IsSpreker)
                    _currentSpreker.Niveau1_ID = ((Niveau1)box.SelectedItem).ID;
                else
                    _currentFunctie.Niveau1_ID = ((Niveau1)box.SelectedItem).ID;
            }

        }

        private void niveau2ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                if (IsSpreker)
                    _currentSpreker.Niveau2_ID = ((Niveau2)box.SelectedItem).ID;
                else
                    _currentFunctie.Niveau2_ID = ((Niveau2)box.SelectedItem).ID;

            }
            else
            {
                if (IsSpreker)
                    _currentSpreker.Niveau2_ID = null;
                else
                    _currentFunctie.Niveau2_ID = null;
            }
        }

        private void niveau3ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau3 n3 = (Niveau3)box.SelectedItem;
                if (IsSpreker)
                    _currentSpreker.Niveau3_ID = n3.ID;
                else
                    _currentFunctie.Niveau3_ID = n3.ID;

            }
            else
            {
                if (IsSpreker)
                    _currentSpreker.Niveau3_ID = null;
                else
                    _currentFunctie.Niveau3_ID = null;
            }
        }

        private void FunctieTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Tab)
            {
                TextBox box = (TextBox)sender;
                switch (box.Name)
                {
                    case "emailTextbox":
                        _currentFunctie.Email = box.Text;
                        break;
                    case "mobielTextBox":
                        _currentFunctie.Mobiel = box.Text;
                        break;
                    case "telefoonTextBox":
                        _currentFunctie.Telefoon = box.Text;
                        break;
                    case "erkenningsnummerTextBox":
                        _currentFunctie.Erkenningsnummer = box.Text;
                        break;
                    case "titelTextBox":
                        _currentFunctie.Oorspronkelijke_Titel = box.Text;
                        break;
                }
            }
        }

        private void functieOpslaanButton_Click(object sender, RoutedEventArgs e)
        {
            Bedrijf bedrijf = (Bedrijf)bedrijvenComboBox.SelectedItem;
            if (IsSpreker)
            {
                if (_currentSpreker != null)
                {
                    try
                    {
                        if (SprekerIsValid(_currentSpreker))
                        {
                            if (bedrijf != null)
                                _currentSpreker.Bedrijf_ID = bedrijf.ID;
                            _currentSpreker.Gewijzigd = DateTime.Now;
                            _currentSpreker.Gewijzigd_door = currentUser.Naam;
                            new SprekerServices().SaveSprekerWijzigingen(_currentSpreker);
                            RemoveSprekerFromListBox();
                        }
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }

                }
            }
            else
            {
                if (_currentFunctie != null)
                {
                    try
                    {
                        if (FunctieIsValid(_currentFunctie))
                        {
                            if (bedrijf != null)
                                _currentFunctie.Bedrijf_ID = bedrijf.ID;
                            _currentFunctie.Gewijzigd = DateTime.Now;
                            _currentFunctie.Gewijzigd_door = currentUser.Naam;
                            new FunctieServices().SaveFunctieWijzigingen(_currentFunctie);
                            RemoveFunctieFromListBox();
                        }
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private bool FunctieIsValid(Functies func)
        {
            bool valid = true;
            string errortext = "";
            if (func.Niveau1 == null)
            {
                valid = false;
                errortext += "Geen Niveau ingegeven!\n";
            }

            if (bedrijvenComboBox.SelectedIndex == -1)
            {
                valid = false;
                errortext += "Geen bedrijf gekozen!\n";
            }

            if (string.IsNullOrEmpty(func.Attesttype))
            {
                valid = false;
                errortext += "Geen attesttype ingevuld!";
            }

            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }

        private bool SprekerIsValid(Spreker spreker)
        {
            bool valid = true;
            string errortext = "";
            if (spreker.Niveau1 == null)
            {
                valid = false;
                errortext += "Geen Niveau ingegeven!\n";
            }

            if (bedrijvenComboBox.SelectedIndex == -1)
            {
                valid = false;
                errortext += "Geen bedrijf gekozen!\n";
            }

            if (string.IsNullOrEmpty(spreker.Attesttype))
            {
                valid = false;
                errortext += "Geen attesttype ingevuld!";
            }
            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void RemoveSprekerFromListBox()
        {
            if (_currentSpreker != null)
            {
                List<Spreker> sprekers = new List<Spreker>();
                foreach (Spreker item in sprekersListBox.Items)
                {
                    if (item.ID != _currentSpreker.ID)
                        sprekers.Add(item);
                }
                sprekersListBox.ItemsSource = sprekers;
                sprekersListBox.DisplayMemberPath = "SprekerNaamEnTitel";
                //bedrijvenComboBox.SelectedIndex = 0;
                if (sprekers.Count > 0)
                    sprekersListBox.SelectedIndex = 0;
                else if (functiesListBox.Items.Count > 0)
                    functiesListBox.SelectedIndex = 0;
                else
                    DialogResult = true;

            }
        }

        private void RemoveFunctieFromListBox()
        {
            if (_currentFunctie != null)
            {
                List<Functies> functies = new List<Functies>();
                foreach (Functies item in functiesListBox.Items)
                {
                    if (item.ID != _currentFunctie.ID)
                        functies.Add(item);
                }//functiesListBox.Items.
                functiesListBox.ItemsSource = functies;
                functiesListBox.DisplayMemberPath = "NaamEnFunctie";
                //bedrijvenComboBox.SelectedIndex = 0;
                if (functies.Count > 0)
                {
                    functiesListBox.SelectedIndex = 0;
                }
                else if (sprekersListBox.Items.Count > 0)
                    sprekersListBox.SelectedIndex = 0;
                else
                    DialogResult = true;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void newCompanyButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwBedrijf NB = new NieuwBedrijf(currentUser);
            if (NB.ShowDialog() == true)
            {
                Bedrijf bedrijf = NB.Bedrijf;
                //ReloadBedrijven();
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                bedrijven.Add(bedrijf);
                bedrijvenComboBox.Text = bedrijf.NaamEnBtwNummer;
                if (IsSpreker)
                    _currentSpreker.Bedrijf_ID = bedrijf.ID;
                else
                    _currentFunctie.Bedrijf_ID = bedrijf.ID;
            }
        }

        private void ReloadBedrijven()
        {
            IsBinding = true;
            List<Bedrijf> bedrijven = new BedrijfServices().FindActive();
            bedrijvenComboBox.ItemsSource = bedrijven;
            bedrijvenComboBox.DisplayMemberPath = "NaamEnBtwNummer";
            IsBinding = false;
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZB = new ZoekContactWindow("Bedrijf");
            if (ZB.ShowDialog() == true)
            {
                Bedrijf MyBedrijf = ZB.bedrijf;
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                bedrijven.Add(MyBedrijf);
                bedrijvenComboBox.ItemsSource = bedrijven;
                bedrijvenComboBox.DisplayMemberPath = "NaamEnAdres";
                bedrijvenComboBox.SelectedIndex = 0;
                if (IsSpreker)
                    _currentSpreker.Bedrijf_ID = MyBedrijf.ID;
                else
                    _currentFunctie.Bedrijf_ID = MyBedrijf.ID;
            }
        }

        private void attesttypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                if (IsSpreker)
                {
                    _currentSpreker.AttestType_ID = ((AttestType)box.SelectedItem).ID;
                    _currentSpreker.Attesttype = ((AttestType)box.SelectedItem).Naam;
                }
                else
                {
                    _currentFunctie.Attesttype_ID = ((AttestType)box.SelectedItem).ID;
                    _currentFunctie.Attesttype = ((AttestType)box.SelectedItem).Naam;
                }
            }
            else
            {
                if (IsSpreker)
                {
                    _currentSpreker.AttestType_ID = null;
                    _currentSpreker.Attesttype = "";
                }
                else
                {
                    _currentFunctie.Attesttype_ID = null;
                    _currentFunctie.Attesttype = "";
                }
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            switch (box.Name)
            {
                case "emailTextbox":
                    if (IsSpreker)
                        _currentSpreker.Email = box.Text;
                    else
                        _currentFunctie.Email = box.Text;
                    break;
                case "mobielTextBox":
                    if (IsSpreker)
                        _currentSpreker.Mobiel = Helper.CleanPhonenumber(box.Text);
                    else
                        _currentFunctie.Mobiel = Helper.CleanPhonenumber(box.Text);
                    break;
                case "telefoonTextBox":
                    if (IsSpreker)
                        _currentSpreker.Telefoon = Helper.CleanPhonenumber(box.Text);
                    else
                        _currentFunctie.Telefoon = Helper.CleanPhonenumber(box.Text);
                    break;
                case "erkenningsnummerTextBox":
                    if (IsSpreker)
                        _currentSpreker.Erkenningsnummer = box.Text;
                    else
                        _currentFunctie.Erkenningsnummer = box.Text;
                    break;
                case "titelTextBox":
                    if (IsSpreker)
                        _currentSpreker.Oorspronkelijke_Titel = box.Text;
                    else
                        _currentFunctie.Oorspronkelijke_Titel = box.Text;
                    break;
                case "tariefTextBox":
                    if (IsSpreker)
                        _currentSpreker.Tarief = box.Text;
                    break;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsCancel)
            {
                if (functiesListBox.Items.Count > 0 || sprekersListBox.Items.Count > 0)
                {
                    ConfocusMessageBox.Show("Je hebt nog niet alle functies aangepast!\n Pas deze nog aan om geen gegevens te verliezen.", ConfocusMessageBox.Kleuren.Rood);
                    e.Cancel = true;
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            IsCancel = true;
            DialogResult = false;
        }
    }
}

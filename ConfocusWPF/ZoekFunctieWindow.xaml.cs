﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ZoekFunctieWindow.xaml
    /// </summary>
    public partial class ZoekFunctieWindow : Window
    {
        private bool isHistoriek;

        public bool IsHistoriek
        {
            get { return isHistoriek; }
            set { isHistoriek = value; }
        }

        public Functies Functie;
        public ZoekFunctieWindow()
        {
            InitializeComponent();
            ZoekVoornaamTextBox.Focus();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ZoekFuncties();
            }
        }

        private void ZoekFuncties()
        {
            if (!string.IsNullOrEmpty(ZoekVoornaamTextBox.Text) || !string.IsNullOrEmpty(ZoekAchternaamTextBox.Text))
            {
                resultLabel.Content = "Zoeken ...";
                ContactZoekTerm zoekterm = new ContactZoekTerm();
                zoekterm.Voornaam = DBHelper.Simplify(ZoekVoornaamTextBox.Text);
                zoekterm.Achternaam = DBHelper.Simplify(ZoekAchternaamTextBox.Text);
                FunctieServices fService = new FunctieServices();
                List<Functies> functies = new List<Functies>();
                if (isHistoriek)
                {
                    functies = fService.GetFunctieByContactName(zoekterm);
                }
                else
                    functies = fService.GetActiveFunctieByContactName(zoekterm);
                resultListBox.ItemsSource = functies;
                resultListBox.DisplayMemberPath = "NaamEnNiveaus"; //NaamBedrijfFunctie
                resultLabel.Content = functies.Count + " resultaten gevonden.";
            }
        }

        private void Zoekbutton_Click(object sender, RoutedEventArgs e)
        {
            ZoekFuncties();
        }

        private void resultListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (resultListBox.SelectedIndex > -1)
            {
                Functie = (Functies)resultListBox.SelectedItem;
            }
        }

        private void resultListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (resultListBox.SelectedIndex > -1)
            {
                Functie = (Functies)resultListBox.SelectedItem;
                DialogResult = true;
            }
        }
    }
}

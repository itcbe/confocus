﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusWPF.Flexmail
{
    public class EmailAdres
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string Reference { get; set; }
        public string Niveau1 { get; set; }
        public string Niveau2 { get; set; }
        public string Niveau3 { get; set; }
        public string Flexmail_ID { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Mobiel { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string Address { get; set; }
        public string County { get; set; }
        public string Jobtitle { get; set; }
        public string Language { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }

        public EmailAdres()
        {

        }

        public EmailAdres(string firstname, string lastname, string company, string email, string reference)
        {
            FirstName = firstname;
            LastName = lastname;
            Company = company;
            Email = email;
            Reference = reference;
        }

        public EmailAdres(string firstname, string lastname, string company, string email, string niveau1, string niveau2, string niveau3, string reference)
        {
            FirstName = firstname;
            LastName = lastname;
            Company = company;
            Email = email;
            Niveau1 = niveau1;
            Niveau2 = niveau2;
            Niveau3 = niveau3;
            Reference = reference;
        }

        public EmailAdres(string firstname, string lastname, string company, string email, string niveau1, string niveau2, string niveau3, string reference
            , string phone, string fax, string mobile, string city, string zip, string address, string county, string jobtitle, string language, string type, string title)
        {
            FirstName = firstname;
            LastName = lastname;
            Company = company;
            Email = email;
            Niveau1 = niveau1;
            Niveau2 = niveau2;
            Niveau3 = niveau3;
            Reference = reference;
            Phone = phone;
            Fax = fax;
            Mobiel = mobile;
            City = city;
            Zipcode = zip;
            Address = address;
            County = county;
            Jobtitle = jobtitle;
            Language = language;
            Type = type;
            Title = title;
        }

        public override bool Equals(object obj)
        {
            if (obj is EmailAdres)
            {
                if (Reference == ((EmailAdres)obj).Reference)
                    return true;
                else
                    return false;
            }
            else
                return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

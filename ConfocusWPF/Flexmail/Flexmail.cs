﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfocusWPF.FlexmailService;
using ADOClassLibrary;

namespace ConfocusWPF.Flexmail
{
    public class Flexmail
    {
        private int userid;
        private string token;
        private List<EmailAdres> Bounced = new List<EmailAdres>();
        private List<EmailAdres> Blacklist = new List<EmailAdres>();
        private List<EmailAdres> Invalid = new List<EmailAdres>();
        private List<EmailAdres> DoubleEmails = new List<EmailAdres>();
        private List<EmailAdres> AllSend = new List<EmailAdres>();


        public Flexmail()
        {
            Instelling_Service iService = new Instelling_Service();
            Instelling instelling = iService.Find();
            userid = (int)instelling.Flexmail_User_ID;
            token = instelling.Flexmail_User_Token;
        }

        internal int CreateMailingList(string name, int categorieId)
        {
            int id = 0;
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.CreateMailingListReq request = new FlexmailService.CreateMailingListReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.categoryId = categorieId;
            request.mailingListName = name;
            request.mailingListLanguage = "NL";

            FlexmailService.CreateMailingListResp responce = service.CreateMailingList(request);

            if (responce.errorCode == 0)
            {
                try
                {
                    id = GetMailinglistByName(name, categorieId);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            else if (responce.errorCode == 225)
                throw new Exception("De lijst bestaat reeds!");
            else
                throw new Exception("Fout bij het maken van de lijst");
            return id;
        }

        internal int GetMailinglistByName(string name, int categorieId)
        {
            int id = 0;
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.GetMailingListsReq request = new FlexmailService.GetMailingListsReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.categoryId = categorieId;
            FlexmailService.GetMailingListsResp responce = service.GetMailingLists(request);

            if (responce.errorCode == 0)
            {
                foreach (FlexmailService.MailingListType list in responce.mailingListTypeItems)
                {
                    if (list.mailingListName == name)
                    {
                        id = list.mailingListId;
                        break;
                    }
                }
            }
            else
                throw new Exception("Fout bij get ophalen van de lijst ID!\n" + responce.errorMessage);

            return id;
        }

        internal List<EmailAdres> GetEmailListByRef(List<EmailAdres> list, int listid)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.GetEmailAddressesReq request = new FlexmailService.GetEmailAddressesReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.mailingListIds = new int[1] { listid };

            FlexmailService.EmailAddressType[] Adressen = new FlexmailService.EmailAddressType[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                EmailAdres adres = list[i];
                FlexmailService.EmailAddressType reference = new FlexmailService.EmailAddressType();
                reference.flexmailId = adres.Flexmail_ID;
                Adressen[i] = reference;
            }

            request.emailAddressTypeItems = Adressen;        

            FlexmailService.GetEmailAddressesResp responce = service.GetEmailAddresses(request);

            if (responce.errorCode == 0)
            {
                List<EmailAdres> results = new List<EmailAdres>();
                foreach (FlexmailService.EmailAddressType item in responce.emailAddressTypeItems)
                {
                    EmailAdres adres = new EmailAdres();
                    adres.FirstName = item.surname;
                    adres.LastName = item.name;
                    adres.Company = item.company;
                    adres.Reference = item.referenceId;
                    adres.Niveau1 = item.market;
                    adres.Niveau2 = item.free_field_1;
                    adres.Niveau3 = item.free_field_2;
                    adres.Email = item.emailAddress;
                    results.Add(adres);
                }
                return results;
            }
            else
                return new List<EmailAdres>();
        }

        internal void ImportMailAdressesNew(List<FilterFunctie> functies, int listid)
        {
            AllSend.Clear();
            DateTime tijd = DateTime.Now;
            string baseref = "Em" + tijd.Year + tijd.Month + tijd.Day + tijd.Hour + tijd.Minute;
            int aantalInvalid = 0;
            int aantalBlacklist = 0;
            int aantalBounced = 0;
            int aantalAlreadyExcists = 0;

            List<Array> lijsten = GetLists(functies.Count);
            int listindex = 0;
            int targetindex = lijsten[0].Length;
            int index = 0;
            foreach (FlexmailService.EmailAddressType[] list in lijsten)
            {
                for (int i = index; i < functies.Count; i++)
                {
                    string reference = baseref + "-" + i;
                    FilterFunctie functie = functies[i];
                    FlexmailService.EmailAddressType adres = new FlexmailService.EmailAddressType();
                    try { adres.surname = functie.Achternaam; } catch (Exception) { adres.name = ""; }
                    try { adres.name = functie.Voornaam; } catch (Exception) { adres.surname = ""; }
                    adres.emailAddress = functie.Email.Trim();
                    try { adres.company = functie.Bedrijfsnaam; } catch (Exception) { adres.company = ""; }
                    try { adres.phone = functie.BedrijfsTelefoon; } catch (Exception) { adres.phone = ""; }
                    try { adres.fax = functie.BedrijfsFax; } catch (Exception) { adres.fax = ""; }
                    adres.mobile = functie.Mobiel;

                    try { adres.city = functie.BedrijfsPlaats; } catch (Exception) { adres.city = ""; }
                    try { adres.zipcode = functie.BedrijfsPostcode; } catch (Exception) { adres.zipcode = ""; }
                    try { adres.address = functie.BedrijfsAdres; } catch (Exception) { adres.address = ""; }
                    try { adres.province = functie.BedrijfsProvincie; } catch (Exception) { adres.province = ""; }
                    try { adres.market = functie.Niveau1Naam; } catch (Exception) { adres.market = ""; }
                    try { adres.free_field_1 = string.IsNullOrEmpty(functie.Niveau2Naam) ? "" : functie.Niveau2Naam; } catch (Exception) { adres.free_field_1 = ""; }
                    try { adres.free_field_2 = string.IsNullOrEmpty(functie.Niveau3Naam) ? "" : functie.Niveau3Naam; } catch (Exception) { adres.free_field_2 = ""; }

                    adres.jobtitle = functie.Oorspronkelijke_Titel;
                    try { adres.language = functie.TaalNaam == null ? "" : functie.TaalNaam; } catch (Exception) { adres.language = ""; };
                    adres.free_field_3 = functie.Type;
                    try { adres.title = functie.Aanspreking; } catch (Exception) { adres.title = ""; }
                    adres.referenceId = reference;

                    if (adres.referenceId != "")
                    {
                        list[listindex] = adres;
                        AllSend.Add(new EmailAdres(adres.surname, adres.name, adres.company, adres.emailAddress, adres.market, adres.free_field_1, adres.free_field_2, adres.referenceId));
                    }
                    if (listindex == 999)
                    {
                        listindex = 0;
                        index = i + 1;
                        break;
                    }
                    else
                        listindex++;
                }
            }

            //string myref = "";
            Invalid.Clear();
            Bounced.Clear();
            Blacklist.Clear();
            DoubleEmails.Clear();

            foreach (FlexmailService.EmailAddressType[] list in lijsten)
            {
                try
                {
                    FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
                    FlexmailService.ImportEmailAddressesReq request = new FlexmailService.ImportEmailAddressesReq();
                    FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

                    header.userId = userid;
                    header.userToken = token;

                    request.header = header;
                    request.mailingListId = listid;
                    request.emailAddressTypeItems = list;
                    FlexmailService.ImportEmailAddressesResp responce = service.ImportEmailAddresses(request);

                    if (responce.errorCode != 0)
                    {
                        string fout = "";
                        switch (responce.errorCode)
                        {
                            case 221:
                                fout = "Het lijst id is niet aanwezig!";
                                break;
                            case 222:
                                fout = "Het lijst id is fout";
                                break;
                            case 223:
                                fout = "Eén of meerdere emailadressen zijn niet aanvaard! (Dubbele, Leeg)";
                                foreach (FlexmailService.ImportEmailAddressRespType item in responce.importEmailAddressRespTypeItems)
                                {
                                    if (item.errorCode == 226)
                                    {
                                        aantalInvalid++;
                                        EmailAdres adres = GetAdresFromItem(item);//new EmailAdres();
                                        Invalid.Add(adres);
                                    }
                                    else if (item.errorCode == 227)
                                    {
                                        aantalAlreadyExcists++;
                                        EmailAdres adres = GetAdresFromItem(item);
                                        DoubleEmails.Add(adres);
                                    }
                                    else if (item.errorCode == 228)
                                    {
                                        aantalBlacklist++;
                                        EmailAdres adres = GetAdresFromItem(item);
                                        Blacklist.Add(adres);
                                    }
                                    else if (item.errorCode == 230)
                                    {
                                        aantalBounced++;
                                        EmailAdres adres = GetAdresFromItem(item);
                                        Bounced.Add(adres);
                                    }
                                }
                                break;
                            case 229:
                                fout = "Jullie plan limiet is bereikt!";
                                break;
                            default:

                                break;
                        }
                        //throw new Exception(fout);
                    }
                }
                catch (Exception ex)
                {

                    //throw;
                }
            }
            GetResults(listid, aantalInvalid, aantalBlacklist, aantalAlreadyExcists, aantalBounced);

        }

        internal void ImportMailAddresses(List<Functies> functies, int listid)
        {
            AllSend.Clear();
            DateTime tijd = DateTime.Now;
            string baseref = "Em" + tijd.Year + tijd.Month + tijd.Day + tijd.Hour + tijd.Minute;
            int aantalInvalid = 0;
            int aantalBlacklist = 0;
            int aantalBounced = 0;
            int aantalAlreadyExcists = 0;

            List<Array> lijsten = GetLists(functies.Count);
            int listindex = 0;
            int targetindex = lijsten[0].Length;
            int index = 0;
            foreach (FlexmailService.EmailAddressType[] list in lijsten)
            {
                for (int i = index; i < functies.Count; i++)
                {
                    string reference = baseref + "-" + i;
                    Functies functie = functies[i];
                    FlexmailService.EmailAddressType adres = new FlexmailService.EmailAddressType();
                    try { adres.surname = functie.Contact.Achternaam; } catch (Exception) { adres.name = ""; }
                    try { adres.name = functie.Contact.Voornaam; } catch (Exception) { adres.surname = ""; }
                    adres.emailAddress = functie.Email.Trim();
                    try { adres.company = functie.Bedrijf.Naam; } catch (Exception) { adres.company = ""; }
                    try { adres.phone = functie.Bedrijf.Telefoon; } catch (Exception) { adres.phone = ""; }
                    try { adres.fax = functie.Bedrijf.Fax; } catch (Exception) { adres.fax = ""; }
                    adres.mobile = functie.Mobiel;

                    try { adres.city = functie.Bedrijf.Plaats; } catch (Exception) { adres.city = ""; }
                    try { adres.zipcode = functie.Bedrijf.Postcode; } catch (Exception) { adres.zipcode = ""; }
                    try { adres.address = functie.Bedrijf.Adres; } catch (Exception) { adres.address = ""; }
                    try { adres.province = functie.Bedrijf.Provincie; } catch (Exception) { adres.province = ""; }
                    try { adres.market = functie.Niveau1.Naam; } catch (Exception) { adres.market = ""; }
                    try { adres.free_field_1 = functie.Niveau2 == null ? "" : functie.Niveau2.Naam; } catch (Exception) { adres.free_field_1 = ""; }
                    try { adres.free_field_2 = functie.Niveau3 == null ? "" : functie.Niveau3.Naam; } catch (Exception) { adres.free_field_2 = ""; }

                    adres.jobtitle = functie.Oorspronkelijke_Titel;
                    try { adres.language = functie.Contact.Taal == null ? "" : functie.Contact.Taal.Naam; } catch (Exception) { adres.language = ""; };
                    adres.free_field_3 = functie.Type;
                    try { adres.title = functie.Contact.Aanspreking; } catch (Exception) { adres.title = ""; }
                    adres.referenceId = reference;

                    if (adres.referenceId != "")
                    { 
                        list[listindex] = adres;
                        AllSend.Add(new EmailAdres(adres.surname, adres.name, adres.company, adres.emailAddress, adres.market, adres.free_field_1, adres.free_field_2, adres.referenceId));
                    }
                    if (listindex == 999)
                    {
                        listindex = 0;
                        index = i + 1;
                        break;
                    }
                    else
                        listindex++;
                }
            }

            //string myref = "";
            Invalid.Clear();
            Bounced.Clear();
            Blacklist.Clear();
            DoubleEmails.Clear();

            foreach (FlexmailService.EmailAddressType[] list in lijsten)
            {
                try
                {
                    FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
                    FlexmailService.ImportEmailAddressesReq request = new FlexmailService.ImportEmailAddressesReq();
                    FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

                    header.userId = userid;
                    header.userToken = token;

                    request.header = header;
                    request.mailingListId = listid;
                    request.emailAddressTypeItems = list;
                    FlexmailService.ImportEmailAddressesResp responce = service.ImportEmailAddresses(request);

                    if (responce.errorCode != 0)
                    {
                        string fout = "";
                        switch (responce.errorCode)
                        {
                            case 221:
                                fout = "Het lijst id is niet aanwezig!";
                                break;
                            case 222:
                                fout = "Het lijst id is fout";
                                break;
                            case 223:
                                fout = "Eén of meerdere emailadressen zijn niet aanvaard! (Dubbele, Leeg)";
                                foreach (FlexmailService.ImportEmailAddressRespType item in responce.importEmailAddressRespTypeItems)
                                {
                                    if (item.errorCode == 226)
                                    { 
                                        aantalInvalid++;
                                        EmailAdres adres = GetAdresFromItem(item);//new EmailAdres();
                                        Invalid.Add(adres);
                                    }
                                    else if (item.errorCode == 227)
                                    { 
                                        aantalAlreadyExcists++;
                                        EmailAdres adres = GetAdresFromItem(item);
                                        DoubleEmails.Add(adres);
                                    }
                                    else if (item.errorCode == 228)
                                    { 
                                        aantalBlacklist++;
                                        EmailAdres adres = GetAdresFromItem(item);
                                        Blacklist.Add(adres);
                                    }
                                    else if (item.errorCode == 230)
                                    { 
                                        aantalBounced++;
                                        EmailAdres adres = GetAdresFromItem(item);
                                        Bounced.Add(adres);
                                    }
                                }
                                break;
                            case 229:
                                fout = "Jullie plan limiet is bereikt!";
                                break;
                            default:

                                break;
                        }
                        //throw new Exception(fout);
                    }                    
                }
                catch (Exception ex)
                {

                    //throw;
                }                
            }
            GetResults(listid, aantalInvalid, aantalBlacklist, aantalAlreadyExcists, aantalBounced);
        }

        private EmailAdres GetAdresFromItem(ImportEmailAddressRespType item)
        {
            EmailAdres adres = new EmailAdres();
            adres.Reference = item.referenceId;
            adres.Flexmail_ID = item.flexmailId.ToString();

            return adres;
        }

        private void GetResults(int listid, int aantalInvalid, int aantalBlacklist, int aantalAlreadyExcists, int aantalBounced)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.GetEmailAddressesReq request = new FlexmailService.GetEmailAddressesReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.mailingListIds = new int[1] { listid };

            FlexmailService.GetEmailAddressesResp responce = service.GetEmailAddresses(request);

            if (responce.errorCode == 0)
            {
                List<EmailAdres> results = new List<EmailAdres>();
                StringBuilder verslag = new StringBuilder();
                int aantal = 0;
                foreach (FlexmailService.EmailAddressType adres in responce.emailAddressTypeItems)
                {
                    aantal++;
                    results.Add(new EmailAdres(adres.surname, adres.name,  adres.company, adres.emailAddress, adres.referenceId));

                }
                verslag.Append("Emailadressen geimporteerd: " + aantal + ".\n");
                verslag.Append("Emailadressen niet juist: " + aantalInvalid + ".\n");
                verslag.Append("Emailadressen blacklist: " + aantalBlacklist + ".\n");
                verslag.Append("Emailadressen bounced: " + aantalBounced + ".\n");
                verslag.Append("Emailadressen dubbel: " + aantalAlreadyExcists + ".\n");

                List<EmailAdres> gesorteerd = (from e in results orderby e.LastName select e).ToList();
                List<EmailAdres> notImported = new List<EmailAdres>();
                foreach (EmailAdres adres in AllSend)
                {
                    //bool gevonden = false;
                    EmailAdres adr = (from a in gesorteerd
                                where a.LastName.Trim().ToLower() == adres.LastName.Trim().ToLower()
                                && a.FirstName.Trim().ToLower() == adres.FirstName.Trim().ToLower()
                                && a.Email.Trim().ToLower() == adres.Email.Trim().ToLower()
                                select a).FirstOrDefault();
                    //foreach (EmailAdres FMAdrres in gesorteerd)
                    //{
                    //    string functiemail = adres.Email.Trim().ToLower();
                    //    string fmMail = FMAdrres.Email.Trim().ToLower();
                    //    if (functiemail == fmMail)
                    //    { 
                    //        gevonden = true;
                    //        break;
                    //    }
                    //}
                    if (adr == null)
                    {
                        notImported.Add(adres);
                    }
                }
                //verslag.Append("Emailadressen niet geïmporteerd: " + notImported.Count + ".\n");
                FlexMailResults FR = new FlexMailResults(gesorteerd, verslag.ToString(), Invalid, Blacklist, Bounced, DoubleEmails,AllSend, listid, notImported);
                FR.Show();
            }
            else
                ConfocusMessageBox.Show("Geen verslag kunnen maken.", ConfocusMessageBox.Kleuren.Rood);


        }

        private List<Array> GetLists(int count)
        {
            if (count != 0)
            {
                List<Array> lists = new List<Array>();
                double aantallen = count / 1000;
                int aantallijsten = (int)aantallen;
                int rest = 0;
                int aantal = count;
                while (aantal > 1000)
                {
                    aantal -= 1000;
                }
                rest = aantal;

                if (rest > 0)
                    aantallijsten++;

                for (int i = 1; i <= aantallijsten; i++)
                {
                    if (i == aantallijsten)
                        lists.Add(new FlexmailService.EmailAddressType[rest]);
                    else
                        lists.Add(new FlexmailService.EmailAddressType[1000]);
                }


                return lists;
            }
            else
                return null;


        }

        internal void ImportBlackList(List<Functies> functies)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.ImportBlacklistReq request = new FlexmailService.ImportBlacklistReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            FlexmailService.EmailAddressType[] emaillist = new FlexmailService.EmailAddressType[functies.Count];

            for (int i = 0; i < functies.Count; i++)
            {
                Functies functie = functies[i];
                FlexmailService.EmailAddressType adres = new FlexmailService.EmailAddressType();
                adres.emailAddress = functie.Email;
                emaillist[i] = adres;
            }

            request.emailAddressTypeItems = emaillist;
            FlexmailService.ImportBlacklistResp responce = service.ImportBlacklist(request);
            if (responce.errorCode != 0)
                throw new Exception("Fout bij het importeren van de blacklist!\n" + responce.errorMessage);
        }

        internal List<Categorie> GetCategories()
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();

            FlexmailService.GetCategoriesReq request = new FlexmailService.GetCategoriesReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;

            FlexmailService.GetCategoriesResp responce = service.GetCategories(request);

            if (responce.errorCode == 0)
            {
                List<Categorie> results = new List<Categorie>();
                foreach (FlexmailService.CategoryType categorie in responce.categoryTypeItems)
                {
                    results.Add(new Categorie(categorie.categoryId, categorie.categoryName));
                }
                return results;

            }
            else
                throw new Exception("Fout bij het ophalen van de categorieën!\n" + responce.errorMessage);
        }

        internal List<FlexmailLijst> GetLijsten()
        {
            List<FlexmailLijst> lijsten = new List<FlexmailLijst>();
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();

            FlexmailService.GetMailingListsReq request = new FlexmailService.GetMailingListsReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;

            FlexmailService.GetMailingListsResp responce = service.GetMailingLists(request);

            if (responce.errorCode == 0)
            {
                foreach (FlexmailService.MailingListType list in responce.mailingListTypeItems)
                {
                    lijsten.Add(new FlexmailLijst(list.mailingListId, list.mailingListName));
                }
            }
            else
                throw new Exception("Fout bij het ophalen van de bestaande lijsten!\n" + responce.errorMessage);
            return lijsten;
        }

        internal bool MaakLijstLeeg(int id)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();

            FlexmailService.TruncateMailingListReq request = new FlexmailService.TruncateMailingListReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.mailingListId = id;
            request.header = header;
            FlexmailService.TruncateMailingListResp responce = service.TruncateMailingList(request);

            if (responce.errorCode == 0)
                return true;
            else
                return false;
        }

        internal bool UpdateEmailAddressesNew(List<FilterFunctie> functies, int listid)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.GetEmailAddressesReq request = new FlexmailService.GetEmailAddressesReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.mailingListIds = new int[1] { listid };

            FlexmailService.GetEmailAddressesResp responce = service.GetEmailAddresses(request);

            if (responce.errorCode == 0)
            {
                List<EmailAdres> results = new List<EmailAdres>();
                foreach (FlexmailService.EmailAddressType adres in responce.emailAddressTypeItems)
                {
                    results.Add(new EmailAdres(adres.name, adres.surname, adres.company, adres.emailAddress, adres.market, adres.free_field_1, adres.free_field_2, adres.referenceId, adres.phone, adres.fax, adres.mobile, adres.city, adres.zipcode, adres.address, adres.province, adres.jobtitle, adres.language, adres.free_field_3, adres.title));
                }

                foreach (FilterFunctie functie in functies)
                {
                    foreach (EmailAdres adres in results)
                    {
                        if (adres.LastName == functie.Achternaam && adres.FirstName == functie.Voornaam && adres.Company == functie.Bedrijfsnaam)
                        {
                            adres.Phone = functie.Telefoon;
                            adres.Address = functie.BedrijfsAdres;
                            adres.Zipcode = functie.BedrijfsPostcode;
                            adres.City = functie.BedrijfsPlaats;
                            adres.County = functie.BedrijfsProvincie;
                            adres.Fax = functie.BedrijfsFax;
                            adres.Mobiel = functie.Mobiel;
                            adres.Email = functie.Email;
                            UpdateEmailAdres(adres, listid);
                        }
                    }
                }
            }
            return true;

        }

        internal bool UpdateEmailAddresses(List<Functies> functies, int listid)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.GetEmailAddressesReq request = new FlexmailService.GetEmailAddressesReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.mailingListIds = new int[1] { listid };

            FlexmailService.GetEmailAddressesResp responce = service.GetEmailAddresses(request);

            if (responce.errorCode == 0)
            {
                List<EmailAdres> results = new List<EmailAdres>();
                foreach (FlexmailService.EmailAddressType adres in responce.emailAddressTypeItems)
                {
                    results.Add(new EmailAdres(adres.name, adres.surname, adres.company, adres.emailAddress, adres.market, adres.free_field_1, adres.free_field_2, adres.referenceId, adres.phone, adres.fax, adres.mobile, adres.city, adres.zipcode, adres.address, adres.province, adres.jobtitle, adres.language, adres.free_field_3, adres.title));
                }

                foreach (Functies functie in functies)
                {
                    foreach (EmailAdres adres in results)
                    {
                        if (adres.LastName == functie.Contact.Achternaam && adres.FirstName == functie.Contact.Voornaam && adres.Company == functie.Bedrijf.Naam)
                        {
                            adres.Phone = functie.Telefoon;
                            adres.Address = functie.Bedrijf.Adres;
                            adres.Zipcode = functie.Bedrijf.Postcode;
                            adres.City = functie.Bedrijf.Plaats;
                            adres.County = functie.Bedrijf.Provincie;
                            adres.Fax = functie.Bedrijf.Fax;
                            adres.Mobiel = functie.Mobiel;                            
                            adres.Email = functie.Email;
                            UpdateEmailAdres(adres, listid);
                        }
                    }
                }
            }
            return true;
        }

        private void UpdateEmailAdres(EmailAdres adres, int listid)
        {
            FlexmailService.FlexmailAPIService service = new FlexmailService.FlexmailAPIService();
            FlexmailService.UpdateEmailAddressReq request = new UpdateEmailAddressReq();
            FlexmailService.APIRequestHeader header = new FlexmailService.APIRequestHeader();

            header.userId = userid;
            header.userToken = token;

            request.header = header;
            request.mailingListId =  listid;

            EmailAddressType adrestype = new EmailAddressType();

            try { adrestype.surname = adres.LastName; } catch (Exception) { adrestype.surname = ""; }
            try { adrestype.name = adres.FirstName; } catch (Exception) { adrestype.name = ""; }
            adrestype.emailAddress = adres.Email.Trim();
            try { adrestype.company = adres.Company; } catch (Exception) { adrestype.company = ""; }
            try { adrestype.phone = adres.Phone; } catch (Exception) { adrestype.phone = ""; }
            try { adrestype.fax = adres.Fax; } catch (Exception) { adrestype.fax = ""; }
            adrestype.mobile = adres.Mobiel;

            try { adrestype.city = adres.City; } catch (Exception) { adrestype.city = ""; }
            try { adrestype.zipcode =   adres.Zipcode; } catch (Exception) { adrestype.zipcode = ""; }
            try { adrestype.address = adres.Address; } catch (Exception) { adrestype.address = ""; }
            try { adrestype.province = adres.County; } catch (Exception) { adrestype.province = ""; }
            try { adrestype.market = adres.Niveau1; } catch (Exception) { adrestype.market = ""; }
            try { adrestype.free_field_1 = adres.Niveau2; } catch (Exception) { adrestype.free_field_1 = ""; }
            try { adrestype.free_field_2 = adres.Niveau3; } catch (Exception) { adrestype.free_field_2 = ""; }

            adrestype.jobtitle = adres.Jobtitle;
            try { adrestype.language = adres.Language; } catch (Exception) { adrestype.language = ""; };
            adrestype.free_field_3 = adres.Type;
            try { adrestype.title = adres.Title; } catch (Exception) { adrestype.title = ""; }
            adrestype.referenceId = adres.Reference;

            request.emailAddressType = adrestype;

            FlexmailService.UpdateEmailAddressResp responce = service.UpdateEmailAddress(request);
            if (responce.errorCode == 0)
            {
                ;
            }
        }
    }
}

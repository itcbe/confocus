﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusWPF.Flexmail
{
    public class Categorie
    {
        public int ID { get; set; }
        public string Naam { get; set; }

        public Categorie()
        {

        }

        public Categorie(int id, string naam)
        {
            ID = id;
            Naam = naam;   
        }
    }
}

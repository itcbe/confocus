﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for TaalBeheer.xaml
    /// </summary>
    public partial class TaalBeheer : Window
    {
        private CollectionViewSource taalViewSource;
        private List<Taal> Talen = new List<Taal>();
        private List<Taal> gevondenTalen = new List<Taal>();
        private List<Taal> gewijzigdeTalen = new List<Taal>();
        private Boolean IsScroll = false, IsButtonClick = false;
        private Gebruiker currentUser;

        public TaalBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Taal beheer : Ingelogd als " + currentUser.Naam;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            taalViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            taalViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            taalViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            taalViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            Nieuwetaal NT = new Nieuwetaal(currentUser);
            if (NT.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            TaalServices service = new TaalServices();
            Talen = service.FindAll();
            taalViewSource.Source = Talen;
            goUpdate();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();
            
        }

        private void SaveWijzigingen()
        {
            if (gewijzigdeTalen.Count > 0)
            {
                TaalServices service = new TaalServices();
                foreach (Taal t in gewijzigdeTalen)
                {
                    try
                    {
                        service.SaveTaalWijzigingen(t);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                gewijzigdeTalen.Clear();
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            }
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            taalViewSource = ((CollectionViewSource)(this.FindResource("taalViewSource")));
            LoadData();           
        }

        private void goUpdate()
        {
            if (taalDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (taalViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (taalViewSource.View.CurrentPosition == taalDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                taalDataGrid.SelectedIndex = 0;
            if (taalDataGrid.Items.Count != 0)
                taalDataGrid.ScrollIntoView(taalDataGrid.SelectedItem);
        }

        private void taalDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdate();
                IsScroll = false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Control_LostFocus(object sender, RoutedEventArgs e)
        {
            if (taalDataGrid.SelectedIndex > -1)
            {
                Taal taal = (Taal)taalDataGrid.SelectedItem;
                taal.Naam = naamTextBox.Text;
                taal.Code = codeTextBox.Text;
                TaalServices service = new TaalServices();
                try
                {
                    service.SaveTaalWijzigingen(taal);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            //if (gewijzigdeTalen.Contains(taal))
            //{
            //    int pos = 0;
            //    foreach (Taal t in gewijzigdeTalen)
            //    {
            //        if (t.ID == taal.ID)
            //            break;
            //        else pos++;
            //    }
            //    gewijzigdeTalen.RemoveAt(pos);
            //    gewijzigdeTalen.Add(taal);
            //}
            //else
            //    gewijzigdeTalen.Add(taal);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gewijzigdeTalen.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveWijzigingen();
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
                ZoekInLijst();
        }

        private void ZoekInLijst()
        {
            gevondenTalen.Clear();

            gevondenTalen = (from t in Talen
                             where t.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower())
                             select t).ToList();

            if (gevondenTalen.Count > 0)
            {
                taalViewSource.Source = gevondenTalen;
            }
            else
            {
                taalViewSource.Source = Talen;
            }
            goUpdate();
        }

        private void gridTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Taal taal = (Taal)taalDataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "gridNaamTextBox":
                        taal.Naam = text.Text;
                        break;
                    case "gridCodeTextBox":
                        taal.Code = text.Text;
                        break;
                }
                try
                {
                    TaalServices service = new TaalServices();
                    service.SaveTaalWijzigingen(taal);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

    }
}

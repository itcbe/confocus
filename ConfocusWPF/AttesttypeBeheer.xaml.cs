﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for AttesttypeBeheer.xaml
    /// </summary>
    public partial class AttesttypeBeheer : Window
    {
        private CollectionViewSource attestTypeViewSource;
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private List<AttestType> attesten = new List<AttestType>();
        private List<AttestType> gevondenAttesten = new List<AttestType>();
        private Gebruiker currentUser;

        public AttesttypeBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            attestTypeViewSource = ((CollectionViewSource)(this.FindResource("attestTypeViewSource")));
            LoadAttesttypes();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTypeViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTypeViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTypeViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTypeViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwAttesttype NA = new NieuwAttesttype(currentUser);
            if (NA.ShowDialog() == true)
            {
                LoadAttesttypes();
            }
        }

        private void LoadAttesttypes()
        {
            Attesttype_Services aService = new Attesttype_Services();
            attesten = aService.FindAll();
            attestTypeViewSource.Source = attesten;
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekInList();
        }

        private void ZoekInList()
        {
            String zoektext = ZoekTextBox.Text;
            if (zoektext != "")
            {
                gevondenAttesten.Clear();

                gevondenAttesten = (from a in attesten
                                    where a.Naam.ToLower().Contains(zoektext.ToLower())
                                    select a).ToList();

                attestTypeViewSource.Source = gevondenAttesten;
            }
            else
            {
                attestTypeViewSource.Source = attesten;
            }
        }

        private void GoUpdate()
        {
            if (attestTypeDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (attestTypeViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (attestTypeViewSource.View.CurrentPosition == attestTypeDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                attestTypeDataGrid.SelectedIndex = 0;
            if (attestTypeDataGrid.Items.Count != 0)
                attestTypeDataGrid.ScrollIntoView(attestTypeDataGrid.SelectedItem);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void naamTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (attestTypeDataGrid.SelectedIndex > -1)
            {
                String naam = ((TextBox)sender).Text;
                AttestType attest = (AttestType)attestTypeDataGrid.SelectedItem;
                attest.Naam = naam;
                attest.Gewijzigd_door = currentUser.Naam;
                attest.Gewijzigd = DateTime.Now;
                SaveAndReload(attest);
            }
        }

        private void SaveAndReload(AttestType attest)
        {
            try
            {
                Attesttype_Services aService = new Attesttype_Services();
                aService.SaveAttesttypeChanges(attest);
                int index = attestTypeDataGrid.SelectedIndex;
                LoadAttesttypes();
                if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                    ZoekInList();
                attestTypeDataGrid.SelectedIndex = index;
                
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void naamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                if (attestTypeDataGrid.SelectedIndex > -1)
                {
                    String naam = ((TextBox)sender).Text;
                    AttestType attest = (AttestType)attestTypeDataGrid.SelectedItem;
                    attest.Naam = naam;
                    attest.Gewijzigd_door = currentUser.Naam;
                    attest.Gewijzigd = DateTime.Now;
                    SaveAndReload(attest);          
                }
            }
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (attestTypeDataGrid.SelectedIndex > -1)
            {
                AttestType attest = (AttestType)attestTypeDataGrid.SelectedItem;
                attest.Actief = ((CheckBox)sender).IsChecked == true;
                attest.Gewijzigd_door = currentUser.Naam;
                attest.Gewijzigd = DateTime.Now;
                Attesttype_Services aService = new Attesttype_Services();
                try
                {
                    aService.SaveAttesttypeChanges(attest);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for DagtypeBeheer.xaml
    /// </summary>
    public partial class DagtypeBeheer : Window
    {

        private CollectionViewSource dagTypeViewSource;
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private List<DagType> DagTypes = new List<DagType>();
        private List<DagType> gevondenDagTypes = new List<DagType>();
        private Gebruiker currentUser;

        public DagtypeBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            dagTypeViewSource = ((CollectionViewSource)(this.FindResource("dagTypeViewSource")));
            LoadDagTypes();
        }

        private void LoadDagTypes()
        {
            DagType_Services dService = new DagType_Services();
            DagTypes = dService.FindActive();
            dagTypeViewSource.Source = DagTypes;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            dagTypeViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            dagTypeViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            dagTypeViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            dagTypeViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwDagType ND = new NieuwDagType(currentUser);
            if (ND.ShowDialog() == true)
            {
                LoadDagTypes();
            }
        }

        private void GoUpdate()
        {
            if (dagTypeDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (dagTypeViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (dagTypeViewSource.View.CurrentPosition == dagTypeDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                dagTypeDataGrid.SelectedIndex = 0;
            if (dagTypeDataGrid.Items.Count != 0)
                dagTypeDataGrid.ScrollIntoView(dagTypeDataGrid.SelectedItem);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            String zoektext = ((TextBox)sender).Text;
            if (zoektext != "")
            {
                gevondenDagTypes.Clear();

                gevondenDagTypes = (from a in DagTypes
                                    where a.Naam.ToLower().Contains(zoektext.ToLower())
                                    select a).ToList();

                dagTypeViewSource.Source = gevondenDagTypes;
            }
            else
            {
                dagTypeViewSource.Source = DagTypes;
            }
        }

        private void naamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                if (dagTypeDataGrid.SelectedIndex > -1)
                {
                    String naam = ((TextBox)sender).Text;
                    DagType type = (DagType)dagTypeDataGrid.SelectedItem;
                    type.Naam = naam;
                    type.Gewijzigd_door = currentUser.Naam;
                    type.Gewijzigd = DateTime.Now;
                    DagType_Services dService = new DagType_Services();
                    try
                    {
                        dService.Update(type);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (dagTypeDataGrid.SelectedIndex > -1)
            {
                DagType type = (DagType)dagTypeDataGrid.SelectedItem;
                type.Actief = ((CheckBox)sender).IsChecked == true;
                type.Gewijzigd_door = currentUser.Naam;
                type.Gewijzigd = DateTime.Now;
                DagType_Services dService = new DagType_Services();
                try
                {
                    dService.Update(type);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                //LoadDagTypes();
            }
        }

        private void nederlandsTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                if (dagTypeDataGrid.SelectedIndex > -1)
                {
                    String naam = ((TextBox)sender).Text;
                    DagType type = (DagType)dagTypeDataGrid.SelectedItem;
                    type.Nederlands = naam;
                    type.Gewijzigd_door = currentUser.Naam;
                    type.Gewijzigd = DateTime.Now;
                    DagType_Services dService = new DagType_Services();
                    try
                    {
                        dService.Update(type);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                    //LoadDagTypes();
                }
            }
        }

        private void fransTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                if (dagTypeDataGrid.SelectedIndex > -1)
                {
                    String naam = ((TextBox)sender).Text;
                    DagType type = (DagType)dagTypeDataGrid.SelectedItem;
                    type.Frans = naam;
                    type.Gewijzigd_door = currentUser.Naam;
                    type.Gewijzigd = DateTime.Now;
                    DagType_Services dService = new DagType_Services();
                    try
                    {
                        dService.Update(type);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                    //LoadDagTypes();
                }
            }
        }


    }
}

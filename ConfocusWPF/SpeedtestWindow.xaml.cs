﻿using ADOClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SpeedtestWindow.xaml
    /// </summary>
    public partial class SpeedtestWindow : Window
    {
        private System.Windows.Data.CollectionViewSource functiesViewSource;
        private System.Windows.Data.CollectionViewSource filterFunctiesViewSource;
        public SpeedtestWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var watch = Stopwatch.StartNew();
                var manager = new ConfocusDBManager();
                List<FilterFunctie> functies = manager.GetFilterFuncties(new ConfocusClassLibrary.ContactSearchFilter());//GetFuncties();//new FunctieServices().FindAll();

                filterFunctiesViewSource.Source = functies;
                //functiesViewSource.View.MoveCurrentToFirst();
                watch.Stop();
                aantalLabel.Content = functies.Count + " functies";
                timerLabel.Content = watch.ElapsedMilliseconds;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var watch = Stopwatch.StartNew();
                List<Functies> functies = new FunctieServices().GetAll();
                functiesViewSource.Source = functies;
                functiesViewSource.View.MoveCurrentToFirst();
                watch.Stop();
                aantalLabel.Content = functies.Count + " functies";
                timerLabel.Content = watch.ElapsedMilliseconds;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            functiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functiesViewSource")));
            filterFunctiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("filterFunctiesViewSource")));
    }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            functiesViewSource.Source = new List<Functies>();
            filterFunctiesViewSource.Source = new List<FilterFunctie>();
        }
    }
}

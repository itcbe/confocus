﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweDeelnemer.xaml
    /// </summary>
    public partial class NieuweDeelnemer : Window
    {
        private Gebruiker currentUser;
        private Inschrijving myInschrijving;
        public NieuweDeelnemer(Gebruiker user, Inschrijving inschrijving)
        {
            InitializeComponent();
            this.currentUser = user;
            this.myInschrijving = inschrijving;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            CollectionViewSource deelnemerViewSource = ((CollectionViewSource)(this.FindResource("deelnemerViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            throw new NotImplementedException();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Deelnemer deelnemer = GetDeelnemerFromForm();
            if (deelnemer != null)
            {
                DeelnemerServices service = new DeelnemerServices();
                Deelnemer savedDeelnemer = service.SaveDeelnemer(deelnemer);
                if (savedDeelnemer != null)
                {
                    ConfocusMessageBox.Show("Deelnemer opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                    DialogResult = true;
                }
            }
        }

        private Deelnemer GetDeelnemerFromForm()
        {
            if (ISFormValid())
            {
                Deelnemer deelnemer = new Deelnemer();
                deelnemer.Inschrijving_ID = myInschrijving.ID;
                deelnemer.Contact_ID = ((Contact)volledigeNaamComboBox.SelectedItem).ID;
                //deelnemer.Onderdeel_ID = ((Onderdeel)onderdeelComboBox.SelectedItem).ID;
                deelnemer.Functie = functieTextBox.Text;
                deelnemer.Mail_ontvangen = mail_ontvangenCheckBox.IsChecked == true;
                deelnemer.Evaluatie_ontvangen = evaluatie_ontvangenCheckBox.IsChecked == true;
                deelnemer.Aanwezig = aanwezigCheckBox.IsChecked == true;
                deelnemer.Attest_opmaken = attest_opmakenComboBox.Text;
                deelnemer.Actief = actiefCheckBox.IsChecked == true;
                deelnemer.Aangemaakt = DateTime.Now;
                deelnemer.Aangemaakt_door = currentUser.Naam;

                return deelnemer;
            }
            else
                return null;
        }

        private bool ISFormValid()
        {
            bool valid = true;
            String errorText = "";
            if (volledigeNaamComboBox.SelectedIndex < 0)
            {
                errorText += "Er is geen Contact gekozen!\n";
                valid = false;
            }

            if (onderdeelComboBox.SelectedIndex < 0)
            {
                errorText += "Er is geen Onderdeel gekozen!\n";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errorText, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }
    }
}

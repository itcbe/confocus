﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConfocusWPF.Logger
{
    public static class Logger
    {
        private static string logFilePath = "F:\\LogFiles\\ConfocusWPFLog.txt";
        private static string baseDirectory = "F:\\LogFiles\\";

        public static void Log(string t)
        {
            try
            {
                if (!Directory.Exists(baseDirectory))
                    Directory.CreateDirectory(baseDirectory);
                if (!File.Exists(logFilePath))
                    File.Create(logFilePath);
                using (StreamWriter w = File.AppendText(logFilePath))
                {
                    w.WriteLine(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                    w.WriteLine("+ " + t);
                    w.WriteLine();
                }
            }
            catch (Exception ex)
            {
                ;             
            }
        }

        public static void Log(string file, string t)
        {
            try
            {
                if (!Directory.Exists(baseDirectory))
                    Directory.CreateDirectory(baseDirectory);
                string path = baseDirectory + file;
                if (!File.Exists(path))
                    File.Create(path);
                using (StreamWriter w = File.AppendText(path))
                {
                    w.WriteLine(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                    w.WriteLine("+ " + t);
                    w.WriteLine();
                }
            }
            catch (Exception ex)
            {
                ;
            }
        }
    }
}

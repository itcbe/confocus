﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Forms;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ConfiguratieBeheer.xaml
    /// </summary>
    public partial class ConfiguratieBeheer : Window
    {
        private Gebruiker currentUser;
        private string Filename = "";
        private Instelling _instelling;

        public ConfiguratieBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Configuratie voor " + currentUser.Naam;
            GetConfig();
        }

        private void GetConfig()
        {
            GebruikerService service = new GebruikerService();
            Filename = service.GetConfigFileName(currentUser.Naam);
            try
            {
                ContactConfig contactconfig = Helper.GetContactConfigFromXml(Filename);
                SetConfig(contactconfig);
                Instelling_Service iService = new Instelling_Service();
                _instelling = iService.Find();
                SetInstelling();
                SaveButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Probleem bij het openen van de instellingen!\n " + ex.Message);
            }
        }

        private void SetInstelling()
        {
            if (_instelling != null)
            {
                hostTextBox.Text = _instelling.FTPHost;
                gebruikersnaamTextBox.Text = _instelling.FTPGebruikersnaam;
                paswoordTextBox.Text = _instelling.FTPPaswoord;
                inschrijvingenpadTextBox.Text = _instelling.NieuweInschrijvingenPad;
                ftpBackupFolderTextBox.Text = _instelling.BackupFolder;
                useridTextBox.Text = _instelling.Flexmail_User_ID.ToString();
                usertokenTextBox.Text = _instelling.Flexmail_User_Token;
                folderTextBox.Text = _instelling.DataFolder;
            }
        }

        private void SetConfig(ContactConfig contactconfig)
        {
            // Contact
            ContactIdChk.IsChecked = contactconfig.ID;
            ContactVoornaamChk.IsChecked = contactconfig.Voornaam;
            ContactAchternaamChk.IsChecked = contactconfig.Achternaam;
            ContactGeboortedatumChk.IsChecked = contactconfig.Geboortedatum;
            ContactAansprekingChk.IsChecked = contactconfig.Aanspreking;
            TaalChk.IsChecked = contactconfig.Taal;
            ContactNotitiesChk.IsChecked = contactconfig.ContactNotities;
            ContactAchternaamChk.IsChecked = contactconfig.ContactActief;
            // Bedrijf
            BedrijfChk.IsChecked = contactconfig.Bedrijf;
            BtwNummerChk.IsChecked = contactconfig.BTWnummer;
            TelefoonChk.IsChecked = contactconfig.Telefoon;
            FaxChk.IsChecked = contactconfig.Fax;
            EmailChk.IsChecked = contactconfig.Email;
            WebsiteChk.IsChecked = contactconfig.Website;
            AdresChk.IsChecked = contactconfig.Adres;
            PostcodeChk.IsChecked = contactconfig.Postcode;
            GemeenteChk.IsChecked = contactconfig.Gemeente;
            BedrijfNotitiesChk.IsChecked = contactconfig.BedrijfsNotities;
            BedrijfActiefChk.IsChecked = contactconfig.BedrijfActief;
            ContactGewijzigdChk.IsChecked = contactconfig.Gewijzigd;
            ContactGewijzigdDoorChk.IsChecked = contactconfig.GewijzigdDoor;
            ContactAangemaaktChk.IsChecked = contactconfig.Aangemaakt;
            ContactAangemaaktDoorChk.IsChecked = contactconfig.AangemaaktDoor;
            // Functie
            Niveau1Chk.IsChecked = contactconfig.Niveau1;
            Niveau2Chk.IsChecked = contactconfig.Niveau2;
            Niveau3Chk.IsChecked = contactconfig.Niveau3;
            AttesttypeChk.IsChecked = contactconfig.Attesttype;
            ErkenningsnummerChk.IsChecked = contactconfig.Erkenningsnummer;
            StartdatumChk.IsChecked = contactconfig.Startdatum;
            EinddatumChk.IsChecked = contactconfig.Einddatum;
            ContactTypeChk.IsChecked = contactconfig.Type;
            ContactMailChk.IsChecked = contactconfig.ContactEmail;
            ContactTelefoonChk.IsChecked = contactconfig.ContactTelefoon;
            NieuwsBriefMailChk.IsChecked = contactconfig.NieuwsbriefMail;
            NieuwsBriefFaxChk.IsChecked = contactconfig.NieuwsbriefFax;
            FunctieActiefChk.IsChecked = contactconfig.FunctieActief;

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContactConfig myConfig = GetConfigFromForm();
                Helper.SaveContactConfig(Filename, myConfig);
                GetInstelling();
                Instelling_Service iService = new Instelling_Service();
                iService.Update(_instelling);
                SaveButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Probleem bij het opslaan van de instellingen!\n" + ex.Message);
            }
        }

        private void GetInstelling()
        {
            if (_instelling != null)
            {
                _instelling.FTPHost = hostTextBox.Text;
                _instelling.FTPGebruikersnaam = gebruikersnaamTextBox.Text;
                _instelling.FTPPaswoord = paswoordTextBox.Text;
                _instelling.NieuweInschrijvingenPad = inschrijvingenpadTextBox.Text;
                _instelling.BackupFolder = ftpBackupFolderTextBox.Text;
                _instelling.Flexmail_User_ID = int.Parse(useridTextBox.Text);
                _instelling.Flexmail_User_Token = usertokenTextBox.Text;
                _instelling.DataFolder = folderTextBox.Text;
           }
        }

        private ContactConfig GetConfigFromForm()
        {
            ContactConfig contactconfig = new ContactConfig();
            // Contact
            contactconfig.ID = ContactIdChk.IsChecked == true;
            contactconfig.Voornaam = ContactVoornaamChk.IsChecked == true;
            contactconfig.Achternaam = ContactAchternaamChk.IsChecked == true ;
            contactconfig.Geboortedatum = ContactGeboortedatumChk.IsChecked == true ;
            contactconfig.Aanspreking = ContactAansprekingChk.IsChecked == true ;
            contactconfig.ContactNotities = ContactNotitiesChk.IsChecked == true;
            contactconfig.Taal = TaalChk.IsChecked == true;
            contactconfig.ContactActief = ContactActiefChk.IsChecked == true;
            // Bedrijf
            contactconfig.Bedrijf = BedrijfChk.IsChecked == true;
            contactconfig.BTWnummer = BtwNummerChk.IsChecked == true;
            contactconfig.Telefoon = TelefoonChk.IsChecked == true;
            contactconfig.Fax = FaxChk.IsChecked == true;
            contactconfig.Email = EmailChk.IsChecked == true;
            contactconfig.Website = WebsiteChk.IsChecked == true;
            contactconfig.Adres = AdresChk.IsChecked == true;
            contactconfig.Postcode = PostcodeChk.IsChecked == true;
            contactconfig.Gemeente = GemeenteChk.IsChecked == true;
            contactconfig.BedrijfActief = BedrijfActiefChk.IsChecked == true;
            contactconfig.BedrijfsNotities = BedrijfNotitiesChk.IsChecked == true;
            contactconfig.Gewijzigd = ContactGewijzigdChk.IsChecked == true ;
            contactconfig.GewijzigdDoor = ContactGewijzigdDoorChk.IsChecked == true ;
            contactconfig.Aangemaakt = ContactAangemaaktChk.IsChecked == true ;
            contactconfig.AangemaaktDoor = ContactAangemaaktDoorChk.IsChecked == true ;
            // Functie
            contactconfig.Niveau1 = Niveau1Chk.IsChecked == true;
            contactconfig.Niveau2 = Niveau2Chk.IsChecked == true;
            contactconfig.Niveau3 = Niveau3Chk.IsChecked == true;
            contactconfig.Attesttype = AttesttypeChk.IsChecked == true;
            contactconfig.Erkenningsnummer = ErkenningsnummerChk.IsChecked == true;
            contactconfig.Startdatum = StartdatumChk.IsChecked == true;
            contactconfig.Einddatum = EinddatumChk.IsChecked == true;
            contactconfig.Type = ContactTypeChk.IsChecked == true;
            contactconfig.ContactEmail = ContactMailChk.IsChecked == true;
            contactconfig.ContactTelefoon = ContactTelefoonChk.IsChecked == true;
            contactconfig.NieuwsbriefMail = NieuwsBriefMailChk.IsChecked == true;
            contactconfig.NieuwsbriefFax = NieuwsBriefFaxChk.IsChecked == true;
            contactconfig.FunctieActief = FunctieActiefChk.IsChecked == true;



            return contactconfig;
        }

        private void ChechBox_Click(object sender, RoutedEventArgs e)
        {
            SaveButton.IsEnabled = true;
        }

        private void FTPTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            SaveButton.IsEnabled = true;
        }

        private void browseInschrijvingenPadButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog OF = new FolderBrowserDialog();
            //OF.InitialDirectory = @"C:\Confocus Data\";
            DialogResult result = OF.ShowDialog();
            if (result.ToString() == "OK")
            {
                inschrijvingenpadTextBox.Text = OF.SelectedPath.ToString();
                SaveButton.IsEnabled = true;
            }
        }


        private void Grid_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }

        private void useridTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;
            if (SaveButton.IsEnabled == false)
                SaveButton.IsEnabled = true;
        }

        private void usertokenTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (SaveButton.IsEnabled == false)
                SaveButton.IsEnabled = true;
        }

        private void dataFolderButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog OF = new FolderBrowserDialog();
            //OF.InitialDirectory = @"C:\Confocus Data\";
            DialogResult result = OF.ShowDialog();
            if (result.ToString() == "OK")
            {
                folderTextBox.Text = OF.SelectedPath.ToString();
                SaveButton.IsEnabled = true;
            }
        }
    }
}

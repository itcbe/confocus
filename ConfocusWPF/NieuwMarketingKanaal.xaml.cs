﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwMarketingkanalen.xaml
    /// </summary>
    public partial class NieuwMarketingKanaal : Window
    {
        private Gebruiker currentUser;
        private Decimal? MarketingID;

        public NieuwMarketingKanaal(Gebruiker user, decimal? marketingId)
        {
            InitializeComponent();
            this.currentUser = user;
            this.MarketingID = marketingId;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (MarketingID != null)
            {
                MarketingkanalenServices mService = new MarketingkanalenServices();
                Marketingkanalen kanaal = mService.GetKanaalByID((Decimal)MarketingID);
                if (kanaal != null)
                {
                    BindKanaal(kanaal);
                }
            }
        }

        private void BindKanaal(Marketingkanalen kanaal)
        {
            this.Title = "Marketing kanaal : " + kanaal.Marketing_kanaal;
            marketing_kanaalTextBox.Text = kanaal.Marketing_kanaal;
            beschrijvingTextBox.Text = kanaal.Beschrijving;
            bereikTextBox.Text = kanaal.Bereik;
            sectorTextBox.Text = kanaal.Sector;
            contactpersoonTextBox.Text = kanaal.Contactpersoon;
            functieTextBox.Text = kanaal.Functie;
            overeenkomstTextBox.Text = kanaal.Overeenkomst;
            adverterenTextBox.Text = kanaal.Adverteren;
            opmerkingenTextBox.Text = kanaal.Opmerkingen;
            contacthistoriekTextBox.Text = kanaal.Contacthistoriek;
            telefoonTextBox.Text = kanaal.Telefoon;
            emailTextBox.Text = kanaal.Email;
            websiteTextBox.Text = kanaal.Website;
            aankondiging_vanTextBox.Text = kanaal.Aankondiging_van;
            feedbackTextBox.Text = kanaal.Feedback;
            taalComboBox.Text = kanaal.Taal;
            mobielNummerTextBox.Text = kanaal.Mobiel;
            statusTextBox.Text = kanaal.Status;
            actiefCheckBox.IsChecked = kanaal.Actief;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Marketingkanalen kanaal = GetKanaalFromForm();
            if (IsValidKanaal(kanaal))
            {
                MarketingkanalenServices kService = new MarketingkanalenServices();
                if (MarketingID != null)
                {
                    kanaal.ID = (Decimal)MarketingID;
                    kanaal.Gewijzigd = DateTime.Now;
                    kanaal.Gewijzigd_door = currentUser.Naam;

                    kService.UpdateMarketingkanaal(kanaal);
                    DialogResult = true;
                }
                else
                {
                    kanaal.Gemaakt = DateTime.Now;
                    kanaal.Gemaakt_door = currentUser.Naam;
                    try
                    {
                        kService.AddMarketingkanaal(kanaal);
                        DialogResult = true;
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                
            }
        }

        private bool IsValidKanaal(Marketingkanalen kanaal)
        {
            bool valid = true;

            if (String.IsNullOrEmpty(kanaal.Marketing_kanaal))
            {
                ConfocusMessageBox.Show("Het veld marketing kanaal is niet ingevuld!", ConfocusMessageBox.Kleuren.Rood);
                valid = false;
            }

            return valid;
        }

        private Marketingkanalen GetKanaalFromForm()
        {
            Marketingkanalen kanaal = new Marketingkanalen();

            kanaal.Marketing_kanaal = marketing_kanaalTextBox.Text;
            kanaal.Beschrijving = beschrijvingTextBox.Text;
            kanaal.Bereik = bereikTextBox.Text;
            kanaal.Sector = sectorTextBox.Text;
            kanaal.Contactpersoon = contactpersoonTextBox.Text;
            kanaal.Functie = functieTextBox.Text;
            kanaal.Overeenkomst = overeenkomstTextBox.Text;
            kanaal.Adverteren = adverterenTextBox.Text;
            kanaal.Opmerkingen = opmerkingenTextBox.Text;
            kanaal.Contacthistoriek = contacthistoriekTextBox.Text;
            kanaal.Telefoon = telefoonTextBox.Text;
            kanaal.Mobiel = mobielNummerTextBox.Text;
            kanaal.Email = emailTextBox.Text;
            kanaal.Website = websiteTextBox.Text;
            kanaal.Aankondiging_van = aankondiging_vanTextBox.Text;
            kanaal.Feedback = feedbackTextBox.Text;
            kanaal.Taal = taalComboBox.Text;
            kanaal.Status = statusTextBox.Text;
            kanaal.Actief = actiefCheckBox.IsChecked == true;

            return kanaal;
        }

        private void websiteTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TextBox box = (TextBox)sender;
                Helper.OpenUrl(box.Text);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }
    }
}

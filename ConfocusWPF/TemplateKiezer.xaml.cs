﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusDBLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for TemplateKiezer.xaml
    /// </summary>
    public partial class TemplateKiezer : Window
    {
        public AttestTemplate Template { get; set; }
        public String ZoekTerm { get; set; }

        public TemplateKiezer()
        {
            InitializeComponent();
        }

        private void TemplateListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.SelectedIndex > -1)
            {
                Template = (AttestTemplate)box.SelectedItem;
                DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<AttestTemplate> templates = new AttestTemplate_Service().GetByName(ZoekTerm);
            TemplateListBox.ItemsSource = templates;
        }

        private void TemplateListBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                DialogResult = false;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                DialogResult = false;
        }
    }
}

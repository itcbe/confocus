﻿using System;
using System.Net.Mail;
using System.Windows;
using data = ConfocusDBLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ZoekVervangWindow.xaml
    /// </summary>
    public partial class ZoekVervangWindow : Window
    {
        public data.ReplaceEmail replaceEmail = new data.ReplaceEmail();

        public ZoekVervangWindow()
        {
            InitializeComponent();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void vervangButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsInputValid())
            {
                replaceEmail.searchText = zoekTextBox.Text;
                replaceEmail.replaceText = vervangTextBox.Text;
                DialogResult = true;
            }
        }

        private bool IsInputValid()
        {
            bool valid = true;
            string errortext = "";
            if (zoekTextBox.Text == "")// && IsEmail(zoekTextBox.Text))
            {
                valid = false;
                errortext += "Geen zoekterm ingegeven!\n";
            }

            //if (!IsEmail(zoekTextBox.Text))
            //{
            //    valid = false;
            //    errortext += "Zoekterm is geen emailadres!";
            //}

            if(!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private bool IsEmail(string p)
        {
            try
            {
                MailAddress m = new MailAddress(p);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            zoekTextBox.Focus();
        }
    }
}

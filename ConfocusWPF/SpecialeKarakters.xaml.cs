﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SpecialeKarakters.xaml
    /// </summary>
    public partial class SpecialeKarakters : Window
    {

        private CollectionViewSource SpecialeTekensViewSource;
        private List<Speciaal_teken> tekens;
        private bool IsScroll;
        private bool IsButtonClick;
        private bool datagridChange;

        public SpecialeKarakters()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SpecialeTekensViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("SpecialeTekensViewSource")));
            tekens = new Speciale_Tekens_Services().GetAll();
            SpecialeTekensViewSource.Source = tekens;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            SpecialeTekensViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            SpecialeTekensViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            SpecialeTekensViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            SpecialeTekensViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwSpeciaalTeken NT = new NieuwSpeciaalTeken();
            if (NT.ShowDialog() == true)
            {
                ReLoadData();
            }
        }

        private void ReLoadData()
        {
            tekens = new Speciale_Tekens_Services().GetAll();
            SpecialeTekensViewSource.Source = tekens;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Speciaal_teken teken = (Speciaal_teken)speciaalTekenDataGrid.SelectedItem;
                teken.Alternatief = alternatiefTextBox.Text;
                teken.Speciaal_karakter = speciaal_karakterTextBox.Text;
                new Speciale_Tekens_Services().Update(teken);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                var results = (from t in tekens where t.Speciaal_karakter == ZoekTextBox.Text select t).ToList();
            }
        }

        private void GoUpdate()
        {
            if (speciaalTekenDataGrid.Items.Count <= 1) 
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (SpecialeTekensViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (SpecialeTekensViewSource.View.CurrentPosition == speciaalTekenDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                speciaalTekenDataGrid.SelectedIndex = 0;
            if (speciaalTekenDataGrid.Items.Count != 0)
                if (speciaalTekenDataGrid.SelectedItem != null)
                    speciaalTekenDataGrid.ScrollIntoView(speciaalTekenDataGrid.SelectedItem);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

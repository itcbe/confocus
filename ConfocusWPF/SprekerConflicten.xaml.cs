﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SprekerConflicten.xaml
    /// </summary>
    public partial class SprekerConflicten : Window
    {
        public Spreker GekozenSpreker { get; set; }
        private List<Sessie_Spreker> _sessiesprekers;
        private Gebruiker _currentUser;
        public SprekerConflicten(Gebruiker user, List<Sessie_Spreker> sprekers)
        {
            InitializeComponent();
            _sessiesprekers = sprekers;
            _currentUser = user;
        }

        private void ZoekSprekerButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekSprekerWindow ZS = new ZoekSprekerWindow();
            if (ZS.ShowDialog() == true)
            {
                GekozenSpreker = ZS.Spreker;
                SetSprekerToForm();
            }
        }

        private void SetSprekerToForm()
        {
            if (GekozenSpreker != null)
            {
                NaamLabel.Content = GekozenSpreker.NaamEnNiveaus;
                BedrijfLabel.Content = GekozenSpreker.Bedrijf.Naam;
                FunctieLabel.Content = GekozenSpreker.Oorspronkelijke_Titel;
                N1Label.Content = GekozenSpreker.Niveau1naam;             
                N2Label.Content = GekozenSpreker.Niveau2naam;
                N3Label.Content = GekozenSpreker.Niveau3naam;
                EmailLabel.Content = GekozenSpreker.Email;
            }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
        }

        private void AanpasButton_Click(object sender, RoutedEventArgs e)
        {
            SessieSprekerServices sService = new SessieSprekerServices();
            foreach (Sessie_Spreker spreker in _sessiesprekers)
            {
                spreker.Spreker_ID = GekozenSpreker.ID;
                sService.Update(spreker);
            }
            ConfocusMessageBox.Show("Sessies aangepast.", ConfocusMessageBox.Kleuren.Rood);
            this.DialogResult = true;
        }

        private void CanselButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

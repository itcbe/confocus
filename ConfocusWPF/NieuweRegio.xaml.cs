﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweRegio.xaml
    /// </summary>
    public partial class NieuweRegio : Window
    {
        private Gebruiker currentUser;

        public NieuweRegio(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(regioTextBox.Text))
                {
                    Regio regio = new Regio();
                    regio.Regio1 = regioTextBox.Text;
                    regio.Actief = true;
                    regio.Aangemaakt = DateTime.Now;
                    regio.Gewijzigd = DateTime.Now;
                    regio.Gewijzigd_door = currentUser.Naam;
                    regio.Aangemaakt_door = currentUser.Naam;

                    new RegioService().Save(regio);
                    DialogResult = true;
                }
                else
                    ConfocusMessageBox.Show("Geen regio ingegeven!!!", ConfocusMessageBox.Kleuren.Rood);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van de regio!!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            regioTextBox.Focus();
        }
    }
}

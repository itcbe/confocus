﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ITCLibrary;
using System.Windows.Media.Animation;
using System.Collections.ObjectModel;
using ADOClassLibrary;
using System.Data.Objects.SqlClient;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Private members
        /// </summary>
        private Gebruiker currentUser;
        private Int32 UserID;
        private CollectionViewSource contactViewSource;
        private Boolean IsScroll = false, IsButtonClick = false, IsUserClick = false;
        private CollectionViewSource functiesViewSource;
        private CollectionViewSource bedrijfViewSource;
        private CollectionViewSource erkenningViewSource;
        private List<FilterFunctie> _filterFuncties;
        private ObservableCollection<Functies> functies = new ObservableCollection<Functies>();
        private List<Functies> gevondenfuncties = new List<Functies>();
        private string _dataFolder;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="currentuser">De constructor heeft de currentuser doorgekregen van het loginscherm en representeerd de ingelogde user.
        /// Deze is nodig om de rechten van het programma in te stellen en de aangemaakt door en gewijzigd door velden in de tabellen</param>
        public MainWindow(Gebruiker currentuser)
        {
            InitializeComponent();
            currentUser = currentuser;
            UserID = currentUser.ID;
            this.Title = "Zoek naar klant/prospect : Ingelogd als " + currentUser.Naam;
            aansprekingColumn.Visibility = System.Windows.Visibility.Hidden;
            // Zet de kolomkoppen volgens het configuratie bestand
            SetKolomkoppen();
            // Pas de view aan volgens de rechten van de ingelogde user
            ControleerMachtigingen();
            _dataFolder = new Instelling_Service().Find().DataFolder;
        }

        /// <summary>
        /// Controleerd de machtigingen van de ingelogde user en past de view aan
        /// </summary>
        private void ControleerMachtigingen()
        {
            if (currentUser.Rol != 1)
            {
                GebruikersRollenMenuItem.Visibility = Visibility.Collapsed;
                ImportMarketingMenuItem.Visibility = Visibility.Collapsed;
                excel.Visibility = Visibility.Collapsed;
                doorPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                GebruikersRollenMenuItem.Visibility = Visibility.Visible;
                ImportMarketingMenuItem.Visibility = Visibility.Collapsed;
                excel.Visibility = Visibility.Visible;
                doorPanel.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Leest het configuratie bestand uit en verbergt of toont de kolomkoppen.
        /// Deze instelling wordt voornamelijk gebruikt bij het exporteren van de contacten naar CSV
        /// </summary>
        private void SetKolomkoppen()
        {
            try
            {
                GebruikerService service = new GebruikerService();
                String Filename = service.GetConfigFileName(currentUser.Naam);

                if (Filename != "")
                {
                    ContactConfig config = Helper.GetContactConfigFromXml(Filename);
                    iDColumn.Visibility = GetKolomVisibility(config.ID);
                    voornaamColumn.Visibility = GetKolomVisibility(config.Voornaam);
                    achternaamColumn.Visibility = GetKolomVisibility(config.Achternaam);
                    geboortedatumColumn.Visibility = GetKolomVisibility(config.Geboortedatum);
                    aansprekingColumn.Visibility = GetKolomVisibility(config.Aanspreking);
                    ContactEmailColumn.Visibility = GetKolomVisibility(config.ContactEmail);
                    ContactTelefoonColumn.Visibility = GetKolomVisibility(config.ContactTelefoon);
                    ContactNotitiesColumn.Visibility = GetKolomVisibility(true);
                    typeColumn.Visibility = GetKolomVisibility(config.Type);
                    taalColumn.Visibility = GetKolomVisibility(config.Taal);
                    niveau1Column.Visibility = GetKolomVisibility(config.Niveau1);
                    niveau2Column.Visibility = GetKolomVisibility(config.Niveau2);
                    niveau3Column.Visibility = GetKolomVisibility(config.Niveau3);
                    bedrijfColumn.Visibility = GetKolomVisibility(config.Bedrijf);
                    TelefoonColumn.Visibility = GetKolomVisibility(config.Telefoon);
                    faxColumn.Visibility = GetKolomVisibility(config.Fax);
                    bedrijfEmailColumn.Visibility = GetKolomVisibility(config.Email);
                    adresColumn.Visibility = GetKolomVisibility(config.Adres);
                    postcodeColumn.Visibility = GetKolomVisibility(config.Postcode);
                    GemeenteColumn.Visibility = GetKolomVisibility(config.Gemeente);
                    notitiesColumn.Visibility = GetKolomVisibility(config.ContactNotities);
                    nieuwsbriefMailColumn.Visibility = GetKolomVisibility(config.NieuwsbriefMail);
                    nieuwsbriefFaxColumn.Visibility = GetKolomVisibility(config.NieuwsbriefFax);
                    gewijzigdColumn.Visibility = GetKolomVisibility(config.Gewijzigd);
                    gewijzigdDoorColumn.Visibility = GetKolomVisibility(config.GewijzigdDoor);
                    aangemaaktColumn.Visibility = GetKolomVisibility(config.Aangemaakt);
                    aangemaaktdoorColumn.Visibility = GetKolomVisibility(config.AangemaaktDoor);
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private Visibility GetKolomVisibility(bool b)
        {
            if (!b)
                return Visibility.Hidden;
            return Visibility.Visible;
        }

        /* ___________________________________________
         * |                                          |
         * |             Menu aanroepen               |
         * |__________________________________________|
         * */

        private void AansprekingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AansprekingBeheer AB = new AansprekingBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }

        private void BedrijvenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            BedrijfBeheer BB = new BedrijfBeheer(currentUser);
            BB.Show();
            BB.Activate();
        }

        private void ErkenningMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ErkenningBeheer EB = new ErkenningBeheer(currentUser, null);
            EB.Show();
            EB.Activate();
        }

        private void FunctiesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            InschrijvingenService iService = new InschrijvingenService();
            iService.UpdateTaantal();
            //HumanResourceWindow HW = new HumanResourceWindow(currentUser);
            //HW.Show();
            //HW.Activate();
        }

        private void NiveausMenuItem_Click(object sender, RoutedEventArgs e)
        {
            NiveauBeheer NB = new NiveauBeheer(currentUser);
            NB.Show();
            NB.Activate();
        }

        private void SprekerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SprekerBeheer SB = new SprekerBeheer(currentUser);
            SB.Show();
            SB.Activate();
        }

        private void TalenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            TaalBeheer TB = new TaalBeheer(currentUser);
            TB.Show();
            TB.Activate();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Wil je het programma sluiten?", "Afsluiten", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void GebruikerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GebruikerBeheer GB = new GebruikerBeheer(currentUser);
            GB.Show();
            GB.Activate();
        }

        private void ContactMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ContactServices cService = new ContactServices();
            List<Contact> contacten = cService.FindAll();
            foreach (Contact contact in contacten)
            {
                cService.SaveContactChanges(contact);
            }
            //ContactBeheer CB = new ContactBeheer(currentUser);
            //CB.Show();
        }

        private void InportPostcodesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportPostcodes IP = new ImportPostcodes();
            IP.Show();
            IP.Activate();
        }

        private void LocatieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            LocatieBeheer LB = new LocatieBeheer(currentUser);
            LB.Show();
            LB.Activate();
        }

        private void OnderwerpMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnderwerpBeheer OB = new OnderwerpBeheer(currentUser);
            OB.Show();
            OB.Activate();
        }

        private void SeminarieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SeminarieBeheer SB = new SeminarieBeheer(currentUser);
            SB.Show();
            SB.Activate();
        }

        private void GebruikersRollenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GebruikersRolen GR = new GebruikersRolen(currentUser);
            GR.Show();
            GR.Activate();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            contactViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("contactViewSource")));
            functiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functiesViewSource")));
            bedrijfViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("bedrijfViewSource")));
            erkenningViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("erkenningViewSource")));

            LoadData();
        }

        /********************************************
         *               Data ophalen               *
         *******************************************/

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;
            // Laad de weergaven in de filtercombobox
            LoadWeergaveComboBox();
            // Laad de Niveaus in de Niveau 1 filtercombobox
            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> n1Lijst = n1Service.FindActive();
            Niveau1 n1 = new Niveau1();
            n1.Naam = "Kies";
            n1Lijst.Insert(0, n1);
            niveau1ComboBox.ItemsSource = n1Lijst;
            niveau1ComboBox.DisplayMemberPath = "Naam";
            niveau1ComboBox.SelectedIndex = 0;
            // Laad de andere niveau's 
            LoadNiveau2En3();
            // Laad de postcodes en gemeentes in de Gemeente ComboBox filter
            PostcodeService postService = new PostcodeService();
            List<Postcodes> gemeentes = postService.FindAllGemeentes();
            Postcodes gemeente = new Postcodes();
            gemeente.Postcode = "Kies";
            gemeente.Gemeente = "Kies";
            gemeentes.Insert(0, gemeente);
            LocatieComboBox.ItemsSource = gemeentes;
            LocatieComboBox.DisplayMemberPath = "Gemeente";
            LocatieComboBox.SelectedIndex = 0;
            // Laad de default email opties
            List<string> emailoptions = new List<string>() { "Kies", "Leeg" };
            emailComboBox.ItemsSource = emailoptions;
            emailComboBox.SelectedIndex = 0;
            //Laad de taal opties
            TaalServices tService = new TaalServices();
            List<Taal> talen = tService.FindAll();
            Taal taal = new Taal();
            taal.Naam = "Kies";
            talen.Insert(0, taal);
            languageComboBox.ItemsSource = talen;
            languageComboBox.DisplayMemberPath = "Naam";
            languageComboBox.SelectedIndex = 0;
            //Laad de gebruikers is de door combobox
            List<Gebruiker> doorgebruikers = new GebruikerService().GetAvtiveGebruikers();
            Gebruiker dummy = new Gebruiker();
            dummy.Naam = "";
            doorgebruikers.Insert(0, dummy);
            doorComboBox.ItemsSource = doorgebruikers;
            doorComboBox.DisplayMemberPath = "Naam";
        }

        /// <summary>
        /// Herladen van de contacten
        /// </summary>
        /// <param name="functie"></param>
        private void ReLoadContacten(FilterFunctie functie)
        {
            ActivateFilters(functie);
        }

        /// <summary>
        /// Het laden van de contacten in een aparte tread.
        /// Deze functie is niet meer van toepassing door het wijzigen van de filteropties naar 
        /// een Stored Procedure
        /// </summary>
        /// <param name="functie"></param>
        private void LoadContacten(Functies functie)
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
                FunctieServices functieService = new FunctieServices();
                functies = functieService.FindActive();
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactViewSource.SetValue(CollectionViewSource.SourceProperty, functies); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
                if (functie != null)
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactDataGrid.SetValue(DataGrid.SelectedItemProperty, functie); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate
                {
                    ActivateFilters(null);
                }, null);

            }
            catch (InvalidOperationException ex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ShowError(ex); }, null);
            }
            catch (Exception nex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ShowError(nex); }, null);
            }
        }

        private void ShowError(Exception ex)
        {
            if (ex is InvalidOperationException)
                ConfocusMessageBox.Show("Fout bij het filteren van de contacten!\n" + ((InvalidOperationException)ex).Message, ConfocusMessageBox.Kleuren.Rood);
            else
                ConfocusMessageBox.Show("Fout bij het filteren van de contacten!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
        }

        /// <summary>
        /// Start de loader animatie
        /// </summary>
        private void StartAnimation()
        {
            reloadprogressBar.SetValue(ProgressBar.ValueProperty, 0.0);
            Duration duration = new Duration(TimeSpan.FromSeconds(25));
            DoubleAnimation doubleanimation = new DoubleAnimation(200.0, duration);
            reloadprogressBar.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);
        }

        /// <summary>
        /// Laad de weergaven in de combobox
        /// </summary>
        private void LoadWeergaveComboBox()
        {
            WeergaveServices weergaveService = new WeergaveServices();
            List<Weergave> weergaven = weergaveService.GetWeergavesByUserId(UserID);
            Weergave gave = new Weergave();
            gave.Naam = "Kies";
            weergaven.Insert(0, gave);
            WeergavenComboBox.ItemsSource = weergaven;
            WeergavenComboBox.DisplayMemberPath = "Naam";
            WeergavenComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Laad de default gegevens in de Niveau 2 en 3 comboboxen
        /// </summary>
        private void LoadNiveau2En3()
        {
            List<Niveau2> n2lijst = new List<Niveau2>();
            Niveau2 n2 = new Niveau2();
            n2.Naam = "Leeg";
            n2lijst.Add(n2);
            niveau2ComboBox.ItemsSource = n2lijst;
            niveau2ComboBox.DisplayMemberPath = "Naam";

            List<Niveau3> n3lijst = new List<Niveau3>();
            Niveau3 n3 = new Niveau3();
            n3.Naam = "Leeg";
            n3lijst.Add(n3);
            niveau3ComboBox.ItemsSource = n3lijst;
            niveau3ComboBox.DisplayMemberPath = "Naam";
        }

        /*************************************************
         *              Toolbar functies                 *
         ************************************************/
        /// <summary>
        /// First button klik event
        /// Navigeert de contacten datagrid naar zijn eerste positie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        /// <summary>
        /// Previous button klik event
        /// navigeert naar het vorige item in de datagrid als deze er is.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        /// <summary>
        /// Next button klik event
        /// Navigeert naar het volgende item in de datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        /// <summary>
        /// Last button klik event
        /// Navigeert naar het laatste contact in de datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        /// <summary>
        /// Methode / event om de icoontjes in de knoppen oranje te alten oplichten als men over de knop gaat met de cursor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "excel":
                    img = excelImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "flex":
                    img = flexImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        /// <summary>
        /// Methode / event om de icoontjes in de knoppen wit te laten oplichten als men de knop verlaat met de cursor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "excel":
                    img = excelImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "flex":
                    img = flexImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));

        }

        /// <summary>
        /// Method om de navigatie knoppen hun status te laten weergeven
        /// vb. Als de positie in de datagrid op de eerste plaats staat moeten de knoppen vorige en eeste 
        /// onbeschikbaar zijn enz...
        /// </summary>
        private void goUpdate()
        {
            if (contactDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (contactViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (contactViewSource.View.CurrentPosition == contactDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                contactDataGrid.SelectedIndex = 0;
            if (contactDataGrid.Items.Count != 0)
                contactDataGrid.ScrollIntoView(contactDataGrid.SelectedItem);
        }

        /// <summary>
        /// Klik event voor de filterknop
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ActivateFilters(null);
        }

        /// <summary>
        /// Key up event voor de voor en achternaam textboxen
        /// Als men enter of return klikt moet de filter al starten.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zoekNaamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ActivateFilters(null);
            }
        }

        /// <summary>
        /// Method die de filteropties ophaald en in een nieuwe thread doorgeeft aan GetContacten
        /// Hierdoor kan de loader animatie blijven lopen
        /// </summary>
        /// <param name="functie"></param>
        private void ActivateFilters(FilterFunctie functie)
        {
            gevondenfuncties.Clear();
            ContactSearchFilter filter = GetContactFilter();
            //Logger.Logger.Log("ConfocusZoekLog.txt", "Zoeken naar: " + filter.Voornaam + " " + filter.Achternaam + ".");
            new Thread(delegate () { GetContacten(filter, functie); }).Start();
            SetAantal();
            goUpdate();

        }

        /// <summary>
        /// Method die de unieke contacten telt. 
        /// Hiervoor wordt gebruikgemaakt van het contact ID
        /// </summary>
        /// <param name="list"></param>
        private void GetUniekeContacten(List<FilterFunctie> list)
        {
            int uCount = list.Select(u => u.Contact_ID).Distinct().Count();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate
            {
                UniekeLabel.SetValue(Label.ContentProperty, uCount + " unieke contacten.");
            }, null);
        }

        /// <summary>
        /// BAckground method om de filter door te sturen naar de stored procedure
        /// Zn de resultaten toe te wijzen aan de datagrid
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="functie"></param>
        private void GetContacten(ContactSearchFilter filter, FilterFunctie functie)
        {
            // Start de loader (maak deze zichtbaar
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
            List<FilterFunctie> results = new List<FilterFunctie>();
            try
            {
                // creeer een DB conectie en haal de gevonden resultaten
                var manager = new ConfocusDBManager();
                results = manager.GetFilterFuncties(filter);
                //start een background thread om de unieke contacten er uit te halen.
                new Thread(delegate () { GetUniekeContacten(results); }).Start();

            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }));
            }
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { _filterFuncties = results; }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactViewSource.SetValue(CollectionViewSource.SourceProperty, results); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
            if (functie != null)
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { contactDataGrid.SetValue(DataGrid.SelectedItemProperty, functie); }, null);
        }

        /// <summary>
        /// Verzamel alle filters
        /// </summary>
        /// <returns></returns>
        private ContactSearchFilter GetContactFilter()
        {
            ContactSearchFilter filter = new ContactSearchFilter();
            if (zoekVoorNaamTextBox.Text != "")
                filter.Voornaam = DBHelper.Simplify(zoekVoorNaamTextBox.Text);
            if (zoekAchterNaamTextBox.Text != "")
                filter.Achternaam = DBHelper.Simplify(zoekAchterNaamTextBox.Text);
            if (bedrijvenComboBox.SelectedIndex > 0)
            {
                filter.BedrijfsNaam = ((Bedrijf)bedrijvenComboBox.SelectedItem).Naam;
            }
            if (emailComboBox.Text != "Kies")
            {
                filter.Email = emailComboBox.Text;
            }
            if (typeComoBox.SelectedIndex > 0)
            {
                filter.ContactType = typeComoBox.Text;
            }
            if (niveau1ComboBox.SelectedIndex > 0)
            {
                filter.Niveau1Naam = ((Niveau1)niveau1ComboBox.SelectedItem).Naam;
            }
            if (niveau2ComboBox.SelectedIndex > 0)
            {
                filter.Niveau2Naam = ((Niveau2)niveau2ComboBox.SelectedItem).Naam;
            }
            if (niveau3ComboBox.SelectedIndex > 0)
            {
                filter.Niveau3Naam = ((Niveau3)niveau3ComboBox.SelectedItem).Naam;
            }
            if (LocatieComboBox.SelectedIndex > 0)
            {
                filter.Gemeente = LocatieComboBox.Text;
            }
            if (ProvincieComboBox.SelectedIndex > 0)
            {
                filter.Provincie = ProvincieComboBox.Text;
            }
            if (aangemaaktDatepicker.SelectedDate != null && aangemaaktDatepicker.SelectedDate > new DateTime(2014, 12, 1))
            {
                filter.Aangemaakt = (DateTime)aangemaaktDatepicker.SelectedDate;
            }
            if (languageComboBox.SelectedIndex > 0)
            {
                filter.TaalNaam = ((Taal)languageComboBox.SelectedItem).Naam;
            }
            if (faxnummerTextBox.Text != "")
            {
                filter.Faxnummer = faxnummerTextBox.Text;
            }
            if (doorComboBox.SelectedIndex > 0)
            {
                filter.Door = ((Gebruiker)doorComboBox.SelectedItem).Naam;
            }
            return filter;
        }

        /// <summary>
        /// event: Vul de ontbrekende details aan bij wijzigen geselecteerd contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void contactDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactDataGrid.SelectedItem is FilterFunctie)
            {
                try
                {
                    FilterFunctie functie = (FilterFunctie)contactDataGrid.SelectedItem;
                    if (functie != null)
                    {
                        if (functie.ContactGewijzigd == null)
                            ContactAangemaakLabelt.Content = "Aangemaakt op " + functie.ContactAangemaakt + " door " + functie.ContactAangemaakt_door + ".";
                        else
                            ContactAangemaakLabelt.Content = "Gewijzigd op " + functie.ContactGewijzigd + " door " + functie.ContactGewijzigd_door + ".";

                        if (functie.Gewijzigd == null)
                        {
                            FunctieAangemaaktLabel.Content = "Aangemaakt op " + functie.Aangemaakt + " door " + functie.Aangemaakt_door + ".";
                        }
                        else
                        {
                            FunctieAangemaaktLabel.Content = "Gewijzigd op " + functie.Gewijzigd + " door " + functie.Gewijzigd_door + ".";
                        }
                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het tonen van het selecteerde contact!\n " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdate();
                IsScroll = false;
            }
            SetAantal();
        }

        /// <summary>
        /// Event van de reset knop
        /// Maakt de filter leeg
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            MaakFiltersLeeg();
            WeergavenComboBox.SelectedIndex = 0;
            contactViewSource.Source = functies;
            SetAantal();
        }

        /// <summary>
        /// hulpmethod voor het leegmaken van de filters
        /// </summary>
        private void MaakFiltersLeeg()
        {
            bedrijvenComboBox.SelectedIndex = 0;
            List<string> emailoptions = new List<string>() { "Kies", "Leeg" };
            emailComboBox.ItemsSource = emailoptions;
            emailComboBox.SelectedIndex = 0;
            typeComoBox.SelectedIndex = 0;
            niveau1ComboBox.SelectedIndex = 0;
            LoadNiveau2En3();
            LocatieComboBox.SelectedIndex = 0;
            ProvincieComboBox.SelectedIndex = 0;
            aangemaaktDatepicker.SelectedDate = null;
            zoekVoorNaamTextBox.Text = "";
            zoekAchterNaamTextBox.Text = "";
            faxnummerTextBox.Text = "";
            languageComboBox.SelectedIndex = 0;
            doorComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Toont het aantal contacten gevonden door de filter
        /// </summary>
        private void SetAantal()
        {
            if (contactDataGrid.Items.Count > 1)
            {
                AantalLabel.Content = (contactDataGrid.SelectedIndex + 1) + " van " + (contactDataGrid.Items.Count) + " contacten.";
            }
            else
                AantalLabel.Content = "0 van 0 contacten.";
        }

        /// <summary>
        /// Wissel account menu event
        /// Sluit het huidige scherm en staart het loginscherm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WisselAccountMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Splash SS = new Splash();
            SS.Show();
            this.Close();
        }

        /// <summary>
        /// Configuratie menuitem event
        /// Opent het configuratie scherm
        /// Na sluiten van het configuratie scherm worden de kolomkoppen aangepast.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ConfiguratieBeheer CB = new ConfiguratieBeheer(currentUser);
            if (CB.ShowDialog() == true)
            {
                SetKolomkoppen();
            }
        }

        /// <summary>
        /// Nieuw contact knop event
        /// Opent het nieuw contactscherm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NieuwContact NC = new NieuwContact(currentUser, "Functie");
                if (NC.ShowDialog() == true)
                {
                    Functies functie = NC.SelectedFunctie;
                    FilterFunctie func = (from f in _filterFuncties where f.ID == functie.ID select f).FirstOrDefault();
                    ActivateFilters(func);
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het openen van nieuw contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            if (contactDataGrid.SelectedIndex > -1)
            {
                try
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    //BewerkContact BC = new BewerkContact(Username, functie);
                    NieuwContact BC = new NieuwContact(currentUser, functie);
                    if (BC.ShowDialog() == true)
                    {
                        LoadData();
                    }

                }
                catch (Exception ex)
                {

                    ConfocusMessageBox.Show("Fout bij het laden van bewerkscherm!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void ZoekSprekerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ZoekSprekerScherm ZS = new ZoekSprekerScherm(currentUser);
            ZS.Show();
            ZS.Activate();
            this.Close();
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            contactDataGrid.Cursor = Cursors.Wait;
            //LoadData();
            ReLoadContacten(null);
            contactDataGrid.Cursor = Cursors.Arrow;
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Niveau1 n1 = (Niveau1)box.SelectedItem;

                Niveau2Services service = new Niveau2Services();
                List<Niveau2> n2lijst = service.GetSelected(n1.ID);
                Niveau2 n2 = new Niveau2();
                n2.Naam = "Leeg";
                n2lijst.Insert(0, n2);
                niveau2ComboBox.ItemsSource = n2lijst;
                niveau2ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau2 n2 = (Niveau2)box.SelectedItem;

                Niveau3Services service = new Niveau3Services();
                List<Niveau3> n3lijst = service.GetSelected(n2.ID);
                Niveau3 n3 = new Niveau3();
                n3.Naam = "Leeg";
                n3lijst.Insert(0, n3);
                niveau3ComboBox.ItemsSource = n3lijst;
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void ToExcel(string url)
        {
            int lengte = contactDataGrid.Items.Count;
            String[] headers = GetActiveHeaders();
            String[][] contacten = new String[lengte][];
            int teller = 0;
            foreach (Object item in contactDataGrid.Items)
            {
                if (item is Functies)
                {
                    Functies functie = item as Functies;

                    String[] lid = GetActiveData(functie, headers);
                    contacten[teller] = lid;
                    teller++;
                }
                else if (item is FilterFunctie)
                {
                    FilterFunctie functie = item as FilterFunctie;
                    String[] lid = GetActiveData(functie, headers);
                    contacten[teller] = lid;
                    teller++;
                }
            }

            // Naar CSV
            List<string> csvheaders = new List<string>();
            foreach (string header in headers)
            {
                csvheaders.Add(header);
            }
            CSVDocument doc = new CSVDocument(url);
            doc.AddRow(csvheaders);
            foreach (string[] data in contacten)
            {
                List<string> conta = new List<string>();
                foreach (string waarde in data)
                {
                    conta.Add(waarde);

                }
                doc.AddRow(conta);
            }
            doc.Save();
            System.Diagnostics.Process.Start(url);

            //Naar Excel
            //ExcelDocument Xl = new ExcelDocument();
            //Xl.SetColumnHeaders(headers, 1, true, true, true);
            //Xl.InsertList(contacten, true, 2);
            //object misValue = System.Reflection.Missing.Value;

        }

        private string[] GetActiveData(FilterFunctie functie, string[] headers)
        {
            List<String> myFunctie = new List<string>();
            if (headers.Contains("Contact ID"))
                myFunctie.Add(functie.Contact_ID.ToString());
            if (headers.Contains("Voornaam"))
                myFunctie.Add(functie.Voornaam);
            if (headers.Contains("Achternaam"))
                myFunctie.Add(functie.Achternaam);
            if (headers.Contains("Geboortedatum"))
            {
                if (functie.Geboortedatum != null)
                    myFunctie.Add(((DateTime)functie.Geboortedatum).ToShortDateString());
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Geslacht"))
                myFunctie.Add(functie.Aanspreking);
            if (headers.Contains("Type"))
                myFunctie.Add(functie.Type);
            if (headers.Contains("Contact notities"))
                myFunctie.Add(Helper.CleanForExcel(functie.Contactnotities));
            if (headers.Contains("Functie email"))
            {
                string email = functie.Email == null ? "" : functie.Email;
                email = Helper.CleanForExcel(email);
                myFunctie.Add(email);
            }
            if (headers.Contains("Functie telefoon"))
                myFunctie.Add(Helper.CleanForExcel(functie.Telefoon));
            if (headers.Contains("Functie mobiel"))
                myFunctie.Add(Helper.CleanForExcel(functie.Mobiel));
            if (headers.Contains("Contact actief"))
                myFunctie.Add(functie.ContactactiefAsString);
            if (headers.Contains("Taal"))
                if (functie.TaalNaam != null)
                    myFunctie.Add(functie.TaalNaam);
                else
                    myFunctie.Add("");
            if (headers.Contains("Functie ID"))
                myFunctie.Add(functie.ID.ToString());


            if (headers.Contains("Niveau1"))
                myFunctie.Add(functie.Niveau1Naam);
            if (headers.Contains("Niveau2"))
            {
                if (!string.IsNullOrEmpty(functie.Niveau2Naam))
                    myFunctie.Add(functie.Niveau2Naam);
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Niveau3"))
            {
                if (!string.IsNullOrEmpty(functie.Niveau3Naam))
                    myFunctie.Add(functie.Niveau3Naam);
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Oorspronkelijke titel"))
            {
                if (!string.IsNullOrEmpty(functie.Oorspronkelijke_Titel))
                    myFunctie.Add(Helper.CleanForExcel(functie.Oorspronkelijke_Titel));
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Attesttype"))
            {
                if (!string.IsNullOrEmpty(functie.Attesttype))
                    myFunctie.Add(functie.Attesttype);
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Erkenningsnummer"))
            {
                if (!string.IsNullOrEmpty(functie.Erkenningsnummer))
                    myFunctie.Add(Helper.CleanForExcel(functie.Erkenningsnummer));
                else
                    myFunctie.Add("");
            }

            if (headers.Contains("Bedrijf"))
            {
                string bedrijf = functie.Bedrijfsnaam == null ? "" : functie.Bedrijfsnaam;
                bedrijf = Helper.CleanForExcel(bedrijf);
                myFunctie.Add(bedrijf);
            }
            if (headers.Contains("Btwnummer"))
            {
                if (!string.IsNullOrEmpty(functie.Btwnummer))
                    myFunctie.Add(functie.Btwnummer);
                else
                    myFunctie.Add("");
            }

            if (headers.Contains("Bedrijf telefoon"))
            {
                string telefoon = functie.BedrijfsTelefoon == null ? "" : functie.BedrijfsTelefoon;
                telefoon = Helper.CleanForExcel(telefoon);
                myFunctie.Add(telefoon);
            }
            if (headers.Contains("Bedrijf fax"))
            {
                string fax = functie.BedrijfsFax == null ? "" : functie.BedrijfsFax;
                fax = Helper.CleanForExcel(fax);
                fax = fax.Replace('\n', ' ');
                myFunctie.Add(fax);
            }
            if (headers.Contains("Bedrijf email"))
            {
                string email = functie.BedrijfsEmail == null ? "" : functie.BedrijfsEmail;
                email = Helper.CleanForExcel(email);
                myFunctie.Add(email);
            }
            if (headers.Contains("Bedrijf website"))
            {
                string website = functie.Website == null ? "" : functie.Website;
                website = Helper.CleanForExcel(website);
                myFunctie.Add(website);
            }

            if (headers.Contains("Adres"))
            {
                string adres = functie.BedrijfsAdres == null ? "" : functie.BedrijfsAdres;
                adres = Helper.CleanForExcel(adres);
                myFunctie.Add(adres);
            }
            if (headers.Contains("Postcode"))
                myFunctie.Add(functie.BedrijfsPostcode);
            if (headers.Contains("Gemeente"))
                myFunctie.Add(functie.BedrijfsPlaats);
            if (headers.Contains("Land"))
                myFunctie.Add(functie.Land);

            if (headers.Contains("Bedrijf notities"))
            {
                string notitie = functie.Bedrijfsnotities == null ? "" : functie.Bedrijfsnotities;
                notitie = Helper.CleanForExcel(notitie);
                myFunctie.Add(notitie);
            }
            if (headers.Contains("Bedrijf actief"))
                myFunctie.Add(functie.BedrijfactiefAsString);

            if (headers.Contains("Nieuwsbrief via mail"))
                myFunctie.Add(functie.Mail == true ? "Ja" : "Nee");
            if (headers.Contains("Nieuwsbrief via fax"))
                myFunctie.Add(functie.Fax == true ? "Ja" : "Nee");
            if (headers.Contains("Functie actief"))
                myFunctie.Add(functie.FunctieActiefAsString);

            //if (headers.Contains("Inschrijving communicatie"))
            //{
            //    string info = "";
            //    try
            //    {
            //        InschrijvingenService iService = new InschrijvingenService();
            //        List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = iService.GetInschrijvingByFunctionID(functie.ID);
            //        bool Ccgevonden = false, Notitiegevonden = false, Facturatiegevonden = false;
            //        foreach (ConfocusDBLibrary.Inschrijvingen inschrijving in inschrijvingen)
            //        {
            //            if (!string.IsNullOrEmpty(inschrijving.CC_Email) && !Ccgevonden)
            //            {
            //                info += "CC mail: " + inschrijving.CC_Email + "- ";
            //                Ccgevonden = true;
            //            }
            //            if (!string.IsNullOrEmpty(inschrijving.Notitie) && !Notitiegevonden)
            //            {
            //                info += "Notitie: " + inschrijving.Notitie + "- ";
            //                Notitiegevonden = true;
            //            }
            //            if (!string.IsNullOrEmpty(inschrijving.FacturatieOpNaam) && !Facturatiegevonden)
            //            {
            //                info += "Facturatie op: " + inschrijving.FacturatieOpNaam + "- ";
            //                Facturatiegevonden = true;
            //            }

            //        }

            //    }
            //    catch (Exception )
            //    {
            //    }
            //    myFunctie.Add(Helper.CleanForExcel(info));
            //}

            if (headers.Contains("Gewijzigd"))
            {
                if (functie.ContactGewijzigd != null)
                    myFunctie.Add(((DateTime)functie.ContactGewijzigd).ToShortDateString());
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Gewijzigd door"))
                myFunctie.Add(functie.ContactGewijzigd_door);
            if (headers.Contains("Aangemaakt"))
            {
                if (functie.ContactAangemaakt != null)
                    myFunctie.Add(((DateTime)functie.ContactAangemaakt).ToShortDateString());
                else
                    myFunctie.Add("");
            }
            if (headers.Contains("Aangemaakt door"))
                myFunctie.Add(functie.ContactAangemaakt_door);

            return myFunctie.ToArray();

        }

        private string[] GetActiveData(Functies functie, string[] headers)
        {
            List<String> mySpreker = new List<string>();
            if (headers.Contains("ID"))
                mySpreker.Add(functie.Contact_ID.ToString());
            if (headers.Contains("Voornaam"))
                mySpreker.Add(functie.Contact.Voornaam);
            if (headers.Contains("Achternaam"))
                mySpreker.Add(functie.Contact.Achternaam);
            if (headers.Contains("Geboortedatum"))
            {
                if (functie.Contact.Geboortedatum != null)
                    mySpreker.Add(((DateTime)functie.Contact.Geboortedatum).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Aanspreking"))
                mySpreker.Add(functie.Contact.Aanspreking);
            if (headers.Contains("Type"))
                mySpreker.Add(functie.Type);
            if (headers.Contains("Contact email"))
            {
                string email = functie.Email == null ? "" : functie.Email;
                email = email.Replace('\n', ' ');
                email = email.Replace(';', '-');
                mySpreker.Add(email);
            }
            if (headers.Contains("Contact telefoon"))
                mySpreker.Add(functie.Telefoon);
            if (headers.Contains("Taal"))
                if (functie.Contact.Taal != null)
                    mySpreker.Add(functie.Contact.Taal.Naam);
                else
                    mySpreker.Add("");
            if (headers.Contains("Niveau1"))
                mySpreker.Add(functie.Niveau1.Naam);
            if (headers.Contains("Niveau2"))
            {
                if (functie.Niveau2 != null)
                    mySpreker.Add(functie.Niveau2.Naam);
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Niveau3"))
            {
                if (functie.Niveau3 != null)
                    mySpreker.Add(functie.Niveau3.Naam);
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Bedrijf"))
            {
                string bedrijf = functie.Bedrijf.Naam == null ? "" : functie.Bedrijf.Naam;
                bedrijf = bedrijf.Replace('\n', ' ');
                bedrijf = bedrijf.Replace(';', '-');
                mySpreker.Add(bedrijf);
            }
            if (headers.Contains("Telefoon"))
            {
                string telefoon = functie.Bedrijf.Telefoon == null ? "" : functie.Bedrijf.Telefoon;
                telefoon = telefoon.Replace(';', '-');
                telefoon = telefoon.Replace('\n', ' ');
                mySpreker.Add(telefoon);
            }
            if (headers.Contains("Fax"))
            {
                string fax = functie.Bedrijf.Fax == null ? "" : functie.Bedrijf.Fax;
                fax = fax.Replace(';', '-');
                fax = fax.Replace('\n', ' ');
                mySpreker.Add(fax);
            }
            if (headers.Contains("Email"))
            {
                string email = functie.Bedrijf.Email == null ? "" : functie.Bedrijf.Email;
                email = email.Replace(';', '-');
                email = email.Replace('\n', ' ');
                mySpreker.Add(email);
            }
            if (headers.Contains("Adres"))
            {
                string adres = functie.Bedrijf.Adres == null ? "" : functie.Bedrijf.Adres;
                adres = adres.Replace("\n", " ");
                adres = adres.Replace(";", "-");
                mySpreker.Add(adres);
            }
            if (headers.Contains("Postcode"))
                mySpreker.Add(functie.Bedrijf.Postcode);
            if (headers.Contains("Gemeente"))
                mySpreker.Add(functie.Bedrijf.Plaats);
            if (headers.Contains("Notities"))
            {
                string notitie = functie.Contact.Notities == null ? "" : functie.Contact.Notities;
                notitie = notitie.Replace('\n', ' ');
                notitie = notitie.Replace(';', '-');
                mySpreker.Add(notitie);
            }
            if (headers.Contains("Nieuwsbrief via mail"))
                mySpreker.Add(functie.Mail == true ? "Ja" : "Nee");
            if (headers.Contains("Nieuwsbrief via fax"))
                mySpreker.Add(functie.Fax == true ? "Ja" : "Nee");
            if (headers.Contains("Gewijzigd"))
            {
                if (functie.Contact.Gewijzigd != null)
                    mySpreker.Add(((DateTime)functie.Contact.Gewijzigd).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Gewijzigd door"))
                mySpreker.Add(functie.Contact.Gewijzigd_door);
            if (headers.Contains("Aangemaakt"))
            {
                if (functie.Contact.Aangemaakt != null)
                    mySpreker.Add(((DateTime)functie.Contact.Aangemaakt).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Aangemaakt door"))
                mySpreker.Add(functie.Contact.Aangemaakt_door);

            return mySpreker.ToArray();
        }

        private string[] GetActiveHeaders()
        {
            List<String> myHeaders = new List<string>();
            GebruikerService service = new GebruikerService();
            String Filename = service.GetConfigFileName(currentUser.Naam);

            if (Filename != "")
            {
                ContactConfig config = Helper.GetContactConfigFromXml(Filename);
                //if (config.ID)
                    myHeaders.Add("Contact ID");
                //if (config.Voornaam)
                    myHeaders.Add("Voornaam");
                //if (config.Achternaam)
                    myHeaders.Add("Achternaam");
                //if (config.Geboortedatum)
                    myHeaders.Add("Geboortedatum");
                //if (config.Aanspreking)
                    myHeaders.Add("Geslacht");
                //if (config.Type)
                    myHeaders.Add("Type");
                myHeaders.Add("Contact notities");
                //if (config.ContactEmail)
                myHeaders.Add("Functie email");
                //if (config.ContactTelefoon)
                    myHeaders.Add("Functie telefoon");
                myHeaders.Add("Functie mobiel");
                myHeaders.Add("Contact actief");
                //if (config.Taal)
                myHeaders.Add("Taal");
                myHeaders.Add("Functie ID");
                //if (config.Niveau1)
                myHeaders.Add("Niveau1");
                //if (config.Niveau2)
                    myHeaders.Add("Niveau2");
                //if (config.Niveau3)
                    myHeaders.Add("Niveau3");
                myHeaders.Add("Oorspronkelijke titel");
                myHeaders.Add("Attesttype");
                myHeaders.Add("Erkenningsnummer");
                //if (config.Bedrijf)
                myHeaders.Add("Bedrijf");
                myHeaders.Add("Btwnummer");
                //if (config.Telefoon)
                myHeaders.Add("Bedrijf telefoon");
                //if (config.Fax)
                    myHeaders.Add("Bedrijf fax");
                //if (config.Email)
                    myHeaders.Add("Bedrijf email");
                myHeaders.Add("Bedrijf website");
                //if (config.Adres)
                myHeaders.Add("Adres");
                //if (config.Postcode)
                    myHeaders.Add("Postcode");
                //if (config.Gemeente)
                    myHeaders.Add("Gemeente");
                myHeaders.Add("Land");
                //if (config.ContactNotities)
                myHeaders.Add("Bedrijf notities");
                myHeaders.Add("Bedrijf actief");
                //if (config.NieuwsbriefMail)
                myHeaders.Add("Nieuwsbrief via mail");
                //if (config.NieuwsbriefFax)
                    myHeaders.Add("Nieuwsbrief via fax");
                myHeaders.Add("Functie actief");
                //myHeaders.Add("Inschrijving communicatie");
                //if (config.Gewijzigd)
                myHeaders.Add("Gewijzigd");
                //if (config.GewijzigdDoor)
                    myHeaders.Add("Gewijzigd door");
                //if (config.Aangemaakt)
                    myHeaders.Add("Aangemaakt");
                //if (config.AangemaaktDoor)
                    myHeaders.Add("Aangemaakt door");
            }
            return myHeaders.ToArray();
        }

        private void excelButton_Click(object sender, RoutedEventArgs e)
        {
            WeergaveInputWindow IW = new WeergaveInputWindow();

            if (IW.ShowDialog() == true)
            {
                string filename = IW.Naam;
                string url = _dataFolder + filename + ".csv";
                Task.Factory.StartNew(() => { ToExcel(url); });
            }
        }

        private void SaveWeergaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (WeergavenComboBox.SelectedIndex > 0)
            {
                Weergave geselecteerde = (Weergave)WeergavenComboBox.SelectedItem;
                Weergave myWeergave = GetWeergaveFromForm();
                if (myWeergave != null)
                {
                    myWeergave.ID = geselecteerde.ID;
                    myWeergave.Naam = geselecteerde.Naam;
                    myWeergave.User_ID = geselecteerde.User_ID;
                    WeergaveServices service = new WeergaveServices();
                    Weergave Saved = service.SaveWeergaveChanges(myWeergave);
                    if (Saved != null)
                    {
                        ConfocusMessageBox.Show("Weergave opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                        LoadWeergaveComboBox();
                    }
                    else
                        ConfocusMessageBox.Show("Fout bij het opslaan van de weergave!", ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                Weergave myWeergave = GetWeergaveFromForm();
                if (myWeergave != null)
                {
                    WeergaveInputWindow WW = new WeergaveInputWindow();
                    if (WW.ShowDialog() == true)
                    {
                        myWeergave.Naam = WW.Naam;
                        if (myWeergave != null)
                        {
                            WeergaveServices service = new WeergaveServices();
                            Weergave Saved = service.SaveWeergave(myWeergave);
                            if (Saved != null)
                            {
                                ConfocusMessageBox.Show("Weergave opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                LoadWeergaveComboBox();
                            }
                            else
                                ConfocusMessageBox.Show("Fout bij het opslaan van de weergave!", ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }
            }
        }

        private Weergave GetWeergaveFromForm()
        {
            try
            {
                Boolean IsLeeg = true;
                Weergave weergave = new Weergave();
                weergave.User_ID = UserID;
                if (bedrijvenComboBox.Text != "")
                    IsLeeg = false;
                if (emailComboBox.Text != "")
                    IsLeeg = false;
                if (typeComoBox.Text != "")
                    IsLeeg = false;
                if (niveau1ComboBox.Text != "")
                    IsLeeg = false;
                if (niveau2ComboBox.Text != "")
                    IsLeeg = false;
                if (niveau3ComboBox.Text != "")
                    IsLeeg = false;
                if (LocatieComboBox.Text != "")
                    IsLeeg = false;
                if (ProvincieComboBox.Text != "")
                    IsLeeg = false;
                if (zoekVoorNaamTextBox.Text != "")
                    IsLeeg = false;
                if (aangemaaktDatepicker.SelectedDate != null)
                    IsLeeg = false;

                weergave.Bedrijf = bedrijvenComboBox.Text;
                weergave.Email = emailComboBox.Text;
                weergave.Type = typeComoBox.Text;
                weergave.Niveau1 = niveau1ComboBox.Text;
                weergave.Niveau2 = niveau2ComboBox.Text;
                weergave.Niveau3 = niveau3ComboBox.Text;
                weergave.Gemeente = LocatieComboBox.Text;
                weergave.Provincie = ProvincieComboBox.Text;
                weergave.Zoeknaam = zoekVoorNaamTextBox.Text;
                weergave.Achternaam = zoekAchterNaamTextBox.Text;
                if (aangemaaktDatepicker.SelectedDate != null)
                    weergave.Datum = aangemaaktDatepicker.SelectedDate;

                if (IsLeeg)
                    return null;
                else
                    return weergave;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void WeergavenComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Weergave gekozenWeergave = (Weergave)box.SelectedItem;
                SetFilters(gekozenWeergave);
            }
            else
            {
                MaakFiltersLeeg();
            }
        }

        private void SetFilters(Weergave weergave)
        {
            try
            {
                MaakFiltersLeeg();
                if (weergave.Bedrijf != "")
                    bedrijvenComboBox.Text = weergave.Bedrijf;
                if (weergave.Email != "")
                    emailComboBox.Text = weergave.Email;
                if (weergave.Type != null)
                    typeComoBox.Text = weergave.Type;
                if (weergave.Niveau1 != "")
                    niveau1ComboBox.Text = weergave.Niveau1;
                if (weergave.Niveau2 != "")
                    niveau2ComboBox.Text = weergave.Niveau2;
                if (weergave.Niveau3 != "")
                    niveau3ComboBox.Text = weergave.Niveau3;
                if (weergave.Gemeente != "")
                    LocatieComboBox.Text = weergave.Gemeente;
                if (weergave.Provincie != "")
                    ProvincieComboBox.Text = weergave.Provincie;
                if (weergave.Zoeknaam != "")
                    zoekVoorNaamTextBox.Text = weergave.Zoeknaam;
                if (!string.IsNullOrEmpty(weergave.Achternaam))
                    zoekAchterNaamTextBox.Text = weergave.Achternaam;
                if (weergave.Datum != null)
                    aangemaaktDatepicker.SelectedDate = weergave.Datum;

                ActivateFilters(null);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van de weergave." + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }


        /******************** Detail wijzigingen Contact ***************************/

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Contact contact = ((Functies)contactDataGrid.SelectedItem).Contact;
                    switch (box.Name)
                    {
                        case "voornaamTextBox":
                            contact.Voornaam = box.Text;
                            break;
                        case "achternaamTextBox":
                            contact.Achternaam = box.Text;
                            break;
                        case "notitiesTextBox1":
                            contact.Notities = box.Text;
                            break;
                        default:
                            break;
                    }

                    SaveContactChanges(contact);
                }
            }
        }

        private void SaveContactChanges(Contact contact)
        {
            if (contact != null)
            {
                contact.Gewijzigd = DateTime.Now;
                contact.Gewijzigd_door = currentUser.Naam;

                try
                {
                    ContactServices sevice = new ContactServices();
                    sevice.SaveContactChanges(contact);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void TextBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (IsUserClick)
            {
                if (contactDataGrid.SelectedIndex > -1)
                {
                    ComboBox box = (ComboBox)sender;
                    Contact contact = ((Functies)contactDataGrid.SelectedItem).Contact;
                    switch (box.Name)
                    {
                        case "aansprekingTextBox":
                            if (box.SelectedIndex > -1)
                                contact.Aanspreking = box.SelectedIndex == 0 ? "M" : "V";
                            break;
                        case "typeComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Type = box.SelectedIndex == 0 ? "Klant" : "Prospect";
                            break;
                        case "taalComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Taal_ID = ((Taal)box.SelectedItem).ID;
                            break;
                        case "SecundaireTaalComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Secundaire_Taal_ID = ((Taal)box.SelectedItem).ID;
                            break;
                        case "contactErkenningComboBox":
                            if (box.SelectedIndex > -1)
                                contact.Erkenning_ID = ((Erkenning)box.SelectedItem).ID;
                            break;
                        default:
                            break;
                    }

                    SaveContactChanges(contact);
                }
                IsUserClick = false;
            }
        }

        private void ComboBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsUserClick = true;
        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {

            if ((sender as ComboBox).SelectedIndex > -1 || (sender as ComboBox).Name == "niveau3DetailComboBox")
                IsUserClick = true;
        }

        private void geboortedatumTextBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsUserClick)
            {
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Contact contact = ((Functies)contactDataGrid.SelectedItem).Contact;
                    DatePicker datum = (DatePicker)sender;
                    contact.Geboortedatum = datum.SelectedDate;
                    SaveContactChanges(contact);
                }
                IsUserClick = false;
            }
        }

        private void geboortedatumTextBox_CalendarOpened(object sender, RoutedEventArgs e)
        {
            IsUserClick = true;
        }

        /******************** Functie detail wijzigingen *****************************/

        //private void niveau1DetailComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    //niveau2DetailComboBox.ItemsSource = new List<Niveau2>();
        //    //niveau3DetailComboBox.ItemsSource = new List<Niveau3>();
        //    ComboBox box = (ComboBox)sender;
        //    Niveau1 n1 = (Niveau1)box.SelectedItem;
        //    if (n1 != null)
        //    {
        //        Niveau2Services n2Service = new Niveau2Services();
        //        List<Niveau2> n2Lijst = n2Service.GetSelected(n1.ID);
        //        niveau2DetailComboBox.ItemsSource = n2Lijst;
        //        niveau2DetailComboBox.DisplayMemberPath = "Naam";

        //        if (IsUserClick)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Functies functie = (Functies)contactDataGrid.SelectedItem;
        //                functie.Niveau1_ID = n1.ID;
        //                functie.Niveau1 = n1;
        //                functie.Niveau2 = null;
        //                functie.Niveau2_ID = null;
        //                functie.Niveau3 = null;
        //                functie.Niveau3_ID = null;

        //                SaveFunctieDetailChanges(functie);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //}

        private void SaveFunctieDetailChanges(Functies functie)
        {
            functie.Gewijzigd = DateTime.Now;
            functie.Gewijzigd_door = currentUser.Naam;
            try
            {
                FunctieServices service = new FunctieServices();
                service.SaveFunctieWijzigingen(functie);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan!<n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void niveau2DetailComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    niveau3DetailComboBox.ItemsSource = new List<Niveau3>();
        //    ComboBox box = (ComboBox)sender;
        //    if (box.SelectedIndex > -1)
        //    {
        //        Niveau2 n2 = (Niveau2)box.SelectedItem;
        //        Niveau3Services n3Service = new Niveau3Services();
        //        List<Niveau3> n3Lijst = n3Service.GetSelected(n2.ID);
        //        niveau3DetailComboBox.ItemsSource = n3Lijst;
        //        niveau3DetailComboBox.DisplayMemberPath = "Naam";

        //        if (IsUserClick)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Functies functie = (Functies)contactDataGrid.SelectedItem;
        //                functie.Niveau2 = n2;
        //                functie.Niveau2_ID = n2.ID;
        //                functie.Niveau3 = null;
        //                functie.Niveau3_ID = null;

        //                SaveFunctieDetailChanges(functie);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //}

        //private void niveau3DetailComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        ComboBox box = (ComboBox)sender;
        //        Niveau3 n3 = (Niveau3)box.SelectedItem;
        //        if (n3 != null)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Functies functie = (Functies)contactDataGrid.SelectedItem;
        //                functie.Niveau3 = n3;
        //                functie.Niveau3_ID = n3.ID;

        //                SaveFunctieDetailChanges(functie);
        //            }
        //        }
        //        IsUserClick = false;
        //    }
        //}

        private void functieTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    switch (box.Name)
                    {
                        case "emailTextBox":
                            functie.Email = box.Text;
                            break;
                        case "telefoonTextBox":
                            functie.Telefoon = box.Text;
                            break;
                        case "mobielTextBox":
                            functie.Mobiel = box.Text;
                            break;
                        case "OorspronkelijkeTextBox":
                            functie.Oorspronkelijke_Titel = box.Text;
                            break;
                        default:
                            break;
                    }

                    SaveFunctieDetailChanges(functie);
                }
            }
        }

        private void functieDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsUserClick)
            {
                DatePicker picker = (DatePicker)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Functies functie = (Functies)contactDataGrid.SelectedItem;
                    switch (picker.Name)
                    {
                        case "startdatumTextBox":
                            functie.Startdatum = picker.SelectedDate;
                            break;
                        case "einddatumTextBox":
                            functie.Einddatum = picker.SelectedDate;
                            break;
                        default:
                            break;
                    }

                    SaveFunctieDetailChanges(functie);
                }
                IsUserClick = false;
            }
        }

        private void functieDatePicker_CalendarOpened(object sender, RoutedEventArgs e)
        {
            IsUserClick = true;
        }

        //private void mailCheckBox_Click(object sender, RoutedEventArgs e)
        //{
        //    Functies functie = (Functies)contactDataGrid.SelectedItem;
        //    functie.Mail = mailCheckBox.IsChecked == true;
        //    SaveFunctieDetailChanges(functie);
        //}

        //private void faxCheckBox_Click(object sender, RoutedEventArgs e)
        //{
        //    Functies functie = (Functies)contactDataGrid.SelectedItem;
        //    functie.Fax = faxCheckBox.IsChecked == true;
        //    SaveFunctieDetailChanges(functie);
        //}

        /************************** Bedrijf details wijzigingen ********************************/

        private void bedrijfTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                if (contactDataGrid.SelectedIndex > -1)
                {
                    Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
                    switch (box.Name)
                    {
                        case "naamTextBox3":
                            bedrijf.Naam = box.Text;
                            break;
                        case "ondernemersnummerTextBox":
                            bedrijf.Ondernemersnummer = box.Text;
                            break;
                        case "adresTextBox":
                            bedrijf.Adres = box.Text;
                            break;
                        case "telefoonTextBox1":
                            bedrijf.Telefoon = box.Text;
                            break;
                        case "faxTextBox":
                            bedrijf.Fax = box.Text;
                            break;
                        case "emailTextBox1":
                            bedrijf.Email = box.Text;
                            break;
                        case "websiteTextBox":
                            bedrijf.Website = box.Text;
                            break;
                        case "notitiesTextBox":
                            bedrijf.Notities = box.Text;
                            break;
                        default:
                            break;
                    }

                    SaveBedrijfDetailChanges(bedrijf);
                }
            }
        }

        private void SaveBedrijfDetailChanges(Bedrijf bedrijf)
        {
            bedrijf.Gewijzigd = DateTime.Now;
            bedrijf.Gewijzigd_door = currentUser.Naam;
            try
            {
                BedrijfServices service = new BedrijfServices();
                service.SaveBedrijfChanges(bedrijf);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void bedrijfPostcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    PostcodeService service = new PostcodeService();
        //    Postcodes postcode = (Postcodes)bedrijfPostcodeComboBox.SelectedItem;
        //    if (postcode != null)
        //    {
        //        List<Postcodes> gemeentes = service.GetGemeentesByPostcode(postcode.Postcode);
        //        bedrijfPlaatsComboBox.ItemsSource = gemeentes;
        //        bedrijfPlaatsComboBox.DisplayMemberPath = "Gemeente";
        //        bedrijfPlaatsComboBox.SelectedIndex = 0;
        //        landTextBox.Text = postcode.Land;

        //        //if (bedrijfProvincieComboBox.Items.Contains(postcode.Provincie))
        //            bedrijfProvincieComboBox.Text = postcode.Provincie;
        //        //else
        //            //bedrijfProvincieComboBox.SelectedIndex = -1;

        //        if (IsUserClick)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
        //                bedrijf.Postcode = postcode.Postcode;
        //                bedrijf.Plaats = postcode.Gemeente;
        //                bedrijf.Provincie = postcode.Provincie;
        //                bedrijf.Land = postcode.Land;

        //                SaveBedrijfDetailChanges(bedrijf);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //    else
        //    {
        //        bedrijfPostcodeComboBox.SelectedIndex = -1;
        //        bedrijfPlaatsComboBox.SelectedIndex = -1;
        //        bedrijfProvincieComboBox.SelectedIndex = -1;
        //    }
        //}

        private void BedrijfComboBox_DropDownOpened(object sender, EventArgs e)
        {
            IsUserClick = true;
        }

        //private void bedrijfPlaatsComboBox_SizeChanged(object sender, SizeChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        if (contactDataGrid.SelectedIndex > -1)
        //        {
        //            Postcodes postcode = (Postcodes)bedrijfPlaatsComboBox.SelectedItem;

        //            Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
        //            bedrijf.Plaats = postcode.Gemeente;

        //            SaveBedrijfDetailChanges(bedrijf);
        //        }
        //        IsUserClick = false;
        //    }
        //}

        //private void bedrijfProvincieComboBox_SizeChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        Postcodes postcode = (Postcodes)bedrijfPostcodeComboBox.SelectedItem;
        //        if (bedrijfProvincieComboBox.SelectedIndex > -1)
        //        {
        //            if (contactDataGrid.SelectedIndex > -1)
        //            {
        //                String provincie = ((ComboBoxItem)bedrijfProvincieComboBox.SelectedItem).Content.ToString();
        //                Bedrijf bedrijf = ((Functies)contactDataGrid.SelectedItem).Bedrijf;
        //                bedrijf.Provincie = provincie;

        //                if (postcode.Provincie == null)
        //                {
        //                    postcode.Provincie = provincie;
        //                    PostcodeService postService = new PostcodeService();
        //                    postService.ChangePostcode(postcode);
        //                }


        //                SaveBedrijfDetailChanges(bedrijf);
        //            }
        //            IsUserClick = false;
        //        }
        //    }
        //}

        private void contactDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                if (contactDataGrid.SelectedIndex < (contactDataGrid.Items.Count - 1))
                {
                    NextButton_Click(contactDataGrid, new RoutedEventArgs());
                }
            }
            else if (e.Key == Key.Up)
            {
                if (contactDataGrid.SelectedIndex > 0)
                {
                    PrevButton_Click(contactDataGrid, new RoutedEventArgs());
                }
            }
        }

        private void OpenSiteButton_Click(object sender, RoutedEventArgs e)
        {
            //if (websiteTextBox.Text != "")
            //{
            //    String url = websiteTextBox.Text;
            //    try
            //    {
            //        System.Diagnostics.Process.Start(url);
            //    }
            //    catch (Exception ex)
            //    {
            //        ConfocusMessageBox.Show("Fout bij het openen van de website!\nControleer of de URL correct is en probeer opnieuw.\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            //    }
            //}
        }

        private void OpenMailButton_Click(object sender, RoutedEventArgs e)
        {
            //if (emailTextBox1.Text != "")
            //{
            //    String url = "mailto:'" + emailTextBox1.Text + "'";
            //    try
            //    {
            //        System.Diagnostics.Process.Start(url);
            //    }
            //    catch (Exception ex)
            //    {
            //        ConfocusMessageBox.Show("Fout bij het openen van het email programma!\nControleer of het adres correct is en probeer opnieuw.\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            //    }
            //}
        }

        private void ImportContactMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel IE = new ImportExcel(currentUser);
            IE.Show();
            IE.Activate();
        }

        private void KalenderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Kalender kalender = new Kalender(currentUser);
            kalender.Show();
            kalender.Activate();
        }

        private void MarketingkanaalMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Markeningkanalen MK = new Markeningkanalen(currentUser);
            MK.Show();
            MK.Activate();
        }

        private void ImportMarketingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportMarketing IM = new ImportMarketing(currentUser);
            IM.Show();
            IM.Activate();
        }

        private void AttesttypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AttesttypeBeheer AB = new AttesttypeBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }

        private void contactDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.Target is System.Windows.Controls.TextBlock
                || e.MouseDevice.Target is System.Windows.Controls.TextBox
                || e.MouseDevice.Target is System.Windows.Controls.Border)
            {
                FilterFunctie fFunctie = (FilterFunctie)contactDataGrid.SelectedItem;
                Functies functie = new FunctieServices().GetFunctieByID(fFunctie.ID);
                NieuwContact BC = new NieuwContact(currentUser, functie);
                if (BC.ShowDialog() == true)
                {
                    try
                    {
                        ReLoadContacten(fFunctie);
                        contactDataGrid.SelectedItem = fFunctie;
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message + "\nCloseNieuwContact module", ConfocusMessageBox.Kleuren.Rood);
                    }
                }

            }
        }

        private void contactDetailHideButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Toon details")
            {
                button.Content = "Verberg details";
                contactDetailRow.Height = new GridLength(300);
            }
            else
            {
                button.Content = "Toon details";
                contactDetailRow.Height = new GridLength(30);
            }
        }

        private void meerButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Meer")
            {
                button.Content = "Minder";
                searchPanel.Height = new GridLength(120);
            }
            else
            {
                button.Content = "Meer";
                searchPanel.Height = new GridLength(65);
            }
        }

        private void aangemaaktDatepicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ActivateFilters(null);
        }

        private void DagTypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DagtypeBeheer DB = new DagtypeBeheer(currentUser);
            DB.Show();
            DB.Activate();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            ShowSearchWindow();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            SpeedtestWindow SW = new SpeedtestWindow();
            SW.Show();
            //FunctieServices fService = new FunctieServices();
            //fService.SetProspect();
        }

        private void FlexmailWeergaveButton_Click(object sender, RoutedEventArgs e)
        {
            List<FilterFunctie> functies = new List<FilterFunctie>();
            foreach (FilterFunctie func in contactDataGrid.Items)
            {
                if (!string.IsNullOrEmpty(func.Email))// && !func.ContainsContact(functies))
                    functies.Add(func);
            }
            FlexmailActieWindows FW = new FlexmailActieWindows(functies);
            FW.Show();
            FW.Activate();
        }

        private void ZoekBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZB = new ZoekContactWindow("Bedrijf");
            if (ZB.ShowDialog() == true)
            {
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                Bedrijf bedrijf = new Bedrijf();
                bedrijf.Naam = "Kies";
                bedrijven.Add(bedrijf);
                bedrijven.Add(ZB.bedrijf);
                bedrijvenComboBox.ItemsSource = bedrijven;
                bedrijvenComboBox.DisplayMemberPath = "Naam";
                bedrijvenComboBox.SelectedIndex = 1;
            }
        }

        private void faxnummerTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
                ActivateFilters(null);
        }

        private void SpecialeTekensMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SpecialeKarakters SK = new SpecialeKarakters();
                SK.Show();
                SK.Activate();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ZoekEmailButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekEmailInvoerWindow EW = new ZoekEmailInvoerWindow();
            if (EW.ShowDialog() == true)
            {
                List<string> emailoptions = new List<string>();
                emailoptions.Add("Kies");
                emailoptions.Add("Leeg");
                emailoptions.Add(EW.Emailadres);

                emailComboBox.ItemsSource = emailoptions;
                emailComboBox.SelectedIndex = 2;
            }
        }

        private void WeergaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ContactWeergaveBeheer CW = new ContactWeergaveBeheer(currentUser);
            if (CW.ShowDialog() == true)
            {
                LoadWeergaveComboBox();
            }
        }

        private void XMLCreatorMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //C:\Confocus XML Creator/ConfocusXMLCreator.exe
            Process.Start(@"C:\Confocus XML Creator/ConfocusXMLCreator.exe");
        }

        private void FixMenuItem_Click(object sender, RoutedEventArgs e)
        {
            BedrijfServices bService = new BedrijfServices();
            FunctieServices fservice = new FunctieServices();
            List<Bedrijf> bedrijven = bService.FindActive();
            int counter = 0;
            var faxnummergroup = (from b in bedrijven where !string.IsNullOrEmpty(b.Fax) group b by b.Fax into g orderby g.Key select new { Fax = g.Key, bedrijf = g.ToList() }).ToList();
            foreach (var bedrijvengroep in faxnummergroup)
            {

                foreach (var bedrijf in bedrijvengroep.bedrijf)
                {
                    List<Functies> functions = fservice.GetFunctiesInBedrijf(bedrijf.ID);
                    bool fax = true;
                    foreach (Functies func in functions)
                    {
                        if (func.Fax == false)
                        {
                            fax = false;
                            break;
                        }
                    }
                    if (fax == false)
                    {
                        foreach (Functies func in functions)
                        {
                            if (func.Fax == true)
                            {
                                func.Fax = false;
                                fservice.SaveFunctieWijzigingen(func);
                                counter++;
                            }

                        }
                    }
                }
            }
            MessageBox.Show(counter + " contacten aangepast");
        }

        private void RegioMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RegioBeheer RW = new RegioBeheer(currentUser);
            if (RW.ShowDialog() == true)
            {

            }
        }

        private void SeminarieTypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SeminarieTypeBeheer TW = new SeminarieTypeBeheer(currentUser);
            if (TW.ShowDialog() == true)
            {

            }
        }

        private void UpdateTimestampsItem_Click(object sender, RoutedEventArgs e)
        {
            UpdateTimestamps();
        }

        private async void UpdateTimestamps()
        {
            try
            {
                // Update sessies
                await Task.Run(() => UpdateSessieTimestamps());
                // Update locaties
                await Task.Run(() => UpdateLocatieTimestamps());
                // Create new XML
                Process.Start(@"C:\Confocus XML Creator/ConfocusXMLCreator.exe");
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het updaten van de timestamps!!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void UpdateLocatieTimestamps()
        {
            try
            {
                LocatieServices lService = new LocatieServices();
                List<Locatie> locaties = lService.FindActive();
                foreach (Locatie locatie in locaties)
                {
                    locatie.Gewijzigd = DateTime.Now;
                    lService.SaveLocatieChanges(locatie);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateSessieTimestamps()
        {
            try
            {
                SessieServices sesService = new SessieServices();
                List<Sessie> sessies = sesService.GetActueleSessies();
                foreach (Sessie sessie in sessies)
                {
                    sessie.Gewijzigd = DateTime.Now;
                    sesService.SaveSessieTimetampChanges(sessie);//SaveSessieTimestampChanges(sessie);
                    // Update seminarie
                    SeminarieServices semService = new SeminarieServices();
                    Seminarie seminarie = semService.GetSeminarieByID((decimal)sessie.Seminarie_ID);
                    if (seminarie.Gewijzigd < DateTime.Now.AddMinutes(-30))
                    {
                        seminarie.Gewijzigd = DateTime.Now;
                        semService.SaveSeminarieChanges(seminarie);
                        // Update doelgroepen
                        Seminarie_DoelgroepServices dService = new Seminarie_DoelgroepServices();
                        List<Seminarie_Doelgroep> doelgroepen = dService.GetDoelgroepenBySeminarieID(seminarie.ID);
                        foreach (Seminarie_Doelgroep doelgroep in doelgroepen)
                        {
                            doelgroep.Gewijzigd = DateTime.Now;
                            dService.SaveSeminarieDoelgroepChanges(doelgroep);
                        }
                        // Update erkenningen
                        Seminarie_ErkenningenService eService = new Seminarie_ErkenningenService();
                        List<Seminarie_Erkenningen> erkenningen = eService.GetBySeminarieID(seminarie.ID);
                        foreach (Seminarie_Erkenningen erkenning in erkenningen)
                        {
                            erkenning.Gewijzigd = DateTime.Now;
                            eService.UpdateErkenning(erkenning);
                        }
                    }
                    // Update sprekers
                    SessieSprekerServices ssService = new SessieSprekerServices();
                    List<Sessie_Spreker> sprekers = ssService.GetSprekersBySessieID(sessie.ID);
                    foreach (Sessie_Spreker spreker in sprekers)
                    {
                        spreker.Gewijzigd = DateTime.Now;
                        ssService.Update(spreker);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateSeminarieTimestamps()
        {
            try
            {
                SeminarieServices semService = new SeminarieServices();
                List<Seminarie> seminaries = semService.GetAllActiveForInschrijving();
                foreach (Seminarie seminarie in seminaries)
                {
                    semService.SaveSeminarieChanges(seminarie);
                    // Update doelgroepen
                    Seminarie_DoelgroepServices dService = new Seminarie_DoelgroepServices();
                    List<Seminarie_Doelgroep> doelgroepen = dService.GetDoelgroepenBySeminarieID(seminarie.ID);
                    foreach (Seminarie_Doelgroep doelgroep in doelgroepen)
                    {
                        dService.SaveSeminarieDoelgroepChanges(doelgroep);
                    }
                    // Update erkenningen
                    Seminarie_ErkenningenService eService = new Seminarie_ErkenningenService();
                    List<Seminarie_Erkenningen> erkenningen = eService.GetBySeminarieID(seminarie.ID);
                    foreach (Seminarie_Erkenningen erkenning in erkenningen)
                    {
                        eService.UpdateErkenning(erkenning);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ShowSearchWindow()
        {
            ReplaceEmailWindow RW = new ReplaceEmailWindow(currentUser);
            if (RW.ShowDialog() == true)
            {
                if (contactDataGrid.Items.Count > 0)
                {
                    FilterFunctie functie = (FilterFunctie)contactDataGrid.SelectedItem;
                    ActivateFilters(functie);
                }

            }
            //ZoekVervangWindow ZW = new ZoekVervangWindow();
            //if (ZW.ShowDialog() == true)
            //{
            //    ReplaceEmail mail = ZW.replaceEmail;
            //    FunctieServices fService = new FunctieServices();
            //    int aantal = fService.ReplaceEmail(mail);
            //    ConfocusMessageBox.Show(aantal + " emailadressen vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            //    ReLoadContacten(null);
            //    //ActivateFilters();
            //}
        }

        private void AttestTemplateMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AttestTemplateBeheer AB = new AttestTemplateBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }
    }
}

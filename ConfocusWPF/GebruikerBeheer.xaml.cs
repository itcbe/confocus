﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for GebruikerBeheer.xaml
    /// </summary>
    public partial class GebruikerBeheer : Window
    {
        private CollectionViewSource gebruikerViewSource;
        private List<Gebruiker> gebruikers = new List<Gebruiker>();
        private List<Gebruiker> gevondenGebruikers = new List<Gebruiker>();
        private List<Gebruiker> gewijzigdeGebruikers = new List<Gebruiker>();
        private Boolean IsScroll = false, IsButtonClick = false;
        private Gebruiker currentUser;

        public GebruikerBeheer(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
            this.Title = "Gebruiker beheer : Ingelogd als " + currentUser.Naam;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (currentUser.Rol != 1)
            //{
            //    roleLabel.Visibility = Visibility.Hidden;
            //    rolComboBox.Visibility = Visibility.Hidden;
            //}
            //else
            //{
            //    roleLabel.Visibility = Visibility.Visible;
            //    rolComboBox.Visibility = Visibility.Visible;
            //}

            gebruikerViewSource = ((CollectionViewSource)(this.FindResource("gebruikerViewSource")));
            LoadData();
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweGebruiker NG = new NieuweGebruiker(currentUser);
            if (NG.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            List<UserRole> roles = new UserRole_Services().FindActive();
            rolComboBox.ItemsSource = roles;
            rolComboBox.DisplayMemberPath = "Rol";

            GebruikerService service = new GebruikerService();
            gebruikers = service.FindAll();
            gebruikerViewSource.Source = gebruikers;
            goUpdate();
        }

        private void goUpdate()
        {
            if (gebruikerDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (gebruikerViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (gebruikerViewSource.View.CurrentPosition == gebruikerDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                gebruikerDataGrid.SelectedIndex = 0;
            if (gebruikerDataGrid.Items.Count != 0)
                gebruikerDataGrid.ScrollIntoView(gebruikerDataGrid.SelectedItem);
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();           
        }

        private void SaveWijzigingen()
        {
            if (gewijzigdeGebruikers.Count > 0)
            {
                GebruikerService service = new GebruikerService();
                foreach (Gebruiker G in gewijzigdeGebruikers)
                {
                    G.Gewijzigd = DateTime.Now;
                    G.Gewijzigd_door = currentUser.Naam;
                    SaveAndReload(G);              
                }
                gewijzigdeGebruikers.Clear();
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            else
                ConfocusMessageBox.Show("Geen wijzigingen gevonden.", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + "_hover.gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));

        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + ".gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Control_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Helper.CheckEmail(emailTextBox.Text) || String.IsNullOrEmpty(emailTextBox.Text))
            {
                SaveGebruiker();             
            }
            else
                ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }

        private void SaveGebruiker()
        {
            try
            {
                Gebruiker gebruiker = (Gebruiker)gebruikerDataGrid.SelectedItem;
                gebruiker.Naam = naamTextBox.Text;
                gebruiker.Email = emailTextBox.Text;
                gebruiker.Actief = actiefCheckBox.IsChecked == true;
                if (rolComboBox.SelectedIndex > -1)
                    gebruiker.Rol = ((UserRole)rolComboBox.SelectedItem).ID;
                gebruiker.Gewijzigd = DateTime.Now;
                gebruiker.Gewijzigd_door = currentUser.Naam;

                SaveAndReload(gebruiker);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van de gebruiker!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SaveAndReload(Gebruiker gebruiker)
        {
            GebruikerService service = new GebruikerService();
            try
            {
                service.SaveGebruikerWijzigingen(gebruiker);
                int index = gebruikerDataGrid.SelectedIndex;
                LoadData();
                if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                    ZoekInLijst();
                gebruikerDataGrid.SelectedIndex = index;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van de gebruiker!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void wijzigPaswoord_Click(object sender, RoutedEventArgs e)
        {
            if (gebruikerDataGrid.SelectedIndex > -1)
            { 
                WijzigPaswoord WP = new WijzigPaswoord(currentUser, (Gebruiker)gebruikerDataGrid.SelectedItem);
                if (WP.ShowDialog() == true)
                    LoadData();
            }
        }

        private void gebruikerDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Gebruiker user = (Gebruiker)gebruikerDataGrid.SelectedItem;
            if (user.UserRole != null)
            { 
                rolComboBox.Text = user.UserRole.Rol;
                roleLabel.Content = user.UserRole.Rol;
            }
            if (currentUser.Rol == 1)
            {
                naamTextBox.Visibility = System.Windows.Visibility.Visible;
                emailTextBox.Visibility = System.Windows.Visibility.Visible;
                naamLabel.Visibility = System.Windows.Visibility.Hidden;
                emailLabel.Visibility = System.Windows.Visibility.Hidden;
                roleLabel.Visibility = Visibility.Hidden;
                wijzigPaswoord.Visibility = System.Windows.Visibility.Visible;
                rolComboBox.Visibility = System.Windows.Visibility.Visible;
                save.IsEnabled = true;
            }
            else if(currentUser.Naam == user.Naam)
            {
                naamTextBox.Visibility = System.Windows.Visibility.Visible;
                emailTextBox.Visibility = System.Windows.Visibility.Visible;
                naamLabel.Visibility = System.Windows.Visibility.Hidden;
                emailLabel.Visibility = System.Windows.Visibility.Hidden;
                roleLabel.Visibility = Visibility.Visible;
                wijzigPaswoord.Visibility = System.Windows.Visibility.Visible;
                rolComboBox.Visibility = System.Windows.Visibility.Hidden;
                save.IsEnabled = true;
            }
            else
            {
                naamTextBox.Visibility = System.Windows.Visibility.Hidden;
                emailTextBox.Visibility = System.Windows.Visibility.Hidden;
                naamLabel.Visibility = System.Windows.Visibility.Visible;
                emailLabel.Visibility = System.Windows.Visibility.Visible;
                roleLabel.Visibility = Visibility.Visible;
                wijzigPaswoord.Visibility = System.Windows.Visibility.Hidden;
                rolComboBox.Visibility = System.Windows.Visibility.Hidden;
                save.IsEnabled = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gewijzigdeGebruikers.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveWijzigingen();
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
                ZoekInLijst();
        }

        private void rolComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (gebruikerDataGrid.SelectedIndex > -1)
            {
                SaveGebruiker();
            }
        }

        private void ZoekInLijst()
        {
            gevondenGebruikers.Clear();

            gevondenGebruikers = (from G in gebruikers
                                  where G.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                  select G).ToList();
            if (gevondenGebruikers.Count > 0)
                gebruikerViewSource.Source = gevondenGebruikers;
            else
                gebruikerViewSource.Source = gebruikers;
        }
    }
}

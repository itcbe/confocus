﻿using ConfocusClassLibrary;
using ITCLibrary;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using data = ConfocusDBLibrary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweSeminarie.xaml
    /// </summary>
    /// 
    public class BoolToStringConverter : BoolToValueConverter<String> { }

    public partial class NieuweSeminarie : Window
    {
        private data.Gebruiker currentUser;
        private string rootTitle;
        private data.Seminarie currentSeminarie;
        private data.Sessie currentSessie;
        private data.Sessie deliveredSessie;
        private CollectionViewSource sessieViewSource;
        private CollectionViewSource inschrijvingenViewSource;
        private CollectionViewSource deelnemerViewSource;
        private CollectionViewSource seminarie_MarketingViewSource;
        private CollectionViewSource seminarie_DoelgroepViewSource;
        private CollectionViewSource sessie_SprekerViewSource;
        private CollectionViewSource seminarie_ErkenningenViewSource;
        private int _TabIndex = 0;
        private List<data.Spreker> activeSprekers = new List<data.Spreker>();
        private DateTime? sessiedatum;
        private bool IsChanged = false, IsInit = false;
        private bool UserClick = false;
        private string _dataFolder;

        public bool IsHistoriek = false;

        public NieuweSeminarie(data.Gebruiker user, data.Seminarie seminarie, DateTime? datum, data.Sessie sessie, bool ishistoriek)
        {
            IsInit = true;
            InitializeComponent();
            this.currentUser = user;
            this.IsHistoriek = ishistoriek;
            if (datum != null)
                this.sessiedatum = datum;
            else
                this.sessiedatum = null;
            if (sessie != null)
                deliveredSessie = sessie;

            if (!String.IsNullOrEmpty(seminarie.Titel))
            {
                data.SeminarieServices sService = new data.SeminarieServices();
                this.currentSeminarie = sService.GetSeminarieByID(seminarie.ID);
                rootTitle = "Bewerk seminarie";
                this.Title = rootTitle;
            }
            else
            {
                rootTitle = "Nieuwe seminarie";
                this.Title = rootTitle;
            }
            toevoegButton.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _dataFolder = new data.Instelling_Service().Find().DataFolder;
            sessieViewSource = ((CollectionViewSource)(FindResource("sessieViewSource")));
            inschrijvingenViewSource = ((CollectionViewSource)(FindResource("inschrijvingenViewSource")));
            deelnemerViewSource = ((CollectionViewSource)(FindResource("deelnemerViewSource")));
            seminarie_MarketingViewSource = ((CollectionViewSource)(FindResource("seminarie_MarketingViewSource")));
            seminarie_DoelgroepViewSource = ((CollectionViewSource)(FindResource("seminarie_DoelgroepViewSource")));
            sessie_SprekerViewSource = ((CollectionViewSource)(FindResource("sessie_SprekerViewSource")));
            seminarie_ErkenningenViewSource = ((CollectionViewSource)(FindResource("seminarie_ErkenningenViewSource")));

            try
            {
                LoadDataAsync();
                //LoadData();
                SeminarieTab.Focus();
                if (sessiedatum != null)
                    datumDatePicker.SelectedDate = sessiedatum;
                if (currentSeminarie != null)
                    InschrijvingenTab.Focus();
                else
                    SeminarieTab.Focus();

                IsChanged = false;
                IsInit = false;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void LoadData()
        //{
        //    //LoadDoelgroepData();
        //    LoadNiveau1();
        //    LoadTaal();

        //    data.GebruikerService gService = new data.GebruikerService();
        //    List<data.Gebruiker> verantwoordelijken = gService.FindActive();
        //    verantwoordelijkeComboBox.ItemsSource = verantwoordelijken;
        //    verantwoordelijkeComboBox.DisplayMemberPath = "Naam";
        //    terplaatseComboBox.ItemsSource = verantwoordelijken;
        //    terplaatseComboBox.DisplayMemberPath = "Naam";
        //    sessieVerantwoordelijkeComboBox.ItemsSource = verantwoordelijken;
        //    sessieVerantwoordelijkeComboBox.DisplayMemberPath = "Naam";

        //    LoadErkenningen();

        //    data.LocatieServices lService = new data.LocatieServices();
        //    List<data.Locatie> locaties = lService.FindActive();
        //    locatieComboBox1.ItemsSource = locaties;
        //    locatieComboBox1.DisplayMemberPath = "Naam";

        //    //data.BedrijfServices bService = new data.BedrijfServices();
        //    //List<data.Bedrijf> bedrijven = bService.FindActive();
        //    //facturatie_op_IDComboBox.ItemsSource = bedrijven;
        //    //facturatie_op_IDComboBox.DisplayMemberPath = "Naam";

        //    //data.Attesttype_Services aService = new data.Attesttype_Services();
        //    //List<data.AttestType> attesten = aService.FindActive();
        //    //attesttypeComboBox.ItemsSource = attesten;
        //    //attesttypeComboBox.DisplayMemberPath = "Naam";

        //    data.DagType_Services dService = new ConfocusDBLibrary.DagType_Services();
        //    List<data.DagType> dagtypes = dService.FindActive();
        //    dagtypeComboBox.ItemsSource = dagtypes;
        //    dagtypeComboBox.DisplayMemberPath = "Naam";

        //    data.MarketingkanalenServices mService = new data.MarketingkanalenServices();
        //    List<data.Marketingkanalen> kanalen = mService.FindAll();
        //    MarketingKanalenListBox.ItemsSource = kanalen;
        //    MarketingKanalenListBox.DisplayMemberPath = "Marketing_kanaal";

        //    data.SeminarieServices sService = new data.SeminarieServices();
        //    List<data.Seminarie> alleSeminaries = sService.FindAll();
        //    alleSeminarieComboBox.ItemsSource = alleSeminaries;
        //    alleSeminarieComboBox.DisplayMemberPath = "Titel";

        //    data.SprekerServices spService = new data.SprekerServices();
        //    activeSprekers = spService.FindActive();
        //    sprekerComboBox.ItemsSource = activeSprekers;
        //    sprekerComboBox.DisplayMemberPath = "SprekerNaamEnTitel";

        //    LoadAttestTypes();
        //    LoadSeminarieList();

        //}

        private async void LoadDataAsync()
        {
            List<Task<object>> Taken = new List<Task<object>>();

            await Task.Run(() => Parallel.Invoke(LoadNiveau1Async, LoadTaalAsync, LoadSeminarieTypesAsync, LoadErkenningenAsync, LoadGebruikersAsync, LoadLocatiesAsync, LoadDagtypeAsync, LoadMarketingKanalenAsync, LoadAlleSeminariesAsync, LoadSprekerAsync));

            LoadSeminarieListAsync();

        }

        private void LoadSprekerAsync()
        {
            data.SprekerServices spService = new data.SprekerServices();
            activeSprekers = spService.FindActive();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sprekerComboBox.SetValue(ComboBox.ItemsSourceProperty, activeSprekers);}, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sprekerComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "SprekerNaamEnTitel"); }, null);
        }

        private void LoadSeminarieTypesAsync()
        {
            data.SeminarieTypeService tService = new data.SeminarieTypeService();
            List<data.Seminarietype> seminarietypes = tService.GetActive();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { typeComboBox.SetValue(ComboBox.ItemsSourceProperty, seminarietypes); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { typeComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Type"); }, null);

        }

        private void LoadAlleSeminariesAsync()
        {
            data.SeminarieServices sService = new data.SeminarieServices();
            List<data.Seminarie> alleSeminaries = sService.FindAll();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { alleSeminarieComboBox.SetValue(ComboBox.ItemsSourceProperty, alleSeminaries); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { alleSeminarieComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Titel"); }, null);
        }

        private void LoadMarketingKanalenAsync()
        {
            data.MarketingkanalenServices mService = new data.MarketingkanalenServices();
            List<data.Marketingkanalen> kanalen = mService.FindAll();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { MarketingKanalenListBox.SetValue(ComboBox.ItemsSourceProperty, kanalen); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { MarketingKanalenListBox.SetValue(ComboBox.DisplayMemberPathProperty, "Marketing_kanaal"); }, null);
        }

        private void LoadDagtypeAsync()
        {
            data.DagType_Services dService = new ConfocusDBLibrary.DagType_Services();
            List<data.DagType> dagtypes = dService.FindActive();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { dagtypeComboBox.SetValue(ComboBox.ItemsSourceProperty, dagtypes); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { dagtypeComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
        }

        private void LoadLocatiesAsync()
        {
            data.LocatieServices lService = new data.LocatieServices();
            List<data.Locatie> locaties = lService.FindActive();

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { locatieComboBox1.SetValue(ComboBox.ItemsSourceProperty, locaties); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { locatieComboBox1.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
        }

        private void LoadGebruikersAsync()
        {
            data.GebruikerService gService = new data.GebruikerService();
            List<data.Gebruiker> verantwoordelijken = gService.FindActive();

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { verantwoordelijkeComboBox.SetValue(ComboBox.ItemsSourceProperty, verantwoordelijken); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { verantwoordelijkeComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { terplaatseComboBox.SetValue(ComboBox.ItemsSourceProperty, verantwoordelijken); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { terplaatseComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieVerantwoordelijkeComboBox.SetValue(ComboBox.ItemsSourceProperty, verantwoordelijken); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieVerantwoordelijkeComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
        }

        private void LoadNiveau1Async()
        {
            data.Niveau1Services n1Service = new data.Niveau1Services();
            List<data.Niveau1> n1list = n1Service.FindActive();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { Niveau1ComboBox.SetValue(ComboBox.ItemsSourceProperty, n1list); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { Niveau1ComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { Niveau1ComboBox.SetValue(ComboBox.SelectedIndexProperty, -1); }, null);
        }

        private void LoadTaalAsync()
        {
            data.TaalServices tService = new data.TaalServices();
            List<data.Taal> talen = tService.FindAll();

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SeminarieTaalComboBox.SetValue(ComboBox.ItemsSourceProperty, talen); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { SeminarieTaalComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam"); }, null);
        }

        private void LoadErkenningenAsync()
        {
            data.ErkenningServices eService = new data.ErkenningServices();
            List<data.Erkenning> erkenningen = eService.FindActive();

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { seminarieErkenningComboBox.SetValue(ComboBox.ItemsSourceProperty, erkenningen); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate {
                seminarieErkenningComboBox.SetValue(ComboBox.DisplayMemberPathProperty, "Naam");
            }, null);
        }

        private async void LoadSeminarieListAsync()
        {
            data.SeminarieServices sService = new data.SeminarieServices();
            List<data.Seminarie> seminaries;
            if (IsHistoriek)
                seminaries = await Task.Run(() => sService.FindAll());
            else
                seminaries = await Task.Run(() => sService.FindAll());//sService.FindActive());

            List <data.Seminarie> sessieSeminaries = new List<data.Seminarie>();
            foreach (data.Seminarie sem in seminaries)
            {
                sessieSeminaries.Add(sem);
            }

            data.Seminarie nieuw = new data.Seminarie();
            nieuw.Titel = "Nieuw";
            seminaries.Insert(0, nieuw);
            SeminarieComboBox.ItemsSource = seminaries;
            SeminarieComboBox.DisplayMemberPath = "Titel";

            if (currentSeminarie != null)
                SeminarieComboBox.SelectedItem = currentSeminarie;
            else
                SeminarieComboBox.SelectedIndex = 0;
            IsChanged = false;
            Task.Run(() => LoadSeminarie_ErkenningListAsync());
            Task.Run(() => LoadSeminareDoelgroepenAsync(currentSeminarie));
            if (deliveredSessie != null)
            {
                sessieDataGrid.SelectedItem = deliveredSessie;
            }

            deliveredSessie = null;
        }

        private void LoadSeminarie_ErkenningListAsync()
        {
            if (currentSeminarie != null)
            {
                data.Seminarie_ErkenningenService eService = new data.Seminarie_ErkenningenService();
                List<data.Seminarie_Erkenningen> erkenningen = eService.GetBySeminarieID(currentSeminarie.ID);
                try
                {
                    seminarie_ErkenningenViewSource.Source =  erkenningen;
                }
                catch (Exception)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { seminarie_ErkenningenViewSource.SetValue(DataGrid.ItemsSourceProperty, erkenningen); }, null);
                }
            }
        }


        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void volgendeButton_Click(object sender, RoutedEventArgs e)
        {
            DoelgroepTab.IsSelected = true;
        }

        private void SaveSeminarieButton_Click(object sender, RoutedEventArgs e)
        {
            if (SeminarieComboBox.SelectedIndex > 0)
            {
                int index = SeminarieComboBox.SelectedIndex;
                SaveSeminarieChanges();
                SeminarieComboBox.SelectedIndex = index;
            }
            else
            {
                SaveNewSeminarie();
                //LoadSeminarieList();
            }
            if (currentSeminarie != null)
                new data.SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser);
            LoadSessionsAsync();
            LoadSeminarieListAsync();
            IsChanged = false;
        }

        private void SaveSeminarieChanges()
        {
            data.Seminarie mySeminarie = GetSeminarieFromForm();
            if (mySeminarie != null)
            {
                try
                {
                    mySeminarie.ID = ((data.Seminarie)SeminarieComboBox.SelectedItem).ID;
                    mySeminarie.Modified = currentSeminarie.Modified;
                    mySeminarie.Gewijzigd = DateTime.Now;
                    mySeminarie.Gewijzigd_door = currentUser.Naam;
                    currentSeminarie = null;
                    data.SeminarieServices sService = new data.SeminarieServices();
                    data.Seminarie savedSeminarie = sService.SaveSeminarieChanges(mySeminarie);

                    if (mySeminarie.Actief == false)
                    {
                        sService.ZetSessiesOpNonActief(mySeminarie.ID);
                        sService.ZetErkenningenOpNonActief(mySeminarie.ID);
                    }
                    else
                    {
                        sService.ZetSessieOpActief(mySeminarie.ID);
                        sService.ZetErkenningenOpActief(mySeminarie.ID);
                    }

                    if (savedSeminarie != null)
                    {
                        ConfocusMessageBox.Show("Seminarie opgeslagen", ConfocusMessageBox.Kleuren.Blauw);
                        currentSeminarie = savedSeminarie;

                    }
                    else
                        ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Blauw);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void ActivateOnderwerpTab()
        {
        }

        private void SaveNewSeminarie()
        {
            data.Seminarie mySeminarie = GetSeminarieFromForm();
            if (mySeminarie != null)
            {
                mySeminarie.Aangemaakt = DateTime.Now;
                mySeminarie.Aangemaakt_door = currentUser.Naam;
                data.SeminarieServices sService = new data.SeminarieServices();
                data.Seminarie savedSeminarie = sService.SaveSeminarie(mySeminarie);
                if (savedSeminarie != null)
                {
                    ConfocusMessageBox.Show("Seminarie opgeslagen", ConfocusMessageBox.Kleuren.Blauw);
                    currentSeminarie = savedSeminarie;
                    ActivateOnderwerpTab();
                }
                else
                    ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Blauw);
            }
        }

        private data.Seminarie SaveNewSeminarie(data.Seminarie seminarie)
        {
            seminarie.Aangemaakt = DateTime.Now;
            seminarie.Aangemaakt_door = currentUser.Naam;
            data.SeminarieServices sService = new data.SeminarieServices();
            data.Seminarie savedSeminarie = sService.SaveSeminarie(seminarie);
            return savedSeminarie;
        }

        private data.Seminarie GetSeminarieFromForm()
        {
            if (IsSeminarieValid())
            {
                data.Seminarie seminarie = new data.Seminarie();
                seminarie.Titel = titelTextBox.Text;
                seminarie.Type = typeComboBox.Text;
                seminarie.Weeknummer = weeknummerTextBox.Text;
                if (startdatum_organisatieDatePicker.SelectedDate != null)
                    seminarie.Startdatum_organisatie = startdatum_organisatieDatePicker.SelectedDate;
                if (deadline_organisatieDatePicker.SelectedDate != null)
                    seminarie.Deadline_organisatie = deadline_organisatieDatePicker.SelectedDate;
                if (start_marketingDatePicker.SelectedDate != null)
                    seminarie.Start_marketing = start_marketingDatePicker.SelectedDate;
                if (verantwoordelijkeComboBox.SelectedIndex > -1)
                    seminarie.Verantwoordelijke_ID = ((data.Gebruiker)verantwoordelijkeComboBox.SelectedItem).ID;
                //if (doelgroepComboBox.SelectedIndex > -1)
                //    seminarie.Doelgroep_ID = ((Doelgroep)doelgroepComboBox.SelectedItem).ID;
                if (SeminarieTaalComboBox.SelectedIndex > -1)
                {
                    seminarie.Taal_ID = ((data.Taal)SeminarieTaalComboBox.SelectedItem).ID;
                }
                if (seminarieStatusComboBox.SelectedIndex > -1)
                    seminarie.Status = seminarieStatusComboBox.Text;
                seminarie.Actief = actiefCheckBox.IsChecked == true;

                return seminarie;
            }
            else
                return null;
        }

        private bool IsSeminarieValid()
        {
            bool valid = true;
            String errortext = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));

            if (titelTextBox.Text == "")
            {
                errortext += "Geef de seminarie een naam. Dit is verplicht!\n";
                valid = false;
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

            if (seminarieStatusComboBox.SelectedIndex == -1)
            {
                errortext += "Geef een status aan. Dit is verplicht!\n";
                valid = false;
            }

            if (SeminarieTaalComboBox.SelectedIndex == -1)
            {
                errortext += "Er is geen taal gekozen voor deze seminarie! Dit is verplicht!\n";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void seminarieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            data.Seminarie sem = (data.Seminarie)SeminarieComboBox.SelectedItem;
            SeminarieReload();
            LoadSeminarieMarketingAsync(sem);
        }

        private void SeminarieReload()
        {
            try
            {
                data.Seminarie seminarie = new data.Seminarie();
                if (SeminarieComboBox.SelectedIndex > 0)
                {
                    seminarie = (data.Seminarie)SeminarieComboBox.SelectedItem;
                    if (UserClick)
                        currentSeminarie = seminarie;
                    BindSeminarie(seminarie);
                    ClearSessies();
                    LoadSessionsAsync();
                    Task.Run(() => ReloadSeminarieErkenningenAsync());
                    ReloadDoelgroepenAsync();
                }
                else
                {
                    ClearSeminarie();
                    ClearSessies();
                }
                if (SeminarieComboBox.SelectedIndex > 0)
                {
                    SessieSeminarieLabel.Content = seminarie.Titel;
                    MarketingSeminarieLabel.Content = seminarie.Titel;
                    this.Title = rootTitle + ": " + seminarie.Titel;
                }
                else
                {
                    SessieSeminarieLabel.Content = "Geen seminarie gekozen!";
                    MarketingSeminarieLabel.Content = "Geen seminarie gekozen!";
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het ophalen van de seminarie gegevens!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
            UserClick = false;
        }

        private void ClearSessies()
        {
            sessieViewSource.Source = new List<data.Sessie>();
            ClearSessie();
        }

        private void ClearSeminarie()
        {
            titelTextBox.Text = "";
            weeknummerTextBox.Text = "";
            seminarieStatusComboBox.SelectedIndex = -1;
            typeComboBox.SelectedIndex = -1;
            startdatum_organisatieDatePicker.SelectedDate = null;
            start_marketingDatePicker.SelectedDate = null;
            deadline_organisatieDatePicker.SelectedDate = null;
            verantwoordelijkeComboBox.SelectedIndex = -1;
            //doelgroepComboBox.SelectedIndex = -1;
            actiefCheckBox.IsChecked = true;
            //currentSeminarie = null;
            IsChanged = false;
        }

        private void BindSeminarie(data.Seminarie seminarie)
        {
            ClearSeminarie();
            titelTextBox.Text = seminarie.Titel;
            typeComboBox.Text = seminarie.Type;
            if (seminarie.Status != null)
                seminarieStatusComboBox.Text = seminarie.Status;
            weeknummerTextBox.Text = seminarie.Weeknummer;
            if (seminarie.Startdatum_organisatie != null)
                startdatum_organisatieDatePicker.SelectedDate = seminarie.Startdatum_organisatie;
            if (seminarie.Deadline_organisatie != null)
                deadline_organisatieDatePicker.SelectedDate = seminarie.Deadline_organisatie;
            if (seminarie.Start_marketing != null)
                start_marketingDatePicker.SelectedDate = seminarie.Start_marketing;

            if (seminarie.Verantwoordelijke_ID != null)
            {
                data.GebruikerService gService = new data.GebruikerService();
                data.Gebruiker gebruiker = gService.GetUserByID(seminarie.Verantwoordelijke_ID);
                if (gebruiker != null)
                    verantwoordelijkeComboBox.Text = gebruiker.Naam;
            }
            if (seminarie.Projectnummer != null)
            {
                projectnummerTextBox.Text = seminarie.Projectnummer;
            }
            if (seminarie.Taal_ID != null)
                SeminarieTaalComboBox.Text = seminarie.Taalnaam;
            else
                SeminarieTaalComboBox.SelectedIndex = -1;

            actiefCheckBox.IsChecked = seminarie.Actief;
            currentSeminarie = seminarie;
            IsChanged = false;
        }


        private void SaveDoelgroep(data.Doelgroep doelgroep)
        {
            data.DoelgroepServices service = new data.DoelgroepServices();
            data.Doelgroep savedDoelgroep = service.SaveDoelgroep(doelgroep);
            if (savedDoelgroep != null)
            {
                ConfocusMessageBox.Show("Onderdeel opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                LoadDoelgroepData();
            }
            else
                ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);
        }

        private void SaveOnderwerpChanges(data.Doelgroep onderwerp)
        {
            data.DoelgroepServices service = new data.DoelgroepServices();
            data.Doelgroep savedOnderwerp = service.SaveDoelgroepChanges(onderwerp);
            if (savedOnderwerp != null)
            {
                ConfocusMessageBox.Show("Onderdeel opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                //LoadOnderwerpData();
                LoadDoelgroepData();
            }
            else
                ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);
        }


        private void onderwerpVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            MarketingTab.IsSelected = true;
        }


        private void LoadDoelgroepData()
        {
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (IsChanged)
            {
                if (MessageBox.Show("Er zijn nog onopgeslagen wijzigingen!\n Wil U toch afsluiten?", "Wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    e.Cancel = true;
            }
            else if (currentSeminarie != null && sessieDataGrid.Items.Count < 1)
            {
                ConfocusMessageBox.Show(data.ErrorMessages.GeenSessieIngegeven, ConfocusMessageBox.Kleuren.Rood);
                e.Cancel = true;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string name = box.Name;
            IsChanged = true;
            if (sessieDataGrid != null)
                sessieDataGrid.IsEnabled = false;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsChanged = true;
            if (sessieDataGrid != null)
                sessieDataGrid.IsEnabled = false;
        }

        private void DatePickers_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            IsChanged = true;
            if (sessieDataGrid != null)
                sessieDataGrid.IsEnabled = false;
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (currentSessie != null)
            {
                if (currentSessie.HasInschrijvingen)
                {
                    ConfocusMessageBox.Show("Je kan deze sessie niet verwijderen! Er zijn nog inschrijvingen voor deze sessie.", ConfocusMessageBox.Kleuren.Rood);
                    CheckBox box = (CheckBox)sender;
                    box.IsChecked = true;
                }
                else
                {
                    IsChanged = true;
                    if (sessieDataGrid != null)
                        sessieDataGrid.IsEnabled = false;
                }
            }
        }


        /*************************** Inschrijvingen *******************************/

        private void SetDeelnemers()
        {
            try
            {
                data.DeelnemerServices dService = new data.DeelnemerServices();
                List<data.Deelnemer> deelnemers = dService.GetFromSessie(currentSessie.ID);//.GetDeelmenersByInschrijvingID(inschrijving.ID);
                deelnemerViewSource.GroupDescriptions.Clear();
                deelnemerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SessieNaam"));
                deelnemerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("Bedrijfsnaam"));
                deelnemerViewSource.Source = deelnemers;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        #region Inschrijvingen

        private void CheckBox_Click_1(object sender, RoutedEventArgs e)
        {
            CheckBox check = (CheckBox)sender;
            ContentPresenter CP = (ContentPresenter)check.TemplatedParent;
            String s = CP.TemplatedParent.ToString();
            ListBoxItem CBI = (ListBoxItem)CP.TemplatedParent;
            CBI.IsSelected = (bool)check.IsChecked;
            //HaalGeselecteerdeOp();
        }


        #endregion


        #region Marketing kanalen

        private void KanaalToevoegenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SeminarieComboBox.SelectedIndex > 0)
                {
                    if (MarketingKanalenListBox.SelectedIndex > -1)
                    {
                        data.Seminarie_Marketing marketing = new data.Seminarie_Marketing();
                        data.Seminarie seminarie = (data.Seminarie)SeminarieComboBox.SelectedItem;
                        data.Marketingkanalen kanaal = (data.Marketingkanalen)MarketingKanalenListBox.SelectedItem;

                        marketing.Seminarie_ID = seminarie.ID;
                        marketing.Marketing_ID = kanaal.ID;
                        marketing.Status = statusTextBox1.Text;
                        marketing.Opmerking = OpmerkingTextBox1.Text;
                        if (opvolgdatumDatePicker1.SelectedDate != null)
                            marketing.Opvolgdatum = opvolgdatumDatePicker1.SelectedDate;

                        data.Seminarie_MarketingServices smService = new data.Seminarie_MarketingServices();
                        smService.SaveSeminarieMarketing(marketing);
                        Task.Run(() => LoadSeminarieMarketingAsync(currentSeminarie));
                        statusTextBox1.Text = "";
                        opvolgdatumDatePicker1.SelectedDate = null;
                        if (currentSeminarie != null)
                            new data.SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser);
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void LoadSeminarieMarketingAsync(data.Seminarie seminarie)
        {
            if (SeminarieComboBox.SelectedIndex > 0)
            {
                //data.Seminarie seminarie = (data.Seminarie)SeminarieComboBox.SelectedItem;
                data.Seminarie_MarketingServices smService = new data.Seminarie_MarketingServices();
                List<data.Seminarie_Marketing> seminarieMarketing = smService.GetMarketingBySeminarie(seminarie.ID);

                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { seminarie_MarketingViewSource.SetValue(CollectionViewSource.SourceProperty, seminarieMarketing); }, null);
            }
        }

        private void seminarie_MarketingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (seminarie_MarketingDataGrid.SelectedIndex > -1)
            {
                data.Seminarie_Marketing marketing = (data.Seminarie_Marketing)seminarie_MarketingDataGrid.SelectedItem;
                NieuwMarketingKanaal NM = new NieuwMarketingKanaal(currentUser, marketing.Marketing_ID);
                if (NM.ShowDialog() == true)
                {
                    LoadSeminarieMarketingAsync(currentSeminarie);
                }
            }
        }

        private void MarketingVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            ErkenningenTab.IsSelected = true;
        }

        #endregion



        private void GridDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            DatePicker picker = (DatePicker)sender;
            data.Seminarie_Marketing kanaal = (data.Seminarie_Marketing)seminarie_MarketingDataGrid.SelectedItem;

            if (picker.SelectedDate.HasValue)
            {
                kanaal.Opvolgdatum = picker.SelectedDate;
            }
            else
                kanaal.Opvolgdatum = null;

            data.Seminarie_MarketingServices mService = new data.Seminarie_MarketingServices();
            mService.UpdateSeminarieMarketing(kanaal);
        }

        private void GridTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            data.Seminarie_Marketing kanaal = (data.Seminarie_Marketing)seminarie_MarketingDataGrid.SelectedItem;
            if (box.Name == "statusGridTextBox")
                kanaal.Status = box.Text;
            else
                kanaal.Opmerking = box.Text;

            data.Seminarie_MarketingServices mService = new data.Seminarie_MarketingServices();
            mService.UpdateSeminarieMarketing(kanaal);
        }

        private void deelnemerExtraButton_Click(object sender, RoutedEventArgs e)
        {
            data.Seminarie seminarie = (data.Seminarie)SeminarieComboBox.SelectedItem;
            data.Sessie sessie = (data.Sessie)sessieDataGrid.SelectedItem;
            if (seminarie != null && sessie != null)
            {
                Aanwezigheden AW = new Aanwezigheden(seminarie.ID, sessie.ID, currentUser);
                AW.Show();
            }
            else
            {
                Aanwezigheden AW = new Aanwezigheden(null, null, currentUser);
                AW.Show();
            }
        }

        private void VanBestaandeButton_Click(object sender, RoutedEventArgs e)
        {
            if (alleSeminarieComboBox.SelectedIndex > -1)
            {
                try
                {
                    //UserClick = true;
                    data.Seminarie oldSeminarie = (data.Seminarie)alleSeminarieComboBox.SelectedItem;
                    data.Seminarie newSeminarie = new data.Seminarie();
                    newSeminarie.Titel = oldSeminarie.Titel;
                    newSeminarie.Doelgroep_ID = oldSeminarie.Doelgroep_ID;
                    newSeminarie.Type = oldSeminarie.Type;
                    newSeminarie.Verantwoordelijke_ID = oldSeminarie.Verantwoordelijke_ID;
                    newSeminarie.Actief = true;
                    newSeminarie.Status = "Te plannen";
                    data.Seminarie savedSeminarie = SaveNewSeminarie(newSeminarie);
                    List<data.Sessie> oldSessies = GetSessies(oldSeminarie);
                    foreach (data.Sessie sessie in oldSessies)
                    {
                        data.Sessie newSessie = new data.Sessie();
                        newSessie.Naam = sessie.Naam;
                        if (sessie.Datum != null)
                            newSessie.Datum = ((DateTime)sessie.Datum).AddYears(1);
                        else
                            newSessie.Datum = DateTime.Now.AddYears(1);
                        newSessie.Status = "Actief";
                        newSessie.URL = "www.confocus.be/";
                        newSessie.Verantwoordelijke_Sessie_ID = oldSeminarie.Verantwoordelijke_ID;
                        newSessie.Verantwoordelijke_Terplaatse_ID = oldSeminarie.Verantwoordelijke_ID;
                        newSessie.Actief = true;
                        newSessie.Seminarie_ID = savedSeminarie.ID;
                        SaveNewSessie(newSessie);
                    }

                    List<data.Seminarie_Doelgroep> doelgroepen = new data.Seminarie_DoelgroepServices().GetDoelgroepenBySeminarieID(oldSeminarie.ID);
                    foreach (data.Seminarie_Doelgroep item in doelgroepen)
                    {
                        data.Seminarie_Doelgroep newDoelgroep = new data.Seminarie_Doelgroep();
                        newDoelgroep.Niveau1_ID = item.Niveau1_ID;
                        newDoelgroep.Niveau2_ID = item.Niveau2_ID;
                        newDoelgroep.Niveau3_ID = item.Niveau3_ID;
                        newDoelgroep.Seminarie_ID = savedSeminarie.ID;
                        newDoelgroep.Opmerking = item.Opmerking;

                        SaveNewDoelgroep(newDoelgroep);
                    }

                    List<data.Seminarie_Marketing> oldmarketing = new data.Seminarie_MarketingServices().GetMarketingBySeminarie(oldSeminarie.ID);
                    foreach (data.Seminarie_Marketing item in oldmarketing)
                    {
                        data.Seminarie_Marketing newMarketing = new data.Seminarie_Marketing();
                        newMarketing.Marketing_ID = item.Marketing_ID;
                        newMarketing.Seminarie_ID = savedSeminarie.ID;

                        saveNewMarketing(newMarketing);
                    }

                    List<data.Seminarie_Erkenningen> oldErkenningen = new data.Seminarie_ErkenningenService().GetBySeminarieID(oldSeminarie.ID);
                    foreach (data.Seminarie_Erkenningen item in oldErkenningen)
                    {
                        data.Seminarie_Erkenningen newErkenning = new data.Seminarie_Erkenningen();
                        newErkenning.Erkenning_ID = item.Erkenning_ID;
                        newErkenning.Seminarie_ID = savedSeminarie.ID;

                        SaveNewErkenning(newErkenning);
                    }

                    LoadSeminarieListAsync();
                    SeminarieComboBox.SelectedItem = savedSeminarie;

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het dupliceren van de seminarie!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                ConfocusMessageBox.Show("Er is geen seminarie gekozen om te dupliceren!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SaveNewErkenning(data.Seminarie_Erkenningen erkenning)
        {
            try
            {
                data.Seminarie_ErkenningenService service = new data.Seminarie_ErkenningenService();
                service.SaveSeminarie_Erkenningen(erkenning);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void saveNewMarketing(data.Seminarie_Marketing marketing)
        {
            try
            {
                data.Seminarie_MarketingServices service = new data.Seminarie_MarketingServices();
                service.SaveSeminarieMarketing(marketing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveNewDoelgroep(data.Seminarie_Doelgroep doelgroep)
        {
            try
            {
                data.Seminarie_DoelgroepServices service = new ConfocusDBLibrary.Seminarie_DoelgroepServices();
                doelgroep.Aangemaakt = DateTime.Now;
                doelgroep.Aangemaakt_door = currentUser.Naam;
                doelgroep.Actief = true;
                service.SaveSeminarieDoelgroep(doelgroep);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveNewSessie(data.Sessie newSessie)
        {
            try
            {
                newSessie.Aangemaakt = DateTime.Now;
                newSessie.Aangemaakt_door = currentUser.Naam;
                data.SessieServices sService = new data.SessieServices();
                sService.SaveSessie(newSessie);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<data.Sessie> GetSessies(data.Seminarie oldSeminarie)
        {
            data.SessieServices sService = new data.SessieServices();
            List<data.Sessie> sessies = sService.GetSessiesBySeminarieID((Decimal)oldSeminarie.ID);
            return sessies;
        }

        //private void doelgroep_ToevoegButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (currentSeminarie != null && currentSeminarie.ID > 0)
        //    {

        //    }
        //    else
        //        ConfocusMessageBox.Show("Er is geen seminarie ingevuld of opgeslagen.", ConfocusMessageBox.Kleuren.Rood);
        //}

        private void LoadSeminareDoelgroepen()
        {
            if (currentSeminarie != null && currentSeminarie.ID > 0)
            {
                data.Seminarie_DoelgroepServices sdService = new data.Seminarie_DoelgroepServices();
                List<data.Seminarie_Doelgroep> doelgroepen = sdService.GetDoelgroepenBySeminarieID(currentSeminarie.ID);

                seminarie_DoelgroepViewSource.Source = doelgroepen;
            }
            else
                seminarie_DoelgroepViewSource.Source = new List<data.Seminarie_Doelgroep>();
        }

        private void LoadSeminareDoelgroepenAsync(data.Seminarie seminarie)
        {
            if (seminarie != null && seminarie.ID > 0)
            {
                data.Seminarie_DoelgroepServices sdService = new data.Seminarie_DoelgroepServices();
                List<data.Seminarie_Doelgroep> doelgroepen = sdService.GetDoelgroepenBySeminarieID(currentSeminarie.ID);

                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { seminarie_DoelgroepViewSource.SetValue(CollectionViewSource.SourceProperty, doelgroepen); }, null);
            }
            else
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { seminarie_DoelgroepViewSource.SetValue(CollectionViewSource.SourceProperty, seminarie_DoelgroepViewSource.Source = new List<data.Seminarie_Doelgroep>()); }, null);
        }

        private void doelgroepNieuwButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweDoelgroep ND = new NieuweDoelgroep(currentUser);
            if (ND.ShowDialog() == true)
            {
                LoadDoelgroepData();
            }
        }


        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweErkenning NE = new NieuweErkenning(currentUser);
            if (NE.ShowDialog() == true)
            {
                LoadErkenningenAsync();
            }
        }

        private void erkenningToevoegButton_Click(object sender, RoutedEventArgs e)
        {
            if (seminarieErkenningComboBox.SelectedIndex > -1)
            {
                data.Seminarie_Erkenningen erkenning = GetSeminarieErkenningFromForm(false);
                if (erkenning != null)
                {
                    data.Seminarie_ErkenningenService eService = new data.Seminarie_ErkenningenService();
                    try
                    {
                        erkenning = eService.SaveSeminarie_Erkenningen(erkenning);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                    LoadSeminarie_ErkenningListAsync();
                    foreach (data.Seminarie_Erkenningen erk in seminarie_ErkenningenDataGrid.Items)
                    {
                        if (erk.ID == erkenning.ID)
                            seminarie_ErkenningenDataGrid.SelectedItem = erk;
                    }
                    Helper.SetAantalByInschrijvingen(erkenning, currentUser.Naam, false);
                    if (AllesRadioButton.IsChecked == true)
                        InschrijvingenFilter_Click(AllesRadioButton, new RoutedEventArgs());
                    else if (SessieRadioButton.IsChecked == true)
                        InschrijvingenFilter_Click(SessieRadioButton, new RoutedEventArgs());
                    else if (GefactureerdRadioButton.IsChecked == true)
                        InschrijvingenFilter_Click(GefactureerdRadioButton, new RoutedEventArgs());
                    else
                        InschrijvingenFilter_Click(NietGefactureerdRadioButton, new RoutedEventArgs());
                }
                if (currentSessie != null)
                    new data.SessieServices().UpdateSeminarieSessiesTimeStamp((decimal)currentSessie.Seminarie_ID, currentUser);
            }
        }


        private data.Seminarie_Erkenningen GetSeminarieErkenningFromForm(bool update)
        {
            if (IsSemErkValid(update))
            {
                data.Seminarie_Erkenningen erkenning = new data.Seminarie_Erkenningen();
                erkenning.Seminarie_ID = currentSeminarie.ID;
                erkenning.Erkenning_ID = ((data.Erkenning)seminarieErkenningComboBox.SelectedItem).ID;
                erkenning.Datum = datumDatePicker1.SelectedDate;
                erkenning.Nummer = nummerTextBox.Text;
                erkenning.Status = erkenningStatusComboBox.Text;
                erkenning.Opmerking = ErkenningOpmerkingTextBox.Text;
                //if (aantalTextBox.Text != "")
                //{
                //    var numberFormatInfo = new NumberFormatInfo();
                //    numberFormatInfo.NumberDecimalSeparator = ",";
                //    erkenning.Aantal = Decimal.Parse(aantalTextBox.Text, numberFormatInfo);
                //}
                if (erkenningSessieComboBox.SelectedIndex > 0)
                    erkenning.Sessie_ID = ((data.Sessie)erkenningSessieComboBox.SelectedItem).ID;
                //erkenning.Type = erkenningTypeComboBox.Text;
                //erkenning.Actief = actiefCheckBox1.IsChecked == true;
                erkenning.TAantal = aantalTextBox.Text;
                erkenning.Actief = true;
                erkenning.Aangemaakt_door = currentUser.Naam;
                erkenning.Aangemaakt = DateTime.Now;

                return erkenning;
            }
            else
                return null;
        }

        private bool IsSemErkValid(bool update)
        {
            bool valid = true;
            String errortext = "";

            if (seminarieErkenningComboBox.SelectedIndex < 0)
            {
                valid = false;
                errortext += "Er is geen erkenning gekozen.\n";
            }


            if (currentSeminarie == null)
            {
                valid = false;
                errortext += "Er is geen seminarie gekozen in het tab Seminarie!\n";
            }

            if (erkenningSessieComboBox.SelectedIndex < 1)
            {
                valid = false;
                errortext += "Er is geen seminarie gekozen!\n";
            }

            //if (aantalTextBox.Text != "")
            //{
            //    decimal aantal = 99999;
            //    if (!decimal.TryParse(aantalTextBox.Text, out aantal))
            //    {
            //        valid = false;
            //        errortext += "Het aantal is geen geldig getal!\nEnkel gehele getallen (6) en komma getallen (5,5) zijn geldig.";
            //    }
            //}
            if (valid && !update)
            {
                foreach (data.Seminarie_Erkenningen erk in seminarie_ErkenningenDataGrid.Items)
                {
                    if (((data.Erkenning)seminarieErkenningComboBox.SelectedItem).ID == erk.Erkenning_ID)
                        if (erk.Sessie_ID == ((data.Sessie)erkenningSessieComboBox.SelectedItem).ID)
                        {
                            valid = false;
                            errortext = "Deze erkenning is reeds toegevoegd aan deze Sessie!";
                        }
                }
            }

            if (!valid)
            {
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            }
            return valid;
        }

        private void ErkenningVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            SessiesTab.IsSelected = true;
        }

        private void seminarie_ErkenningenDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seminarie_ErkenningenDataGrid.SelectedIndex > -1)
            {
                data.Seminarie_Erkenningen erkenning = (data.Seminarie_Erkenningen)seminarie_ErkenningenDataGrid.SelectedItem;
                seminarieErkenningComboBox.Text = erkenning.Erkenning.Naam;
                SetSeminarieErkenning(erkenning);

            }
        }

        private void seminarieErkenningComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seminarieErkenningComboBox.SelectedIndex > -1)
            {
                data.Erkenning selected = (data.Erkenning)seminarieErkenningComboBox.SelectedItem;
                if (seminarie_ErkenningenDataGrid.Items.Count > 0)
                {
                    data.Seminarie_Erkenningen erkenning = null;
                    foreach (var item in seminarie_ErkenningenDataGrid.Items)
                    {
                        data.Seminarie_Erkenningen myerkenning = item as data.Seminarie_Erkenningen;
                        if (myerkenning.Erkenning_ID == selected.ID)
                        {
                            data.Seminarie_ErkenningenService eService = new data.Seminarie_ErkenningenService();
                            erkenning = eService.GetByID(myerkenning.ID);
                            break;
                        }
                    }
                    if (erkenning != null)
                    {
                        SetSeminarieErkenning(erkenning);
                    }
                    else
                        ClearSeminarieErkenningFields();
                }
                else
                    ClearSeminarieErkenningFields();
            }
        }

        private void ClearSeminarieErkenningFields()
        {
            datumDatePicker1.SelectedDate = null;
            erkenningStatusComboBox.SelectedIndex = -1;
            //erkenningTypeComboBox.SelectedIndex = -1;
            aantalTextBox.Text = "";
            nummerTextBox.Text = "";
            //actiefCheckBox1.IsChecked = true;
        }

        private void SetSeminarieErkenning(data.Seminarie_Erkenningen erkenning)
        {
            datumDatePicker1.SelectedDate = erkenning.Datum;
            erkenningStatusComboBox.Text = erkenning.Status;
            //erkenningTypeComboBox.Text = erkenning.Type;
            aantalTextBox.Text = erkenning.TAantal;
            nummerTextBox.Text = erkenning.Nummer;
            ErkenningOpmerkingTextBox.Text = erkenning.Opmerking;
            if (erkenning.Sessie == null)
            {
                erkenningSessieComboBox.SelectedIndex = -1;
            }
            else
                erkenningSessieComboBox.Text = erkenning.Sessie.NaamDatumLocatie;
            //actiefCheckBox1.IsChecked = erkenning.Actief;
        }

        private void seminarie_ErkenningenDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (seminarie_ErkenningenDataGrid.SelectedIndex > -1)
            {
                data.Erkenning erkenning = ((data.Erkenning)((data.Seminarie_Erkenningen)seminarie_ErkenningenDataGrid.SelectedItem).Erkenning);
                ErkenningBeheer EB = new ErkenningBeheer(currentUser, erkenning.ID);
                if (EB.ShowDialog() == true)
                {

                }
            }
        }

        private void SeminarieComboBox_DropDownClosed(object sender, EventArgs e)
        {
            UserClick = false;
        }

        private void SeminarieComboBox_DropDownOpened(object sender, EventArgs e)
        {
            UserClick = true;
        }


        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInit)
            {
                if (e.OriginalSource == seminarieTabControl)
                {
                    if (IsChanged == true)
                    {

                        e.Handled = true;
                        IsInit = true;
                        seminarieTabControl.SelectedIndex = _TabIndex;
                        IsInit = false;
                        ConfocusMessageBox.Show("Er zijn nog onopgeslagen wijzigingen!\nSla deze eerst op.", ConfocusMessageBox.Kleuren.Rood);
                    }
                    else
                        _TabIndex = seminarieTabControl.SelectedIndex;
                }
            }
        }


        private void Niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (Niveau1ComboBox.SelectedIndex > -1)
                {
                    data.Niveau1 n1 = (data.Niveau1)Niveau1ComboBox.SelectedItem;
                    data.Niveau2Services n2Service = new data.Niveau2Services();
                    List<data.Niveau2> n2List = n2Service.GetSelected(n1.ID);
                    data.Niveau2 kies = new data.Niveau2();
                    kies.ID = 0;
                    kies.Naam = "Kies";
                    n2List.Insert(0, kies);
                    Niveau2ComboBox.ItemsSource = n2List;
                    Niveau2ComboBox.DisplayMemberPath = "Naam";
                    Niveau2ComboBox.SelectedIndex = 0;
                    Niveau3ComboBox.ItemsSource = new List<data.Niveau3>();
                }
                else
                {
                    Niveau2ComboBox.ItemsSource = new List<data.Niveau2>();
                    Niveau2ComboBox.DisplayMemberPath = "Naam";
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Er is een fout opgetreden bij het selecteren van niveau 1!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void Niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (Niveau2ComboBox.SelectedIndex > -1)
                {
                    data.Niveau2 n2 = (data.Niveau2)Niveau2ComboBox.SelectedItem;
                    data.Niveau3Services n3Service = new data.Niveau3Services();
                    List<data.Niveau3> n3List = n3Service.GetSelected(n2.ID);
                    data.Niveau3 kies = new data.Niveau3();
                    kies.ID = 0;
                    kies.Naam = "Kies";
                    n3List.Insert(0, kies);
                    Niveau3ComboBox.ItemsSource = n3List;
                    Niveau3ComboBox.DisplayMemberPath = "Naam";
                    Niveau3ComboBox.SelectedIndex = 0;
                }
                else
                {
                    Niveau3ComboBox.ItemsSource = new List<data.Niveau3>();
                    Niveau3ComboBox.DisplayMemberPath = "Naam";
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Er is een fout opgetreden bij het selecteren van niveau 2!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }


        private void uRLTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TextBox box = (TextBox)sender;
                Helper.OpenUrl(box.Text);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void sprekerZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                ZoekSprekers();
            }
        }

        private void sessie_SprekerDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sessie_SprekerDataGrid.SelectedItem is data.Sessie_Spreker)
            {
                data.Sessie_Spreker sespreker = (data.Sessie_Spreker)sessie_SprekerDataGrid.SelectedItem;
                data.Spreker spreker = sespreker.Spreker;
                NieuwContact NC = new NieuwContact(currentUser, spreker);
                NC.ShowDialog();
            }
        }

        private void sprekerSessieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInit)
            {
                ComboBox box = (ComboBox)sender;
                if (box.SelectedIndex > -1)
                {
                    //data.Sessie sessie = (data.Sessie)box.SelectedItem;
                    //data.SessieSprekerServices ssService = new data.SessieSprekerServices();
                    //List<data.Sessie_Spreker> sprekers = ssService.GetSprekersBySeminarieID(currentSeminarie.ID);


                    //sessie_SprekerViewSource.Source = sprekers;
                    //totaalSprekerLabel.Content = "totaal sprekers: " + sprekers.Count;
                    //deelnemerSprekerTotaalLabel.Content = sprekers.Count;

                }
            }
        }

        private void verwijderButton_Click(object sender, RoutedEventArgs e)
        {
            data.Seminarie_Marketing marketing = (data.Seminarie_Marketing)seminarie_MarketingDataGrid.SelectedItem;
            if (MessageBox.Show("Wil U het gekozen marketingkanaal verwijderen?", "Verwijderen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                data.Seminarie_MarketingServices smService = new data.Seminarie_MarketingServices();
                try
                {
                    if (smService.Delete(marketing))
                    {
                        ConfocusMessageBox.Show("Marketingkanaal is verwijderd uit dit seminarie.", ConfocusMessageBox.Kleuren.Blauw);
                        LoadSeminarieMarketingAsync(currentSeminarie);
                    }

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Marketingkanaal kon niet verwijderd worden!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void verwijderSprekerButton_Click(object sender, RoutedEventArgs e)
        {
            data.Sessie_Spreker spreker = (data.Sessie_Spreker)sessie_SprekerDataGrid.SelectedItem;
            if (MessageBox.Show("Wil U het gekozen spreker verwijderen uit de sessie?", "Verwijderen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                data.SessieSprekerServices ssService = new data.SessieSprekerServices();
                try
                {
                    if (ssService.Delete(spreker))
                    {
                        ConfocusMessageBox.Show("Spreker is verwijderd uit de sessie.", ConfocusMessageBox.Kleuren.Blauw);
                        //ReloadSessieSprekers();
                        ReloadSprekersAsync();
                        
                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Spreker kon niet verwijderd worden!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }


        private async void ReloadSessieSprekersAsync()
        {
            data.Sessie sessie = null;
            if (sprekerSessieComboBox.SelectedIndex > -1)
            {
                sessie = (data.Sessie)sprekerSessieComboBox.SelectedItem;
            }
            else if (currentSessie != null)
                sessie = currentSessie;

            if (sessie != null)
            {
                data.SessieSprekerServices ssService = new data.SessieSprekerServices();
                List<data.Sessie_Spreker> sprekers = await Task.Run(() => ssService.GetSprekersBySessieID(sessie.ID));
                sessie_SprekerDataGrid.CancelEdit(DataGridEditingUnit.Row);
                sessie_SprekerViewSource.GroupDescriptions.Clear();
                sessie_SprekerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("Sessie.Naam"));
                sessie_SprekerViewSource.Source = sprekers;

                sessie_SprekerViewSource.Source = sprekers;
                totaalSprekerLabel.Content = "totaal sprekers: " + sprekers.Count;
            }
        }


        private void verwijderErkenningButton_Click(object sender, RoutedEventArgs e)
        {
            data.Seminarie_Erkenningen erkenning = (data.Seminarie_Erkenningen)seminarie_ErkenningenDataGrid.SelectedItem;
            data.Seminarie_ErkenningenService eService = new data.Seminarie_ErkenningenService();
            eService.Remove(erkenning);

            Task.Run(() => ReloadSeminarieErkenningenAsync());
        }

        private void ReloadSeminarieErkenningenAsync()
        {
            data.Seminarie_ErkenningenService eService = new data.Seminarie_ErkenningenService();
            List<data.Seminarie_Erkenningen> erkenningen = eService.GetBySeminarieID(currentSeminarie.ID);

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { seminarie_ErkenningenViewSource.SetValue(CollectionViewSource.SourceProperty, erkenningen); }, null);
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            ExportInschrijvingen();
        }

        private void ExportInschrijvingen()
        {
            try
            {
                data.InschrijvingenService iService = new data.InschrijvingenService();
                data.SessieSprekerServices sService = new data.SessieSprekerServices();
                decimal sessieid = currentSessie.ID;
                CSVDocument doc = null;
                string filename = _dataFolder + "// " + currentUser.Naam + "Export.csv";
                bool correct = false;
                int version = 0;
                while (!correct)
                {
                    try
                    {
                        doc = new CSVDocument(filename);
                        correct = true;
                    }
                    catch (Exception)
                    {
                        version++;
                        filename = Helper.NextCSVPath(filename, version);
                    }
                }
                if (doc != null)
                {
                    List<data.Sessie_Spreker> sprekerslist = (from s in sessie_SprekerDataGrid.ItemsSource.Cast<data.Sessie_Spreker>() orderby s.Spreker.Contact.Achternaam select s).ToList();
                    List<data.Inschrijvingen> inschrijvingenLijst = (from i in inschrijvingenDataGrid.ItemsSource.Cast<data.Inschrijvingen>() orderby i.Functies.Contact.Achternaam select i).ToList();
                    List<string> headers = new List<string>() {"Sessie"
                        , "Achternaam"
                        , "Voornaam"
                        , "Oorspronkelijke titel"
                        , "Bedrijf"
                        , "Bedrijf Adres"
                        , "Bedrijf BTWnummer"
                        , "Bedrijf Telefoon"
                        , "Bedrijf Fax"
                        , "Bedrijf Email"
                        , "Bedrijf Website"
                        , "Facuratie op"
                        , "Facuratie Adres"
                        , "Facuratie BTWnummer"
                        , "Facuratie Telefoon"
                        , "Facuratie Fax"
                        , "Facuratie Email"
                        , "Facuratie Website"
                        , "Attesttype"
                        , "Status"
                        , "Telefoonnummer"
                        , "GSM-nummer"
                        , "E-mailadres"
                        , "Type"
                        , "Inschrijvingdatum"
                        , "Notitie"
                        , "Erkenningsnummer"
                        , "Via"
                        , "CC-Email"
                        , "Dagtype"
                        , "Factuurnummer"
                        , "Aanwezig" };
                    doc.AddRow(headers);
                    foreach (data.Sessie_Spreker item in sprekerslist)
                    {
                        if (AllesRadioButton.IsChecked == true)//item.Sessie_ID == sessieid)
                        {
                            data.Sessie_Spreker spreker = sService.GetById(item.ID);
                            List<string> sprekers = new List<string>();
                            sprekers.Add(spreker.SessieNaam);
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Achternaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Voornaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Oorspronkelijke_Titel));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Naam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Adres == null ? "" : spreker.Spreker.Bedrijf.FullAdres));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Ondernemersnummer == null ? "" : spreker.Spreker.Bedrijf.Ondernemersnummer));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Telefoon == null ? "" : spreker.Spreker.Bedrijf.Telefoon));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Fax == null ? "" : spreker.Spreker.Bedrijf.Fax));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Email == null ? "" : spreker.Spreker.Bedrijf.Email));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Website == null ? "" : spreker.Spreker.Bedrijf.Website));
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Attesttype));
                            sprekers.Add("");
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Telefoon == null ? "" : spreker.Spreker.Telefoon));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Mobiel == null ? "" : spreker.Spreker.Mobiel));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Email == null ? "" : spreker.Spreker.Email));
                            sprekers.Add("Spreker");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            doc.AddRow(sprekers);
                        }
                        else if (item.Sessie_ID == sessieid){
                            data.Sessie_Spreker spreker = sService.GetById(item.ID);
                            List<string> sprekers = new List<string>();
                            sprekers.Add(spreker.SessieNaam);
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Achternaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Voornaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Oorspronkelijke_Titel == null ? "" : spreker.Spreker.Oorspronkelijke_Titel));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Naam == null ? "" : spreker.Spreker.Bedrijf.Naam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Adres == null ? "" : spreker.Spreker.Bedrijf.FullAdres));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Ondernemersnummer == null ? "" : spreker.Spreker.Bedrijf.Ondernemersnummer));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Telefoon == null ? "" : spreker.Spreker.Bedrijf.Telefoon));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Fax == null ? "" : spreker.Spreker.Bedrijf.Fax));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Email == null ? "" : spreker.Spreker.Bedrijf.Email));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Website == null ? "" : spreker.Spreker.Bedrijf.Website));
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Attesttype == null ? "" : spreker.Spreker.Attesttype));
                            sprekers.Add("");
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Telefoon == null ? "" : spreker.Spreker.Telefoon));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Mobiel == null ? "" : spreker.Spreker.Mobiel));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Email == null ? "" : spreker.Spreker.Email));
                            sprekers.Add("Spreker");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            sprekers.Add("");
                            doc.AddRow(sprekers);
                        }
                    }
                    foreach (data.Inschrijvingen item in inschrijvingenLijst)
                    {
                       // if (item.Sessie_ID == sessieid)
                        //{
                        data.Inschrijvingen inschrijving = iService.GetInschrijvingByID(item.ID);
                        List<string> deelnemer = new List<string>();
                        deelnemer.Add(inschrijving.SessieNaam);
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Contact.Achternaam == null ? "" : inschrijving.Functies.Contact.Achternaam));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Contact.Voornaam == null ? "" : inschrijving.Functies.Contact.Voornaam));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Oorspronkelijke_Titel == null ? "" : inschrijving.Functies.Oorspronkelijke_Titel));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Naam == null ? "" : inschrijving.Functies.Bedrijf.Naam));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Adres == null ? "" : inschrijving.Functies.Bedrijf.FullAdres));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Ondernemersnummer == null ? "" : inschrijving.Functies.Bedrijf.Ondernemersnummer));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Telefoon == null ? "" : inschrijving.Functies.Bedrijf.Telefoon));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Fax == null ? "" : inschrijving.Functies.Bedrijf.Fax));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Email == null ? "" : inschrijving.Functies.Bedrijf.Email));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Bedrijf.Website == null ? "" : inschrijving.Functies.Bedrijf.Website));
                        if (inschrijving.FacturatieBedrijf != null)
                        {
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Naam == null ? "" : inschrijving.FacturatieBedrijf.Naam));
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Adres == null ? "" : inschrijving.FacturatieBedrijf.FullAdres));
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Ondernemersnummer == null ? "" : inschrijving.FacturatieBedrijf.Ondernemersnummer));
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Telefoon == null ? "" : inschrijving.FacturatieBedrijf.Telefoon));
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Fax == null ? "" : inschrijving.FacturatieBedrijf.Fax));
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Email == null ? "" : inschrijving.FacturatieBedrijf.Email));
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.FacturatieBedrijf.Website == null ? "" : inschrijving.FacturatieBedrijf.Website));
                        }
                        else
                        {
                            deelnemer.Add("");
                            deelnemer.Add("");
                            deelnemer.Add("");
                            deelnemer.Add("");
                            deelnemer.Add("");
                            deelnemer.Add("");
                            deelnemer.Add("");
                        }
                        if (inschrijving.Functies.AttestType1 != null)
                            deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.AttestType1.Naam));
                        else
                            deelnemer.Add("");
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Status));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Telefoon == null ? "" : inschrijving.Functies.Telefoon));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Mobiel == null ? "" : inschrijving.Functies.Mobiel));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Email == null ? "" : inschrijving.Functies.Email));
                        deelnemer.Add("Klant");
                        deelnemer.Add(((DateTime)inschrijving.Datum_inschrijving).ToShortDateString());
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Notitie));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Functies.Erkenningsnummer));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Via));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.CC_Email));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Dagtype));
                        deelnemer.Add(Helper.CleanForExcel(inschrijving.Factuurnummer));
                        deelnemer.Add(inschrijving.Aanwezig == true ? "Ja" : "Nee");
                        doc.AddRow(deelnemer);
                        //}
                    }
                    doc.Save();
                    Helper.OpenUrl(filename);
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }


        private void EditErkenningButton_Click(object sender, RoutedEventArgs e)
        {
            if (seminarie_ErkenningenDataGrid.SelectedIndex > -1)
            {
                data.Seminarie_Erkenningen myerkenning = (data.Seminarie_Erkenningen)seminarie_ErkenningenDataGrid.SelectedItem;
                data.Seminarie_Erkenningen erkenning = GetSeminarieErkenningFromForm(true);
                if (erkenning != null)
                {
                    int index = seminarie_ErkenningenDataGrid.SelectedIndex;
                    data.Seminarie_ErkenningenService eService = new data.Seminarie_ErkenningenService();
                    try
                    {
                        
                        erkenning.ID = myerkenning.ID;
                        erkenning.Modified = myerkenning.Modified;
                        eService.UpdateErkenning(erkenning);
                        Helper.SetAantalByInschrijvingen(erkenning, currentUser.Naam, true);
                        if (AllesRadioButton.IsChecked == true)
                            InschrijvingenFilter_Click(AllesRadioButton, new RoutedEventArgs());
                        else if (SessieRadioButton.IsChecked == true)
                            InschrijvingenFilter_Click(SessieRadioButton, new RoutedEventArgs());
                        else if (GefactureerdRadioButton.IsChecked == true)
                            InschrijvingenFilter_Click(GefactureerdRadioButton, new RoutedEventArgs());
                        else
                            InschrijvingenFilter_Click(NietGefactureerdRadioButton, new RoutedEventArgs());
                        
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show("Fout bij het opslaan van de wijzigingen!/n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                    LoadSeminarie_ErkenningListAsync();
                    seminarie_ErkenningenDataGrid.SelectedIndex = index;
                    if (currentSeminarie != null)
                        new data.SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser );
                }

            }
        }

        private void inschrijvingenDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedItem is data.Inschrijvingen)
            {
                data.Inschrijvingen inschrijving = (data.Inschrijvingen)grid.SelectedItem;
                data.FunctieServices fService = new data.FunctieServices();
                data.Functies functie = fService.GetFunctieByID((decimal)inschrijving.Functie_ID);
                NieuwContact NC = new NieuwContact(currentUser, functie);
                var result = NC.ShowDialog();
                if (result == true)
                {
                    try
                    {
                        int index = grid.SelectedIndex;
                        if (SessieRadioButton.IsChecked == true)
                            LoadInschrijvingen();
                        else
                        {
                            if (AllesRadioButton.IsChecked == true)
                                InschrijvingenFilter_Click(AllesRadioButton, new RoutedEventArgs());
                            else if (SessieRadioButton.IsChecked == true)
                                InschrijvingenFilter_Click(SessieRadioButton, new RoutedEventArgs());
                            else if (GefactureerdRadioButton.IsChecked == true)
                                InschrijvingenFilter_Click(GefactureerdRadioButton, new RoutedEventArgs());
                            else
                                InschrijvingenFilter_Click(NietGefactureerdRadioButton, new RoutedEventArgs());
                        }

                        grid.SelectedIndex = index;


                    }
                    catch (Exception ex)
                    {
                        grid.SelectedIndex = 0;
                    }
                }
            }
        }

        private void ChangeContactButton_Click(object sender, RoutedEventArgs e)
        {
            if (inschrijvingenDataGrid.SelectedIndex > -1)
            {
                ZoekFunctieWindow ZF = new ZoekFunctieWindow();
                if (ZF.ShowDialog() == true)
                {
                    if (ZF.Functie != null)
                    {
                        data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                        int index = inschrijvingenDataGrid.SelectedIndex;
                        inschrijving.Functie_ID = ZF.Functie.ID;
                        data.InschrijvingenService iService = new data.InschrijvingenService();
                        try
                        {
                            iService.SaveInschrijvingCHanges(inschrijving);
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                        if (AllesRadioButton.IsChecked == true)
                            ReloadInschijvingenAsync();
                        else
                        { 
                            LoadInschrijvingen();
                            if (SessieRadioButton.IsChecked == true)
                            {
                                InschrijvingenFilter_Click(SessieRadioButton, new RoutedEventArgs());
                            }
                            else if (GefactureerdRadioButton.IsChecked == true)
                                InschrijvingenFilter_Click(GefactureerdRadioButton, new RoutedEventArgs());
                            else if (NietGefactureerdRadioButton.IsChecked == true)
                                InschrijvingenFilter_Click(NietGefactureerdRadioButton, new RoutedEventArgs());
                        }
                        inschrijvingenDataGrid.SelectedIndex = index;
                        //inschrijvingenDataGrid.ScrollIntoView(inschrijving);
                    }
                }
            }
        }

        private void ChangeFacturatieBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            if (inschrijvingenDataGrid.SelectedIndex > -1)
            {
                ZoekContactWindow ZF = new ZoekContactWindow("Bedrijf");
                if (ZF.ShowDialog() == true)
                {
                    if (ZF.bedrijf != null)
                    {
                        data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                        int index = inschrijvingenDataGrid.SelectedIndex;
                        inschrijving.Facturatie_op_ID = ZF.bedrijf.ID;
                        data.InschrijvingenService iService = new data.InschrijvingenService();
                        try
                        {
                            iService.SaveInschrijvingCHanges(inschrijving);
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                        if (AllesRadioButton.IsChecked == true)
                            ReloadInschijvingenAsync();
                        else
                            LoadInschrijvingen();
                        inschrijvingenDataGrid.SelectedIndex = index;
                        //inschrijvingenDataGrid.ScrollIntoView(inschrijving);
                    }
                }
            }
        }

        private void mailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Aanwezigheden AW = new Aanwezigheden(currentSeminarie.ID, currentSessie.ID, currentUser);
                if (AW.ShowDialog() == true)
                {
                    LoadInschrijvingen();
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het openen van de mailmodulle!!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void naamTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (currentSessie != null)
                ConfocusMessageBox.Show("Let op! Je wijzigt de naam van een bestaande sessie.", ConfocusMessageBox.Kleuren.Rood);

        }

        private void removeBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            if (inschrijvingenDataGrid.SelectedIndex > -1)
            {
                data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                int index = inschrijvingenDataGrid.SelectedIndex;
                data.InschrijvingenService iService = new data.InschrijvingenService();
                try
                {
                    inschrijving.Facturatie_op_ID = null;
                    inschrijving.Gewijzigd = DateTime.Now;
                    inschrijving.Gewijzigd_door = currentUser.Naam;
                    iService.SaveInschrijvingCHanges(inschrijving);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                if (AllesRadioButton.IsChecked == true)
                    ReloadInschijvingenAsync();
                else
                    LoadInschrijvingen();
                inschrijvingenDataGrid.SelectedIndex = index;
            }
        }

        private void seminarie_MarketingDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            data.Seminarie_Marketing kanaal = (data.Seminarie_Marketing)((DataGrid)sender).SelectedItem;
            SetSeminarieMarketingToForm(kanaal);
            KanaalOpslaanButton.IsEnabled = true;
        }

        private void SetSeminarieMarketingToForm(data.Seminarie_Marketing kanaal)
        {
            try
            {
                if (kanaal != null)
                {

                    MarketingKanalenListBox.Text = kanaal.Marketingkanalen == null ? "" : kanaal.Marketingkanalen.Marketing_kanaal;
                    if (kanaal.Opvolgdatum != null)
                    {
                        opvolgdatumDatePicker1.SelectedDate = (DateTime)kanaal.Opvolgdatum;
                    }
                    else
                        opvolgdatumDatePicker1.SelectedDate = null;
                    statusTextBox1.Text = kanaal.Status;
                    OpmerkingTextBox1.Text = kanaal.Opmerking;

                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van het marketingkanaal!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void KanaalOpslaanButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SeminarieComboBox.SelectedIndex > 0)
                {
                    if (MarketingKanalenListBox.SelectedIndex > -1)
                    {
                        data.Seminarie_Marketing marketing = (data.Seminarie_Marketing)seminarie_MarketingDataGrid.SelectedItem;//new data.Seminarie_Marketing();
                        data.Seminarie seminarie = (data.Seminarie)SeminarieComboBox.SelectedItem;
                        data.Marketingkanalen kanaal = (data.Marketingkanalen)MarketingKanalenListBox.SelectedItem;

                        marketing.Seminarie_ID = seminarie.ID;
                        marketing.Marketing_ID = kanaal.ID;
                        marketing.Status = statusTextBox1.Text;
                        marketing.Opmerking = OpmerkingTextBox1.Text;
                        if (opvolgdatumDatePicker1.SelectedDate != null)
                            marketing.Opvolgdatum = opvolgdatumDatePicker1.SelectedDate;

                        data.Seminarie_MarketingServices smService = new data.Seminarie_MarketingServices();
                        smService.UpdateSeminarieMarketing(marketing);//.SaveSeminarieMarketing(marketing);
                        LoadSeminarieMarketingAsync(currentSeminarie);
                        if (currentSeminarie != null)
                            new data.SessieServices().UpdateSeminarieSessiesTimeStamp(currentSeminarie.ID, currentUser);
                        //statusTextBox1.Text = "";
                        //opvolgdatumDatePicker1.SelectedDate = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void MarketingKanalenListBox_DropDownClosed(object sender, EventArgs e)
        {
            opvolgdatumDatePicker1.SelectedDate = null;
            statusTextBox1.Text = "";
            OpmerkingTextBox1.Text = "";
            KanaalOpslaanButton.IsEnabled = false;

        }

        private void actiefCheckBox3_Click(object sender, RoutedEventArgs e)
        {
            if (inschrijvingenDataGrid.SelectedIndex > -1)
            {

            }
        }

        private async void ReloadInschijvingenAsync()
        {
            data.InschrijvingenService iService = new data.InschrijvingenService();
            List<data.Inschrijvingen> inschrijvingen = await Task.Run(() => iService.GetInschrijvingBySeminarieID(currentSeminarie.ID));
            List<data.Inschrijvingen> opdatum = (from i in inschrijvingen orderby i.Sessie.Datum select i).ToList();
            inschrijvingenViewSource.Source = null;
            inschrijvingenViewSource.Source = opdatum;
        }

        private void titelTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBox box = (TextBox)sender;
            box.SelectAll();
        }

        private void BadgesButton_Click(object sender, RoutedEventArgs e)
        {
            LabelWindow LW = new LabelWindow();
            LW.Show();
        }

        private void sessieDetailShowButton_Click(object sender, RoutedEventArgs e)
        {
            if (sessieDetailPanel.Height == new GridLength(400))
            {
                sessieDetailPanel.Height = new GridLength(23);
                sessieDetailShowButton.Content = "Toon details";
            }
            else
            {
                sessieDetailPanel.Height = new GridLength(400);
                sessieDetailShowButton.Content = "Verberg details";
            }
        }

        private void SetSeminarieInfo(data.Seminarie sem)
        {
            List<data.Inschrijvingen> alleInschrijvingen = new data.SeminarieServices().GetAllRegistrations(sem.ID);
            List<data.Inschrijvingen> UniekeInschrijvingen = new List<data.Inschrijvingen>();

            foreach (data.Inschrijvingen inschrijving in alleInschrijvingen)
            {
                bool gevonden = false;
                foreach (data.Inschrijvingen item in UniekeInschrijvingen)
                {
                    if (item.Functies.Contact_ID == inschrijving.Functies.Contact_ID)
                        gevonden = true;
                }
                if (!gevonden)
                    UniekeInschrijvingen.Add(inschrijving);
            }

            SeminarieInschrijvingenTotaalLabel.Content = alleInschrijvingen.Count;
            SeminarieUniekeInschrijvingenLabel.Content = UniekeInschrijvingen.Count;
            SeminarieSessieTotaalLabel.Content = sem.AantalSessies;

            List<data.Sessie_Spreker> sprekers = new data.SprekerServices().GetSprekersBySeminarieID(sem.ID);

            SeminarieSprekerTotaalLabel.Content = sprekers.Count;

            List<data.Sessie_Spreker> uniekeSprekers = new List<data.Sessie_Spreker>();
            foreach (data.Sessie_Spreker spreker in sprekers)
            {
                bool gevonden = false;
                foreach (data.Sessie_Spreker item in uniekeSprekers)
                {
                    if (item.Spreker.Contact_ID == spreker.Spreker.Contact_ID)
                        gevonden = true;
                }
                if (!gevonden)
                    uniekeSprekers.Add(spreker);
            }
            SeminarieUniekeSprekersTotaalLabel.Content = uniekeSprekers.Count;
            List<data.Seminarie_Doelgroep> doelgroepen = new data.Seminarie_DoelgroepServices().GetDoelgroepenBySeminarieID(sem.ID);
            List<DoelgroepAantal> doelgroepaantallen = new List<DoelgroepAantal>();
            foreach (data.Seminarie_Doelgroep doelgroep in doelgroepen)
            {
                DoelgroepAantal aantal = new DoelgroepAantal();
                aantal.Naam = doelgroep.Naam;
                int teller = 0;
                foreach (data.Inschrijvingen inschrijving in UniekeInschrijvingen)
                {
                    if (inschrijving.Functies.Niveau1_ID == doelgroep.Niveau1_ID && (doelgroep.Niveau2_ID == null || inschrijving.Functies.Niveau2_ID == doelgroep.Niveau2_ID) && (doelgroep.Niveau3_ID == null || inschrijving.Functies.Niveau3_ID == doelgroep.Niveau3_ID))
                    {
                        teller++;
                    }
                }
                aantal.Aantal = teller;
                doelgroepaantallen.Add(aantal);
            }

            StringBuilder doelgroepinfo = new StringBuilder();
            if (doelgroepaantallen.Count > 0)
                doelgroepinfo.Append("\t- Per doelgroep -\n\n");
            foreach (DoelgroepAantal item in doelgroepaantallen)
            {
                doelgroepinfo.Append(item.Naam + " : \t" + item.Aantal + "\n\n");
            }

            DoelgroepInschrijvingenTextBlock.Text = doelgroepinfo.ToString();

        }
    }
}
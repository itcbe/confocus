﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        //public event DispatcherUnhandledExceptionEventHandler DispatcherUnhandledException;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            EventManager.RegisterClassHandler(typeof(Window), Window.PreviewKeyUpEvent, new KeyEventHandler(OnWindowKeyUp));
        }

        private void OnWindowKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.H && e.KeyboardDevice.IsKeyDown(Key.LeftCtrl))
            {
                if (sender is MainWindow)
                {
                    MainWindow window = (MainWindow)sender;
                    window.ShowSearchWindow();
                }
                else if (sender is ZoekSprekerScherm)
                {
                    ZoekSprekerScherm window = (ZoekSprekerScherm)sender;
                    window.ShowSearchWindow();
                }
            }
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Fout opgetreden: " + e.Exception.Message);

            e.Handled = true;
        }
    }
}

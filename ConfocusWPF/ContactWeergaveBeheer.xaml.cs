﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ContactWeergaveBeheer.xaml
    /// </summary>
    public partial class ContactWeergaveBeheer : Window
    {
        private List<Weergave> weergaven;
        private List<SessieWeergaven> sessieWeergaven;
        private System.Windows.Data.CollectionViewSource weergaveViewSource;
        private System.Windows.Data.CollectionViewSource sessieWeergavenViewSource;
        private bool IsScroll;
        private Gebruiker currentUser;

        public ContactWeergaveBeheer(Gebruiker currentuser)
        {
            InitializeComponent();
            this.currentUser = currentuser;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "first2":
                    naam = "first";
                    img = firstImage2;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "back2":
                    naam = "back";
                    img = backImage2;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "next2":
                    naam = "next";
                    img = nextImage2;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "last2":
                    naam = "last";
                    img = lastImage2;
                    break;
                //case "new":
                //    img = newImage;
                //    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "first2":
                    naam = "first";
                    img = firstImage2;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "back2":
                    naam = "back";
                    img = backImage2;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "next2":
                    naam = "next";
                    img = nextImage2;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "last2":
                    naam = "last";
                    img = lastImage2;
                    break;
                //case "new":
                //    img = newImage;
                //    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + ".gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void goUpdate()
        {
            if (weergaveDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (weergaveViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (weergaveViewSource.View.CurrentPosition == weergaveDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                weergaveDataGrid.SelectedIndex = 0;
            if (weergaveDataGrid.Items.Count != 0)
                weergaveDataGrid.ScrollIntoView(weergaveDataGrid.SelectedItem);
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            //IsButtonClick = true;
            // datagridChange = true;
            if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
            {
                weergaveViewSource.View.MoveCurrentToFirst();
                goUpdate();

            }
            else
            {
                sessieWeergavenViewSource.View.MoveCurrentToFirst();
                goUpdate2();
            }
            IsScroll = false;
            //IsButtonClick = false;
            //datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            //IsButtonClick = true;
            // datagridChange = true;
            if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
            {
                weergaveViewSource.View.MoveCurrentToPrevious();
                goUpdate();
            }
            else
            {
                sessieWeergavenViewSource.View.MoveCurrentToPrevious();
                goUpdate2();
            }
            IsScroll = false;
            //IsButtonClick = false;
            //datagridChange = false;

        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            //IsButtonClick = true;
            // datagridChange = true;
            if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
            {
                weergaveViewSource.View.MoveCurrentToNext();
                goUpdate();
            }
            else
            {
                sessieWeergavenViewSource.View.MoveCurrentToNext();
                goUpdate2();
            }
            IsScroll = false;
            //IsButtonClick = false;
            //datagridChange = false;

        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            //IsButtonClick = true;
            // datagridChange = true;
            if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
            { 
                weergaveViewSource.View.MoveCurrentToLast();
                goUpdate();
            }
            else
            {
                sessieWeergavenViewSource.View.MoveCurrentToLast();
                goUpdate2();
            }
            IsScroll = false;
            //IsButtonClick = false;
            //datagridChange = false;

        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ZoekTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            box.Text = "";
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekInLijst();
        }

        private void ZoekInLijst()
        {
            string zoektekst = "";
            if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
                zoektekst = ZoekTextBox.Text.ToLower();
            else
                zoektekst = ZoekTextBox2.Text.ToLower();

            if (!string.IsNullOrEmpty(zoektekst))
            {
                if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
                {
                    List<Weergave> result = (from w in weergaven where w.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower()) select w).ToList();
                    weergaveViewSource.Source = result;
                }
                else
                {
                    List<SessieWeergaven> result = (from w in sessieWeergaven where w.Naam.ToLower().Contains(zoektekst) select w).ToList();
                    sessieWeergavenViewSource.Source = result; 
                }
            }
            else
            {
                if (((TabItem)weergavenTabControl.SelectedItem).Header.ToString() == "Contacten")
                    weergaveViewSource.Source = weergaven;
                else
                    sessieWeergavenViewSource.Source = sessieWeergaven;
            }
                
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            weergaveViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("weergaveViewSource")));
            sessieWeergavenViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sessieWeergavenViewSource")));
            LoadWeergaven();
        }

        private void LoadWeergaven()
        {
            WeergaveServices wService = new WeergaveServices();
            weergaven = wService.GetWeergavesByUserId(currentUser.ID);
            weergaveViewSource.Source = weergaven;

            SessieWeergaven_Service swService = new SessieWeergaven_Service();
            sessieWeergaven = swService.GetWeergavesByUserId(currentUser.ID);
            sessieWeergavenViewSource.Source = sessieWeergaven;
        }

        private void verwijderButton_Click(object sender, RoutedEventArgs e)
        {
            Weergave selected = (Weergave)weergaveDataGrid.SelectedItem;
            new WeergaveServices().Delete(selected);
            LoadWeergaven();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }

        #region Sessies

        private void ZoekTextBox2_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekInLijst();
        }

        private void ZoekTextBox2_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            box.Text = "";
        }

        private void verwijderButton2_Click(object sender, RoutedEventArgs e)
        {
            SessieWeergaven selected = (SessieWeergaven)sessieWeergavenDataGrid.SelectedItem;
            new SessieWeergaven_Service().Delete(selected);
            LoadWeergaven();
        }
        private void goUpdate2()
        {
            if (sessieWeergavenDataGrid.Items.Count <= 2)
            {
                back2.IsEnabled = false;
                first2.IsEnabled = false;
                next2.IsEnabled = false;
                last2.IsEnabled = false;
            }
            else if (sessieWeergavenViewSource.View.CurrentPosition == 0)
            {
                back2.IsEnabled = false;
                first2.IsEnabled = false;
                next2.IsEnabled = true;
                last2.IsEnabled = true;
            }
            else if (sessieWeergavenViewSource.View.CurrentPosition == sessieWeergavenDataGrid.Items.Count - 2)
            {
                back2.IsEnabled = true;
                first2.IsEnabled = true;
                next2.IsEnabled = false;
                last2.IsEnabled = false;
            }
            else
            {
                back2.IsEnabled = true;
                first2.IsEnabled = true;
                next2.IsEnabled = true;
                last2.IsEnabled = true;
            }
            if (!IsScroll)
                sessieWeergavenDataGrid.SelectedIndex = 0;
            if (sessieWeergavenDataGrid.Items.Count != 0)
                sessieWeergavenDataGrid.ScrollIntoView(sessieWeergavenDataGrid.SelectedItem);
        }

        #endregion

    }
}

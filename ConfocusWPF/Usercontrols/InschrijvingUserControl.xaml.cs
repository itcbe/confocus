﻿using ConfocusClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using data = ConfocusDBLibrary;

namespace ConfocusWPF.Usercontrols
{
    public delegate void InschrijvingChangedEvenHandler(object sender, ConfocusClassLibrary.InschrijvingEventArgs e);
    //public delegate void BedrijfComboBoxChangedEventHandler(object sender, EventArgs e);
    /// <summary>
    /// Interaction logic for InschrijvingUserControl.xaml
    /// </summary>
    public partial class InschrijvingUserControl : UserControl
    {
        private data.Inschrijvingen _inschrijving = new data.Inschrijvingen();
        private List<data.Functies> _functies = null;
        private data.Gebruiker currentUser;

        public data.Bedrijf FacturatieOp { get; set; }

        public event InschrijvingChangedEvenHandler InschrijvingChanged;
        //public event BedrijfComboBoxChangedEventHandler BedrijfChanged;

        protected virtual void OnChanged(InschrijvingEventArgs e)
        {
            if (InschrijvingChanged != null)
                InschrijvingChanged(this, e);
        }

        //protected virtual void  BedrijfOnchanged(EventArgs e)
        //{
        //    if (BedrijfChanged != null)
        //        BedrijfChanged(this, e);
        //}

        public data.Gebruiker CurrentUser
        {
            set
            {
                currentUser = value;
            }
        }


        public data.Inschrijvingen Inschrijving
        {
            get
            {
                return _inschrijving;
            }
            set
            {
                _inschrijving = value;
            }
        }

        public List<data.Functies> Functies
        {
            get
            {
                return _functies;
            }
            set
            {
                _functies = value;
                SetFunctionAndCompany();
                
            }
        }

        private void SetFunctionAndCompany()
        {
            TitelComboBox.ItemsSource = _functies;
            TitelComboBox.DisplayMemberPath = "Oorspronkelijke_Titel";
            TitelComboBox.SelectedIndex = 0;
            TitelComboBox_DropDownClosed(TitelComboBox, new EventArgs());
            SetBedrijvenToCombo();
        }

        private void SetBedrijvenToCombo()
        {
            if (_functies != null)
            {
                if (_functies.Count > 0)
                {
                    List<data.Bedrijf> bedrijven = new List<data.Bedrijf>();
                    foreach (data.Functies functie in _functies)
                    {
                        data.Functies fullfunctie = new data.FunctieServices().GetFunctieByID(functie.ID);
                        if (fullfunctie.Bedrijf != null)
                        {
                            if (!bedrijven.Contains(fullfunctie.Bedrijf))
                                bedrijven.Add(fullfunctie.Bedrijf);
                        }
                    }

                    List<data.Bedrijf> sortedBedrijven = bedrijven.OrderBy(o => o.Naam).ToList();
                    BedrijfComboBox.ItemsSource = sortedBedrijven;
                    BedrijfComboBox.DisplayMemberPath = "Naam";
                    BedrijfComboBox.SelectedIndex = 0;
                }
            }
        }

        public InschrijvingUserControl(bool border)
        {
            InitializeComponent();
            SetComboBoxItems();
            if (border)
            {
                MainBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                MainBorder.BorderThickness = new Thickness(1);
            }
        }

        private void SetComboBoxItems()
        {
            //data.Attesttype_Services aService = new data.Attesttype_Services();
            //List<data.AttestType> attesten = aService.FindActive();
            //AttesttypeComboBox.ItemsSource = attesten;
            //AttesttypeComboBox.DisplayMemberPath = "Naam";

            //data.BedrijfServices bService = new data.BedrijfServices();
            //List<data.Bedrijf> bedrijven = bService.FindActive();
            //facturatieOpComboBox.ItemsSource = bedrijven;
            //facturatieOpComboBox.DisplayMemberPath = "Naam";

            data.DagType_Services dService = new data.DagType_Services();
            List<data.DagType> dagtypes = dService.FindActive();
            dagtypeComboBox.ItemsSource = dagtypes;
            dagtypeComboBox.DisplayMemberPath = "Naam";
            dagtypeComboBox.Text = "Volledige sessie";
        }

        public void BindInschrijving()
        {
            if (_inschrijving != null)
            {
                ViaComboBox.Text = _inschrijving.Via;
                StatusComboBox.Text = _inschrijving.Status;
                CCEmailTextBox.Text = _inschrijving.CC_Email;
                NotitieComboBox.Text = _inschrijving.Notitie;
                dagtypeComboBox.Text = _inschrijving.Dagtype;
                AantalTextBox.Text = _inschrijving.Aantal.ToString();
                //facturatieOpComboBox.SelectedValue = _inschrijving.Facturatie_op_ID.ToString();

            }
        }

        public void CreateInschrijving()
        {
            int aantal = 0;
            _inschrijving.Via = ViaComboBox.Text;
            _inschrijving.Status = StatusComboBox.Text;
            _inschrijving.CC_Email = CCEmailTextBox.Text;
            _inschrijving.Notitie = NotitieComboBox.Text;
            _inschrijving.Dagtype = dagtypeComboBox.Text;
            if (int.TryParse(AantalTextBox.Text, out aantal))
            {
                _inschrijving.Aantal = aantal;
            }
            //else
            //    _inschrijving.Aantal = 1;
            //_inschrijving.Facturatie_op_ID = ((data.Bedrijf)facturatieOpComboBox.SelectedItem).ID;
        }

        private void TitelComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            SetControls(box);
            InschrijvingEventArgs iea = new InschrijvingEventArgs(_inschrijving);
            OnChanged(iea);

        }

        private void SetControls(ComboBox box)
        {
            if (box.SelectedIndex > -1)
            {
                data.Functies functie = (data.Functies)box.SelectedItem;
                data.FunctieServices fService = new data.FunctieServices();
                data.Functies fullFunctie = fService.GetFunctieByID(functie.ID);

                ErkenningsnummerTextBox.Content = fullFunctie.Erkenningsnummer;
                if (fullFunctie.AttestType1 != null)
                {
                    AttesttypeComboBox.Text = fullFunctie.AttestType1.Naam;
                }
                
                _inschrijving.Functie_ID = fullFunctie.ID;
                _inschrijving.Functies = fullFunctie;
                InschrijvingEventArgs iea = new InschrijvingEventArgs(_inschrijving);
                OnChanged(iea);
                OpenContactButton.IsEnabled = true;
            }
            else
                OpenContactButton.IsEnabled = false;

        }

        private void BedrijfComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            SetFunctions(box);
        }

        private void SetFunctions(ComboBox box)
        {
            if (box.SelectedIndex > -1)
            {
                data.Bedrijf bedrijf = (data.Bedrijf)box.SelectedItem;

                var functies = _functies.Where(f => f.Bedrijf_ID == bedrijf.ID && f.Actief == true).OrderBy(f => f.Oorspronkelijke_Titel).ToList();
                TitelComboBox.ItemsSource = functies;
                TitelComboBox.DisplayMemberPath = "Oorspronkelijke_Titel";
            }
        }

        private void BedrijfComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            SetFunctions(box);
        }

        private void TitelComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.LineFeed)
            {
                ComboBox box = (ComboBox)sender;
                SetControls(box);
                InschrijvingEventArgs iea = new InschrijvingEventArgs(_inschrijving);
                OnChanged(iea);
            }
        }

        private void AantalTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (e.Key == Key.Enter || e.Key == Key.Tab || e.Key == Key.LineFeed)
            {
                int getal;
                if (!int.TryParse(box.Text, out getal))
                {
                    ConfocusMessageBox.Show("Aantal is geen getal!", ConfocusMessageBox.Kleuren.Rood);
                    box.Text = "";
                }
            }
        }

        private void OpenContactButton_Click(object sender, RoutedEventArgs e)
        {
            if (TitelComboBox.SelectedIndex > -1)
            {
                data.Functies functie = (data.Functies)TitelComboBox.SelectedItem;
                NieuwContact nc = new NieuwContact(currentUser, functie);
                if (nc.ShowDialog() == true)
                {
                    int i = 0;
                    foreach (data.Functies func in _functies)
                    {
                        if (func.ID == nc.SelectedFunctie.ID)
                            break;
                        else
                            i++;
                    }
                    _functies.RemoveAt(i);
                    _functies.Insert(i, nc.SelectedFunctie);
                    TitelComboBox.ItemsSource = _functies;
                    TitelComboBox.DisplayMemberPath = "Oorspronkelijke_Titel";
                    TitelComboBox.SelectedIndex = i;
                    //_inschrijving.Functies = nc.SelectedFunctie;
                    SetControls(TitelComboBox);
                    InschrijvingEventArgs iea = new InschrijvingEventArgs(_inschrijving);
                    OnChanged(iea);
                }
            }
        }

        public void ClearForm()
        {
            BedrijfComboBox.SelectedIndex = -1;
            TitelComboBox.SelectedIndex = -1;
            AttesttypeComboBox.Text = "";
            ErkenningsnummerTextBox.Content = "";
            ViaComboBox.SelectedIndex = -1;
            StatusComboBox.Text = "Inschrijving";
            CCEmailTextBox.Text = "";
            AantalTextBox.Text = "";
            facturatieOpComboBox.Text = "";
            dagtypeComboBox.Text = "Volledige sessie";
            NotitieComboBox.Text = "";
            OpenContactButton.IsEnabled = false;
        }

        private void GetFacturatieBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZC = new ZoekContactWindow("Bedrijf");
            if (ZC.ShowDialog() == true)
            {
                FacturatieOp = ZC.bedrijf;
                facturatieOpComboBox.Text = FacturatieOp.NaamEnAdres;
                _inschrijving.Facturatie_op_ID = FacturatieOp.ID;
            }
        }
    }
}

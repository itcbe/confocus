﻿using ConfocusDBLibrary;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ConfocusWPF.Usercontrols
{
    /// <summary>
    /// Interaction logic for WebInschrijvingUserControl.xaml
    /// </summary>
    public partial class WebInschrijvingUserControl : UserControl
    {
        private string _status = "";
        private string _notitie = "";
        private string _aantal = "";
        private string _ccemail = "";
        private string _dagtype = "";

        public string Status
        {
            get
            {
                if (_status == statusComboBox.Text)
                    return _status;
                else
                    return statusComboBox.Text;
            }
            set
            {
                _status = value;
                statusComboBox.Text = value;
            }
        }

        public string Notitie
        {
            get { return _notitie; }
            set
            {
                _notitie = value;
                notitieTextBox.Text = value;
            }
        }

        //public string Aantal
        //{
        //    get { return _aantal; }
        //    set
        //    {
        //        _aantal = value;
        //        aantalComboBox.Text = value;
        //    }
        //}


        public string CCEmail
        {
            get { return _ccemail; }
            set
            {
                _ccemail = value;
                ccEmailTextBox.Text = value;
            }
        }

        public string Dagtype
        {
            get
            {
                if (_dagtype == dagtypeComboBox.Text)
                    return _dagtype;
                else
                    return dagtypeComboBox.Text;
            }
            set
            {
                _dagtype = value;
                dagtypeComboBox.Text = value;
            }
        }

        public WebInschrijvingUserControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DagType_Services dService = new DagType_Services();
            List<DagType> dagtypes = dService.FindActive();
            dagtypeComboBox.ItemsSource = dagtypes;
            dagtypeComboBox.DisplayMemberPath = "Naam";
            //dagtypeComboBox.Text = "Volledige sessie";
        }

        private void ComboBox_DropDownClosed(object sender, System.EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            switch (box.Name)
            {
                case "statusComboBox":
                    _status = box.Text;
                    break;
                case "aantalComboBox":
                    _aantal = box.Text;
                    break;
                case "dagtypeComboBox":
                    _dagtype = box.Text;
                    break;
                default:
                    break;
            }
        }

        private void TextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            switch (box.Name)
            {
                case "ccEmailTextBox":
                    _ccemail = box.Text;
                    break;
                case "notitieTextBox":
                    _notitie = box.Text;
                    break;
                default:
                    break;
            }
        }

        private void dagtypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _dagtype = dagtypeComboBox.Text;
        }

        private void ComboBox_DropDownClosed(object sender, SelectionChangedEventArgs e)
        {
            _status = statusComboBox.Text;
        }

        public void Clear()
        {
            notitieTextBox.Text = "";
            _notitie = "";
            statusComboBox.SelectedIndex = 1;
            _status = "";
            ccEmailTextBox.Text = "";
            _ccemail = "";
            dagtypeComboBox.SelectedIndex = 1;
            _dagtype = "";
            aantalComboBox.Text = "";
            _aantal = "";
        }

        //private void aantalComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    _aantal = aantalComboBox.Text;
        //}
    }
}

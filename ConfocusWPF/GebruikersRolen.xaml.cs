﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for GebruikersRolen.xaml
    /// </summary>
    public partial class GebruikersRolen : Window
    {
        private CollectionViewSource gebruikerRolViewSource;
        private List<UserRole> rollen = new List<UserRole>();
        private List<UserRole> gevondenRollen = new List<UserRole>();
        private Gebruiker currentUser;
        private Boolean IsScroll = false, IsButtonClick = false;
        public GebruikersRolen(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekInLijst();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerRolViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerRolViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerRolViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            gebruikerRolViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void goUpdate()
        {
            if (gebruikersrolDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (gebruikerRolViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (gebruikerRolViewSource.View.CurrentPosition == gebruikersrolDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                gebruikersrolDataGrid.SelectedIndex = 0;
            if (gebruikersrolDataGrid.Items.Count != 0)
                gebruikersrolDataGrid.ScrollIntoView(gebruikersrolDataGrid.SelectedItem);
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NieuweRol NR = new NieuweRol(currentUser);
                if (NR.ShowDialog() == true)
                {
                    rollen = new UserRole_Services().FindActive();
                    gebruikerRolViewSource.Source = rollen;
                    gebruikersrolDataGrid.SelectedItem = NR.Rol;
                    goUpdate();
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserRole role = (UserRole)gebruikersrolDataGrid.SelectedItem;
                role.Rol = RoltextBox.Text;
                role.Actief = ActiefCheckBox.IsChecked == true;
                role.Gewijzigd = DateTime.Now;
                role.Gewijzigd_door = currentUser.Naam;
                new UserRole_Services().SaveRoleChanges(role);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van de rol!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            gebruikerRolViewSource = ((CollectionViewSource)(this.FindResource("gebruikerRolViewSource")));
            rollen = new UserRole_Services().FindActive();
            gebruikerRolViewSource.Source = rollen;
            goUpdate(); 
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ZoekInLijst()
        {
            gevondenRollen.Clear();

            gevondenRollen = (from r in rollen
                                  where r.Rol.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                  select r).ToList();
            if (gevondenRollen.Count > 0)
                gebruikerRolViewSource.Source = gevondenRollen;
            else
                gebruikerRolViewSource.Source = rollen;
        }
    }
}

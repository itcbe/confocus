﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ADOClassLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for FlexmailActieWindows.xaml
    /// </summary>
    public partial class FlexmailActieWindows : Window
    {
        private List<Functies> functies;
        private List<FilterFunctie> filterFuncties;

        public FlexmailActieWindows(List<Functies> func)
        {
            InitializeComponent();
            functies = func;
        }

        public FlexmailActieWindows(List<FilterFunctie> func)
        {
            InitializeComponent();
            this.filterFuncties = func;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //Laden van de flexmail opties voor het formulier.
                Flexmail.Flexmail flexmail = new Flexmail.Flexmail();
                List<Flexmail.Categorie> categorieen = flexmail.GetCategories();
                categorieComboBox.ItemsSource = categorieen;
                categorieComboBox.DisplayMemberPath = "Naam";

                List<Flexmail.FlexmailLijst> lijsten = flexmail.GetLijsten();
                bestaandeLijstComboBox.ItemsSource = lijsten;
                bestaandeLijstComboBox.DisplayMemberPath = "Naam";
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
            System.Windows.Data.CollectionViewSource functiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functiesViewSource")));
            if (functies != null)
            {
                functiesViewSource.Source = functies;
                aantalLabel.Content = "aantal contacten: " + functies.Count;
            }
            else if (filterFuncties != null)
            {
                functiesViewSource.Source = filterFuncties;
                aantalLabel.Content = "aantal contacten: " + filterFuncties.Count;
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void sentListButton_Click(object sender, RoutedEventArgs e)
        {
            string name = listnameTextBox.Text;
            if (categorieComboBox.SelectedIndex > -1)
            {
                int categorieId = ((Flexmail.Categorie)categorieComboBox.SelectedItem).ID;
                //if (!string.IsNullOrEmpty(name))
                //{
                Flexmail.Flexmail flexmail = new Flexmail.Flexmail();
                try
                    {
                        int listid = 0;
                    if (!string.IsNullOrEmpty(name))
                    {
                        listid = flexmail.CreateMailingList(name, categorieId);
                    }
                    else if (bestaandeLijstComboBox.SelectedIndex > -1)
                    {
                        listid = ((Flexmail.FlexmailLijst)bestaandeLijstComboBox.SelectedItem).ID;
                        //flexmail.MaakLijstLeeg(listid);
                    }
                    else
                    { 
                        throw new Exception("Geen lijst gekozen!");
                    }

                    if (listid != 0)
                    { 
                        if (functies != null)
                        {
                            flexmail.ImportMailAddresses(functies, listid);
                        }
                        else if (filterFuncties != null)
                        {
                            flexmail.ImportMailAdressesNew(filterFuncties, listid);
                        }
                        ConfocusMessageBox.Show("Lijst geëxporteerd.", ConfocusMessageBox.Kleuren.Blauw);
                    }
                    else
                        ConfocusMessageBox.Show("Lijst niet aangemaakt.", ConfocusMessageBox.Kleuren.Rood);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                //}
                //else
                //    ConfocusMessageBox.Show("Geen lijstnaam ingegeven!", ConfocusMessageBox.Kleuren.Rood);
            }
            else
                ConfocusMessageBox.Show("Geen categorie gekozen!", ConfocusMessageBox.Kleuren.Rood);
        }

        private void functiesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            string name = listnameTextBox.Text;
            Flexmail.Flexmail flexmail = new Flexmail.Flexmail();
            try
            {
                int listid = 0;
                if (bestaandeLijstComboBox.SelectedIndex > -1)
                {
                    listid = ((Flexmail.FlexmailLijst)bestaandeLijstComboBox.SelectedItem).ID;
                    //flexmail.MaakLijstLeeg(listid);
                }
                else
                {
                    throw new Exception("Geen lijst gekozen!");
                }

                if (listid != 0)
                {
                    if (functies != null)
                    {
                        flexmail.UpdateEmailAddresses(functies, listid);
                    }
                    else if (filterFuncties != null)
                    {
                        flexmail.UpdateEmailAddressesNew(filterFuncties, listid);
                    }
                    ConfocusMessageBox.Show("Lijst bijgewerkt.", ConfocusMessageBox.Kleuren.Blauw);
                }
                else
                    ConfocusMessageBox.Show("Geen lijst gekozen!", ConfocusMessageBox.Kleuren.Rood);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweConcurentie.xaml
    /// </summary>
    public partial class NieuweConcurentie : Window
    {
        private Gebruiker currentUser;
        private Concurentie current;
        public NieuweConcurentie(Gebruiker user, DateTime datum)
        {
            InitializeComponent();
            this.currentUser = user;
            datumDatePicker.SelectedDate = datum;
        }

        public NieuweConcurentie(Gebruiker user, decimal id)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.currentUser = user;
            SetConcurentie(id);
        }

        private void SetConcurentie(decimal id)
        {
            actiefCheckBox.Visibility = System.Windows.Visibility.Visible;
            actieflabel.Visibility = System.Windows.Visibility.Visible;
            ConcurentieServices cService = new ConcurentieServices();
            current = cService.GetByID(id);
            datumDatePicker.Text = current.Datum.ToShortDateString();
            omschrijvingTextBox.Text = current.Omschrijving;
            actiefCheckBox.IsChecked = current.Actief == true;
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid())
            {
                if (current != null)
                {
                    try
                    {
                        current.Datum = (DateTime)datumDatePicker.SelectedDate;
                        current.Omschrijving = omschrijvingTextBox.Text;
                        current.Actief = actiefCheckBox.IsChecked == true;
                        current.Gewijzigd = DateTime.Now;
                        current.Gewijzigd_door = currentUser.Naam;
                    }
                    catch (Exception ex)
                    {                      
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                else
                {
                    try
                    {
                        Concurentie concu = new Concurentie();
                        concu.Datum = (DateTime)datumDatePicker.SelectedDate;
                        concu.Omschrijving = omschrijvingTextBox.Text;
                        concu.Aangemaakt = DateTime.Now;
                        concu.Aangemaakt_door = currentUser.Naam;
                        concu.Actief = true;
                        ConcurentieServices cService = new ConcurentieServices();
                        cService.SaveConcurentie(concu);
                        DialogResult = true;
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }

                }
            }
        }

        private bool IsValid()
        {
            bool valid = true;
            String errortext = "";
            if (datumDatePicker.SelectedDate == null)
            {
                valid = false;
                errortext += "Geen datum gekozen!\n";
            }

            if (String.IsNullOrEmpty(omschrijvingTextBox.Text))
            {
                valid = false;
                errortext += "Geen omschrijving ingegeven!";
            }

            if (!valid)
            {
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            }

            return valid;
        }
    }
}

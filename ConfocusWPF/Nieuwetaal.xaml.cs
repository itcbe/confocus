﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for Nieuwetaal.xaml
    /// </summary>
    public partial class Nieuwetaal : Window
    {
        private Gebruiker currentUser;

        public Nieuwetaal(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Taal taal = GetTaalFromForm();
            if (taal != null)
            {
                TaalServices service = new TaalServices();
                service.SaveTaal(taal);
                DialogResult = true;
            }
        }

        private Taal GetTaalFromForm()
        {
            if (IsFormValid())
            {
                Taal t = new Taal();
                t.Naam = naamTextBox.Text;
                t.Code = codeTextBox.Text;
                return t;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "Geen taal ingegeven!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);
            if (codeTextBox.Text == "")
            {
                errorMessage += "Geen taalcode ingegeven!\n";
                Helper.SetTextBoxInError(codeTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(codeTextBox, normalTemplate);
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }
    }
}

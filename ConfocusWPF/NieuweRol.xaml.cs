﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweRol.xaml
    /// </summary>
    public partial class NieuweRol : Window
    {
        private Gebruiker currentUser;
        public UserRole Rol = null;
        public NieuweRol(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(RoltextBox.Text))
                    throw new Exception("Geen rol ingegeven!");

                UserRole role = new UserRole();
                role.Rol = RoltextBox.Text;
                role.Actief = true;
                role.Aangemaakt = DateTime.Now;
                role.Aangemaakt_door = currentUser.Naam;
                role.Gewijzigd = DateTime.Now;
                role.Gewijzigd_door = currentUser.Naam;

                Rol = new UserRole_Services().Save(role);
                DialogResult = true;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }
    }
}

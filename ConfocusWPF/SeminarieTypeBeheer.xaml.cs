﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SeminarieTypeBeheer.xaml
    /// </summary>
    public partial class SeminarieTypeBeheer : Window
    {
        private System.Windows.Data.CollectionViewSource seminarietypeViewSource;
        private List<Seminarietype> seminarietypes = new List<Seminarietype>();
        private List<Seminarietype> gevondenSeminarietypes = new List<Seminarietype>();
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private Gebruiker CurrentUser;
        public SeminarieTypeBeheer(Gebruiker user)
        {
            InitializeComponent();
            CurrentUser = user;
            this.Title = "Seminarietype beheer : Ingelogd als " + CurrentUser.Naam;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            seminarietypeViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            seminarietypeViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            seminarietypeViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            seminarietypeViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void GoUpdate()
        {
            if (seminarietypeDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (seminarietypeViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (seminarietypeViewSource.View.CurrentPosition == seminarietypeDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                seminarietypeDataGrid.SelectedIndex = 0;
            if (seminarietypeDataGrid.Items.Count != 0)
                if (seminarietypeDataGrid.SelectedItem != null)
                    seminarietypeDataGrid.ScrollIntoView(seminarietypeDataGrid.SelectedItem);
        }


        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwSeminarieType TW = new NieuwSeminarieType(CurrentUser);
            if (TW.ShowDialog() == true)
            {
                ReloadSeminarieTypes();
            }
        }

        private void ReloadSeminarieTypes()
        {
            int index = seminarietypeDataGrid.SelectedIndex;
            LoadTypes();
            seminarietypeDataGrid.SelectedIndex = index;
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekTypes();
        }

        private void ZoekTypes()
        {
            if (ZoekTextBox.Text != "")
            {
                gevondenSeminarietypes.Clear();
                gevondenSeminarietypes = (from r in seminarietypes
                                          where r.Type.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                  select r).ToList();

                seminarietypeViewSource.Source = gevondenSeminarietypes;
            }
            else
                seminarietypeViewSource.Source = seminarietypes;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();
        }

        private void SaveWijzigingen()
        {
            try
            {
                if (!string.IsNullOrEmpty(typeTextBox.Text))
                {
                    Seminarietype type = (Seminarietype)seminarietypeDataGrid.SelectedItem;
                    type.Type = typeTextBox.Text;
                    type.Actief = actiefCheckBox.IsChecked == true;
                    type.Gewijzigd = DateTime.Now;
                    type.Gewijzigd_door = CurrentUser.Naam;

                    new SeminarieTypeService().Update(type);
                }
                else
                    ConfocusMessageBox.Show("Het seminarietype veld mag niet leeg zijn!", ConfocusMessageBox.Kleuren.Rood);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het seminarietype!!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void typeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab || e.Key == Key.Return)
                SaveWijzigingen();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            seminarietypeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("seminarietypeViewSource")));
            LoadTypes();
        }

        private void LoadTypes()
        {
            seminarietypes = new SeminarieTypeService().GetAll();
            seminarietypeViewSource.Source = seminarietypes;
        }
    }
}

﻿using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweSpreker.xaml
    /// </summary>
    public partial class NieuweSpreker : Window
    {
        private Gebruiker currentUser;

        public NieuweSpreker(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            BedrijfServices bedrijfService = new BedrijfServices();
            bedrijfComboBox.ItemsSource = bedrijfService.FindAll();
            bedrijfComboBox.DisplayMemberPath = "Naam";

            ContactServices contactService = new ContactServices();
            contactComboBox.ItemsSource = contactService.FindAll();
            contactComboBox.DisplayMemberPath = "VolledigeNaam";

            Niveau1Services n1Service = new Niveau1Services();
            niveau1ComboBox.ItemsSource = n1Service.FindAll();
            niveau1ComboBox.DisplayMemberPath = "Naam";

            ErkenningServices erService = new ErkenningServices();
            erkenningComboBox.ItemsSource = erService.FindAll();
            erkenningComboBox.DisplayMemberPath = "Naam";
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Spreker sp = GetSprekerFromForm();
            if (sp != null)
            {
                SprekerServices service = new SprekerServices();
                service.SaveSpreker(sp);
                DialogResult = true;
            }
        }

        private Spreker GetSprekerFromForm()
        {
            if (IsFormValid())
            {
                Spreker sp = new Spreker();
                sp.Bedrijf_ID = ((Bedrijf)bedrijfComboBox.SelectedItem).ID;
                sp.Contact_ID = ((Contact)contactComboBox.SelectedItem).ID;
                sp.Tarief = tariefTextBox.Text;
                sp.Onderwerp = onderwerpTextBox.Text;
                if (waarderingTextBox.Text != "")
                    sp.Waardering = Int32.Parse(waarderingTextBox.Text);
                if (niveau1ComboBox.SelectedIndex > -1)
                    sp.Niveau1_ID = ((Niveau1)niveau1ComboBox.SelectedItem).ID;
                if (niveau2ComboBox.SelectedIndex > -1)
                    sp.Niveau2_ID = ((Niveau2)niveau2ComboBox.SelectedItem).ID;
                if (niveau3ComboBox.SelectedIndex > -1)
                    sp.Niveau3_ID = ((Niveau3)niveau3ComboBox.SelectedItem).ID;
                if (erkenningComboBox.SelectedIndex > -1)
                    sp.Erkenning_ID = ((Erkenning)erkenningComboBox.SelectedItem).ID;
                sp.CV = cVTextBox.Text;
                sp.Actief = actiefCheckBox.IsChecked == true;
                sp.Aangemaakt = DateTime.Now;
                sp.Aangemaakt_door = currentUser.Naam;

                return sp;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            if (contactComboBox.Text == "")
            {
                errorMessage += "Er is geen contact gekozen!\n";
                valid = false;
            }
            if (tariefTextBox.Text != "")
            {
                String strTarief = tariefTextBox.Text;
                if (strTarief.Contains('.'))
                    strTarief = strTarief.Replace('.', ',');
                Decimal tarief = 0;
                if (!decimal.TryParse(strTarief, out tarief))
                {
                    errorMessage += "Bij tarief is geen getal ingevuld!\n";
                    valid = false;
                }
            }
            if (waarderingTextBox.Text != "")
            {
                int waardering = 0;
                if (!Int32.TryParse(waarderingTextBox.Text, out waardering))
                {
                    errorMessage += "Bij waardering is geen getal ingevuld!\n";
                    valid = false;
                }
            }

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > -1)
            {
                Decimal N1ID = ((Niveau1)(combo.SelectedItem)).ID;
                Niveau2Services n2Service = new Niveau2Services();
                List<Niveau2> N2Lijst = n2Service.GetSelected(N1ID);
                niveau2ComboBox.ItemsSource = N2Lijst;
                niveau2ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > -1)
            {
                Decimal N2ID = ((Niveau2)(combo.SelectedItem)).ID;
                Niveau3Services n3Service = new Niveau3Services();
                List<Niveau3> N3Lijst = n3Service.GetSelected(N2ID);
                niveau3ComboBox.ItemsSource = N3Lijst;
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void SelectCVButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog OpenCV = new OpenFileDialog();
            if (OpenCV.ShowDialog() == true)
            {
                cVTextBox.Text = OpenCV.FileName;
            }
        }
    }
}

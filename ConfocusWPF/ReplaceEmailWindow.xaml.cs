﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ReplaceEmailWindow.xaml
    /// </summary>
    public partial class ReplaceEmailWindow : Window
    {
        private System.Windows.Data.CollectionViewSource functiesViewSource;
        private string _toReplace = "";
        private string _toSearch = "";
        private Gebruiker currentUser;
        private List<Functies> verwerkteFuncties = new List<Functies>();
        
        public ReplaceEmailWindow(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            functiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("functiesViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // functiesViewSource.Source = [generic data source]
            ZoekTextBox.Focus();
        }

        private void functiesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            emailTextBox1.Focus();
            SetAantalLabel();
        }

        private void SetAantalLabel()
        {
            aantalLabel.Content = (functiesDataGrid.SelectedIndex + 1) + " van " + (functiesDataGrid.Items.Count);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            verwerkteFuncties.Clear();
            FilterContacten(0);
        }

        private void FilterContacten(int index)
        {
            if (!string.IsNullOrEmpty(ZoekTextBox.Text))
            {
                _toReplace = VervangTextBox.Text;
                _toSearch = ZoekTextBox.Text;
                FunctieServices fService = new FunctieServices();
                List<Functies> functies = fService.GetReplaceEmail(_toSearch);
                List<Functies> gefilterde = new List<Functies>();
                foreach (Functies func in functies)
                {
                    if (!verwerkteFuncties.Contains(func))
                        gefilterde.Add(func);
                }
                functiesViewSource.Source = gefilterde;
                if (index > 0)
                {
                    functiesDataGrid.SelectedIndex = index;
                    functiesDataGrid.ScrollIntoView(functiesDataGrid.SelectedItem);
                }

                emailTextBox1.Focus();
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                if (emailTextBox1.IsFocused == true)
                {
                    emailTextBox.Focus();
                }
                else if (emailTextBox.IsFocused == true)
                {
                    //int index = functiesDataGrid.SelectedIndex;
                    //if (index < functiesDataGrid.Items.Count)
                    //{
                    //functiesDataGrid.SelectedIndex = ++index;
                    verwerkteFuncties.Add((Functies)functiesDataGrid.SelectedItem);
                    FilterContacten(functiesDataGrid.SelectedIndex);
                    //}
                        
                    emailTextBox1.Focus();
                }
            }
            else if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                e.Handled = true;
                Functies functie = (Functies)functiesDataGrid.SelectedItem;
                if (emailTextBox1.IsFocused == true)
                {
                    string replaced = ReplaceBedrijfsEmail(functie, _toReplace);
                    emailTextBox1.Text = replaced;
                    emailTextBox.Focus();
                }
                else if (emailTextBox.IsFocused == true)
                {
                    ReplaceFunctionEmail(functie, _toReplace);
                    //int index = functiesDataGrid.SelectedIndex;
                    //if (index < functiesDataGrid.Items.Count)
                    //{
                        //functiesDataGrid.SelectedIndex = ++index;
                        FilterContacten(functiesDataGrid.SelectedIndex);
                        //functiesDataGrid.ScrollIntoView(functiesDataGrid.SelectedItem);
                    //}
                        
                    emailTextBox1.Focus();
                }
            }
        }

        private string ReplaceFunctionEmail(Functies functie, string _toReplace)
        {
            if (functie != null)
            {
                FunctieServices fService = new FunctieServices();
                Functies currentFunctie = fService.GetFunctieByID(functie.ID);
                if (currentFunctie != null)
                {
                    if (!string.IsNullOrEmpty(currentFunctie.Email))
                    {
                        string email = currentFunctie.Email.ToLower();
                        string replaced = "";
                        if (!string.IsNullOrEmpty(email))
                        {
                            replaced = email.Replace(_toSearch.ToLower(), _toReplace.ToLower());
                        }
                        functie.Email = replaced;
                        functie.Gewijzigd = DateTime.Now;
                        functie.Gewijzigd_door = currentUser.Naam;
                        fService.SaveFunctieWijzigingen(functie);
                        return replaced;
                    }
                    else
                        return "";
                }
                else
                    return "";
            }
            else
                return "";
        }

        private string ReplaceBedrijfsEmail(Functies functie, string _toReplace)
        {
            if (functie != null)
            {
                BedrijfServices bService = new BedrijfServices();
                Bedrijf currentbedrijf = bService.GetBedrijfByID(functie.Bedrijf_ID);
                if (currentbedrijf != null)
                {
                    if (!String.IsNullOrEmpty(currentbedrijf.Email))
                    {
                        string email = currentbedrijf.Email.ToLower(); ;
                        string replaced = "";
                        if (!string.IsNullOrEmpty(email))
                        {
                            replaced = email.Replace(_toSearch.ToLower(), _toReplace.ToLower());
                        }
                        Bedrijf bedrijf = functie.Bedrijf;
                        bedrijf.Email = replaced;
                        bedrijf.Gewijzigd = DateTime.Now;
                        bedrijf.Gewijzigd_door = currentUser.Naam;
                        bService.SaveBedrijfChanges(bedrijf);
                        return replaced;
                    }
                    else
                        return "";
                }
                else
                    return "";
            }
            else
                return "";
        }

        private void VervangTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _toReplace = VervangTextBox.Text;
        }

        private void ZoekTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _toSearch = ZoekTextBox.Text;
        }

        private void ChangeAllButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Functies> functies = new List<Functies>();
                foreach (Functies func in functiesDataGrid.Items)
                {
                    ReplaceBedrijfsEmail(func, _toReplace);
                    ReplaceFunctionEmail(func, _toReplace);
                }
                functiesViewSource.Source = new List<Functies>();
                ConfocusMessageBox.Show("Alle functies zijn vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ChangeAllCompanyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Functies> functies = new List<Functies>();
                foreach (Functies func in functiesDataGrid.Items)
                {
                    ReplaceBedrijfsEmail(func, _toReplace);
                }
                functiesViewSource.Source = new List<Functies>();
                ConfocusMessageBox.Show("Alle functies zijn vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ChangeAllFunctionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Functies> functies = new List<Functies>();
                foreach (Functies func in functiesDataGrid.Items)
                {
                    //ReplaceBedrijfsEmail(func, _toReplace);
                    ReplaceFunctionEmail(func, _toReplace);
                }
                functiesViewSource.Source = new List<Functies>();
                ConfocusMessageBox.Show("Alle functies zijn vervangen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void switchButton_Click(object sender, RoutedEventArgs e)
        {
            string replace = _toReplace;
            string zoek = _toSearch;
            ZoekTextBox.Text = replace;
            VervangTextBox.Text = zoek;
        }
    }
}

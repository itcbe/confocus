﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ZoekEmailInvoerWindow.xaml
    /// </summary>
    public partial class ZoekEmailInvoerWindow : Window
    {
        public string Emailadres { get; set; }
        public ZoekEmailInvoerWindow()
        {
            InitializeComponent();
        }

        private void EmailtextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                Emailadres = EmailtextBox.Text;
                DialogResult = true;
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            EmailtextBox.Focus();
        }
    }
}

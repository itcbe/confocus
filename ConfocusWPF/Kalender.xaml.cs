﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System.Globalization;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for Kalender.xaml
    /// </summary>
    public partial class Kalender : Window
    {
        private DateTime CurrentDate { get; set; }
        private Gebruiker currentUser;
        private KalenderDagDetaills dagDetailsWindow;
        public Kalender(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void ClearKalender()
        {
            for (int i = 1; i <= 42; i++)
            {
                String control1naam = "Dag" + i;
                String control3naam = "DagBlock" + i;
                String control4naam = "ConcuBlock" + i;
                Label daglabel = (Label)dagenGrid.FindName(control1naam);
                daglabel.Content = "";
                TextBlock block = (TextBlock)dagenGrid.FindName(control3naam);
                block.Text = "";
                block.ToolTip = "";
                block.Background = new SolidColorBrush(Color.FromArgb(0,255, 255, 255));
                TextBlock concuBlock = (TextBlock)dagenGrid.FindName(control4naam);
                concuBlock.Text = "";
                concuBlock.ToolTip = "";
                concuBlock.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
                block.MouseEnter -= DagBlock_MouseEnter;
                block.MouseLeave -= DagBlock_MouseLeave;
                concuBlock.MouseEnter -= ConcuBlock_MouseEnter;
                concuBlock.MouseLeave -= ConcuBlock_MouseLeave;
                block.Tag = null;
                concuBlock.Tag = null;
            }
        }

        public void SetKalender(DateTime datum)
        {
            try
            {
                ClearKalender();
                DateTime vandaag = DateTime.Today;
                SessieServices sService = new SessieServices();
                List<Sessie> sessies = sService.GetSessiesFromMonth(datum);
                ConcurentieServices cService = new ConcurentieServices();
                List<Concurentie> concurentie = cService.GetFromMonth(datum);
                ConfocusClassLibrary.Kalender kalender = new ConfocusClassLibrary.Kalender(datum);
                for (int i = 1; i <= kalender.myMaand.AantalDagen; i++)
                {
                    DateTime dag = new DateTime(datum.Year, datum.Month, i);
                    var dagSessie = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == i select s).FirstOrDefault();
                    if (dagSessie != null)
                        kalender.AddDag(dag,dagSessie.Seminarie.Titel + ": " + dagSessie.Naam + " Inschr: " + dagSessie.AantalEffectieveInschrijvingen, (decimal)dagSessie.Seminarie_ID);
                    else
                        kalender.AddDag(dag, "", 0);
                }
                DateTime EersteDag = new DateTime(datum.Year, datum.Month, 1);
                SetWeekNummers(EersteDag);
                int Startdag = (int)EersteDag.DayOfWeek;
                if (Startdag == 0)
                {
                    Startdag = 7;
                }
                DateTime vorigemaand = datum.AddMonths(-1);
                int dagVorige = DateTime.DaysInMonth(vorigemaand.Year, vorigemaand.Month) - (Startdag - 2);
                int dagVolgende = 1;
                for (int j = 1; j < 43; j++)
                {
                    String control1naam = "Dag" + j;
                    String control3naam = "DagBlock" + j;
                    String control4naam = "ConcuBlock" + j;
                    Label daglabel = (Label)dagenGrid.FindName(control1naam);
                    Grid grid = (Grid)daglabel.Parent;
                    Border border = (Border)grid.Parent;
                    TextBlock block = (TextBlock)dagenGrid.FindName(control3naam);
                    TextBlock concuBlock = (TextBlock)dagenGrid.FindName(control4naam);

                    if (j >= Startdag && j <= (kalender.myMaand.AantalDagen + Startdag - 1))
                    {
                        border.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                        int dagDezeMaand = j - Startdag + 1;

                        if (dagDezeMaand == vandaag.Day && datum.Month == vandaag.Month && datum.Year == vandaag.Year)
                            daglabel.Foreground = new SolidColorBrush(Color.FromRgb(91, 43, 130));
                        else
                            daglabel.Foreground = new SolidColorBrush(Color.FromRgb(73, 152, 247));
                        daglabel.Content = dagDezeMaand;
                        block.MouseEnter += DagBlock_MouseEnter;
                        block.MouseLeave += DagBlock_MouseLeave;
                        block.Cursor = Cursors.Hand;
                        concuBlock.MouseEnter += ConcuBlock_MouseEnter;
                        concuBlock.MouseLeave += ConcuBlock_MouseLeave;
                        concuBlock.Cursor = Cursors.Hand;
                        String sessie = ((from s in kalender.Dagen where s.dagNummer == dagDezeMaand select s.Afspraak).FirstOrDefault());
                        decimal nummer = ((from s in kalender.Dagen where s.dagNummer == dagDezeMaand select s.seminarieNummer).FirstOrDefault());
                        if (sessie == "")
                        {
                            block.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
                            block.ToolTip = (new DateTime(datum.Year, datum.Month, dagDezeMaand)).ToShortDateString();
                        }
                        else
                        {
                            block.Background = new SolidColorBrush(Color.FromArgb(255, 178, 222, 232));
                            block.Text = sessie;
                            block.Tag = nummer;
                            block.ToolTip = (new DateTime(datum.Year, datum.Month, dagDezeMaand)).ToShortDateString();
                        }
                        Concurentie myConcu = (from c in concurentie where c.Datum == new DateTime(datum.Year, datum.Month, dagDezeMaand) select c).FirstOrDefault();
                        if (myConcu != null)
                        {
                            concuBlock.Background = new SolidColorBrush(Color.FromArgb(100, 255, 0, 0));
                            concuBlock.Text = myConcu.Omschrijving;
                            concuBlock.Tag = myConcu.ID;
                            concuBlock.ToolTip = (new DateTime(datum.Year, datum.Month, dagDezeMaand)).ToShortDateString();
                        }
                        else
                        {
                            concuBlock.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
                            concuBlock.ToolTip = (new DateTime(datum.Year, datum.Month, dagDezeMaand)).ToShortDateString();
                        }
                    }
                    else
                    {
                        if (j < Startdag)
                        {
                            daglabel.Content = dagVorige;
                            dagVorige++;
                        }
                        else
                        {
                            daglabel.Content = dagVolgende;
                            dagVolgende++;
                        }
                        daglabel.Foreground = new SolidColorBrush(Color.FromRgb(149, 149, 149));
                        border.Background = new SolidColorBrush(Color.FromRgb(230, 230, 230));
                        block.Cursor = Cursors.Arrow;
                        block.MouseEnter -= DagBlock_MouseEnter;
                        block.MouseLeave -= DagBlock_MouseLeave;
                        concuBlock.MouseEnter -= ConcuBlock_MouseEnter;
                        concuBlock.MouseLeave -= ConcuBlock_MouseLeave;
                        concuBlock.Cursor = Cursors.Arrow;
                    }
                }
                MaandLabel.Content = kalender.myMaand.MaandNaam + " " + datum.Year;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private StackPanel GetDagPanel()
        {
            StackPanel dagPanel = new StackPanel();
            dagPanel.Width = 100;
            dagPanel.Height = 25;
            Grid dagGrid = new Grid();
            ColumnDefinition addDefinition = new ColumnDefinition();
            addDefinition.Width = new GridLength(10);
            ColumnDefinition contentDefinition = new ColumnDefinition();
            contentDefinition.Width = new GridLength();
            ColumnDefinition moreDefinition = new ColumnDefinition();
            moreDefinition.Width = new GridLength(20);
            dagGrid.ColumnDefinitions.Add(addDefinition);
            dagGrid.ColumnDefinitions.Add(contentDefinition);
            dagGrid.ColumnDefinitions.Add(moreDefinition);
            Label addLabel = new Label();
            addLabel.Content = "+";
            addLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            addLabel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            addLabel.MouseLeftButtonUp += addLabel_MouseLeftButtonUp;
            Label contentLabel = new Label();
            contentLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            contentLabel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            Button knop = new Button();
            knop.Content = "M";
            knop.Width = 20;
            knop.Height = 20;
            knop.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            knop.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            knop.Click += moreButton_Click;
            Grid.SetColumn(addLabel, 0);
            Grid.SetColumn(contentLabel, 1);
            Grid.SetColumn(knop, 2);
            dagGrid.Children.Add(addLabel);
            dagGrid.Children.Add(contentLabel);
            dagGrid.Children.Add(knop);
            return dagPanel;
        }

        private void addLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ConfocusMessageBox.Show("geklikt", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void moreButton_Click(object sender, RoutedEventArgs e)
        {
            ConfocusMessageBox.Show("geklikt", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void SetWeekNummers(DateTime EersteDag)
        {
            int weeknummer = GetWeekNummer(EersteDag);
            week1.Content = weeknummer;
            week2.Content = weeknummer + 1;
            week3.Content = weeknummer + 2;
            week4.Content = weeknummer + 3;
            if ((weeknummer + 4) > 52)
                week5.Content = 1;
            else
                week5.Content = weeknummer + 4;
            if ((weeknummer + 5) > 52)
            {
                if ((weeknummer + 5) > 53)
                    week6.Content = 2;
                else
                    week6.Content = 1;
            }
            else
                week6.Content = weeknummer + 5;
        }

        private int GetWeekNummer(DateTime EersteDag)
        {
            var cultureInfo = CultureInfo.CurrentCulture;
            var calendar = cultureInfo.Calendar;
            var calendarWeekRule = cultureInfo.DateTimeFormat.CalendarWeekRule;
            int weeknummer = calendar.GetWeekOfYear(EersteDag, calendarWeekRule, cultureInfo.DateTimeFormat.FirstDayOfWeek);
            if (weeknummer > 52)
                weeknummer = 1;
            return weeknummer;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime datum = DateTime.Today;
            CurrentDate = datum;
            kalenderDatePicker.SelectedDate = datum;
            weekDatePicker.SelectedDate = datum;
            WeekTab.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime datum = CurrentDate.AddMonths(1);
            CurrentDate = datum;
            kalenderDatePicker.SelectedDate = CurrentDate;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DateTime datum = CurrentDate.AddMonths(-1);
            CurrentDate = datum;
            kalenderDatePicker.SelectedDate = CurrentDate;
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePicker picker = (DatePicker)sender;
            weekDatePicker.SelectedDate = picker.SelectedDate;
            CurrentDate = (DateTime)picker.SelectedDate;
            //SetKalender(CurrentDate);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DagBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock block = (TextBlock)sender;
            if (block.Text != "+" && block.Tag != null)
            {
                if (dagDetailsWindow == null)
                {
                    TextBlock blok = (TextBlock)sender;
                    SessieServices sService = new SessieServices();
                    var toolTekst = blok.ToolTip.ToString();

                    decimal sessieId = 0;
                    if (decimal.TryParse(toolTekst.Split('\n')[0],out sessieId))
                    {
                        Sessie sessie = sService.GetSessieByID(sessieId);

                        NieuweSeminarie NS = new NieuweSeminarie(currentUser, sessie.Seminarie, sessie.Datum, sessie, false);
                        if (NS.ShowDialog() == true)
                        {
                            Close();
                        }
                    }
                    else
                    {
                        dagDetailsWindow = new KalenderDagDetaills(currentUser);
                        dagDetailsWindow.Datum = block.ToolTip.ToString().Split('\n')[0];
                        dagDetailsWindow.Closed += dagDetailsWindow_Closed;
                        var point = Mouse.GetPosition(Application.Current.MainWindow);

                        dagDetailsWindow.Left = point.X - 5;
                        dagDetailsWindow.Top = point.Y - 5;
                        dagDetailsWindow.Topmost = true;
                        dagDetailsWindow.Show();
                    }
                }
            }
            else if(block.Text == "+")
            {
                DateTime datum;
                if (!DateTime.TryParse(block.ToolTip.ToString(), out datum))
                    datum = DateTime.Today;
                Seminarie seminarie = new Seminarie();
                OpenSeminarie(block, datum, seminarie, null);
            }
        }

        private void OpenSeminarie(TextBlock blok, DateTime? datum, Seminarie seminarie, Sessie sessie)
        {
           
            NieuweSeminarie NS = new NieuweSeminarie(currentUser, seminarie, datum, sessie, false);
            if (NS.ShowDialog() == true)
            {
                //SetKalender(CurrentDate);
            }
        }

        private void DagBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            TextBlock block = (TextBlock)sender;
            if (block.Tag == null)
            {
                block.Text = "+";
            }
        }

        private void dagDetailsWindow_Closed(object sender, EventArgs e)
        {
            dagDetailsWindow = null;
        }


        private void DagBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            TextBlock block = (TextBlock)sender;
            if (block.Tag == null)
            {
                block.Text = "";
            }
        }

        private void ConcuBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock block = (TextBlock)sender;
            if (block.Text == "+")
            {
                DateTime datum = DateTime.Parse(block.ToolTip.ToString());
                NieuweConcurentie NC = new NieuweConcurentie(currentUser, datum);
                if (NC.ShowDialog() == true)
                {
                    //SetKalender(CurrentDate);
                }
            }
            else if (block.Tag != null)
            {
                Decimal id = Decimal.Parse(block.Tag.ToString());
                NieuweConcurentie NC = new NieuweConcurentie(currentUser, id);
                if (NC.ShowDialog() == true)
                {
                    //SetKalender(CurrentDate);
                }
            }
        }

        private void ConcuBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            TextBlock block = (TextBlock)sender;
            if (block.Tag == null)
            {
                block.Text = "+";
            }
        }

        private void ConcuBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            TextBlock block = (TextBlock)sender;
            if (block.Tag == null)
            {
                block.Text = "";
            }
        }


        private void weekDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DatePicker picker = (DatePicker)sender;
                kalenderDatePicker.SelectedDate = picker.SelectedDate;
                CurrentDate = (DateTime)picker.SelectedDate;
                SetWeek((DateTime)picker.SelectedDate);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van de sessies!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private async void SetWeek(DateTime datum)
        {
            LoadingGrid.Visibility = Visibility.Visible;
            ClearWeek();
            int kalwidth = (int)AgendaGrid.Width - 30;
            int weeknummer = GetWeekNummer(datum);
            int weekdag = (int)datum.DayOfWeek;
            int Maandag = 1;
            int aantalDagen = DateTime.DaysInMonth(datum.Year, datum.Month);
            bool isVorigeMaand = false;
            if (datum.Day < weekdag)
            {
                DateTime vorigemaand = datum.AddMonths(-1);
                int dagen = DateTime.DaysInMonth(vorigemaand.Year, vorigemaand.Month);
                Maandag = dagen + (datum.Day - weekdag + 1);
                isVorigeMaand = true;
                aantalDagen = dagen;
            }
            else if (datum.Day == weekdag)
            {
                Maandag = 1;
            }
            else
            {
                Maandag = datum.Day - weekdag + 1;
            }         
            weekLabel.Content = datum.ToString("MMMM") + " " + datum.Year + " Week: " + weeknummer;
            SessieServices sService = new SessieServices();
            if (isVorigeMaand)
                datum = datum.AddMonths(-1);
            List<Sessie> sessies = await Task.Run(() => sService.GetSessiesForWeek(new DateTime(datum.Year, datum.Month, Maandag)));
            int aantal = 0;
            int start = 0;
            Weekdag1Label.Content = "Maandag " + Maandag;
            List<Sessie> dag1sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == Maandag orderby s.Beginuur, s.Naam select s).ToList();
            if (dag1sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);//140;//(int)(144 / dag1sessies.Count) - 4;
                start = 2;
                foreach (Sessie mysessie in dag1sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 1, start, aantal);
                        start += 4;
                    }
                }
            }
            //var sessie = (from s in sessies where ((DateTime)s.Datum).Day == Maandag select s).FirstOrDefault();

            if ((Maandag + 1) > aantalDagen)
            {
                Maandag = 1 - 1;
            }
            Weekdag2Label.Content = "Dinsdag " + (Maandag + 1);
            List<Sessie> dag2sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == (Maandag + 1) orderby s.Beginuur, s.Naam select s).ToList();
            if (dag2sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);//140;// (int)(144 / dag2sessies.Count) - 4;
                start = 2;
                foreach (Sessie mysessie in dag2sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 2, start, aantal);
                        start += 4;// aantal + 2;
                    }
                }
            }
            if ((Maandag + 2) > aantalDagen)
            {
                Maandag = 1 - 2;
            }
            Weekdag3Label.Content = "Woensdag " + (Maandag + 2);
            List<Sessie> dag3sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == (Maandag + 2) orderby s.Beginuur, s.Naam select s).ToList();
            if (dag3sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);
                start = 2;
                foreach (Sessie mysessie in dag3sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 3, start, aantal);
                        start += 4;
                    }
                }
            }
            if ((Maandag + 3) > aantalDagen)
            {
                Maandag = 1 - 3;
            }
            Weekdag4Label.Content = "Donderdag " + (Maandag + 3);
            List<Sessie> dag4sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == (Maandag + 3) orderby s.Beginuur, s.Naam select s).ToList();
            if (dag4sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);
                start = 2;
                foreach (Sessie mysessie in dag4sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 4, start, aantal);
                        start += 4;
                    }
                }
            }
            if ((Maandag + 4) > aantalDagen)
            {
                Maandag = 1 - 4;
            }
            Weekdag5Label.Content = "Vrijdag " + (Maandag + 4);
            List<Sessie> dag5sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == (Maandag + 4) orderby s.Beginuur, s.Naam select s).ToList();
            if (dag5sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);
                start = 2;
                foreach (Sessie mysessie in dag5sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 5, start, aantal);
                        start += 4;
                    }
                }

            }
            if ((Maandag + 5) > aantalDagen)
            {
                Maandag = 1 - 5;
            }
            Weekdag6Label.Content = "Zaterdag " + (Maandag + 5);
            List<Sessie> dag6sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == (Maandag + 5) orderby s.Beginuur, s.Naam select s).ToList();
            if (dag6sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);
                start = 2;
                foreach (Sessie mysessie in dag6sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 6, start, aantal);
                        start += 4;
                    }
                }
            }
            if ((Maandag + 6) > aantalDagen)
            {
                Maandag = 1 - 6;
            }
            Weekdag7Label.Content = "Zondag " + (Maandag + 6);
            List<Sessie> dag7sessies = (from s in sessies where s.Datum != null && ((DateTime)s.Datum).Day == (Maandag + 6) orderby s.Beginuur, s.Naam select s).ToList();
            if (dag7sessies.Count > 0)
            {
                aantal = (int)(kalwidth / 7 - 5);
                start = 2;
                foreach (Sessie mysessie in dag7sessies)
                {
                    if (mysessie != null)
                    {
                        SetSessieToDay(mysessie, 7, start, aantal);
                        start += 4;
                    }
                }
            }
            LoadingGrid.Visibility = Visibility.Hidden;
        }

        private void SetSessieToDay(Sessie sessie, int dag, int start, int lengte)
        {
            Border border = new Border();
            border.BorderThickness = new Thickness(1);
            border.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            border.Margin = new Thickness(2, 2, 2, 2);
            border.Width = lengte;
            border.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            if (sessie.Status == "Geannuleerd")
                border.Background = new SolidColorBrush(Color.FromRgb(203, 119, 119)); // rood
            else if (sessie.Status == "Verplaatst")
            {
                border.Background = new SolidColorBrush(Color.FromRgb(255, 255, 153)); // Geel
            }
            else if (sessie.Status == "Actief niet online")
            {
                border.Background = new SolidColorBrush(Color.FromRgb(190, 190, 190)); // grijs
            }
            else if (sessie.Seminarie.Type == "On demand webinar")
                border.Background = new SolidColorBrush(Color.FromRgb(4, 145, 73)); //Oranje
            else if (sessie.Seminarie.Type == "Live webinar")
                border.Background = new SolidColorBrush(Color.FromRgb(0, 255, 0)); // fel groen
            else if (sessie.Seminarie.Type == "Coming soon")
                border.Background = new SolidColorBrush(Color.FromRgb(245, 182, 66)); // donker groen
            else
            {
                
                    border.Background = new SolidColorBrush(Color.FromRgb(178, 222, 232)); //Licht blauw
            }

            TextBlock block = new TextBlock();
            block.Tag = sessie.Seminarie_ID;
            string titel = sessie.SeminarieTitel + "\n" + sessie.Naam + "\n" + (sessie.Beginuur == null ? "" : sessie.Beginuur) + " tot " + (sessie.Einduur == null ? "" : sessie.Einduur) + "\n" + sessie.LocatieNaam + "\nInsch: " + sessie.AantalEffectieveInschrijvingen + " : " + sessie.VoorNaamVTerplaatse;
            block.Text = titel;
            block.Margin = new Thickness(2, 2, 2, 2);
            block.Width = lengte;
            block.ToolTip = sessie.ID + "\n" + sessie.SeminarieTitel + "\n" + sessie.NaamDatumLocatie;//((DateTime)sessie.Datum).ToShortDateString() +
            block.MouseDown += DagBlock_MouseDown;
            block.Cursor = Cursors.Hand;
            block.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            block.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;

            border.Child = block;
            AgendaGrid.Children.Add(border);
            Grid.SetRow(border, start);
            Grid.SetColumn(border, dag);
            Grid.SetRowSpan(border, start + 4);

        }

        private int GetEindPos(string einduur, int startpos)
        {
            try
            {
                int result = 0;
                String[] uurArray = einduur.Split(':');
                String uur = uurArray[0];
                String minuten = uurArray[1];
                int intUur = 0;
                if (int.TryParse(uur, out intUur))
                {
                    switch (intUur)
                    {
                        case 7:
                            result = 2;
                            break;
                        case 8:
                            result = 4;
                            break;
                        case 9:
                            result = 6;
                            break;
                        case 10:
                            result = 8;
                            break;
                        case 11:
                            result = 10;
                            break;
                        case 12:
                            result = 12;
                            break;
                        case 13:
                            result = 14;
                            break;
                        case 14:
                            result = 16;
                            break;
                        case 15:
                            result = 18;
                            break;
                        case 16:
                            result = 20;
                            break;
                        case 17:
                            result = 22;
                            break;
                        case 18:
                            result = 24;
                            break;
                        case 19:
                            result = 26;
                            break;
                        case 20:
                            result = 28;
                            break;
                        case 21:
                            result = 30;
                            break;
                        case 22:
                            result = 32;
                            break;
                        case 23:
                            result = 34;
                            break;
                        default:
                            result = 16;
                            break;
                    }
                    int intminuut = 0;
                    if (int.TryParse(minuten, out intminuut))
                    {
                        if (intminuut > 20)
                            result += 1;
                        if (intminuut > 40)
                            result += 1;
                    }
                    result -= startpos;
                }
                else
                    result = 16;

                return result;
            }
            catch (Exception ex)
            {
                return 16;
            }
        }

        private int GetStartPos(string beginuur)
        {
            try
            {
                int result = 0;
                String[] uurArray = beginuur.Split(':');
                String uur = uurArray[0];
                String minuten = uurArray[1];
                int intUur = 0;
                if (int.TryParse(uur, out intUur))
                {
                    switch (intUur)
                    {
                        case 7:
                            result = 2;
                            break;
                        case 8:
                            result = 4;
                            break;
                        case 9:
                            result = 6;
                            break;
                        case 10:
                            result = 8;
                            break;
                        case 11:
                            result = 10;
                            break;
                        case 12:
                            result = 12;
                            break;
                        case 13:
                            result = 14;
                            break;
                        case 14:
                            result = 16;
                            break;
                        case 15:
                            result = 18;
                            break;
                        case 16:
                            result = 20;
                            break;
                        case 17:
                            result = 22;
                            break;
                        case 18:
                            result = 24;
                            break;
                        case 19:
                            result = 26;
                            break;
                        case 20:
                            result = 28;
                            break;
                        case 21:
                            result = 30;
                            break;
                        case 22:
                            result = 32;
                            break;
                        case 23:
                            result = 34;
                            break;
                        default:
                            result = 4;
                            break;
                    }
                    int intMinuut = 0;
                    if (int.TryParse(minuten, out intMinuut))
                    {
                        if (intMinuut > 20)
                            result += 1;
                        if (intMinuut > 40)
                            result += 1;
                    }
                }
                else
                    result = 4;

                return result;
            }
            catch (Exception ex)
            {
                return 4;
            }

        }

        private void ClearWeek()
        {
            for (int i = AgendaGrid.Children.Count - 1; i > 0; i--)
            {
                var item = AgendaGrid.Children[i];

                if (item is Border)
                {
                    Border border = (Border)item;
                    if (border.Child is TextBlock)
                        AgendaGrid.Children.Remove(border);
                }
            }
        }

        private void weekTerugButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime datum = CurrentDate.AddDays(-7);
            CurrentDate = datum;
            weekDatePicker.SelectedDate = CurrentDate;
        }

        private void weekVerderButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime datum = CurrentDate.AddDays(7);
            CurrentDate = datum;
            weekDatePicker.SelectedDate = CurrentDate;
        }

    }
}

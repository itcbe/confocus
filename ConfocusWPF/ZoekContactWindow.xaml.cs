﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ZoekContactWindow.xaml
    /// </summary>
    public partial class ZoekContactWindow : Window
    {
        public Functies functie;
        public Contact contact;
        public Bedrijf bedrijf;
        private bool isHistoriek = false;
        public bool IsHistoriek
        {
            get
            { return isHistoriek; }
            set
            { isHistoriek = value; }
        }

        private string mySoort = "";
        public ZoekContactWindow(string soort)
        {
            InitializeComponent();
            mySoort = soort;
            if (mySoort == "Contact")
            {
                Title = "Zoek contact";
                nameLabel.Content = "Voornaam:";
            }
            else if (mySoort == "Bedrijf")
            { 
                Title = "Zoek bedrijf";
                nameLabel.Content = "Naam:";
                achternaamLabel.Content = "Adres:";
                //achternaamLabel.Visibility = Visibility.Hidden;
                //zoekAchternaamTextBox.Visibility = Visibility.Hidden;
            }
            else
                Title = "Zoek";
            zoekVoornaamTextBox.Focus();
        }

        private void zoekButton_Click(object sender, RoutedEventArgs e)
        {
            if (mySoort == "Contact")
                ZoekContact();
            else if (mySoort == "Bedrijf")
                ZoekBedrijf();

        }

        private void ZoekBedrijf()
        {
            ContactZoekTerm searchterm = new ContactZoekTerm();
            searchterm.Bedrijfsnaam = zoekVoornaamTextBox.Text;
            searchterm.Bedrijfsadres = zoekAchternaamTextBox.Text;
            if (!string.IsNullOrEmpty(searchterm.Bedrijfsnaam) || !string.IsNullOrEmpty(searchterm.Bedrijfsadres))
            {
                BedrijfServices bService = new BedrijfServices();
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                if (isHistoriek)
                    bedrijven = bService.GetBySearchTerm(searchterm); 
                else
                    bedrijven = bService.GetAvtiveBySearchTerm(searchterm);
                contactenlistBox.ItemsSource = bedrijven;
                contactenlistBox.DisplayMemberPath = "NaamEnAdres";
                resultLabel.Content = bedrijven.Count + " bedrijven gevonden.";
            }
        }

        private void ZoekContact()
        {
            ContactZoekTerm Searchterm = new ContactZoekTerm();
            Searchterm.Voornaam = DBHelper.Simplify(zoekVoornaamTextBox.Text);
            Searchterm.Achternaam = DBHelper.Simplify(zoekAchternaamTextBox.Text);
            if (!string.IsNullOrEmpty(Searchterm.Voornaam) || !string.IsNullOrEmpty(Searchterm.Achternaam))
            {
                ContactServices cService = new ContactServices();
                List<Contact> contacten = new List<Contact>();
                if (isHistoriek)
                    contacten = cService.GetBySearchTerm(Searchterm);
                else
                    contacten = cService.GetActiveBySearchTerm(Searchterm);

                contactenlistBox.ItemsSource = contacten;
                contactenlistBox.DisplayMemberPath = "NaamEnGeboortedatum";
                resultLabel.Content = contacten.Count + " contacten gevonden.";
            }
            else
                ConfocusMessageBox.Show("Geen naam ingegeven!", ConfocusMessageBox.Kleuren.Rood);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void listBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (mySoort == "Contact")
                contact = (Contact)box.SelectedItem;
            else if (mySoort == "Bedrijf")
                bedrijf = (Bedrijf)box.SelectedItem;
            
            DialogResult = true;
        }

        private void zoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                if (mySoort == "Contact")
                    ZoekContact();
                else if (mySoort == "Bedrijf")
                    ZoekBedrijf();
            }
        }
    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwcurrentBedrijf.xaml
    /// </summary>
    public partial class NieuwBedrijf : Window
    {

        private Gebruiker currentUser;
        private Bedrijf _bedrijf;

        public Bedrijf Bedrijf
        {
            get { return _bedrijf; }
            set { _bedrijf = value; }
        }
        

        public NieuwBedrijf(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PostcodeService service = new PostcodeService();
            List<Postcodes> postcodes = service.FindAllPostcodes();
            postcodeComboBox.ItemsSource = postcodes;
            postcodeComboBox.DisplayMemberPath = "Postcode";
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Bedrijf b = GetBedrijfFromForm();
            if (b != null)
            {
                try
                {
                    BedrijfServices service = new BedrijfServices();
                    _bedrijf = service.SaveBedrijf(b);
                    DialogResult = true;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Bedrijf GetBedrijfFromForm()
        {
            if (IsFormValid())
            {
                Bedrijf b = new Bedrijf();
                b.Naam = naamTextBox.Text;
                b.Ondernemersnummer = Helper.CleanVatnumber(ondernemersnummerTextBox.Text);
                b.Adres = adresTextBox.Text;
                b.Postcode = postcodeComboBox.Text;
                b.Plaats = plaatsComboBox.Text;
                b.Provincie = provincieComboBox.Text;
                b.Land = landTextBox.Text;
                b.Telefoon = telefoonTextBox.Text;
                b.Fax = faxTextBox.Text;
                b.Website = websiteTextBox.Text;
                b.Email = emailTextBox.Text;
                b.Notities = notitiesTextBox.Text;
                b.Actief = actiefCheckBox.IsChecked == true;
                b.Aangemaakt = DateTime.Now;
                b.Aangemaakt_door = currentUser.Naam;
                b.BedrijfsnaamFonetisch = DBHelper.Simplify(b.Naam);

                if (postcodeComboBox.SelectedIndex > -1)
                {
                    Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                    b.Postcode = postcodeComboBox.Text;
                    b.Zipcode = postcode.ID;
                    if (plaatsComboBox.SelectedIndex > -1)
                        b.Plaats = ((Postcodes)plaatsComboBox.SelectedItem).Gemeente;
                    else
                        b.Plaats = postcode.Gemeente;
                    b.Provincie = postcode.Provincie;
                    b.Land = postcode.Land;

                }

                return b;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (string.IsNullOrEmpty(naamTextBox.Text))
            {
                errorMessage += "De naam is niet ingevuld!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

            if (ondernemersnummerTextBox.Text.Length > 12)
            {
                errorMessage += "De ondernemingsnummer is te lang!\nVerwijder eventuele spaties of punten.";
                Helper.SetTextBoxInError(ondernemersnummerTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(ondernemersnummerTextBox, normalTemplate);

            if (emailTextBox.Text.Length > 120)
            {
                errorMessage += "De emailadres is te lang! Maximum 120 karakters.";
                Helper.SetTextBoxInError(emailTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(emailTextBox, normalTemplate);

            if (telefoonTextBox.Text.Length > 30)
            {
                errorMessage += "Het telefoonnummer is te lang!\nVerwijder eventuele spaties of punten.";
                Helper.SetTextBoxInError(telefoonTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(telefoonTextBox, normalTemplate);

            if (faxTextBox.Text.Length > 30)
            {
                errorMessage += "Het faxnummer is te lang!\nVerwijder eventuele spaties of punten.";
                Helper.SetTextBoxInError(faxTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(faxTextBox, normalTemplate);

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }

        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (postcodeComboBox.SelectedIndex > -1)
            {
                Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                PostcodeService service = new PostcodeService();
                List<Postcodes> gemeentes = service.GetGemeentesByPostcode(postcode.Postcode);
                plaatsComboBox.ItemsSource = gemeentes;
                plaatsComboBox.DisplayMemberPath = "Gemeente";
                plaatsComboBox.SelectedIndex = 0;
                provincieComboBox.Text = postcode.Provincie;
                landTextBox.Text = postcode.Land;
            }
            else
            {
                plaatsComboBox.ItemsSource = new List<Postcodes>();
                provincieComboBox.SelectedIndex = -1;
                landTextBox.Text = "";
            }
        }

        private void EmailTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (!String.IsNullOrEmpty(box.Text))
                if (!Helper.CheckEmail(box.Text))
                    ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }
    }
}

﻿using ConfocusDBLibrary;
using ConfocusWPF.Flexmail;
using ITCLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for FlexMailResults.xaml
    /// </summary>
    public partial class FlexMailResults : Window
    {
        private CollectionViewSource emailCollectionViewSource;
        private List<EmailAdres> Adressen = new List<EmailAdres>();
        private string Verslag = "";
        private List<EmailAdres> invalid;
        private List<EmailAdres> blacklist;
        private List<EmailAdres> bounced;
        private List<EmailAdres> doubleEmails;
        private List<EmailAdres> allSend;
        private int _listID = 0;
        public List<EmailAdres> notimported { get; set; }

        public FlexMailResults(List<EmailAdres> adressen, string verslag)
        {
            InitializeComponent();
            Adressen = adressen;
            Verslag = verslag;

        }

        public FlexMailResults(List<EmailAdres> adressen, string verslag, List<EmailAdres> invalid, List<EmailAdres> blacklist, List<EmailAdres> bounced, List<EmailAdres> doubleEmails, List<EmailAdres> all, int listid, List<EmailAdres> notimported)
        {
            InitializeComponent();
            this.Adressen = adressen;
            this.Verslag = verslag;
            this.invalid = invalid;
            this.blacklist = blacklist;
            this.bounced = bounced;
            this.doubleEmails = doubleEmails;
            this.notimported = notimported;
            allSend = all;
            this._listID = listid;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            emailCollectionViewSource = ((CollectionViewSource)(this.FindResource("emailadresViewSource")));
            emailCollectionViewSource.Source = Adressen;

            if (notimported.Count > 0)
                Verslag += "\nNiet geimporteerd: " + notimported.Count;

            textBlock.Text = Verslag;


        }

        private void InvalidButton_Click(object sender, RoutedEventArgs e)
        {
            if (invalid.Count > 0)
            {
                var excists = allSend.AsQueryable().Where(em => em.Reference.Any(x => invalid.Contains(em))).ToList();
                string[][] adressen = new string[excists.Count + 1][];
                adressen[0] = new string[8] { "Naam", "Voornaam", "Bedrijf", "Email", "Niveau 1", "Niveau 2", "Niveau 3", "Referencie" };
                int f = 1;
                foreach (EmailAdres item in excists)
                {
                    adressen[f] = new string[8] { item.FirstName, item.LastName, item.Company, item.Email, (item.Niveau1 == null ? "" : item.Niveau1), (item.Niveau2 == null ? "" : item.Niveau2), (item.Niveau3 == null ? "" : item.Niveau3), item.Reference };
                    f++;
                }

                ToExcel(adressen);

            }
        }

        private void BlacklistButton_Click(object sender, RoutedEventArgs e)
        {
            if (blacklist.Count > 0)
            {
                var excists = allSend.AsQueryable().Where(em => em.Reference.Any(x => blacklist.Contains(em))).ToList();
                string[][] adressen = new string[excists.Count + 1][];
                adressen[0] = new string[8] { "Naam", "Voornaam", "Bedrijf", "Email", "Niveau 1", "Niveau 2", "Niveau 3", "Referencie" };
                int f = 1;
                foreach (EmailAdres item in excists)
                {
                    adressen[f] = new string[8] { item.FirstName, item.LastName, item.Company, item.Email, (item.Niveau1 == null ? "" : item.Niveau1), (item.Niveau2 == null ? "" : item.Niveau2), (item.Niveau3 == null ? "" : item.Niveau3), item.Reference };
                    f++;
                }

                ToExcel(adressen);
            }
        }

        private void BouncedButton_Click(object sender, RoutedEventArgs e)
        {
            if (bounced.Count > 0)
            {
                var excists = allSend.AsQueryable().Where(em => em.Reference.Any(x => bounced.Contains(em))).ToList();
                string[][] adressen = new string[excists.Count + 1][];
                adressen[0] = new string[8] { "Naam", "Voornaam", "Bedrijf", "Email", "Niveau 1", "Niveau 2", "Niveau 3", "Referencie" };
                int f = 1;
                foreach (EmailAdres item in excists)
                {
                    adressen[f] = new string[8] { item.FirstName, item.LastName, item.Company, item.Email, (item.Niveau1 == null ? "" : item.Niveau1), (item.Niveau2 == null ? "" : item.Niveau2), (item.Niveau3 == null ? "" : item.Niveau3), item.Reference };
                    f++;
                }

                ToExcel(adressen);
            }
        }

        private void DoubleButton_Click(object sender, RoutedEventArgs e)
        {
            if (doubleEmails.Count > 0)
            {
                var excists = allSend.AsQueryable().Where(em => em.Reference.Any(x => doubleEmails.Contains(em))).ToList();
                string[][] adressen = new string[excists.Count + 1][];
                adressen[0] = new string[8] { "Naam", "Voornaam", "Bedrijf", "Email", "Niveau 1", "Niveau 2", "Niveau 3", "Referencie" };
                int f = 1;
                foreach (EmailAdres item in excists)
                {
                    adressen[f] = new string[8] { item.FirstName, item.LastName, item.Company, item.Email, (item.Niveau1 == null ? "" : item.Niveau1), (item.Niveau2 == null ? "" : item.Niveau2), (item.Niveau3 == null ? "" : item.Niveau3), item.Reference };
                    f++;
                }

                ToExcel(adressen);
            }
        }

        private void ToExcel(string[][] adressen)
        {
            ExcelDocument excel = new ExcelDocument();
            int[] widths = new int[8] { 30,30,30,30,30,30,30,30};
            excel.InsertList(adressen, true, 1, widths, 18);

        }

        private void NotImportedButton_Click(object sender, RoutedEventArgs e)
        {
            if (notimported.Count > 0)
            {
                ExcelDocument excel = new ExcelDocument();
                int[] widths = new int[7] { 30, 30, 30, 30, 30, 30, 30 };
                String[][] functies = new string[notimported.Count + 1][];
                string[] titels = new string[7] { "Voornaam", "Naam", "Bedrijf", "Email", "Niveau1", "niveau2", "Niveau3" };
                functies[0] = titels;
                int index = 1;
                foreach (EmailAdres functie in notimported)
                {
                    string[] arrfunctie = new string[7]
                    {
                    functie.FirstName,
                    functie.LastName,
                    functie.Company,
                    functie.Email,
                    functie.Niveau1,
                    functie.Niveau2,
                    functie.Niveau3
                    };
                    functies[index] = arrfunctie;
                    index++;
                }
                excel.InsertList(functies, true, 1, widths, 18);

            }
        }
    }
}

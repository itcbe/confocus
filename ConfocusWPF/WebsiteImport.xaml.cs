﻿using ADOClassLibrary;
using ConfocusClassLibrary;
using ConfocusDBLibrary;
using ConfocusWPF.Usercontrols;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for WebsiteImport.xaml
    /// </summary>
    public partial class WebsiteImport : Window
    {
        /// <summary>
        /// Private memebers
        /// </summary>
        private Gebruiker currentUser;
        private WebsiteInschrijving _inschrijving;
        private Instelling _instelling;
        private WebInschrijvingUserControl mainInschrijvingControl;
        private ConfocusDBLibrary.Inschrijvingen currentInschrijving;
        private string _dataFolder = "";
        //private List<Seminarie> _seminaries;
        private List<InschrijvingSessie> _sessies;
        private bool geldigeInschrijving = true;

        /// <summary>
        /// Properties
        /// </summary>
        public Contact MyContact { get; set; }
        public Functies MyFunctie { get; set; }
        public Bedrijf MyBedrijf { get; set; }
        public Functies MyVerantwoordelijkeOpleiding { get; set; }
        public Functies MyVerantwoordelijkePersoneel { get; set; }
        public ConfocusDBLibrary.Inschrijvingen MyInschrijving { get; set; }
        public Bedrijf MyFacturatieOp { get; set; }
        public Sessie MySessie { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">Ingelogde gebruiker</param>
        public WebsiteImport(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        /// <summary>
        /// Functie die alle inschrijvingen overhaald van de FTP server naar een lokale map
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void websiteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ftp myftp = new ftp();
                myftp.GetSFTP();
                LoadInschrijvingenInComboBox();
                ConfocusMessageBox.Show(ErrorMessages.DownLoadKlaar, ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                if (ex.Message == ErrorMessages.DownLoadKlaar)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Blauw);
                    LoadInschrijvingenInComboBox();
                }
                else
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Event als men met de muis over de knop gaat wordt de kleur oranje van het icoontje
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button button = (Button)sender;
            Image img = new Image();
            switch (button.Name)
            {
                case "website":
                    img = websiteImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "openall":
                    img = openallImg;
                    break;
                case "closeall":
                    img = closeallImg;
                    break;
                default:
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + button.Name + "_hover.gif"));
        }

        /// <summary>
        /// Event als men met de muis van de knop af gaat wordt de kleur wit van het icoontje
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button button = (Button)sender;
            Image img = new Image();
            switch (button.Name)
            {
                case "website":
                    img = websiteImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "openall":
                    img = openallImg;
                    break;
                case "closeall":
                    img = closeallImg;
                    break;

                default:
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + button.Name + ".gif"));
        }

        /// <summary>
        /// Event bij het wijzigen van de facturatie bedrijf combobox wordt deze aan de inschrijving gekoppeld.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void facturatieBedrijfComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (_inschrijving != null)
            {
                if (box.SelectedIndex > -1)
                {
                    Bedrijf bedrijf = (Bedrijf)box.SelectedItem;
                    _inschrijving.FacturatieBedrijf = bedrijf;
                    if (bedrijf.ID == 0)
                    {
                        if (currentInschrijving != null)
                            currentInschrijving.Facturatie_op_ID = null;
                        _inschrijving.FacturatieBedrijf = null;
                    }
                    else
                    {
                        if (currentInschrijving != null)
                            currentInschrijving.Facturatie_op_ID = bedrijf.ID;
                        _inschrijving.FacturatieBedrijf = bedrijf;
                    }
                    CheckInput();
                }
            }
        }

        /// <summary>
        /// Event Als het venster geladen is haal dan de gegevens op
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Laat de inschrijvingnamen in de inschrijving combobox
            _dataFolder = new Instelling_Service().Find().DataFolder;
            LoadInschrijvingenInComboBox();
            mainInschrijvingControl = new WebInschrijvingUserControl();
            InschrijvingFormPanel.Children.Add(mainInschrijvingControl);
            // Laad de seminaries
            LoadSeminariesAsync();
        }

        /// <summary>
        /// Functie die asyncroon de seminaries gaat inladen
        /// </summary>
        private async void LoadSeminariesAsync()
        {
            StartLoader("Seminaries laden...");
            _sessies = await Task.Run(() => new ConfocusDBManager().GetInschrijvingSessies());
            //_seminaries = await Task.Run(() => new SeminarieServices().GetAllActiveForInschrijving());//GetSeminariesWithActiveSessies();
            StopLoader();
        }

        /// <summary>
        /// Functie die het laderpanel verbergt
        /// </summary>
        private void StopLoader()
        {
            LoadingPanel.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// functie die het laderpanel toont
        /// </summary>
        /// <param name="v"></param>
        private void StartLoader(string v)
        {
            LoadingTekxtBlock.Text = v;
            LoadingPanel.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Functie die de inschrijvingen van de map Nieuwe FTP laad in de combobox
        /// </summary>
        private void LoadInschrijvingenInComboBox()
        {
            try
            {
                Instelling_Service iService = new Instelling_Service();
                _instelling = iService.Find();
                string path = _instelling.NieuweInschrijvingenPad;
                List<string> files = new List<string>();
                foreach (string file in Directory.EnumerateFiles(path, "*.xml"))
                {
                    files.Add(file.Replace(_instelling.NieuweInschrijvingenPad + "\\", ""));
                }

                websiteInschrijvingenComboBox.ItemsSource = files;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van de inschrijvingen!!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Event die de gekozen inschrijving gaat inlezen 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void websiteInschrijvingenComboBox_DropDownClosed(object sender, EventArgs e)
        {
            geldigeInschrijving = true;
            ComboBox box = (ComboBox)sender;
            ClearForm();
            if (box.SelectedIndex > -1)
            {
                StartLoader("Xml laden...");
                string filename = box.Text;
                try
                {
                    mainInschrijvingControl.Status = "Inschrijving";
                    mainInschrijvingControl.Dagtype = "Volledige sessie";
                    _inschrijving = XMLReader.GetInschrijvingFromXml(filename);
                    // Haal de juiste gegvens op adv de XML en toon deze in het venster
                    BindInschrijvingAsync(_inschrijving);
                    StopLoader();
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        /// <summary>
        /// Functie die het formulier leeg maakt.
        /// </summary>
        private void ClearForm()
        {
            mainInschrijvingControl.Clear();
            MyVerantwoordelijkePersoneel = null;
            MyVerantwoordelijkeOpleiding = null;
            MyFunctie = null;
            MyContact = null;
            MyBedrijf = null;
            MyFacturatieOp = null;
            MyInschrijving = null;
            contactWebsiteNaamLabel.Content = "-";
            contactVoorstelNaamLabel.Content = "-";
            contactFunctieComboBox.ItemsSource = new List<Functies>();
            websiteBedrijfLabel.Content = "-";
            bedrijfVoorstelLabel.Content = "-";
            BedrijfComboBox.ItemsSource = new List<Bedrijf>();
            facturatieBedrijfLabel.Content = "-";
            facturatieVoorstelBedrijfLabel.Content = "-";
            facturatieBedrijfComboBox.ItemsSource = new List<Bedrijf>();
            opleidingNaamLabel.Content = "";
            opleidingEmailLabel.Content = "";
            seminarieStackPanel.Children.Clear();
        }

        /// <summary>
        /// Functie die alle gegevens op het formulier plaatst en contoleerd
        /// </summary>
        /// <param name="inschrijving"></param>
        private async void BindInschrijvingAsync(WebsiteInschrijving inschrijving)
        {
            // De XML tonen op het formulier
            XMLTextBlock.Text = await Task.Run(() => SetXmlToTextBlock(inschrijving));
            // Invullen van de contactnaam en bedrijf in het website label op het formulier
            contactWebsiteNaamLabel.Content = inschrijving.Achternaam + " " + inschrijving.Voornaam + ", " + inschrijving.Bedrijfsnaam + " email: " + inschrijving.Email;
            // Als er functies gevonden zijn voeg de eerste functie in als MyFunctie
            LoadFunctionsAsync(inschrijving);

            // facturatie bedrijf
            facturatieBedrijfLabel.Content = inschrijving.FacturatieBedrijfsNaam + ", " + inschrijving.FacturatieBTWNummer + " Tel: " + inschrijving.FacturatieTelefoon;
            GetFacturatieGegevensAsync(inschrijving);

            CheckInput();

            mainInschrijvingControl.Notitie = inschrijving.Opmerking;
            if (!string.IsNullOrEmpty(inschrijving.Tav))
                mainInschrijvingControl.Notitie += "\nTav: " + inschrijving.Tav;
            if (!string.IsNullOrEmpty(inschrijving.Erkenningsnummer))
                mainInschrijvingControl.Notitie += "\nErkenningsnummer: " + inschrijving.Erkenningsnummer;
            if (!string.IsNullOrEmpty(inschrijving.CommunicatieEmail))
                mainInschrijvingControl.CCEmail = inschrijving.CommunicatieEmail;

        }

        /// <summary>
        /// Functie om de facturatie gegevens te controleren en in te stellen.
        /// </summary>
        /// <param name="inschrijving"></param>
        private async void GetFacturatieGegevensAsync(WebsiteInschrijving inschrijving)
        {
            List<Bedrijf> facturatiebedrijven = new List<Bedrijf>();
            if (inschrijving.Identiek && MyBedrijf.ID != 0)
            {
                facturatieVoorstelBedrijfLabel.Content = MyBedrijf.Naam + ", " + MyBedrijf.Ondernemersnummer + " Tel: " + MyBedrijf.Telefoon;
                MyFacturatieOp = MyBedrijf;
                Bedrijf leeg = new Bedrijf();
                leeg.Naam = "";
                facturatiebedrijven.Insert(0, leeg);
                facturatiebedrijven.Add(MyBedrijf);
                facturatieBedrijfComboBox.ItemsSource = facturatiebedrijven;
                facturatieBedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
                
                facturatieBedrijfComboBox.Text = MyBedrijf.NaamEnAdres;
            }
            else
            {
                facturatiebedrijven = await Task.Run(() => new BedrijfServices().GetBedrijfByName(inschrijving.FacturatieBedrijfsNaam));//GetBedrijfBySearchTerm
                if (facturatiebedrijven.Count > 0)
                {
                    var query = (from b in facturatiebedrijven where b.Postcode == _inschrijving.FacturatiePostcode select b);
                    Bedrijf temp = query.FirstOrDefault();
                    if (temp != null)
                        if (temp.ID == MyBedrijf.ID)
                        {
                            MyFacturatieOp = null;
                            if (currentInschrijving != null)
                                currentInschrijving.Facturatie_op_ID = null;
                        }
                        else
                            MyFacturatieOp = temp;
                    else
                        MyFacturatieOp = facturatiebedrijven[0];

                    if (MyFacturatieOp != null)
                    {
                        if (MyFacturatieOp.ID == MyBedrijf.ID)
                        {
                            MyFacturatieOp = null;
                            if (currentInschrijving != null)
                                currentInschrijving.Facturatie_op_ID = null;
                        }
                        else
                        {
                            facturatieVoorstelBedrijfLabel.Content = MyFacturatieOp.Naam + ", " + MyFacturatieOp.Ondernemersnummer + " Tel: " + MyFacturatieOp.Telefoon;
                            Bedrijf leeg = new Bedrijf();
                            leeg.Naam = "";
                            facturatiebedrijven.Insert(0, leeg);
                            facturatieBedrijfComboBox.ItemsSource = facturatiebedrijven;
                            facturatieBedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
                            if (temp != null)
                                facturatieBedrijfComboBox.Text = temp.NaamEnAdres;
                            else
                                facturatieBedrijfComboBox.Text = MyFacturatieOp.NaamEnAdres;
                        }
                    }
                }
            }
            CheckInput();
        }

        private List<Bedrijf> GetBedrijvenAsync(WebsiteInschrijving inschrijving)
        {
            List<Bedrijf> bedrijven = inschrijving.GetBedrijf();
            return bedrijven;
        }

        //private void BindInschrijving(WebsiteInschrijving inschrijving)
        //{
        //    Task.Run(() => SetXmlToTextBlock(inschrijving));
        //    List<Bedrijf> bedrijven = inschrijving.GetBedrijf();
        //    BedrijfComboBox.ItemsSource = bedrijven;
        //    BedrijfComboBox.DisplayMemberPath = "NaamEnBtwNummer";

        //    BedrijfServices bService = new BedrijfServices();
        //    // functies
        //    List<Functies> functies = LoadFunctions(inschrijving);

        //    contactWebsiteNaamLabel.Content = inschrijving.Achternaam + " " + inschrijving.Voornaam + ", " + inschrijving.Bedrijfsnaam + " email: " + inschrijving.Email;
        //    if (functies.Count > 0)
        //    {
        //        MyFunctie = functies[0];
        //        if (MyFunctie != null)
        //        {
        //            contactVoorstelNaamLabel.Content = MyFunctie.Contact.Achternaam + " " + MyFunctie.Contact.Voornaam + ", " + MyFunctie.Bedrijf.Naam + " email: " + MyFunctie.Email;
        //            MyContact = MyFunctie.Contact;
        //            contactFunctieComboBox.Text = MyFunctie.NaamBedrijfFunctie;
        //            NaamComboBox_DropDownClosed(contactFunctieComboBox, new EventArgs());
        //        }
        //    }
        //    else
        //    {
        //        MyFunctie = new Functies();
        //        MyFunctie.ID = 0;
        //        MyFunctie.Email = inschrijving.Email;
        //        MyFunctie.Oorspronkelijke_Titel = inschrijving.FunctieNaam;
        //        MyFunctie.Fax = true;
        //        MyFunctie.Mail = true;
        //        MyFunctie.Startdatum = inschrijving.Startdatum;
        //        MyContact = new Contact();
        //        MyContact.ID = 0;
        //        MyContact.Voornaam = inschrijving.Voornaam;
        //        MyContact.Achternaam = inschrijving.Achternaam;
        //        MyContact.Aanspreking = inschrijving.Geslacht;
        //        TaalServices tService = new TaalServices();
        //        Taal taal = tService.GetTaalByNameOrCode("NL");
        //        if (taal != null)
        //        {
        //            MyContact.Taal_ID = taal.ID;
        //            MyContact.Taal = taal;
        //        }

        //    }

        //    // bedrijven
        //    websiteBedrijfLabel.Content = inschrijving.Bedrijfsnaam + ", " + inschrijving.BTWNummer + ", " + inschrijving.Adres + ", " + inschrijving.Postcode + ", " + inschrijving.Gemeente;
        //    if (MyFunctie.Bedrijf != null)
        //    {
        //        MyBedrijf = MyFunctie.Bedrijf;
        //        if (MyBedrijf != null)
        //        {
        //            bedrijfVoorstelLabel.Content = MyBedrijf.Naam + ", " + MyBedrijf.Ondernemersnummer + ", " + MyBedrijf.Adres + ", " + MyBedrijf.Postcode + ", " + inschrijving.Gemeente;
        //            BedrijfComboBox.Text = MyBedrijf.NaamEnBtwNummer;
        //            if (string.IsNullOrEmpty(MyBedrijf.Ondernemersnummer))
        //            {
        //                if (!string.IsNullOrEmpty(_inschrijving.BTWNummer))
        //                    MyBedrijf.Ondernemersnummer = _inschrijving.BTWNummer;
        //            }
        //            List<Bedrijf> mybedrijven = new List<Bedrijf>();
        //            mybedrijven.Add(MyBedrijf);
        //            BedrijfComboBox.ItemsSource = mybedrijven;
        //            BedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
        //            BedrijfComboBox.SelectedIndex = 0;
        //            MyFunctie.Bedrijf = MyBedrijf;
        //        }
        //    }
        //    else if (bedrijven.Count > 0)
        //    {
        //        MyBedrijf = bedrijven[0];
        //        if (MyBedrijf != null)
        //        {
        //            bedrijfVoorstelLabel.Content = MyBedrijf.Naam + ", " + MyBedrijf.Ondernemersnummer + ", " + MyBedrijf.Adres + ", " + MyBedrijf.Postcode + ", " + inschrijving.Gemeente;
        //            BedrijfComboBox.Text = MyBedrijf.NaamEnBtwNummer;
        //            List<Bedrijf> mybedrijven = new List<Bedrijf>();
        //            mybedrijven.Add(MyBedrijf);
        //            BedrijfComboBox.ItemsSource = mybedrijven;
        //            BedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
        //            BedrijfComboBox.SelectedIndex = 0;
        //            MyFunctie.Bedrijf = MyBedrijf;
        //        }
        //    }
        //    else
        //    {
        //        MyBedrijf = new Bedrijf();
        //        MyBedrijf.ID = 0;
        //        MyBedrijf.Naam = inschrijving.Bedrijfsnaam;
        //        MyBedrijf.Ondernemersnummer = inschrijving.BTWNummer;
        //        MyBedrijf.Adres = inschrijving.Adres;
        //        Postcodes postcode = new PostcodeService().GetPostcodeByIDAndCity(inschrijving.Postcode, inschrijving.Gemeente);
        //        if (postcode != null)
        //        {
        //            MyBedrijf.Zipcode = postcode.ID;
        //            MyBedrijf.Postcode = postcode.Postcode;
        //            MyBedrijf.Plaats = postcode.Gemeente;
        //            MyBedrijf.Provincie = postcode.Provincie;
        //            MyBedrijf.Land = postcode.Land;
        //        }
        //        else
        //        {
        //            MyBedrijf.Postcode = inschrijving.Postcode;
        //            MyBedrijf.Plaats = inschrijving.Gemeente;
        //        }
        //        MyBedrijf.Telefoon = inschrijving.Telefoon;
        //        MyBedrijf.Fax = inschrijving.Fax;
        //        MyFunctie.Bedrijf = MyBedrijf;
        //    }

        //    // Mainform
        //    mainInschrijvingControl.Notitie = inschrijving.Opmerking;
        //    if (!string.IsNullOrEmpty(inschrijving.Tav))
        //        mainInschrijvingControl.Notitie += "\nTav: " + inschrijving.Tav;
        //    if (!string.IsNullOrEmpty(inschrijving.Erkenningsnummer))
        //        mainInschrijvingControl.Notitie += "\nErkenningsnummer: " + inschrijving.Erkenningsnummer;

        //    // facturatiegegevens
        //    facturatieBedrijfLabel.Content = inschrijving.FacturatieBedrijfsNaam + ", " + inschrijving.FacturatieBTWNummer + " Tel: " + inschrijving.FacturatieTelefoon;
        //    List<Bedrijf> facturatiebedrijven = bService.GetBedrijfBySearchTerm(inschrijving.FacturatieBedrijfsNaam);
        //    if (facturatiebedrijven.Count > 0)
        //    {
        //        var query = (from b in facturatiebedrijven where b.Postcode == _inschrijving.FacturatiePostcode select b);
        //        Bedrijf temp = query.FirstOrDefault();
        //        if (temp != null)
        //            if (temp.ID == MyBedrijf.ID)
        //            {
        //                MyFacturatieOp = null;
        //                if (currentInschrijving != null)
        //                    currentInschrijving.Facturatie_op_ID = null;
        //            }
        //            else
        //                MyFacturatieOp = temp;
        //        else
        //            MyFacturatieOp = facturatiebedrijven[0];

        //        if (MyFacturatieOp != null)
        //        {
        //            if (MyFacturatieOp.ID == MyBedrijf.ID)
        //            {
        //                MyFacturatieOp = null;
        //                if (currentInschrijving != null)
        //                    currentInschrijving.Facturatie_op_ID = null;
        //            }
        //            else
        //            {
        //                facturatieVoorstelBedrijfLabel.Content = MyFacturatieOp.Naam + ", " + MyFacturatieOp.Ondernemersnummer + " Tel: " + MyFacturatieOp.Telefoon;
        //                Bedrijf leeg = new Bedrijf();
        //                leeg.Naam = "";
        //                facturatiebedrijven.Insert(0, leeg);
        //                facturatieBedrijfComboBox.ItemsSource = facturatiebedrijven;
        //                facturatieBedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
        //                if (temp != null)
        //                    facturatieBedrijfComboBox.Text = temp.NaamEnAdres;
        //                else
        //                    facturatieBedrijfComboBox.Text = MyFacturatieOp.NaamEnAdres;
        //            }
        //        }
        //    }

        //    SetVerantwoordelijken(inschrijving);
        //    CheckInput();
        //}

            /// <summary>
            /// Functie die de functie opzoekt als deze bestaat of een nieuwe contact laat aanmaken.
            /// 
            /// </summary>
            /// <param name="inschrijving"></param>
        private void LoadFunctionsAsync(WebsiteInschrijving inschrijving)
        {
            List<Functies> functies = inschrijving.GetFuncties();
            int index = contactFunctieComboBox.SelectedIndex;
            contactFunctieComboBox.ItemsSource = functies;
            contactFunctieComboBox.DisplayMemberPath = "NaamBedrijfFunctie";
            contactFunctieComboBox.SelectedIndex = index;
            websiteBedrijfLabel.Content = inschrijving.Bedrijfsnaam + ", " + inschrijving.BTWNummer + ", " + inschrijving.Adres + ", " + inschrijving.Postcode + ", " + inschrijving.Gemeente;
            List<Bedrijf> bedrijven = GetBedrijvenAsync(inschrijving);
            BedrijfComboBox.ItemsSource = bedrijven;
            BedrijfComboBox.DisplayMemberPath = "NaamEnBtwNummer";
            if (functies.Count > 0)
            {
                MyFunctie = functies[0];
                if (MyFunctie != null)
                {
                    contactVoorstelNaamLabel.Content = MyFunctie.Contact.Achternaam + " " + MyFunctie.Contact.Voornaam + ", " + MyFunctie.Bedrijf.Naam + " email: " + MyFunctie.Email;
                    MyContact = MyFunctie.Contact;
                    contactFunctieComboBox.Text = MyFunctie.NaamBedrijfFunctie;
                    NaamComboBox_DropDownClosed(contactFunctieComboBox, new EventArgs());
                }
            }
            else
            {
                MyFunctie = new Functies();
                MyFunctie.ID = 0;
                MyFunctie.Email = inschrijving.Email;
                MyFunctie.Oorspronkelijke_Titel = inschrijving.FunctieNaam;
                MyFunctie.Fax = true;
                MyFunctie.Mail = true;
                MyFunctie.Startdatum = inschrijving.Startdatum;
                MyContact = new Contact();
                MyContact.ID = 0;
                MyContact.Voornaam = inschrijving.Voornaam;
                MyContact.Achternaam = inschrijving.Achternaam;
                MyContact.Aanspreking = inschrijving.Geslacht;
                TaalServices tService = new TaalServices();
                Taal taal = tService.GetTaalByNameOrCode("NL");
                if (taal != null)
                {
                    MyContact.Taal_ID = taal.ID;
                    MyContact.Taal = taal;
                }
                // bedrijf
                if (MyFunctie.Bedrijf != null)
                {
                    MyBedrijf = MyFunctie.Bedrijf;
                    if (MyBedrijf != null)
                    {
                        //bedrijfVoorstelLabel.Content = MyBedrijf.Naam + ", " + MyBedrijf.Ondernemersnummer + ", " + MyBedrijf.Adres + ", " + MyBedrijf.Postcode + ", " + inschrijving.Gemeente;
                        BedrijfComboBox.Text = MyBedrijf.NaamEnBtwNummer;
                        if (string.IsNullOrEmpty(MyBedrijf.Ondernemersnummer))
                        {
                            if (!string.IsNullOrEmpty(_inschrijving.BTWNummer))
                                MyBedrijf.Ondernemersnummer = _inschrijving.BTWNummer;
                        }
                        List<Bedrijf> mybedrijven = new List<Bedrijf>();
                        mybedrijven.Add(MyBedrijf);
                        BedrijfComboBox.ItemsSource = mybedrijven;
                        BedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
                        BedrijfComboBox.SelectedIndex = 0;
                        MyFunctie.Bedrijf = MyBedrijf;
                    }
                }
                else if (bedrijven.Count > 0)
                {
                    MyBedrijf = bedrijven[0];
                    if (MyBedrijf != null)
                    {
                        bedrijfVoorstelLabel.Content = MyBedrijf.Naam + ", " + MyBedrijf.Ondernemersnummer + ", " + MyBedrijf.Adres + ", " + MyBedrijf.Postcode + ", " + inschrijving.Gemeente;
                        BedrijfComboBox.Text = MyBedrijf.NaamEnBtwNummer;
                        List<Bedrijf> mybedrijven = new List<Bedrijf>();
                        mybedrijven.Add(MyBedrijf);
                        BedrijfComboBox.ItemsSource = mybedrijven;
                        BedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
                        BedrijfComboBox.SelectedIndex = 0;
                        MyFunctie.Bedrijf = MyBedrijf;
                    }
                }
                else
                {
                    MyBedrijf = new Bedrijf();
                    MyBedrijf.ID = 0;
                    MyBedrijf.Naam = inschrijving.Bedrijfsnaam;
                    MyBedrijf.Ondernemersnummer = inschrijving.BTWNummer;
                    MyBedrijf.Adres = inschrijving.Adres;
                    Postcodes postcode = new PostcodeService().GetPostcodeByIDAndCity(inschrijving.Postcode, inschrijving.Gemeente);
                    if (postcode != null)
                    {
                        MyBedrijf.Zipcode = postcode.ID;
                        MyBedrijf.Postcode = postcode.Postcode;
                        MyBedrijf.Plaats = postcode.Gemeente;
                        MyBedrijf.Provincie = postcode.Provincie;
                        MyBedrijf.Land = postcode.Land;
                    }
                    else
                    {
                        MyBedrijf.Postcode = inschrijving.Postcode;
                        MyBedrijf.Plaats = inschrijving.Gemeente;
                    }
                    MyBedrijf.Telefoon = inschrijving.Telefoon;
                    MyBedrijf.Fax = inschrijving.Fax;
                    MyFunctie.Bedrijf = MyBedrijf;

                }
                SetVerantwoordelijken(inschrijving);
            }
        }

        /// <summary>
        /// Functie om de verantwoordelijk in te stellen als deze aanwezig zijn in de inschrijving
        /// </summary>
        /// <param name="inschrijving"></param>
        private void SetVerantwoordelijken(WebsiteInschrijving inschrijving)
        {
            inschrijving.SetVerantwoordelijkeOpleiding(MyBedrijf);
            inschrijving.SetVerantwoordelijkePersoneel(MyBedrijf);
            if (inschrijving.Opleiding != null)
            {
                opleidingNaamLabel.Content = inschrijving.Opleiding.Contact.Voornaam + " " + inschrijving.Opleiding.Contact.Achternaam;
                opleidingEmailLabel.Content = inschrijving.Opleiding.Email;
                MyVerantwoordelijkeOpleiding = inschrijving.Opleiding;
            }
            else
            {
                opleidingNaamLabel.Content = inschrijving.VerantwoordelijkeOpleiding;
                opleidingEmailLabel.Content = inschrijving.VerantwoordelijkeOpleidingMail;
            }
        }

        private List<Functies> LoadFunctions(WebsiteInschrijving inschrijving)
        {
            List<Functies> functies = inschrijving.GetFuncties();
            int index = contactFunctieComboBox.SelectedIndex;
            contactFunctieComboBox.ItemsSource = functies;
            contactFunctieComboBox.DisplayMemberPath = "NaamBedrijfFunctie";
            contactFunctieComboBox.SelectedIndex = index;
            return functies;
        }

        /// <summary>
        /// Functie die de XML weergeeft aan de rechter kant.
        /// </summary>
        /// <param name="inschrijving"></param>
        /// <returns></returns>
        private string SetXmlToTextBlock(WebsiteInschrijving inschrijving)
        {
            //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { XMLTextBlock.SetValue(TextBlock.TextProperty, ""); }, null);
            //XMLTextBlock.Text = "";
            string xml = "";
            xml += "Deelnemer\n";
            xml += "\tVoornaam: " + inschrijving.Voornaam + "\n";
            xml += "\tAchternaam: " + inschrijving.Achternaam + "\n";
            xml += "\tFunctie: " + inschrijving.FunctieNaam + "\n";
            xml += "\tGeslacht: " + inschrijving.Geslacht + "\n";
            xml += "\tEmail: " + inschrijving.Email + "\n";
            xml += "\tErkenningsnummer: " + inschrijving.Erkenningsnummer + "\n";
            xml += "Bedrijf\n";
            xml += "\tNaam: " + inschrijving.Bedrijfsnaam + "\n";
            xml += "\tBTWnummer: " + inschrijving.BTWNummer + "\n";
            xml += "\tAdres: " + inschrijving.Adres + "\n";
            xml += "\tPostcode: " + inschrijving.Postcode + "\n";
            xml += "\tGemeente: " + inschrijving.Gemeente + "\n";
            xml += "\tTelefoon: " + inschrijving.Telefoon + "\n";
            xml += "\tFax: " + inschrijving.Fax + "\n";
            xml += "Facturatie\n";
            xml += "\tNaam: " + inschrijving.Bedrijfsnaam + "\n";
            xml += "\tBTWnummer: " + inschrijving.BTWNummer + "\n";
            xml += "\tAdres: " + inschrijving.Adres + "\n";
            xml += "\tPostcode: " + inschrijving.Postcode + "\n";
            xml += "\tGemeente: " + inschrijving.Gemeente + "\n";
            xml += "\tTav: " + inschrijving.Tav + "\n";
            xml += "Communicatie\n";
            xml += "\tVoornaam: " + inschrijving.CommunicatieVoornaam + "\n";
            xml += "\tNaam: " + inschrijving.CommunicatieAchternaam + "\n";
            xml += "\tEmail: " + inschrijving.CommunicatieEmail + "\n";
            xml += "\tFunctie: " + inschrijving.CommunicatieFunctie + "\n";
            xml += "Inschrijving\n";
            xml += "\tOpmerking: " + _inschrijving.Opmerking + "\n";
            xml += "\tDatum: " + _inschrijving.InschrijvingsDatum + "\n";
            int i = 1;
            foreach (WebSessie sessie in inschrijving.websessies)
            {
                xml += "Sessie\n";
                xml += "\tID: " + sessie.Sessie_Id + "\n";
                xml += "\tNaam: " + sessie.SessieNaam + "\n";
                i++;
            }
            return xml;
            //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { XMLTextBlock.SetValue(TextBlock.TextProperty, xml); }, null);
            //XMLTextBlock.Text = xml;
        }

        /// <summary>
        /// Event om het openen en sluiten van de verschillende panels
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContactPanelToggleButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            RowDefinition row = new RowDefinition();
            double closedSize = 27;
            double openSize = 125;
            switch (button.Name)
            {
                case "ContactPanelToggleButton":
                    row = contactRow;
                    break;
                case "BedrijfPanelToggleButton":
                    row = bedrijfRow;
                    break;
                case "inschrijvingPanelToggleButton":
                    row = inschrijvingRow;
                    openSize = 225;
                    break;
                case "FacturatiePanelToggleButton":
                    row = facturatieRow;
                    break;
                case "VerantwoordelijkePanelToggleButton":
                    row = varantwoordelijkeRow;
                    break;
                default:
                    break;
            }
            if (button.Content.ToString() == "Sluiten")
            {
                row.Height = new GridLength(closedSize);
                button.Content = "Openen";
                //seminarieScrollViewer.Height = seminarieRow.ActualHeight + openSize - closedSize - 30;
            }
            else
            {
                row.Height = new GridLength(openSize);
                button.Content = "Sluiten";
                //seminarieScrollViewer.Height = seminarieRow.ActualHeight - openSize;
            }
        }

        //private void SetSeminariesAsync(decimal? functieid)
        //{
        //    try
        //    {
        //        seminarieStackPanel.Children.Clear();
        //        currentInschrijving = null;
        //        DateTime evalDate = DateTime.Today.AddDays(-21);
        //        foreach (Seminarie sem in _seminaries)
        //        {
        //            if (sem.Sessie.Count > 0)
        //            {
        //                foreach (Sessie sessie in sem.Sessie)
        //                {
        //                    //todo terug activeren om seminaries uit het verleden niet meer te tonen
        //                    if (sessie.Datum >= evalDate)
        //                    {
        //                        if (functieid == null)
        //                            CreateSeminarieCheckBox(sem, null);
        //                        else
        //                            CreateSeminarieCheckBox(sem, functieid);
        //                        break;
        //                    }
        //                }
        //            }
        //        }
        //        StopLoader();
        //    }
        //    catch (Exception ex)
        //    {
        //        ConfocusMessageBox.Show("Fout bij het controleren en plaatsen vande Seminaries\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
        //    }
        //}

            /// <summary>
            /// Functie om de seminarie checkboxen aan te maken
            /// </summary>
            /// <param name="functieid"></param>
        private void SetSeminarieNew(decimal? functieid)
        {
            try
            {
                seminarieStackPanel.Children.Clear();
                currentInschrijving = null;
                var gevraagdeSessie = (from s in _sessies where s.ID.ToString() == _inschrijving.websessies[0].Sessie_Id select s).FirstOrDefault();
                if (gevraagdeSessie != null)
                {
                    var seminaries = (from s in _sessies group s by s.Seminarie_ID into g select g).ToList();
                    foreach (var sem in seminaries)
                    {
                        if (functieid == null)
                            CreateSeminarieNewCheckBox(sem, null);
                        else
                            CreateSeminarieNewCheckBox(sem, functieid);
                    }
                }
                else
                {
                    ConfocusMessageBox.Show("De gevraagde sessie is niet actief!!!", ConfocusMessageBox.Kleuren.Rood);
                    geldigeInschrijving = false;
                }
                StopLoader();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het controleren en plaatsen vande Seminaries\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void CreateSeminarieNewCheckBox(IGrouping<decimal, InschrijvingSessie> sem, decimal? functieid)
        {
            try
            {
                StackPanel panel = new StackPanel();
                InschrijvingSessie first = (InschrijvingSessie)sem.First();
                Functies fullFunctie = null;
                if (functieid != null)
                    fullFunctie = new FunctieServices().GetFunctieByID((decimal)functieid);
                CheckBox box = new CheckBox();
                box.Name = "SeminarieCheckBox_" + first.Seminarie_ID.ToString();
                box.Content = first.SeminarieNaam;
                box.IsChecked = false;
                box.Margin = new Thickness(10, 5, 0, 5);
                box.Click += SeminarieCheckBox_click;
                StackPanel seminarieChildrenStackPanel = new StackPanel();
                seminarieChildrenStackPanel.Name = "SeminarieChildPanel_" + first.Seminarie_ID.ToString();
                seminarieChildrenStackPanel.Visibility = Visibility.Collapsed;
                bool hasActiveSessions = false;
                    DateTime evalDate = DateTime.Today.AddDays(-21);
                foreach (var sessie in sem)
                {
                    //todo terug activeren om seminaries uit het verleden niet meer te tonen
                    if (sessie.Actief == true)
                    {
                        hasActiveSessions = true;
                        DateTime datum = sessie.Datum == null ? DateTime.Now : (DateTime)sessie.Datum;
                        string reversedatum = datum.Year + "/" + (datum.Month < 10 ? ("0" + datum.Month.ToString()) : datum.Month.ToString()) + "/" + (datum.Day < 10 ? "0" + datum.Day.ToString() : datum.Day.ToString());
                        bool ingeschreven = false;
                        // Check of deze persoon reeds is ingeschreven voor deze sessie
                        ingeschreven = DBHelper.CheckInschrijving(sessie.ID, functieid, fullFunctie.Contact_ID);
                        // Check of deze inschrijving een nieuwe is 
                        bool nieuweInschrijving = false;
                        foreach (WebSessie websessie in _inschrijving.websessies)
                        {
                            //string websessienaam = websessie.SessieNaam.Split(':')[1].Split('-')[0].ToLower();
                            if (!String.IsNullOrEmpty(websessie.Sessie_Id))
                            {
                                if (websessie.Sessie_Id == sessie.ID.ToString())
                                {
                                    nieuweInschrijving = true;
                                }
                            }
                            //else if (sessie.Naam.ToLower().Contains(websessienaam) && reversedatum == websessie.Sessiedatum)
                            //    nieuweInschrijving = true;

                        }

                        CheckBox sesbox = new CheckBox();
                        sesbox.Name = "SessieCheckBox_" + sessie.ID.ToString();
                        sesbox.Content = sessie.NaamDatumLocatie;
                        sesbox.Margin = new Thickness(40, 5, 0, 5);
                        sesbox.IsChecked = ingeschreven || nieuweInschrijving;
                        sesbox.IsEnabled = !ingeschreven;
                        if (sessie.Status == "Geannuleerd")
                            sesbox.Foreground = new SolidColorBrush(Colors.Red);
                        else if (sessie.Status == "Actief niet online")
                            sesbox.Foreground = new SolidColorBrush(Colors.Gray);
                        else if (sessie.Status == "Verplaatst")
                            sesbox.Foreground = new SolidColorBrush(Colors.Blue);
                        else
                            sesbox.Foreground = new SolidColorBrush(Colors.Black);
                        sesbox.Click += CheckBox_click;
                        StackPanel invoerStackPanel = new StackPanel();
                        if (ingeschreven)
                        {
                            box.IsChecked = true;
                            Label label = new Label();
                            label.Name = sesbox.Name + "Label";
                            label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                            label.Content = "Reeds ingeschreven.";
                            label.Margin = new Thickness(70, 5, 5, 5);
                            invoerStackPanel.Children.Add(label);
                        }
                        invoerStackPanel.Name = sesbox.Name + "Panel";
                        invoerStackPanel.Orientation = Orientation.Vertical;
                        seminarieChildrenStackPanel.Children.Add(sesbox);
                        seminarieChildrenStackPanel.Children.Add(invoerStackPanel);

                        if (nieuweInschrijving)
                        {
                            box.IsChecked = true;
                            CheckBox_click(sesbox, new RoutedEventArgs());
                        }
                    }
                }
                if (hasActiveSessions)
                {
                    panel.Children.Add(box);
                    panel.Children.Add(seminarieChildrenStackPanel);
                    seminarieStackPanel.Children.Add(panel);
                    if (box.IsChecked == true)
                        SeminarieCheckBox_click(box, new RoutedEventArgs());
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("fout bij het aanmaken van de Checkboxen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void CreateSeminarieCheckBox(Seminarie sem, decimal? functieid)
        {
            try
            {
                StackPanel panel = new StackPanel();
                Functies fullFunctie = null;
                if (functieid != null)
                    fullFunctie = new FunctieServices().GetFunctieByID((decimal)functieid);
                CheckBox box = new CheckBox();
                box.Name = "SeminarieCheckBox_" + sem.ID.ToString();
                box.Content = sem.Titel;
                box.IsChecked = false;
                box.Margin = new Thickness(10, 5, 0, 5);
                box.Click += SeminarieCheckBox_click;
                StackPanel seminarieChildrenStackPanel = new StackPanel();
                seminarieChildrenStackPanel.Name = "SeminarieChildPanel_" + sem.ID.ToString();
                seminarieChildrenStackPanel.Visibility = Visibility.Collapsed;
                bool hasActiveSessions = false;
                if (sem.Sessie != null)
                {
                    var sessies = (from s in sem.Sessie
                                   orderby s.Datum
                                   select s).ToList();
                    DateTime evalDate = DateTime.Today.AddDays(-21);
                    foreach (Sessie sessie in sessies)
                    {
                        //todo terug activeren om seminaries uit het verleden niet meer te tonen
                        if (sessie.Actief == true && sessie.Datum >= evalDate/* && sessie.Status != "Geannuleerd"*/)
                        {
                            hasActiveSessions = true;
                            SessieServices sServices = new SessieServices();
                            //Sessie sessie = sessie;//sServices.GetSessieByID(sessie.ID);

                            DateTime datum = sessie.Datum == null ? DateTime.Now : (DateTime)sessie.Datum;
                            string reversedatum = datum.Year + "/" + (datum.Month < 10 ? ("0" + datum.Month.ToString()) : datum.Month.ToString()) + "/" + (datum.Day < 10 ? "0" + datum.Day.ToString() : datum.Day.ToString());
                            bool ingeschreven = false;
                            bool nieuweInschrijving = false;
                            foreach (WebSessie websessie in _inschrijving.websessies)
                            {
                                //string websessienaam = websessie.SessieNaam.Split(':')[1].Split('-')[0].ToLower();
                                if (!String.IsNullOrEmpty(websessie.Sessie_Id))
                                {
                                    if (websessie.Sessie_Id == sessie.ID.ToString())
                                    {
                                        nieuweInschrijving = true;
                                    }
                                }
                                //else if (sessie.Naam.ToLower().Contains(websessienaam) && reversedatum == websessie.Sessiedatum)
                                //    nieuweInschrijving = true;

                                foreach (ConfocusDBLibrary.Inschrijvingen inschrijving in sessie.Inschrijvingen)
                                {
                                    if (functieid != null && fullFunctie != null)
                                    {
                                        //ConfocusDBLibrary.Inschrijvingen fullinschrijving = new InschrijvingenService().GetInschrijvingByID(inschrijving.ID);
                                        if (inschrijving.Functies.Contact_ID == fullFunctie.Contact_ID)
                                        {
                                            if (inschrijving.Actief == true)
                                            {
                                                ingeschreven = true;
                                                if (websessie.Sessie_Id == sessie.ID.ToString())
                                                {
                                                    currentInschrijving = inschrijving;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            CheckBox sesbox = new CheckBox();
                            sesbox.Name = "SessieCheckBox_" + sessie.ID.ToString();
                            sesbox.Content = sessie.NaamDatumLocatie;
                            sesbox.Margin = new Thickness(40, 5, 0, 5);
                            sesbox.IsChecked = ingeschreven || nieuweInschrijving;
                            sesbox.IsEnabled = !ingeschreven;
                            if (sessie.Status == "Geannuleerd")
                                sesbox.Foreground = new SolidColorBrush(Colors.Red);
                            else if (sessie.Status == "Actief niet online")
                                sesbox.Foreground = new SolidColorBrush(Colors.Gray);
                            else if (sessie.Status == "Verplaatst")
                                sesbox.Foreground = new SolidColorBrush(Colors.Blue);
                            else
                                sesbox.Foreground = new SolidColorBrush(Colors.Black);
                            sesbox.Click += CheckBox_click;
                            StackPanel invoerStackPanel = new StackPanel();
                            if (ingeschreven)
                            {
                                box.IsChecked = true;
                                Label label = new Label();
                                label.Name = sesbox.Name + "Label";
                                label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                label.Content = "Reeds ingeschreven.";
                                label.Margin = new Thickness(70, 5, 5, 5);
                                invoerStackPanel.Children.Add(label);
                            }
                            invoerStackPanel.Name = sesbox.Name + "Panel";
                            invoerStackPanel.Orientation = Orientation.Vertical;
                            seminarieChildrenStackPanel.Children.Add(sesbox);
                            seminarieChildrenStackPanel.Children.Add(invoerStackPanel);

                            if (nieuweInschrijving)
                            {
                                box.IsChecked = true;
                                CheckBox_click(sesbox, new RoutedEventArgs());
                            }
                        }
                    }
                }
                if (hasActiveSessions)
                {
                    panel.Children.Add(box);
                    panel.Children.Add(seminarieChildrenStackPanel);
                    seminarieStackPanel.Children.Add(panel);
                    if (box.IsChecked == true)
                        SeminarieCheckBox_click(box, new RoutedEventArgs());
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("fout bij het aanmaken van de Checkboxen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Click Event voor het openen en sluiten van de seminarie panels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SeminarieCheckBox_click(object sender, RoutedEventArgs e)
        {
            CheckBox check = (CheckBox)sender;
            bool gevonden = false;
            foreach (object obj in seminarieStackPanel.Children)
            {
                if (obj is Panel)
                {
                    Panel myPanel = (Panel)obj;
                    int index = 0;
                    foreach (object myobj in myPanel.Children)
                    {
                        if (myobj is CheckBox)
                        {
                            CheckBox mycheck = (CheckBox)myobj;
                            if (mycheck.Name == check.Name)
                            {
                                Panel panel = (Panel)myPanel.Children[index + 1];
                                if (check.IsChecked == true)
                                {
                                    panel.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    panel.Visibility = Visibility.Collapsed;
                                }
                                gevonden = true;
                                break;
                            }
                        }
                        index++;
                    }
                }
                if (gevonden)
                    break;
            }
        }

        /// <summary>
        /// Click Event voor het openen en sluiten van de sessie checkboxen 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBox_click(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (CheckBox)sender;
            string[] namen = checkbox.Name.Split('_');
            string rootname = namen[0];
            Panel panel = checkbox.Parent as Panel;
            if (rootname == "SeminarieCheckBox")
            {
                if (checkbox.IsChecked == true)
                {
                    bool hasSavedItems = false;
                    string panelname = "";
                    foreach (object obj in panel.Children)
                    {
                        if (obj is CheckBox)
                        {
                            CheckBox mycheck = obj as CheckBox;
                            if (mycheck.Name != checkbox.Name)
                            {
                                if (mycheck.IsEnabled == true)
                                {
                                    mycheck.IsChecked = true;
                                    hasSavedItems = false;
                                }
                                else
                                    hasSavedItems = true;

                                panelname = mycheck.Name + "Panel";
                            }
                            else
                                continue;
                        }
                        else if (obj is StackPanel)
                        {
                            if (!hasSavedItems)
                            {
                                StackPanel thePanel = (StackPanel)obj;
                                if (thePanel.Name == panelname)
                                {
                                    //SetDeelnemerControls(thePanel);
                                    hasSavedItems = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool hasSavedItems = false;
                    string panelname = "";
                    foreach (object obj in panel.Children)
                    {

                        if (obj is CheckBox)
                        {

                            CheckBox mycheck = obj as CheckBox;
                            if (mycheck.Name != checkbox.Name)
                            {
                                if (mycheck.IsEnabled == true)
                                {
                                    mycheck.IsChecked = false;
                                    hasSavedItems = false;


                                }
                                else
                                    hasSavedItems = true;

                                panelname = mycheck.Name + "Panel";
                            }
                            else
                                continue;
                        }
                        else if (obj is StackPanel)
                        {
                            if (!hasSavedItems)
                            {
                                StackPanel mypanel = obj as StackPanel;
                                if (mypanel.Name == panelname)
                                {
                                    mypanel.Children.Clear();
                                    hasSavedItems = false;
                                }
                            }
                        }
                    }
                }
            }
            else if (rootname == "SessieCheckBox")
            {
                if (checkbox.IsChecked == false)
                {
                    // controle als er nog boxen gechecked zijn.
                    bool nogchecked = false;
                    foreach (object item in panel.Children)
                    {
                        if (item is CheckBox)
                        {
                            CheckBox myCheck = item as CheckBox;
                            string[] checkNamen = myCheck.Name.Split('_');
                            string naam = checkNamen[0];
                            if (naam == "SessieCheckBox")
                            {
                                nogchecked = true;
                                break;
                            }
                        }
                    }


                    string panelName = checkbox.Name + "Panel";
                    foreach (object obj in panel.Children)
                    {
                        if (obj is StackPanel)
                        {
                            StackPanel thepanel = (StackPanel)obj;
                            if (thepanel.Name == panelName)
                            {
                                thepanel.Children.Clear();
                                break;
                            }
                        }
                    }

                    if (!nogchecked)
                    {
                        foreach (object obj in panel.Children)
                        {
                            if (obj is CheckBox)
                            {
                                CheckBox myCheck = obj as CheckBox;
                                string[] checkNamen = myCheck.Name.Split('_');
                                string naam = checkNamen[0];
                                if (naam == "SeminarieCheckBox")
                                {
                                    myCheck.IsChecked = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (checkbox.IsChecked == true)
                {
                    foreach (object obj in panel.Children)
                    {
                        if (obj is CheckBox)
                        {
                            CheckBox myCheck = obj as CheckBox;
                            string[] checkNamen = myCheck.Name.Split('_');
                            string naam = checkNamen[0];
                            if (naam == "SeminarieCheckBox")
                            {
                                myCheck.IsChecked = true;
                                break;
                            }
                        }
                    }
                    string panelName = checkbox.Name + "Panel";
                    foreach (var item in panel.Children)
                    {
                        if (item is StackPanel)
                        {
                            StackPanel thePanel = (StackPanel)item;
                            if (thePanel.Name == panelName)
                            {
                                if (thePanel.Children.Count == 0)
                                {
                                    string sesboxnaam = checkbox.Name;
                                    //sesboxnaam += "Nieuwe inschrijving";
                                    WebInschrijvingUserControl control = new WebInschrijvingUserControl();
                                    control.Name = sesboxnaam + "_InschrijvingControl";
                                    control.Margin = new Thickness(73, 0, 0, 0);
                                    thePanel.Children.Add(control);
                                }
                                else
                                {
                                    string sessieidstr = checkbox.Name.Split('_')[1];
                                    decimal sessieid = 0;
                                    decimal.TryParse(sessieidstr, out sessieid);
                                    ConfocusDBLibrary.Inschrijvingen inschr = new InschrijvingenService().GetInschrijvingByFunctieIDAndSessieID(MyFunctie.ID, sessieid);
                                    if (inschr != null)
                                    {
                                        if (inschr.Status == "Inschrijving")
                                            ConfocusMessageBox.Show("Het contact is reeds ingeschreven voor deze sessie!" + (inschr == null ? "" : "\n Status: " + inschr.Status), ConfocusMessageBox.Kleuren.Rood);
                                        else if (inschr.Status == "Interesse")
                                        {
                                            inschr.Status = "Inschrijving";
                                            new InschrijvingenService().SaveInschrijvingCHanges(inschr);
                                            ConfocusMessageBox.Show("De status van deze inschrijving is bijgewerkt naar Inschrijving", ConfocusMessageBox.Kleuren.Blauw);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void NaamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            SetFunctie(sender);
        }

        private void NaamComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.LineFeed)
            {
                SetFunctie(sender);
            }
        }

        private void SetFunctie(object sender)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Functies functie = (Functies)box.SelectedItem;
                MyFunctie = functie;
                SetFunctieToForm(false);
                CheckInput();
            }
        }

        private void LoadVoornamen(string naam)
        {
            if (naam != "")
            {
                ContactServices cService = new ContactServices();
                List<string> voornamen = cService.GetDictinctVoornaamByAchternaam(naam);
                //contactVoornaamComboBox.ItemsSource = voornamen;
            }
        }

        private void VoornaamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            //SetControls(box);

        }

        private void CloseAllButton_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            foreach (RowDefinition row in closebleGrid.RowDefinitions)
            {

                if (row.Name.Contains("Row") && i < closebleGrid.RowDefinitions.Count - 1)
                    row.Height = new GridLength(27);
                i++;
            }
            SetButtonName("Openen");
            //seminarieScrollViewer.Height = seminarieRow.ActualHeight + 462;
        }

        private void SetButtonName(string v)
        {
            ContactPanelToggleButton.Content = v;
            BedrijfPanelToggleButton.Content = v;
            inschrijvingPanelToggleButton.Content = v;
            FacturatiePanelToggleButton.Content = v;
        }

        private void OpenAllButton_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            foreach (RowDefinition row in closebleGrid.RowDefinitions)
            {
                if (row.Name.Contains("Row") && i < closebleGrid.RowDefinitions.Count - 1)
                {
                    if (row.Name == "inschrijvingRow")
                    {
                        row.Height = new GridLength(225);
                    }
                    else if (row.Name == "varantwoordelijkeRow")
                    {
                        row.Height = new GridLength(150);
                    }
                    else
                        row.Height = new GridLength(125);
                }
                i++;
            }
            SetButtonName("Sluiten");
            //if ((seminarieRow.ActualHeight - 498) > 0)
            //    seminarieScrollViewer.Height = seminarieRow.ActualHeight - 498;
        }

        /// <summary>
        /// Nieuw contact aanmaken met de gegevens in de inschrijving.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nieuwContactButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Contact webcontact = GetContactFromInschrijving();
                List<Contact> bestaand = new ContactServices().GetContactenByNamen(webcontact.Voornaam, webcontact.Achternaam);
                if (bestaand.Count > 0)
                {
                    if (MessageBox.Show("Er is reeds een contact met deze naam.\n Wil U toch een nieuw contact maken met dezelfde naam?", "Contact bestaat reeds!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        Bedrijf webbedrijf;
                        try
                        {
                            if (MyFunctie.Bedrijf.ID != 0)
                                webbedrijf = MyFunctie.Bedrijf;
                            else
                                webbedrijf = GetBedrijfFromInschrijving();
                        }
                        catch (Exception)
                        {
                            webbedrijf = GetBedrijfFromInschrijving();
                        }

                        Functies webfunctie = GetFunctieFromInschrijving();

                        if (webcontact != null && webbedrijf != null && webfunctie != null)
                        {
                            NieuwContact NC = new NieuwContact(currentUser, webcontact, webbedrijf, webfunctie);
                            if (NC.ShowDialog() == true)
                            {
                                if (NC.SelectedFunctie != null && NC.SelectedFunctie.ID != 0)
                                {
                                    MyFunctie = NC.SelectedFunctie;
                                    if (MyFunctie != null)
                                    {
                                        MyFunctie = new FunctieServices().GetFunctieByID(MyFunctie.ID);
                                        MyContact = MyFunctie.Contact;
                                        MyBedrijf = MyFunctie.Bedrijf;
                                        SetFunctieToForm(true);
                                        CheckInput();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Bedrijf webbedrijf;
                    try
                    {
                        if (MyFunctie.Bedrijf.ID != 0)
                            webbedrijf = MyFunctie.Bedrijf;
                        else
                            webbedrijf = GetBedrijfFromInschrijving();
                    }
                    catch (Exception)
                    {
                        webbedrijf = GetBedrijfFromInschrijving();
                    }

                    Functies webfunctie = GetFunctieFromInschrijving();

                    if (webcontact != null && webbedrijf != null && webfunctie != null)
                    {
                        NieuwContact NC = new NieuwContact(currentUser, webcontact, webbedrijf, webfunctie);
                        if (NC.ShowDialog() == true)
                        {
                            if (NC.SelectedFunctie != null && NC.SelectedFunctie.ID != 0)
                            {
                                MyFunctie = NC.SelectedFunctie;
                                if (MyFunctie != null)
                                {
                                    MyFunctie = new FunctieServices().GetFunctieByID(MyFunctie.ID);
                                    MyContact = MyFunctie.Contact;
                                    MyBedrijf = MyFunctie.Bedrijf;
                                    SetFunctieToForm(true);
                                    CheckInput();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het verwerken van het contact! \n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private Functies GetFunctieFromInschrijving()
        {
            Functies functie = new Functies();
            functie.ID = 0;
            functie.Oorspronkelijke_Titel = _inschrijving.FunctieNaam;
            functie.Email = _inschrijving.Email;
            functie.Telefoon = _inschrijving.Telefoon;
            functie.Startdatum = DateTime.Today;
            functie.Type = "Prospect";
            functie.Mail = true;
            functie.Fax = true;
            functie.Actief = true;

            return functie;
        }

        private Bedrijf GetBedrijfFromInschrijving()
        {
            Bedrijf bedrijf = new Bedrijf();
            bedrijf.ID = 0;
            bedrijf.Naam = _inschrijving.Bedrijfsnaam;
            bedrijf.Ondernemersnummer = _inschrijving.BTWNummer;
            bedrijf.Adres = _inschrijving.Adres;
            bedrijf.Postcode = _inschrijving.Postcode;
            bedrijf.Plaats = _inschrijving.Gemeente;
            bedrijf.Telefoon = _inschrijving.Telefoon;
            bedrijf.Fax = _inschrijving.Fax;
            bedrijf.Actief = true;

            return bedrijf;
        }

        private Contact GetContactFromInschrijving()
        {
            Contact contact = new Contact();
            contact.ID = 0;
            contact.Voornaam = _inschrijving.Voornaam;
            contact.Achternaam = _inschrijving.Achternaam;
            contact.Aanspreking = _inschrijving.Geslacht;
            TaalServices tService = new TaalServices();
            Taal taal = tService.GetTaalByNameOrCode(_inschrijving.Taal);
            if (taal != null)
                contact.Taal_ID = taal.ID;
            contact.Actief = true;

            return contact;
        }

        private void SetFunctieToForm(bool IsInschrijving)
        {
            try
            {
                FunctieServices fService = new FunctieServices();
                Functies fullFunctie = fService.GetFunctieByID(MyFunctie.ID);
                List<Functies> functies = fService.GetFunctieByContact(fullFunctie.Contact_ID);
                if (fullFunctie != null)
                {
                    //Contact
                    MyContact = fullFunctie.Contact;
                    MyBedrijf = fullFunctie.Bedrijf;
                    contactVoorstelNaamLabel.Content = fullFunctie.Contact.Achternaam + " " + fullFunctie.Contact.Voornaam + ", " + fullFunctie.Bedrijf.Naam;
                    contactFunctieComboBox.ItemsSource = functies;
                    contactFunctieComboBox.DisplayMemberPath = "NaamBedrijfFunctie";
                    contactFunctieComboBox.Text = fullFunctie.NaamBedrijfFunctie;

                    //Bedrijf
                    bedrijfVoorstelLabel.Content = fullFunctie.Bedrijf.Naam + ", " + (fullFunctie.Bedrijf.Ondernemersnummer == null ? "-" : fullFunctie.Bedrijf.Ondernemersnummer) + ", " + (fullFunctie.Bedrijf.Adres == null ? " " : fullFunctie.Bedrijf.Adres) + ", " + (fullFunctie.Bedrijf.Postcode == null ? " " : fullFunctie.Bedrijf.Postcode) + "' " + (fullFunctie.Bedrijf.Plaats == null ? " " : fullFunctie.Bedrijf.Plaats);
                    MyBedrijf = fullFunctie.Bedrijf;
                    List<Bedrijf> bedrijven = new List<Bedrijf>();
                    bedrijven.Add(fullFunctie.Bedrijf);
                    BedrijfComboBox.ItemsSource = bedrijven;
                    BedrijfComboBox.DisplayMemberPath = "NaamEnBtwNummer";
                    BedrijfComboBox.SelectedIndex = 0;

                    if (_inschrijving.Identiek)
                    {
                        MyFacturatieOp = null;// fullFunctie.Bedrijf;
                        if (currentInschrijving != null)
                            currentInschrijving.Facturatie_op_ID = null;
                    }
                    if (IsInschrijving)
                    {
                        SetVerantwoordelijken(_inschrijving);
                    }
                    //SetSeminariesAsync(MyFunctie.ID);
                    SetSeminarieNew(MyFunctie.ID);
                    CheckInput();
                }
                else
                    StopLoader();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het instellen van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }


        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (geldigeInschrijving)
            {

                try
                {
                    bool IsSaved = true;
                    if (currentInschrijving != null)
                    {
                        if (currentInschrijving.Status != "Inschrijving")
                        {
                            if (MyFunctie.Attesttype_ID == null)
                                throw new Exception("Dit contact heeft geen attesttype! Gelieve dit aan te vullen.");

                            currentInschrijving.Status = "Inschrijving";
                            currentInschrijving.Notitie = mainInschrijvingControl.Notitie;
                            currentInschrijving.Via = "Website";
                            currentInschrijving.CC_Email = mainInschrijvingControl.CCEmail;
                            currentInschrijving.Dagtype = mainInschrijvingControl.Dagtype;
                            currentInschrijving.Gewijzigd = DateTime.Now;
                            currentInschrijving.Gewijzigd_door = currentUser.Naam;
                            InschrijvingenService iService = new InschrijvingenService();
                            try
                            {
                                iService.SaveInschrijvingCHanges(currentInschrijving);
                            }
                            catch (Exception ex)
                            {
                                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                        IsSaved = VerwerkNieuweInschrijvingen();

                    }
                    else
                    {
                        IsSaved = VerwerkNieuweInschrijvingen();
                    }
                    if (IsSaved)
                    {
                        ConfocusMessageBox.Show("Inschrijving(en) opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                        MoveInschrijvingNaarVerwerkt();
                        ClearForm();
                    }

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message + "\n" + (ex.InnerException == null ? "" : ex.InnerException.Message), ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                MoveInschrijvingNaarFout();
                ConfocusMessageBox.Show("De inschrijving is verplaatst naar de map Foute FTP op de F schijf.", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private bool CheckInschrijving(decimal functie_ID, decimal sessie_ID)
        {
            ConfocusDBLibrary.Inschrijvingen inschrijving = new InschrijvingenService().GetInschrijvingByFunctieIDAndSessieID(functie_ID, sessie_ID);
            if (inschrijving == null)
            {
                ConfocusMessageBox.Show("Inschrijving kan niet gevonden worden en is mogelijk niet opgeslagen!!!", ConfocusMessageBox.Kleuren.Rood);
                Logger.Logger.Log("ConfocusInschrijvingLog.txt", "Inschrijving voor sessie: " + sessie_ID + " van Functie met ID: " + functie_ID + " is niet opgeslagen!");
                return false;
            }
            return true;
        }

        private bool VerwerkNieuweInschrijvingen()
        {
            bool IsSaved = true;
            if (MyFunctie.ID == 0)
                throw new Exception("Het Contact is niet geregistreerd. Kies eerst een contact!");
            if (MyFunctie.AttestType1 == null)
                throw new Exception("Dit contact heeft geen attesttype! Gelieve dit aan te vullen.");

            List<decimal> sessieIds = GetNieuweSessies();

            foreach (decimal sessieId in sessieIds)
            {
                MyInschrijving = new ConfocusDBLibrary.Inschrijvingen();
                MyInschrijving.Sessie_ID = sessieId;
                string controlname = "SessieCheckBox_" + sessieId + "_InschrijvingControl";
                Seminarie_ErkenningenService sService = new Seminarie_ErkenningenService();
                List<Seminarie_Erkenningen> erkeningen = sService.GetBySessieID(sessieId);
                WebInschrijvingUserControl control = GetInschrijvinhControl(sessieId, controlname);

                if (IsInschrijvingValid())
                {
                    AttestType attesttype = new Attesttype_Services().GetByID((decimal)MyFunctie.Attesttype_ID);
                    MyInschrijving.Functie_ID = MyFunctie.ID;

                    if (control.Status == "")
                        MyInschrijving.Status = mainInschrijvingControl.Status;
                    else
                        MyInschrijving.Status = control.Status;
                    if (control.CCEmail == "")
                        MyInschrijving.CC_Email = mainInschrijvingControl.CCEmail;
                    else
                        MyInschrijving.CC_Email = control.CCEmail;
                    if (control.Notitie == "")
                        MyInschrijving.Notitie = mainInschrijvingControl.Notitie;
                    else
                        MyInschrijving.Notitie = control.Notitie;
                    if (control.Dagtype == "")
                        MyInschrijving.Dagtype = mainInschrijvingControl.Dagtype;
                    else
                        MyInschrijving.Dagtype = control.Dagtype;

                    MyInschrijving.TAantal = "";
                    MyInschrijving.Via = "Website";
                    MyInschrijving.Datum_inschrijving = _inschrijving.InschrijvingsDatum;
                    MyInschrijving.Datum_aangemaakt = DateTime.Now;
                    MyInschrijving.Factuurnummer = "";
                    MyInschrijving.Notitie_factuur = "";
                    MyInschrijving.Aanwezig = false;
                    MyInschrijving.Mail_ontvangen = false;
                    MyInschrijving.Reminder_Verstuurd = false;
                    MyInschrijving.Annulatie_Verstuurd = false;
                    MyInschrijving.Verplaatsing_Verstuurd = false;
                    MyInschrijving.Attest_Verstuurd = false;
                    MyInschrijving.Evaluatie_ontvangen = false;
                    MyInschrijving.Gefactureerd = false;
                    if (MyVerantwoordelijkeOpleiding != null)
                        MyInschrijving.VerantwoordelijkeOpleiding = MyVerantwoordelijkeOpleiding.ID;
                    if (MyVerantwoordelijkePersoneel != null)
                        MyInschrijving.VerantwoordelijkePersoneel = MyVerantwoordelijkePersoneel.ID;
                    MyInschrijving.Aangemaakt = DateTime.Now;
                    MyInschrijving.Aangemaakt_door = currentUser.Naam;
                    MyInschrijving.Actief = true;
                    if (MyFacturatieOp != null && MyFacturatieOp.ID != 0)
                        MyInschrijving.Facturatie_op_ID = MyFacturatieOp.ID;

                    InschrijvingenService iService = new InschrijvingenService();
                    iService.SaveInschrijving(MyInschrijving);
                    IsSaved = CheckInschrijving((decimal)MyInschrijving.Functie_ID, (decimal)MyInschrijving.Sessie_ID);
                    if (IsSaved)
                        MergeAndSaveFunctionAndCompany();
                }
            }
            return IsSaved;
        }

        private void MoveInschrijvingNaarVerwerkt()
        {
            string file = websiteInschrijvingenComboBox.Text;
            string filepath = _dataFolder + @"\Nieuwe FTP\" + file;
            //string[] splitfile = filename.Split('\\');
            //string file = splitfile[splitfile.Length - 1];
            string target = _dataFolder + @"\Verwerkte FTP\" + file;
            File.Move(filepath, target);
            LoadInschrijvingenInComboBox();
        }

        private void MoveInschrijvingNaarFout()
        {
            string file = websiteInschrijvingenComboBox.Text;
            string filepath = _dataFolder + @"\Nieuwe FTP\" + file;
            //string[] splitfile = filename.Split('\\');
            //string file = splitfile[splitfile.Length - 1];
            string target = _dataFolder + @"\Foute FTP\" + file;
            File.Move(filepath, target);
            LoadInschrijvingenInComboBox();
        }

        private void MergeAndSaveFunctionAndCompany()
        {
            if (MyFunctie != null)
            {
                FunctieServices fService = new FunctieServices();
                Functies uptodate = fService.GetFunctieByID(MyFunctie.ID);
                uptodate.Type = "Klant";
                if (!string.IsNullOrEmpty(_inschrijving.Email))
                {
                    if (uptodate.Email != null)
                    {
                        if (uptodate.Email == "" && Helper.CheckEmail(_inschrijving.Email))
                            uptodate.Email = _inschrijving.Email;
                    }
                    else if (Helper.CheckEmail(_inschrijving.Email))
                        uptodate.Email = _inschrijving.Email;
                }
                //if (!string.IsNullOrEmpty(_inschrijving.FunctieNaam))
                //    MyFunctie.Oorspronkelijke_Titel = _inschrijving.FunctieNaam;
                uptodate.Gewijzigd = DateTime.Now;
                uptodate.Gewijzigd_door = currentUser.Naam;

                try
                {
                    fService.SaveFunctieWijzigingen(uptodate);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                CheckIfContactIsCustomer(uptodate);

            }
            if (MyBedrijf != null)
            {
                bool bedrijfChanged = false;
                if (!string.IsNullOrEmpty(_inschrijving.Telefoon) && _inschrijving.Telefoon != "0032")
                {
                    _inschrijving.Telefoon = Helper.CleanPhonenumber(_inschrijving.Telefoon);
                    if (MyBedrijf.Telefoon != _inschrijving.Telefoon)
                    {
                        MyBedrijf.Telefoon = _inschrijving.Telefoon;
                        bedrijfChanged = true;
                    }
                }
                if (string.IsNullOrEmpty(MyBedrijf.Ondernemersnummer))
                {
                    if (!string.IsNullOrEmpty(_inschrijving.BTWNummer))
                    {
                        MyBedrijf.Ondernemersnummer = Helper.CleanVatnumber(_inschrijving.BTWNummer);
                        bedrijfChanged = true;
                    }
                }
                if (bedrijfChanged)
                {
                    MyBedrijf.Gewijzigd_door = currentUser.Naam;
                    MyBedrijf.Gewijzigd = DateTime.Now;
                    BedrijfServices bService = new BedrijfServices();
                    try
                    {
                        bService.SaveBedrijfChanges(MyBedrijf);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private void CheckIfContactIsCustomer(Functies functie)
        {
            try
            {
                if (functie != null)
                {
                    if (functie.Type == "Klant")
                    {
                        FunctieServices fService = new FunctieServices();
                        List<Functies> functies = fService.GetFunctieByContact(functie.Contact_ID);
                        foreach (Functies func in functies)
                        {
                            if (func.Type != "Klant")
                            {
                                func.Type = "Klant";
                                func.Gewijzigd = DateTime.Today;
                                func.Gewijzigd_door = currentUser.Naam;
                                fService.SaveFunctieWijzigingen(func);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het naar Klant zetten van de functies voor dit contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private WebInschrijvingUserControl GetInschrijvinhControl(decimal sessieId, string controlname)
        {
            SessieServices sesService = new SessieServices();
            Sessie sessie = sesService.GetSessieByID(sessieId);
            SeminarieServices semService = new SeminarieServices();
            Seminarie semenarie = semService.GetSeminarieByID(sessie.ID);
            foreach (object item in seminarieStackPanel.Children)
            {
                if (item is StackPanel)
                {
                    StackPanel seminarieChildStackpanel = item as StackPanel;
                    foreach (var sessieItem in seminarieChildStackpanel.Children)
                    {
                        if (sessieItem is StackPanel)
                        {
                            StackPanel sessieStackPanel = sessieItem as StackPanel;
                            foreach (var sessiechildItem in sessieStackPanel.Children)
                            {
                                if (sessiechildItem is StackPanel)
                                {
                                    StackPanel panel = sessiechildItem as StackPanel;
                                    foreach (var obj in panel.Children)
                                    {
                                        if (obj is WebInschrijvingUserControl)
                                        {
                                            WebInschrijvingUserControl control = obj as WebInschrijvingUserControl;
                                            if (control.Name == controlname)
                                                return control;

                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }

            //InschrijvingUserControl control = Helper.FindChild<InschrijvingUserControl>(seminarieStackPanel, controlname);
            return null;

        }

        private bool IsInschrijvingValid()
        {
            bool valid = true;
            string errormessage = "";
            if (MyFunctie == null)
            {
                valid = false;
                errormessage += "De contactgegevens zijn niet correct!\n";
            }

            if (!valid)
            {
                ConfocusMessageBox.Show(errormessage, ConfocusMessageBox.Kleuren.Rood);
            }

            return valid;
        }

        private List<decimal> GetNieuweSessies()
        {
            List<decimal> sessies = new List<decimal>();
            bool sessiegevonden = false;
            foreach (object obj in seminarieStackPanel.Children)
            {
                if (obj is StackPanel)
                {
                    StackPanel myPanel = (StackPanel)obj;
                    foreach (object myPanelChild in myPanel.Children)
                    {
                        if (myPanelChild is StackPanel)
                        {
                            StackPanel sessieStackpanel = myPanelChild as StackPanel;
                            foreach (object sessieChild in sessieStackpanel.Children)
                            {
                                if (sessieChild is CheckBox)
                                {
                                    CheckBox box = (CheckBox)sessieChild;
                                    if (box.Name.Contains("SessieCheckBox"))
                                    {
                                        if (box.IsChecked == true && box.IsEnabled == true)
                                        {
                                            sessiegevonden = true;
                                            String[] sessiegegevens = box.Name.Split('_');
                                            decimal sessieId = Decimal.Parse(sessiegegevens[1]);
                                            string invoerControlName = box.Name + "Invoer";

                                            foreach (object child in sessieStackpanel.Children)
                                            {
                                                if (child is StackPanel)
                                                {
                                                    StackPanel panel = child as StackPanel;
                                                    string panelName = box.Name + "Panel";
                                                    if (panel.Name == panelName)
                                                    {
                                                        foreach (var panelchild in panel.Children)
                                                        {
                                                            if (panelchild is WebInschrijvingUserControl)
                                                            {
                                                                sessies.Add(sessieId);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return sessies;
        }

        private void SearchCompanyButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZW = new ZoekContactWindow("Bedrijf");
            if (ZW.ShowDialog() == true)
            {
                MyBedrijf = ZW.bedrijf;
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                bedrijven.Add(MyBedrijf);
                BedrijfComboBox.ItemsSource = bedrijven;
                BedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
                BedrijfComboBox.SelectedIndex = 0;
                bedrijfVoorstelLabel.Content = MyBedrijf.Naam + ", " + MyBedrijf.Ondernemersnummer + ", " + MyBedrijf.Adres + ", " + MyBedrijf.Postcode;
                CheckInput();
            }
        }

        private void SearchInvoiceCompanyButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZW = new ZoekContactWindow("Bedrijf");
            if (ZW.ShowDialog() == true)
            {
                MyFacturatieOp = ZW.bedrijf;
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                bedrijven.Add(MyFacturatieOp);
                facturatieBedrijfComboBox.ItemsSource = bedrijven;
                facturatieBedrijfComboBox.DisplayMemberPath = "NaamEnAdres";
                facturatieBedrijfComboBox.SelectedIndex = 0;
                facturatieVoorstelBedrijfLabel.Content = MyFacturatieOp.Naam + ", " + MyFacturatieOp.Ondernemersnummer + ", " + MyFacturatieOp.Adres + ", " + MyFacturatieOp.Postcode;
                CheckInput();
            }
        }

        private void zoekContactButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekFunctieWindow ZC = new ZoekFunctieWindow();
            if (ZC.ShowDialog() == true)
            {
                MyFunctie = ZC.Functie;
                if (MyFunctie.AttestType1 == null)
                    ConfocusMessageBox.Show("Dit contact heeft geen attesttype! Gelieve dit aan te vullen.", ConfocusMessageBox.Kleuren.Rood);
                SetFunctieToForm(false);
                CheckInput();
            }
        }

        private void ZoekOpleidingContactButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekFunctieWindow ZC = new ZoekFunctieWindow();
            if (ZC.ShowDialog() == true)
            {
                MyVerantwoordelijkeOpleiding = ZC.Functie;
                SetVerantwoordelijkeOpleidingToForm();
                CheckInput();
            }
        }

        private void SetVerantwoordelijkeOpleidingToForm()
        {
            opleidingNaamLabel.Content = MyVerantwoordelijkeOpleiding.Contact.VolledigeNaam;
            opleidingEmailLabel.Content = MyVerantwoordelijkeOpleiding.Email;
        }

        private void ZoekPersoneelContactButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekFunctieWindow ZC = new ZoekFunctieWindow();
            if (ZC.ShowDialog() == true)
            {
                MyVerantwoordelijkePersoneel = ZC.Functie;
                SetVerantwoordelijkePersoneelToForm();
                CheckInput();
            }
        }

        private void SetVerantwoordelijkePersoneelToForm()
        {
            //personeelNaamLabel.Content = MyVerantwoordelijkePersoneel.Contact.VolledigeNaam;
            //personeelEmailLabel.Content = MyVerantwoordelijkePersoneel.Email;
        }

        private void NieuwPersoneelContactButton_Click(object sender, RoutedEventArgs e)
        {
            Contact contact = new Contact();
            contact.Voornaam = _inschrijving.VerantwoordelijkePersoneel.Split(' ')[0];
            contact.Achternaam = _inschrijving.VerantwoordelijkePersoneel.Split(' ').Count() > 1 ? _inschrijving.VerantwoordelijkePersoneel.Split(' ')[1] : "";
            Functies functie = new Functies();
            functie.Startdatum = DateTime.Now;
            functie.Email = _inschrijving.VerantwoordelijkePersoneelMail;
            functie.Type = "Prospect";
            NieuwContact NC = new NieuwContact(currentUser, contact, MyBedrijf, functie);
            if (NC.ShowDialog() == true)
            {
                MyVerantwoordelijkePersoneel = NC.SelectedFunctie;
                CheckInput();
            }
        }

        private void NieuwOpleidingContactButton_Click(object sender, RoutedEventArgs e)
        {
            Contact contact = new Contact();
            contact.Voornaam = _inschrijving.CommunicatieVoornaam;
            contact.Achternaam = _inschrijving.CommunicatieAchternaam;
            Functies functie = new Functies();
            functie.Startdatum = DateTime.Now;
            functie.Email = _inschrijving.CommunicatieEmail;
            functie.Type = "Prospect";
            NieuwContact NC = new NieuwContact(currentUser, contact, MyBedrijf, functie);
            //NieuwContact NC = new NieuwContact(currentUser, "Functie");
            if (NC.ShowDialog() == true)
            {
                MyVerantwoordelijkeOpleiding = NC.SelectedFunctie;
                CheckInput();
            }
        }

        private void CheckInput()
        {
            if (MyFunctie != null && MyFunctie.ID != 0)
                ContactValidImage.Visibility = Visibility.Visible;
            else
                ContactValidImage.Visibility = Visibility.Hidden;
            if (MyBedrijf != null && MyBedrijf.ID != 0)
                BedrijfValidImage.Visibility = Visibility.Visible;
            else
                BedrijfValidImage.Visibility = Visibility.Hidden;
            if (MyFacturatieOp != null && MyFacturatieOp.ID != 0)
                FacturatieOpValidImage.Visibility = Visibility.Visible;
            else
                FacturatieOpValidImage.Visibility = Visibility.Hidden;
            if (MyVerantwoordelijkeOpleiding != null)
                VerantwoordelijkeOpleidingImage.Visibility = Visibility.Visible;
            else
                VerantwoordelijkeOpleidingImage.Visibility = Visibility.Hidden;
        }

        private void bewerkContactButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MyContact != null && MyBedrijf != null && MyFunctie != null && MyContact.ID != 0 && MyBedrijf.ID != 0 && MyFunctie.ID != 0)
                {
                    NieuwContact NC = new NieuwContact(currentUser, MyFunctie);
                    if (NC.ShowDialog() == true)
                    {
                        if (NC.SelectedFunctie != null && NC.SelectedFunctie.ID != 0)
                        {
                            MyFunctie = NC.SelectedFunctie;
                            if (MyFunctie != null)
                            {
                                try
                                {
                                    MyFunctie = new FunctieServices().GetFunctieByID(MyFunctie.ID);
                                    MyContact = MyFunctie.Contact;
                                    MyBedrijf = MyFunctie.Bedrijf;
                                    if (MyFunctie.Attesttype_ID == null)
                                        ConfocusMessageBox.Show("Dit contact heeft geen attesttype! Gelieve dit aan te vullen.", ConfocusMessageBox.Kleuren.Rood);
                                    //LoadFunctionsAsync(_inschrijving);
                                    SetFunctieToForm(false);
                                    //SetFunctieToComboBox();
                                    CheckInput();
                                }
                                catch (Exception ex)
                                {
                                    ConfocusMessageBox.Show("Fout bij het verwerken van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                                }
                            }
                        }
                    }
                }

                else
                    ConfocusMessageBox.Show("Het contact kan niet bewerkt worden!\n Waarschijnlijk bestaat het nog niet!", ConfocusMessageBox.Kleuren.Rood);
            }
            catch (Exception ex)
            {
                if (ex is NullReferenceException)
                    ConfocusMessageBox.Show("Het contact kan niet bewerkt worden!\n Waarschijnlijk bestaat het nog niet!", ConfocusMessageBox.Kleuren.Rood);
                else
                    ConfocusMessageBox.Show("Fout bij het openen van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void SetFunctieToComboBox()
        {
            List<Functies> functies = new List<Functies>();
            functies.Add(MyFunctie);
            contactFunctieComboBox.ItemsSource = functies;
            contactFunctieComboBox.DisplayMemberPath = "";
            contactFunctieComboBox.SelectedIndex = 0;
        }

        private void BedrijfComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            Bedrijf bedrijf = (Bedrijf)box.SelectedItem;

            if (bedrijf != null)
            {
                int index = 0;
                bool gevonden = false;
                foreach (Functies functie in contactFunctieComboBox.Items)
                {
                    if (functie.Bedrijf_ID == bedrijf.ID)
                    {
                        MyFunctie = functie;
                        gevonden = true;
                        break;
                    }
                    index++;
                }
                contactFunctieComboBox.SelectedIndex = gevonden == true ? index : -1;
                //SetFunctieToForm(true);
                //MyBedrijf = bedrijf;
                //MyFacturatieOp = bedrijf;
                CheckInput();
            }
        }
    }
}

﻿using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for EmailEditor.xaml
    /// </summary>
    public partial class EmailEditor : Window
    {
        private String Path = "";
        private Boolean Gewijzigd = false;
        private string _dataFolder = new Instelling_Service().Find().DataFolder;
        public EmailEditor(String path)
        {
            InitializeComponent();
            if (path != "")
            {
                this.Path = path.ToString();
                this.Title = "Email Editor: " + Path;
                LoadTemplate(Path);
            }
            Editor.KeyUp += SetGewijzigd;

        }

        private void SetGewijzigd(object sender, KeyboardEventArgs e)
        {
            Gewijzigd = true;
        }

        private void OpenMailButton_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "saveas":
                    img = saveasImage;
                    break;
                default:
                    img = editImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void OpenMailButton_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "saveas":
                    img = saveasImage;
                    break;
                default:
                    img = editImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void OpenMailButton_Click(object sender, RoutedEventArgs e)
        {

             //Example: "Image files (*.bmp, *.jpg)|*.bmp;*.jpg|All files (*.*)|*.*"
            OpenFileDialog OD = new OpenFileDialog();
            OD.Filter = "html bestanden (*.html,*.htm)|*.html;*.htm|Alles (*.*)|*.*";
            if (OD.ShowDialog() == true)
            {
                Path = OD.FileName;
                this.Title = "Email Editor: " + Path;
                LoadTemplate(Path);
            }
        }

        private void LoadTemplate(string path)
        {
            //ConfocusMessageBox.Show(path, ConfocusMessageBox.Kleuren.Blauw);
            try
            {
                using (TextReader tr = new StreamReader(path))
                {
                    string filecontent = tr.ReadToEnd();
                    Editor.ContentHtml = filecontent;
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het openen van het bestand! " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (Path != "")
            {
                SaveFile(Path);
            }
            else
            {
                SaveFileAs();
            }
            Gewijzigd = false;
        }

        private void SaveFileAs()
        {
            SaveFileDialog SD = new SaveFileDialog();
            SD.Filter = "html bestanden (*.html,*.htm)|*.html;*.htm|Alles (*.*)|*.*";
            SD.InitialDirectory = _dataFolder + "Templates\\";
            SD.FileName = _dataFolder + "Templates\\Nieuw Bestand.html";
            if (SD.ShowDialog() == true)
            {
                try
                {
                    Path = SD.FileName;
                    String mail = "<html><body>" + Editor.ContentHtml + "</body></html>";
                    File.WriteAllText(Path, mail, Encoding.UTF8);
                    this.Title = "Email Editor: " + Path; 
                    ConfocusMessageBox.Show("Bestand Opgeslagen!", ConfocusMessageBox.Kleuren.Blauw);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan van het bestand! " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void SaveFile(string Path)
        {
            try
            {
                String mail = "<html><body>" + Editor.ContentHtml + "</body></html>";
                File.WriteAllText(Path, mail, Encoding.UTF8);
                this.Title = "Email Editor: " + Path;
                ConfocusMessageBox.Show("Bestand Opgeslagen!", ConfocusMessageBox.Kleuren.Blauw);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het bestand! " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            if (Gewijzigd)
            {
                MessageBoxResult result = MessageBox.Show("Het huidige bestand is gewijzigd!\n Wil je deze nog opslaan?", "Let op!!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    if (Path == "")
                        SaveFileAs();
                    else
                        SaveFile(Path);
                    CreateNew();
                }
                else if (result == MessageBoxResult.No)
                {
                    CreateNew();
                }
            }
            else
            {
                CreateNew();
            }
        }

        private void CreateNew()
        {
            Editor.ContentHtml = "";
            Gewijzigd = false;
            Path = "";
            this.Title = "Email Editor: nieuw bestand";
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileAs();
            Gewijzigd = false;
        }
    }
}

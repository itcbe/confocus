﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for OnderwerpBeheer.xaml
    /// </summary>
    public partial class OnderwerpBeheer : Window
    {
        private CollectionViewSource onderwerpViewSource;
        //private List<Onderwerp> onderwerpen = new List<Onderwerp>();
        //private List<Onderwerp> gevondenOnderwerpen = new List<Onderwerp>();
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private Gebruiker currentUser;

        public OnderwerpBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Onderwerp beheer : Ingelogd als " + currentUser.Naam;
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            onderwerpViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            onderwerpViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            onderwerpViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            onderwerpViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void GoUpdate()
        {
            throw new NotImplementedException();
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (ZoekTextBox.Text != "")
            //{
            //    gevondenOnderwerpen.Clear();
            //    gevondenOnderwerpen = (from o in onderwerpen
            //                           where o.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower())
            //                           select o).ToList();
            //    onderwerpViewSource.Source = gevondenOnderwerpen;

            //}
            //else
            //    onderwerpViewSource.Source = onderwerpen;
            //GoUpdate();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            onderwerpViewSource = ((CollectionViewSource)(this.FindResource("onderwerpViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            Niveau1Services n1service = new Niveau1Services();
            List<Niveau1> n1Lijst = n1service.FindAll();
            niveau1ComboBox.ItemsSource = n1Lijst;
            niveau1ComboBox.DisplayMemberPath = "Naam";

            SeminarieServices sService = new SeminarieServices();
            List<Seminarie> seminaries = sService.FindActive();
            seminarieComboBox.ItemsSource = seminaries;
            seminarieComboBox.DisplayMemberPath = "Naam";

            GoUpdate();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

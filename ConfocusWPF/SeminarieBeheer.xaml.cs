﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SeminarieBeheer.xaml
    /// </summary>
    public partial class SeminarieBeheer : Window
    {
        private CollectionViewSource doelgroepViewSource;
        private CollectionViewSource sessieViewSource;
        private List<Sessie> gevondenSessies = new List<Sessie>();
        List<Sessie> sessies = new List<Sessie>();
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private Gebruiker currentUser;
        private string _dataFolder;

        public SeminarieBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Seminarie beheer : Ingelogd als " + currentUser.Naam;

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            doelgroepViewSource = ((CollectionViewSource)(this.FindResource("doelgroepViewSource")));
            sessieViewSource = ((CollectionViewSource)(this.FindResource("sessieViewSource")));
            _dataFolder = new Instelling_Service().Find().DataFolder;
            LoadData();
            this.Activate();
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            UpdateWeergaveList();

            GebruikerService gService = new GebruikerService();
            List<Gebruiker> gebruikers = gService.FindActive();
            gebruikers.Insert(0, new Gebruiker() { Naam = "Kies" });
            //sessieFilterOrganisatorComboBox.ItemsSource = gebruikers;
            //sessieFilterOrganisatorComboBox.DisplayMemberPath = "Naam";
            //sessieFilterOrganisatorComboBox.SelectedIndex = 0;

            SessieVerantwComboBox.ItemsSource = gebruikers;
            SessieVerantwComboBox.DisplayMemberPath = "Naam";
            SessieVerantwComboBox.SelectedIndex = 0;

            TerplaatseVerantwComboBox.ItemsSource = gebruikers;
            TerplaatseVerantwComboBox.DisplayMemberPath = "Naam";
            TerplaatseVerantwComboBox.SelectedIndex = 0;

            LocatieServices lService = new LocatieServices();
            List<Locatie> locaties = lService.FindActive();
            locaties.Insert(0, new Locatie() { Naam = "Kies" });
            sessieFilterLocatieComboBox.ItemsSource = locaties;
            sessieFilterLocatieComboBox.DisplayMemberPath = "Naam";
            sessieFilterLocatieComboBox.SelectedIndex = 0;

            List<Seminarietype> seminarietypes = new SeminarieTypeService().GetActive();
            seminarietypes.Insert(0, new Seminarietype() { Type = "Kies" });
            seminarieTypeComboBox.ItemsSource = seminarietypes;
            seminarieTypeComboBox.DisplayMemberPath = "Type";
            seminarieTypeComboBox.SelectedIndex = 0;

            List<Erkenning> erkenningen = new ErkenningServices().FindActive();
            List<ConfocusCheckBoxListBoxItem> checkErkenningen = new List<ConfocusCheckBoxListBoxItem>();
            ConfocusCheckBoxListBoxItem leeg = new ConfocusCheckBoxListBoxItem();
            leeg.ID = 0;
            leeg.Name = "Blanco";
            leeg.IsChecked = false;
            checkErkenningen.Add(leeg);
            foreach (Erkenning erk in erkenningen)
            {
                ConfocusCheckBoxListBoxItem item = new ConfocusCheckBoxListBoxItem();
                item.ID = erk.ID;
                item.Name = erk.Naam;
                item.IsChecked = false;
                checkErkenningen.Add(item);
            }
            //erkenningen.Insert(0, new Erkenning() { Naam = "Kies" });
            erkenningComboBox.ItemsSource = checkErkenningen;
            //erkenningComboBox.DisplayMemberPath = "Naam";
            //erkenningComboBox.SelectedIndex = 0;

            new Thread(delegate () { LoadSessies(); }).Start();
            

            GoUpdate();

        }

        private void LoadSessies()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);

            SessieServices sService = new SessieServices();
            DateTime filterdate = DateTime.Today.AddDays(-21);
            sessies = sService.FindResent(filterdate);

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieViewSource.SetValue(CollectionViewSource.SourceProperty, sessies); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
        }

        private void UpdateWeergaveList()
        {
            SessieWeergaven_Service sService = new SessieWeergaven_Service();
            List<SessieWeergaven> weergaven = sService.FindAll();
            weergaven.Insert(0, new SessieWeergaven() { Naam = "Kies" });
            sessieFilterWeergaveComboBox.ItemsSource = weergaven;
            sessieFilterWeergaveComboBox.DisplayMemberPath = "Naam";
            sessieFilterWeergaveComboBox.SelectedIndex = 0;
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            sessieViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            sessieViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            sessieViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            sessieViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void GoUpdate()
        {
            if (sessieDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (sessieViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (sessieViewSource.View.CurrentPosition == sessieDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                sessieDataGrid.SelectedIndex = 0;
            if (sessieDataGrid.Items.Count != 0)
                sessieDataGrid.ScrollIntoView(sessieDataGrid.SelectedItem);
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            Seminarie seminarie = new Seminarie();
            NieuweSeminarie NS = new NieuweSeminarie(currentUser, seminarie, null, null, false);
            NS.Show();
            NS.Activate();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "inschrijving":
                    img = InschrijvingImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "excel":
                    img = ExporterenImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "inschrijving":
                    img = InschrijvingImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "excel":
                    img = ExporterenImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ActivateFilters();
                //if (ZoekTextBox.Text != "")
                //{
                //    gevondenSessies = (from s in sessies
                //                       where s.Seminarie.Titel.ToLower().Contains(ZoekTextBox.Text.ToLower())
                //                       || s.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower())
                //                       || (s.Gebruiker1 != null && s.Gebruiker1.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower()))
                //                       select s).ToList();

                //    sessieViewSource.Source = gevondenSessies;
                //}
                //else
                //    sessieViewSource.Source = sessies;
                //GoUpdate();
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void inschrijvingenButton_Click(object sender, RoutedEventArgs e)
        {
            //if (seminarieDataGrid.SelectedIndex > -1)
            //{
            //    Seminarie seminarie = (Seminarie)seminarieDataGrid.SelectedItem;
            //    Inschrijvingen IS = new Inschrijvingen(Username, seminarie);
            //    IS.ShowDialog();
            //}
        }

        private void seminarieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (sessieDataGrid.Items.Count > 0)
            {
                Sessie sessie = (Sessie)sessieDataGrid.SelectedItem;
                Seminarie seminarie = sessie.Seminarie;
                if (sessie.Erkenning_ID != null)
                {
                    ErkenningServices eService = new ErkenningServices();
                    Erkenning erkenning = eService.GetErkenningByID(sessie.Erkenning_ID);
                    erkenningTextBox.Text = erkenning.Naam;
                }
                else
                    erkenningTextBox.Text = "";
                if (sessie.Verantwoordelijke_Sessie_ID != null)
                {
                    GebruikerService gService = new GebruikerService();
                    Gebruiker verantwoordelijke_sessie = gService.GetUserByID(sessie.Verantwoordelijke_Sessie_ID);
                    sessieverantTextBox.Text = verantwoordelijke_sessie.Naam;
                }
                else
                    sessieverantTextBox.Text = "";
                if (sessie.Verantwoordelijke_Terplaatse_ID != null)
                {
                    GebruikerService gService = new GebruikerService();
                    Gebruiker verantwoordelijke_terplaatse = gService.GetUserByID(sessie.Verantwoordelijke_Terplaatse_ID);
                    terplaatseTextBox.Text = verantwoordelijke_terplaatse.Naam;
                }
                else
                    terplaatseTextBox.Text = "";
                if (seminarie.Verantwoordelijke_ID != null)
                {
                    GebruikerService gService = new GebruikerService();
                    Gebruiker gebruiker = gService.GetUserByID(seminarie.Verantwoordelijke_ID);
                    verantwoordelijkeTextBox.Text = gebruiker.Naam;
                }
                else
                    verantwoordelijkeTextBox.Text = "";
                AantalInschrijvingenLabel.Content = seminarie.AantalInschrijvingen;
                if (seminarie.Gewijzigd == null)
                    seminarieAangemaaktLabel.Content = "Aangemaakt op " + seminarie.Aangemaakt + " door " + seminarie.Aangemaakt_door;
                else
                    seminarieAangemaaktLabel.Content = "Gewijzigd op " + seminarie.Gewijzigd + " door " + seminarie.Gewijzigd_door;
                if (sessie.Gewijzigd == null)
                    sessieAangemaaktLabel.Content = "Aangemaakt op " + sessie.Aangemaakt.ToString() + " door " + sessie.Aangemaakt_door;
                else
                    sessieAangemaaktLabel.Content = "Gewijzigd op " + sessie.Gewijzigd.ToString() + " door " + sessie.Gewijzigd_door;
                sessieActiefLabel.Content = sessie.Actief == true ? "Ja" : "Nee";
                SetSeminarie(seminarie);
                SetDoelgroepen(seminarie);
            }
        }

        private void SetSeminarie(Seminarie seminarie)
        {
            if (seminarie != null)
            {
                titelTextBox.Text = seminarie.Titel;
                typeTextBox.Text = seminarie.Type;
                weeknummerTextBox.Text = seminarie.Weeknummer;
                startdatum_organisatieTextBox.Text = seminarie.Startdatum_organisatie.ToString();
                start_marketingTextBox.Text = seminarie.Start_marketing.ToString();
                deadline_organisatieTextBox.Text = seminarie.Deadline_organisatie.ToString();
                seminarieStatusTextBox.Text = seminarie.Status;
                seminarieActiefLabel.Content = seminarie.Actief == true ? "Ja" : "Nee";

            }
        }

        private void SetSessies(Seminarie seminarie)
        {
            SessieServices sService = new SessieServices();
            List<Sessie> sessies = sService.GetSessiesBySeminarieID(seminarie.ID);
        }

        private void SetDoelgroepen(Seminarie seminarie)
        {
            if (seminarie != null)
            {
                ClearDoelgroep();
                Seminarie_DoelgroepServices sdService = new Seminarie_DoelgroepServices();
                List<Seminarie_Doelgroep> doelgroepen = sdService.GetDoelgroepenBySeminarieID(seminarie.ID);
                doelgroepListBox.ItemsSource = doelgroepen;
                doelgroepListBox.DisplayMemberPath = "Naam";
                doelgroepListBox.SelectedIndex = 0;
            }
        }

        private void ClearDoelgroep()
        {
            niveau1TextBox.Text = "";
            niveau2TextBox.Text = "";
            niveau3TextBox.Text = "";
        }


        private void niveau3ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BindSessieDetails(Sessie mySessie)
        {
            ClearSessieDetails();
            if (mySessie != null)
            {
                naamTextBox1.Text = mySessie.Naam;
                if (mySessie.Datum != null)
                    datumTextBox.Text = ((DateTime)mySessie.Datum).ToShortDateString();
                beginuurComboBox.Text = mySessie.Beginuur;
                einduurTextBox.Text = mySessie.Einduur;
                statusTextBox.Text = mySessie.Status;
                //zaalTextBox.Text = mySessie.Zaal;
                status_infoTextBox.Text = mySessie.Status_info;
                uRLTextBox.Text = mySessie.URL;
                standaard_prijsTextBox.Text = mySessie.Standaard_prijs;
                prijs_Non_ProfitTextBox.Text = mySessie.Prijs_Non_Profit;
                prijs_Profit_PriveTextBox.Text = mySessie.Prijs_Profit_Prive;
                kortingsprijsTextBox.Text = mySessie.Kortingsprijs;
                targetTextBox.Text = mySessie.Target;
                //opstelling_zaalTextBox.Text = mySessie.Opstelling_zaal;
                catering_LunchTextBox.Text = mySessie.Catering_Lunch;
                status_LocatieTextBox.Text = mySessie.Status_Locatie;
                status_DatumTextBox.Text = mySessie.Status_Datum;
                kMO_Port_UrenTextBox.Text = mySessie.KMO_Port_Uren;
                opmerkingTextBox.Text = mySessie.Opmerking;
                //beamerTextBox.Text = mySessie.Beamer;
                //laptopTextBox.Text = mySessie.Laptop;
                sessieActiefLabel.Content = mySessie.Actief == true ? "Ja" : "Nee";
                if (mySessie.Erkenning_ID != null)
                {
                    ErkenningServices eService = new ErkenningServices();
                    Erkenning er = eService.GetErkenningByID(mySessie.Erkenning_ID);
                    if (er != null)
                        erkenningTextBox.Text = er.Naam;
                }
                if (mySessie.Locatie_ID != null)
                {
                    LocatieServices lService = new LocatieServices();
                    Locatie locatie = lService.GetLocatieByID((decimal)mySessie.Locatie_ID);
                    if (locatie != null)
                    {
                        locatieTextBox.Text = locatie.Naam;
                    }
                }
                if (mySessie.Verantwoordelijke_Terplaatse_ID != null)
                {
                    GebruikerService gService = new GebruikerService();
                    Gebruiker gebruiker = gService.GetUserByID(mySessie.Verantwoordelijke_Terplaatse_ID);
                    if (gebruiker != null)
                        terplaatseTextBox.Text = gebruiker.Naam;
                }
                if (mySessie.Verantwoordelijke_Sessie_ID != null)
                {
                    GebruikerService gService = new GebruikerService();
                    Gebruiker gebruiker = gService.GetUserByID(mySessie.Verantwoordelijke_Sessie_ID);
                    if (gebruiker != null)
                        sessieverantTextBox.Text = gebruiker.Naam;
                }

                if (mySessie.Gewijzigd == null)
                    sessieAangemaaktLabel.Content = "Aangemaakt op " + mySessie.Aangemaakt + " door " + mySessie.Aangemaakt_door;
                else
                    sessieAangemaaktLabel.Content = "Gewijzigd op " + mySessie.Gewijzigd + " door " + mySessie.Gewijzigd_door;
            }
        }

        private void ClearSessieDetails()
        {
            naamTextBox1.Text = "";
            datumTextBox.Text = "";
            beginuurComboBox.Text = "";
            einduurTextBox.Text = "";
            statusTextBox.Text = "";
            //zaalTextBox.Text = "";
            status_infoTextBox.Text = "";
            uRLTextBox.Text = "www.confocus.be/";
            standaard_prijsTextBox.Text = "";
            prijs_Non_ProfitTextBox.Text = "";
            prijs_Profit_PriveTextBox.Text = "";
            kortingsprijsTextBox.Text = "";
            //opstelling_zaalTextBox.Text = "";
            catering_LunchTextBox.Text = "";
            status_LocatieTextBox.Text = "";
            status_DatumTextBox.Text = "";
            kMO_Port_UrenTextBox.Text = "";
            //beamerTextBox.Text = "";
            //laptopTextBox.Text = "";
            sessieActiefLabel.Content = "";
            locatieTextBox.Text = "";
            targetTextBox.Text = "";
            sessieverantTextBox.Text = "";
            terplaatseTextBox.Text = "";

        }

        /* ***************** Seminarie wijzigingen ************************/

        //private void seminarieTextBox_KeyUp(object sender, KeyEventArgs e)
        //{
        //}

        //private void seminarieComboBox_DropDownClosed(object sender, EventArgs e)
        //{
        //}

        private void SaveSeminarieWijzigingen(Seminarie seminarie)
        {
            SeminarieServices sService = new SeminarieServices();
            try
            {
                sService.SaveSeminarieChanges(seminarie);
                if (seminarie.Actief == false)
                    sService.ZetSessiesOpNonActief(seminarie.ID);
                else
                    sService.ZetSessieOpActief(seminarie.ID);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
        }

        private void SeminarieDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        /****************** Doelgroep wijzigingen *****************/

        private void DoelgroepTextBox_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void SaveDoelgroepWijzigingen(Doelgroep doelgroep)
        {
            if (doelgroep != null)
            {
                DoelgroepServices dService = new DoelgroepServices();
                doelgroep.Gewijzigd = DateTime.Now;
                doelgroep.Gewijzigd_door = currentUser.Naam;
                try
                {
                    dService.SaveDoelgroepChanges(doelgroep);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void DoelgroepComboBox_DropDownClosed(object sender, EventArgs e)
        {
        }

        private void actiefCheckBox1_Click(object sender, RoutedEventArgs e)
        {
        }


        private void SaveSessieWijzigingen(Sessie sessie)
        {
            if (sessie != null)
            {
                sessie.Gewijzigd = DateTime.Now;
                sessie.Gewijzigd_door = currentUser.Naam;
                try
                {
                    SessieServices sService = new SessieServices();
                    sService.SaveSessieChanges(sessie);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }


        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            EditSeminarie();

        }

        private void EditSeminarie()
        {
            if (sessieDataGrid.SelectedIndex > -1)
            {
                Sessie sessie = (Sessie)sessieDataGrid.SelectedItem;
                Seminarie seminarie = sessie.Seminarie;
                NieuweSeminarie NS = new NieuweSeminarie(currentUser, seminarie, null, sessie, false);
                NS.Show();
                NS.Activate();
                //if (NS.ShowDialog() == true)
                //{
                //    LoadData();
                //}
            }
        }

        private void kalenderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Kalender KL = new Kalender(currentUser);
            KL.Show();
            KL.Activate();
        }

        private void inschrijvingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Inschrijvingen IS = new Inschrijvingen(currentUser, null);
            IS.Show();
            IS.Activate();
        }

        private void CloseApplicationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Wil je het programma sluiten?", "Afsluiten", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void DeelnemersMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Aanwezigheden AW = new Aanwezigheden(null, null, currentUser);
            AW.Show();
            AW.Activate();
        }

        private void seminarieDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.Target is System.Windows.Controls.TextBlock 
                || e.MouseDevice.Target is System.Windows.Controls.TextBox
                || e.MouseDevice.Target is System.Windows.Controls.Border)
                EditSeminarie();
        }

        private void EmailEditorMenuItem_Click(object sender, RoutedEventArgs e)
        {
            EmailEditor editor = new EmailEditor("");
            editor.Show();
            editor.Activate();
        }

        private void HistoriekMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SeminarieHistoriek SH = new SeminarieHistoriek(currentUser);
            SH.Show();
            SH.Activate();
        }

        private void doelgroepListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (doelgroepListBox.SelectedIndex > -1)
            {
                ClearDoelgroep();
                Seminarie_Doelgroep SD = (Seminarie_Doelgroep)doelgroepListBox.SelectedItem;
                //    naamTextBox.Text = SD.Doelgroep.Naam;
                if (SD.Niveau1 != null)
                    niveau1TextBox.Text = SD.Niveau1.Naam;
                if (SD.Niveau2 != null)
                    niveau2TextBox.Text = SD.Niveau2.Naam;
                if (SD.Niveau3 != null)
                    niveau3TextBox.Text = SD.Niveau3.Naam;
                //    DoelgroepStatusTextBox.Text = SD.Status;
                if (SD.Gewijzigd == null)
                    doelgroepAangemaaktLabel.Content = "Aangemaakt op " + SD.Aangemaakt.ToString() + " door " + SD.Aangemaakt_door;
                else
                    doelgroepAangemaaktLabel.Content = "Gewijzigd op " + SD.Gewijzigd.ToString() + " door " + SD.Gewijzigd_door;
            }
        }

        private void seminarieDetailsHideButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Toon details")
            {
                button.Content = "Verberg details";
                sessieDetailRow.Height = new GridLength(400);
            }
            else
            {
                button.Content = "Toon details";
                sessieDetailRow.Height = new GridLength(30);
            }
        }

        private void uRLTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TextBox box = (TextBox)sender;
                Helper.OpenUrl(box.Text);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ActivateFilters();
            this.Activate();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetFilters();
        }

        private void MaakFiltersLeeg()
        {
            ZoekTextBox.Text = "";
            sessieFilterTaalComboBox.SelectedIndex = 0;
            sessieFilterLocatieComboBox.SelectedIndex = 0;
            sessieFilterStatusInschrijvingComboBox.SelectedIndex = 0;
            sessieFilterAantalInschrTextBox.Text = "";
            SessieVerantwComboBox.SelectedIndex = 0;
            TerplaatseVerantwComboBox.SelectedIndex = 0;
            AllesCheckBox.IsChecked = false;
            List<ConfocusCheckBoxListBoxItem> items = new List<ConfocusCheckBoxListBoxItem>();
            foreach (ConfocusCheckBoxListBoxItem item in erkenningComboBox.Items)
            {
                item.IsChecked = false;
                items.Add(item);
            }
            //erkenningComboBox.SelectedIndex = 0;
            erkenningComboBox.ItemsSource = items;
            erkenningStatus.SelectedIndex = 0;        
        }

        private void SaveWeergaveButton_Click(object sender, RoutedEventArgs e)
        {
            SessieWeergaven_Service sService = new SessieWeergaven_Service();
            if (sessieFilterWeergaveComboBox.SelectedIndex > 0)
            {
                SessieWeergaven weergave = (SessieWeergaven)sessieFilterWeergaveComboBox.SelectedItem;
                weergave = GetWeergaveFilters(weergave);

                try
                {
                    sService.Update(weergave);
                    UpdateWeergaveList();
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                WeergaveInputWindow WW = new WeergaveInputWindow();
                if (WW.ShowDialog() == true)
                {

                    SessieWeergaven weergave = new SessieWeergaven();
                    weergave = GetWeergaveFilters(weergave);
                    weergave.Naam = WW.Naam;

                    try
                    {
                        sService.Save(weergave);
                        UpdateWeergaveList();
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private SessieWeergaven GetWeergaveFilters(SessieWeergaven weergave)
        {
            int aantal, weeknummer;
            if (int.TryParse(sessieFilterAantalInschrTextBox.Text, out aantal))
            {
                weergave.Aantal_inschrijvingen = aantal;
            }

            if (sessieFilterLocatieComboBox.SelectedIndex > 0)
            {
                weergave.Locatie_ID = ((Locatie)sessieFilterLocatieComboBox.SelectedItem).ID;
            }

            if (sessieFilterStatusInschrijvingComboBox.SelectedIndex > 0)
            {
                weergave.InschrijvingStatus = sessieFilterStatusInschrijvingComboBox.Text;
            }

            if (sessieFilterTaalComboBox.SelectedIndex > 0)
            {
                weergave.Taalcode = sessieFilterTaalComboBox.Text;
            }

            if (SessieVerantwComboBox.SelectedIndex > 0)
                weergave.Verantwoordelijke_sessie_ID = ((Gebruiker)SessieVerantwComboBox.SelectedItem).ID;

            if (TerplaatseVerantwComboBox.SelectedIndex > 0)
                weergave.Verantwoordelijke_terplaatse_ID = ((Gebruiker)TerplaatseVerantwComboBox.SelectedItem).ID;

            if (erkenningComboBox.SelectedIndex > 0)
                weergave.Erkenning_ID = ((Erkenning)erkenningComboBox.SelectedItem).ID;

            if (erkenningStatus.SelectedIndex > 0)
                weergave.Erkenning_status = erkenningStatus.Text;

            if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                weergave.Zoektekst = ZoekTextBox.Text;

            weergave.User_ID = currentUser.ID;

            return weergave;
        }

        private void ActivateFilters()
        {
            SessieSearchFilter filter = GetSearchFilter();
            gevondenSessies.Clear();
            Task.Run(() => ZoekSessiesAsync(filter));
            //new Thread( delegate() { ZoekSessiesAsync(filter); }).Start();       
            GoUpdate();
        }

        private void ZoekSessiesAsync(SessieSearchFilter filter)
        {
            try
            {

            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
            List<Sessie> selected = new List<Sessie>();
                // get language id
                decimal taal_id = GetLanguageID(filter.Taalcode);
                // Filter the data
                if (!string.IsNullOrEmpty(filter.InschrijvingStatus))
                {
                    List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = new InschrijvingenService().GetInschrijvingenPerStatus(filter.InschrijvingStatus);
                    selected = GetSessiesFromInschrijvingen(inschrijvingen);
                    //selected = new SessieServices().GetSessiesFromInschrijvingsStatus(filter.InschrijvingStatus);//GetSessiesFromInschrijvingen(inschrijvingen);
                }
                else
                {
                    if (filter.Erkenningen.Count > 0)
                    {
                        if (filter.Erkenningen.Contains(0))
                        {
                            List<Sessie> semerkenningensessies = new Seminarie_ErkenningenService().GetAllErkenningenSessies();
                            List<Sessie> blancoSessies = new SessieServices().GetSessiesExErkenningSessies(semerkenningensessies);
                            selected = blancoSessies;
                        }
                        else
                        {
                            List<Seminarie_Erkenningen> semerkenningen = new Seminarie_ErkenningenService().GetByErkenningIDs(filter.Erkenningen);
                            if (!string.IsNullOrEmpty(filter.Erkenningstatus))
                                semerkenningen = semerkenningen.Where(se => se.Status == filter.Erkenningstatus).ToList();
                            selected = new SessieServices().GetByIDS(GetSessionErkenningen(semerkenningen));
                        }
                    }
                    else if (!string.IsNullOrEmpty(filter.Erkenningstatus))
                    {
                        List<Seminarie_Erkenningen> semerkenningen = new Seminarie_ErkenningenService().GetAllErkenningen();
                        if (!string.IsNullOrEmpty(filter.Erkenningstatus))
                            semerkenningen = semerkenningen.Where(se => se.Status == filter.Erkenningstatus).ToList();
                        selected = new SessieServices().GetByIDS(GetSessionErkenningen(semerkenningen));
                    }
                    else
                        selected = new SessieServices().GetFullSessies(sessies);
                }
                var query = from s in selected select s;

                if (!string.IsNullOrEmpty(filter.Zoektekst))
                    query = query.AsQueryable().Where(s => s.SeminarieTitel.ToLower().Contains(filter.Zoektekst.ToLower()) || s.Naam.ToLower().Contains(filter.Zoektekst.ToLower()));

                if (!string.IsNullOrEmpty(filter.Organisatieweek))
                {
                    query = query.AsQueryable().Where(s => s.Seminarie.Weeknummer == filter.Organisatieweek);
                }

                if (!string.IsNullOrEmpty(filter.Seminarietype))
                {
                    query = query.AsQueryable().Where(s => s.Seminarie.Type == filter.Seminarietype);
                }

                if (filter.Locatie != null)
                {
                    query = query.AsQueryable().Where(s => s.Locatie_ID == filter.Locatie.ID);
                }

                if (filter.VSessie != null)
                {
                    query = query.AsQueryable().Where(s => s.Verantwoordelijke_Sessie_ID == filter.VSessie.ID);
                }

                if (filter.VTerplaatse != null)
                {
                    query = query.AsQueryable().Where(s => s.Verantwoordelijke_Terplaatse_ID == filter.VTerplaatse.ID);
                }

                if (filter.AantalDeelnemers != -1)
                {
                    query = query.AsQueryable().Where(s => s.AantalInschrijvingen == filter.AantalDeelnemers);
                }

                if (taal_id != 0)
                {
                    query = query.AsQueryable().Where(s => s.Seminarie.Taal_ID == taal_id);
                }

                try
                {

                    DateTime driewekenTerug = DateTime.Today.AddDays(-21);
                    if (filter.InschrijvingStatus == "Interesse" || filter.InschrijvingStatus == "Opvolgen")
                    {
                        driewekenTerug = DateTime.Today.AddYears(-5);
                        List<Sessie> tempsessie = query.AsQueryable().Where(s => s.Sorteerdatum > driewekenTerug || s.TeFacturerenInschrijvingen > 0).OrderBy(s => s.Sorteerdatum).ToList();
                        gevondenSessies = (from s in tempsessie where s.AantalEffectieveInschrijvingenAndInteresse > 0 select s).ToList();
                    }
                    else
                        gevondenSessies = query.AsQueryable().Where(s => s.Sorteerdatum > driewekenTerug || s.TeFacturerenInschrijvingen > 0).OrderBy(s => s.Sorteerdatum).ToList();
                }
                catch (Exception ex)
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() => { ConfocusMessageBox.Show("Fout bij het filteren!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood); }));
                }
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieViewSource.SetValue(CollectionViewSource.SourceProperty, gevondenSessies); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
            }
            catch (Exception nex)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() => { ConfocusMessageBox.Show("Fout bij het filteren!\n" + nex.Message, ConfocusMessageBox.Kleuren.Rood); }));
            }
        }

        private List<decimal> GetSessionErkenningen(List<Seminarie_Erkenningen> semerkenningen)
        {
            List<decimal> erksessies = new List<decimal>();
            foreach (Seminarie_Erkenningen semerk in semerkenningen)
            {
                bool gevonden = false;
                foreach (decimal ses in erksessies)
                {
                    if (semerk.Sessie_ID != null)
                    {
                        if (semerk.Sessie_ID == ses)
                            gevonden = true;
                    }
                    else
                        gevonden = true;
                }
                if (!gevonden)
                    if (semerk.Sessie_ID != null)
                        erksessies.Add((decimal)semerk.Sessie_ID);
            }
            return erksessies;

        }

        private decimal GetLanguageID(string taalcode)
        {
            if (!string.IsNullOrEmpty(taalcode))
                return new TaalServices().GetTaalByNameOrCode(taalcode).ID;
            else
                return 0;

        }

        private SessieSearchFilter GetSearchFilter()
        {
            SessieSearchFilter filter = new SessieSearchFilter();
            //erkenningen
            List<decimal> erkenningIDs = new List<decimal>();
            foreach (ConfocusCheckBoxListBoxItem item in erkenningComboBox.Items)
            {
                if (item.IsChecked)
                    erkenningIDs.Add(item.ID);
            }
            filter.Erkenningen = erkenningIDs;
            //Locatie
            if (sessieFilterLocatieComboBox.SelectedIndex > 0)
            {
               filter.Locatie = (Locatie)sessieFilterLocatieComboBox.SelectedItem;           
            }
            if (sessieFilterTaalComboBox.SelectedIndex > 0)
                filter.Taalcode = sessieFilterTaalComboBox.Text;
            //Inschrijvingstatus
            if (sessieFilterStatusInschrijvingComboBox.SelectedIndex > 0)
            {
                filter.InschrijvingStatus = sessieFilterStatusInschrijvingComboBox.Text;
            }
            //Zoektekst
            if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                filter.Zoektekst = ZoekTextBox.Text;
            //Aantal deelnemers
            if (!string.IsNullOrEmpty(sessieFilterAantalInschrTextBox.Text))
                filter.SetAantalDeelnemers(sessieFilterAantalInschrTextBox.Text);
            else
                filter.AantalDeelnemers = -1;
            //Verantwoordelijke sessie
            if (SessieVerantwComboBox.SelectedIndex > 0)
            {
                filter.VSessie = (Gebruiker)SessieVerantwComboBox.SelectedItem;
            }
            // Verantwoordelijke terplaatse.
            if (TerplaatseVerantwComboBox.SelectedIndex > 0)
            {
                filter.VTerplaatse = (Gebruiker)TerplaatseVerantwComboBox.SelectedItem;
            }
            // Erkenningen status
            if (erkenningStatus.SelectedIndex > 0)
                filter.Erkenningstatus = erkenningStatus.Text;
            // Seminarietype
            if (seminarieTypeComboBox.SelectedIndex > 0)
                filter.Seminarietype = seminarieTypeComboBox.Text;
            return filter;
        }

        private List<Sessie> GetSessiesFromInschrijvingen(List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen)
        {
            List<Sessie> sessies = new List<Sessie>();
            foreach (ConfocusDBLibrary.Inschrijvingen inschrijving in inschrijvingen)
            {
                bool gevonden = false;
                foreach (Sessie sessie in sessies)
                {
                    if (sessie.ID == inschrijving.Sessie_ID)
                        gevonden = true;
                }
                if (!gevonden)
                    sessies.Add(inschrijving.Sessie);
            }

            return new SessieServices().GetFullSessies(sessies);
        }

        private List<Sessie> GetSessiesFromSeminaries(List<Seminarie> seminaries)
        {
            List<Sessie> sessies = new List<Sessie>();
            SessieServices sService = new SessieServices();
            foreach (Seminarie sem in seminaries)
            {
                List<Sessie> tempsessies = sService.GetSessiesBySeminarieID(sem.ID);
                foreach (Sessie ses in tempsessies)
                {
                    sessies.Add(ses);
                }
            }
            DateTime datum = DateTime.Today.AddDays(-14);
            return sessies.Where(s => s.Datum >= datum && s.Status == "Actief").ToList();
        }

        private void FilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.LineFeed || e.Key == Key.Tab)
                ActivateFilters();
        }

        private void sessieFilterWeergaveComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                SessieWeergaven weergave = (SessieWeergaven)box.SelectedItem;
                SetWeergave(weergave);
                ActivateFilters();
            }
            else
                new Thread(delegate() { ResetFilters(); }).Start();
        }

        private void websiteInschrijvingenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            WebsiteImport WI = new WebsiteImport(currentUser);
            WI.Show();
            WI.Activate();
        }

        private void AllesCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            bool isChecked = box.IsChecked == true;
            List<ConfocusCheckBoxListBoxItem> items = new List<ConfocusCheckBoxListBoxItem>();
            foreach (ConfocusCheckBoxListBoxItem myItem in erkenningComboBox.Items)
            {
                myItem.IsChecked = isChecked;
                items.Add(myItem);
            }
            erkenningComboBox.ItemsSource = null;
            erkenningComboBox.ItemsSource = items;
        }

        private void DocumentCreatieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DocumentCreatie DC = new DocumentCreatie(currentUser);
            DC.Show();
        }

        private void exporteren_Click(object sender, RoutedEventArgs e)
        {
            ExportSessiesAsync();            
        }

        private void ExportSessiesAsync()
        {
            WeergaveInputWindow IW = new WeergaveInputWindow();
            // vraag de bestandsnaam aan de gebruiker
            if (IW.ShowDialog() == true)
            {
                try
                {
                    //creëer het pad
                    string filename = IW.Naam;
                    string url = _dataFolder + filename + ".csv";
                    // Laad de datagriddata in een array
                    var data = GetDataFromGrid(sessieDataGrid);
                    //Genereer een CSV bestand en open deze in Excel
                    Helper.ToExcel(url, data);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private string[][] GetDataFromGrid(DataGrid grid)
        {
            List<string[]> data = new List<string[]>();
            // Get Headers
            var headers = grid.Columns.Cast<DataGridColumn>();
            data.Add(headers.Select(column => "\"" + column.Header + "\"").ToArray());
            // Get Data
            foreach (Sessie sessie in grid.Items)
            {
                Seminarie seminarie = new SeminarieServices().GetSeminarieByID((decimal)sessie.Seminarie_ID);
                List<string> row = new List<string>();
                row.Add(sessie.ID.ToString());
                row.Add(sessie.SeminarieTitel);
                row.Add(sessie.Naam);
                row.Add(sessie.Datum.ToString());
                row.Add(sessie.Status);
                row.Add(sessie.LocatieNaam);
                row.Add(sessie.Status_Locatie);
                row.Add(sessie.Beginuur);
                row.Add(sessie.Einduur);
                row.Add(sessie.Standaard_prijs);
                row.Add(sessie.AantalDeelnemers);
                row.Add(sessie.Target == null ? "" : sessie.Target);
                row.Add(sessie.AantalGasten);
                row.Add(sessie.AantalSprekers);
                row.Add(sessie.TeFacturerenInschrijvingen.ToString());
                row.Add(sessie.NaamVTerplaatse);
                row.Add(sessie.OpTeVolgenKlanten.ToString());
                row.Add(seminarie.Weeknummer); // organisatie week
                row.Add(seminarie.Type);
                row.Add(seminarie.Start_marketing == null ? "" : seminarie.Start_marketing.ToString());
                row.Add(seminarie.Startdatum_organisatie == null ? "" : seminarie.Startdatum_organisatie.ToString());
                row.Add(seminarie.Deadline_organisatie == null ? "" : seminarie.Deadline_organisatie.ToString());
                row.Add(seminarie.AantalDeelnemers.ToString());
                data.Add(row.ToArray());
            }
            return data.ToArray();
        }


        private void ResetFilters()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
            MaakFiltersLeeg();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieFilterWeergaveComboBox.SetValue(ComboBox.SelectedIndexProperty, 0); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { sessieViewSource.SetValue(CollectionViewSource.SourceProperty, sessies); }, null);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);

        }

        private void SetWeergave(SessieWeergaven weergave)
        {
            ResetFilters();
            if (weergave.Locatie_ID != null)
                sessieFilterLocatieComboBox.Text = weergave.Locatie.Naam;
            if (weergave.InschrijvingStatus != null)
                sessieFilterStatusInschrijvingComboBox.Text = weergave.InschrijvingStatus;
            if (weergave.Taalcode != null)
                sessieFilterTaalComboBox.Text = weergave.Taalcode;
            if (weergave.Aantal_inschrijvingen != null)
                sessieFilterAantalInschrTextBox.Text = weergave.Aantal_inschrijvingen.ToString();
            if (weergave.Verantwoordelijke_sessie_ID != null)
                TerplaatseVerantwComboBox.Text = weergave.VerantSessieNaam;
            if (weergave.Verantwoordelijke_terplaatse_ID != null)
                SessieVerantwComboBox.Text = weergave.VerantSessieNaam;
            if (weergave.Erkenning_ID != null)
                erkenningComboBox.Text = weergave.ErkenningNaam;
            if (!string.IsNullOrEmpty(weergave.Erkenning_status))
                erkenningStatus.Text = weergave.Erkenning_status;
            if (!string.IsNullOrEmpty(weergave.Zoektekst))
                ZoekTextBox.Text = weergave.Zoektekst;
        }

    }
}

﻿using data = ConfocusDBLibrary;
using ConfocusClassLibrary;
using ITCLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Outlook = Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Outlook;
using ConfocusDBLibrary;
using System.Diagnostics;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for Aanwezigheden.xaml
    /// </summary>
    public partial class Aanwezigheden : System.Windows.Window
    {
        /// <summary>
        /// Private Members
        /// </summary>
        private CollectionViewSource deelnemerViewSource;
        private CollectionViewSource sessie_SprekerViewSource;
        private decimal? SeminarieNummer;
        private decimal? SessieNummer;
        private Gebruiker currentUser;
        private string _dataFolder = "";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="seminarienummer"></param>
        /// <param name="sessienummer"></param>
        /// <param name="user">Ingelogde gebruiker voor het meegeven van de gewijzigd/aangemaakt door aan de database</param>
        public Aanwezigheden(decimal? seminarienummer, decimal? sessienummer, Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            if (seminarienummer != null)
                this.SeminarieNummer = seminarienummer;
            if (sessienummer != null)
                this.SessieNummer = sessienummer;
        }

        /// <summary>
        /// Event als het venster geladen is haal dan de gegevens op uit de database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            deelnemerViewSource = ((CollectionViewSource)(this.FindResource("deelnemerViewSource")));
            sessie_SprekerViewSource = ((CollectionViewSource)(this.FindResource("sessie_SprekerViewSource")));
            _dataFolder = new data.Instelling_Service().Find().DataFolder;
            try
            {
                LoadComboBoxes();
                if (SeminarieNummer != null)
                    SetSeminarie();
                if (SessieNummer != null)
                    SetSessie();
                CollectionViewSource inschrijvingenViewSource = ((CollectionViewSource)(this.FindResource("inschrijvingenViewSource")));
                // Load data by setting the CollectionViewSource.Source property:
                // inschrijvingenViewSource.Source = [generic data source]
                GetInschrijvingen();
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het tonen van de afwezigheden venster!!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Zet de meegegeven sessie in de sessie combobox
        /// </summary>
        private void SetSessie()
        {
            SessieServices sService = new SessieServices();
            Sessie sessie = sService.GetSessieByID((decimal)SessieNummer);
            //juiste weergave naam selecteren
            sessieComboBox.Text = sessie.NaamDatumLocatie;
            sprekerSessieComboBox.Text = sessie.NaamDatumLocatie;
        }

        /// <summary>
        /// Zet de meegegeven seminarie in de seminarie combobox
        /// </summary>
        private void SetSeminarie()
        {
            SeminarieServices sService = new SeminarieServices();
            Seminarie seminarie = sService.GetSeminarieByID((Decimal)SeminarieNummer);
            seminarieComboBox.Text = seminarie.TitelEnDatum;
            sprekerSeminarieComboBox.Text = seminarie.TitelEnDatum;
        }

        /// <summary>
        /// Laad de gegevens in de seminarie en locatie comboboxen
        /// </summary>
        private void LoadComboBoxes()
        {
            SeminarieServices sService = new SeminarieServices();
            List<Seminarie> seminaries = sService.FindActive();
            // voeg een lege seminarie toe op de eerste plaats zodat ze deze kunnen selecteren om de combobox leeg te maken
            Seminarie firstSem = new Seminarie();
            seminaries.Insert(0, firstSem);
            seminarieComboBox.ItemsSource = seminaries;
            seminarieComboBox.DisplayMemberPath = "TitelEnDatum";
            sprekerSeminarieComboBox.ItemsSource = seminaries;
            sprekerSeminarieComboBox.DisplayMemberPath = "TitelEnDatum";

            LocatieServices lService = new LocatieServices();
            List<Locatie> locaties = lService.FindActive();
            nieuweLocatieComboBox.ItemsSource = locaties;
            nieuweLocatieComboBox.DisplayMemberPath = "Naam";
        }

        /// <summary>
        /// Event om het venster te sluiten
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true;
            }
            catch (System.Exception)
            {
                this.Close();
            }
        }

        private void seminarieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seminarieComboBox.SelectedIndex > 0)
            {
                Seminarie seminarie = (Seminarie)seminarieComboBox.SelectedItem;
                SessieServices sesService = new SessieServices();
                List<Sessie> sessies = sesService.GetSessiesBySeminarieID(seminarie.ID);
                // Maak een default sessie aan om met deze optie alle sessies te laden
                Sessie firstSessie = new Sessie();
                firstSessie.Naam = "Alle deelnemers";
                sessies.Insert(0, firstSessie);
                sessieComboBox.ItemsSource = sessies;
                sessieComboBox.DisplayMemberPath = "NaamDatumLocatie";
            }
        }

        private void sessieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadDeelnemers();
        }

        private void LoadSeminarieDeelnemers()
        {
            Seminarie seminarie = (Seminarie)seminarieComboBox.SelectedItem;
            if (seminarie != null)
            {
                List<Deelnemer> deelnemers = GetAlleSeminarieDeelnemers();
                deelnemerViewSource.Source = deelnemers;
            }
        }

        private void LoadDeelnemers()
        {
            if (sessieComboBox.SelectedIndex == 0)
            {
                //Todo: Alle deelnemers van alle sessies van de gekozen seminarie ophalen
                //
                List<Deelnemer> deelnemers = GetAlleSeminarieDeelnemers();
                deelnemerViewSource.Source = deelnemers;
            }
            else if (sessieComboBox.SelectedIndex > 0)
            {
                // TODO: Alle deelnemers van de gekozen sessie ophalen
                //List<Deelnemer> deelnemers = GetAlleSessieDeelnemers();
                //deelnemerViewSource.Source = deelnemers;
                //TODO: Alle Sprekers van de Sessie ophalen.
                //List<Sessie_Spreker> sprekers = GetSprekersVanSessie();
                //sessie_SprekerViewSource.Source = sprekers;
            }

        }

        /// <summary>
        /// Laad alle sprekers van de geselecteerde sessie
        /// </summary>
        /// <returns></returns>
        private List<Sessie_Spreker> GetSprekersVanSessie()
        {
            Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
            SessieSprekerServices sService = new SessieSprekerServices();
            List<Sessie_Spreker> sprekers = sService.GetSprekersBySessieID(sessie.ID);
            return sprekers;
        }

        /// <summary>
        /// Laad alle deelnemers van de geselecteerde seminarie
        /// </summary>
        /// <returns></returns>
        private List<Deelnemer> GetAlleSeminarieDeelnemers()
        {
            Seminarie seminarie = (Seminarie)seminarieComboBox.SelectedItem;
            SessieServices sesService = new SessieServices();
            List<Deelnemer> alleDeelnemers = new List<Deelnemer>();
            List<Sessie> sessies = sesService.GetSessiesBySeminarieID(seminarie.ID);
            foreach (Sessie sessie in sessies)
            {
                InschrijvingenService iService = new InschrijvingenService();
                List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = iService.GetInschrijvingBySessieID(sessie.ID);

                DeelnemerServices dService = new DeelnemerServices();
                foreach (ConfocusDBLibrary.Inschrijvingen inschrijving in inschrijvingen)
                {
                    List<Deelnemer> deelnemers = dService.GetDeelmenersByInschrijvingID(inschrijving.ID);
                    foreach (Deelnemer deelnemer in deelnemers)
                    {
                        alleDeelnemers.Add(deelnemer);
                    }
                }
            }

            var sorted = (from d in alleDeelnemers orderby d.Achternaam select d).ToList();

            return sorted;
        }

        /// <summary>
        /// Haald alle sessiedeelnemers op.
        /// </summary>
        /// <returns>Lijst van Inschrijvingen</returns>
        private List<data.Inschrijvingen> GetAlleSessieDeelnemers()
        {
            try
            {

                List<data.Inschrijvingen> inschrijvingen = new List<data.Inschrijvingen>();
                foreach (var item in inschrijvingenDataGrid.Items)
                {
                    inschrijvingen.Add(item as data.Inschrijvingen);
                }
                inschrijvingen = inschrijvingen.OrderBy(i => i.Functies.Contact.Achternaam).ToList();
                return inschrijvingen;
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout ophalen deelnemers: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                return null;
            }
        }

        /// <summary>
        /// Event genereerd een document met de een overzicht van de deelnemers van de sessie / seminarie 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toWordButton_Click(object sender, RoutedEventArgs e)
        {

            if (inschrijvingenDataGrid.Items.Count > 0)
            {
                if (sessieComboBox.SelectedIndex > 0)
                {
                    Sessie sessie = ((ConfocusDBLibrary.Inschrijvingen)inschrijvingenDataGrid.Items[0]).Sessie;
                    string taal = sessie.SeminarieTaalCode;

                    List<Sessie_Spreker> Sprekers = GetSprekersVanSessie();
                    List<List<data.Inschrijvingen>> deelnemersLijsten = GetDeelnemersLijsten();

                    CreateDefaultAanwezigheidsLijstDocument(Sprekers, deelnemersLijsten, taal);
                    //CreateAanwezigheidsLijstPerAttest(Sprekers, deelnemersLijsten, taal);
                    CreateDefaultAftekenLijst(Sprekers, deelnemersLijsten, taal);
                }
                else
                {
                    //Seminarie seminarie = new SeminarieServices().GetSeminarieByID
                    string taal = ((Seminarie)seminarieComboBox.SelectedItem).Taalcode;
                    List<Sessie_Spreker> Sprekers = GetSprekersVanSeminarie(((Seminarie)seminarieComboBox.SelectedItem).ID);
                    List<data.Inschrijvingen> deelnemersLijsten = GetDeelnemersLijstenFromSeminarie(((Seminarie)seminarieComboBox.SelectedItem).ID);

                    CreateSeminarieOverzichtLijst(Sprekers, deelnemersLijsten, taal);
                }

            }
        }

        /// <summary>
        /// Creeerd een lijst van Sprekers en deelnemers van een seminarie
        /// </summary>
        /// <param name="sprekers"></param>
        /// <param name="deelnemersLijsten"></param>
        /// <param name="taal"></param>
        private void CreateSeminarieOverzichtLijst(List<Sessie_Spreker> sprekers, List<data.Inschrijvingen> deelnemersLijsten, string taal)
        {
            int totaalrijen = deelnemersLijsten.Count + sprekers.Count;
            List<PrintableSeminarieDeelnemer> deelnemerlist = new List<PrintableSeminarieDeelnemer>();
            string[][] arrdeelnemers = new string[totaalrijen + 4][];
            if (taal == "FR")
                arrdeelnemers[0] = new string[8] { "Session", "Nom", "Prénom", "Société", "Fonction", "heure de début", "heure de fin", "" };
            else
                arrdeelnemers[0] = new string[8] { "Sessie", "Naam", "Voornaam", "Bedrijf", "Functie", "Beginuur", "Einduur", "" };
            SprekerServices sService = new SprekerServices();
            for (int i = 0; i < sprekers.Count; i++)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)sprekers[i];
                Spreker fullspreker = sService.GetSprekerById(spreker.Spreker_ID);
                string sessie = spreker.Sessie.Naam;
                string voornaam = spreker.Spreker.Contact.Voornaam;
                string achternaam = (fullspreker.Contact == null || fullspreker.Contact.Achternaam == "") ? "z" : fullspreker.Contact.Achternaam;
                string bedrijf = fullspreker.Bedrijf.Naam;
                string functie = fullspreker.Oorspronkelijke_Titel;
                string type = "1";
                string beginuur = spreker.Sessie.Beginuur;
                string einduur = spreker.Sessie.Einduur;
                //arrdeelnemers[rijteller + 1] = new string[5] { achternaam, voornaam, bedrijf, functie, type };
                //rijteller++;
                deelnemerlist.Add(new PrintableSeminarieDeelnemer(sessie, achternaam, voornaam, bedrijf, functie, beginuur, einduur, type, false));
            }

            InschrijvingenService dservice = new InschrijvingenService();
            BedrijfServices bservice = new BedrijfServices();

            foreach (data.Inschrijvingen deelnemer in deelnemersLijsten)
            {
                if (deelnemer.Status == "Inschrijving" || deelnemer.Status == "Gast")
                {

                    data.Inschrijvingen fulldeelnemer = dservice.GetInschrijvingByID(deelnemer.ID);
                    if (!fulldeelnemer.Sessie.Naam.ToLower().Contains("lunch"))
                    {
                        string sessie = fulldeelnemer.Sessie.Naam;
                        string naam = "z";
                        string voornaam = "";
                        if (fulldeelnemer.Functies.Contact != null)
                        {
                            naam = fulldeelnemer.Functies.Contact.Achternaam;
                            voornaam = fulldeelnemer.Functies.Contact.Voornaam;

                        }
                        string bedrijf = "";
                        Bedrijf curbedrijf = bservice.GetBedrijfByID((decimal)fulldeelnemer.Functies.Bedrijf_ID);
                        if (curbedrijf != null)
                            bedrijf = curbedrijf.Naam;
                        string functie = fulldeelnemer.Functies.Oorspronkelijke_Titel;
                        string type = "0";
                        string beginuur = fulldeelnemer.Sessie.Beginuur;
                        string einduur = fulldeelnemer.Sessie.Einduur;
                        //arrdeelnemers[rijteller + 1] = new string[5] { naam, voornaam, bedrijf, functie, "0" };
                        //rijteller++;
                        deelnemerlist.Add(new PrintableSeminarieDeelnemer(sessie, naam, voornaam, bedrijf, functie, beginuur, einduur, type, false));
                    }
                }
            }

            var sorteddeelnemers = (from d in deelnemerlist
                                    orderby d.Achternaam
                                    select d).ToList();
            int j = 1;
            foreach (var item in sorteddeelnemers)
            {
                if (item is PrintableSeminarieDeelnemer && item != null)
                {
                    arrdeelnemers[j] = new string[8] { item.Sessie, item.Achternaam, item.Voornaam, item.Bedrijf, item.Functie, item.Beginuur, item.Einduur, item.Type };
                    j++;
                }
            }
            for (int k = j; k < (j + 3); k++)
            {
                arrdeelnemers[k] = new string[8] { "", "", "", "", "", "", "", "" };
            }

            CreateSeminarieLijst(arrdeelnemers, taal);

        }

        /// <summary>
        /// Creeerd een EXCEL document aftekenlijst voor alle deelnemers
        /// </summary>
        /// <param name="arrdeelnemers"></param>
        /// <param name="taal"></param>
        private void CreateSeminarieLijst(string[][] arrdeelnemers, string taal)
        {

            string uri = _dataFolder + "Templates\\Aftekenlijst.xltx";
            int[] widths = new int[8] { 50, 20, 15, 23, 23, 12, 12, 0 };
            ExcelDocument excel = new ExcelDocument(uri);
            excel.InsertText(((Seminarie)seminarieComboBox.SelectedItem).Titel, 1, 1, 1, true);
            excel.InsertList(arrdeelnemers, true, 3, widths, 47);
        }

        private List<data.Inschrijvingen> GetDeelnemersLijstenFromSeminarie(decimal iD)
        {
            List<data.Inschrijvingen> alleinschrijvingen = new InschrijvingenService().GetInschrijvingBySeminarieID(iD);
            return alleinschrijvingen.OrderBy(i => i.Sessie.Beginuur).OrderBy(i => i.Functies.Contact.VolledigeNaam).ToList();
        }

        private List<Sessie_Spreker> GetSprekersVanSeminarie(decimal iD)
        {
            SessieSprekerServices sService = new SessieSprekerServices();
            List<Sessie_Spreker> sprekers = sService.GetSprekersBySeminarieID(iD);
            return sprekers;
        }

        private void CreateDefaultAftekenLijst(List<Sessie_Spreker> Sprekers, List<List<data.Inschrijvingen>> deelnemersLijsten, string taal)
        {
            int totaalrijen = inschrijvingenDataGrid.Items.Count + Sprekers.Count;
            List<PrintableDeelnemer> deelnemerlist = new List<PrintableDeelnemer>();
            string[][] arrdeelnemers = new string[totaalrijen + 4][];
            if (taal == "FR")
                arrdeelnemers[0] = new string[7] { "Nom", "Prénom", "Société", "Fonction", "Signature", "Signature", "" };
            else
                arrdeelnemers[0] = new string[7] { "Naam", "Voornaam", "Bedrijf", "Functie", "Handtekening (aanvang)", "Handtekening (einde)", "" };
            for (int i = 0; i < Sprekers.Count; i++)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)Sprekers[i];
                //spreker fullspreker = sservice.getsprekerbyid(spreker.spreker_id);
                string voornaam = spreker.Spreker.Contact.Voornaam;
                string achternaam = (spreker.Spreker.Contact == null || spreker.Spreker.Contact.Achternaam == "") ? "z" : spreker.Spreker.Contact.Achternaam;
                string bedrijf = spreker.Spreker.Bedrijf.Naam;
                string functie = spreker.Spreker.Oorspronkelijke_Titel;
                string type = "1";
                //arrdeelnemers[rijteller + 1] = new string[5] { achternaam, voornaam, bedrijf, functie, type };
                //rijteller++;
                deelnemerlist.Add(new PrintableDeelnemer(achternaam, voornaam, bedrijf, functie, type, false));
            }

            InschrijvingenService dservice = new InschrijvingenService();
            BedrijfServices bservice = new BedrijfServices();

            foreach (List<data.Inschrijvingen> deelnemers in deelnemersLijsten)
            {
                foreach (data.Inschrijvingen deelnemer in deelnemers)
                {
                    if (deelnemer.Status == "Inschrijving" || deelnemer.Status == "Gast")
                    {

                        data.Inschrijvingen fulldeelnemer = dservice.GetInschrijvingByID(deelnemer.ID);
                        string naam = "z";
                        string voornaam = "";
                        if (fulldeelnemer.Functies.Contact != null)
                        {
                            naam = fulldeelnemer.Functies.Contact.Achternaam;
                            voornaam = fulldeelnemer.Functies.Contact.Voornaam;

                        }
                        string bedrijf = "";
                        Bedrijf curbedrijf = bservice.GetBedrijfByID((decimal)fulldeelnemer.Functies.Bedrijf_ID);
                        if (curbedrijf != null)
                            bedrijf = curbedrijf.Naam;
                        string functie = fulldeelnemer.Functies.Oorspronkelijke_Titel;
                        string type = "0";
                        //arrdeelnemers[rijteller + 1] = new string[5] { naam, voornaam, bedrijf, functie, "0" };
                        //rijteller++;
                        deelnemerlist.Add(new PrintableDeelnemer(naam, voornaam, bedrijf, functie, type, false));
                    }
                }
            }

            var sorteddeelnemers = (from d in deelnemerlist
                                    orderby d.Achternaam
                                    select d).ToList();
            int j = 1;
            foreach (var item in sorteddeelnemers)
            {
                if (item is PrintableDeelnemer && item != null)
                {
                    arrdeelnemers[j] = new string[7] { item.Achternaam, item.Voornaam, item.Bedrijf, item.Functie, "", "", item.Type };
                    j++;
                }
            }

            for (int k = j; k < (j + 3); k++)
            {
                arrdeelnemers[k] = new String[7] { "", "", "", "", "", "", "" };
            }

            AlgemeneAftekenExcel(arrdeelnemers, taal);

        }

        private void CreateAanwezigheidsLijstPerAttest(List<Sessie_Spreker> Sprekers, List<List<data.Inschrijvingen>> deelnemersLijsten, string taal)
        {
            int totaalRijen = inschrijvingenDataGrid.Items.Count + Sprekers.Count;
            int rijTeller = 1;
            bool Toevoegen = true;
            String[][] arrDeelnemers = new String[totaalRijen + 4][];
            string[] colomkoppen;
            if (taal == "FR")
                colomkoppen = new string[7] { "Nom", "Prénom", "Société", "Fonction", "Signature", "Signature", "0" };
            else
                colomkoppen = new string[7] { "Naam", "Voornaam", "Bedrijf", "Functie", "Handtekening (aanvang)", "Handtekening (einde)", "0" };
            //var colomkoppen = new String[7] { "Naam", "Voornaam", "Bedrijf", "Functie", "Handtekening", "Handtekening", "0" };
            List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
            for (int i = 0; i < Sprekers.Count; i++)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)Sprekers[i];
                sprekers.Add(spreker);
                //Spreker fullSpreker = sService.GetSprekerById(spreker.Spreker_ID);
            }

            InschrijvingenService dService = new InschrijvingenService();
            BedrijfServices bService = new BedrijfServices();

            foreach (List<data.Inschrijvingen> deelnemers in deelnemersLijsten)
            {
                List<PrintableDeelnemer> deelnemerlist = new List<PrintableDeelnemer>();
                String[][] Deelnemers = new String[deelnemers.Count + 4][];
                String attesttype = "";
                int aantalSprekers = 0;
                bool first = true;
                foreach (data.Inschrijvingen deelnemer in deelnemers)
                {

                    data.Inschrijvingen fullDeelnemer = dService.GetInschrijvingByID(deelnemer.ID);
                    attesttype = (fullDeelnemer.Functies.AttestType1 == null) ? "Administratie" : fullDeelnemer.Functies.AttestType1.Naam;

                    if (fullDeelnemer.Status == "Inschrijving" || fullDeelnemer.Status == "Gast")
                    {
                        if (first)
                        {
                            if (sprekers.Count > 0)
                            {
                                var gevondensprekers = (from s in sprekers
                                                        where s.SprekerAttesttype == attesttype
                                                        || (s.SprekerAttesttype == "" && attesttype == "Administratie")
                                                        select s).ToList();

                                Deelnemers = new String[deelnemers.Count + 4 + gevondensprekers.Count][];
                                Deelnemers[0] = colomkoppen;
                                aantalSprekers = gevondensprekers.Count;
                                rijTeller = 1 + gevondensprekers.Count;

                                string type = "1";
                                if (gevondensprekers.Count > 0)
                                {
                                    int teller = 1;
                                    foreach (Sessie_Spreker spr in gevondensprekers)
                                    {
                                        string achternaam = spr.Spreker.Contact.Achternaam;
                                        deelnemerlist.Add(new PrintableDeelnemer(achternaam, spr.Spreker.Contact.Voornaam, spr.Spreker.Bedrijf.Naam, spr.Spreker.Oorspronkelijke_Titel, type, false));
                                        //Deelnemers[teller] = new String[7] { spr.Spreker.Contact.Achternaam, spr.Spreker.Contact.Voornaam, spr.Spreker.currentBedrijf.Naam, spr.Spreker.Functie, type, "", "" };
                                        //teller++;
                                    }
                                }

                            }
                            else
                            {
                                Deelnemers[0] = colomkoppen;
                                rijTeller = 1;
                            }
                            first = false;
                        }



                        String naam = "z";
                        String voornaam = "";
                        if (fullDeelnemer.Functies.Contact != null)
                        {
                            naam = fullDeelnemer.Functies.Contact.Achternaam;
                            voornaam = fullDeelnemer.Functies.Contact.Voornaam;
                        }
                        String bedrijf = "";
                        Bedrijf curBedrijf = bService.GetBedrijfByID((Decimal)fullDeelnemer.Functies.Bedrijf_ID);
                        if (curBedrijf != null)
                            bedrijf = curBedrijf.Naam;
                        String functie = fullDeelnemer.Functies.Oorspronkelijke_Titel;
                        String myType = "0";
                        deelnemerlist.Add(new PrintableDeelnemer(naam, voornaam, bedrijf, functie, myType, false));
                        //Deelnemers[rijTeller] = new String[6] { naam, voornaam, bedrijf, functie, "", "" };
                        //rijTeller++;
                    }
                }

                var gesorteerdelijst = (from d in deelnemerlist
                                        orderby d.Achternaam
                                        select d).ToList();

                int j = 1;
                foreach (var item in gesorteerdelijst)
                {
                    if (item is PrintableDeelnemer && item != null)
                    {
                        Deelnemers[j] = new String[7] { item.Achternaam, item.Voornaam, item.Bedrijf, item.Functie, "", "", item.Type };
                        j++;
                    }
                }


                if (Deelnemers.Length > 0)
                {
                    for (int k = j; k < (j + 3); k++)
                    {
                        Deelnemers[k] = new String[7] { "", "", "", "", "", "", "" };
                    }
                    DeelnemersExcel(Deelnemers, attesttype, taal);

                }
                //DeelnemersPerAttestDoc(Deelnemers, attesttype, aantalSprekers);
            }
        }

        private void DeelnemersExcel(string[][] deelnemers, string attesttype, string taal)
        {
            string uri = _dataFolder + "Templates\\Aftekenlijst.xltx";
            int[] widths = new int[7] { 15, 15, 25, 19, 23, 23, 0 };
            ExcelDocument excel = new ExcelDocument(uri);
            excel.InsertText(((Seminarie)seminarieComboBox.SelectedItem).Titel + ": " + ((Sessie)sessieComboBox.SelectedItem).Naam, 1, 1, 1, true);
            if (taal == "FR")
                excel.InsertText(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " de " + ((Sessie)sessieComboBox.SelectedItem).Beginuur + " à " + ((Sessie)sessieComboBox.SelectedItem).Einduur + " au " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam, 2, 1, 1, true);
            else
                excel.InsertText(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " van " + ((Sessie)sessieComboBox.SelectedItem).Beginuur + " tot " + ((Sessie)sessieComboBox.SelectedItem).Einduur + " in " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam, 2, 1, 1, true);
            //excel.InsertText("Dit is seminarie 1 sessie : Geen idee of dit werkt", 1, 1, 5, true);
            //excel.InsertText("Datum, (dagtype) locatie", 3, 1, 5, false);
            excel.InsertTextWithBorder(attesttype, 4, 5, 0);
            excel.InsertList(deelnemers, true, 5, widths, 47);
            //excel.InsertImage(@"C:\Confocus Data\Images\logo.png", 290, 10);

        }



        private void CreateDefaultAanwezigheidsLijstDocument(List<Sessie_Spreker> Sprekers, List<List<data.Inschrijvingen>> deelnemersLijsten, string taal)
        {
            int totaalrijen = inschrijvingenDataGrid.Items.Count + Sprekers.Count;
            List<PrintableDeelnemer> deelnemerlist = new List<PrintableDeelnemer>();
            string[][] arrdeelnemers = new string[totaalrijen + 1][];
            if (taal == "FR")
                arrdeelnemers[0] = new string[5] { "Nom", "Prénom", "Société", "Fonction", "" };
            else
                arrdeelnemers[0] = new string[5] { "Naam", "Voornaam", "Bedrijf", "Functie", "" };
            for (int i = 0; i < Sprekers.Count; i++)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)Sprekers[i];
                //spreker fullspreker = sservice.getsprekerbyid(spreker.spreker_id);
                string voornaam = spreker.Spreker.Contact.Voornaam;
                string achternaam = (spreker.Spreker.Contact == null || spreker.Spreker.Contact.Achternaam == "") ? "z" : spreker.Spreker.Contact.Achternaam;
                string bedrijf = spreker.Spreker.Bedrijf.Naam;
                string functie = spreker.Spreker.Oorspronkelijke_Titel;
                string type = "1";
                //arrdeelnemers[rijteller + 1] = new string[5] { achternaam, voornaam, bedrijf, functie, type };
                //rijteller++;
                deelnemerlist.Add(new PrintableDeelnemer(achternaam, voornaam, bedrijf, functie, type, false));
            }

            InschrijvingenService dservice = new InschrijvingenService();
            BedrijfServices bservice = new BedrijfServices();

            foreach (List<data.Inschrijvingen> deelnemers in deelnemersLijsten)
            {
                foreach (data.Inschrijvingen deelnemer in deelnemers)
                {
                    //if (deelnemer.Status == "Inschrijving" || deelnemer.Status == "Gast")
                    //{
                    data.Inschrijvingen fulldeelnemer = dservice.GetInschrijvingByID(deelnemer.ID);
                    string naam = "z";
                    string voornaam = "";
                    if (fulldeelnemer.Functies.Contact != null)
                    {
                        naam = fulldeelnemer.Functies.Contact.Achternaam;
                        voornaam = fulldeelnemer.Functies.Contact.Voornaam;

                    }
                    string bedrijf = "";
                    Bedrijf curbedrijf = bservice.GetBedrijfByID((decimal)fulldeelnemer.Functies.Bedrijf_ID);
                    if (curbedrijf != null)
                        bedrijf = curbedrijf.Naam;
                    string functie = fulldeelnemer.Functies.Oorspronkelijke_Titel;
                    string type = "0";
                    bool cursief = false;
                    if (deelnemer.Status == "Geannuleerd" || deelnemer.Status == "Verplaatst")
                    {
                        cursief = true;
                    }
                    //arrdeelnemers[rijteller + 1] = new string[5] { naam, voornaam, bedrijf, functie, "0" };
                    //rijteller++;
                    deelnemerlist.Add(new PrintableDeelnemer(naam, voornaam, bedrijf, functie, type, cursief));

                    //}
                }
            }

            var sorteddeelnemers = (from d in deelnemerlist
                                    orderby d.Achternaam
                                    select d).ToList();
            int j = 1;
            foreach (var item in sorteddeelnemers)
            {
                if (item is PrintableDeelnemer && item != null)
                {
                    arrdeelnemers[j] = new string[6] { item.Achternaam, item.Voornaam, item.Bedrijf, item.Functie, item.Type, item.Cursief.ToString() };
                    j++;
                }
            }

            //for (int k = j; k < (j + 3); k++)
            //{
            //    arrdeelnemers[k] = new string[6] { "", "", "", "", "", "false" };
            //}



            AlgemeneDeelnemersExcel(arrdeelnemers, taal);
            //AlgemeneDeelnemersLijstDoc(arrdeelnemers, Sprekers.Count);
        }

        private void AlgemeneDeelnemersExcel(string[][] arrdeelnemers, string taal)
        {
            string uri = _dataFolder + "Templates\\Deelnemerslijst.xltx";
            int[] widths = new int[7] { 17, 17, 46, 36, 0, 0, 0 };
            ExcelDocument excel = new ExcelDocument(uri);
            excel.InsertText(((Seminarie)seminarieComboBox.SelectedItem).Titel + ": " + ((Sessie)sessieComboBox.SelectedItem).Naam, 1, 1, 1, true);
            if (taal == "FR")
            {
                excel.InsertText(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " de " + ((Sessie)sessieComboBox.SelectedItem).Beginuur + " à " + ((Sessie)sessieComboBox.SelectedItem).Einduur + " au " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam, 2, 1, 1, true);
            }
            else
                excel.InsertText(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " van " + ((Sessie)sessieComboBox.SelectedItem).Beginuur + " tot " + ((Sessie)sessieComboBox.SelectedItem).Einduur + " in " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam, 2, 1, 1, true);
            excel.InsertList(arrdeelnemers, true, 5, widths, 20);
            //excel.InsertImage(@"C:\Confocus Data\Images\logo.png", 510, 10);
        }

        private void AlgemeneAftekenExcel(string[][] arrdeelnemers, string taal)
        {
            string uri = _dataFolder + "Templates\\Aftekenlijst.xltx";
            int[] widths = new int[7] { 15, 15, 25, 19, 23, 23, 0 };
            ExcelDocument excel = new ExcelDocument(uri);
            excel.InsertText(((Seminarie)seminarieComboBox.SelectedItem).Titel + ": " + ((Sessie)sessieComboBox.SelectedItem).Naam, 1, 1, 1, true);
            if (taal == "FR")
                excel.InsertText(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " de " + ((Sessie)sessieComboBox.SelectedItem).Beginuur + " à " + ((Sessie)sessieComboBox.SelectedItem).Einduur + " au " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam, 2, 1, 1, true);
            else
                excel.InsertText(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " van " + ((Sessie)sessieComboBox.SelectedItem).Beginuur + " tot " + ((Sessie)sessieComboBox.SelectedItem).Einduur + " in " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam, 2, 1, 1, true);
            excel.InsertList(arrdeelnemers, true, 5, widths, 47);
            //excel.InsertImage(@"C:\Confocus Data\Images\logo.png", 510, 10);
        }


        private List<List<data.Inschrijvingen>> GetDeelnemersLijsten()
        {
            List<List<data.Inschrijvingen>> lijsten = new List<List<data.Inschrijvingen>>();
            List<data.Inschrijvingen> alledeelnemers = new List<data.Inschrijvingen>();
            foreach (data.Inschrijvingen item in inschrijvingenDataGrid.Items)
            {
                if (item.Functies.AttestType1 != null)
                {
                    alledeelnemers.Add(item);
                }
                else
                {
                    Attesttype_Services aService = new Attesttype_Services();
                    AttestType type = aService.GetAdminitratiefType();
                    item.Functies.AttestType1 = type;
                    alledeelnemers.Add(item);
                }
            }

            var gesorteerdeLijst = from d in alledeelnemers
                                   orderby d.Functies.Contact.Achternaam
                                   group d by d.Functies.AttestType1.Naam into AttestGroep
                                   select new { AttestGroep, AttestType = AttestGroep.Key };

            foreach (var groepitem in gesorteerdeLijst)
            {
                List<data.Inschrijvingen> groepsdeelnemer = new List<data.Inschrijvingen>();
                foreach (data.Inschrijvingen deelnemer in groepitem.AttestGroep)
                {
                    groepsdeelnemer.Add(deelnemer);
                }
                lijsten.Add(groepsdeelnemer);
            }
            return lijsten;
        }

        private Bedrijf GetBedrijfBySprekerID(decimal id)
        {
            SprekerServices sService = new SprekerServices();
            Bedrijf bedrijf = sService.GetSprekerBedrijf(id);
            return bedrijf;
        }

        private Contact GetContactBySprekerID(decimal id)
        {
            SprekerServices sService = new SprekerServices();
            Contact contact = sService.GetSprekerContact(id);
            return contact;
        }

        private void AlgemeneDeelnemersLijstDoc(String[][] deelnemers, int aantalSprekers = 0)
        {
            if (deelnemers[0] != null)
            {
                String titel = "";
                DateTime datum = DateTime.Now;
                WordDocument doc = new WordDocument(titel, "Liggend");

                int[] colwidths = new int[4] { 100, 100, 250, 250 };
                int colheight = 30;

                doc.addPictureRight(@_dataFolder + "Images\\logo.png", 150, 60);
                doc.setAlignment(WordDocument.Alignment.Left);
                doc.setFontsize(10);
                doc.toggleBold();
                doc.toggleUnderline();
                doc.setFontsize(12);
                doc.addLine(((Seminarie)seminarieComboBox.SelectedItem).Titel + ": " + ((Sessie)sessieComboBox.SelectedItem).Naam);
                doc.setFontsize(10);
                doc.toggleUnderline();
                doc.toggleBold();
                if (sessieComboBox.Text != "Alle sessies")
                    doc.addLine(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " in " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam);
                doc.addTable(deelnemers, true, colwidths, colheight, aantalSprekers);
            }
        }

        private void DeelnemersPerAttestDoc(String[][] deelnemers, String attesttype = "", int aantalSprekers = 0)
        {
            if (deelnemers[0] != null)
            {
                String titel = "";
                DateTime datum = DateTime.Now;
                WordDocument doc = new WordDocument(titel, "Staand");
                int[] colwidths = new int[6] { 80, 80, 80, 80, 80, 80 };
                int colheight = 45;

                doc.addPictureRight(_dataFolder + "Images\\logo.png", 150, 60);
                doc.setAlignment(WordDocument.Alignment.Left);
                doc.setFontsize(10);
                doc.toggleBold();
                doc.toggleUnderline();
                doc.setFontsize(12);
                doc.addLine(((Seminarie)seminarieComboBox.SelectedItem).Titel + ": " + ((Sessie)sessieComboBox.SelectedItem).Naam);
                doc.setFontsize(10);
                doc.toggleUnderline();
                doc.toggleBold();
                if (sessieComboBox.Text != "Alle sessies")
                    doc.addLine(((DateTime)((Sessie)sessieComboBox.SelectedItem).Datum).ToShortDateString() + " in " + ((Sessie)sessieComboBox.SelectedItem).Locatie.Naam);
                if (attesttype != "")
                    doc.addAttestTable(deelnemers, true, colwidths, colheight, aantalSprekers, attesttype);
                //else
                //    doc.addTable(deelnemers, true, colwidths, colheight, sessie_SprekerDataGrid.Items.Count);        
                //doc.save();
            }
        }

        private void mailButton_Click(object sender, RoutedEventArgs e)
        {
            var type = AttestTemplateComboBox.Text;
            switch (type)
            {
                case "Aanwezig":
                    MailAttesten();
                    break;
                case "Annulatie":
                    MailAnnulatie();
                    break;
                case "Afwezig":
                    MailAfwezig();
                    break;
                case "Inhouse":
                    MailInhouse();
                    break;
                case "Locatiewijziging":
                    MailLocatieWijziging();
                    break;
                case "Reminder":
                    MailReminder();
                    break;
                case "Verplaatsing":
                    MailVerplaatsing();
                    break;
                case "Blanco":
                    MailBlanco();
                    break;
            }

        }

        private void MailInhouse()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                bool rechtsdag = false;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtsdag = true;
                }
                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    data.Inschrijvingen fullInschrijving = new ConfocusDBLibrary.InschrijvingenService().GetInschrijvingByID(inschrijving.ID);
                    if (fullInschrijving.Aanwezig == true)
                    {
                        Taal taal = GetTaal(fullInschrijving);//tService.GetTaalByID((decimal)fullInschrijving.Functies.Contact.Taal_ID);
                        string mailbody = StelMailBodyOpPlanning(sessie, fullInschrijving, taal.Code, rechtsdag);

                        Functies functie = GetFunctieFromDeelnemer(fullInschrijving);
                        string Mailto = functie.Email;
                        if (!string.IsNullOrEmpty(Mailto))
                        {
                            try
                            {
                                ConfocusClassLibrary.Attachment attest = GetAanwezigheidsAttest(fullInschrijving, taal);
                                if (attest != null)
                                {
                                    Outlook.Application oApp = new Outlook.Application();
                                    Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                    DateTime mydate = new DateTime(2012, 01, 01);
                                    oMailItem.Attachments.Add(attest.Link, Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                                    if (taal.Code == "FR")
                                        if (sessie != null)
                                            oMailItem.Subject = "Votre présence á la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                        else
                                            oMailItem.Subject = "Votre présence á la formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                    else
                                        if (sessie != null)
                                        oMailItem.Subject = "Uw aanwezigheid: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                    else
                                        oMailItem.Subject = "Uw aanwezigheid: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                    oMailItem.To = Mailto;
                                    if (!string.IsNullOrEmpty(fullInschrijving.CC_Email))
                                        oMailItem.CC = fullInschrijving.CC_Email;
                                    oMailItem.HTMLBody = mailbody;
                                    oMailItem.Importance = Outlook.OlImportance.olImportanceHigh;
                                    oMailItem.Display(false);
                                    //oMailItem.Save();
                                    //oMailItem.Close( OlInspectorClose.olSave);
                                    //oMailItem.Send();

                                    List<string> mailoptions = new List<string>();
                                    mailoptions.Add("Evaluatie_ontvangen");
                                    mailoptions.Add("Attest_Verstuurd");
                                    mailoptions.Add("Inhouse_Verstuurd");
                                    if (sessieComboBox.SelectedIndex < 1)
                                        SetInschrijvingMailOptions(inschrijving, mailoptions, true);
                                    else
                                        SetInschrijvingMailOptions(inschrijving, mailoptions, false);

                                }
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(functie.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }

                    }
                }

            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void MailLocatieWijziging()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                bool rechtsdag = false;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtsdag = true;
                }
                //string mailbody = StelMailBodyOpPlanning(sessie, inschrijvingen[0], "NL");
                string mailbodyNl = "";
                string mailbodyFr = "";
                string MailtoNl = "";
                string MailtoFr = "";
                List<string> mailsnl = new List<string>();
                List<string> mailsfr = new List<string>();
                int aantalMailadressenNl = 0;
                int aantalMailadressenFr = 0;
                int i = 0;
                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    if (inschrijving.Status == "Gast" || inschrijving.Status == "Inschrijving")
                    {

                        Functies functie = GetFunctieFromDeelnemer(inschrijving);
                        if (!string.IsNullOrEmpty(functie.Email))
                        {
                            if (functie.Contact.Taal.Code == "FR")
                            {
                                if (mailbodyFr == "")
                                    mailbodyFr = StelMailBodyOpPlanning(sessie, inschrijvingen[i], "FR", rechtsdag);
                                if (aantalMailadressenFr > 9)
                                {
                                    aantalMailadressenFr = 0;
                                    mailsfr.Add(MailtoFr);
                                    MailtoFr = functie.Email + "; ";
                                    aantalMailadressenFr++;
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoFr += inschrijving.CC_Email + ";";
                                        aantalMailadressenFr++;
                                    }
                                }
                                else
                                {
                                    MailtoFr += functie.Email + "; ";
                                    aantalMailadressenFr++;

                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoFr += inschrijving.CC_Email + ";";
                                        aantalMailadressenFr++;
                                    }
                                }
                            }
                            else
                            {
                                if (mailbodyNl == "")
                                    mailbodyNl = StelMailBodyOpPlanning(sessie, inschrijvingen[i], "NL", rechtsdag);
                                if (aantalMailadressenNl > 9)
                                {
                                    aantalMailadressenNl = 0;
                                    mailsnl.Add(MailtoNl);
                                    MailtoNl = functie.Email + "; ";
                                    aantalMailadressenNl++;
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoNl += inschrijving.CC_Email + ";";
                                        aantalMailadressenNl++;
                                    }
                                }
                                else
                                {
                                    MailtoNl += functie.Email + "; ";
                                    aantalMailadressenNl++;
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoNl += inschrijving.CC_Email + ";";
                                        aantalMailadressenNl++;
                                    }
                                }
                            }
                            List<string> mailoptions = new List<string>();
                            mailoptions.Add("Locatiewijziging_verstuurd");
                            if (sessieComboBox.SelectedIndex < 1)
                                SetInschrijvingMailOptions(inschrijving, mailoptions, true);
                            else
                                SetInschrijvingMailOptions(inschrijving, mailoptions, false);
                        }
                    }
                }
                if (!mailsfr.Contains(MailtoFr))
                    mailsfr.Add(MailtoFr);
                if (!mailsnl.Contains(MailtoNl))
                    mailsnl.Add(MailtoNl);
                if (mailsnl.Count > 0)
                {
                    foreach (string mailto in mailsnl)
                    {
                        if (!String.IsNullOrEmpty(mailto))
                        {
                            try
                            {
                                Outlook.Application oApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                if (sessie != null)
                                    oMailItem.Subject = "Locatiewijziging: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + ", " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Locatiewijziging: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.BCC = mailto;
                                oMailItem.HTMLBody = mailbodyNl;
                                oMailItem.Importance = OlImportance.olImportanceHigh;
                                oMailItem.Display(true);
                                //oMailItem.Send();
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                }
                if (mailsfr.Count > 0)
                {
                    foreach (string mailto in mailsfr)
                    {
                        if (!String.IsNullOrEmpty(mailto))
                        {
                            try
                            {
                                Outlook.Application oApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                if (sessie != null)
                                    oMailItem.Subject = "modifictaion de l’endroit de la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + ", " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "modifictaion de l’endroit de la formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.BCC = mailto;
                                oMailItem.HTMLBody = mailbodyFr;
                                oMailItem.Importance = OlImportance.olImportanceHigh;
                                oMailItem.Display(true);
                                //oMailItem.Send();
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                }


            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Mail de afwezigen
        /// </summary>
        private void MailAfwezig()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                bool rechtsdag = false;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtsdag = true;
                }

                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    if (inschrijving.Status != "Verplaatst" && inschrijving.Status != "Geannuleerd")
                    {
                        data.Inschrijvingen fullInschrijving = new ConfocusDBLibrary.InschrijvingenService().GetInschrijvingByID(inschrijving.ID);
                        if (fullInschrijving.Aanwezig == false)
                        {
                            //TaalServices tService = new TaalServices();
                            Taal taal = GetTaal(fullInschrijving);//tService.GetTaalByID((decimal)fullInschrijving.Functies.Contact.Taal_ID);
                            string mailbody = StelMailBodyOpPlanning(sessie, fullInschrijving, taal.Code, rechtsdag);
                            Functies functie = GetFunctieFromDeelnemer(fullInschrijving);
                            string Mailto = functie.Email;
                            if (!string.IsNullOrEmpty(Mailto))
                            {
                                try
                                {
                                    //ConfocusClassLibrary.Attachment attest = GetAanwezigheidsAttest(fullInschrijving);
                                    Outlook.Application oApp = new Outlook.Application();
                                    Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                    DateTime mydate = new DateTime(2012, 01, 01);
                                    //oMailItem.Attachments.Add(attest.Link, Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                                    if (taal.Code == "FR")
                                        if (sessie != null)
                                            oMailItem.Subject = "Votre absence á notre formation: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                        else
                                            oMailItem.Subject = "Votre absence á notre formation: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                    else
                                        if (sessie != null)
                                        oMailItem.Subject = "Uw afwezigheid op onze opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                    else
                                        oMailItem.Subject = "Uw afwezigheid op onze opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                    oMailItem.To = Mailto;
                                    if (!string.IsNullOrEmpty(fullInschrijving.CC_Email))
                                        oMailItem.CC = fullInschrijving.CC_Email;
                                    oMailItem.HTMLBody = mailbody;
                                    oMailItem.Importance = Outlook.OlImportance.olImportanceHigh;
                                    oMailItem.Display(false);
                                    //oMailItem.Save();
                                    //oMailItem.Close( OlInspectorClose.olSave);
                                    //oMailItem.Send();

                                    List<string> mailoptions = new List<string>();
                                    mailoptions.Add("Mail_ontvangen");
                                    if (sessieComboBox.SelectedIndex < 1)
                                        SetInschrijvingMailOptions(fullInschrijving, mailoptions, true);
                                    else
                                        SetInschrijvingMailOptions(fullInschrijving, mailoptions, false);

                                }
                                catch (System.Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show(functie.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SetInschrijvingMailOptions(data.Inschrijvingen inschrijving, List<string> mailoptions, bool rechtsdag)
        {
            if (!rechtsdag)
            {
                foreach (string option in mailoptions)
                {
                    switch (option)
                    {
                        case "Mail_ontvangen":
                            inschrijving.Mail_ontvangen = true;
                            break;
                        case "Verplaatsing_Verstuurd":
                            inschrijving.Verplaatsing_Verstuurd = true;
                            break;
                        case "Reminder_Verstuurd":
                            inschrijving.Reminder_Verstuurd = true;
                            break;
                        case "Annulatie_Verstuurd":
                            inschrijving.Annulatie_Verstuurd = true;
                            break;
                        case "Evaluatie_ontvangen":
                            inschrijving.Evaluatie_ontvangen = true;
                            break;
                        case "Attest_Verstuurd":
                            inschrijving.Attest_Verstuurd = true;
                            break;
                        case "Locatiewijziging_verstuurd":
                            inschrijving.Locatiewijziging_Verstuurd = true;
                            break;
                        case "Inhouse_Verstuurd":
                            inschrijving.Inhouse_Verstuurd = true;
                            break;

                    }
                }
                SaveDeelnemer(inschrijving);
            }
            else
            {
                List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = new InschrijvingenService().GetInschrijvingBySeminarieID((decimal)inschrijving.Sessie.Seminarie_ID);
                List<data.Inschrijvingen> contactinschrijvingen = (from i in inschrijvingen where i.Functies.Contact_ID == inschrijving.Functies.Contact_ID select i).ToList();
                foreach (ConfocusDBLibrary.Inschrijvingen inschr in contactinschrijvingen)
                {
                    if (inschr.Status == "Inschrijving" || inschr.Status == "Gast")
                    {

                        foreach (string option in mailoptions)
                        {
                            switch (option)
                            {
                                case "Mail_ontvangen":
                                    inschr.Mail_ontvangen = true;
                                    break;
                                case "Verplaatsing_Verstuurd":
                                    inschr.Verplaatsing_Verstuurd = true;
                                    break;
                                case "Reminder_Verstuurd":
                                    inschr.Reminder_Verstuurd = true;
                                    break;
                                case "Annulatie_Verstuurd":
                                    if (inschr.Aanwezig == true)
                                    {
                                        inschr.Annulatie_Verstuurd = true;
                                    }
                                    break;
                                case "Evaluatie_ontvangen":
                                    if (inschr.Aanwezig == true)
                                    {
                                        inschr.Evaluatie_ontvangen = true;
                                    }
                                    break;
                                case "Attest_Verstuurd":
                                    if (inschr.Aanwezig == true)
                                    {
                                        inschr.Attest_Verstuurd = true;
                                    }
                                    break;
                                case "Locatiewijziging_verstuurd":
                                    inschr.Locatiewijziging_Verstuurd = true;
                                    break;
                                case "Inhouse_Verstuurd":
                                    if (inschr.Aanwezig == true)
                                    {
                                        inschr.Inhouse_Verstuurd = true;
                                    }
                                    break;

                            }
                        }
                        SaveDeelnemer(inschr);
                    }
                }
            }
        }

        /// <summary>
        /// Blanco mailbody voor het mailen van alle deelnemers
        /// </summary>
        private void MailBlanco()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                    sessie = null;
                string MailtoNl = "";
                string MailtoFr = "";
                List<string> mailsnl = new List<string>();
                List<string> mailsfr = new List<string>();
                int aantalMailadressenNl = 0;
                int aantalMailadressenFr = 0;
                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    Functies functie = GetFunctieFromDeelnemer(inschrijving);
                    if (functie.Email != "")
                    {
                        if (functie.Contact.Taal.Code == "FR")
                        {
                            if (aantalMailadressenFr == 10)
                            {
                                aantalMailadressenFr = 0;
                                mailsfr.Add(MailtoFr);
                                MailtoFr = functie.Email + "; ";
                                aantalMailadressenFr++;
                            }
                            else
                            {
                                MailtoFr += functie.Email + "; ";
                                aantalMailadressenFr++;
                            }
                        }
                        else
                        {
                            if (aantalMailadressenNl == 10)
                            {
                                aantalMailadressenNl = 0;
                                mailsnl.Add(MailtoNl);
                                MailtoNl = functie.Email + "; ";
                                aantalMailadressenNl++;
                            }
                            else
                            {
                                MailtoNl += functie.Email + "; ";
                                aantalMailadressenNl++;
                            }
                        }
                    }
                }
                if (!mailsfr.Contains(MailtoFr))
                    mailsfr.Add(MailtoFr);
                if (!mailsnl.Contains(MailtoNl))
                    mailsnl.Add(MailtoNl);
                foreach (string mailtonl in mailsnl)
                {
                    if (!String.IsNullOrEmpty(mailtonl))
                    {
                        try
                        {
                            Outlook.Application oApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            if (sessie != null)
                                oMailItem.Subject = sessie.SeminarieTitel + ": " + sessie.Naam + " " + ((DateTime)sessie.Datum).ToShortDateString() + " " + sessie.LocatieNaam;
                            else
                                oMailItem.Subject = ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;

                            oMailItem.BCC = mailtonl;
                            oMailItem.HTMLBody = "";
                            oMailItem.Display(true);
                            //oMailItem.Send();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                    }

                }
                foreach (string mailtofr in mailsfr)
                {
                    if (!string.IsNullOrEmpty(mailtofr))
                    {
                        try
                        {
                            Outlook.Application oApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            if (sessie != null)
                                oMailItem.Subject = sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + ")" + sessie.LocatieNaam;
                            else
                                oMailItem.Subject = ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;

                            oMailItem.BCC = mailtofr;
                            oMailItem.HTMLBody = "";
                            oMailItem.Display(true);
                            //oMailItem.Send();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        /// <summary>
        /// Mail verplaatsing sessie
        /// </summary>
        private void MailVerplaatsing()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                bool rechtsdag = false;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtsdag = true;
                }

                string mailbody;
                string MailtoNl = "";
                string MailtoFr = "";
                List<string> mailsnl = new List<string>();
                List<string> mailsfr = new List<string>();
                int aantalMailadressenNl = 0;
                int aantalMailadressenFr = 0;
                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    if (inschrijving.Status == "Gast" || inschrijving.Status == "Inschrijving")
                    {
                        Functies functie = GetFunctieFromDeelnemer(inschrijving);
                        if (functie.Email != "")
                        {
                            if (functie.Contact.Taal.Code == "FR")
                            {
                                if (aantalMailadressenFr == 10)
                                {
                                    aantalMailadressenFr = 0;
                                    mailsfr.Add(MailtoFr);
                                    MailtoFr = functie.Email + "; ";
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                        MailtoFr += inschrijving.CC_Email + ";";

                                    aantalMailadressenFr++;
                                }
                                else
                                {
                                    MailtoFr += functie.Email + "; ";
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                        MailtoFr += inschrijving.CC_Email + ";";

                                    aantalMailadressenFr++;
                                }
                            }
                            else
                            {
                                if (aantalMailadressenNl == 10)
                                {
                                    aantalMailadressenNl = 0;
                                    mailsnl.Add(MailtoNl);
                                    MailtoNl = functie.Email + "; ";
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                        MailtoNl += inschrijving.CC_Email + ";";
                                    aantalMailadressenNl++;
                                }
                                else
                                {
                                    MailtoNl += functie.Email + "; ";
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                        MailtoNl += inschrijving.CC_Email + ";";
                                    aantalMailadressenNl++;
                                }
                            }

                            List<string> mailoptions = new List<string>();
                            mailoptions.Add("Verplaatsing_Verstuurd");
                            if (sessieComboBox.SelectedIndex < 1)
                                SetInschrijvingMailOptions(inschrijving, mailoptions, true);
                            else
                                SetInschrijvingMailOptions(inschrijving, mailoptions, false);

                        }
                    }
                }
                if (!mailsfr.Contains(MailtoFr))
                    mailsfr.Add(MailtoFr);
                if (!mailsnl.Contains(MailtoNl))
                    mailsnl.Add(MailtoNl);
                foreach (string mailtonl in mailsnl)
                {
                    if (!String.IsNullOrEmpty(mailtonl))
                    {
                        try
                        {
                            mailbody = StelMailBodyOpPlanning(sessie, inschrijvingen[0], "NL", rechtsdag);
                            Outlook.Application oApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            if (sessie != null)
                                oMailItem.Subject = "Verplaatsing: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + sessie.Locatie.Naam + ")";
                            else
                                oMailItem.Subject = "Verplaatsing: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                            oMailItem.BCC = mailtonl;
                            oMailItem.HTMLBody = mailbody;
                            oMailItem.Display(true);
                            //oMailItem.Send();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }

                }
                foreach (string mailstoFR in mailsfr)
                {
                    if (!String.IsNullOrEmpty(mailstoFR))
                    {
                        try
                        {
                            mailbody = StelMailBodyOpPlanning(sessie, inschrijvingen[0], "FR", rechtsdag);
                            Outlook.Application oApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            if (sessie != null)
                                oMailItem.Subject = "Report de la formation: Confocus " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " " + sessie.Locatie.Naam + ")";
                            else
                                oMailItem.Subject = "Report de la formation: Confocus " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum; oMailItem.BCC = mailstoFR;
                            oMailItem.HTMLBody = mailbody;
                            oMailItem.Display(true);
                            oMailItem.Importance = OlImportance.olImportanceHigh;
                            //oMailItem.Send();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        /// <summary>
        /// Mail reminder sessie
        /// </summary>
        private void MailReminder()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();

                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                bool rechtsdag = false;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtsdag = true;
                }

                if (!rechtsdag)
                {

                    string mailbodyNl = "";
                    string mailbodyFr = "";
                    string MailtoNl = "";
                    string MailtoFr = "";
                    List<string> mailsnl = new List<string>();
                    List<string> mailsfr = new List<string>();
                    int aantalMailadressenNl = 0;
                    int aantalMailadressenFr = 0;
                    List<string> mails = new List<string>();
                    int i = 0;
                    foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                    {
                        if (inschrijving.Status == "Gast" || inschrijving.Status == "Inschrijving")
                        {
                            Functies functie = GetFunctieFromDeelnemer(inschrijving);
                            if (functie.Email != "")
                            {
                                if (functie.Contact.Taal.Code == "FR")
                                {
                                    if (mailbodyFr == "")
                                        mailbodyFr = StelMailBodyOpPlanning(sessie, inschrijvingen[i], "FR", rechtsdag);
                                    if (aantalMailadressenFr == 10)
                                    {
                                        aantalMailadressenFr = 0;
                                        mailsfr.Add(MailtoFr);
                                        MailtoFr = functie.Email + "; ";
                                        if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                            MailtoFr += inschrijving.CC_Email + ";";
                                        aantalMailadressenFr++;

                                    }
                                    else
                                    {
                                        MailtoFr += functie.Email + "; ";
                                        if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                            MailtoFr += inschrijving.CC_Email + ";";
                                        aantalMailadressenFr++;
                                        //inschrijving.Reminder_Verstuurd = true;
                                        //SaveDeelnemer(inschrijving);
                                    }
                                }
                                else
                                {
                                    if (mailbodyNl == "")
                                        mailbodyNl = StelMailBodyOpPlanning(sessie, inschrijvingen[i], "NL", rechtsdag);
                                    if (aantalMailadressenNl == 10)
                                    {
                                        aantalMailadressenNl = 0;
                                        mailsnl.Add(MailtoNl);
                                        MailtoNl = functie.Email + "; ";
                                        if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                            MailtoNl += inschrijving.CC_Email + ";";
                                        aantalMailadressenNl++;
                                        //inschrijving.Reminder_Verstuurd = true;
                                        //SaveDeelnemer(inschrijving);
                                    }
                                    else
                                    {
                                        MailtoNl += functie.Email + "; ";
                                        if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                            MailtoNl += inschrijving.CC_Email + ";";
                                        aantalMailadressenNl++;
                                        //inschrijving.Reminder_Verstuurd = true;
                                        //SaveDeelnemer(inschrijving);
                                    }
                                }
                            }
                            i++;
                            List<string> mailoptions = new List<string>();
                            mailoptions.Add("Reminder_Verstuurd");
                            if (sessieComboBox.SelectedIndex < 1)
                                SetInschrijvingMailOptions(inschrijving, mailoptions, true);
                            else
                                SetInschrijvingMailOptions(inschrijving, mailoptions, false);
                        }
                    }
                    if (!mailsfr.Contains(MailtoFr))
                        mailsfr.Add(MailtoFr);
                    if (!mailsnl.Contains(MailtoNl))
                        mailsnl.Add(MailtoNl);
                    foreach (string mailto in mailsnl)
                    {
                        if (!String.IsNullOrEmpty(mailto))
                        {
                            try
                            {
                                Outlook.Application oApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                if (sessie != null)
                                    oMailItem.Subject = "Reminder: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " " + ((DateTime)sessie.Datum).ToShortDateString() + " " + sessie.Locatie.Naam + ".";
                                else
                                    oMailItem.Subject = "Reminder: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.BCC = mailto;
                                oMailItem.HTMLBody = mailbodyNl;
                                oMailItem.Display(true);
                                //oMailItem.Send();
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }

                    }
                    foreach (string mailto in mailsfr)
                    {
                        if (!String.IsNullOrEmpty(mailto))
                        {
                            try
                            {
                                Outlook.Application oApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                if (sessie != null)
                                    oMailItem.Subject = "Rappel: Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " " + ((DateTime)sessie.Datum).ToShortDateString() + " " + sessie.Locatie.Naam + ".";
                                else
                                    oMailItem.Subject = "Rappel: Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.BCC = mailto;
                                oMailItem.HTMLBody = mailbodyFr;
                                oMailItem.Display(true);
                                //oMailItem.Send();
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }

                    }
                }
                else
                {
                    foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                    {
                        if (inschrijving.Status == "Gast" || inschrijving.Status == "Inschrijving")
                        {
                            string sessiedatum = ((DateTime)inschrijving.Sessie.Datum).ToShortDateString();
                            Functies functie = GetFunctieFromDeelnemer(inschrijving);
                            if (functie.Email != "")
                            {
                                if (functie.Contact.Taal.Code == "FR")
                                {

                                    string mailbody = StelMailBodyOpPlanning(sessie, inschrijving, "FR", rechtsdag);

                                    try
                                    {
                                        Outlook.Application oApp = new Outlook.Application();
                                        Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                        DateTime mydate = new DateTime(2012, 01, 01);
                                        if (sessie != null)
                                            oMailItem.Subject = "Rappel: Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " " + ((DateTime)sessie.Datum).ToShortDateString() + " " + sessie.Locatie.Naam + ".";
                                        else
                                            oMailItem.Subject = "Rappel: Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).Titel + " - " + sessiedatum;
                                        oMailItem.To = functie.Email;
                                        oMailItem.HTMLBody = mailbody;
                                        oMailItem.Display(true);
                                        //oMailItem.Send();
                                    }
                                    catch (System.Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                                    }
                                }
                                else
                                {
                                    string mailbody = StelMailBodyOpPlanning(sessie, inschrijving, "NL", rechtsdag);
                                    try
                                    {
                                        Outlook.Application oApp = new Outlook.Application();
                                        Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                        DateTime mydate = new DateTime(2012, 01, 01);
                                        if (sessie != null)
                                            oMailItem.Subject = "Reminder: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " " + ((DateTime)sessie.Datum).ToShortDateString() + " " + sessie.Locatie.Naam + ".";
                                        else
                                            oMailItem.Subject = "Reminder: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).Titel + " - " + sessiedatum;
                                        oMailItem.To = functie.Email;
                                        oMailItem.HTMLBody = mailbody;
                                        oMailItem.Display(false);
                                        oMailItem.Save();
                                        //oMailItem.Send();
                                    }
                                    catch (System.Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Mail annulatie sessie/seminarie
        /// </summary>
        private void MailAnnulatie()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                bool rechtsdag = false;
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtsdag = true;
                }

                //string mailbody = StelMailBodyOpPlanning(sessie, inschrijvingen[0], "NL");
                string mailbodyNl = "";
                string mailbodyFr = "";
                string MailtoNl = "";
                string MailtoFr = "";
                List<string> mailsnl = new List<string>();
                List<string> mailsfr = new List<string>();
                int aantalMailadressenNl = 0;
                int aantalMailadressenFr = 0;
                int i = 0;
                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    if (inschrijving.Status == "Gast" || inschrijving.Status == "Inschrijving")
                    {

                        Functies functie = GetFunctieFromDeelnemer(inschrijving);
                        if (!string.IsNullOrEmpty(functie.Email))
                        {
                            if (functie.Contact.Taal.Code == "FR")
                            {
                                if (mailbodyFr == "")
                                    mailbodyFr = StelMailBodyOpPlanning(sessie, inschrijvingen[i], "FR", rechtsdag);
                                if (aantalMailadressenFr > 9)
                                {
                                    aantalMailadressenFr = 0;
                                    mailsfr.Add(MailtoFr);
                                    MailtoFr = functie.Email + "; ";
                                    aantalMailadressenFr++;
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoFr += inschrijving.CC_Email + ";";
                                        aantalMailadressenFr++;
                                    }
                                }
                                else
                                {
                                    MailtoFr += functie.Email + "; ";
                                    aantalMailadressenFr++;

                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoFr += inschrijving.CC_Email + ";";
                                        aantalMailadressenFr++;
                                    }
                                }
                            }
                            else
                            {
                                if (mailbodyNl == "")
                                    mailbodyNl = StelMailBodyOpPlanning(sessie, inschrijvingen[i], "NL", rechtsdag);
                                if (aantalMailadressenNl > 9)
                                {
                                    aantalMailadressenNl = 0;
                                    mailsnl.Add(MailtoNl);
                                    MailtoNl = functie.Email + "; ";
                                    aantalMailadressenNl++;
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoNl += inschrijving.CC_Email + ";";
                                        aantalMailadressenNl++;
                                    }
                                }
                                else
                                {
                                    MailtoNl += functie.Email + "; ";
                                    aantalMailadressenNl++;
                                    if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    {
                                        MailtoNl += inschrijving.CC_Email + ";";
                                        aantalMailadressenNl++;
                                    }
                                }
                            }
                            List<string> mailoptions = new List<string>();
                            mailoptions.Add("Annulatie_Verstuurd");
                            if (sessieComboBox.SelectedIndex < 1)
                                SetInschrijvingMailOptions(inschrijving, mailoptions, true);
                            else
                                SetInschrijvingMailOptions(inschrijving, mailoptions, false);
                        }
                    }
                }
                if (!mailsfr.Contains(MailtoFr))
                    mailsfr.Add(MailtoFr);
                if (!mailsnl.Contains(MailtoNl))
                    mailsnl.Add(MailtoNl);
                if (mailsnl.Count > 0)
                {
                    foreach (string mailto in mailsnl)
                    {
                        if (!String.IsNullOrEmpty(mailto))
                        {
                            try
                            {
                                Outlook.Application oApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                if (sessie != null)
                                    oMailItem.Subject = "Annulatie: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + ", " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Annulatie: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.BCC = mailto;
                                oMailItem.HTMLBody = mailbodyNl;
                                oMailItem.Importance = OlImportance.olImportanceHigh;
                                oMailItem.Display(true);
                                //oMailItem.Send();
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                }
                if (mailsfr.Count > 0)
                {
                    foreach (string mailto in mailsfr)
                    {
                        if (!String.IsNullOrEmpty(mailto))
                        {
                            try
                            {
                                Outlook.Application oApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                if (sessie != null)
                                    oMailItem.Subject = "Annulation: formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + ", " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Annulation: formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).TitelEnDatum;
                                oMailItem.BCC = mailto;
                                oMailItem.HTMLBody = mailbodyFr;
                                oMailItem.Importance = OlImportance.olImportanceHigh;
                                oMailItem.Display(true);
                                //oMailItem.Send();
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                }


            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /// <summary>
        /// Aanwezigheids attesten mailen
        /// </summary>
        private void MailAttesten()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                bool rechtstdag = false;
                List<data.Inschrijvingen> inschrijvingen = GetAlleSessieDeelnemers();
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                if (sessieComboBox.SelectedIndex < 1)
                {
                    sessie = null;
                    rechtstdag = true;
                }

                foreach (data.Inschrijvingen inschrijving in inschrijvingen)
                {
                    if (inschrijving.Aanwezig == true && inschrijving.Attest_Verstuurd != true)
                    {
                        data.Inschrijvingen fullInschrijving = new ConfocusDBLibrary.InschrijvingenService().GetInschrijvingByID(inschrijving.ID);

                        //TaalServices tService = new TaalServices();
                        Taal taal = GetTaal(fullInschrijving); //tService.GetTaalByID((decimal)fullInschrijving.Functies.Contact.Taal_ID);
                        string mailbody = StelMailBodyOpPlanning(sessie, fullInschrijving, taal.Code, rechtstdag);
                        Functies functie = GetFunctieFromDeelnemer(fullInschrijving);
                        string Mailto = functie.Email;
                        if (!string.IsNullOrEmpty(Mailto))
                        {
                            try
                            {
                                ConfocusClassLibrary.Attachment attest = GetAanwezigheidsAttest(fullInschrijving, taal);
                                if (attest != null)
                                {
                                    Outlook.Application oApp = new Outlook.Application();
                                    Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                                    DateTime mydate = new DateTime(2012, 01, 01);
                                    oMailItem.Attachments.Add(attest.Link, Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                                    if (taal.Code == "FR")
                                        if (sessie != null)
                                            oMailItem.Subject = "Votre présence à la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                        else
                                            oMailItem.Subject = "Votre présence à la formation Confocus: " + ((Seminarie)seminarieComboBox.SelectedItem).Titel;
                                    else
                                        if (sessie != null)
                                        oMailItem.Subject = "Aanwezigheidsattest: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                    else
                                        oMailItem.Subject = "Aanwezigheidsattest: Confocus opleiding: " + ((Seminarie)seminarieComboBox.SelectedItem).Titel;
                                    oMailItem.To = Mailto;
                                    if (!string.IsNullOrEmpty(fullInschrijving.CC_Email))
                                        oMailItem.CC = fullInschrijving.CC_Email;
                                    oMailItem.HTMLBody = mailbody;
                                    oMailItem.Importance = Outlook.OlImportance.olImportanceHigh;
                                    oMailItem.Display(false);
                                    //oMailItem.Save();
                                    //oMailItem.Close( OlInspectorClose.olSave);
                                    //oMailItem.Send();

                                    List<string> mailoptions = new List<string>();
                                    mailoptions.Add("Evaluatie_ontvangen");
                                    mailoptions.Add("Attest_Verstuurd");
                                    if (sessieComboBox.SelectedIndex < 1)
                                        SetInschrijvingMailOptions(inschrijving, mailoptions, true);
                                    else
                                        SetInschrijvingMailOptions(inschrijving, mailoptions, false);

                                }
                            }
                            catch (System.Runtime.InteropServices.COMException cex)
                            {
                                MessageBox.Show(cex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(functie.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }

                    }
                }

            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private Taal GetTaal(data.Inschrijvingen inschrijving)
        {
            TaalServices tService = new TaalServices();
            Taal seminarietaal;
            Taal contactPrimaireTaal;
            Taal contactSecundaireTaal;

            SeminarieServices sService = new SeminarieServices();
            Seminarie seminarie = sService.GetSeminarieByID((decimal)inschrijving.Sessie.Seminarie_ID);

            seminarietaal = tService.GetTaalByID((decimal)seminarie.Taal_ID);
            contactPrimaireTaal = tService.GetTaalByID((decimal)inschrijving.Functies.Contact.Taal_ID);
            if (inschrijving.Functies.Contact.Secundaire_Taal_ID != null)
                contactSecundaireTaal = tService.GetTaalByID((decimal)inschrijving.Functies.Contact.Secundaire_Taal_ID);
            else
                contactSecundaireTaal = null;

            if (seminarietaal.Equals(contactPrimaireTaal))
            {
                return contactPrimaireTaal;
            }
            else if (seminarietaal.Equals(contactSecundaireTaal))
            {
                return contactSecundaireTaal;
            }
            else
                return contactPrimaireTaal;

        }

        private void SaveDeelnemer(data.Inschrijvingen deelnemer)
        {
            try
            {
                InschrijvingenService iService = new InschrijvingenService();
                deelnemer.Gewijzigd = DateTime.Now;
                deelnemer.Gewijzigd_door = currentUser.Naam;
                iService.SaveInschrijvingCHanges(deelnemer);
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show(ErrorMessages.DeelnemerNietOpgeslagen, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private ConfocusClassLibrary.Attachment GetAanwezigheidsAttest(data.Inschrijvingen deelnemer, Taal taal)
        {
            if (deelnemer != null)
            {
                try
                {
                    string dagtype = GetDagTypeString(deelnemer);
                    object oMissing = System.Reflection.Missing.Value;
                    object template = GetTemplate(deelnemer, taal);
                    string filename = deelnemer.Functies.Contact.VolledigeNaam + GetCleanDate((DateTime)deelnemer.Sessie.Datum) + "_";
                    if (deelnemer.Functies.Attesttype_ID != null)
                    {
                        Attesttype_Services aService = new Attesttype_Services();
                        AttestType type = aService.GetByID((decimal)deelnemer.Functies.Attesttype_ID);
                        filename += type.Naam + ".pdf";
                    }
                    //else if (!string.IsNullOrEmpty(deelnemer.Attesttype))
                    //{
                    //    filename += deelnemer.Attesttype + ".pdf";
                    //}
                    else
                    {
                        filename += "administratieattest.pdf";
                    }

                    string path = _dataFolder + @"Aanwezigheids Attesten\" + filename;
                    Sessie sessie = new SessieServices().GetSessieByID((decimal)deelnemer.Sessie_ID);
                    Seminarie_ErkenningenService sService = new Seminarie_ErkenningenService();
                    List<Seminarie_Erkenningen> semErkenningen = sService.GetBySeminarieID((decimal)sessie.Seminarie_ID);

                    WordDocument doc;
                    try
                    {
                        doc = new WordDocument(template, "Staand");
                    }
                    catch (System.Exception ex)
                    {
                        Taal primair = new TaalServices().GetTaalByID((decimal)deelnemer.Functies.Contact.Taal_ID);
                        Taal secundair = new TaalServices().GetTaalByID((decimal)deelnemer.Functies.Contact.Secundaire_Taal_ID);
                        try
                        {
                            if (template.ToString().Contains("_FR") && (primair.Code == "NL" || secundair.Code == "NL"))
                            {
                                string templ = template.ToString().Replace("_FR", "_NL");
                                template = templ;
                                doc = new WordDocument(template, "Staand");
                            }
                            else if (template.ToString().Contains("_NL") && (primair.Code == "FR" || secundair.Code == "FR"))
                            {
                                string templ = template.ToString().Replace("_NL", "_FR");
                                template = templ;
                                doc = new WordDocument(template, "Staand");
                            }
                            else
                                throw ex;
                        }
                        catch (System.Exception)
                        {
                            throw new System.Exception("Er is geen template beschikbaar voor deze erkenning!");
                        }

                    }
                    //doc.save(path);
                    List<String> fields = new List<string>();
                    //1
                    fields.Add(deelnemer.Functies.Contact.Voornaam);
                    //2
                    fields.Add(deelnemer.Functies.Contact.Achternaam);
                    //3
                    fields.Add(deelnemer.Functies.Oorspronkelijke_Titel);
                    //4
                    fields.Add(deelnemer.Functies.Bedrijf.Naam);
                    //5
                    fields.Add(((Seminarie)seminarieComboBox.SelectedItem).Titel);
                    //6
                    if (sessie != null)
                        fields.Add(sessie.Naam);
                    else
                        fields.Add("");
                    //7
                    if (sessie.Locatie != null)
                        fields.Add(sessie.Locatie.Naam);
                    else
                        fields.Add("");
                    //8
                    fields.Add(((DateTime)sessie.Datum).ToShortDateString());
                    //9
                    fields.Add(sessie.Beginuur);
                    //10
                    fields.Add(sessie.Einduur);
                    //11
                    fields.Add(dagtype);
                    //12
                    fields.Add(deelnemer.Functies.Erkenningsnummer);

                    string erknummersessie = "";
                    //string erknummercontact = "";
                    string erkAantal = "";
                    foreach (Seminarie_Erkenningen erk in semErkenningen)
                    {
                        if (erk.Sessie_ID == sessie.ID && deelnemer.Functies.Attesttype == erk.Erkenning.Attest)
                        {
                            erknummersessie = erk.Nummer;
                            if (erk.TAantal == deelnemer.TAantal)
                                erkAantal = erk.TAantal;
                            else if (!string.IsNullOrEmpty(deelnemer.TAantal))
                                erkAantal = deelnemer.TAantal;
                            else
                                erkAantal = erk.TAantal;
                            break;
                        }
                        else
                        {
                            if (deelnemer.Functies.Attesttype == erk.Erkenning.Naam)
                            {
                                erknummersessie = erk.Nummer;
                                //erkAantal = deelnemer.DecimalAantal.ToString();
                            }
                        }

                    }
                    //13
                    fields.Add(erknummersessie);
                    //14
                    if (erkAantal != "")
                        fields.Add(erkAantal);
                    else if (deelnemer.Aantal != null)
                        fields.Add(deelnemer.TAantal);
                    else
                        fields.Add("");
                    //15
                    fields.Add(DateTime.Now.ToShortDateString());
                    //16
                    if (sessieComboBox.SelectedIndex < 1)
                    {
                        List<string> sessiefields = GetSessieInfo(deelnemer);
                        foreach (string field in sessiefields)
                        {
                            fields.Add(field);
                        }
                    }
                    else
                    {
                        //16
                        fields.Add("");
                        //17
                        fields.Add("");
                        //18
                        fields.Add("");
                        //19
                        fields.Add("");
                    }
                    //20
                    fields.Add(deelnemer.Functies.Type);
                    doc.Activate();
                    doc.ChangeFields(fields);
                    doc.SaveAsPDF(path);
                    //doc.save(path);
                    doc.quit();

                    ConfocusClassLibrary.Attachment attachment = new ConfocusClassLibrary.Attachment();
                    attachment.Naam = "Aanwezigheids attest";
                    attachment.Link = path;

                    return attachment;

                }
                catch (System.Exception ex)
                {
                    throw new System.Exception(ex.Message);
                }
            }
            else
                return null;
        }

        private List<string> GetSessieInfo(data.Inschrijvingen deelnemer)
        {
            List<string> results = new List<string>();
            SessieServices sService = new SessieServices();
            string sessie1 = "";
            string sessie2 = "";
            string sessie3 = "";
            string sessie4 = "";
            //Seminarie seminarie = new SeminarieServices().GetSeminarieByID((decimal)deelnemer.Sessie.Seminarie_ID);
            List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = new InschrijvingenService().GetInschrijvingBySeminarieIDAndFunctieID((decimal)deelnemer.Sessie.Seminarie_ID, deelnemer.Functies.Contact_ID);
            int i = 1;
            foreach (ConfocusDBLibrary.Inschrijvingen reg in inschrijvingen)
            {
                Sessie sess = sService.GetSessieByID((decimal)reg.Sessie_ID);
                if (!sess.Naam.ToLower().Contains("lunch"))
                {
                    switch (i)
                    {
                        case 1:
                            sessie1 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                        case 2:
                            sessie2 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                        case 3:
                            sessie3 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                        case 4:
                            sessie4 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                    }
                    i++;
                }
            }
            if (i == 3)
            {
                sessie3 = "";
                sessie4 = "";
            }
            else if (i == 4)
                sessie4 = "";

            results.Add(sessie1);
            results.Add(sessie2);
            results.Add(sessie3);
            results.Add(sessie4);
            return results;
        }

        private string GetDagTypeString(data.Inschrijvingen deelnemer)
        {
            try
            {
                DagType_Services dService = new DagType_Services();
                DagType dagtype = dService.GetByName(deelnemer.Dagtype);
                if (deelnemer.Functies.Contact.Taal_ID != null)
                {
                    Taal taal = new TaalServices().GetTaalByID((decimal)deelnemer.Functies.Contact.Taal_ID);
                    if (taal.Code == "NL")
                        return dagtype.Nederlands;
                    else
                        return dagtype.Frans;
                }
                else
                    return "volledige sessie";
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                return "volledige sessie";
            }
        }

        private object GetTemplate(data.Inschrijvingen deelnemer, Taal taal)
        {
            string template = _dataFolder + "Templates\\";
            string filename = "";
            string taalcode = taal.Code;//new TaalServices().GetTaalByID((decimal)deelnemer.Functies.Contact.Taal_ID).Code;
            AttestType attest = null;
            if (deelnemer.Functies.Attesttype_ID != null)
            {
                Attesttype_Services aService = new Attesttype_Services();
                attest = aService.GetByID((decimal)deelnemer.Functies.Attesttype_ID);
            }
            //string contactAttest = deelnemer.Functies.Contact.Attesttype;
            if (attest != null)
            {
                if (attest.Naam != "Geen")
                {
                    List<AttestType> attesten = new Attesttype_Services().FindActive();
                    foreach (var myattest in attesten)
                    {
                        if (myattest.Naam == attest.Naam)
                        {
                            if (sessieComboBox.SelectedIndex < 1)
                                filename = myattest.Naam + "_RECHTSDAG_" + taalcode + ".docx";
                            else
                                filename = myattest.Naam + "_" + taalcode + ".docx";
                            break;
                        }
                    }
                    if (filename == "")
                        filename = "Administratief_" + taalcode + ".docx";
                }
                else
                    filename = "Administratief_" + taalcode + ".docx";
            }
            else
            {
                filename = "Administratief_" + taalcode + ".docx";
            }

            return template + filename;
        }

        private string GetCleanDate(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }

        private Functies GetFunctieFromDeelnemer(data.Inschrijvingen deelnemer)
        {
            try
            {
                FunctieServices fService = new FunctieServices();
                Functies theFunctie = new Functies();
                Functies functie = fService.GetFunctieByID((decimal)deelnemer.Functie_ID);

                return functie;
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }
        }

        private string StelMailBodyOpPlanning(Sessie sessie, data.Inschrijvingen inschrijving, string taal, bool rechtsdag)
        {
            Sessie fullSessie = new Sessie();
            if (sessie != null)
                fullSessie = new SessieServices().GetSessieByID((decimal)inschrijving.Sessie_ID);
            string paswoord = paswoordTextBox.Text;

            string nieuweDatum = "";
            if (sessie != null)
                nieuweDatum = nieuweDatumDatePicker.SelectedDate.HasValue == false ? ((DateTime)fullSessie.Datum).AddDays(21).ToShortDateString() : ((DateTime)nieuweDatumDatePicker.SelectedDate).ToShortDateString();
            string body = "";
            string nieuweLocatie = "";
            string rechtsdagplanning = "";
            if (nieuweLocatieComboBox.SelectedIndex > -1)
                nieuweLocatie = ((Locatie)nieuweLocatieComboBox.SelectedItem).Naam;
            string filename = GetCorrectFilename(inschrijving, taal, rechtsdag);
            //todo Get correct file name.
            string lunchtext = "";
            if (rechtsdag)
            {
                Functies functie = new FunctieServices().GetFunctieByID((decimal)inschrijving.Functie_ID);
                List<data.Inschrijvingen> inschrijvingen = new InschrijvingenService().GetSessiesByContactIDAndSeminarieID(functie.Contact_ID, (decimal)inschrijving.Sessie.Seminarie_ID, (DateTime)inschrijving.Sessie.Datum);//.GetSessiesByFunctieAndSemId(inschrijving.Functies.ID, (decimal)inschrijving.Sessie.Seminarie_ID, (DateTime)inschrijving.Sessie.Datum);

                List<Sessie> zalen = new List<Sessie>();
                foreach (var item in inschrijvingen)
                {
                    Sessie myses = item.Sessie;
                    if (!zalen.Contains(myses))
                        zalen.Add(myses);
                }
                bool lunch = false;
                foreach (Sessie zaal in zalen)
                {
                    if (zaal.Naam.Contains("Lunch"))
                    {
                        lunch = true;
                        continue;
                    }
                    rechtsdagplanning += zaal.Beginuur + " tot " + zaal.Einduur + " - " + zaal.Naam +  "<br/>";
                }
                if (lunch)
                {
                    lunchtext = "U bent ook ingeschreven voor de lunch";
                }                    
                else
                {
                    lunchtext = "U bent niet ingeschreven voor de lunch";
                }
            }

            string naam = inschrijving.Functies.Contact.Achternaam;
            string aanspreking = GetAanspreking(inschrijving, taal);
            string SeminarieTitel = inschrijving.SeminarieTitel;
            string Locatie = inschrijving.Sessie.LocatieNaam;
            string Adres = inschrijving.Sessie.LocatieAdres;
            string Sessiedatum = ((DateTime)inschrijving.Sessie.Datum).ToShortDateString();
            string verantwoordelijke = "";
            if (sessie != null)
                verantwoordelijke = GetSessieVerantwoordelijke(fullSessie.Verantwoordelijke_Sessie_ID);
            string verantwoordelijkeEmail = "";
            if (sessie != null)
                verantwoordelijkeEmail = GetSessieVerantwoordelijkeEmail(fullSessie.Verantwoordelijke_Sessie_ID);

            string agenda = Editor.ContentHtml;
            body = System.IO.File.ReadAllText(filename);
            body = body.Replace("#aanspreking#", aanspreking);
            body = body.Replace("#naam#", naam);
            body = body.Replace("#beginuur_sessie#", inschrijving.Sessie.Beginuur);
            body = body.Replace("#einduur_sessie#", inschrijving.Sessie.Einduur);
            body = body.Replace("#url_sessie#", inschrijving.Sessie.URL);
            body = body.Replace("#naam_sessie#", inschrijving.Sessie.Naam);
            body = body.Replace("#datum_sessie#", Sessiedatum);
            body = body.Replace("#Paswoord#", paswoord);
            body = body.Replace("#nieuwe_datum#", nieuweDatum);
            body = body.Replace("#nieuwe_locatie#", nieuweLocatie);
            body = body.Replace("#planning#", rechtsdagplanning);
            //body = body.Replace("#Periode#", "Voorjaar 2016");
            body = body.Replace("#Lunch#", lunchtext);
            body = body.Replace("#Extra#", agenda);
            body = body.Replace("#locatie_sessie#", Locatie);
            body = body.Replace("#adres_locatie#", Adres);
            body = body.Replace("#naam_seminarie#", SeminarieTitel);
            body = body.Replace("#seminarie#", SeminarieTitel);
            body = body.Replace("#naam_sessieverantwoordelijke#", verantwoordelijke);
            body = body.Replace("#e-mailverantwoordelijkesessie#", verantwoordelijkeEmail);
            if (taal == "FR")
                body = ITCHelper.TranslateToHTML(body);

            return body;
        }

        private string GetSessieVerantwoordelijkeEmail(int? ID)
        {
            if (ID != null)
            {
                GebruikerService gService = new GebruikerService();
                Gebruiker geb = gService.GetUserByID((int)ID);
                return geb.Email;
            }
            else return "";
        }

        private string GetSessieVerantwoordelijke(int? ID)
        {
            if (ID != null)
            {
                GebruikerService gService = new GebruikerService();
                Gebruiker geb = gService.GetUserByID((int)ID);
                return geb.Naam;
            }
            else return "";

        }

        private string GetAanspreking(data.Inschrijvingen inschrijving, string taal)
        {
            var geslacht = inschrijving.Functies.Contact.Aanspreking;
            AansprekingServices aService = new AansprekingServices();
            Aanspreking aanspreking;
            Taal sessietaal = new TaalServices().GetTaalByNameOrCode(taal);
            if (sessietaal.ID == (decimal)inschrijving.Functies.Contact.Taal_ID)
                aanspreking = aService.GetByLanguage((decimal)inschrijving.Functies.Contact.Taal_ID);
            else if (inschrijving.Functies.Contact.Secundaire_Taal_ID != null)
            {
                if (sessietaal.ID == (decimal)inschrijving.Functies.Contact.Secundaire_Taal_ID)
                    aanspreking = aService.GetByLanguage((decimal)inschrijving.Functies.Contact.Secundaire_Taal_ID);
                else
                    aanspreking = aService.GetByLanguage(sessietaal.ID);
            }
            else
                aanspreking = aService.GetByLanguage(sessietaal.ID);

            if (geslacht.ToLower() == "v")
                return aanspreking.Aanspreking_vrouw;
            else
                return aanspreking.Aanspreking_man;
        }

        private string GetCorrectFilename(data.Inschrijvingen inschrijving, string taal, bool rechtsdag)
        {
            String name = AttestTemplateComboBox.Text;
            //String language = new TaalServices().GetTaalByID((decimal)inschrijving.Functies.Contact.Taal_ID).Code;
            if (!rechtsdag)
                name += " " + taal;
            else
                name += "Rechtsdag " + taal;
            AttestTemplate template = new AttestTemplate_Service().GetByName(name)[0];
            String filename = template.Uri;
            return filename;
        }

        private void bewerkMailTemplateButton_Click(object sender, RoutedEventArgs e)
        {
            if (AttestTemplateComboBox.SelectedIndex > -1)
            {
                String template = AttestTemplateComboBox.Text;
                TemplateKiezer TK = new TemplateKiezer();
                TK.ZoekTerm = template;
                TK.Owner = this;
                if (TK.ShowDialog() == true)
                {
                    AttestTemplate temp = TK.Template;
                    EmailEditor editor = new EmailEditor(temp.Uri);
                    editor.Show();
                }
            }
        }
        private void bewerkSprekerMailTemplateButton_Click(object sender, RoutedEventArgs e)
        {
            if (sprekerAttestTemplateComboBox.SelectedIndex > -1)
            {
                String template = sprekerAttestTemplateComboBox.Text;
                TemplateKiezer TK = new TemplateKiezer();
                TK.ZoekTerm = template;
                TK.Owner = this;
                if (TK.ShowDialog() == true)
                {
                    AttestTemplate temp = TK.Template;
                    EmailEditor editor = new EmailEditor(temp.Uri);
                    editor.Show();
                }
            }
        }


        private void OpenEditorButton_Click(object sender, RoutedEventArgs e)
        {
            EmailEditor editor = new EmailEditor("");
            editor.Show();
        }

        private void filterButton_Click(object sender, RoutedEventArgs e)
        {
            GetInschrijvingen();
        }

        private void GetInschrijvingen()
        {
            if (sessieComboBox.SelectedIndex > 0)
            {
                Sessie sessie = (Sessie)sessieComboBox.SelectedItem;
                InschrijvingenService iService = new InschrijvingenService();
                List<data.Inschrijvingen> inschrijvingen = new List<data.Inschrijvingen>();
                string template = AttestTemplateComboBox.Text;
                string status = statusComboBox.Text;
                switch (template)
                {
                    case "Blanco":
                        inschrijvingen = iService.GetBlancoInschrijvingenByIDAndStatus(sessie.ID, status);
                        break;
                    case "Aanwezig":
                        inschrijvingen = iService.GetAanwezigeInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    case "Afwezig":
                        inschrijvingen = iService.GetAfwezigeInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    case "Annulatie":
                        inschrijvingen = iService.GetReminderInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    case "Reminder":
                        inschrijvingen = iService.GetReminderInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    case "Verplaatsing":
                        inschrijvingen = iService.GetReminderInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    case "Locatiewijziging":
                        inschrijvingen = iService.GetReminderInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    case "Inhouse":
                        inschrijvingen = iService.GetAanwezigeInschrijvingenBySessieID(sessie.ID, status);
                        break;
                    default:
                        inschrijvingen = iService.GetBlancoInschrijvingenByIDAndStatus(sessie.ID, status);
                        break;
                }
                inschrijvingenDataGrid.ItemsSource = inschrijvingen;
            }
            else
            {
                Seminarie seminarie = (Seminarie)seminarieComboBox.SelectedItem;
                InschrijvingenService iService = new InschrijvingenService();
                List<data.Inschrijvingen> inschrijvingen = new List<data.Inschrijvingen>();
                string template = AttestTemplateComboBox.Text;
                string status = statusComboBox.Text;
                switch (template)
                {
                    case "Blanco":
                        inschrijvingen = iService.GetBlancoInschrijvingenBySeminarieIDAndStatus(seminarie.ID, status);
                        break;
                    case "Aanwezig":
                        inschrijvingen = iService.GetAanwezigeInschrijvingenBySeminarieID(seminarie.ID, status);
                        break;
                    case "Afwezig":
                        inschrijvingen = iService.GetAfwezigeInschrijvingenBySeminarieID(seminarie.ID, status);
                        break;
                    case "Annulatie":
                        inschrijvingen = iService.GetReminderInschrijvingenBySeminarieID(seminarie.ID, status);
                        break;
                    case "Reminder":
                        inschrijvingen = iService.GetReminderInschrijvingenBySeminarieID(seminarie.ID, status);
                        break;
                    case "Verplaatsing":
                        inschrijvingen = iService.GetReminderInschrijvingenBySeminarieID(seminarie.ID, status);
                        break;
                        //default:
                        //    inschrijvingen = iService.GetBlancoInschrijvingenBySeminarieIDAndStatus(seminarie.ID, status);
                        //    break;
                }
                inschrijvingenDataGrid.ItemsSource = inschrijvingen;
            }
        }

        private void AanwezigCheckBox_Click(object sender, RoutedEventArgs e)
        {
            //Todo save aanwezig changes
            System.Windows.Controls.CheckBox box = (System.Windows.Controls.CheckBox)sender;
            data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
            inschrijving.Aanwezig = box.IsChecked == true;
            try
            {
                inschrijving = new InschrijvingenService().SaveInschrijvingCHanges(inschrijving);
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void inschrijvingenDataGrid_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }

        private void ExtraAttestButton_Click(object sender, RoutedEventArgs e)
        {
            data.Inschrijvingen inschrijving = (data.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
            bool rechtsdag = false;
            if (sessieComboBox.SelectedIndex < 1)
                rechtsdag = true;
            ExtraAttestWindow EA = new ExtraAttestWindow(inschrijving.ID, "Inschrijving", rechtsdag, currentUser);
            EA.Show();
        }

        private void AanwezigButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (data.Inschrijvingen inschrijving in inschrijvingenDataGrid.Items)
            {
                if (inschrijving.Status == "Inschrijving" || inschrijving.Status == "Gast")
                {
                    inschrijving.Aanwezig = true;
                    try
                    {
                        new InschrijvingenService().SaveInschrijvingCHanges(inschrijving);
                    }
                    catch (System.Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
            GetInschrijvingen();
        }
    }
}

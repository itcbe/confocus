﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for BestaandBedrijfWindow.xaml
    /// </summary>
    public partial class BestaandBedrijfWindow : Window
    {
        private List<Bedrijf> bedrijven = new List<Bedrijf>();
        public Bedrijf myBedrijf;

        public BestaandBedrijfWindow(List<Bedrijf> bed)
        {
            InitializeComponent();
            this.bedrijven = bed;
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void gebruikbestaandeButton_Click(object sender, RoutedEventArgs e)
        {
            if (bedrijvenListBox.SelectedIndex != -1)
            {
                myBedrijf = (Bedrijf)bedrijvenListBox.SelectedItem;
                DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bedrijvenListBox.ItemsSource = bedrijven;
            bedrijvenListBox.DisplayMemberPath = "NaamEnBtwNummer";
        }
    }
}

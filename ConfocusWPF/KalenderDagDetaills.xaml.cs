﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for KalenderDagDetaills.xaml
    /// </summary>
    public partial class KalenderDagDetaills : Window
    {
        private string _datum;

        public string Datum
        {
            get { return _datum; }
            set { _datum = value; }
        }

        private Sessie _sessie;

        public Sessie Sessie
        {
            get { return _sessie; }
            set { _sessie = value; }
        }

        private string _datumLblText;

        public string DatumLblText
        {
            get { return _datumLblText; }
        }
        

        private Gebruiker currentUser;        
              
        public KalenderDagDetaills(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime datum;
            if (!DateTime.TryParse(Datum, out datum))
                datum = DateTime.Today;

            SessieServices sService = new SessieServices();
            List<Sessie> sessies = sService.GetSessieByDate(datum);
            sessieListBox.ItemsSource = sessies;
            _datumLblText = datum.ToShortDateString();
            if (sessies.Count > 1)
            {
                _datumLblText += ": " + sessies.Count + " sessies";
            }
            else if (sessies.Count == 1)
                _datumLblText += ": " + sessies.Count + " sessie";
            datumLabel.Content = _datumLblText;
            //sessieListBox.DisplayMemberPath = "SessieNaamEnDeelnemers";
        }

        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            Close();
        }

        private void sessieListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sessie = (Sessie)sessieListBox.SelectedItem;
            DateTime datum;
            if (!DateTime.TryParse(Datum, out datum))
                datum = DateTime.Today;
            NieuweSeminarie NS = new NieuweSeminarie(currentUser, Sessie.Seminarie, datum, Sessie, false);
            if (NS.ShowDialog() == true)
            {
                Close();
            }
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            Border border = (Border)sender;
            border.Background = new SolidColorBrush(Color.FromArgb(255, 178, 222, 232));
        }

        private void Border_MouseLeave(object sender, MouseEventArgs e)
        {
            Border border = (Border)sender;
            border.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
        }
    }
}

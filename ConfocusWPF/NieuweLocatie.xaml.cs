﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweLocatie.xaml
    /// </summary>
    public partial class NieuweLocatie : Window
    {
        private Gebruiker currentUser;
        public NieuweLocatie(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            PostcodeService postservice = new PostcodeService();
            List<Postcodes> postcodes = postservice.FindAllPostcodes();
            postcodeComboBox.ItemsSource = postcodes;
            postcodeComboBox.DisplayMemberPath = "Postcode";

            RegioService rService = new RegioService();
            List<Regio> regios = rService.GetActief();
            regioComboBox.ItemsSource = regios;
            regioComboBox.DisplayMemberPath = "Regio1";
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Locatie locatie = GetLocatieFromForm();
            if (locatie != null)
            {
                LocatieServices service = new LocatieServices();
                Locatie SavedLocatie = service.SaveLocatie(locatie);
                if (SavedLocatie != null)
                {
                    ConfocusMessageBox.Show("Locatie opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                    DialogResult = true;
                }
                else
                    ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private Locatie GetLocatieFromForm()
        {
            if (IsLocatieValid())
            {
                Locatie locatie = new Locatie();
                locatie.Naam = naamTextBox.Text;
                locatie.Adres = adresTextBox.Text;
                locatie.Postcode = postcodeComboBox.Text;
                locatie.Plaats = plaatsComboBox.Text;
                locatie.Regio = regioComboBox.Text;
                locatie.Contact = contactTextBox.Text;
                locatie.Telefoon = telefoonTextBox.Text;
                locatie.Beamer = beamerCheckBox.IsChecked == true;
                locatie.Laptop = laptopCheckBox.IsChecked == true;
                locatie.Actief = actiefCheckBox.IsChecked == true;
                locatie.Aangemaakt_door = currentUser.Naam;
                locatie.Aangemaakt = DateTime.Now;

                return locatie;
            }
            else
                return null;
        }

        private bool IsLocatieValid()
        {
            bool valid = true;
            String errortext = "";
            if (naamTextBox.Text == "")
            {
                errortext += "Er is geen naam ingegeven! Dit is een verplicht veld.\n";
                valid = false;
            }
            if (postcodeComboBox.SelectedIndex == -1)
            {
                errortext += "Er is geen postcode gekozen!\n";
                valid = false;
            }
            if (plaatsComboBox.SelectedIndex == -1)
            {
                errortext += "Er is geen plaats gekozen!\n";
                valid = false;
            }
            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }


        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PostcodeService service = new PostcodeService();
            Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
            List<Postcodes> gemeentes = service.GetGemeentesByPostcode(postcode.Postcode);
            plaatsComboBox.ItemsSource = gemeentes;
            plaatsComboBox.DisplayMemberPath = "Gemeente";
            if (gemeentes.Count > 0)
                plaatsComboBox.SelectedIndex = 0;
        }
    }
}

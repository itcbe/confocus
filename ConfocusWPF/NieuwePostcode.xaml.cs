﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwePostcode.xaml
    /// </summary>
    public partial class NieuwePostcode : Window
    {
        public NieuwePostcode()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Postcodes postcode = GetPostcodeFromForm();
            if (postcode != null)
            {
                PostcodeService service = new PostcodeService();
                try
                {
                    service.SavePostcode(postcode);
                    DialogResult = true;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Postcodes GetPostcodeFromForm()
        {
            if (IsPostcodeValid())
            {
                Postcodes p = new Postcodes();
                p.Postcode = postcodeTextBox.Text;
                p.Gemeente = gemeenteTextBox.Text;
                p.Provincie = provincieTextBox.Text;
                p.Land = landTextBox.Text;

                return p;

            }
            else
                return null;
        }

        private bool IsPostcodeValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (postcodeTextBox.Text == "")
            {
                errorMessage += "Geen postcode ingegeven!\n";
                Helper.SetTextBoxInError(postcodeTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(postcodeTextBox, normalTemplate);

            if (gemeenteTextBox.Text == "")
            {
                errorMessage += "Geen gemeente ingegeven!\n";
                Helper.SetTextBoxInError(gemeenteTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(gemeenteTextBox, normalTemplate);
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }
    }
}

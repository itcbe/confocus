﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using ITCLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ZoekSprekerScherm.xaml
    /// </summary>
    public partial class ZoekSprekerScherm : Window
    {
        private Gebruiker currentUser;
        private Int32 UserID;
        private Boolean IsScroll = false, IsButtonClick = false, IsUserClick = false;
        private CollectionViewSource contactViewSource;
        private CollectionViewSource functiesViewSource;
        private CollectionViewSource bedrijfViewSource;
        private CollectionViewSource erkenningViewSource;
        private List<Spreker> sprekers = new List<Spreker>();
        private List<Spreker> gevondenSprekers = new List<Spreker>();


        public ZoekSprekerScherm(Gebruiker currentuser)
        {
            InitializeComponent();
            currentUser = currentuser;
            GebruikerService service = new GebruikerService();
            Gebruiker user = service.GetUserByName(currentUser.Naam);
            UserID = user.ID;
            this.Title = "Zoek naar spreker/coach : Ingelogd als " + currentUser.Naam;

            SetKolomkoppen();
            ControleerMachtigingen();
        }

        private void ControleerMachtigingen()
        {
            if (currentUser.Rol != 1)
            {
                GebruikersRollenMenuItem.Visibility = Visibility.Collapsed;
                excel.Visibility = Visibility.Collapsed;
                doorPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                GebruikersRollenMenuItem.Visibility = Visibility.Visible;
                excel.Visibility = Visibility.Visible;
                doorPanel.Visibility = Visibility.Visible;
            }

        }

        /* ___________________________________________
         * |                                          |
         * |             Menu aanroepen               |
         * |__________________________________________|
         * */

        private void AansprekingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AansprekingBeheer AB = new AansprekingBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }

        private void BedrijvenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            BedrijfBeheer BB = new BedrijfBeheer(currentUser);
            BB.Show();
            BB.Activate();
        }

        private void ErkenningMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ErkenningBeheer EB = new ErkenningBeheer(currentUser, null);
            EB.Show();
            EB.Activate();
        }

        private void FunctiesMenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NiveausMenuItem_Click(object sender, RoutedEventArgs e)
        {
            NiveauBeheer NB = new NiveauBeheer(currentUser);
            NB.Show();
            NB.Activate();
        }

        private void SprekerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SprekerBeheer SB = new SprekerBeheer(currentUser);
            SB.Show();
            SB.Activate();
        }

        private void TalenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            TaalBeheer TB = new TaalBeheer(currentUser);
            TB.Show();
            TB.Activate();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Wil je het programma sluiten?", "Afsluiten", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void GebruikerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GebruikerBeheer GB = new GebruikerBeheer(currentUser);
            GB.Show();
            GB.Activate();
        }

        private void ContactMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ContactBeheer CB = new ContactBeheer(currentUser);
            CB.Show();
            CB.Activate();
        }

        private void LocatieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            LocatieBeheer LB = new LocatieBeheer(currentUser);
            LB.Show();
            LB.Activate();
        }

        private void SeminarieMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SeminarieBeheer SB = new SeminarieBeheer(currentUser);
            SB.Show();
            SB.Activate();
        }


        private void InportPostcodesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportPostcodes IP = new ImportPostcodes();
            IP.Show();
            IP.Activate();
        }

        private void ImportContactMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel IE = new ImportExcel(currentUser);
            IE.Show();
            IE.Activate();
        }

        private void ConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ConfiguratieBeheer CB = new ConfiguratieBeheer(currentUser);
            if (CB.ShowDialog() == true)
            {
                SetKolomkoppen();
            }
        }

        /**************************  Einde menu functies  *******************************/

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "excel":
                    img = excelImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "edit":
                    img = editImage;
                    break;
                case "refresh":
                    img = refreshImage;
                    break;
                case "excel":
                    img = excelImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "reset":
                    img = resetImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void goUpdate()
        {
            if (contactDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (contactViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (contactViewSource.View.CurrentPosition == contactDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                contactDataGrid.SelectedIndex = 0;
            if (contactDataGrid.Items.Count != 0)
                contactDataGrid.ScrollIntoView(contactDataGrid.SelectedItem);
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsUserClick = false;
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsUserClick = false;
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsUserClick = false;
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsUserClick = false;
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void SetKolomkoppen()
        {
            try
            {
                GebruikerService service = new GebruikerService();
                String Filename = service.GetConfigFileName(currentUser.Naam);

                if (Filename != "")
                {
                    ContactConfig config = Helper.GetContactConfigFromXml(Filename);

                    if (!config.ID)
                        iDColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        iDColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Voornaam)
                        voornaamColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        voornaamColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Achternaam)
                        achternaamColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        achternaamColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Geboortedatum)
                        geboortedatumColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        geboortedatumColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Aanspreking)
                        aansprekingColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        aansprekingColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Type)
                        typeColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        typeColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.ContactEmail)
                        ContactEmailColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        ContactEmailColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.ContactTelefoon)
                        ContactTelefoonColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        ContactTelefoonColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Taal)
                        taalColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        taalColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Niveau1)
                        niveau1Column.Visibility = System.Windows.Visibility.Hidden;
                    else
                        niveau1Column.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Niveau2)
                        niveau2Column.Visibility = System.Windows.Visibility.Hidden;
                    else
                        niveau2Column.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Niveau3)
                        niveau3Column.Visibility = System.Windows.Visibility.Hidden;
                    else
                        niveau3Column.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Bedrijf)
                        bedrijfColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        bedrijfColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Telefoon)
                        telefoonColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        telefoonColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Fax)
                        faxColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        faxColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Email)
                        bedrijfEmailColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        bedrijfEmailColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Adres)
                        adresColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        adresColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Postcode)
                        postcodeColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        postcodeColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Gemeente)
                        GemeenteColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        GemeenteColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.ContactNotities)
                        notitiesColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        notitiesColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Gewijzigd)
                        gewijzigdColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        gewijzigdColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.GewijzigdDoor)
                        gewijzigdDoorColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        gewijzigdDoorColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.Aangemaakt)
                        aangemaaktColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        aangemaaktColumn.Visibility = System.Windows.Visibility.Visible;
                    if (!config.AangemaaktDoor)
                        aangemaaktdoorColumn.Visibility = System.Windows.Visibility.Hidden;
                    else
                        aangemaaktdoorColumn.Visibility = System.Windows.Visibility.Visible;
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwContact NC = new NieuwContact(currentUser, "Spreker");
            if (NC.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            if (contactDataGrid.SelectedIndex > -1)
            {
                Spreker mySpreker = (Spreker)contactDataGrid.SelectedItem;

                NieuwContact BS = new NieuwContact(currentUser, mySpreker);
                if (BS.ShowDialog() == true)
                {
                    LoadData();
                }
            }

        }

        private void SetAantal()
        {
            if (contactDataGrid.Items.Count > 1)
                AantalLabel.Content = (contactDataGrid.SelectedIndex + 1) + " van " + (contactDataGrid.Items.Count) + " contacten.";
            else
                AantalLabel.Content = "0 van 0 contacten.";
        }


        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ActivateFilters();
            
        }

        private void zoekNaamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                ActivateFilters();
            }
        }


        private void ActivateFilters()
        {
            gevondenSprekers.Clear();
            ContactSearchFilter filter = GetSprekerFilter();
            taalTextBox.Text = "";
            OpenCVButton.Visibility = System.Windows.Visibility.Hidden;

            //new Thread(delegate () { GetContacten(filter, functie); }).Start();
            SprekerServices sService = new SprekerServices();
            gevondenSprekers = sService.GetBySearch(filter);
            contactViewSource.Source = gevondenSprekers;
            List<Contact> unieke = new List<Contact>();
            foreach (Spreker sprek in gevondenSprekers)
            {
                if (!unieke.Contains(sprek.Contact))
                    unieke.Add(sprek.Contact);
            }
            UniekAantalLabel.Content = unieke.Count() + " unieke sprekers.";
            SetAantal();
            goUpdate();
            
        }

        private ContactSearchFilter GetSprekerFilter()
        {
            ContactSearchFilter filter = new ContactSearchFilter();
            if (zoekVoornaamTextBox.Text != "")
                filter.Voornaam = DBHelper.Simplify(zoekVoornaamTextBox.Text);
            if (!string.IsNullOrEmpty(zoekAchternaamTextBox.Text))
                filter.Achternaam = DBHelper.Simplify(zoekAchternaamTextBox.Text);
            if (bedrijvenComboBox.SelectedIndex > 0)
            {
                filter.Bedrijf_ID = ((Bedrijf)bedrijvenComboBox.SelectedItem).ID;
            }
            if (emailComboBox.Text != "Kies")
            {
                filter.Email = emailComboBox.Text;
            }
            if (typeComoBox.SelectedIndex > 0)
            {
                filter.ContactType = typeComoBox.Text;
            }
            if (niveau1ComboBox.SelectedIndex > 0)
            {
                filter.Niveau1_ID = ((Niveau1)niveau1ComboBox.SelectedItem).ID;
            }
            if (niveau2ComboBox.SelectedIndex > 0)
            {
                filter.Niveau2_ID = ((Niveau2)niveau2ComboBox.SelectedItem).ID;
            }
            if (niveau3ComboBox.SelectedIndex > 0)
            {
                filter.Niveau3_ID = ((Niveau3)niveau3ComboBox.SelectedItem).ID;
            }
            if (LocatieComboBox.SelectedIndex > 0)
            {
                filter.Gemeente = LocatieComboBox.Text;
            }
            if (ProvincieComboBox.SelectedIndex > 0)
            {
                filter.Provincie = ProvincieComboBox.Text;
            }
            if (aangemaaktDatepicker.SelectedDate != null && aangemaaktDatepicker.SelectedDate > new DateTime(2014, 12, 1))
            {
                filter.Aangemaakt = (DateTime)aangemaaktDatepicker.SelectedDate;
            }
            if (doorComboBox.SelectedIndex > 0)
            {
                filter.Door = ((Gebruiker)doorComboBox.SelectedItem).Naam;
            }
            if (languageComboBox.SelectedIndex > 0)
                filter.Taal_ID = ((Taal)languageComboBox.SelectedItem).ID;
            if (faxnummerTextBox.Text != "")
                filter.Faxnummer = faxnummerTextBox.Text;
            return filter;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            MaakFiltersLeeg();
            WeergavenComboBox.SelectedIndex = 0;
            contactViewSource.Source = sprekers;
            SetAantal();
        }

        private void MaakFiltersLeeg()
        {
            bedrijvenComboBox.SelectedIndex = 0;
            emailComboBox.ItemsSource = new List<string>() { "Kies", "Leeg" };
            emailComboBox.SelectedIndex = 0;
            typeComoBox.SelectedIndex = 0;
            niveau1ComboBox.SelectedIndex = 0;
            LoadNiveau2En3();
            LocatieComboBox.SelectedIndex = 0;
            ProvincieComboBox.SelectedIndex = 0;
            aangemaaktDatepicker.SelectedDate = null;
            zoekVoornaamTextBox.Text = "";
            zoekAchternaamTextBox.Text = "";
            faxnummerTextBox.Text = "";
            doorComboBox.SelectedIndex = 0;
            //languageComboBox.SelectedIndex = 0;
        }

        private void LoadNiveau2En3()
        {
            List<Niveau2> n2lijst = new List<Niveau2>();
            Niveau2 n2 = new Niveau2();
            n2.Naam = "Leeg";
            n2lijst.Add(n2);
            niveau2ComboBox.ItemsSource = n2lijst;
            niveau2ComboBox.DisplayMemberPath = "Naam";

            List<Niveau3> n3lijst = new List<Niveau3>();
            Niveau3 n3 = new Niveau3();
            n3.Naam = "Leeg";
            n3lijst.Add(n3);
            niveau3ComboBox.ItemsSource = n3lijst;
            niveau3ComboBox.DisplayMemberPath = "Naam";
        }

        private void contactDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactDataGrid.SelectedItem is Spreker)
            {
                Spreker s = (Spreker)contactDataGrid.SelectedItem;
                if (s.CV == "")
                    OpenCVButton.Visibility = System.Windows.Visibility.Hidden;
                else
                    OpenCVButton.Visibility = System.Windows.Visibility.Visible;
                Contact contact = s.Contact;
                taalTextBox.Text= "";
                if (contact.Taal_ID != null)
                {
                    decimal taalid = (decimal)contact.Taal_ID;
                    TaalServices taalservice = new TaalServices();
                    Taal taal = taalservice.GetTaalByID(taalid);
                    taalTextBox.Text = taal.Naam;
                }
                secundaireTaalTextBox.Text = "";
                if (contact.Secundaire_Taal_ID != null)
                {
                    
                    decimal taalid = (decimal)contact.Secundaire_Taal_ID;
                    TaalServices taalservice = new TaalServices();
                    Taal taal = taalservice.GetTaalByID(taalid);
                    secundaireTaalTextBox.Text = taal.Naam;
                }
                //contactErkenningTextBox.Text = "";
                //if (contact.Erkenning_ID != null)
                //{
                //    ErkenningServices eService = new ErkenningServices();
                //    Erkenning erkenning = eService.GetErkenningByID(contact.Erkenning_ID);
                //    contactErkenningTextBox.Text = erkenning.Naam;
                //}

                // Aangemaakt / Gewijzigd Labels invullen.

                if (s.Contact.Gewijzigd == null)
                    ContactAangemaakLabelt.Content = "Aangemaakt op " + s.Contact.Aangemaakt + " door " + s.Contact.Aangemaakt_door + ".";
                else
                    ContactAangemaakLabelt.Content = "Gewijzigd op " + s.Contact.Gewijzigd + " door " + s.Contact.Gewijzigd_door + ".";

                if (s.Gewijzigd == null)
                    SprekerAangemaaktLabel.Content = "Aangemaakt op " + s.Aangemaakt + " door " + s.Aangemaakt_door + ".";
                else
                    SprekerAangemaaktLabel.Content = "Gewijzigd op " + s.Gewijzigd + " door " + s.Gewijzigd_door + ".";

                //if (s.currentBedrijf.Gewijzigd == null)
                //    BedrijfAangemaaktLabel.Content = "Aangemaakt op " + s.currentBedrijf.Aangemaakt + " door " + s.currentBedrijf.Aangemaakt_door + ".";
                //else
                //    BedrijfAangemaaktLabel.Content = "Gewijzigd op " + s.currentBedrijf.Gewijzigd + " door " + s.currentBedrijf.Gewijzigd_door + ".";

            }
            else
            {
                taalTextBox.Text = "";
                secundaireTaalTextBox.Text = "";
            }
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdate();
                IsScroll = false;
            }
            SetAantal();
        }

        private void contactDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                if (contactDataGrid.SelectedIndex < (contactDataGrid.Items.Count - 1))
                {
                    NextButton_Click(contactDataGrid, new RoutedEventArgs());
                }
            }
            else if (e.Key == Key.Up)
            {
                if (contactDataGrid.SelectedIndex > 0)
                {
                    PrevButton_Click(contactDataGrid, new RoutedEventArgs());
                }
            }
        }

        private void ClearSpreker()
        {
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            contactViewSource = ((CollectionViewSource)(this.FindResource("contactViewSource")));
            functiesViewSource = ((CollectionViewSource)(this.FindResource("functiesViewSource")));
            bedrijfViewSource = ((CollectionViewSource)(this.FindResource("bedrijfViewSource")));
            erkenningViewSource = ((CollectionViewSource)(this.FindResource("erkenningViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            // Gegevens laden die nodig zijn in het scherm
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;
            //VerbergSprekerGrid();

            LoadWeergaveComboBox();

            //BedrijfServices bedrijfService = new BedrijfServices();
            //List<Bedrijf> bedrijven = bedrijfService.FindActive();
            //Bedrijf bedrijf = new Bedrijf();
            //bedrijf.Naam = "Kies";
            //bedrijven.Insert(0, bedrijf);
            //bedrijvenComboBox.ItemsSource = bedrijven;
            //bedrijvenComboBox.DisplayMemberPath = "Naam";
            //bedrijvenComboBox.SelectedIndex = 0;

            TaalServices tService = new TaalServices();
            List<Taal> talen = tService.FindAll();
            Taal taal = new Taal();
            taal.Naam = "Kies";
            talen.Insert(0, taal);
            languageComboBox.ItemsSource = talen;
            languageComboBox.DisplayMemberPath = "Naam";
            languageComboBox.SelectedIndex = 0;
            //taalComboBox.ItemsSource = talen;
            //taalComboBox.DisplayMemberPath = "Naam";
            //secundaireTaalComboBox.ItemsSource = talen;
            //secundaireTaalComboBox.DisplayMemberPath = "Naam";

            //ErkenningServices eService = new ErkenningServices();
            //List<Erkenning> erkenningen = eService.FindActive();
            //contactErkenningComboBox.ItemsSource = erkenningen;
            //contactErkenningComboBox.DisplayMemberPath = "Naam";

            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> n1Lijst = n1Service.FindActive();
            Niveau1 n1 = new Niveau1();
            n1.Naam = "Kies";
            n1Lijst.Insert(0, n1);
            niveau1ComboBox.ItemsSource = n1Lijst;
            niveau1ComboBox.DisplayMemberPath = "Naam";
            niveau1ComboBox.SelectedIndex = 0;
            //sprekerNiveau1ComboBox.ItemsSource = n1Lijst;
            //sprekerNiveau1ComboBox.DisplayMemberPath = "Naam";

            LoadNiveau2En3();

            PostcodeService postService = new PostcodeService();
            List<Postcodes> gemeentes = postService.FindAllGemeentes();
            Postcodes gemeente = new Postcodes();
            gemeente.Gemeente = "Kies";
            gemeentes.Insert(0, gemeente);
            LocatieComboBox.ItemsSource = gemeentes;
            LocatieComboBox.DisplayMemberPath = "Gemeente";
            LocatieComboBox.SelectedIndex = 0;
            List<Postcodes> postcodes = postService.FindAllPostcodes();

            emailComboBox.ItemsSource = new List<string>() { "Kies", "Leeg" };
            emailComboBox.SelectedIndex = 0;

            List<Gebruiker> doorgebruikers = new GebruikerService().GetAvtiveGebruikers();
            Gebruiker dummy = new Gebruiker();
            dummy.Naam = "";
            doorgebruikers.Insert(0, dummy);
            doorComboBox.ItemsSource = doorgebruikers;
            doorComboBox.DisplayMemberPath = "Naam";

            //bedrijfPostcodeComboBox.ItemsSource = postcodes;
            //bedrijfPostcodeComboBox.DisplayMemberPath = "Postcode";

            //FunctieServices functieService = new FunctieServices();
            //functies = functieService.FindActive();
            //contactViewSource.Source = functies;
            //LoadSprekers();
            SetAantal();
            goUpdate();
        }

        private void LoadSprekers()
        {
            SprekerServices sprekerservice = new SprekerServices();
            sprekers = sprekerservice.FindActive();
            contactViewSource.Source = sprekers;

        }

        private void LoadWeergaveComboBox()
        {
            WeergaveServices weergaveService = new WeergaveServices();
            List<Weergave> weergaven = weergaveService.GetWeergavesByUserId(UserID);
            Weergave gave = new Weergave();
            gave.Naam = "Kies";
            weergaven.Insert(0, gave);
            WeergavenComboBox.ItemsSource = weergaven;
            WeergavenComboBox.DisplayMemberPath = "Naam";
            WeergavenComboBox.SelectedIndex = 0;
        }

        private void zoekKlantMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainWindow MW = new MainWindow(currentUser);
            MW.Show();
            MW.Activate();
            this.Close();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Splash SS = new Splash();
            SS.Show();
            SS.Activate();
            this.Close();
        }

        private void OpenCVButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (contactDataGrid.SelectedIndex > - 1)
                {
                    Spreker s = (Spreker)contactDataGrid.SelectedItem;
                    System.Diagnostics.Process.Start(s.CV);
                    //Process wordProcess = new Process();
                    //wordProcess.StartInfo.FileName = s.CV;
                    //wordProcess.StartInfo.UseShellExecute = true;
                    //wordProcess.Start();
                }
            }
            catch (Exception)
            {
                ConfocusMessageBox.Show("Kan CV niet openen.", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void ToExcel()
        {
            int lengte = contactDataGrid.Items.Count;
            String[] headers = GetActiveHeaders();
            String[][] contacten = new String[lengte][];
            int teller = 0;
            foreach (Object item in contactDataGrid.Items)
            {
                if (item is Spreker)
                {
                    Spreker spreker = item as Spreker;

                    String[] lid = GetActiveData(spreker, headers);
                    contacten[teller] = lid;
                    teller++;
                }
            }

            ExcelDocument Xl = new ExcelDocument();
            Xl.SetColumnHeaders(headers, 1, true, true, true);
            Xl.InsertList(contacten, true, 2, new int[0], 20);
            object misValue = System.Reflection.Missing.Value;
            //Xl.Save();
            //SendMail(Xl.fileName);
            //Xl.Close();  
        }

        private string[] GetActiveData(Spreker spreker, string[] headers)
        {
            List<String> mySpreker = new List<string>();
            if (headers.Contains("ID"))
                mySpreker.Add(spreker.Contact_ID.ToString());
            if (headers.Contains("Voornaam"))
                mySpreker.Add(spreker.Contact.Voornaam);
            if (headers.Contains("Achternaam"))
                mySpreker.Add(spreker.Contact.Achternaam);
            if (headers.Contains("Geboortedatum"))
            {
                if (spreker.Contact.Geboortedatum != null)
                    mySpreker.Add(((DateTime)spreker.Contact.Geboortedatum).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Aanspreking"))
                mySpreker.Add(spreker.Contact.Aanspreking);
            if (headers.Contains("Type"))
                mySpreker.Add(spreker.Type);
            if (headers.Contains("Contact email"))
                mySpreker.Add(spreker.Email);
            if (headers.Contains("Contact telefoon"))
                mySpreker.Add(spreker.Telefoon);

            if (headers.Contains("Taal"))
                if (spreker.Contact.Taal != null)
                    mySpreker.Add(spreker.Contact.Taal.Naam);
                else
                    mySpreker.Add("");

            if (headers.Contains("Niveau1"))
                mySpreker.Add(spreker.Niveau1.Naam);
            if (headers.Contains("Niveau2"))
            {
                if (spreker.Niveau2 != null)
                    mySpreker.Add(spreker.Niveau2.Naam);
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Niveau3"))
            {
                if (spreker.Niveau3 != null)
                    mySpreker.Add(spreker.Niveau3.Naam);
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Bedrijf"))
                mySpreker.Add(spreker.Bedrijf.Naam);
            if (headers.Contains("Email"))
                mySpreker.Add(spreker.Bedrijf.Email);
            if (headers.Contains("Adres"))
                mySpreker.Add(spreker.Bedrijf.Adres);
            if (headers.Contains("Postcode"))
                mySpreker.Add(spreker.Bedrijf.Postcode);
            if (headers.Contains("Gemeente"))
                mySpreker.Add(spreker.Bedrijf.Plaats);

            if (headers.Contains("Notities"))
                mySpreker.Add(spreker.Contact.Notities);
            if (headers.Contains("Gewijzigd"))
            {
                if (spreker.Contact.Gewijzigd != null)
                    mySpreker.Add(((DateTime)spreker.Contact.Gewijzigd).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Gewijzigd door"))
                mySpreker.Add(spreker.Contact.Gewijzigd_door);
            if (headers.Contains("Aangemaakt"))
            {
                if (spreker.Contact.Aangemaakt != null)
                    mySpreker.Add(((DateTime)spreker.Contact.Aangemaakt).ToShortDateString());
                else
                    mySpreker.Add("");
            }
            if (headers.Contains("Aangemaakt door"))
                mySpreker.Add(spreker.Contact.Aangemaakt_door);
            
            return mySpreker.ToArray();
        }

        private string[] GetActiveHeaders()
        {
            List<String> myHeaders = new List<string>();
            GebruikerService service = new GebruikerService();
            String Filename = service.GetConfigFileName(currentUser.Naam);

            if (Filename != "")
            {
                ContactConfig config = Helper.GetContactConfigFromXml(Filename);
                if (config.ID)
                    myHeaders.Add("ID");
                if (config.Voornaam)
                    myHeaders.Add("Voornaam");
                if (config.Achternaam)
                    myHeaders.Add("Achternaam");
                if (config.Geboortedatum)
                    myHeaders.Add("Geboortedatum");
                if (config.Aanspreking)
                    myHeaders.Add("Aanspreking");
                if (config.Type)
                    myHeaders.Add("Type");
                if (config.ContactEmail)
                    myHeaders.Add("Contact email");
                if (config.ContactTelefoon)
                    myHeaders.Add("Contact telefoon");
                if (config.Taal)
                    myHeaders.Add("Taal");
                if (config.Niveau1)
                    myHeaders.Add("Niveau1");
                if (config.Niveau2)
                    myHeaders.Add("Niveau2");
                if (config.Niveau3)
                    myHeaders.Add("Niveau3");
                if (config.Bedrijf)
                    myHeaders.Add("Bedrijf");
                if (config.Email)
                    myHeaders.Add("Email");
                if (config.Adres)
                    myHeaders.Add("Adres");
                if (config.Postcode)
                    myHeaders.Add("Postcode");
                if (config.Gemeente)
                    myHeaders.Add("Gemeente");
                if (config.ContactNotities)
                    myHeaders.Add("Notities");
                if (config.Gewijzigd)
                    myHeaders.Add("Gewijzigd");
                if (config.GewijzigdDoor)
                    myHeaders.Add("Gewijzigd door");
                if (config.Aangemaakt)
                    myHeaders.Add("Aangemaakt");
                if (config.AangemaaktDoor)
                    myHeaders.Add("Aangemaakt door");
            }
            return myHeaders.ToArray();
        }

        private void excelButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() => { ToExcel(); });
        }

        private void SaveWeergaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (WeergavenComboBox.SelectedIndex > 0)
            {
                Weergave geselecteerde = (Weergave)WeergavenComboBox.SelectedItem;
                Weergave myWeergave = GetWeergaveFromForm();
                if (myWeergave != null)
                {
                    myWeergave.ID = geselecteerde.ID;
                    myWeergave.Naam = geselecteerde.Naam;
                    myWeergave.User_ID = geselecteerde.User_ID;
                    WeergaveServices service = new WeergaveServices();
                    Weergave Saved = service.SaveWeergaveChanges(myWeergave);
                    if (Saved != null)
                    {
                        ConfocusMessageBox.Show("Weergave opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                        LoadWeergaveComboBox();
                    }
                    else
                        ConfocusMessageBox.Show("Fout bij het opslaan van de weergave!", ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                Weergave myWeergave = GetWeergaveFromForm();
                if (myWeergave != null)
                {
                    WeergaveInputWindow WW = new WeergaveInputWindow();
                    if (WW.ShowDialog() == true)
                    {
                        myWeergave.Naam = WW.Naam;
                        if (myWeergave != null)
                        {
                            WeergaveServices service = new WeergaveServices();
                            Weergave Saved = service.SaveWeergave(myWeergave);
                            if (Saved != null)
                            {
                                ConfocusMessageBox.Show("Weergave opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                LoadWeergaveComboBox();
                            }
                            else
                                ConfocusMessageBox.Show("Fout bij het opslaan van de weergave!", ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }
            }
        }

        private Weergave GetWeergaveFromForm()
        {
            try
            {
                Boolean IsLeeg = true;
                Weergave weergave = new Weergave();
                weergave.User_ID = UserID;
                if (bedrijvenComboBox.Text != "")
                    IsLeeg = false;
                if (emailComboBox.Text != "")
                    IsLeeg = false;
                if (typeComoBox.Text != "")
                    IsLeeg = false;
                if (niveau1ComboBox.Text != "")
                    IsLeeg = false;
                if (niveau2ComboBox.Text != "")
                    IsLeeg = false;
                if (niveau3ComboBox.Text != "")
                    IsLeeg = false;
                if (LocatieComboBox.Text != "")
                    IsLeeg = false;
                if (ProvincieComboBox.Text != "")
                    IsLeeg = false;
                if (zoekVoornaamTextBox.Text != "")
                    IsLeeg = false;
                if (aangemaaktDatepicker.SelectedDate != null)
                    IsLeeg = false;

                weergave.Bedrijf = bedrijvenComboBox.Text;
                weergave.Email = emailComboBox.Text;
                weergave.Type = typeComoBox.Text;
                weergave.Niveau1 = niveau1ComboBox.Text;
                weergave.Niveau2 = niveau2ComboBox.Text;
                weergave.Niveau3 = niveau3ComboBox.Text;
                weergave.Gemeente = LocatieComboBox.Text;
                weergave.Provincie = ProvincieComboBox.Text;
                weergave.Zoeknaam = zoekVoornaamTextBox.Text;
                weergave.Achternaam = zoekAchternaamTextBox.Text;
                if (aangemaaktDatepicker.SelectedDate != null)
                    weergave.Datum = aangemaaktDatepicker.SelectedDate;

                if (IsLeeg)
                    return null;
                else
                    return weergave;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void WeergavenComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Weergave gekozenWeergave = (Weergave)box.SelectedItem;
                SetFilters(gekozenWeergave);
            }
        }

        private void SetFilters(Weergave weergave)
        {
            try
            {
                MaakFiltersLeeg();
                if (weergave.Bedrijf != "")
                    bedrijvenComboBox.Text = weergave.Bedrijf;
                if (weergave.Email != "")
                    emailComboBox.Text = weergave.Email;
                if (weergave.Type != null)
                    typeComoBox.Text = weergave.Type;
                if (weergave.Niveau1 != "")
                    niveau1ComboBox.Text = weergave.Niveau1;
                if (weergave.Niveau2 != "")
                    niveau2ComboBox.Text = weergave.Niveau2;
                if (weergave.Niveau3 != "")
                    niveau3ComboBox.Text = weergave.Niveau3;
                if (weergave.Gemeente != "")
                    LocatieComboBox.Text = weergave.Gemeente;
                if (weergave.Provincie != "")
                    ProvincieComboBox.Text = weergave.Provincie;
                if (weergave.Zoeknaam != "")
                    zoekVoornaamTextBox.Text = weergave.Zoeknaam;
                if (!string.IsNullOrEmpty(weergave.Achternaam))
                    zoekAchternaamTextBox.Text = weergave.Achternaam;
                if (weergave.Datum != null)
                    aangemaaktDatepicker.SelectedDate = weergave.Datum;

                ActivateFilters();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van de weergave." + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        /**************************** Contact details wijzigingen *************************************/

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                Contact contact = ((Spreker)contactDataGrid.SelectedItem).Contact;
                switch (box.Name)
                {
                    case "voornaamTextBox":
                        contact.Voornaam = box.Text;
                        break;
                    case "achternaamTextBox":
                        contact.Achternaam = box.Text;
                        break;
                    case "notitiesTextBox1":
                        contact.Notities = box.Text;
                        break;
                    default:
                        break;
                }

                SaveContactChanges(contact);
            }
        }

        private void SaveContactChanges(Contact contact)
        {
            if (contact != null)
            {
                contact.Gewijzigd = DateTime.Now;
                contact.Gewijzigd_door = currentUser.Naam;

                try
                {
                    ContactServices sevice = new ContactServices();
                    sevice.SaveContactChanges(contact);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        //private void TextBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{

        //    if (IsUserClick)
        //    {
        //        ComboBox box = (ComboBox)sender;
        //        Contact contact = ((Spreker)contactDataGrid.SelectedItem).Contact;
        //        switch (box.Name)
        //        {
        //            case "aansprekingTextBox":

        //                if (box.SelectedIndex > -1)
        //                    contact.Aanspreking = box.SelectedIndex == 0 ? "M" : "V";
        //                break;
        //            case "typeComboBox":
        //                if (box.SelectedIndex > -1)
        //                    contact.Type = box.SelectedIndex == 0 ? "Coach" : "Spreker";
        //                break;
        //            case "taalComboBox":
        //                if (box.SelectedIndex > -1)
        //                    contact.Taal_ID = ((Taal)box.SelectedItem).ID;
        //                break;
        //            case "secundaireTaalComboBox": 
        //                if (box.SelectedIndex > -1)
        //                    contact.Secundaire_Taal_ID = ((Taal)box.SelectedItem).ID;
        //                break;
        //            case "contactErkenningComboBox":
        //                if (box.SelectedIndex > -1)
        //                    contact.Erkenning_ID = ((Erkenning)contactErkenningComboBox.SelectedItem).ID;
        //                break;
        //            default:
        //                break;
        //        }

        //        SaveContactChanges(contact);
        //        IsUserClick = false;
        //    }
        //}

        private void ComboBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //IsUserClick = true;
        }

        private void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            IsUserClick = true;
        }

        private void geboortedatumTextBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsUserClick)
            {
                Contact contact = ((Spreker)contactDataGrid.SelectedItem).Contact;
                DatePicker datum = (DatePicker)sender;
                contact.Geboortedatum = datum.SelectedDate;
                SaveContactChanges(contact);
                IsUserClick = false;
            }
        }

        private void geboortedatumTextBox_CalendarOpened(object sender, RoutedEventArgs e)
        {
            IsUserClick = true;
        }

        /*********************** Spreker detail wijzigingen *************************/

        //private void sprekerNiveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    sprekerNiveau2ComboBox.ItemsSource = new List<Niveau2>();
        //    sprekerNiveau2ComboBox.ItemsSource = new List<Niveau3>();
        //    ComboBox box = (ComboBox)sender;
        //    Niveau1 n1 = (Niveau1)box.SelectedItem;
        //    if (n1 != null)
        //    {
        //        Niveau2Services n2Service = new Niveau2Services();
        //        List<Niveau2> n2Lijst = n2Service.GetSelected(n1.ID);
        //        sprekerNiveau2ComboBox.ItemsSource = n2Lijst;
        //        sprekerNiveau2ComboBox.DisplayMemberPath = "Naam";

        //        if (IsUserClick)
        //        {
        //            Spreker spreker = (Spreker)contactDataGrid.SelectedItem;
        //            spreker.Niveau1_ID = n1.ID;
        //            spreker.Niveau1 = n1;
        //            spreker.Niveau2 = null;
        //            spreker.Niveau2_ID = null;
        //            spreker.Niveau3 = null;
        //            spreker.Niveau3_ID = null;

        //            SaveSprekerDetailChanges(spreker);
        //            IsUserClick = false;
        //        }
        //    }
        //}

        private void SaveSprekerDetailChanges(Spreker spreker)
        {
            spreker.Gewijzigd = DateTime.Now;
            spreker.Gewijzigd_door = currentUser.Naam;
            
            try
            {
                SprekerServices service = new SprekerServices();
                service.SaveSprekerWijzigingen(spreker);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van de Spreker/Coach!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void sprekerNiveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    sprekerNiveau3ComboBox.ItemsSource = new List<Niveau3>();
        //    ComboBox box = (ComboBox)sender;
        //    if (box.SelectedIndex > -1)
        //    {
        //        Niveau2 n2 = (Niveau2)box.SelectedItem;
        //        Niveau3Services n3Service = new Niveau3Services();
        //        List<Niveau3> n3Lijst = n3Service.GetSelected(n2.ID);
        //        sprekerNiveau3ComboBox.ItemsSource = n3Lijst;
        //        sprekerNiveau3ComboBox.DisplayMemberPath = "Naam";

        //        if (IsUserClick)
        //        {
        //            Spreker spreker = (Spreker)contactDataGrid.SelectedItem;
        //            spreker.Niveau2 = n2;
        //            spreker.Niveau2_ID = n2.ID;
        //            spreker.Niveau3 = null;
        //            spreker.Niveau3_ID = null;

        //            SaveSprekerDetailChanges(spreker);
        //            IsUserClick = false;
        //        }
        //    }
        //}

        //private void sprekerNiveau3ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        ComboBox box = (ComboBox)sender;
        //        Niveau3 n3 = (Niveau3)box.SelectedItem;
        //        if (n3 != null)
        //        {
        //            Spreker spreker = (Spreker)contactDataGrid.SelectedItem;
        //            spreker.Niveau3 = n3;
        //            spreker.Niveau3_ID = n3.ID;

        //            SaveSprekerDetailChanges(spreker);
        //        }
        //        IsUserClick = false;
        //    }
        //}

        private void tariefTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                Spreker spreker = (Spreker)contactDataGrid.SelectedItem;

                switch (box.Name)
                {
                    case "tariefTextBox":
                        spreker.Tarief = box.Text;
                        break;
                    case "sprekerEmailTextBox":
                        spreker.Email = box.Text;
                        break;
                    case "sprekerTelefoonTextBox":
                        spreker.Telefoon = box.Text;
                        break;
                    case "sprekerMobielTextBox":
                        spreker.Mobiel = box.Text;
                        break;
                    default:
                        break;
                }
                

                SaveSprekerDetailChanges(spreker);
            }
        }

        //private void waarderingComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        if (waarderingComboBox.SelectedIndex > -1)
        //        {
        //            Int32 waarde = Int32.Parse(((ComboBoxItem)waarderingComboBox.SelectedItem).Content.ToString());
        //            Spreker spreker = (Spreker)contactDataGrid.SelectedItem;
        //            spreker.Waardering = waarde;

        //            SaveSprekerDetailChanges(spreker);
        //        }
        //        IsUserClick = false;
        //    }
        //}

        /************************** Bedrijf details wijzigingen ********************************/

        private void bedrijfTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                TextBox box = (TextBox)sender;
                Bedrijf bedrijf = ((Spreker)contactDataGrid.SelectedItem).Bedrijf;
                switch (box.Name)
                {
                    case "naamTextBox3":
                        bedrijf.Naam = box.Text;
                        break;
                    case "ondernemersnummerTextBox":
                        bedrijf.Ondernemersnummer = box.Text;
                        break;
                    case "adresTextBox":
                        bedrijf.Adres = box.Text;
                        break;
                    case "telefoonTextBox1":
                        bedrijf.Telefoon = box.Text;
                        break;
                    case "faxTextBox":
                        bedrijf.Fax = box.Text;
                        break;
                    case "emailTextBox1":
                        bedrijf.Email = box.Text;
                        break;
                    case "websiteTextBox":
                        bedrijf.Website = box.Text;
                        break;
                    case "notitiesTextBox":
                        bedrijf.Notities = box.Text;
                        break;
                    default:
                        break;
                }

                SaveBedrijfDetailChanges(bedrijf);
            }
        }

        private void SaveBedrijfDetailChanges(Bedrijf bedrijf)
        {
            bedrijf.Gewijzigd = DateTime.Now;
            bedrijf.Gewijzigd_door = currentUser.Naam;
            try
            {
                BedrijfServices service = new BedrijfServices();
                service.SaveBedrijfChanges(bedrijf);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        //private void bedrijfPostcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    PostcodeService service = new PostcodeService();
        //    Postcodes postcode = (Postcodes)bedrijfPostcodeComboBox.SelectedItem;
        //    if (postcode != null)
        //    {
        //        List<Postcodes> gemeentes = service.GetGemeentesByPostcode(postcode.Postcode);
        //        bedrijfPlaatsComboBox.ItemsSource = gemeentes;
        //        bedrijfPlaatsComboBox.DisplayMemberPath = "Gemeente";
        //        bedrijfPlaatsComboBox.SelectedIndex = 0;
        //        //bedrijfProvincieComboBox.Text = postcode.Provincie;
        //        landTextBox.Text = postcode.Land;

        //        //if (bedrijfProvincieComboBox.Items.Contains(postcode.Provincie))
        //            bedrijfProvincieComboBox.Text = postcode.Provincie;
        //        //else
        //            //bedrijfProvincieComboBox.SelectedIndex = -1;

        //        if (IsUserClick)
        //        {
        //            Bedrijf bedrijf = ((Spreker)contactDataGrid.SelectedItem).Bedrijf;
        //            currentBedrijf.Postcode = postcode.Postcode;
        //            currentBedrijf.Plaats = postcode.Gemeente;
        //            currentBedrijf.Provincie = postcode.Provincie;
        //            currentBedrijf.Land = postcode.Land;

        //            SaveBedrijfDetailChanges(bedrijf);
        //            IsUserClick = false;
        //        }
        //    }
        //    else
        //    {
        //        bedrijfPostcodeComboBox.SelectedIndex = -1;
        //        bedrijfPlaatsComboBox.SelectedIndex = -1;
        //        bedrijfProvincieComboBox.SelectedIndex = -1;
        //    }
        //}

        private void BedrijfComboBox_DropDownOpened(object sender, EventArgs e)
        {
            IsUserClick = true;
        }

        //private void bedrijfPlaatsComboBox_DropDownClosed(object sender, EventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        Postcodes postcode = (Postcodes)bedrijfPlaatsComboBox.SelectedItem;
        //        if (postcode != null)
        //        {
        //            Bedrijf bedrijf = ((Spreker)contactDataGrid.SelectedItem).Bedrijf;
        //            currentBedrijf.Plaats = postcode.Gemeente;

        //            SaveBedrijfDetailChanges(bedrijf);
        //            IsUserClick = false;
        //        }
        //    }

        //}

        //private void bedrijfProvincieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (IsUserClick)
        //    {
        //        Postcodes postcode = (Postcodes)bedrijfPostcodeComboBox.SelectedItem;
        //        if (bedrijfProvincieComboBox.SelectedIndex > -1)
        //        {
        //            String provincie = ((ComboBoxItem)bedrijfProvincieComboBox.SelectedItem).Content.ToString();
        //            Bedrijf bedrijf = ((Spreker)contactDataGrid.SelectedItem).Bedrijf;
        //            currentBedrijf.Provincie = provincie;

        //            if (postcode.Provincie == null)
        //            {
        //                postcode.Provincie = provincie;
        //                PostcodeService postService = new PostcodeService();
        //                postService.ChangePostcode(postcode);
        //            }


        //            SaveBedrijfDetailChanges(bedrijf);
        //        }
        //        IsUserClick = false;
        //    }
        //}

        private void OpenSiteButton_Click(object sender, RoutedEventArgs e)
        {
            //if (websiteTextBox.Text != "")
            //{
            //    String url = websiteTextBox.Text;
            //    try
            //    {
            //        System.Diagnostics.Process.Start(url);
            //    }
            //    catch (Exception ex)
            //    {
            //        ConfocusMessageBox.Show("Fout bij het openen van de website!\nControleer of de URL correct is en probeer opnieuw.\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            //    }
            //}
        }

        private void OpenMailButton_Click(object sender, RoutedEventArgs e)
        {
            //if (emailTextBox1.Text != "")
            //{
            //    String url = "mailto:'" + emailTextBox1.Text + "'";
            //    try
            //    {
            //        System.Diagnostics.Process.Start(url);
            //    }
            //    catch (Exception ex)
            //    {
            //        ConfocusMessageBox.Show("Fout bij het openen van het email programma!\nControleer of het adres correct is en probeer opnieuw.\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            //    }
            //}
        }

        private void bedrijfPlaatsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void KalenderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Kalender kalender = new Kalender(currentUser);
            kalender.Show();
            kalender.Activate();
        }

        private void MarketingkanaalMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Markeningkanalen MK = new Markeningkanalen(currentUser);
            MK.Show();
            MK.Activate();
        }

        private void AttesttypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AttesttypeBeheer AB = new AttesttypeBeheer(currentUser);
            AB.Show();
            AB.Activate();
        }

        private void contactDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.Target is System.Windows.Controls.TextBlock
                || e.MouseDevice.Target is System.Windows.Controls.TextBox
                || e.MouseDevice.Target is System.Windows.Controls.Border)
            {
                Spreker spreker = (Spreker)contactDataGrid.SelectedItem;
                NieuwContact BS = new NieuwContact(currentUser, spreker);
                if (BS.ShowDialog() == true)
                {
                    ActivateFilters();
                }
            }
        }

        private void sprekerDetailHideButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Toon details")
            {
                button.Content = "Verberg details";
                sprekerDetailRow.Height = new GridLength(300);
            }
            else
            {
                button.Content = "Toon details";
                sprekerDetailRow.Height = new GridLength(30);
            }
        }

        private void meerButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (button.Content.ToString() == "Meer")
            {
                button.Content = "Minder";
                searchPanel.Height = new GridLength(120);
            }
            else
            {
                button.Content = "Meer";
                searchPanel.Height = new GridLength(65);
            }
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Niveau1 n1 = (Niveau1)box.SelectedItem;

                Niveau2Services service = new Niveau2Services();
                List<Niveau2> n2lijst = service.GetSelected(n1.ID);
                Niveau2 n2 = new Niveau2();
                n2.Naam = "Leeg";
                n2lijst.Insert(0, n2);
                niveau2ComboBox.ItemsSource = n2lijst;
                niveau2ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Niveau2 n2 = (Niveau2)box.SelectedItem;

                Niveau3Services service = new Niveau3Services();
                List<Niveau3> n3lijst = service.GetSelected(n2.ID);
                Niveau3 n3 = new Niveau3();
                n3.Naam = "Leeg";
                n3lijst.Insert(0, n3);
                niveau3ComboBox.ItemsSource = n3lijst;
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void ZoekBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekContactWindow ZB = new ZoekContactWindow("Bedrijf");
            if (ZB.ShowDialog() == true)
            {
                List<Bedrijf> bedrijven = new List<Bedrijf>();
                Bedrijf bedrijf = new Bedrijf();
                bedrijf.Naam = "Kies";
                bedrijven.Add(bedrijf);
                bedrijven.Add(ZB.bedrijf);
                bedrijvenComboBox.ItemsSource = bedrijven;
                bedrijvenComboBox.DisplayMemberPath = "Naam";
                bedrijvenComboBox.SelectedIndex = 1;
            }
        }

        private void GebruikersRollenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GebruikersRolen GR = new GebruikersRolen(currentUser);
            GR.Show();
            GR.Activate();
        }

        private void faxnummerTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
                ActivateFilters();
        }

        private void ZoekEmailButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekEmailInvoerWindow EW = new ZoekEmailInvoerWindow();
            if (EW.ShowDialog() == true)
            {
                emailComboBox.ItemsSource = new List<string>() { "Kies", "Leeg", EW.Emailadres };
                emailComboBox.SelectedIndex = 2;
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            ShowSearchWindow();
        }

        public void ShowSearchWindow()
        {
            ReplaceSpeakerEmailWindow RW = new ReplaceSpeakerEmailWindow(currentUser);
            if (RW.ShowDialog() == true)
            {
                if (contactDataGrid.Items.Count > 0)
                {
                    Spreker spreker = (Spreker)contactDataGrid.SelectedItem;
                    ActivateFilters();
                }
            }
            //ZoekVervangWindow ZW = new ZoekVervangWindow();
            //if (ZW.ShowDialog() == true)
            //{
            //    ReplaceEmail mail = ZW.replaceEmail;
            //    SprekerServices sService = new SprekerServices();
            //    int aantal = sService.ReplaceEmail(mail);
            //    ConfocusMessageBox.Show(aantal + " emailadressen vervangen.", ConfocusMessageBox.Kleuren.Blauw);

            //    //ReLoadSpreker(null);
            //}
        }
    }
}

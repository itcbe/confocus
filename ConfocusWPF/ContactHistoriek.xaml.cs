﻿using ConfocusClassLibrary;
using ITCLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using data = ConfocusDBLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ContactHistoriek.xaml
    /// </summary>
    public partial class ContactHistoriek : Window
    {
        List<data.Inschrijvingen> inschrijvingen = new List<data.Inschrijvingen>();
        public ContactHistoriek(List<data.Inschrijvingen> inschrijvingen)
        {
            InitializeComponent();
            this.inschrijvingen = inschrijvingen;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource inschrijvingenViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("inschrijvingenViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            inschrijvingenViewSource.Source = this.inschrijvingen;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.DefaultExt = ".csv";
            savedialog.Filter = "Excel|*.csv";

            //WeergaveInputWindow WI = new WeergaveInputWindow();
            if (savedialog.ShowDialog() == true)
            {
                string fileName = savedialog.FileName;//@"C:/Confocus Data/" + WI.Naam + ".csv";

                try
                {
                    data.InschrijvingenService iService = new data.InschrijvingenService();
                    data.SessieSprekerServices sService = new data.SessieSprekerServices();



                    CSVDocument doc = new CSVDocument(fileName);
                    List<string> headers = new List<string>()
                {
                    "Seminarienaam",
                    "Sessienaam",
                    "Datum Sessie",
                    "Locatie Sessie",
                    "Status Sessie",                 
                    "Voornaam",
                    "Achternaam",
                    "Oorspronkelijke titel",
                    "Bedrijf",
                    "Attesttype",
                    "Status",
                    "Aanwezigheid"
                };
                    doc.AddRow(headers);
                    foreach (data.Inschrijvingen item in inschrijvingenDataGrid.Items)
                    {
                        ConfocusDBLibrary.Inschrijvingen inschrijving = iService.GetInschrijvingByID(item.ID);
                        List<string> deelnemer = new List<string>();
                        deelnemer.Add(inschrijving.Sessie.SeminarieTitel);
                        deelnemer.Add(inschrijving.Sessie.NaamDatumLocatie);
                        deelnemer.Add(inschrijving.Sessie.Datum.ToString());
                        deelnemer.Add(inschrijving.Sessie.LocatieNaam);
                        deelnemer.Add(inschrijving.Sessie.Status);

                        try
                        {
                            deelnemer.Add(inschrijving.Functies.Contact.Voornaam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(inschrijving.Functies.Contact.Achternaam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(inschrijving.Functies.Oorspronkelijke_Titel);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(inschrijving.Functies.Bedrijf.Naam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }
                        try
                        {
                            deelnemer.Add(inschrijving.Functies.AttestType1 == null ? "" : inschrijving.Functies.AttestType1.Naam);
                        }
                        catch (Exception)
                        {
                            deelnemer.Add("");
                        }

                        deelnemer.Add(inschrijving.Status);
                        if (inschrijving.Aanwezig == true)
                            deelnemer.Add("Aanwezig");
                        else
                            deelnemer.Add("Afwezig");
                        doc.AddRow(deelnemer);
                    }
                    doc.Save();
                    Helper.OpenUrl(fileName);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }
    }
}

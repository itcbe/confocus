﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;

namespace ConfocusWPF
{
    public class ImportContact
    {
        private List<ExcelContact> fouteContacten = new List<ExcelContact>();
        private Gebruiker currentUser;

        public ImportContact(Gebruiker user)
        {
            this.currentUser = user;
        }

        public List<ExcelContact> readExcel(String filename)
        {

            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook workbook;
            Excel.Worksheet worksheet;
            Excel.Range range;
            workbook = excelApp.Workbooks.Open(filename);
            worksheet = (Excel.Worksheet)workbook.Sheets[1];
            List<ExcelContact> mycontacten = new List<ExcelContact>();

            try
            {

                int column = 0, row = 0;

                range = worksheet.UsedRange;
                for (row = 2; row <= range.Rows.Count; row++)
                {
                    try
                    {
                        ExcelContact mycontact = new ExcelContact();

                        mycontact.Niveau1 = (range.Cells[row, 1] as Excel.Range).Value2 == null ? null : (range.Cells[row, 1] as Excel.Range).Value2.ToString();
                        mycontact.Niveau2 = (range.Cells[row, 2] as Excel.Range).Value2 == null ? null : (range.Cells[row, 2] as Excel.Range).Value2.ToString();
                        mycontact.Niveau3 = (range.Cells[row, 3] as Excel.Range).Value2 == null ? null : (range.Cells[row, 3] as Excel.Range).Value2.ToString();
                        mycontact.Oorspronkelijke_titel = (range.Cells[row, 4] as Excel.Range).Value2 == null ? null : (range.Cells[row, 4] as Excel.Range).Value2.ToString();
                        mycontact.Bedrijf = (range.Cells[row, 5] as Excel.Range).Value2 == null ? null : (range.Cells[row, 5] as Excel.Range).Value2.ToString();
                        mycontact.Webpagina = (range.Cells[row, 6] as Excel.Range).Value2 == null ? null : (range.Cells[row, 6] as Excel.Range).Value2.ToString();
                        mycontact.Adres = (range.Cells[row, 7] as Excel.Range).Value2 == null ? null : (range.Cells[row, 7] as Excel.Range).Value2.ToString();
                        mycontact.Postcode = (range.Cells[row, 8] as Excel.Range).Value2 == null ? null : (range.Cells[row, 8] as Excel.Range).Value2.ToString();
                        mycontact.Plaats = (range.Cells[row, 9] as Excel.Range).Value2 == null ? null : (range.Cells[row, 9] as Excel.Range).Value2.ToString();
                        mycontact.Provincie = (range.Cells[row, 10] as Excel.Range).Value2 == null ? null : (range.Cells[row, 10] as Excel.Range).Value2.ToString();
                        mycontact.Land = (range.Cells[row, 11] as Excel.Range).Value2 == null ? null : (range.Cells[row, 11] as Excel.Range).Value2.ToString();
                        mycontact.TelefoonBedrijf = (range.Cells[row, 12] as Excel.Range).Value2 == null ? "0032" : (range.Cells[row, 12] as Excel.Range).Value2.ToString();
                        mycontact.TelefoonContact = (range.Cells[row, 13] as Excel.Range).Value2 == null ? "0032" : (range.Cells[row, 13] as Excel.Range).Value2.ToString();
                        mycontact.Mobiel = (range.Cells[row, 14] as Excel.Range).Value2 == null ? null : (range.Cells[row, 14] as Excel.Range).Value2.ToString();
                        mycontact.Fax = (range.Cells[row, 15] as Excel.Range).Value2 == null ? null : (range.Cells[row, 15] as Excel.Range).Value2.ToString();
                        mycontact.EmailBedrijf = (range.Cells[row, 16] as Excel.Range).Value2 == null ? null : (range.Cells[row, 16] as Excel.Range).Value2.ToString();
                        mycontact.EmailContact = (range.Cells[row, 17] as Excel.Range).Value2 == null ? null : (range.Cells[row, 17] as Excel.Range).Value2.ToString();
                        mycontact.Aanspreking = (range.Cells[row, 18] as Excel.Range).Value2 == null ? null : (range.Cells[row, 18] as Excel.Range).Value2.ToString();
                        mycontact.Achternaam = (range.Cells[row, 19] as Excel.Range).Value2 == null ? null : (range.Cells[row, 19] as Excel.Range).Value2.ToString();
                        mycontact.Voornaam = (range.Cells[row, 20] as Excel.Range).Value2 == null ? null : (range.Cells[row, 20] as Excel.Range).Value2.ToString();
                        mycontact.Taal = (range.Cells[row, 21] as Excel.Range).Value2 == null ? null : (range.Cells[row, 21] as Excel.Range).Value2.ToString();
                        mycontact.Type = (range.Cells[row, 22] as Excel.Range).Value2 == null ? null : (range.Cells[row, 22] as Excel.Range).Value2.ToString();

                        mycontacten.Add(mycontact);
                    }
                    catch (Exception nex)
                    {
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het importeren van de Excel!");
            }
            finally
            {
                workbook.Close(true, Missing.Value, Missing.Value);
                excelApp.Quit();
            }
            return mycontacten;
        }

        public List<ExcelContact> saveExcelToDB(List<ExcelContact> mycontacten)
        {
            String errortext = "";
            try
            {

                TaalServices tService = new TaalServices();
                Niveau1Services n1Service = new Niveau1Services();
                Niveau2Services n2Service = new Niveau2Services();
                Niveau3Services n3Service = new Niveau3Services();
                foreach (ExcelContact c in mycontacten)
                {
                    // Contact aanmaken 
                    Taal taal = tService.GetTaalByNameOrCode(c.Taal);
                    Contact contact = new Contact();
                    if (c.Achternaam != null && c.Voornaam != null)
                    {
                        if (taal != null)
                            contact.Taal_ID = taal.ID;
                        contact.Voornaam = c.Voornaam;
                        contact.Achternaam = c.Achternaam;
                        contact.Type = c.Type;
                        if (c.Aanspreking == "v" || c.Aanspreking == "m")
                        {
                            contact.Aanspreking = c.Aanspreking.ToUpper();
                        }
                        else
                            contact.Aanspreking = c.Aanspreking;
                        contact.Aanspreking = c.Aanspreking;
                        contact.Actief = true;
                        contact.Aangemaakt = DateTime.Now;
                        contact.Aangemaakt_door = currentUser.Naam;

                        Contact savedContact = null;
                        try
                        {
                            savedContact = saveContact(contact);
                        }
                        catch (Exception ex)
                        {
                            fouteContacten.Add(c);
                            errortext += "Contact " + c.Voornaam + " " + c.Achternaam + " heeft foute gegevens.\n";
                            continue;
                        }

                        // Bedrijf aanmaken
                        Bedrijf bedrijf = new Bedrijf();
                        bedrijf.Naam = c.Bedrijf;
                        bedrijf.Adres = c.Adres;
                        bedrijf.Postcode = c.Postcode;
                        bedrijf.Plaats = c.Plaats;
                        bedrijf.Provincie = c.Provincie;
                        bedrijf.Land = c.Land;
                        bedrijf.Telefoon = c.TelefoonBedrijf;
                        bedrijf.Fax = c.Fax;
                        bedrijf.Website = c.Webpagina;
                        bedrijf.Email = c.EmailBedrijf;
                        bedrijf.Actief = true;
                        bedrijf.Aangemaakt = DateTime.Now;
                        bedrijf.Aangemaakt_door = currentUser.Naam;

                        Bedrijf savedBedrijf = null;
                        try
                        {
                            savedBedrijf = saveBedrijf(bedrijf);
                        }
                        catch (Exception ex)
                        {
                            fouteContacten.Add(c);
                            errortext += "Bedrijf " + c.Bedrijf + " heeft foute gegevens.\n";
                            continue;
                        }

                        // Functie aanmaken
                        if (savedContact != null && savedBedrijf != null)
                        {
                            Functies functie = new Functies();
                            Niveau1 n1 = n1Service.GetNiveau1ByName(c.Niveau1);

                            if (n1 != null)
                            {
                                functie.Niveau1_ID = n1.ID;
                            }
                            else
                            {
                                fouteContacten.Add(c);
                                errortext += "Niveau 1: " + c.Niveau1 + " bestaat niet!\n";
                                continue;
                            }

                            if (c.Niveau2 != null)
                            {
                                Niveau2 n2 = n2Service.GetNiveau2ByName(c.Niveau2, n1.ID);
                                if (n2 != null)
                                    functie.Niveau2_ID = n2.ID;

                                if (c.Niveau3 != null && n2 != null)
                                {
                                    Niveau3 n3 = n3Service.GetNiveau3ByName(c.Niveau3, n2.ID);
                                    if (n3 != null)
                                        functie.Niveau3_ID = n3.ID;
                                }
                            }
                            functie.Oorspronkelijke_Titel = c.Oorspronkelijke_titel;
                            functie.Contact_ID = savedContact.ID;
                            functie.Bedrijf_ID = savedBedrijf.ID;
                            functie.Email = c.EmailContact;
                            functie.Telefoon = c.TelefoonContact;
                            functie.Mobiel = c.Mobiel;
                            functie.Actief = true;
                            functie.Startdatum = DateTime.Now;
                            functie.Fax = true;
                            functie.Mail = true;
                            functie.Type = c.Type;
                            functie.Aangemaakt = DateTime.Now;
                            functie.Aangemaakt_door = currentUser.Naam;

                            Functies savedFunctie = null;
                            try
                            {
                                savedFunctie = saveFunctie(functie);
                            }
                            catch (Exception ex)
                            {
                                fouteContacten.Add(c);
                                errortext += "Functie van: " + c.Voornaam + " " + c.Achternaam + " bevat foute gegevens!\n";
                                continue;
                            }
                        }
                        else
                        {

                            fouteContacten.Add(c);
                            errortext += "Kan de functie niet opslaan omdat het contact en/of het bedrijf fouten bevat!\n";
                            continue;
                        }
                    }
                    else
                    {
                        errortext += "Geen voornaam of/en achternaam ingevuld voor contacten\n";
                        fouteContacten.Add(c);
                        continue;
                    }
                }
                if (errortext != "")
                    ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
                return fouteContacten;
            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het opslaan van de contacten!\n" + errortext + "\n" + ex.Message);
            }
        }
        private Contact saveContact(Contact contact)
        {
            ContactServices service = new ContactServices();
            try
            {
                Contact saved = service.SaveContact(contact, true);
                return saved;
            }
            catch (Exception ex)
            {
                if (ex.Message == ErrorMessages.ContactBestaatReeds)
                {
                    Contact bestaand = service.GetContactByName(contact);
                    bestaand = MergeContact(contact, bestaand);
                    service.SaveContactChanges(bestaand);
                    return bestaand;
                }
                throw new Exception(ex.Message);
            }
        }

        private Contact MergeContact(Contact contact, Contact bestaand)
        {
            if (contact.Attesttype != null && contact.Attesttype != "")
                if (bestaand.Attesttype != contact.Attesttype)
                    bestaand.Attesttype = contact.Attesttype;

            if (contact.Erkenning_ID != null)
                if (bestaand.Erkenning_ID != contact.Erkenning_ID)
                    bestaand.Erkenning_ID = contact.Erkenning_ID;

            if (contact.Geboortedatum != null)
                if (bestaand.Geboortedatum != contact.Geboortedatum)
                    bestaand.Geboortedatum = contact.Geboortedatum;

            if (contact.Notities != null && contact.Notities != "")
                if (bestaand.Notities.ToLower() != contact.Notities.ToLower())
                    bestaand.Notities = contact.Notities;

            if (contact.Secundaire_Taal_ID != null)
                if (bestaand.Secundaire_Taal_ID != contact.Secundaire_Taal_ID)
                    bestaand.Secundaire_Taal_ID = contact.Secundaire_Taal_ID;

            if (contact.Taal_ID != null)
                if (bestaand.Taal_ID != contact.Taal_ID)
                    bestaand.Taal_ID = contact.Taal_ID;

            return bestaand;
        }

        private Bedrijf saveBedrijf(Bedrijf bedrijf)
        {
            BedrijfServices service = new BedrijfServices();
            try
            {
                Bedrijf saved = service.SaveBedrijf(bedrijf);
                return saved;
            }
            catch (Exception ex)
            {
                if (ex.Message == ErrorMessages.BedrijfBestaatReeds)
                {
                    Bedrijf bestaand = service.GetBestaandBedrijfByName(bedrijf.Naam);
                    bestaand = MergeBedrijf(bedrijf, bestaand);
                    service.SaveBedrijfChanges(bestaand);
                    return bestaand;
                }
                else
                    throw new Exception(ex.Message);
            }
        }

        private Bedrijf MergeBedrijf(Bedrijf bedrijf, Bedrijf bestaand)
        {
            if (!string.IsNullOrEmpty(bedrijf.Adres))
                if (bestaand.Adres != bedrijf.Adres)
                    bestaand.Adres = bedrijf.Adres;
            if (!string.IsNullOrEmpty(bedrijf.Email))
                if (!string.IsNullOrEmpty(bestaand.Email))
                {
                    if (!bestaand.Email.ToLower().Contains(bedrijf.Email.ToLower()))
                        bestaand.Email += "; " + bedrijf.Email;
                }
                else
                    bestaand.Email = bedrijf.Email;
            if (!string.IsNullOrEmpty(bedrijf.Fax) && bedrijf.Fax != "0032")
                if (bestaand.Fax != bedrijf.Fax)
                    bestaand.Fax = bedrijf.Fax;
            if (!string.IsNullOrEmpty(bedrijf.Land))
            {
                if (!string.IsNullOrEmpty(bestaand.Land))
                {
                    if (bestaand.Land.ToLower() != bedrijf.Land.ToLower())
                        bestaand.Land = bedrijf.Land;
                }
                else
                    bestaand.Land = bedrijf.Land;
            }
            if (!string.IsNullOrEmpty(bedrijf.Notities))
            {
                if (!string.IsNullOrEmpty(bestaand.Notities))
                {
                    if (bestaand.Notities.ToLower() != bedrijf.Notities.ToLower())
                        bestaand.Notities = bedrijf.Notities;
                }
                else
                    bestaand.Notities = bedrijf.Notities;

            }
            if (!string.IsNullOrEmpty(bedrijf.Ondernemersnummer))
                if (bestaand.Ondernemersnummer != bedrijf.Ondernemersnummer)
                    bestaand.Ondernemersnummer = bedrijf.Ondernemersnummer;
            if (!string.IsNullOrEmpty(bedrijf.Telefoon) && bedrijf.Telefoon != "0032")
                if (bestaand.Telefoon != bedrijf.Telefoon )
                    bestaand.Telefoon = bedrijf.Telefoon;
            if (!string.IsNullOrEmpty(bedrijf.Website))
                if (bestaand.Website != bedrijf.Website)
                    bestaand.Website = bedrijf.Website;
            return bestaand;
        }

        private Functies saveFunctie(Functies functie)
        {
            FunctieServices service = new FunctieServices();
            try
            {
                Functies saved = service.SaveFunctie(functie);
                return saved;
            }
            catch (Exception ex)
            {
                if (ex.Message == ErrorMessages.FunctieBestaatReeds)
                {
                    Functies bestaand = service.GetFunctieByContactBedrijfNiveaus(functie);
                    bestaand = MergeFunctie(functie, bestaand);
                    service.SaveFunctieWijzigingen(bestaand);
                    return bestaand;
                }
                else
                    throw new Exception(ex.Message);
            }
        }

        private Functies MergeFunctie(Functies functie, Functies bestaand)
        {
            if (!string.IsNullOrEmpty(functie.Email))
                if (!string.IsNullOrEmpty(bestaand.Email))
                {
                    if (bestaand.Email.ToLower() != functie.Email.ToLower())
                        bestaand.Email = functie.Email;

                }
                else
                {
                    bestaand.Email += "; " + functie.Email;
                }
            if (functie.Erkenning_ID != null)
                if (bestaand.Erkenning_ID != functie.Erkenning_ID)
                    bestaand.Erkenning_ID = functie.Erkenning_ID;
            if (!string.IsNullOrEmpty(functie.Erkenningsnummer))
                if (bestaand.Erkenningsnummer != functie.Erkenningsnummer)
                    bestaand.Erkenningsnummer = functie.Erkenningsnummer;
            if (!string.IsNullOrEmpty(functie.Mobiel) && functie.Mobiel != "0032")
                if (bestaand.Mobiel != functie.Mobiel)
                    bestaand.Mobiel = functie.Mobiel;
            if (!string.IsNullOrEmpty(functie.Oorspronkelijke_Titel))
                if (!string.IsNullOrEmpty(bestaand.Oorspronkelijke_Titel))
                {
                    if (bestaand.Oorspronkelijke_Titel.ToLower() != functie.Oorspronkelijke_Titel.ToLower())
                        bestaand.Oorspronkelijke_Titel = functie.Oorspronkelijke_Titel;
                }
                else
                {
                    bestaand.Oorspronkelijke_Titel = functie.Oorspronkelijke_Titel;
                }
            if (!string.IsNullOrEmpty(functie.Telefoon) && functie.Telefoon != "0032")
                if (bestaand.Telefoon != functie.Telefoon)
                    bestaand.Telefoon = functie.Telefoon;
            //bestaand.Type = functie.Type;
            return bestaand;
        }
    }
}

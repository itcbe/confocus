﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusDBLibrary;

namespace ConfocusWPF
{
    
    /// <summary>
    /// Interaction logic for AansprekingBeheer.xaml
    /// </summary>
    public partial class AansprekingBeheer : Window
    {
        /// <summary>
        /// private members
        /// </summary>
        private CollectionViewSource aansprekingViewSource;
        private Boolean IsScroll = false, IsButtonClick = false;
        private List<Aanspreking> aansprekingen = new List<Aanspreking>();
        private List<Aanspreking> gevondenAansprekingen = new List<Aanspreking>();
        private List<Aanspreking> gewijzigdeAansprekingen = new List<Aanspreking>();
        private Gebruiker currentUser;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">De ingelogde gebruiker om bij opslaan van de gegevens mee te te sturen als gewijzigd/aangemaakt door</param>
        public AansprekingBeheer(Gebruiker user)
        {
            InitializeComponent();
            currentUser = user;
            this.Title = "Aanspreking beheer : Ingelogd als " + currentUser.Naam;

        }

        /// <summary>
        /// Laad alle hulpgegevens en aansprekingen in
        /// </summary>
        private void LoadData()
        {
            AansprekingServices service = new AansprekingServices();
            TaalServices taalservice = new TaalServices();
            List<Taal> talen = taalservice.FindAll();
            aansprekingen = service.FindAll();

            naamComboBox.ItemsSource = talen;
            naamComboBox.DisplayMemberPath = "Naam";
           
            aansprekingViewSource = ((CollectionViewSource)(this.FindResource("aansprekingViewSource")));
            aansprekingViewSource.Source = aansprekingen;
            goUpdate();
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            aansprekingViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            this.Close();
        }

        /// <summary>
        /// Event als het window geladen is laad dan de gegevens in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        /// <summary>
        /// past de status van de navigatie knoppen aan tov de datagrid positie
        /// </summary>
        private void goUpdate()
        {
            if (aansprekingDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (aansprekingViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (aansprekingViewSource.View.CurrentPosition == aansprekingDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                aansprekingDataGrid.SelectedIndex = 0;
            if (aansprekingDataGrid.Items.Count != 0)
                aansprekingDataGrid.ScrollIntoView(aansprekingDataGrid.SelectedItem);
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            aansprekingViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            aansprekingViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            aansprekingViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        /// <summary>
        /// Event geeft de afbeeldingen op de knoppen oranje weer als men over de knoppen gaat met de muis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        /// <summary>
        /// Event zet de afbeeldingen wit weer als men de knop verlaat met de muis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        /// <summary>
        /// Event Zet de taal in het details gedeelte juist.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aansprekingDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (aansprekingDataGrid.SelectedIndex > -1)
            {
                Aanspreking myAan = (Aanspreking)aansprekingDataGrid.SelectedItem;
                naamComboBox.Text = myAan.Taal.Naam;
                if (!IsButtonClick)
                {
                    IsScroll = true;
                    goUpdate();
                    IsScroll = false;
                }
            }
        }

        /// <summary>
        /// Event zoekknop klik. start de zoekactie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        /// <summary>
        /// Voert de zoekactie uit.
        /// </summary>
        private void ZoekInLijst()
        {
            gevondenAansprekingen.Clear();

            gevondenAansprekingen = (from aanspr in aansprekingen
                                     where aanspr.Aanspreking_man.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                     || aanspr.Aanspreking_vrouw.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                     select aanspr).ToList();
            if (gevondenAansprekingen.Count > 0)
            {
                aansprekingViewSource.Source = gevondenAansprekingen;
            }
            else
            {
                aansprekingViewSource.Source = aansprekingen;
            }
            goUpdate();
        }

        /// <summary>
        /// start het nieuwe aanspreking venster
        /// Herlaad de lijst als er een nieuwe aansprekingen aangemaakt is.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweAanspreking NA = new NieuweAanspreking(currentUser);
            if (NA.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();
            
        }

        /// <summary>
        /// functie om de wijzigingen op te slaan in de detailsview.
        /// </summary>
        private void SaveWijzigingen()
        {
            if (gewijzigdeAansprekingen.Count > 0)
            {
                AansprekingServices service = new AansprekingServices();
                foreach (Aanspreking aanspreking in gewijzigdeAansprekingen)
                {
                    service.SaveAansprekingWijzigingen(aanspreking);
                }
                LoadData();
                gewijzigdeAansprekingen.Clear();
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            else
                ConfocusMessageBox.Show("Geen wijzigingen gevonden.", ConfocusMessageBox.Kleuren.Blauw);
        }

        /// <summary>
        /// Event bij het verlaten van een veld worden mogelijke wijzigingen geregistreerd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void control_LostFocus(object sender, RoutedEventArgs e)
        {
            registerChanges();
        }

        private void naamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            registerChanges();
        }

        /// <summary>
        /// Functie om eventuele wijzigeingen te registreren
        /// </summary>
        private void registerChanges()
        {
            if (aansprekingDataGrid.SelectedIndex > -1)
            {
                Aanspreking aanspreking = (Aanspreking)aansprekingDataGrid.SelectedItem;
                if (naamComboBox.SelectedIndex > -1)
                    aanspreking.Taal_ID = ((Taal)naamComboBox.SelectedItem).ID;
                else
                    aanspreking.Taal_ID = null;
                aanspreking.Aanspreking_man = aanspreking_manTextBox.Text;
                aanspreking.Aanspreking_vrouw = aanspreking_vrouwTextBox.Text;
                aanspreking.Gewijzigd = DateTime.Now;
                aanspreking.Gewijzigd_door = currentUser.Naam;

                AansprekingServices service = new AansprekingServices();
                try
                {
                    service.SaveAansprekingWijzigingen(aanspreking);
                    gewijzigdeAansprekingen.Clear();
                    int index = aansprekingDataGrid.SelectedIndex;
                    LoadData();
                    aansprekingDataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }

            }
        }

        /// <summary>
        /// Event voor het sluiten van het venster na controle of er nog onopgeslagen wijzigingen zijn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gewijzigdeAansprekingen.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveWijzigingen();
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
                ZoekInLijst();
        }

        private void GridTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Aanspreking aanspreking = (Aanspreking)aansprekingDataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "manTextBox":
                        aanspreking.Aanspreking_man = text.Text;
                        break;
                    case "vrouwTextBox":
                        aanspreking.Aanspreking_vrouw = text.Text;
                        break;
                }
                try
                {
                    AansprekingServices service = new AansprekingServices();
                    service.SaveAansprekingWijzigingen(aanspreking);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }

        }
    }
}

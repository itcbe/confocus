﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwSeminarieType.xaml
    /// </summary>
    public partial class NieuwSeminarieType : Window
    {
        private Gebruiker CurrentUser;
        public NieuwSeminarieType(Gebruiker user)
        {
            InitializeComponent();
            CurrentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            seminarieTypeTextBox.Focus();
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveSeminarieType();
        }

        private void SaveSeminarieType()
        {
            try
            {
                if (!string.IsNullOrEmpty(seminarieTypeTextBox.Text))
                {
                    Seminarietype type = new Seminarietype();
                    type.Type = seminarieTypeTextBox.Text;
                    type.Actief = true;
                    type.Aangemaakt = DateTime.Now;
                    type.Gewijzigd = DateTime.Now;
                    type.Aangemaakt_door = CurrentUser.Naam;
                    type.Gewijzigd_door = CurrentUser.Naam;
                    new SeminarieTypeService().Save(type);
                    DialogResult = true;
                }
                else
                    ConfocusMessageBox.Show("Geen type ingegeven!!!", ConfocusMessageBox.Kleuren.Rood);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het seminarietype!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }
    }
}

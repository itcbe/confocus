﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwNiveau1.xaml
    /// </summary>
    public partial class NieuwNiveau1 : Window
    {
        private Gebruiker currentUser;

        public NieuwNiveau1(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            naamTextBox.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Niveau1 N1 = GetNiveauFromForm();
            if (N1 != null)
            {
                Niveau1Services service = new Niveau1Services();
                service.SaveNiveau1(N1);
                DialogResult = true;
            }
        }

        private Niveau1 GetNiveauFromForm()
        {
            if (IsFormValid())
            {
                Niveau1 n = new Niveau1();
                n.Naam = naamTextBox.Text;
                n.Naam_FR = beschrijvingTextBox.Text;
                n.Actief = actiefCheckBox.IsChecked == true;
                n.Aangemaakt = DateTime.Now;
                n.Aangemaakt_door = currentUser.Naam;

                return n;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "De naam is niet ingevult!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}

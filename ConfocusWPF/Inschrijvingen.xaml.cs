﻿using ConfocusClassLibrary;
using data = ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ConfocusWPF.Usercontrols;
using System.Windows.Media;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for Inschrijvingen.xaml
    /// </summary>
    public partial class Inschrijvingen : Window
    {
        private data.Gebruiker currentUser;
        private decimal? InschrijvingID = null;
        private InschrijvingUserControl mainInvoerControl;
        private List<data.Functies> _functies;


        public Inschrijvingen(data.Gebruiker user, decimal? inschrijvingID)
        {
            InitializeComponent();

            currentUser = user;
            if (inschrijvingID != null)
                InschrijvingID = (decimal)inschrijvingID;

            mainInvoerControl = new ConfocusWPF.Usercontrols.InschrijvingUserControl(false);
            mainInvoerControl.CurrentUser = currentUser;
            mainInvoerControl.InschrijvingChanged += mainInvoerControl_InschrijvingChanged;

            InvulFormPanel.Children.Add(mainInvoerControl);
        }

        void mainInvoerControl_InschrijvingChanged(object sender, InschrijvingEventArgs e)
        {
            if (e.Inschrijving != null)
            {
                if (e.Inschrijving.Functies != null)
                {
                    TelefoonLabel.Content = e.Inschrijving.Functies.Telefoon;
                    GSMLabel.Content = e.Inschrijving.Functies.Mobiel;
                    EmailLabel.Content = e.Inschrijving.Functies.Email;

                    data.InschrijvingenService iService = new data.InschrijvingenService();
                    List<data.Inschrijvingen> inschrijvingen = iService.GetInschrijvingenOpFunction(e.Inschrijving.Functies.Contact_ID);

                    ProcessInschrijvingen(inschrijvingen, e.Inschrijving);
                }
            }
        }

        private void ProcessInschrijvingen(List<data.Inschrijvingen> inschrijvingen, data.Inschrijvingen myinschrijving)
        {
            //List<StackPanel> ingeschrevenPanels = new List<StackPanel>();
            ResetSessieCheckBoxes();
            foreach (data.Inschrijvingen inschrijving in inschrijvingen)
            {
                data.Inschrijvingen fullInschrijving = new data.InschrijvingenService().GetInschrijvingByID(inschrijving.ID);
                foreach (object obj in seminarieStackPanel.Children)
                {
                    if (obj is StackPanel)
                    {
                        StackPanel myPanel = (StackPanel)obj;
                        CheckBox semBox = null;
                        foreach (object myPanelChild in myPanel.Children)
                        {
                            if (myPanelChild is CheckBox)
                            {
                                semBox = myPanelChild as CheckBox;
                            }
                            else if (myPanelChild is StackPanel)
                            {
                                StackPanel sessieStackpanel = myPanelChild as StackPanel;
                                foreach (object sessieChild in sessieStackpanel.Children)
                                {
                                    if (sessieChild is CheckBox)
                                    {
                                        CheckBox box = (CheckBox)sessieChild;
                                        if (box.Name.Contains("SessieCheckBox"))
                                        {
                                            string[] sessiegegevens = box.Name.Split('_');
                                            decimal sessieId = decimal.Parse(sessiegegevens[1]);
                                            if (fullInschrijving.Sessie_ID == sessieId)
                                            {
                                                box.IsChecked = true;
                                                box.IsEnabled = false;
                                                box.IsChecked = true;
                                                if (semBox != null)
                                                {
                                                    semBox.IsChecked = true;
                                                    SeminarieCheckBox_click(semBox, new RoutedEventArgs());
                                                }
                                                //ingeschrevenPanels.Add(sessieStackpanel);
                                                //Label label = new Label();
                                                //label.Name = box.Name + "Label";
                                                //label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                                //label.Content = "Reeds ingeschreven.";
                                                //label.Margin = new Thickness(70, 5, 5, 5);
                                                //sessieStackpanel.Children.Add(label);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //foreach (StackPanel panel in ingeschrevenPanels)
            //{
            //    Label label = new Label();
            //    //label.Name = box.Name + "Label";
            //    label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            //    label.Content = "Reeds ingeschreven.";
            //    label.Margin = new Thickness(70, 5, 5, 5);
            //    panel.Children.Add(label);
            //}
        }

        private void ResetSessieCheckBoxes()
        {
            foreach (object obj in seminarieStackPanel.Children)
            {
                if (obj is StackPanel)
                {
                    StackPanel myPanel = (StackPanel)obj;
                    foreach (object myPanelChild in myPanel.Children)
                    {
                        if (myPanelChild is StackPanel)
                        {
                            StackPanel sessieStackpanel = myPanelChild as StackPanel;
                            foreach (object sessieChild in sessieStackpanel.Children)
                            {
                                if (sessieChild is CheckBox)
                                {
                                    CheckBox box = (CheckBox)sessieChild;
                                    box.IsEnabled = true;
                                    box.IsChecked = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetSeminaries(null);
        }

        private void SetSeminaries(Decimal? bedrijfid)
        {
            data.SeminarieServices sService = new data.SeminarieServices();
            List<data.Seminarie> seminaries = sService.FindActive();

            foreach (data.Seminarie sem in seminaries)
            {
                if (sem.Sessie.Count > 0)
                {
                    foreach (data.Sessie sessie in sem.Sessie)
                    {
                        //todo terug activeren als de sessies uit het verleden niet meer getoond moeten worden.
                        if (sessie.Datum >= DateTime.Today.AddDays(-21))
                        {
                            if (bedrijfid == null)
                                CreateSeminarieCheckBox(sem, null);
                            else
                                CreateSeminarieCheckBox(sem, bedrijfid);
                            break;
                        }
                    }
                }
            }
        }

        private void CreateSeminarieCheckBox(data.Seminarie sem, Decimal? bedrijfid)
        {
            try
            {

                StackPanel panel = new StackPanel();

                CheckBox box = new CheckBox();
                box.Name = "SeminarieCheckBox_" + sem.ID.ToString();
                box.Content = sem.Titel;
                box.IsChecked = false;
                box.Margin = new Thickness(10, 5, 0, 5);
                box.Click += SeminarieCheckBox_click;

                StackPanel seminarieChildrenStackPanel = new StackPanel();
                seminarieChildrenStackPanel.Name = "SeminarieChildPanel_" + sem.ID.ToString();
                seminarieChildrenStackPanel.Visibility = System.Windows.Visibility.Collapsed;
                bool HasActiveSessions = false;
                if (sem.Sessie != null)
                {
                    var sessies = (from s in sem.Sessie
                                   orderby s.Datum
                                   select s).ToList();

                    foreach (data.Sessie sessie in sessies)
                    {

                        //todo terug activeren als de sessies uit het verleden niet meer getoond moeten worden.
                        if (sessie.Actief == true && sessie.Datum >= DateTime.Today.AddDays(-21) /*&& sessie.Status != "Geannuleerd"*/)
                        {
                            HasActiveSessions = true;
                            CheckBox sesbox = new CheckBox();
                            sesbox.Name = "SessieCheckBox_" + sessie.ID.ToString();
                            sesbox.Content = sessie.NaamDatumLocatie;
                            sesbox.Margin = new Thickness(40, 5, 0, 5);
                            if (sessie.Status == "Geannuleerd")
                                sesbox.Foreground = new SolidColorBrush(Colors.Red);
                            else if (sessie.Status == "Verplaatst")
                                sesbox.Foreground = new SolidColorBrush(Colors.Blue);
                            else if (sessie.Status == "Actief niet online")
                                sesbox.Foreground = new SolidColorBrush(Colors.Gray);
                            else
                                sesbox.Foreground = new SolidColorBrush(Colors.Black);

                            sesbox.Click += CheckBox_click;
                            seminarieChildrenStackPanel.Children.Add(sesbox);
                            StackPanel invoerStackPanel = new StackPanel();
                            invoerStackPanel.Name = sesbox.Name + "Panel";
                            invoerStackPanel.Orientation = Orientation.Vertical;
                            seminarieChildrenStackPanel.Children.Add(invoerStackPanel);
                        }
                    }
                }
                if (HasActiveSessions)
                {
                    panel.Children.Add(box);
                    panel.Children.Add(seminarieChildrenStackPanel);
                    seminarieStackPanel.Children.Add(panel);
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("fout bij het aanmaken van de Checkboxen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SeminarieCheckBox_click(object sender, RoutedEventArgs e)
        {
            CheckBox check = (CheckBox)sender;
            Boolean gevonden = false;
            foreach (object obj in seminarieStackPanel.Children)
            {
                if (obj is Panel)
                {
                    Panel myPanel = (Panel)obj;
                    int index = 0;
                    foreach (Object myobj in myPanel.Children)
                    {
                        if (myobj is CheckBox)
                        {
                            CheckBox mycheck = (CheckBox)myobj;
                            if (mycheck.Name == check.Name)
                            {
                                Panel panel = (Panel)myPanel.Children[index + 1];
                                if (check.IsChecked == true)
                                {
                                    panel.Visibility = System.Windows.Visibility.Visible;
                                }
                                else
                                {
                                    panel.Visibility = System.Windows.Visibility.Collapsed;
                                }
                                gevonden = true;
                                break;
                            }
                        }
                        index++;
                    }
                }
                if (gevonden)
                    break;
            }
        }

        private void CheckBox_click(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (CheckBox)sender;
            String[] namen = checkbox.Name.Split('_');
            String rootname = namen[0];
            Panel panel = checkbox.Parent as Panel;
            if (rootname == "SeminarieCheckBox")
            {
                if (checkbox.IsChecked == true)
                {
                    bool hasSavedItems = false;
                    String panelname = "";
                    foreach (object obj in panel.Children)
                    {
                        if (obj is CheckBox)
                        {
                            CheckBox mycheck = obj as CheckBox;
                            if (mycheck.Name != checkbox.Name)
                            {
                                if (mycheck.IsEnabled == true)
                                {
                                    mycheck.IsChecked = true;
                                    hasSavedItems = false;
                                }
                                else
                                    hasSavedItems = true;

                                panelname = mycheck.Name + "Panel";
                            }
                            else
                                continue;
                        }
                        else if (obj is StackPanel)
                        {
                            if (!hasSavedItems)
                            {
                                StackPanel thePanel = (StackPanel)obj;
                                if (thePanel.Name == panelname)
                                {
                                    //SetDeelnemerControls(thePanel);
                                    hasSavedItems = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool hasSavedItems = false;
                    String panelname = "";
                    foreach (object obj in panel.Children)
                    {

                        if (obj is CheckBox)
                        {

                            CheckBox mycheck = obj as CheckBox;
                            if (mycheck.Name != checkbox.Name)
                            {
                                if (mycheck.IsEnabled == true)
                                {
                                    mycheck.IsChecked = false;
                                    hasSavedItems = false;


                                }
                                else
                                    hasSavedItems = true;

                                panelname = mycheck.Name + "Panel";
                            }
                            else
                                continue;
                        }
                        else if (obj is StackPanel)
                        {
                            if (!hasSavedItems)
                            {
                                StackPanel mypanel = obj as StackPanel;
                                if (mypanel.Name == panelname)
                                {
                                    mypanel.Children.Clear();
                                    hasSavedItems = false;
                                }
                            }
                        }
                    }
                }
            }
            else if (rootname == "SessieCheckBox")
            {
                if (checkbox.IsChecked == false)
                {
                    // controle als er nog boxen gechecked zijn.
                    bool nogchecked = false;
                    foreach (object item in panel.Children)
                    {
                        if (item is CheckBox)
                        {
                            CheckBox myCheck = item as CheckBox;
                            String[] checkNamen = myCheck.Name.Split('_');
                            String naam = checkNamen[0];
                            if (naam == "SessieCheckBox")
                            {
                                nogchecked = true;
                                break;
                            }
                        }
                    }


                    String panelName = checkbox.Name + "Panel";
                    foreach (object obj in panel.Children)
                    {
                        if (obj is StackPanel)
                        {
                            StackPanel thepanel = (StackPanel)obj;
                            if (thepanel.Name == panelName)
                            {
                                thepanel.Children.Clear();
                                break;
                            }
                        }
                    }

                    if (!nogchecked)
                    {
                        foreach (object obj in panel.Children)
                        {
                            if (obj is CheckBox)
                            {
                                CheckBox myCheck = obj as CheckBox;
                                String[] checkNamen = myCheck.Name.Split('_');
                                String naam = checkNamen[0];
                                if (naam == "SeminarieCheckBox")
                                {
                                    myCheck.IsChecked = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (checkbox.IsChecked == true)
                {
                    foreach (object obj in panel.Children)
                    {
                        if (obj is CheckBox)
                        {
                            CheckBox myCheck = obj as CheckBox;
                            String[] checkNamen = myCheck.Name.Split('_');
                            String naam = checkNamen[0];
                            if (naam == "SeminarieCheckBox")
                            {
                                myCheck.IsChecked = true;
                                break;
                            }
                        }
                    }
                    String panelName = checkbox.Name + "Panel";
                    foreach (var item in panel.Children)
                    {
                        if (item is StackPanel)
                        {
                            StackPanel thePanel = (StackPanel)item;
                            if (thePanel.Name == panelName)
                            {
                                string invoerControlNaam = checkbox.Name + "Invoer";
                                var invoer = new ConfocusWPF.Usercontrols.InschrijvingUserControl(true);
                                invoer.Name = invoerControlNaam;
                                invoer.Margin = new Thickness(70, 5, 5, 5);
                                invoer.Width = 700;
                                invoer.Functies = _functies;
                                invoer.StatusComboBox.SelectedIndex = -1;
                                invoer.dagtypeComboBox.SelectedIndex = -1;
                                thePanel.Children.Add(invoer);
                            }
                        }
                    }
                }
            }
        }


        private void CloseButton_click(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true;
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        private void OpslaanButton_Click(object sender, RoutedEventArgs e)
        {
            if (FormIsValid())
            {
                try
                {
                    SaveInschrijvingen();
                    ClearForm();
                }
                catch (Exception ex)
                {
                    if (ex.Message == "GeenSessieGevonden")
                        ConfocusMessageBox.Show("Er zijn geen sessies gekozen om op in te schrijven.", ConfocusMessageBox.Kleuren.Rood);
                    else
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void ClearForm()
        {
            AchternaamLabel.Content = "";
            VoornaamLabel.Content = "";
            TelefoonLabel.Content = "";
            GSMLabel.Content = "";
            EmailLabel.Content = "";
            mainInvoerControl.ClearForm();
            foreach (object obj in seminarieStackPanel.Children)
            {
                if (obj is StackPanel)
                {
                    StackPanel myPanel = (StackPanel)obj;
                    foreach (object myPanelChild in myPanel.Children)
                    {
                        if (myPanelChild is StackPanel)
                        {
                            StackPanel sessieStackpanel = myPanelChild as StackPanel;
                            foreach (object sessieChild in sessieStackpanel.Children)
                            {
                                if (sessieChild is CheckBox)
                                {
                                    CheckBox box = (CheckBox)sessieChild;
                                    if (box.Name.Contains("SessieCheckBox"))
                                    {
                                        if (box.IsChecked == true)
                                        {
                                            box.IsChecked = false;
                                            CheckBox_click(box, new RoutedEventArgs());
                                            box.IsEnabled = true;

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool FormIsValid()
        {
            bool valid = true;
            String errorMessage = "De inschrijvingen kunnen niet opgeslagen worden:\n\n";
            if (mainInvoerControl.BedrijfComboBox.SelectedIndex < 0)
            {
                valid = false;
                errorMessage += "\t*Er is geen bedrijf gekozen\n";
            }

            if (mainInvoerControl.TitelComboBox.SelectedIndex < 0)
            {
                valid = false;
                errorMessage += "\t*Er is geen functie gekozen\n";
            }

            if (mainInvoerControl.StatusComboBox.SelectedIndex < 0)
            {
                valid = false;
                errorMessage += "\t*Er is geen status gekozen\n";
            }

            if (mainInvoerControl.dagtypeComboBox.SelectedIndex < 0)
            {
                valid = false;
                errorMessage += "\t*Er id geen dagtype gekozen\n";
            }

            if (!valid)
            {
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);
            }

            return valid;
        }

        private void SaveInschrijvingen()
        {
            bool sessiegevonden = false;
            data.InschrijvingenService iService = new data.InschrijvingenService();
            SetKlantFromProspectToKlant();


            foreach (object obj in seminarieStackPanel.Children)
            {
                if (obj is StackPanel)
                {
                    StackPanel myPanel = (StackPanel)obj;
                    foreach (object myPanelChild in myPanel.Children)
                    {
                        if (myPanelChild is StackPanel)
                        {
                            StackPanel sessieStackpanel = myPanelChild as StackPanel;
                            foreach (object sessieChild in sessieStackpanel.Children)
                            {
                                if (sessieChild is CheckBox)
                                {
                                    CheckBox box = (CheckBox)sessieChild;
                                    if (box.Name.Contains("SessieCheckBox"))
                                    {
                                        if (box.IsChecked == true && box.IsEnabled == true)
                                        {
                                            sessiegevonden = true;
                                            String[] sessiegegevens = box.Name.Split('_');
                                            decimal sessieId = Decimal.Parse(sessiegegevens[1]);
                                            data.Inschrijvingen inschrijving = GetMainInschrijving();
                                            inschrijving.Sessie_ID = sessieId;
                                            //if (inschrijving.DecimalAantal == null)
                                            //{
                                            //    decimal? aantal =  GetAantalBySessieErkenning(sessieId, inschrijving.Functie_ID);
                                            //    inschrijving.DecimalAantal = aantal;
                                            //    inschrijving.TAantal = aantal == null ? "" : aantal.ToString();
                                            //}

                                            string invoerControlName = box.Name + "Invoer";
                                            inschrijving.TAantal = "";
                                            foreach (object child in sessieStackpanel.Children)
                                            {
                                                if (child is StackPanel)
                                                {
                                                    StackPanel panel = child as StackPanel;
                                                    string panelName = box.Name + "Panel";
                                                    if (panel.Name == panelName)
                                                    {
                                                        foreach (var panelchild in panel.Children)
                                                        {
                                                            if (panelchild is InschrijvingUserControl)
                                                            {
                                                                var invoer = panelchild as InschrijvingUserControl;
                                                                if (invoer.Name == invoerControlName)
                                                                {
                                                                    inschrijving = GetExtraInvoerParameters(inschrijving, invoer);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            data.Inschrijvingen savedInschrijving = iService.SaveInschrijving(inschrijving);
                                            if (savedInschrijving != null)
                                            {
                                                this.InschrijvingID = savedInschrijving.ID;
                                                box.IsEnabled = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!sessiegevonden)
                throw new Exception("GeenSessieGevonden");
        }

        private decimal? GetAantalBySessieErkenning(decimal sessieId, decimal? functieId)
        {
            data.Seminarie_ErkenningenService sService = new data.Seminarie_ErkenningenService();
            List<data.Seminarie_Erkenningen> erkenningen = sService.GetBySessieID(sessieId);
            if (functieId != null)
            {
                data.Functies functie = new ConfocusDBLibrary.FunctieServices().GetFunctieByID((decimal)functieId);
                decimal? aantal = null;
                foreach (data.Seminarie_Erkenningen erk in erkenningen)
                {

                    if (erk != null)
                    {
                        if (erk.Erkenning.AttestType == functie.Attesttype_ID)
                            if (erk.Aantal != null)
                                aantal = Convert.ToDecimal(erk.Aantal);
                    }

                }
                return aantal;
            }
            else
                return null;
        }

        private void SetKlantFromProspectToKlant()
        {
            try
            {
                data.Functies functie = (data.Functies)mainInvoerControl.TitelComboBox.SelectedItem;
                data.ContactServices cService = new data.ContactServices();
                cService.SetContactTypeToKlant(functie.Contact_ID);
                data.FunctieServices fService = new data.FunctieServices();
                functie.Type = "Klant";
                functie.Gewijzigd = DateTime.Now;
                functie.Gewijzigd_door = currentUser.Naam;
                fService.SaveFunctieWijzigingen(functie);
                CheckIfContactIsCustomer(functie);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void CheckIfContactIsCustomer(data.Functies functie)
        {
            try
            {
                if (functie != null)
                {
                    if (functie.Type == "Klant")
                    {
                        data.FunctieServices fService = new data.FunctieServices();
                        List<data.Functies> functies = fService.GetFunctieByContact(functie.Contact_ID);
                        foreach (data.Functies func in functies)
                        {
                            if (func.Type != "Klant")
                            {
                                func.Type = "Klant";
                                func.Gewijzigd = DateTime.Today;
                                func.Gewijzigd_door = currentUser.Naam;
                                fService.SaveFunctieWijzigingen(func);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het naar Klant zetten van de functies voor dit contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }


        private data.Inschrijvingen GetExtraInvoerParameters(data.Inschrijvingen inschrijving, InschrijvingUserControl invoer)
        {
            if (invoer.TitelComboBox.SelectedIndex > -1)
            {
                inschrijving.Functie_ID = ((data.Functies)invoer.TitelComboBox.SelectedItem).ID;
            }
            if (invoer.ViaComboBox.SelectedIndex > -1)
            {
                inschrijving.Via = invoer.ViaComboBox.Text;
            }
            if (invoer.StatusComboBox.SelectedIndex > -1)
            {
                inschrijving.Status = invoer.StatusComboBox.Text;
            }
            if (invoer.CCEmailTextBox.Text != "")
            {
                inschrijving.CC_Email = invoer.CCEmailTextBox.Text;
            }
            if (invoer.NotitieComboBox.Text != "")
            {
                inschrijving.Notitie = invoer.NotitieComboBox.Text;
            }
            if (invoer.dagtypeComboBox.SelectedIndex > -1)
            {
                inschrijving.Dagtype = invoer.dagtypeComboBox.Text;
            }
            if (invoer.AantalTextBox.Text != "")
            {
                decimal aantal;
                string straantal = invoer.AantalTextBox.Text.Replace('.', ',');
                if (decimal.TryParse(straantal, out aantal))
                {
                    inschrijving.DecimalAantal = aantal;
                    straantal = straantal.Replace(',', '.');
                    invoer.AantalTextBox.Text = straantal;
                }
            }
            if (invoer.FacturatieOp != null)
            {
                inschrijving.Facturatie_op_ID = invoer.FacturatieOp.ID;
            }


            return inschrijving;
        }

        private data.Inschrijvingen GetMainInschrijving()
        {
            data.Inschrijvingen inschrijving = new data.Inschrijvingen();

            inschrijving.Datum_inschrijving = DateTime.Now;
            inschrijving.Status = mainInvoerControl.StatusComboBox.Text;
            inschrijving.Via = mainInvoerControl.ViaComboBox.Text;
            inschrijving.CC_Email = mainInvoerControl.CCEmailTextBox.Text;
            inschrijving.Notitie = mainInvoerControl.NotitieComboBox.Text;
            inschrijving.Dagtype = mainInvoerControl.dagtypeComboBox.Text;
            inschrijving.Aanwezig = false;
            decimal aantal;
            string straantal = mainInvoerControl.AantalTextBox.Text.Replace('.', ',');
            if (decimal.TryParse(straantal, out aantal))
            {
                inschrijving.DecimalAantal = aantal;
                straantal = straantal.Replace(',', '.');
                mainInvoerControl.AantalTextBox.Text = straantal;
            }

            if (mainInvoerControl.FacturatieOp != null)
            {
                inschrijving.Facturatie_op_ID = mainInvoerControl.FacturatieOp.ID;
            }

            if (mainInvoerControl.TitelComboBox.SelectedIndex > -1)
            {
                inschrijving.Functie_ID = ((data.Functies)mainInvoerControl.TitelComboBox.SelectedItem).ID;
            }

            //inschrijving.Attesttype = mainInvoerControl.AttesttypeComboBox.Text;

            inschrijving.Gefactureerd = false;
            inschrijving.Actief = true;
            inschrijving.Aangemaakt = DateTime.Now;
            inschrijving.Aangemaakt_door = currentUser.Naam;

            return inschrijving;
        }


        private void NaamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            string naam = box.Text;
            LoadVoornamen(naam);
        }

        private void NaamComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.LineFeed)
            {
                ComboBox box = (ComboBox)sender;
                string naam = box.Text;
                LoadVoornamen(naam);
            }
        }

        private void LoadVoornamen(string naam)
        {
            //if (naam != "")
            //{
            //    data.ContactServices cService = new data.ContactServices();
            //    List<string> voornamen = cService.GetDictinctVoornaamByAchternaam(naam);
            //    VoornaamComboBox.ItemsSource = voornamen;
            //}
        }

        private void VoornaamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            //ComboBox box = (ComboBox)sender;
            //SetControls(box);

        }



        private void SetControls(data.Contact contact)
        {
            if (contact != null)
            {
                data.FunctieServices fService = new data.FunctieServices();
                _functies = fService.GetFunctieByContact(contact.ID);
                if (_functies.Count > 0)
                {
                    AchternaamLabel.Content = _functies[0].Contact.Achternaam;
                    VoornaamLabel.Content = _functies[0].Contact.Voornaam;
                    TelefoonLabel.Content = _functies[0].Telefoon;
                    GSMLabel.Content = _functies[0].Mobiel;
                    EmailLabel.Content = _functies[0].Email;
                    mainInvoerControl.Functies = _functies;
                }
            }
        }

        private void SetControls(data.Functies functie)
        {
            if (functie != null)
            {
                data.FunctieServices fService = new data.FunctieServices();
                _functies = new List<data.Functies>();
                _functies.Add(functie);
                if (_functies.Count > 0)
                {
                    AchternaamLabel.Content = _functies[0].Contact.Achternaam;
                    VoornaamLabel.Content = _functies[0].Contact.Voornaam;
                    TelefoonLabel.Content = _functies[0].Telefoon;
                    GSMLabel.Content = _functies[0].Mobiel;
                    EmailLabel.Content = _functies[0].Email;
                    mainInvoerControl.Functies = _functies;

                }
            }
        }

        private void VoornaamComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Enter || e.Key == Key.LineFeed)
            //{
            //    ComboBox box = (ComboBox)sender;
            //    SetControls(box);
            //}
        }

        private void zoekButton_Click(object sender, RoutedEventArgs e)
        {
            //ZoekContactWindow ZC = new ZoekContactWindow("Contact");
            //if (ZC.ShowDialog() == true)
            //{
            //    ClearForm();
            //    data.Contact contact = ZC.contact;
            //    SetControls(contact);
            //}

            ZoekFunctieWindow ZF = new ZoekFunctieWindow();
            if (ZF.ShowDialog() == true)
            {
                ClearForm();
                data.Functies functie = ZF.Functie;
                if (functie.AttestType1 == null)
                    ConfocusMessageBox.Show("Dit contact heeft geen attesttype! Gelieve dit aan te vullen.", ConfocusMessageBox.Kleuren.Rood);
                SetControls(functie);
            }
        }
    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweErkenning.xaml
    /// </summary>
    public partial class NieuweErkenning : Window
    {
        private Gebruiker currentUser;

        public NieuweErkenning(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Attesttype_Services aService = new Attesttype_Services();
            List<AttestType> attesten = aService.FindActive();
            attestComboBox.ItemsSource = attesten;
            attestComboBox.DisplayMemberPath = "Naam";
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Erkenning erkenning = GetErkenningFromForm();
            if (erkenning != null)
            {
                ErkenningServices service = new ErkenningServices();
                service.SaveErkenning(erkenning);
                DialogResult = true;
            }
        }

        private Erkenning GetErkenningFromForm()
        {
            if (IsFormValid())
            {
                Erkenning er = new Erkenning();
                er.Naam = naamTextBox.Text;
                er.Contactpersoon = contactpersoonTextBox.Text;
                er.Instantie = instantieTextBox.Text;
                er.Telefoonnummer = telefoonnummerTextBox.Text;
                er.Email = emailTextBox.Text;
                er.Website = websiteTextBox.Text;
                er.Attest = attestComboBox.Text;
                er.Erkend = erkendCheckBox.IsChecked == true;
                er.Notitie = notitieTextBox.Text;
                er.Actief = actiefCheckBox.IsChecked == true;
                er.Aangemaakt = DateTime.Now;
                er.Aangemaakt_door = currentUser.Naam;

                return er;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "Geen naam ingevuld!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void EmailTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (!String.IsNullOrEmpty(box.Text))
                if (!Helper.CheckEmail(box.Text))
                    ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }

    }
}

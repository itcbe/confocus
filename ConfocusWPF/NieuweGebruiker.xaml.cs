﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweGebruiker.xaml
    /// </summary>
    public partial class NieuweGebruiker : Window
    {
        private Gebruiker currentUser;

        public NieuweGebruiker(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Gebruiker G = GetGebruikerFromForm();
            if (G != null)
            {
                G.ColumnHeaders = Helper.CreateConfigXml(G.Naam);
                GebruikerService service = new GebruikerService();
                service.SaveGebruiker(G);
                DialogResult = true;
            }
        }

        private Gebruiker GetGebruikerFromForm()
        {
            if (IsFormValid())
            {
                Encriptor encript = new Encriptor();
                Gebruiker G = new Gebruiker();

                G.Naam = naamTextBox.Text;
                G.Email = emailTextBox.Text;
                G.Paswoord = encript.EncodePassword(paswoordTextBox.Password);
                G.Actief = actiefCheckBox.IsChecked == true;
                G.Aangemaakt = DateTime.Now;
                G.Aangemaakt_door = currentUser.Naam;

                return G;
            }
            else
            {
                return null;
            }
            
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            ControlTemplate passErrorTemplate = (ControlTemplate)(this.FindResource("PasswordBoxErrorControlTemplate"));
            ControlTemplate passNormalTemplate = (ControlTemplate)(this).FindResource("PasswordBoxControlTemplate");
            if (naamTextBox.Text == "")
            {
                errorMessage += "Er is geen naam ingevuld!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);
            if (emailTextBox.Text == "")
            {
                errorMessage += "Er is geen e-mail adres ingegeven!\n";
                Helper.SetTextBoxInError(emailTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(emailTextBox, normalTemplate);
            if (paswoordTextBox.Password == "")
            {
                errorMessage += "Er is geen paswoord ingegeven!\n";
                Helper.SetPassTextBoxInError(paswoordTextBox, passErrorTemplate);
                valid = false;
            }
            else
                Helper.SetPassTextBoxToNormal(paswoordTextBox, passNormalTemplate);
            if (herhaalpaswoordTextBox.Password == "")
            {
                errorMessage += "Er is geen herhaalpaswoord ingegeven!\n";
                Helper.SetPassTextBoxInError(herhaalpaswoordTextBox, passErrorTemplate);
                valid = false;
            }
            else
                Helper.SetPassTextBoxToNormal(herhaalpaswoordTextBox, passNormalTemplate);
            if (paswoordTextBox.Password != herhaalpaswoordTextBox.Password)
            {
                errorMessage += "De paswoorden komen niet overeen!\n";
                Helper.SetPassTextBoxInError(herhaalpaswoordTextBox, passErrorTemplate);
                valid = false;
            }
            else
                Helper.SetPassTextBoxToNormal(herhaalpaswoordTextBox, passNormalTemplate);

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void EmailTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (!String.IsNullOrEmpty(box.Text))
                if (!Helper.CheckEmail(box.Text))
                    ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }

    }
}

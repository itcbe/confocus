﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for LocatieBeheer.xaml
    /// </summary>
    public partial class LocatieBeheer : Window
    {
        private CollectionViewSource locatieViewSource;
        private List<Locatie> locaties = new List<Locatie>();
        private List<Locatie> gevondenLocaties = new List<Locatie>();
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private Gebruiker currentUser;

        public LocatieBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Locatie beheer : Ingelogd als " + currentUser.Naam;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            locatieViewSource = ((CollectionViewSource)(this.FindResource("locatieViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            datagridChange = true;

            PostcodeService postservice = new PostcodeService();
            List<Postcodes> postcodes = postservice.FindAllPostcodes();
            postcodeComboBox.ItemsSource = postcodes;
            postcodeComboBox.DisplayMemberPath = "Postcode";

            // Regios laden
            RegioService rservice = new RegioService();
            List<Regio> regios = rservice.GetActief();
            regioComboBox.ItemsSource = regios;
            regioComboBox.DisplayMemberPath = "Regio1";

            LocatieServices service = new LocatieServices();
            locaties = service.FindActive();
            locatieViewSource.Source = locaties;



            GoUpdate();
            datagridChange = false;
        }

        private void GoUpdate()
        {
            if (locatieDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (locatieViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (locatieViewSource.View.CurrentPosition == locatieDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                locatieDataGrid.SelectedIndex = 0;
            if (locatieDataGrid.Items.Count != 0)
                if (locatieDataGrid.SelectedItem != null)
                    locatieDataGrid.ScrollIntoView(locatieDataGrid.SelectedItem);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            locatieViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            locatieViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            locatieViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            locatieViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweLocatie NL = new NieuweLocatie(currentUser);
            if (NL.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekLocaties();
            
        }

        private void ZoekLocaties()
        {
            if (ZoekTextBox.Text != "")
            {
                gevondenLocaties.Clear();
                gevondenLocaties = (from l in locaties
                                    where l.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                    select l).ToList();

                locatieViewSource.Source = gevondenLocaties;
            }
            else
                locatieViewSource.Source = locaties;
        }

        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (postcodeComboBox.SelectedIndex > -1)
            {
                ComboBox box = (ComboBox)sender;
                Postcodes postcode = (Postcodes)box.SelectedItem;
                PostcodeService service = new PostcodeService();
                List<Postcodes> postcodes = service.GetGemeentesByPostcode(postcode.Postcode);
                plaatsComboBox.ItemsSource = postcodes;
                plaatsComboBox.DisplayMemberPath = "Gemeente";
                if (postcodes.Count > 0)
                    plaatsComboBox.SelectedIndex = 0;
            }
        }

        private void postcodeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (postcodeComboBox.SelectedIndex > -1)
            {
                ComboBox box = (ComboBox)sender;
                Postcodes postcode = (Postcodes)box.SelectedItem;
                PostcodeService service = new PostcodeService();
                List<Postcodes> postcodes = service.GetGemeentesByPostcode(postcode.Postcode);
                plaatsComboBox.ItemsSource = postcodes;
                plaatsComboBox.DisplayMemberPath = "Gemeente";
                if (postcodes.Count > 0)
                    plaatsComboBox.SelectedIndex = 0;
            }
            if (!datagridChange)
                SaveLocatie();
        }

        private void SaveLocatie()
        {
            Locatie locatie = GetLocatieFromForm();
            if (locatie != null)
            {
                LocatieServices service = new LocatieServices();
                try
                {
                    service.SaveLocatieChanges(locatie);
                    int index = locatieDataGrid.SelectedIndex;
                    LoadData();
                    if (!string.IsNullOrEmpty(ZoekTextBox.Text))
                        ZoekLocaties();
                    locatieDataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private Locatie GetLocatieFromForm()
        {
            if (IsLocatieValid())
            {
                Locatie locatie = (Locatie)locatieDataGrid.SelectedItem;
                locatie.Naam = naamTextBox.Text;
                locatie.Adres = adresTextBox.Text;
                locatie.Postcode = postcodeComboBox.SelectedIndex > -1 ? ((Postcodes)postcodeComboBox.SelectedItem).Postcode : ""; 
                locatie.Plaats = plaatsComboBox.SelectedIndex > -1 ? ((Postcodes)plaatsComboBox.SelectedItem).Gemeente : "";
                locatie.Regio = regioComboBox.Text;
                locatie.Contact = contactTextBox.Text;
                locatie.Telefoon = telefoonTextBox.Text;
                locatie.Beamer = beamerCheckBox.IsChecked == true;
                locatie.Laptop = LaptopCheckBox.IsChecked == true;
                locatie.Actief = actiefCheckBox.IsChecked == true;
                locatie.Gewijzigd_door = currentUser.Naam;
                locatie.Gewijzigd = DateTime.Now;

                return locatie;
            }
            else
                return null;
        }

        private bool IsLocatieValid()
        {
            bool valid = true;
            String errortext = "";
            if (naamTextBox.Text == "")
            {
                errortext += "Er is geen naam ingegeven! Dit is een verplicht veld.\n";
                valid = false;
            }
            if (postcodeComboBox.SelectedIndex == -1)
            {
                errortext += "Er is geen postcode gekozen!\n";
                valid = false;
            }
            if (plaatsComboBox.SelectedIndex == -1)
            {
                errortext += "Er is geen plaats gekozen!\n";
                valid = false;
            }
            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }

        private void locatieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (locatieDataGrid.SelectedIndex > -1)
            {
                datagridChange = true;
                Locatie locatie = (Locatie)locatieDataGrid.SelectedItem;
                postcodeComboBox.Text = locatie.Postcode;
                plaatsComboBox.Text = locatie.Plaats;
                if (!string.IsNullOrEmpty(locatie.Regio))
                    regioComboBox.Text = locatie.Regio;
                else
                    regioComboBox.SelectedIndex = -1;
                datagridChange = false;
            }
            if (!IsButtonClick)
            {
                IsScroll = true;
                GoUpdate();
                IsScroll = false;
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            SaveLocatie();
        }

        private void plaatsComboBox_DropDownClosed(object sender, EventArgs e)
        {
            SaveLocatie();
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            SaveLocatie();
        }



        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Locatie locatie = (Locatie)locatieDataGrid.SelectedItem;
                switch (text.Name)
                {
                    case "gridNaamTextBox":
                        locatie.Naam = text.Text;
                        break;
                    case "gridAdresTextBox":
                        locatie.Adres = text.Text;
                        break;
                    case "gridPostcodeTextBox":
                        locatie.Postcode = text.Text;
                        break;
                    case "gridPlaatsTextbox":
                        locatie.Plaats = text.Text;
                        break;
                    
                }
                SaveGridWijzigingen(locatie);
            }
        }

        private void beamerCheckBox_Click(object sender, RoutedEventArgs e)
        {
            SaveLocatie();
        }

        private void laptopCheckBox_Click(object sender, RoutedEventArgs e)
        {
            SaveLocatie();
        }

        private void gridLaptopCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            Locatie locatie = (Locatie)locatieDataGrid.SelectedItem;
            locatie.Laptop = box.IsChecked == true;
            SaveGridWijzigingen(locatie);
        }

        private void regioComboBox_DropDownClosed(object sender, EventArgs e)
        {
            SaveLocatie();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveLocatie();
        }

        private void SaveGridWijzigingen(Locatie locatie)
        {
            SaveLocatie();
        }

        private void gridBeamerCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            Locatie locatie = (Locatie)locatieDataGrid.SelectedItem;
            locatie.Beamer = box.IsChecked == true;
            SaveGridWijzigingen(locatie);
        }

    }
}

﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwContact.xaml
    /// </summary>
    ///
    public partial class NieuwContact : Window
    {
        private CollectionViewSource sessieViewSource;
        private CollectionViewSource inschrijvingenViewSource;
        private CollectionViewSource sessie_SprekerViewSource;
        private List<Functies> functielijst = new List<Functies>();
        private List<Functies> savedFuncties = new List<Functies>();
        private Gebruiker currentUser;
        private List<Postcodes> postcodes = new List<Postcodes>();
        private Contact thisContact = new Contact();
        private Bedrijf thisBedrijf = new Bedrijf();
        private bool IsChanged = false, IsInit = true;
        private int _TabIndex = 0;
        private Contact currentContact;
        private Functies currentFunctie;
        private Spreker currentSpreker;
        private Bedrijf currentBedrijf;
        private Functies _selectedFunctie;
        private decimal oldCompanyID = 0;
        private bool IsContact = false;

        public Functies SelectedFunctie
        {
            get { return _selectedFunctie; }
            //set { _selectedFunctie = value; }
        }

        public NieuwContact(Gebruiker user, string soort)
        {
            try
            {
                IsChanged = false;
                IsInit = true;
                InitializeComponent();
                currentUser = user;
                Title = "Nieuw contact: Ingelogd als " + currentUser.Naam;
                currentContact = new Contact();
                currentContact.ID = 0;
                currentBedrijf = new Bedrijf();
                currentBedrijf.ID = 0;
                if (soort == "Functie")
                {
                    IsContact = true;
                    currentFunctie = new Functies();
                    currentFunctie.ID = 0;
                    currentFunctie.Fax = true;
                    currentFunctie.Mail = true;
                    currentFunctie.Startdatum = DateTime.Today;
                    currentFunctie.Type = "Prospect";
                }
                else
                {
                    IsContact = false;
                    currentSpreker = new Spreker();
                    currentSpreker.ID = 0;
                    currentSpreker.Startdatum = DateTime.Today;
                }
                BedrijfOpslaanButton.IsEnabled = false;
                ContactopslaanButton.IsEnabled = false;
                IsChanged = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NieuwContact(Gebruiker user, Functies functie)
        {
            if (functie != null)
            {
                IsContact = true;
                IsChanged = false;
                IsInit = true;
                InitializeComponent();
                this.currentUser = user;
                this.Title = "Bewerk contact: Ingelogd als " + currentUser.Naam;
                BedrijfOpslaanButton.IsEnabled = false;
                ContactopslaanButton.IsEnabled = false;
                FunctieServices fService = new FunctieServices();
                currentFunctie = fService.GetFunctieByID(functie.ID);
                _selectedFunctie = currentFunctie;
                currentBedrijf = currentFunctie.Bedrijf;
                currentContact = currentFunctie.Contact;
                IsChanged = false;
                SetButtons();
                newButton.Visibility = System.Windows.Visibility.Visible;
            }
        }

        public NieuwContact(Gebruiker user, Contact contact, Bedrijf bedrijf, Functies functie)
        {
            if (functie != null && contact != null && bedrijf != null)
            {
                IsContact = true;
                IsChanged = false;
                IsInit = true;
                InitializeComponent();
                startdatumDatePicker.SelectedDate = functie.Startdatum;
                this.currentUser = user;
                this.Title = "Bewerk contact: Ingelogd als " + currentUser.Naam;
                BedrijfOpslaanButton.IsEnabled = false;
                ContactopslaanButton.IsEnabled = false;
                //FunctieServices fService = new FunctieServices();
                currentFunctie = functie;
                _selectedFunctie = currentFunctie;
                //BedrijfServices bService = new BedrijfServices();
                currentBedrijf = bedrijf;
                //ContactServices cService = new ContactServices();
                currentContact = contact;

                IsChanged = false;
                SetButtons();
                newButton.Visibility = System.Windows.Visibility.Visible;
            }

        }

        private void BindFunctieToForm()
        {
            oorspronkelijkeTextBox.Text = currentFunctie.Oorspronkelijke_Titel;
            emailTextBox1.Text = currentFunctie.Email;
        }

        private void BindContactToForm()
        {
            try
            {
                if (currentContact != null)
                {
                    IDText.Content = currentContact.ID;
                    ContactServices cservice = new ContactServices();
                    Contact fullcontact = cservice.GetContactByID(currentContact.ID);
                    if (fullcontact != null)
                    {
                        currentContact = fullcontact;
                        voornaamTextBox.Text = currentContact.Voornaam;
                        achternaamTextBox.Text = currentContact.Achternaam;
                        aansprekingComboBox.Text = currentContact.Aanspreking;
                        notitiesTextBox.Text = currentContact.Notities;
                        if (currentContact.Taal != null)
                            taalComboBox.Text = currentContact.Taal.Naam;
                        if (currentContact.Secundaire_Taal_ID != null)
                        {
                            try
                            {
                                TaalServices tService = new TaalServices();
                                Taal secundairetaal = tService.GetTaalByID((decimal)currentContact.Secundaire_Taal_ID);
                                if (secundairetaal != null)
                                    secundaireTaalComboBox.Text = secundairetaal.Naam;
                                else
                                    secundaireTaalComboBox.SelectedIndex = -1;
                            }
                            catch (Exception ex)
                            {
                                secundaireTaalComboBox.SelectedIndex = -1;
                            }
                        }
                        if (currentContact.Geboortedatum != null)
                            geboortedatumDatePicker.SelectedDate = (DateTime)currentContact.Geboortedatum;
                        //if (currentContact.Type != "")
                        //    typeComboBox.Text = currentContact.Type;
                    }
                    else
                    {
                        voornaamTextBox.Text = currentContact.Voornaam;
                        achternaamTextBox.Text = currentContact.Achternaam;
                        if (currentContact.Taal != null)
                            taalComboBox.Text = currentContact.Taal.Naam;
                        if (!string.IsNullOrEmpty(currentContact.Aanspreking))
                            aansprekingComboBox.Text = currentContact.Aanspreking.ToUpper();
                        searchContact();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NieuwContact(Gebruiker user, Spreker spreker)
        {
            IsContact = false;
            IsChanged = false;
            IsInit = true;
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Bewerk spreker: Ingelogd als " + currentUser.Naam;
            BedrijfOpslaanButton.IsEnabled = false;
            ContactopslaanButton.IsEnabled = false;
            SprekerServices sService = new SprekerServices();
            Spreker FullSpreker = sService.GetSprekerById(spreker.ID);
            currentSpreker = spreker;
            try
            {
                if (spreker.Bedrijf != null)
                    currentBedrijf = spreker.Bedrijf;
            }
            catch (Exception)
            {
                BedrijfServices bService = new BedrijfServices();
                currentBedrijf = bService.GetBedrijfByID((decimal)spreker.Bedrijf_ID);
            }
            try
            {
                if (spreker.Contact != null)
                    currentContact = spreker.Contact;
            }
            catch (Exception)
            {
                ContactServices cService = new ContactServices();
                currentContact = cService.GetContactByID((decimal)spreker.Contact_ID);
            }
            newButton.Visibility = System.Windows.Visibility.Visible;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                sessieViewSource = ((CollectionViewSource)(this.FindResource("sessieViewSource")));
                inschrijvingenViewSource = ((CollectionViewSource)(this.FindResource("inschrijvingenViewSource")));
                sessie_SprekerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sessie_SprekerViewSource")));

                TaalServices taalService = new TaalServices();
                List<Taal> talen = taalService.FindAll();
                Taal taal = new Taal();
                taal.Naam = "Kies";
                talen.Insert(0, taal);
                taalComboBox.ItemsSource = talen;
                taalComboBox.DisplayMemberPath = "Naam";
                taalComboBox.SelectedIndex = 0;
                secundaireTaalComboBox.ItemsSource = talen;
                secundaireTaalComboBox.DisplayMemberPath = "Naam";
                secundaireTaalComboBox.SelectedIndex = 0;

                //new Thread(delegate () { LoadBedrijfCombobox(); }).Start();

                Niveau1Services n1Service = new Niveau1Services();
                List<Niveau1> N1Lijst = n1Service.FindActive();
                niveau1ComboBox1.ItemsSource = N1Lijst;
                niveau1ComboBox1.DisplayMemberPath = "Naam";

                Attesttype_Services aService = new Attesttype_Services();
                List<AttestType> attesttypes = aService.FindActive();
                AttestType type = new AttestType();
                type.Naam = "Kies";
                attesttypes.Insert(0, type);
                //contactAttesttypeComboBox.ItemsSource = attesttypes;
                //contactAttesttypeComboBox.DisplayMemberPath = "Naam";
                functieAttesttypeComboBox.ItemsSource = attesttypes;
                functieAttesttypeComboBox.DisplayMemberPath = "Naam";

                PostcodeService postcodeService = new PostcodeService();
                postcodes = postcodeService.FindAllPostcodes();
                postcodeComboBox.ItemsSource = postcodes;
                postcodeComboBox.DisplayMemberPath = "Postcode";

                //new Thread(delegate () { LoadContactenComboBox(); }).Start();

                FunctieListBox.DisplayMemberPath = "Functienaam";
                SprekerListBox.DisplayMemberPath = "Sprekernaam";

                startdatumDatePicker.Text = DateTime.Today.ToShortDateString();
                if (currentSpreker != null)
                {
                    HideFunctieInputFields();
                    //ShowSprekerTypes();
                    SetSprekerToForm();
                }
                else if (currentFunctie != null)
                {
                    HideSprekerInputFields();
                    //HideSprekerTypes();
                    SetFunctieToForm();
                }

                BindContactToForm();
                BindBedrijfToForm();
                //BindFunctieToForm();

                if (currentContact.ID == 0)
                {
                    IsChanged = true;
                }
                else
                    IsChanged = false;
                SetButtons();
                voornaamTextBox.Focus();
                IsInit = false;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het laden van het nieuw contact formulier!. " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void SetFullSprekerToForm()
        {
            if (currentSpreker != null)
            {
                SetSelectedContact(currentSpreker.Contact);
                SetSelectedBedrijf(currentSpreker.Bedrijf);
                SetSprekerToForm();
                //contactComboBox.Visibility = System.Windows.Visibility.Hidden;
                IDLabel.Visibility = System.Windows.Visibility.Hidden;
                //BedrijvenComboBox.Visibility = System.Windows.Visibility.Hidden;
                //bestaandebedrijvenLabel.Visibility = System.Windows.Visibility.Hidden;
                IsChanged = false;
                IsInit = false;
            }
        }

        private void SetSelectedBedrijf(Bedrijf bedrijf)
        {
            //if (bedrijf != null)
            //{
            //    for (int i = 0; i < BedrijvenComboBox.Items.Count; i++)
            //    {
            //        Bedrijf mybedrijf = BedrijvenComboBox.Items[i] as Bedrijf;
            //        if (mybedrijf.ID == bedrijf.ID)
            //        {
            //            BedrijvenComboBox.SelectedIndex = i;
            //            break;
            //        }
            //    }
            //}
            //else
            //{
            //    BedrijfServices bService = new BedrijfServices();
            //    Bedrijf myBedrijf = bService.GetBedrijfByID(currentFunctie.Bedrijf_ID);
            //    for (int i = 0; i < BedrijvenComboBox.Items.Count; i++)
            //    {
            //        Bedrijf mybedrijf = BedrijvenComboBox.Items[i] as Bedrijf;
            //        if (mybedrijf.ID == bedrijf.ID)
            //        {
            //            BedrijvenComboBox.SelectedIndex = i;
            //            break;
            //        }
            //    }
            //}
        }

        private void SetSelectedContact(Contact contact)
        {
            //if (contact != null)
            //{
            //    for (int i = 0; i < contactComboBox.Items.Count; i++)
            //    {
            //        Contact mycontact = contactComboBox.Items[i] as Contact;
            //        if (mycontact.ID == contact.ID)
            //        {
            //            contactComboBox.SelectedIndex = i;
            //            break;
            //        }
            //    }
            //}
            //else
            //{
            //    ContactServices cService = new ContactServices();
            //    Contact myContact = cService.GetContactByID(currentFunctie.Contact_ID);
            //    for (int i = 0; i < contactComboBox.Items.Count; i++)
            //    {
            //        Contact mycontact = contactComboBox.Items[i] as Contact;
            //        if (mycontact.ID == myContact.ID)
            //        {
            //            contactComboBox.SelectedIndex = i;
            //            break;
            //        }
            //    }
            //}
        }

        private void SetFullFunctionToForm()
        {
            if (currentFunctie != null)
            {
                SetSelectedContact(currentFunctie.Contact);
                SetSelectedBedrijf(currentFunctie.Bedrijf);
                SetFunctieToForm();
                //contactComboBox.Visibility = System.Windows.Visibility.Hidden;
                IDLabel.Visibility = System.Windows.Visibility.Hidden;
                //BedrijvenComboBox.Visibility = System.Windows.Visibility.Hidden;
                //bestaandebedrijvenLabel.Visibility = System.Windows.Visibility.Hidden;

                IsChanged = false;
                IsInit = false;
            }
        }

        /* __________________________________________________
         * |                                                 |
         * |    Contact functies en methoden                 |
         * |_________________________________________________|
         * */

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (currentContact.ID != 0)
                {
                    //contact mycontact = (contact)contactcombobox.selecteditem;
                    Contact changedcontact = GetContactFromForm();
                    if (changedcontact != null)
                    {
                        try
                        {
                            changedcontact.Gewijzigd = DateTime.Now;
                            changedcontact.Gewijzigd_door = currentUser.Naam;
                            changedcontact.ID = currentContact.ID;
                            changedcontact.Modified = currentContact.Modified;
                            ContactServices service = new ContactServices();
                            service.SaveContactChanges(changedcontact);
                            currentContact = changedcontact;
                            BindContactToForm();
                            IsChanged = false;
                            SetButtons();
                            ConfocusMessageBox.Show("Contact opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show("Fout bij het opslaan van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }
                else
                {
                    Contact myContact = GetContactFromForm();
                    if (myContact != null)
                    {
                        try
                        {
                            myContact.Aangemaakt = DateTime.Now;
                            myContact.Aangemaakt_door = currentUser.Naam;
                            myContact.Actief = true;
                            ContactServices service = new ContactServices();
                            Contact savedContact = service.SaveContact(myContact, false);
                            currentContact = savedContact;
                            BindContactToForm();
                            IsChanged = false;
                            SetButtons();
                            ConfocusMessageBox.Show("Contact opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                            bedrijfTab.IsSelected = true;
                            naamTextBox.Focus();
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show("Fout bij het opslaan van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
            SetButtons();
        }

        private void ContactVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            bedrijfTab.IsSelected = true;
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(delegate ()
            {
                naamTextBox.Focus();
                Keyboard.Focus(naamTextBox);
                if (naamTextBox.Text != "")
                    naamTextBox.SelectAll();
            }));

        }


        private Contact GetContactFromForm()
        {
            if (IsContactFormValid())
            {
                Contact contact = new Contact();
                contact.Voornaam = voornaamTextBox.Text;
                contact.Achternaam = achternaamTextBox.Text;
                contact.Aanspreking = aansprekingComboBox.Text;
                //if (contactAttesttypeComboBox.SelectedIndex > 0)
                //    contact.Attesttype = ((AttestType)contactAttesttypeComboBox.SelectedItem).Naam;
                if (taalComboBox.SelectedIndex > 0)
                    contact.Taal_ID = ((Taal)taalComboBox.SelectedItem).ID;
                if (secundaireTaalComboBox.SelectedIndex > 0)
                    contact.Secundaire_Taal_ID = ((Taal)secundaireTaalComboBox.SelectedItem).ID;
                contact.Notities = notitiesTextBox.Text;
                if (geboortedatumDatePicker.SelectedDate != null)
                    contact.Geboortedatum = geboortedatumDatePicker.SelectedDate;
                //if (typeComboBox.SelectedIndex > -1)
                //    contact.Type = typeComboBox.Text;
                contact.Aangemaakt = DateTime.Now;
                contact.Aangemaakt_door = currentUser.Naam;
                contact.Actief = actiefCheckBox.IsChecked;

                return contact;
            }
            else
                return null;
        }

        private void contactComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox combo = (ComboBox)sender;

                if (combo.SelectedIndex > 0)
                {
                    Contact contact = (Contact)combo.SelectedItem;
                    VulContactIn(contact);
                    ContactVolgendeButton.IsEnabled = true;
                }
                else
                {
                    MaakContactLeeg();
                    thisContact = null;
                    ContactVolgendeButton.IsEnabled = false;

                }
                ClearInfoLabels();
                //BedrijvenComboBox.SelectedIndex = 0;
                ContactopslaanButton.IsEnabled = false;
                IsChanged = false;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het selecteren van van een contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void MaakContactLeeg()
        {
            voornaamTextBox.Text = "";
            voornaamLabel.Content = "";
            achternaamTextBox.Text = "";
            achternaamLabel.Content = "";
            aansprekingComboBox.Text = "";
            aansprekingLabel.Content = "";
            taalComboBox.SelectedIndex = -1;
            secundaireTaalComboBox.SelectedIndex = -1;
            // taalLabel.Content = "";
            notitiesTextBox.Text = "";
            notitiesLabel.Content = "";
            geboortedatumDatePicker.SelectedDate = null;
            //typeComboBox.SelectedIndex = -1;
            typeLabel.Content = "";
            //contactBedrijvenLabel.Content = "";
            //contactFunctieLabel.Content = "";
            FunctieListBox.Items.Clear();
            //HideContactLabels();
        }

        private void HideContactLabels()
        {
            voornaamTextBox.Visibility = System.Windows.Visibility.Visible;
            voornaamLabel.Visibility = System.Windows.Visibility.Hidden;
            achternaamTextBox.Visibility = System.Windows.Visibility.Visible;
            achternaamLabel.Visibility = System.Windows.Visibility.Hidden;
            aansprekingComboBox.Visibility = System.Windows.Visibility.Visible;
            aansprekingLabel.Visibility = System.Windows.Visibility.Hidden;
            taalComboBox.Visibility = System.Windows.Visibility.Visible;
            //taalLabel.Visibility = System.Windows.Visibility.Hidden;
            geboortedatumDatePicker.Visibility = System.Windows.Visibility.Visible;
            geboortedatumLabel.Visibility = System.Windows.Visibility.Hidden;
            notitiesTextBox.Visibility = System.Windows.Visibility.Visible;
            notitiesLabel.Visibility = System.Windows.Visibility.Hidden;
            //typeComboBox.Visibility = System.Windows.Visibility.Visible;
            typeLabel.Visibility = System.Windows.Visibility.Hidden;
            actiefCheckBox.Visibility = System.Windows.Visibility.Visible;
            actiefLabel.Visibility = System.Windows.Visibility.Hidden;
            bedrijfnaamLabel.Content = "";
            functienaamLabel.Content = "";
        }

        private void HideContactInputFields()
        {
            voornaamTextBox.Visibility = System.Windows.Visibility.Hidden;
            voornaamLabel.Visibility = System.Windows.Visibility.Visible;
            achternaamTextBox.Visibility = System.Windows.Visibility.Hidden;
            achternaamLabel.Visibility = System.Windows.Visibility.Visible;
            aansprekingComboBox.Visibility = System.Windows.Visibility.Hidden;
            aansprekingLabel.Visibility = System.Windows.Visibility.Visible;
            taalComboBox.Visibility = System.Windows.Visibility.Hidden;
            // taalLabel.Visibility = System.Windows.Visibility.Visible;
            geboortedatumDatePicker.Visibility = System.Windows.Visibility.Hidden;
            geboortedatumLabel.Visibility = System.Windows.Visibility.Visible;
            notitiesTextBox.Visibility = System.Windows.Visibility.Hidden;
            notitiesLabel.Visibility = System.Windows.Visibility.Visible;
            //typeComboBox.Visibility = System.Windows.Visibility.Hidden;
            typeLabel.Visibility = System.Windows.Visibility.Visible;
            actiefCheckBox.Visibility = System.Windows.Visibility.Hidden;
            actiefLabel.Visibility = System.Windows.Visibility.Visible;
        }

        private void VulContactIn(Contact contact)
        {
            try
            {
                voornaamTextBox.Text = contact.Voornaam;
                //voornaamLabel.Content = contact.Voornaam;
                achternaamTextBox.Text = contact.Achternaam;
                //achternaamLabel.Content = contact.Achternaam;
                aansprekingComboBox.Text = contact.Aanspreking;
                //aansprekingLabel.Content = contact.Aanspreking;
                if (contact.Taal_ID != null)
                {
                    TaalServices taalservice = new TaalServices();
                    Taal preTaal = taalservice.GetTaalByID((Decimal)contact.Taal_ID);
                    taalComboBox.Text = preTaal.Naam;
                }
                if (contact.Secundaire_Taal_ID != null)
                {
                    TaalServices taalservice = new TaalServices();
                    Taal secTaal = taalservice.GetTaalByID((Decimal)contact.Secundaire_Taal_ID);
                    secundaireTaalComboBox.Text = secTaal.Naam;
                }
                //if (contact.Erkenning_ID != null)
                //{
                //    ErkenningServices eService = new ErkenningServices();
                //    Erkenning erkenning = eService.GetErkenningByID(contact.Erkenning_ID);
                //    contactErkenningComboBox.Text = erkenning.Naam;
                //}
                //if (contact.Attesttype != "")
                //    contactAttesttypeComboBox.Text = contact.Attesttype;
                //else
                //    contactAttesttypeComboBox.SelectedIndex = 0;
                notitiesTextBox.Text = contact.Notities;
                //notitiesLabel.Content = contact.Notities;
                //typeComboBox.Text = contact.Type;
                //typeLabel.Content = contact.Type;
                geboortedatumDatePicker.SelectedDate = contact.Geboortedatum;
                //geboortedatumLabel.Content = contact.Geboortedatum;
                actiefCheckBox.IsChecked = contact.Actief == true;
                //actiefLabel.Content = contact.Actief == true ? "Ja" : "Nee";
                //HideContactInputFields();

                thisContact = contact;

                SetContactInfo();

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het invullen van het contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void SetContactInfo()
        {
            contactBedrijvenListbox.Items.Clear();
            contactFunctieListBox.Items.Clear();
            Contact contact = currentContact;
            if (contact != null)
            {
                if (!IsContact)
                {
                    try
                    {
                        SprekerServices service = new SprekerServices();
                        List<Spreker> sprekers = service.GetSprekersByContact(contact.ID);
                        if (sprekers.Count > 0)
                        {
                            Int16 teller = 0;
                            //String strSprekers = "";
                            //String strBedrijven = "";
                            List<Bedrijf> bedrijven = new List<Bedrijf>();
                            foreach (Spreker s in sprekers)
                            {
                                //if (teller < 4)
                                //{
                                //    strSprekers += s.Niveau1.Naam;
                                //    if (s.Niveau2 != null)
                                //        strSprekers += " - " + s.Niveau2.Naam;
                                //    if (s.Niveau3 != null)
                                //        strSprekers += " - " + s.Niveau3.Naam;
                                //}
                                contactBedrijvenListbox.Items.Add(s);
                                contactBedrijvenListbox.DisplayMemberPath = "Bedrijf.Naam";

                                contactFunctieListBox.Items.Add(s);
                                //SprekerListBox.Items.Add(s);
                                contactFunctieListBox.DisplayMemberPath = "Sprekernaam";
                                //SprekerListBox.DisplayMemberPath = "Sprekernaam";

                                Bedrijf bedrijf = s.Bedrijf;
                                if (!bedrijven.Contains(bedrijf))
                                    bedrijven.Add(bedrijf);

                                teller++;
                            }
                            //foreach (Bedrijf b in bedrijven)
                            //{
                            //    //strBedrijven += b.Naam + "\n";
                            //    contactBedrijvenListbox.Items.Add(b);
                            //    contactBedrijvenListbox.DisplayMemberPath = "Naam";


                            //}
                            if (contactBedrijvenListbox.Items.Count > 0)
                                bedrijfnaamLabel.Content = "Bedrijven:";
                            //contactBedrijvenLabel.Content = strBedrijven;
                            if (contactFunctieListBox.Items.Count > 0)
                                functienaamLabel.Content = "Sprekers:";
                            //contactFunctieLabel.Content = strSprekers;
                        }

                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show("Fout bij het weergeven van de contact info!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                else
                {
                    try
                    {
                        FunctieServices service = new FunctieServices();
                        List<Functies> functies = service.GetFunctieByContact(contact.ID);
                        if (functies.Count > 0)
                        {
                            Int16 teller = 0;
                            //String strfuncties = "";
                            //String strBedrijven = "";
                            List<Bedrijf> bedrijven = new List<Bedrijf>();
                            foreach (Functies f in functies)
                            {
                                //if (teller < 4)
                                //{
                                //    strfuncties += f.Niveau1.Naam;
                                //    if (f.Niveau2 != null)
                                //        strfuncties += " - " + f.Niveau2.Naam;
                                //    if (f.Niveau3 != null)
                                //        strfuncties += " - " + f.Niveau3.Naam;
                                //    strfuncties += "\n";
                                //}
                                contactBedrijvenListbox.Items.Add(f);
                                contactBedrijvenListbox.DisplayMemberPath = "Bedrijf.Naam";

                                contactFunctieListBox.Items.Add(f);
                                contactFunctieListBox.DisplayMemberPath = "Functienaam";
                                //Bedrijf bedrijf = f.Bedrijf;
                                //if (!bedrijven.Contains(bedrijf))
                                //    bedrijven.Add(bedrijf);
                                teller++;
                            }
                            //foreach (Bedrijf b in bedrijven)
                            //{
                            //    //strBedrijven += b.Naam + "\n";
                            //    contactBedrijvenListbox.Items.Add(b);
                            //    contactBedrijvenListbox.DisplayMemberPath = "BedrijfNaam";

                            //}
                            if (contactBedrijvenListbox.Items.Count > 0)
                                bedrijfnaamLabel.Content = "Bedrijven:";
                            //contactBedrijvenLabel.Content = strBedrijven;
                            if (contactFunctieListBox.Items.Count > 0)
                                functienaamLabel.Content = "Functies:";
                            //contactFunctieLabel.Content = strfuncties;
                        }

                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show("Fout bij het weergeven van de contact info!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                IsChanged = false;
            }
        }


        private bool IsContactFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (voornaamTextBox.Text == "")
            {
                errorMessage += "De voornaam is niet ingevuld!\n";
                Helper.SetTextBoxInError(voornaamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(voornaamTextBox, normalTemplate);
            if (achternaamTextBox.Text == "")
            {
                errorMessage += "De achternaam is niet ingevuld!\n";
                Helper.SetTextBoxInError(achternaamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(achternaamTextBox, normalTemplate);

            if (taalComboBox.SelectedIndex <= 0)
            {
                errorMessage += "Geen primaire taal gekozen!";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        /* __________________________________________________
         * |                                                 |
         * |    Bedrijf functies en methoden                 |
         * |_________________________________________________|
         * */

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SaveBedrijf();
        }

        private void SaveBedrijf()
        {
            try
            {
                if (currentBedrijf.ID == 0)
                {
                    Bedrijf myBedrijf = GetBedrijfFromForm();
                    if (myBedrijf != null)
                    {
                        //myBedrijf.BedrijfsnaamFonetisch = DBHelper.Simplify(myBedrijf.Naam);
                        myBedrijf.Actief = true;
                        myBedrijf.Aangemaakt = DateTime.Now;
                        myBedrijf.Aangemaakt_door = currentUser.Naam;
                        BedrijfServices service = new BedrijfServices();
                        try
                        {
                            Bedrijf savedBedrijf = service.SaveBedrijf(myBedrijf);
                            currentBedrijf = savedBedrijf;
                            BindBedrijfToForm();
                            //telefoonTextBox1.Text = savedBedrijf.Telefoon;
                            IsChanged = false;
                            ConfocusMessageBox.Show("Bedrijf opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                            functiesTab.IsSelected = true;
                            IsChanged = true;
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show("Fout bij het opslaan van het bedrijf, probeer later opnieuw" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }
                else
                {
                    Bedrijf changedBedrijf = GetBedrijfFromForm();
                    changedBedrijf.ID = currentBedrijf.ID;
                    //changedBedrijf.BedrijfsnaamFonetisch = DBHelper.Simplify(changedBedrijf.Naam);
                    changedBedrijf.Modified = currentBedrijf.Modified;
                    changedBedrijf.Gewijzigd = DateTime.Now;
                    changedBedrijf.Gewijzigd_door = currentUser.Naam;
                    BedrijfServices service = new BedrijfServices();
                    try
                    {
                        service.SaveBedrijfChanges(changedBedrijf);
                        currentBedrijf = changedBedrijf;
                        //telefoonTextBox1.Text = currentBedrijf.Telefoon;
                        IsChanged = false;
                        ConfocusMessageBox.Show("Bedrijf opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message + "\n" + (ex.InnerException == null ? "" : ex.InnerException.Message), ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                SetContactInfo();
                SetButtons();
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void LoadBedrijfCombobox()
        {
        }

        private void LoadContactenComboBox()
        {
        }

        private void BedrijfVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            functiesTab.IsSelected = true;
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(delegate () { niveau1ComboBox1.Focus(); Keyboard.Focus(niveau1ComboBox1); }));
        }

        private void ChangeIsChanged()
        {
            IsChanged = false;
        }

        private Bedrijf GetBedrijfFromForm()
        {
            try
            {
                if (IsBedrijfFormValid())
                {
                    Bedrijf bedrijf = new Bedrijf();

                    bedrijf.Naam = naamTextBox.Text;
                    bedrijf.Ondernemersnummer = Helper.CleanVatnumber(ondernemersnummerTextBox.Text);
                    bedrijf.Adres = adresTextBox.Text;
                    if (postcodeComboBox.SelectedIndex > -1)
                        bedrijf.Postcode = postcodeComboBox.Text;
                    if (plaatsComboBox.SelectedIndex > -1)
                    {
                        bedrijf.Plaats = plaatsComboBox.Text;
                        bedrijf.Zipcode = ((Postcodes)plaatsComboBox.SelectedItem).ID;
                    }
                    if (provincieComboBox.SelectedIndex > -1)
                        bedrijf.Provincie = provincieComboBox.Text;
                    bedrijf.Land = landTextBox.Text;
                    bedrijf.Telefoon = telefoonTextBox.Text;
                    bedrijf.Fax = faxTextBox.Text;
                    bedrijf.Website = websiteTextBox.Text;
                    bedrijf.Email = emailTextBox.Text;
                    bedrijf.Notities = notitiesBedrijfTextBox.Text;
                    bedrijf.Actief = actiefCheckBox1.IsChecked == true;
                    bedrijf.Aangemaakt = DateTime.Now;
                    bedrijf.Aangemaakt_door = currentUser.Naam;
                    bedrijf.BedrijfsnaamFonetisch = DBHelper.Simplify(bedrijf.Naam);

                    return bedrijf;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het verzamelen van de bedrijfsgegevens!n" + ex.Message);
            }
        }

        private bool IsBedrijfFormValid()
        {
            try
            {
                Boolean valid = true;
                String errorMessage = "";
                ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
                ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
                if (naamTextBox.Text == "")
                {
                    errorMessage += "Geen naam ingevuld!\n";
                    Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                    valid = false;
                }
                else
                    Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);

                if (ondernemersnummerTextBox.Text.Length > 12)
                {
                    errorMessage += "De ondernemingsnummer is te lang!\nVerwijder eventuele spaties of punten.";
                    Helper.SetTextBoxInError(ondernemersnummerTextBox, errorTemplate);
                    valid = false;
                }
                else
                    Helper.SetTextBoxToNormal(ondernemersnummerTextBox, normalTemplate);

                if (emailTextBox.Text.Length > 120)
                {
                    errorMessage += "De emailadres is te lang! Maximum 120 karakters.";
                    Helper.SetTextBoxInError(emailTextBox, errorTemplate);
                    valid = false;
                }
                else
                    Helper.SetTextBoxToNormal(emailTextBox, normalTemplate);

                if (telefoonTextBox.Text.Length > 30)
                {
                    errorMessage += "Het telefoonnummer is te lang!\nVerwijder eventuele spaties of punten.";
                    Helper.SetTextBoxInError(telefoonTextBox, errorTemplate);
                    valid = false;
                }
                else
                    Helper.SetTextBoxToNormal(telefoonTextBox, normalTemplate);

                if (faxTextBox.Text.Length > 30)
                {
                    errorMessage += "Het faxnummer is te lang!\nVerwijder eventuele spaties of punten.";
                    Helper.SetTextBoxInError(faxTextBox, errorTemplate);
                    valid = false;
                }
                else
                    Helper.SetTextBoxToNormal(faxTextBox, normalTemplate);

                if (!valid)
                    ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

                return valid;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Er is iets fout gegaan bij de validatie van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                return false;
            }
        }

        private void BedrijvenComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > 0)
            {
                Bedrijf gekozenBedrijf = combo.SelectedItem as Bedrijf;
                //BindBedrijfToForm();
                SetBedrijfsInfo();
                BedrijfOpslaanButton.IsEnabled = false;
                BedrijfVolgendeButton.IsEnabled = true;
            }
            else
            {
                thisBedrijf = null;
                idLabel.Content = "";
                ClearBedrijf();
                //HideBedrijfLabels();
                ClearInfoLabels();
                BedrijfOpslaanButton.IsEnabled = false;
                BedrijfVolgendeButton.IsEnabled = false;
            }
            IsChanged = false;
        }

        private void ClearBedrijf()
        {
            idLabel.Content = "";
            naamTextBox.Text = "";
            ondernemersnummerTextBox.Text = "";
            adresTextBox.Text = "";
            postcodeComboBox.SelectedIndex = -1;
            plaatsComboBox.SelectedIndex = -1;
            provincieComboBox.SelectedIndex = -1;
            landTextBox.Text = "";
            telefoonTextBox.Text = "0032";
            faxTextBox.Text = "0032";
            websiteTextBox.Text = "";
            emailTextBox.Text = "";
            notitiesBedrijfTextBox.Text = "";
            FunctieListBox.Items.Clear();
        }

        private void ClearInfoLabels()
        {
            bedrijfSoortLabel.Content = "";
            bedrijfSoortTitelLabel.Content = "";
            SoortTitelLabel.Content = "";
            SoortLabel.Content = "";
        }

        private void SetBedrijfsInfo()
        {
            bedrijfsFunctieListBox.Items.Clear();
            if (currentContact != null && currentBedrijf != null)
            {
                try
                {
                    if (currentContact.ID != 0 && currentBedrijf.ID != 0)
                    {
                        if (functiesTab.Header.ToString() == "Spreker")
                        {
                            SprekerServices service = new SprekerServices();
                            List<Spreker> sprekers = service.GetSprekersByContactBedrijf(currentContact.ID, currentBedrijf.ID);
                            //String strSprekers = "";
                            SprekerListBox.Items.Clear();
                            foreach (Spreker spr in sprekers)
                            {
                                SprekerListBox.Items.Add(spr);
                                bedrijfsFunctieListBox.Items.Add(spr);
                                bedrijfsFunctieListBox.DisplayMemberPath = "Sprekernaam";

                                //strSprekers += spr.Niveau1.Naam + ": " + spr.Niveau2_ID == null ? "" : spr.Niveau2.Naam + ": " + spr.Niveau3_ID == null ? "" : spr.Niveau3.Naam + "\n";
                            }
                            if (contactFunctieListBox.Items.Count > 0)
                            {
                                bedrijfSoortTitelLabel.Content = "Sprekers";
                                SoortTitelLabel.Content = "Spreker";
                                //bedrijfSoortLabel.Content = strSprekers;
                            }
                            else
                            {
                                bedrijfSoortTitelLabel.Content = "";
                                SoortTitelLabel.Content = "";
                                bedrijfSoortLabel.Content = "";
                            }
                        }
                        else
                        {
                            FunctieServices service = new FunctieServices();
                            List<Functies> functies = service.GetFunctiesByContactBedrijf(currentContact.ID, currentBedrijf.ID);
                            bedrijfSoortTitelLabel.Content = "Functies";
                            SoortTitelLabel.Content = "Functies";
                            //String strFuncties = "";
                            FunctieListBox.Items.Clear();
                            foreach (Functies func in functies)
                            {
                                FunctieListBox.Items.Add(func);
                                bedrijfsFunctieListBox.Items.Add(func);
                                bedrijfsFunctieListBox.DisplayMemberPath = "Functienaam";
                                //strFuncties += func.Niveau1.Naam;
                                //if (func.Niveau2_ID != null)
                                //    strFuncties += ": " + func.Niveau2.Naam;
                                //if (func.Niveau3_ID != null)
                                //    strFuncties += ": " + func.Niveau3.Naam;
                                //strFuncties += "\n";
                            }
                            //bedrijfSoortLabel.Content = strFuncties;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BindBedrijfToForm()
        {
            try
            {
                if (currentBedrijf != null)
                {
                    idLabel.Content = currentBedrijf.ID;
                    BedrijfServices bService = new BedrijfServices();
                    Bedrijf fulcompany = bService.GetBedrijfByID(currentBedrijf.ID);
                    if (fulcompany != null)
                    {
                        currentBedrijf = fulcompany;
                        naamTextBox.Text = currentBedrijf.Naam;
                        ondernemersnummerTextBox.Text = currentBedrijf.Ondernemersnummer;
                        adresTextBox.Text = currentBedrijf.Adres;
                        postcodeComboBox.Text = currentBedrijf.Postcode;
                        plaatsComboBox.Text = currentBedrijf.Plaats;
                        provincieComboBox.Text = currentBedrijf.Provincie;
                        landTextBox.Text = currentBedrijf.Land;

                        telefoonTextBox.Text = currentBedrijf.Telefoon;
                        faxTextBox.Text = currentBedrijf.Fax;
                        websiteTextBox.Text = currentBedrijf.Website;
                        emailTextBox.Text = currentBedrijf.Email;
                        notitiesBedrijfTextBox.Text = currentBedrijf.Notities;
                        actiefCheckBox1.IsChecked = currentBedrijf.Actief;

                        if (currentBedrijf.Zipcode != null)
                        {
                            postcodeComboBox.Text = currentBedrijf.strPostcode;
                            plaatsComboBox.Text = currentBedrijf.strGemeente;
                            provincieComboBox.Text = currentBedrijf.strProvincie;
                            landTextBox.Text = currentBedrijf.strLand;
                        }
                        else
                        {
                            postcodeComboBox.Text = currentBedrijf.Postcode;
                            plaatsComboBox.Text = currentBedrijf.Plaats;
                            provincieComboBox.Text = currentBedrijf.Provincie;
                            landTextBox.Text = currentBedrijf.Land;
                        }

                        //if (functiesTab.Header.ToString() == "Functies")
                        //{
                        //    telefoonTextBox1.Text = currentBedrijf.Telefoon;
                        //}

                        thisBedrijf = currentBedrijf;
                    }
                    else
                    {
                        naamTextBox.Text = currentBedrijf.Naam;
                        ondernemersnummerTextBox.Text = currentBedrijf.Ondernemersnummer;
                        adresTextBox.Text = currentBedrijf.Adres;
                        telefoonTextBox.Text = currentBedrijf.Telefoon;
                        faxTextBox.Text = currentBedrijf.Fax;
                        emailTextBox.Text = currentBedrijf.Email;

                        if (currentBedrijf.Zipcode != null)
                        {
                            postcodeComboBox.Text = currentBedrijf.strPostcode;
                            plaatsComboBox.Text = currentBedrijf.strGemeente;
                            provincieComboBox.Text = currentBedrijf.strProvincie;
                            landTextBox.Text = currentBedrijf.strLand;
                        }
                        else
                        {
                            postcodeComboBox.Text = currentBedrijf.Postcode;
                            plaatsComboBox.Text = currentBedrijf.Plaats;
                            //provincieComboBox.Text = currentBedrijf.Provincie;
                            //landTextBox.Text = currentBedrijf.Land;
                            if (postcodeComboBox.SelectedIndex > -1)
                            {
                                Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                                provincieComboBox.Text = postcode.Provincie;
                                landTextBox.Text = postcode.Land;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het invullen van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void HideBedrijfInputFields()
        {
            naamTextBox.Visibility = Visibility.Hidden;
            naamLabel.Visibility = System.Windows.Visibility.Visible;
            ondernemersnummerTextBox.Visibility = System.Windows.Visibility.Hidden;
            ondernemersnummerLabel.Visibility = System.Windows.Visibility.Visible;
            adresTextBox.Visibility = System.Windows.Visibility.Hidden;
            adresLabel.Visibility = System.Windows.Visibility.Visible;
            postcodeComboBox.Visibility = System.Windows.Visibility.Hidden;
            postcodeLabel.Visibility = System.Windows.Visibility.Visible;
            plaatsComboBox.Visibility = System.Windows.Visibility.Hidden;
            plaatsLabel.Visibility = System.Windows.Visibility.Visible;
            provincieComboBox.Visibility = System.Windows.Visibility.Hidden;
            provincieLabel.Visibility = System.Windows.Visibility.Visible;
            landTextBox.Visibility = System.Windows.Visibility.Hidden;
            landLabel.Visibility = System.Windows.Visibility.Visible;
            telefoonTextBox.Visibility = System.Windows.Visibility.Hidden;
            telefoonLabel.Visibility = System.Windows.Visibility.Visible;
            faxTextBox.Visibility = System.Windows.Visibility.Hidden;
            faxLabel.Visibility = System.Windows.Visibility.Visible;
            websiteTextBox.Visibility = System.Windows.Visibility.Hidden;
            websiteLabel.Visibility = System.Windows.Visibility.Visible;
            emailTextBox.Visibility = System.Windows.Visibility.Hidden;
            emailLabel.Visibility = System.Windows.Visibility.Visible;
            notitiesBedrijfTextBox.Visibility = System.Windows.Visibility.Hidden;
            notitieBedrijfLabel.Visibility = System.Windows.Visibility.Visible;
            actiefCheckBox1.Visibility = System.Windows.Visibility.Hidden;
            actiefBedrijfLabel.Visibility = System.Windows.Visibility.Visible;
        }

        private void HideBedrijfLabels()
        {
            naamTextBox.Visibility = System.Windows.Visibility.Visible;
            naamLabel.Visibility = System.Windows.Visibility.Hidden;
            ondernemersnummerTextBox.Visibility = System.Windows.Visibility.Visible;
            ondernemersnummerLabel.Visibility = System.Windows.Visibility.Hidden;
            adresTextBox.Visibility = System.Windows.Visibility.Visible;
            adresLabel.Visibility = System.Windows.Visibility.Hidden;
            postcodeComboBox.Visibility = System.Windows.Visibility.Visible;
            postcodeLabel.Visibility = System.Windows.Visibility.Hidden;
            plaatsComboBox.Visibility = System.Windows.Visibility.Visible;
            plaatsLabel.Visibility = System.Windows.Visibility.Hidden;
            provincieComboBox.Visibility = System.Windows.Visibility.Visible;
            provincieLabel.Visibility = System.Windows.Visibility.Hidden;
            landTextBox.Visibility = System.Windows.Visibility.Visible;
            landLabel.Visibility = System.Windows.Visibility.Hidden;
            telefoonTextBox.Visibility = System.Windows.Visibility.Visible;
            telefoonLabel.Visibility = System.Windows.Visibility.Hidden;
            faxTextBox.Visibility = System.Windows.Visibility.Visible;
            faxLabel.Visibility = System.Windows.Visibility.Hidden;
            websiteTextBox.Visibility = System.Windows.Visibility.Visible;
            websiteLabel.Visibility = System.Windows.Visibility.Hidden;
            emailTextBox.Visibility = System.Windows.Visibility.Visible;
            emailLabel.Visibility = System.Windows.Visibility.Hidden;
            notitiesBedrijfTextBox.Visibility = System.Windows.Visibility.Visible;
            notitieBedrijfLabel.Visibility = System.Windows.Visibility.Hidden;
            actiefCheckBox1.Visibility = System.Windows.Visibility.Visible;
            actiefBedrijfLabel.Visibility = System.Windows.Visibility.Hidden;
        }

        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (postcodeComboBox.SelectedIndex > -1)
                {
                    IsChanged = true;
                    Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                    var gemeentes = (from p in postcodes
                                     where p.Postcode == postcode.Postcode
                                     select p).ToList();
                    if (gemeentes.Count > 0)
                    {
                        plaatsComboBox.ItemsSource = gemeentes;
                        plaatsComboBox.DisplayMemberPath = "Gemeente";
                        plaatsComboBox.SelectedIndex = 0;
                    }
                    landTextBox.Text = postcode.Land;
                    provincieComboBox.Text = postcode.Provincie;
                }
                else
                {
                    provincieComboBox.SelectedIndex = -1;
                    landTextBox.Text = "";
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Er is iets fout gegaan bij de verwerking van de geselecteerde Postcode!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }


        /* __________________________________________________
         * |                                                 |
         * |    Functies functies en methoden                |
         * |_________________________________________________|
         * */



        private void niveau1ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox combo = (ComboBox)sender;
                if (combo.SelectedIndex > -1)
                {
                    IsChanged = true;
                    Decimal N1ID = ((Niveau1)(combo.SelectedItem)).ID;
                    Niveau2Services n2Service = new Niveau2Services();
                    List<Niveau2> N2Lijst = n2Service.GetSelected(N1ID);
                    Niveau2 kies = new Niveau2();
                    kies.ID = 0;
                    kies.Naam = "Kies";
                    N2Lijst.Insert(0, kies);
                    niveau2ComboBox2.ItemsSource = N2Lijst;
                    niveau2ComboBox2.DisplayMemberPath = "Naam";
                }
                else
                {
                    niveau2ComboBox2.ItemsSource = new List<Niveau2>();
                    niveau2ComboBox2.DisplayMemberPath = "Naam";
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Er is iets fout gegaan bij het selecteren van het niveau!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void niveau2ComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox combo = (ComboBox)sender;
                if (combo.SelectedIndex > 0)
                {
                    IsChanged = true;
                    Decimal N2ID = ((Niveau2)(combo.SelectedItem)).ID;
                    Niveau3Services n3Service = new Niveau3Services();
                    List<Niveau3> N3Lijst = n3Service.GetSelected(N2ID);
                    Niveau3 kies = new Niveau3();
                    kies.ID = 0;
                    kies.Naam = "Kies";
                    N3Lijst.Insert(0, kies);
                    niveau3ComboBox3.ItemsSource = N3Lijst;
                    niveau3ComboBox3.DisplayMemberPath = "Naam";
                }
                else
                {
                    niveau3ComboBox3.ItemsSource = new List<Niveau3>();
                    niveau3ComboBox3.DisplayMemberPath = "Naam";
                }

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Er is iets fout gegaan bij het selecteren van het niveau!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private Functies GetFunctieFromForm()
        {
            if (IsFunctieFormValid())
            {
                try
                {
                    Functies functie = new Functies();

                    if (currentContact.Voornaam != "" || currentContact.Voornaam != null)
                    {
                        functie.Contact_ID = currentContact.ID;
                        functie.Contact = currentContact;
                    }
                    if (currentBedrijf.Naam != "" || currentBedrijf.Naam != null)
                    {
                        functie.Bedrijf_ID = currentBedrijf.ID;
                        functie.Bedrijf = currentBedrijf;
                    }
                    if (niveau1ComboBox1.SelectedIndex > -1)
                    {
                        functie.Niveau1_ID = ((Niveau1)niveau1ComboBox1.SelectedItem).ID;
                        functie.Niveau1 = ((Niveau1)niveau1ComboBox1.SelectedItem);
                    }
                    if (niveau2ComboBox2.SelectedIndex > 0)
                    {
                        functie.Niveau2_ID = ((Niveau2)niveau2ComboBox2.SelectedItem).ID;
                        functie.Niveau2 = ((Niveau2)niveau2ComboBox2.SelectedItem);
                    }
                    if (niveau3ComboBox3.SelectedIndex > 0)
                    {
                        functie.Niveau3_ID = ((Niveau3)niveau3ComboBox3.SelectedItem).ID;
                        functie.Niveau3 = ((Niveau3)niveau3ComboBox3.SelectedItem);
                    }
                    if (functieAttesttypeComboBox.SelectedIndex > -1)
                    {
                        functie.Attesttype = functieAttesttypeComboBox.Text;
                        functie.Attesttype_ID = ((AttestType)functieAttesttypeComboBox.SelectedItem).ID;
                    }
                    else
                    {
                        functie.Attesttype = "";
                    }
                    functie.Erkenningsnummer = erkenningTextBox1.Text;
                    functie.Oorspronkelijke_Titel = oorspronkelijkeTextBox.Text;
                    functie.Email = emailTextBox1.Text;
                    functie.Telefoon = telefoonTextBox1.Text;
                    functie.Mobiel = mobielTextBox.Text;
                    functie.Mail = mailCheckBox.IsChecked == true;

                    FunctieServices fService = new FunctieServices();
                    List<Functies> functies = fService.GetFunctiesInBedrijf(currentBedrijf.ID);
                    bool noFax = false;
                    foreach (Functies funct in functies)
                    {
                        if (funct.Fax == false)
                            noFax = true;
                    }
                    if (noFax)
                        functie.Fax = false;
                    else
                        functie.Fax = faxCheckBox.IsChecked == true;

                    functie.Startdatum = startdatumDatePicker.SelectedDate;
                    if (einddatumDatePicker.SelectedDate != startdatumDatePicker.SelectedDate)
                        functie.Einddatum = einddatumDatePicker.SelectedDate;
                    functie.Actief = actiefCheckBox2.IsChecked == true;
                    functie.Aangemaakt = DateTime.Now;
                    functie.Aangemaakt_door = currentUser.Naam;
                    if (functieTypeComboBox.SelectedIndex > -1)
                        functie.Type = functieTypeComboBox.SelectionBoxItem.ToString();

                    return functie;

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het verzamelen van de functie gegevens!" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    return null;
                }
            }
            else
                return null;
        }

        private bool IsFunctieFormValid()
        {
            try
            {
                Boolean valid = true;
                string errorMessage = "";
                if (currentContact != null)
                {
                    if (currentContact.Voornaam == "" || currentContact.Voornaam == null)
                    {
                        errorMessage += "Geen contact gekozen!\n";
                        valid = false;
                    }
                }
                else
                {
                    errorMessage += "Geen contact gekozen!\n";
                    valid = false;
                }
                if (currentBedrijf != null)
                {
                    if (currentBedrijf.Naam == "" || currentBedrijf.Naam == null)
                    {
                        errorMessage += "Geen bedrijf gekozen!\n";
                        valid = false;
                    }
                }
                else
                {
                    errorMessage += "Geen bedrijf gekozen!\n";
                    valid = false;
                }

                if (niveau1ComboBox1.SelectedIndex == -1)
                {
                    errorMessage += "Geen niveau 1 gekozen!\n";
                    valid = false;
                }
                if (functieTypeComboBox.SelectedIndex <= 0)
                {
                    errorMessage += "Er is geen type gekozen!\n";
                    valid = false;
                }
                if (functieAttesttypeComboBox.SelectedIndex <= 0)
                {
                    errorMessage += "Er is geen attesttype gekozen!\n";
                    valid = false;
                }

                if (startdatumDatePicker.SelectedDate == null)
                {
                    errorMessage += "De startdatum is niet ingevuld!\n";
                    valid = false;
                }

                if (!valid)
                    ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

                return valid;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij de validatie van de functie!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                return false;
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true;
            }
            catch (Exception)
            {
                Close();
            }
        }


        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Functies myFunctie = GetFunctieFromForm();
            if (myFunctie != null)
            {
                functielijst.Add(myFunctie);
                //functiesDataGrid.ItemsSource = functielijst;
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            bool isSaved = false;
            if (functiesTab.Header.ToString() == "Spreker")
            {
                try
                {
                    Spreker mySpreker = GetSprekerFromForm();
                    if (currentSpreker == null)
                        currentSpreker = mySpreker;
                    if (mySpreker != null)
                    {
                        SprekerServices sprekerservice = new SprekerServices();
                        mySpreker.Contact = null;
                        mySpreker.Bedrijf = null;
                        mySpreker.Niveau1 = null;
                        mySpreker.Niveau2 = null;
                        mySpreker.Niveau3 = null;
                        mySpreker.Erkenning = null;
                        if (currentSpreker.ID != 0)
                        {
                            mySpreker.ID = currentSpreker.ID;
                            mySpreker.Modified = currentSpreker.Modified;
                            mySpreker.Gewijzigd = DateTime.Now;
                            mySpreker.Gewijzigd_door = currentUser.Naam;
                            try
                            {
                                if ((mySpreker.Einddatum != null && mySpreker.Einddatum < DateTime.Today) || mySpreker.Actief == false)
                                {
                                    List<Sessie_Spreker> sessiesprekers = new SessieSprekerServices().GetSessiesSprekerBySprekerID(mySpreker.ID);
                                    if (sessiesprekers.Count > 0)
                                    {
                                        SprekerConflicten SC = new SprekerConflicten(currentUser, sessiesprekers);
                                        if (SC.ShowDialog() == true)
                                        {
                                            Spreker mySpeaker = sprekerservice.SaveSprekerWijzigingen(mySpreker);
                                            CheckPhoneNumbersForSpeaker(mySpeaker);
                                            //_selectedSpreker = mySpreker;
                                            IsChanged = false;
                                            isSaved = true;
                                            ConfocusMessageBox.Show("Spreker opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                        }
                                        else
                                        {
                                            ConfocusMessageBox.Show("Geen wijzigingen opgeslagen!", ConfocusMessageBox.Kleuren.Rood);
                                            isSaved = false;
                                        }
                                    }
                                    else
                                    {
                                        Spreker mySpeaker = sprekerservice.SaveSprekerWijzigingen(mySpreker);
                                        CheckPhoneNumbersForSpeaker(mySpeaker);
                                        //_selectedSpreker = mySpreker;
                                        IsChanged = false;
                                        isSaved = true;
                                        ConfocusMessageBox.Show("Spreker opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                    }
                                }
                                else
                                {
                                    Spreker mySpeaker = sprekerservice.SaveSprekerWijzigingen(mySpreker);
                                    CheckPhoneNumbersForSpeaker(mySpeaker);
                                    //_selectedSpreker = mySpreker;
                                    IsChanged = false;
                                    isSaved = true;
                                    ConfocusMessageBox.Show("Spreker opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                }

                            }
                            catch (Exception ex)
                            {

                                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                        else
                        {
                            mySpreker.Actief = true;
                            mySpreker.Aangemaakt = DateTime.Now;
                            mySpreker.Aangemaakt_door = currentUser.Naam;
                            try
                            {
                                Spreker savedSpreker = sprekerservice.SaveSpreker(mySpreker);
                                CheckPhoneNumbersForSpeaker(savedSpreker);
                                ConfocusMessageBox.Show("Spreker opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                //_selectedFunctie = savedSpreker;
                                currentSpreker = savedSpreker;
                                SetSprekerToForm();
                                IsChanged = false;
                                isSaved = true;
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Equals(ErrorMessages.SprekerBestaatReeds))
                                {
                                    if (MessageBox.Show("De spreker bestaat reeds maar is niet meer actief.\nWilt u deze terug activeren?", "Spreker terug activeren?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                    {
                                        mySpreker.Actief = true;
                                        mySpreker.Einddatum = null;
                                        currentSpreker = sprekerservice.ActivateFunction(mySpreker);
                                        ConfocusMessageBox.Show("Spreker opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                        SetSprekerToForm();
                                        IsChanged = false;
                                        isSaved = true;
                                    }
                                }
                                else
                                    ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                    }
                    newButton.Visibility = System.Windows.Visibility.Visible;
                    GetSprekerSessies();
                    IsChanged = !isSaved;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                try
                {
                    Functies myFunctie = GetFunctieFromForm();

                    if (myFunctie != null)
                    {
                        FunctieServices functieService = new FunctieServices();
                        List<Functies> savedFuncties = new List<Functies>();
                        myFunctie.Contact = null;
                        myFunctie.Bedrijf = null;
                        myFunctie.Niveau1 = null;
                        myFunctie.Niveau2 = null;
                        myFunctie.Niveau3 = null;
                        myFunctie.Erkenning = null;
                        if (currentFunctie != null && currentFunctie.ID != 0)
                        {
                            myFunctie.ID = currentFunctie.ID;
                            myFunctie.Modified = currentFunctie.Modified;
                            myFunctie.Gewijzigd = DateTime.Now;
                            myFunctie.Gewijzigd_door = currentUser.Naam;
                            try
                            {
                                _selectedFunctie = functieService.SaveFunctieWijzigingen(myFunctie);                                
                                CheckPhoneNumbersForFunctions(_selectedFunctie);
                                CheckIfContactIsCustomer(_selectedFunctie);
                                _selectedFunctie = DBHelper.CheckFaxCollegesSettings(_selectedFunctie);
                                DBHelper.CheckAantalPuntenBijInschrijvingen(_selectedFunctie);
                                IsChanged = false;
                                ConfocusMessageBox.Show("Functie opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                            }
                            catch (Exception ex)
                            {
                                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                        else
                        {
                            myFunctie.Actief = true;
                            myFunctie.Aangemaakt = DateTime.Now;
                            myFunctie.Aangemaakt_door = currentUser.Naam;
                            try
                            {
                                Functies savedFunctie = functieService.SaveFunctie(myFunctie);
                                if (savedFunctie != null)
                                {
                                    CheckPhoneNumbersForFunctions(savedFunctie);
                                    savedFunctie = DBHelper.CheckFaxCollegesSettings(savedFunctie);
                                    ConfocusMessageBox.Show("Functie opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                    IsChanged = false;
                                    isSaved = true;
                                    _selectedFunctie = savedFunctie;
                                    _selectedFunctie = DBHelper.CheckFaxCollegesSettings(_selectedFunctie);
                                    currentFunctie = savedFunctie;
                                    CheckIfContactIsCustomer(savedFunctie);
                                    SetFunctieToForm();
                                    //DBHelper.CheckAantalPuntenBijInschrijvingen(savedFunctie);
                                }
                                else
                                    ConfocusMessageBox.Show("Fout bij het opslaan!", ConfocusMessageBox.Kleuren.Rood);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Equals(ErrorMessages.FunctieBestaatReeds))
                                {
                                    Functies excistingFunction = functieService.GetExcistingFunction(myFunctie);
                                    if (excistingFunction.Actief == false)
                                    {
                                        if (MessageBox.Show("De functie bestaat reeds maar is niet meer actief.\nWilt u deze terug activeren?", "Functie terug activeren?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                        {
                                            myFunctie.Actief = true;
                                            myFunctie.Einddatum = null;
                                            currentFunctie = functieService.ActivateFunction(myFunctie);
                                            ConfocusMessageBox.Show("Functie opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
                                            CheckPhoneNumbersForFunctions(currentFunctie);
                                            _selectedFunctie = currentFunctie;
                                            _selectedFunctie = DBHelper.CheckFaxCollegesSettings(_selectedFunctie);
                                            CheckIfContactIsCustomer(currentFunctie);
                                            //DBHelper.CheckAantalPuntenBijInschrijvingen(currentFunctie);
                                            SetFunctieToForm();
                                            IsChanged = false;
                                            isSaved = true;
                                        }
                                    }
                                    else
                                    {
                                        if (MessageBox.Show("De functie bestaat reeds! Wilt u deze inladen?", "Functie bestaat reeds", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                                        {
                                            currentFunctie = excistingFunction;
                                            _selectedFunctie = currentFunctie;
                                            SetFunctieToForm();
                                            IsChanged = false;
                                            isSaved = true;
                                        }
                                    }
                                }
                                else
                                    ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                            }
                        }
                    }
                    newButton.Visibility = System.Windows.Visibility.Visible;
                    GetFunctieSessies();
                    IsChanged = false;
                    isSaved = true;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            SetBedrijfsInfo();
            SetContactInfo();
            IsChanged = !isSaved;
            SetButtons();
            IsChanged = !isSaved;
        }

        //private Functies CheckFaxCollegesSettings(Functies f)
        //{
        //    FunctieServices fService = new FunctieServices();
        //    List<Functies> functions = fService.GetFunctiesInBedrijf(f.Bedrijf_ID);
        //    foreach (Functies func in functions)
        //    {
        //        if (func.Fax == false)
        //        {
        //            f.Fax = false;
        //            f = fService.SaveFunctieWijzigingen(f);
        //            break;
        //        }
        //    }
        //    return f;
        //}

        private void CheckPhoneNumbersForSpeaker(Spreker spreker)
        {
            try
            {
                if (spreker != null)
                {
                    SprekerServices sService = new SprekerServices();
                    List<Spreker> mySpekers = sService.GetSprekersByContactBedrijf(spreker.Contact_ID, (decimal)spreker.Bedrijf_ID);
                    foreach (Spreker sprek in mySpekers)
                    {
                        bool gewijzigd = false;
                        if (sprek.Telefoon != sprek.Telefoon)
                        {
                            sprek.Telefoon = sprek.Telefoon;
                            gewijzigd = true;
                        }
                        if (sprek.Mobiel != sprek.Mobiel)
                        {
                            sprek.Mobiel = sprek.Mobiel;
                            gewijzigd = true;
                        }
                        if (gewijzigd)
                        {
                            sprek.Gewijzigd = DateTime.Now;
                            sprek.Gewijzigd_door = currentUser.Naam;
                            sService.SaveSprekerWijzigingen(sprek);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void CheckPhoneNumbersForFunctions(Functies functie)
        {
            try
            {
                if (functie != null)
                {
                    FunctieServices fService = new FunctieServices();
                    List<Functies> myfuncties = fService.GetFunctiesByContactBedrijf(functie.Contact_ID, functie.Bedrijf_ID);
                    foreach (Functies func in myfuncties)
                    {
                        bool gewijzigd = false;
                        if (!string.IsNullOrEmpty(functie.Telefoon))
                        {
                            if (func.Telefoon != functie.Telefoon)
                            {
                                func.Telefoon = functie.Telefoon;
                                gewijzigd = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(functie.Mobiel))
                        {
                            if (func.Mobiel != functie.Mobiel)
                            {
                                func.Mobiel = functie.Mobiel;
                                gewijzigd = true;
                            }
                        }
                        if (gewijzigd)
                        {
                            func.Gewijzigd = DateTime.Now;
                            func.Gewijzigd_door = currentUser.Naam;
                            fService.SaveFunctieWijzigingen(func);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Foout bij het controleren van de telefoonnummers voor dezelfde functies binnen dit bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void CheckIfContactIsCustomer(Functies functie)
        {
            try
            {
                if (functie != null)
                {
                    if (functie.Type == "Klant")
                    {
                        FunctieServices fService = new FunctieServices();
                        List<Functies> functies = fService.GetFunctieByContact(functie.Contact_ID);
                        foreach (Functies func in functies)
                        {
                            if (func.Type != "Klant")
                            {
                                func.Type = "Klant";
                                func.Gewijzigd = DateTime.Today;
                                func.Gewijzigd_door = currentUser.Naam;
                                fService.SaveFunctieWijzigingen(func);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het naar Klant zetten van de functies voor dit contact!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private Spreker GetSprekerFromForm()
        {
            try
            {
                if (IsSprekerValid())
                {
                    Spreker spreker = new Spreker();
                    spreker.ID = 0;
                    spreker.Tarief = tariefTextBox.Text;
                    spreker.Contact_ID = currentContact.ID;
                    spreker.Bedrijf_ID = currentBedrijf.ID;
                    if (niveau1ComboBox1.SelectedIndex > -1)
                    {
                        spreker.Niveau1_ID = ((Niveau1)niveau1ComboBox1.SelectedItem).ID;
                    }
                    if (niveau2ComboBox2.SelectedIndex > 0)
                    {
                        spreker.Niveau2_ID = ((Niveau2)niveau2ComboBox2.SelectedItem).ID;
                    }
                    if (niveau3ComboBox3.SelectedIndex > 0)
                    {
                        spreker.Niveau3_ID = ((Niveau3)niveau3ComboBox3.SelectedItem).ID;
                    }
                    if (functieAttesttypeComboBox.SelectedIndex > 0)
                    {
                        spreker.Attesttype = functieAttesttypeComboBox.Text;
                        spreker.AttestType_ID = ((AttestType)functieAttesttypeComboBox.SelectedItem).ID;
                    }
                    else
                        spreker.Attesttype = "";
                    spreker.Erkenningsnummer = erkenningTextBox1.Text;
                    if (WaaderingComboBox.Text != "")
                        spreker.Waardering = Int32.Parse(WaaderingComboBox.Text);
                    spreker.CV = CvTextbox.Text;
                    spreker.Telefoon = telefoonTextBox1.Text;
                    spreker.Mobiel = mobielTextBox.Text;
                    spreker.Email = emailTextBox1.Text;
                    spreker.Actief = actiefCheckBox2.IsChecked == true;
                    spreker.Oorspronkelijke_Titel = oorspronkelijkeTextBox.Text;
                    if (startdatumDatePicker.SelectedDate != null)
                        spreker.Startdatum = startdatumDatePicker.SelectedDate;
                    if (einddatumDatePicker.SelectedDate != null)
                        spreker.Einddatum = einddatumDatePicker.SelectedDate;
                    spreker.Aangemaakt = DateTime.Now;
                    spreker.Aangemaakt_door = currentUser.Naam;
                    if (functieTypeComboBox.SelectedIndex > -1)
                    {
                        if (functieTypeComboBox.Text == "Prospect" || functieTypeComboBox.Text == "Klant")
                            spreker.Type = "Spreker";
                        else
                            spreker.Type = functieTypeComboBox.Text;
                    }

                    return spreker;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool IsSprekerValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            if (currentContact != null)
            {
                if (currentContact.ID == 0)
                {
                    errorMessage += "Er is geen contact gekozen!\n";
                    valid = false;
                }
            }
            else
            {
                errorMessage += "Er is geen contact gekozen!\n";
                valid = false;
            }
            if (currentBedrijf != null)
            {
                if (currentBedrijf.ID == 0)
                {
                    errorMessage += "Er is geen bedrijf gekozen!\n";
                    valid = false;
                }
            }
            else
            {
                errorMessage += "Er is geen bedrijf gekozen!\n";
                valid = false;
            }
            if (niveau1ComboBox1.SelectedIndex == -1)
            {
                errorMessage += "Er is geen Niveau 1 gekozen!\n";
                valid = false;
            }
            if (WaaderingComboBox.SelectedIndex > -1)
            {
                int waardering = 0;
                if (!Int32.TryParse(WaaderingComboBox.Text, out waardering))
                {
                    errorMessage += "Bij waardering is geen getal ingevuld!\n";
                    valid = false;
                }
            }
            if (functieTypeComboBox.SelectedIndex == -1)
            {
                errorMessage += "Er is geen type gekozen!\n";
                valid = false;
            }
            if (functieAttesttypeComboBox.SelectedIndex == -1)
            {
                errorMessage += "Er is geen attesttype gekozen!\n";
                valid = false;
            }

            if (startdatumDatePicker.SelectedDate == null)
            {
                errorMessage += "De startdatum is niet ingevuld!\n";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void voornaamTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsChanged)
                CheckOfContactBestaat();
        }

        private void CheckOfContactBestaat()
        {
            if (achternaamTextBox.Text != "" && voornaamTextBox.Text != "")
            {
                FunctieServices service = new FunctieServices();
                ContactZoekTerm namen = new ContactZoekTerm();
                namen.Achternaam = achternaamTextBox.Text;
                namen.Voornaam = voornaamTextBox.Text;
                List<Functies> bestaande = service.GetFunctieByContactName(namen);
                if (bestaande.Count > 0)
                {
                    BestaandContactWindow BW = new BestaandContactWindow(bestaande);
                    if (BW.ShowDialog() == true)
                    {
                        thisContact = new ContactServices().GetContactByID(BW.myContact.Contact_ID);
                        currentContact = thisContact;
                        BindContactToForm();
                        IsChanged = false;
                        SetButtons();
                    }
                }
            }
        }

        private void achternaamTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsChanged)
                CheckOfContactBestaat();
        }

        private void naamTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsChanged)
                CheckOfBedrijfBestaat();
        }

        private void CheckOfBedrijfBestaat()
        {
            if (naamTextBox.Text != "")
            {
                BedrijfServices service = new BedrijfServices();
                List<Bedrijf> bestaande = service.GetBedrijfByName(naamTextBox.Text);
                if (bestaande.Count > 0)
                {
                    BestaandBedrijfWindow BB = new BestaandBedrijfWindow(bestaande);
                    if (BB.ShowDialog() == true)
                    {
                        currentBedrijf = BB.myBedrijf;
                        BindBedrijfToForm();
                        IsChanged = false;
                        SetButtons();
                    }
                }
            }
        }

        private void typeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInit)
            {
                if (functieTypeComboBox.SelectedIndex > 0)
                {
                    IsChanged = true;
                    String type = ((ComboBoxItem)functieTypeComboBox.SelectedItem).Content.ToString();
                    if (type == "Spreker" || type == "Coach")
                    {
                        HideFunctieInputFields();
                        FunctieListBox.Visibility = System.Windows.Visibility.Hidden;
                        SprekerListBox.Visibility = System.Windows.Visibility.Visible;
                        functiesTab.Header = "Spreker";
                        currentFunctie = null;
                        if (currentSpreker == null)
                        {
                            currentSpreker = new Spreker();
                            currentSpreker.ID = 0;
                        }
                        SetBedrijfsInfo();
                        SetContactInfo();

                    }
                    else
                    {
                        HideSprekerInputFields();
                        FunctieListBox.Visibility = System.Windows.Visibility.Visible;
                        SprekerListBox.Visibility = System.Windows.Visibility.Hidden;
                        functiesTab.Header = "Functies";
                        currentSpreker = null;
                        if (currentFunctie == null)
                        {
                            currentFunctie = new Functies();
                            currentFunctie.ID = 0;
                        }
                        SetBedrijfsInfo();
                        SetContactInfo();
                    }
                }
            }
            else
                functieTypeComboBox.Text = "Prospect";
            IsChanged = false;
            SetButtons();

        }

        private void HideSprekerInputFields()
        {
            functiesTab.Header = "Functies";
            functieMailLabel.Visibility = System.Windows.Visibility.Visible;
            mailCheckBox.Visibility = System.Windows.Visibility.Visible;
            functieFaxLabel.Visibility = System.Windows.Visibility.Visible;
            faxCheckBox.Visibility = System.Windows.Visibility.Visible;
            functieStartLabel.Visibility = System.Windows.Visibility.Visible;
            startdatumDatePicker.Visibility = System.Windows.Visibility.Visible;
            tariefLabel.Visibility = System.Windows.Visibility.Hidden;
            tariefTextBox.Visibility = System.Windows.Visibility.Hidden;
            WaaderingLabel.Visibility = System.Windows.Visibility.Hidden;
            WaaderingComboBox.Visibility = System.Windows.Visibility.Hidden;
            CvLabel.Visibility = System.Windows.Visibility.Hidden;
            CvTextbox.Visibility = System.Windows.Visibility.Hidden;
            //OpenCVButton.Visibility = System.Windows.Visibility.Hidden;
            //LaadCVButton.Visibility = System.Windows.Visibility.Hidden;
            FunctieListBox.Visibility = System.Windows.Visibility.Visible;
            SprekerListBox.Visibility = System.Windows.Visibility.Hidden;

        }

        private void HideFunctieInputFields()
        {
            functiesTab.Header = "Spreker";
            functieMailLabel.Visibility = System.Windows.Visibility.Hidden;
            mailCheckBox.Visibility = System.Windows.Visibility.Hidden;
            functieFaxLabel.Visibility = System.Windows.Visibility.Hidden;
            faxCheckBox.Visibility = System.Windows.Visibility.Hidden;
            tariefLabel.Visibility = System.Windows.Visibility.Visible;
            tariefTextBox.Visibility = System.Windows.Visibility.Visible;
            WaaderingLabel.Visibility = System.Windows.Visibility.Visible;
            WaaderingComboBox.Visibility = System.Windows.Visibility.Visible;
            CvLabel.Visibility = System.Windows.Visibility.Visible;
            CvTextbox.Visibility = System.Windows.Visibility.Visible;
            //OpenCVButton.Visibility = System.Windows.Visibility.Visible;
            //LaadCVButton.Visibility = System.Windows.Visibility.Visible;
            FunctieListBox.Visibility = System.Windows.Visibility.Hidden;
            SprekerListBox.Visibility = System.Windows.Visibility.Visible;
        }

        private void OpenCVButton_Click(object sender, RoutedEventArgs e)
        {
            //System.Diagnostics.Process.Start("https://confocusbvba.sharepoint.com/cck/_layouts/15/start.aspx#/Gedeelde%20%20documenten/Forms/AllItems.aspx?RootFolder=%2Fcck%2FGedeelde%20%20documenten%2FCV%20%2D%20Spreker&FolderCTID=0x012000AAC3CED665DBCF408B736BCC81C7FFB2&View=%7BCF22F2E5%2D0182%2D4B10%2D863A%2D3B963CCBEF23%7D");
            if (CvTextbox.Text != "")
            {
                try
                {
                    string fileToOpen = CvTextbox.Text;
                    System.Diagnostics.Process.Start(fileToOpen);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Kan het bestand niet openen!\nControleer als de URL bestaat.", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void LaadCVButton_Click(object sender, RoutedEventArgs e)
        {
            string fileName = "";
            string targetPath = @"C:";
            OpenFileDialog OpenCV = new OpenFileDialog();
            OpenCV.InitialDirectory = targetPath;
            if (OpenCV.ShowDialog() == true)
            {
                fileName = OpenCV.FileName;
                string[] parts = fileName.Split('\\');
                string doc = parts[parts.Length - 1];
                //itc test
                //string url = @"https://itcbe.sharepoint.com/_layouts/15/WopiFrame.aspx?sourcedoc=DocC/Confocus%20bvba/Software/" + doc + "&action=default";

                //confocus SharePoint
                string url = @"https://confocusbvba.sharepoint.com/cck/_layouts/15/WopiFrame.aspx?sourcedoc=Gedeelde%20%20documenten/CV%5FSprekers/" + doc + "&action=default";

                CvTextbox.Text = url;
            }
        }


        private void FunctieListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (FunctieListBox.SelectedIndex > -1)
            {
                Functies myFunctie = (Functies)FunctieListBox.SelectedItem;
                currentFunctie = myFunctie;
                _selectedFunctie = currentFunctie;
                currentBedrijf = myFunctie.Bedrijf;
                currentContact = myFunctie.Contact;
                BindContactToForm();
                BindBedrijfToForm();
                SetFunctieToForm();
                newButton.Visibility = System.Windows.Visibility.Visible;
                IsChanged = false;
            }
        }

        private void SetFunctieToForm()
        {
            if (currentFunctie != null)
            {
                try
                {
                    functieIDLabel.Content = currentFunctie.ID.ToString();
                    niveau1ComboBox1.SelectedIndex = -1;
                    einddatumDatePicker.SelectedDate = null;
                    FunctieServices service = new FunctieServices();
                    var fullfunction = service.GetFunctieByID(currentFunctie.ID);
                    if (fullfunction != null)
                    {
                        currentFunctie = fullfunction;
                        if (currentFunctie.Niveau1 != null)
                        {
                            niveau1ComboBox1.Text = currentFunctie.Niveau1.Naam;
                        }

                        if (currentFunctie.Niveau2 != null)
                        {
                            niveau2ComboBox2.Text = currentFunctie.Niveau2.Naam;
                        }

                        if (currentFunctie.Niveau3 != null)
                        {
                            niveau3ComboBox3.Text = currentFunctie.Niveau3.Naam;
                        }
                        else
                            niveau3ComboBox3.SelectedIndex = -1;
                        //niveau3ComboBox3.ItemsSource = new List<Niveau3>();

                        if (currentFunctie.Erkenningsnummer != "")
                        {
                            erkenningTextBox1.Text = currentFunctie.Erkenningsnummer;
                        }
                        else
                            erkenningTextBox1.Text = "";
                        if (string.IsNullOrEmpty(currentFunctie.Oorspronkelijke_Titel))
                            oorspronkelijkeTextBox.Text = "";
                        else
                            oorspronkelijkeTextBox.Text = currentFunctie.Oorspronkelijke_Titel;
                        if (currentFunctie.AttestType1 != null)// && currentFunctie.Attesttype != ""
                            functieAttesttypeComboBox.Text = currentFunctie.AttestType1.Naam;
                        else
                            functieAttesttypeComboBox.SelectedIndex = 0;
                        emailTextBox1.Text = currentFunctie.Email;
                        telefoonTextBox1.Text = currentFunctie.Telefoon;
                        mobielTextBox.Text = currentFunctie.Mobiel;
                        startdatumDatePicker.SelectedDate = currentFunctie.Startdatum;
                        if (currentFunctie.Einddatum == null)
                            einddatumDatePicker.SelectedDate = null;
                        else
                            einddatumDatePicker.SelectedDate = currentFunctie.Einddatum;
                        mailCheckBox.IsChecked = currentFunctie.Mail == true;
                        faxCheckBox.IsChecked = currentFunctie.Fax == true;
                        actiefCheckBox2.IsChecked = currentFunctie.Actief == true;
                        if (!string.IsNullOrEmpty(currentFunctie.Type))
                            functieTypeComboBox.Text = currentFunctie.Type;
                        else
                            functieTypeComboBox.SelectedIndex = 0;
                        SetContactInfo();
                        SetBedrijfsInfo();
                    }
                    else if (currentFunctie != null)
                    {
                        if (currentFunctie.Niveau1 != null)
                        {
                            niveau1ComboBox1.Text = currentFunctie.Niveau1.Naam;
                        }

                        if (currentFunctie.Niveau2 != null)
                        {
                            niveau2ComboBox2.Text = currentFunctie.Niveau2.Naam;
                        }

                        if (currentFunctie.Niveau3 != null)
                        {
                            niveau3ComboBox3.Text = currentFunctie.Niveau3.Naam;
                        }
                        else
                            niveau3ComboBox3.SelectedIndex = -1;

                        if (currentFunctie.Erkenningsnummer != "")
                        {
                            erkenningTextBox1.Text = currentFunctie.Erkenningsnummer;
                        }
                        else
                            erkenningTextBox1.Text = "";
                        if (string.IsNullOrEmpty(currentFunctie.Oorspronkelijke_Titel))
                            oorspronkelijkeTextBox.Text = "";
                        else
                            oorspronkelijkeTextBox.Text = currentFunctie.Oorspronkelijke_Titel;
                        if (currentFunctie.AttestType1 != null)// && currentFunctie.Attesttype != ""
                            functieAttesttypeComboBox.Text = currentFunctie.AttestType1.Naam;
                        else
                            functieAttesttypeComboBox.SelectedIndex = 0;
                        emailTextBox1.Text = currentFunctie.Email;
                        telefoonTextBox1.Text = currentFunctie.Telefoon;
                        mobielTextBox.Text = currentFunctie.Mobiel;
                        startdatumDatePicker.SelectedDate = currentFunctie.Startdatum;
                        einddatumDatePicker.SelectedDate = currentFunctie.Einddatum;
                        mailCheckBox.IsChecked = currentFunctie.Mail == true;
                        faxCheckBox.IsChecked = currentFunctie.Fax == true;
                        actiefCheckBox2.IsChecked = currentFunctie.Actief == true;
                        if (!string.IsNullOrEmpty(currentFunctie.Type))
                            functieTypeComboBox.Text = currentFunctie.Type;
                        else
                            functieTypeComboBox.SelectedIndex = 0;

                    }
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het invullen van de Functie!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                GetFunctieSessies();
            }
        }

        private void GetFunctieSessies()
        {
            try
            {
                if (functiesTab.Header.ToString() == "Functies")
                {
                    if (currentContact != null)
                    {
                        if (currentContact.ID != 0)
                        {
                            InschrijvingenService iService = new InschrijvingenService();
                            //List<Sessie> sessies = iService.GetSessiesByFunctie(currentContact.ID);
                            List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = iService.GetInschrijvingenByContact(currentContact.ID);
                            //sessieViewSource.Source = sessies;
                            inschrijvingenViewSource.Source = inschrijvingen;
                            inschrijvingenDataGrid.Visibility = Visibility.Visible;
                            sessie_SprekerDataGrid.Visibility = Visibility.Hidden;
                        }
                    }
                }
                else if (functiesTab.Header.ToString() == "Spreker")
                {
                    if (currentContact.ID != 0)
                    {
                        SessieSprekerServices sService = new SessieSprekerServices();
                        //List<Sessie> sessies = iService.GetSessiesByFunctie(currentContact.ID);
                        List<ConfocusDBLibrary.Sessie_Spreker> sprekers = sService.GetSessieSprekersByContact(currentContact.ID);
                        //sessieViewSource.Source = sessies;
                        sessie_SprekerViewSource.Source = sprekers;
                        inschrijvingenDataGrid.Visibility = Visibility.Hidden;
                        sessie_SprekerDataGrid.Visibility = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het ophalen van de sessies!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }

        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            ClearFunctieOrSpreker();
        }

        private void ClearFunctieOrSpreker()
        {
            currentFunctie = new Functies();
            currentFunctie.ID = 0;
            currentSpreker = new Spreker();
            currentSpreker.ID = 0;
            functieIDLabel.Content = "";
            newButton.Visibility = System.Windows.Visibility.Hidden;
            niveau1ComboBox1.SelectedIndex = -1;
            niveau2ComboBox2.ItemsSource = new List<Niveau2>();
            niveau3ComboBox3.ItemsSource = new List<Niveau3>();
            functieAttesttypeComboBox.SelectedIndex = -1;
            oorspronkelijkeTextBox.Text = "";

            erkenningTextBox1.Text = "";
            if (functiesTab.Header.ToString() == "Spreker")
            {
                tariefTextBox.Text = "";
                //onderwerpTextbox.Text = "";
                WaaderingComboBox.SelectedIndex = -1;
                //CvTextbox.Text = "";
            }
            else
            {
                emailTextBox1.Text = "";
                telefoonTextBox1.Text = "";
                mobielTextBox.Text = "";
                startdatumDatePicker.SelectedDate = null;
                mailCheckBox.IsChecked = true;
                faxCheckBox.IsChecked = true;
                actiefCheckBox2.IsChecked = true;
                functieTypeComboBox.Text = "Prospect";
            }
        }

        private void SprekerListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (SprekerListBox.SelectedIndex > -1)
            {
                Spreker mySpreker = (Spreker)SprekerListBox.SelectedItem;
                currentSpreker = mySpreker;
                SetSprekerToForm();
                newButton.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void SetSprekerToForm()
        {
            try
            {
                functieIDLabel.Content = currentSpreker.ID.ToString();
                SprekerServices service = new SprekerServices();
                Spreker CompleteSpreker = service.GetSprekerById(currentSpreker.ID);
                if (CompleteSpreker != null)
                {
                    niveau1ComboBox1.Text = CompleteSpreker.Niveau1.Naam;
                    if (CompleteSpreker.Niveau2 != null)
                    {
                        niveau2ComboBox2.Text = CompleteSpreker.Niveau2.Naam;
                    }

                    if (CompleteSpreker.Niveau3 != null)
                    {
                        niveau3ComboBox3.Text = CompleteSpreker.Niveau3.Naam;
                    }
                    else
                        niveau3ComboBox3.ItemsSource = new List<Niveau3>();

                    //if (CompleteSpreker.Erkenning != null)
                    //{
                    erkenningTextBox1.Text = CompleteSpreker.Erkenningsnummer;
                    //}
                    //else
                    //    erkenningTextBox1.SelectedIndex = -1;

                    if (CompleteSpreker.Tarief != null)
                    {
                        tariefTextBox.Text = CompleteSpreker.Tarief.ToString();
                    }
                    else
                        tariefTextBox.Text = "";

                    if (CompleteSpreker.AttestType_ID != null)// && CompleteSpreker.Attesttype != ""
                    {
                        //Attesttype_Services aService = new Attesttype_Services();

                        functieAttesttypeComboBox.Text = CompleteSpreker.AttestType1.Naam;

                    }
                    else
                        functieAttesttypeComboBox.SelectedIndex = 0;

                    //onderwerpTextbox.Text = CompleteSpreker.Onderwerp;
                    CvTextbox.Text = CompleteSpreker.CV;
                    oorspronkelijkeTextBox.Text = CompleteSpreker.Oorspronkelijke_Titel;
                    if (CompleteSpreker.Waardering != null)
                    {
                        WaaderingComboBox.Text = CompleteSpreker.Waardering.ToString();
                    }
                    if (!string.IsNullOrEmpty(CompleteSpreker.Type))
                        functieTypeComboBox.Text = CompleteSpreker.Type;
                    else
                        functieTypeComboBox.SelectedIndex = 2;
                    startdatumDatePicker.SelectedDate = CompleteSpreker.Startdatum;
                    if (CompleteSpreker.Einddatum != null)
                        einddatumDatePicker.SelectedDate = (DateTime)CompleteSpreker.Einddatum;
                    else
                        einddatumDatePicker.SelectedDate = null;
                    actiefCheckBox2.IsChecked = CompleteSpreker.Actief == true;
                    emailTextBox1.Text = CompleteSpreker.Email;
                    telefoonTextBox1.Text = CompleteSpreker.Telefoon;
                    mobielTextBox.Text = CompleteSpreker.Mobiel;
                    currentSpreker = CompleteSpreker;
                    SetBedrijfsInfo();
                    SetContactInfo();
                    GetSprekerSessies();
                }
                GetFunctieSessies();
                IsChanged = false;

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het invullen van de spreker!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void GetSprekerSessies()
        {
            try
            {
                SessieSprekerServices sService = new SessieSprekerServices();
                if (currentContact != null)
                {
                    if (currentContact.ID != 0)
                    {
                        List<Sessie> sessies = sService.GetSessiesBySprekerID(currentContact.ID);
                        sessieViewSource.Source = sessies;
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void PasteCVButton_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsData(DataFormats.Text))
            {
                CvTextbox.Text = Clipboard.GetData(DataFormats.Text) as string;
                Clipboard.Clear();
            }
        }

        private void CvTextbox_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void CvTextbox_Drop(object sender, DragEventArgs e)
        {
            //try
            //{
            //    if (e.Data.GetDataPresent(DataFormats.FileDrop))
            //    {
            //        string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            //        if (Directory.Exists(files[0]))
            //        {
            //            this.CvTextbox.Text = files[0];
            //        }
            //    }


            //    //String a = e.Data.GetData(DataFormats.Text).ToString();
            //    //if (a != "")
            //    //{
            //    //    CvTextbox.Text = a;
            //    //}
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error in DragDrop function: " + ex.Message);
            //}

        }

        private void FileShowTextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            bool isCorrect = true;

            if (e.Data.GetDataPresent(DataFormats.FileDrop, true) == true)
            {
                string[] filenames = (string[])e.Data.GetData(DataFormats.FileDrop, true);
                foreach (string filename in filenames)
                {
                    if (File.Exists(filename) == false)
                    {
                        isCorrect = false;
                        break;
                    }
                    FileInfo info = new FileInfo(filename);
                    //if (info.Extension != ".txt")
                    //{
                    //    isCorrect = false;
                    //    break;
                    //}
                }
            }
            if (isCorrect == true)
                e.Effects = DragDropEffects.All;
            else
                e.Effects = DragDropEffects.None;
            e.Handled = true;

        }

        private void FileShowTextBox_PreviewDrop(object sender, DragEventArgs e)
        {
            string[] filenames = (string[])e.Data.GetData(DataFormats.FileDrop, true);
            if (filenames != null)
            {
                foreach (string filename in filenames)
                    CvTextbox.Text += filename;
            }
            e.Handled = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (IsChanged)
                {
                    if (MessageBox.Show("Er zijn nog onopgeslagen wijzigingen!\nWil U toch afsluiten?", "Wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        DialogResult = true;
                    else
                        e.Cancel = true;
                }
                else if (currentFunctie != null && !currentFunctie.IsEmpty())
                {
                    if (currentFunctie.ID == 0)
                    {
                        if (MessageBox.Show("Er zijn nog onopgeslagen wijzigingen!\nWil U toch afsluiten?", "Wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            DialogResult = true;
                        else
                            e.Cancel = true;
                    }
                    else
                        DialogResult = true;
                }
                else if (currentSpreker != null && !currentSpreker.IsEmpty())
                {
                    if (currentSpreker.ID == 0)
                    {
                        if (MessageBox.Show("Er zijn nog onopgeslagen wijzigingen!\nWil U toch afsluiten?", "Wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            DialogResult = true;
                        else
                            e.Cancel = true;
                    }
                    else
                        DialogResult = true;
                }
                else
                    DialogResult = true;

            }
            catch (Exception)
            {
                try
                {
                    DialogResult = true;
                }
                catch (Exception)
                {
                    this.Close();
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            IsChanged = true;
            SetButtons();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.Name == "postcodeComboBox" && box.SelectedIndex > -1)
            {
                try
                {
                    Postcodes postcode = (Postcodes)box.SelectedItem;
                    provincieComboBox.Text = postcode.Provincie;
                    landTextBox.Text = postcode.Land;

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het invullen van de postcode!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            IsChanged = true;
            SetButtons();
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            IsChanged = true;
            SetButtons();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox check = (CheckBox)sender;
            switch (check.Name)
            {
                case "actiefCheckBox1":
                    if (check.IsChecked == false)
                    {
                        decimal bedrijfsid = currentBedrijf.ID;
                        BedrijfConflictenWindow BC = new BedrijfConflictenWindow(currentUser, bedrijfsid);
                        if (BC.ShowDialog() == true)
                        {
                            if (functiesTab.Header.ToString() == "Functies")
                            {
                                if (currentFunctie != null)
                                {
                                    SaveBedrijf();
                                    FunctieServices fService = new FunctieServices();
                                    Functies functie = fService.GetFunctieByID(currentFunctie.ID);
                                    currentFunctie = functie;
                                    currentContact = functie.Contact;
                                    currentBedrijf = functie.Bedrijf;
                                    BindContactToForm();
                                    BindBedrijfToForm();
                                    SetFunctieToForm();
                                    IsChanged = false;
                                }
                            }
                            else
                            {
                                if (currentSpreker != null)
                                {
                                    SaveBedrijf();
                                    SprekerServices sService = new SprekerServices();
                                    Spreker spreker = sService.GetSprekerById(currentSpreker.ID);
                                    currentSpreker = spreker;
                                    currentContact = spreker.Contact;
                                    currentBedrijf = spreker.Bedrijf;
                                    BindContactToForm();
                                    BindBedrijfToForm();
                                    SetSprekerToForm();
                                    IsChanged = false;
                                }
                            }
                        }
                        else
                        {
                            currentBedrijf.Actief = true;
                            actiefCheckBox1.IsChecked = true;
                        }
                    }
                    else
                        IsChanged = true;
                    SetButtons();
                    break;
                case "mailCheckBox":
                    if (check.IsChecked == false)
                    {
                        if (currentFunctie != null || currentFunctie.ID != 0)
                        {
                            if (MessageBox.Show("U staat op punt dit contact uit te schrijven voor mailing. Wil u doorgaan?", "Let op!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            {
                                try
                                {
                                    Functies myFunctie = GetFunctieFromForm();

                                    if (myFunctie != null)
                                    {
                                        FunctieServices functieService = new FunctieServices();
                                        List<Functies> savedFuncties = new List<Functies>();
                                        myFunctie.Contact = null;
                                        myFunctie.Bedrijf = null;
                                        myFunctie.Niveau1 = null;
                                        myFunctie.Niveau2 = null;
                                        myFunctie.Niveau3 = null;
                                        myFunctie.Erkenning = null;
                                        myFunctie.ID = currentFunctie.ID;
                                        myFunctie.Gewijzigd = DateTime.Now;
                                        myFunctie.Gewijzigd_door = currentUser.Naam;
                                        myFunctie.Mail = false;
                                        functieService.SaveFunctieWijzigingen(myFunctie);

                                        List<Functies> functies = new List<Functies>();
                                        functies.Add(myFunctie);
                                        Flexmail.Flexmail flexmail = new Flexmail.Flexmail();
                                        flexmail.ImportBlackList(functies);
                                        IsChanged = false;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                                }
                            }
                            else
                                check.IsChecked = true;
                        }
                    }
                    break;
                case "faxCheckBox":
                    IsChanged = true;
                    if (check.IsChecked == false)
                    {
                        if (currentFunctie.Bedrijf != null && !string.IsNullOrEmpty(currentFunctie.Bedrijf.Fax))
                        {
                            //Bedrijf uitschrijven voor fax
                            FunctieServices fService = new FunctieServices();
                            //List<Functies> functies = fService.GetFunctiesInBedrijf(currentFunctie.Bedrijf_ID);
                            List<Functies> functies = fService.GetActiveFunctiesByFaxnumber(currentFunctie.Bedrijf.Fax);
                            foreach (Functies functie in functies)
                            {
                                if (functie.Bedrijf.Fax == currentFunctie.Bedrijf.Fax)
                                {
                                    functie.Fax = false;
                                    functie.Gewijzigd = DateTime.Now;
                                    functie.Gewijzigd_door = currentUser.Naam;
                                    fService.SaveFunctieWijzigingen(functie);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private void SetButtons()
        {
            try
            {
                if (contactTab.IsSelected == true)
                {
                    if (currentContact != null)
                    {
                        if (currentContact.ID == 0)
                            IsChanged = true;
                        ContactopslaanButton.IsEnabled = IsChanged;
                        ContactVolgendeButton.IsEnabled = !IsChanged;
                    }
                }
                else if (bedrijfTab.IsSelected == true)
                {
                    if (currentBedrijf != null)
                    {
                        if (currentBedrijf.ID == 0)
                            IsChanged = true;
                        BedrijfOpslaanButton.IsEnabled = IsChanged;
                        BedrijfVolgendeButton.IsEnabled = !IsChanged;

                    }
                }
                else
                {
                    if (functiesTab != null)
                    {
                        if (functiesTab.Header.ToString() == "Functies")
                        {
                            if (currentFunctie != null)
                            {
                                if (currentFunctie.ID == 0)
                                    IsChanged = true;
                                newButton.IsEnabled = !IsChanged;
                                newKeepContent.IsEnabled = !IsChanged;
                            }
                        }
                        else
                        {
                            if (currentSpreker != null)
                            {
                                if (currentSpreker.ID == 0)
                                    IsChanged = true;
                                newButton.IsEnabled = !IsChanged;
                                newKeepContent.IsEnabled = !IsChanged;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het aanpassen van de knoppen!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void geboortedatumDatePicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete || e.Key == Key.Back)
                geboortedatumDatePicker.Text = "Kies een datum";
            else
                SetDatePicker(sender);
        }

        private void geboortedatumDatePicker_GotFocus(object sender, RoutedEventArgs e)
        {
            SetDatePicker(sender);
        }

        private void SetDatePicker(object sender)
        {
            DatePicker picker = (DatePicker)sender;
            if (picker.Text == "Kies een datum")
            {
                picker.Text = "";
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInit)
            {
                if (e.OriginalSource == contactTabControl)
                {
                    if (IsChanged == true)
                    {

                        e.Handled = true;
                        IsInit = true;
                        contactTabControl.SelectedIndex = _TabIndex;
                        IsInit = false;
                        ConfocusMessageBox.Show("Er zijn nog onopgeslagen wijzigingen!\nSla deze eerst op.", ConfocusMessageBox.Kleuren.Rood);
                    }
                    else
                    {
                        _TabIndex = contactTabControl.SelectedIndex;
                        if (_TabIndex == 0)
                            Dispatcher.BeginInvoke(new Action(() => { voornaamTextBox.Focus(); }), System.Windows.Threading.DispatcherPriority.Render);
                        else if (_TabIndex == 1)
                            Dispatcher.BeginInvoke(new Action(() => { naamTextBox.Focus(); }), System.Windows.Threading.DispatcherPriority.Render);
                        else if (_TabIndex == 2)
                            Dispatcher.BeginInvoke(new Action(() => { niveau1ComboBox1.Focus(); }), System.Windows.Threading.DispatcherPriority.Render);
                    }
                }
            }
            SetButtons();

        }

        private void contactBedrijvenListbox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.SelectedItem is Functies)
            {
                Functies functie = (Functies)box.SelectedItem;
                currentFunctie = functie;
                SetFunctieToForm();
                IsChanged = false;
                bedrijfTab.IsSelected = true;

                if (functiesTab.Header.ToString() == "Functie")
                    GetFunctieSessies();
                else
                    GetSprekerSessies();
            }
        }

        private void contactFunctieListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.SelectedItem != null)
            {
                if (box.SelectedItem is Functies)
                {
                    Functies functie = (Functies)box.SelectedItem;
                    ClearFunctieOrSpreker();
                    currentFunctie = functie;
                    _selectedFunctie = currentFunctie;
                    currentBedrijf = functie.Bedrijf;
                    currentContact = functie.Contact;
                    BindBedrijfToForm();
                    BindContactToForm();
                    SetFunctieToForm();
                    IsChanged = false;
                    GetFunctieSessies();
                }
                else if (box.SelectedItem is Spreker)
                {
                    Spreker spreker = (Spreker)box.SelectedItem;
                    ClearFunctieOrSpreker();
                    currentContact = spreker.Contact;
                    currentBedrijf = spreker.Bedrijf;
                    currentSpreker = spreker;                    
                    BindBedrijfToForm();
                    SetSprekerToForm();
                    IsChanged = false;
                    GetSprekerSessies();
                }

                if (box.Name == "contactBedrijvenListbox")
                    bedrijfTab.IsSelected = true;
                else
                    functiesTab.IsSelected = true;
            }
            IsChanged = false;

        }

        private void EmailTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (!String.IsNullOrEmpty(box.Text))
                if (!Helper.CheckEmail(box.Text))
                    ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }

        private void searchBedrijfButton_Click(object sender, RoutedEventArgs e)
        {
            SearchBedrijven();
        }

        private void SearchBedrijven()
        {
            ContactZoekTerm searchterm = new ContactZoekTerm();
            searchterm.Bedrijfsnaam = DBHelper.Simplify(zoekBedrijfTextBox.Text);
            searchterm.Bedrijfsadres = zoekBedrijfAdresTextBox.Text;
            if (!string.IsNullOrEmpty(searchterm.Bedrijfsnaam) || !string.IsNullOrEmpty(searchterm.Bedrijfsadres))
            {
                BedrijfServices bService = new BedrijfServices();
                List<Bedrijf> bedrijven = bService.GetAvtiveBySearchTerm(searchterm);
                BestaandeBedrijvenListBox.ItemsSource = bedrijven;
                BestaandeBedrijvenListBox.DisplayMemberPath = "NaamEnAdres";
                CompanyResultLabel.Content = bedrijven.Count + " bedrijven gevonden.";
            }
        }

        private void BestaandeBedrijvenListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.SelectedIndex > -1)
            {
                oldCompanyID = currentBedrijf.ID;
                currentBedrijf = (Bedrijf)box.SelectedItem;
                BindBedrijfToForm();

                if (oldCompanyID != 0 && oldCompanyID != currentBedrijf.ID)
                    CreateNewFunctionWithContent();

                SetContactInfo();
                SetBedrijfsInfo();
                IsChanged = false;
                SetButtons();
            }
        }

        private void zoekBedrijfTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                SearchBedrijven();
            }
        }

        private void zoekVoornaamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                searchContact();
            }
        }

        private void searchContact()
        {
            ContactZoekTerm searchterm = new ContactZoekTerm();
            searchterm.Voornaam = DBHelper.Simplify(zoekVoornaamTextBox.Text);
            searchterm.Achternaam = DBHelper.Simplify(zoekAchternaamTextBox.Text);
            if (!string.IsNullOrEmpty(searchterm.Voornaam) || !string.IsNullOrEmpty(searchterm.Achternaam))
            {
                FunctieServices fService = new FunctieServices();
                List<Functies> functies = fService.GetActiveFunctieByContactName(searchterm);
                //ContactServices cService = new ContactServices();
                //List<Contact> contacten = cService.GetBySearchTerm(searchterm);
                BestaandeContactenListBox.ItemsSource = functies;
                BestaandeContactenListBox.DisplayMemberPath = "NaamBedrijfFunctie";
                contactresultLabel.Content = functies.Count + " contacten gevonden.";
            }
        }

        private void searchContactButton_Click(object sender, RoutedEventArgs e)
        {
            searchContact();
        }

        private void BestaandeContactenListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox box = (ListBox)sender;
            if (box.SelectedIndex > -1)
            {
                currentContact = ((Functies)box.SelectedItem).Contact;
                BindContactToForm();
                IsChanged = false;
                SetButtons();
                SetContactInfo();
                SetBedrijfsInfo();
            }
        }

        private void bedrijfNieuwButton_Click(object sender, RoutedEventArgs e)
        {
            ClearBedrijf();
            currentBedrijf = new Bedrijf();
            currentBedrijf.ID = 0;
            currentBedrijf.Actief = true;
            BindBedrijfToForm();
            CreateNewFunctionWithContent();
            IsChanged = true;
        }

        private void contactNieuwButton_Click(object sender, RoutedEventArgs e)
        {
            ClearContact();
            currentContact = new Contact();
            currentContact.ID = 0;
            currentContact.Actief = true;
            BindContactToForm();
            IsChanged = true;
        }

        private void ClearContact()
        {
            aansprekingComboBox.SelectedIndex = -1;
            taalComboBox.SelectedIndex = 0;
            secundaireTaalComboBox.SelectedIndex = 0;
            notitiesTextBox.Text = "";
            geboortedatumDatePicker.SelectedDate = null;
            contactBedrijvenListbox.Items.Clear();
            contactFunctieListBox.Items.Clear();
        }

        private void newKeepContent_Click(object sender, RoutedEventArgs e)
        {
            CreateNewFunctionWithContent();
            IsChanged = true;
            SetButtons();
        }

        private void CreateNewFunctionWithContent()
        {
            functieIDLabel.Content = 0;
            //newButton.Visibility = System.Windows.Visibility.Hidden;

            if (functiesTab.Header.ToString() == "Spreker")
            {
                try
                {
                    currentSpreker = new Spreker();
                    currentSpreker.ID = 0;
                    if (niveau1ComboBox1.SelectedIndex > -1)
                    {
                        currentSpreker.Niveau1_ID = ((Niveau1)niveau1ComboBox1.SelectedItem).ID;
                        currentSpreker.Niveau1 = (Niveau1)niveau1ComboBox1.SelectedItem;
                    }
                    if (niveau2ComboBox2.SelectedIndex > 0)
                    {
                        currentSpreker.Niveau2_ID = ((Niveau2)niveau2ComboBox2.SelectedItem).ID;
                        currentSpreker.Niveau2 = (Niveau2)niveau2ComboBox2.SelectedItem;
                    }
                    if (niveau3ComboBox3.SelectedIndex > 0)
                    {
                        currentSpreker.Niveau3_ID = ((Niveau3)niveau3ComboBox3.SelectedItem).ID;
                        currentSpreker.Niveau3 = (Niveau3)niveau3ComboBox3.SelectedItem;
                    }

                    currentSpreker.Tarief = tariefTextBox.Text;
                    currentSpreker.Email = emailTextBox1.Text;
                    currentSpreker.Telefoon = telefoonTextBox1.Text;
                    currentSpreker.Mobiel = mobielTextBox.Text;
                    if (functieAttesttypeComboBox.SelectedIndex > -1)
                        currentSpreker.AttestType_ID = ((AttestType)functieAttesttypeComboBox.SelectedItem).ID;
                    currentSpreker.Erkenningsnummer = erkenningTextBox1.Text;
                    currentSpreker.Type = functieTypeComboBox.Text;
                    if (startdatumDatePicker.SelectedDate != null)
                        currentSpreker.Startdatum = (DateTime)startdatumDatePicker.SelectedDate;
                    currentSpreker.Oorspronkelijke_Titel = oorspronkelijkeTextBox.Text;

                    //onderwerpTextbox.Text = "";
                    if (WaaderingComboBox.SelectedIndex > -1)
                        currentSpreker.Waardering = decimal.Parse(WaaderingComboBox.Text);
                    currentSpreker.CV = CvTextbox.Text;
                    //CvTextbox.Text = "";
                    SetSprekerToForm();

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het maken van een spreker van een bestaande spreker!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
            {
                try
                {
                    currentFunctie = new Functies();
                    currentFunctie.ID = 0;
                    if (niveau1ComboBox1.SelectedIndex > -1)
                    {
                        currentFunctie.Niveau1_ID = ((Niveau1)niveau1ComboBox1.SelectedItem).ID;
                        currentFunctie.Niveau1 = (Niveau1)niveau1ComboBox1.SelectedItem;
                    }
                    if (niveau2ComboBox2.SelectedIndex > 0)
                    {
                        currentFunctie.Niveau2_ID = ((Niveau2)niveau2ComboBox2.SelectedItem).ID;
                        currentFunctie.Niveau2 = (Niveau2)niveau2ComboBox2.SelectedItem;
                    }
                    if (niveau3ComboBox3.SelectedIndex > 0)
                    {
                        currentFunctie.Niveau3_ID = ((Niveau3)niveau3ComboBox3.SelectedItem).ID;
                        currentFunctie.Niveau3 = (Niveau3)niveau3ComboBox3.SelectedItem;
                    }

                    currentFunctie.Email = emailTextBox1.Text;
                    currentFunctie.Telefoon = telefoonTextBox1.Text;
                    currentFunctie.Mobiel = mobielTextBox.Text;
                    currentFunctie.Oorspronkelijke_Titel = oorspronkelijkeTextBox.Text;
                    if (startdatumDatePicker.SelectedDate != null)
                        currentFunctie.Startdatum = (DateTime)startdatumDatePicker.SelectedDate;
                    if (functieAttesttypeComboBox.SelectedIndex > -1)
                        currentFunctie.Attesttype_ID = ((AttestType)functieAttesttypeComboBox.SelectedItem).ID;
                    currentFunctie.Erkenningsnummer = erkenningTextBox1.Text;
                    currentFunctie.Type = functieTypeComboBox.Text;
                    currentFunctie.Mail = mailCheckBox.IsChecked == true;
                    currentFunctie.Fax = faxCheckBox.IsChecked == true;
                    currentFunctie.Actief = true;
                    actiefCheckBox2.IsChecked = true;
                    SetFunctieToForm();
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Fout bij het maken van de functie van een bestaande functie!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void sessieDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (functiesTab.Header.ToString() == "Functies")
            {
                if (inschrijvingenDataGrid.SelectedIndex > -1)
                {
                    ConfocusDBLibrary.Inschrijvingen inschrijving = (ConfocusDBLibrary.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                    Sessie sessie = new SessieServices().GetSessieByID((decimal)inschrijving.Sessie_ID);
                    NieuweSeminarie NC = new NieuweSeminarie(currentUser, sessie.Seminarie, null, sessie, false);
                    NC.Show();
                }
            }
            else if (functiesTab.Header.ToString() == "Spreker")
            {
                if (sessie_SprekerDataGrid.SelectedIndex > -1)
                {
                    Sessie_Spreker spreker = (Sessie_Spreker)sessie_SprekerDataGrid.SelectedItem;
                    Sessie sessie = new SessieServices().GetSessieByID((decimal)spreker.Sessie_ID);
                    NieuweSeminarie NC = new NieuweSeminarie(currentUser, sessie.Seminarie, null, sessie, false);
                    NC.Show();
                }
            }
        }

        private void contactBedrijvenListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void contactTab_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void contactTab_Loaded_1(object sender, RoutedEventArgs e)
        {

        }

        private void contactTab_GotFocus(object sender, RoutedEventArgs e)
        {

        }


        private void OpenWebsiteButton_Click(object sender, RoutedEventArgs e)
        {
            string url = websiteTextBox.Text;
            if (url != "")
            {
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void ShowSprekerTypes()
        {
            foreach (ComboBoxItem item in functieTypeComboBox.Items)
            {
                if (item.Content != null)
                {
                    if (item.Content.ToString() == "Klant" || item.Content.ToString() == "Prospect")
                    {
                        item.Visibility = Visibility.Collapsed;
                    }
                    else
                        item.Visibility = Visibility.Visible;
                }
            }
        }

        private void HideSprekerTypes()
        {
            foreach (ComboBoxItem item in functieTypeComboBox.Items)
            {
                if (item.Content != null)
                {
                    if (item.Content.ToString() == "Klant" || item.Content.ToString() == "Prospect")
                    {
                        item.Visibility = Visibility.Visible;
                    }
                    else
                        item.Visibility = Visibility.Collapsed;
                }
            }
        }
    }
}

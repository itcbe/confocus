﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusDBLibrary;
using ITCLibrary;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for DocumentCreatie.xaml
    /// </summary>
    public partial class DocumentCreatie : Window
    {
        private Gebruiker currentUser;
        private CollectionViewSource sessieViewSource;
        private List<Sessie> _sessies;
        private string _dataFolder;
        public DocumentCreatie()
        {
            InitializeComponent();
        }

        public DocumentCreatie(Gebruiker currentUser)
        {
            InitializeComponent();
            this.currentUser = currentUser;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VanDatePicker.SelectedDate = DateTime.Now.AddDays(-7);
            TotDatePicker.SelectedDate = DateTime.Now;
            sessieViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sessieViewSource")));
            _dataFolder = new Instelling_Service().Find().DataFolder;
        }

        private void SelectAllCheckBox_Click(object sender, RoutedEventArgs e)
        {
            bool check = ((CheckBox)sender).IsChecked == true;
            foreach (Sessie sessie in _sessies)
            {
                sessie.IsSelected = check;
            }
            sessieDataGrid.ItemsSource = new List<Sessie>();
            sessieDataGrid.ItemsSource = _sessies;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime van = (DateTime)VanDatePicker.SelectedDate;
            DateTime tot = (DateTime)TotDatePicker.SelectedDate;
            _sessies = new SessieServices().GetSessiesByDates(van, tot);
            //foreach (Sessie sessie in _sessies)
            //{
            //    sessie.IsSelected = true;
            //}
            
            sessieDataGrid.ItemsSource = _sessies;
        }

        private void EvaluatieButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (Sessie sessie in sessieDataGrid.Items)
            {
                if (sessie.IsSelected == true)
                {
                    CreeerEvaluatieDocument(sessie);
                }
            }
        }

        private void CreeerEvaluatieDocument(Sessie sessie)
        {
            string taal = sessie.SeminarieTaalCode;
            string templatenaam = "TemplateEvaluatie_" + taal;
            object template = _dataFolder + "Templates\\DocumentCreatie\\" + templatenaam + ".docx";
            WordDocument doc;
            try
            {
                doc = new WordDocument(template, "Staand");
                List<Sessie_Spreker> sprekers = new SessieSprekerServices().GetSprekersBySessieID(sessie.ID);
                int aantal = sprekers.Count;
                doc.CopyTable(aantal);
                int row = 1;
                foreach (Sessie_Spreker sprek in sprekers)
                {
                    var paragraphs = doc.app.Documents[1].Tables[2].Cell(row, 1).Range.Paragraphs;
                    if (taal == "FR")
                        paragraphs[1].Range.Text = "Orateur: " + sprek.Spreker.VolledigeNaam;
                    else
                        paragraphs[1].Range.Text = "Gastspreker: " + sprek.Spreker.VolledigeNaam;
                    row += 6;
                }
                string datumlocatie = "";
                List<Sessie> sisterSessies = new SessieServices().GetSessiesBySeminarieIDAndSessieNaam(sessie);
                if (sisterSessies.Count == 1)
                {
                    datumlocatie = ((DateTime)sessie.Datum).ToShortDateString() + " ("+ sessie.LocatieNaam + ")";
                }
                else
                {
                    datumlocatie = "  O  " + ((DateTime)sessie.Datum).ToShortDateString() + " (" + sessie.LocatieNaam + ")\v";
                    foreach (Sessie ses  in sisterSessies)
                    {
                        datumlocatie += "  O  " + ((DateTime)ses.Datum).ToShortDateString() + " (" + ses.LocatieNaam + ")\v";
                    }
                }



                List<String> fields = new List<string>();
                //1
                fields.Add(sessie.SeminarieTitel);
                //2
                fields.Add(datumlocatie);
                //3
                fields.Add(sessie.Naam);
                //4
                fields.Add(sessie.LocatieNaam);

                doc.Activate();
                doc.ChangeEvaluatieFields(fields);
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Er is geen template beschikbaar voor deze erkenning!", ConfocusMessageBox.Kleuren.Rood);
            }          
        }

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    object template = _dataFolder + "Templates\\DocumentCreatie\\Gastspreker.docx";
        //    WordDocument doc;
        //    try
        //    {
        //        doc = new WordDocument(template, "Staand");
        //        doc.CopyTable(3);

        //        var paragraphs = doc.app.Selection.Tables[1].Cell(1, 1).Range.Paragraphs;
        //        paragraphs[1].Range.Text = "Gastspreker: Stefan Kelchtermans";
        //        var paragraphs2 = doc.app.Selection.Tables[1].Cell(7, 1).Range.Paragraphs;
        //        paragraphs2[1].Range.Text = "Gastspreker: Inneke Ponet";
        //        var paragraphs3 = doc.app.Selection.Tables[1].Cell(13, 1).Range.Paragraphs;
        //        paragraphs3[1].Range.Text = "Gastspreker: Raïce Litear";
        //        var paragraphs4 = doc.app.Selection.Tables[1].Cell(19, 1).Range.Paragraphs;
        //        paragraphs4[1].Range.Text = "Gastspreker: Mark Meerten";
        //        doc.Activate();
        //    }
        //    catch (Exception ex)
        //    {
        //        ;
        //    }
        //}

        private void CreeerInleidingDocument(Sessie sessie)
        {
            string taal = sessie.SeminarieTaalCode;
            string templatenaam = "TemplateInleidingSeminarie_" + taal;
            object template = _dataFolder + "Templates\\DocumentCreatie\\"+ templatenaam + ".docx";
            WordDocument doc;
            try
            {
                doc = new WordDocument(template, "Staand");
                List<String> fields = new List<string>();
                List<Sessie_Spreker> sprekers = new SessieSprekerServices().GetSprekersBySessieID(sessie.ID);
                int aantalsprekers = sprekers.Count;
                string gastsprekers = aantalsprekers > 1 ? "s" : "";
                Spreker firstSpreker = new Spreker();// 
                if (aantalsprekers > 0)
                    firstSpreker = new SprekerServices().GetSprekerById(sprekers[0].Spreker_ID);
                string hun = "";
                if (aantalsprekers > 1)
                    hun = "hun";
                else if (firstSpreker.Contact != null)
                {
                    if (sprekers[0].Spreker.Contact.Aanspreking == "V")
                        hun = "haar";
                    else
                        hun = "zijn";
                }
                else
                    hun = "hun";
                //1
                fields.Add(sessie.SeminarieTitel);
                //2
                fields.Add(((DateTime)sessie.Datum).ToShortDateString());
                //3
                fields.Add(sessie.LocatieNaam);
                //4
                fields.Add(gastsprekers);
                //5
                fields.Add(hun);
                //6
                fields.Add(sessie.Naam);

                doc.Activate();
                doc.ChangeInleidingFields(fields);
            }
            catch (Exception)
            {

                ConfocusMessageBox.Show("Er is geen template beschikbaar!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void CreeerEnMailOvereenkomst(Sessie sessie)
        {
            string taal = sessie.SeminarieTaalCode;
            string templatenaam = "TemplateSprekerovereenkomst_" + taal;
            object template = _dataFolder + "Templates\\DocumentCreatie\\" + templatenaam + ".docx";
            string contractfolder = _dataFolder + "Sprekerovereenkomsten\\";
            List<Sessie_Spreker> sprekers = new SessieSprekerServices().GetSprekersBySessieID(sessie.ID);
            foreach (Sessie_Spreker spreker in sprekers)
            {
                Spreker myspreker = new SprekerServices().GetSprekerById(spreker.Spreker_ID);
                string filename = contractfolder + "Contract_" + myspreker.Contact.VolledigeNaam + "_" + DateTime.Today.Day + "-" + DateTime.Today.Month + "-" + DateTime.Today.Year +".pdf";
                string aanspreking = myspreker.Contact.Aanspreking == "M" ? "De Heer" : "Mevrouw";
                WordDocument doc;
                try
                {
                    doc = new WordDocument(template, "Staand");
                    List<String> fields = new List<string>();
                    //1
                    fields.Add(aanspreking);
                    //2
                    fields.Add(myspreker.Contact.Achternaam + " " + myspreker.Contact.Voornaam);
                    //3
                    fields.Add(((DateTime)sessie.Datum).ToLongDateString());
                    //4
                    fields.Add(sessie.SeminarieTitel);
                    //5
                    fields.Add(myspreker.Tarief);
                    //6
                    fields.Add(sessie.NaamVSessie);
                    //7
                    fields.Add(sessie.Naam);
                    //8
                    fields.Add(sessie.LocatieNaam);

                    doc.Activate();
                    doc.ChangeOvereenkomstFields(fields);
                    doc.SaveAsPDF(filename);
                    doc.quit();

                    MailOvereenkomst(filename, myspreker, sessie);
                }
                catch (Exception)
                {

                    ConfocusMessageBox.Show("Er is geen template beschikbaar!", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void MailOvereenkomst(string filename, Spreker spreker, Sessie sessie)
        {
            try
            {
                ConfocusClassLibrary.Attachment attachment = new ConfocusClassLibrary.Attachment();
                attachment.Naam = "Overeenkomst";
                attachment.Link = filename;
                Sessie fullsessie = new SessieServices().GetSessieByID(sessie.ID);
                Taal taal = new TaalServices().GetTaalByID((Decimal)spreker.Contact.Taal_ID);
                string mailbody = GetMailbody(sessie, spreker);
                Outlook.Application oApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                DateTime mydate = new DateTime(2012, 01, 01);
                oMailItem.Attachments.Add(attachment.Link, Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                if (taal.Code == "FR")
                    oMailItem.Subject = "Votre contrat á la formation Confocus: " + fullsessie.SeminarieTitel + ": " + fullsessie.Naam + " (" + ((DateTime)fullsessie.Datum).ToShortDateString() + " - " + fullsessie.Locatie.Naam + ")";
                else
                    oMailItem.Subject = "Uw overeenkomst Confocus opleiding: " + fullsessie.SeminarieTitel + ": " + fullsessie.Naam + " (" + ((DateTime)fullsessie.Datum).ToShortDateString() + " - " + fullsessie.Locatie.Naam + ")";
                oMailItem.To = spreker.Email;
                oMailItem.HTMLBody = mailbody;
                oMailItem.Importance = Outlook.OlImportance.olImportanceHigh;
                oMailItem.Display(true);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string GetMailbody(Sessie sessie, Spreker spreker)
        {
            string filename = "OvereenkomstMailbody " + sessie.SeminarieTaalCode;
            AttestTemplate template = new AttestTemplate_Service().GetByName(filename)[0];
            String file = template.Uri;
            string body = System.IO.File.ReadAllText(file);
            body = body.Replace("#SprekerNaam#", spreker.Contact.VolledigeNaam);
            body = body.Replace("#SeminarieSessieDatumLocatie#", sessie.SeminarieTitel + ": " + sessie.NaamDatumLocatie);
            body = body.Replace("#Aanspreking#", spreker.Contact.Aanspreking == "M" ? "Geachte mijnheer" : "Geachte mevrouw");
            body = body.Replace("#AchternaamSpreker#", spreker.Contact.Achternaam);
            return body;
        }

        private void DataDridCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            Sessie sessie = (Sessie)sessieDataGrid.SelectedItem;
            sessie.IsSelected = box.IsChecked == true;
        }

        private void InleidingButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (Sessie sessie in sessieDataGrid.Items)
            {
                if (sessie.IsSelected == true)
                {
                    CreeerInleidingDocument(sessie);
                }
            }
        }

        private void OvereenkomstButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (Sessie sessie in sessieDataGrid.Items)
            {
                if (sessie.IsSelected == true)
                {
                    CreeerEnMailOvereenkomst(sessie);
                }
            }
        }

    }
}

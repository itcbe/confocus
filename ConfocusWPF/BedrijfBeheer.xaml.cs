﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for BedrijfBeheer.xaml
    /// </summary>
    public partial class BedrijfBeheer : Window
    {
        private CollectionViewSource bedrijfViewSource;
        private List<Bedrijf> Bedrijven = new List<Bedrijf>();
        private List<Bedrijf> gevondenBedrijven = new List<Bedrijf>();
        private List<Bedrijf> gewijzigdeBedrijven = new List<Bedrijf>();
        private Boolean IsScroll = false, IsButtonClick = false, datagridChange = false, IsInit = false;
        private Gebruiker currentUser;
        private Bedrijf currentBedrijf;
        private bool IsChanged = false;

        public BedrijfBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Bedrijf beheer : Ingelogd als " + currentUser.Naam;
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            bedrijfViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            bedrijfViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            bedrijfViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            bedrijfViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
            NieuwBedrijf NB = new NieuwBedrijf(currentUser);
            if (NB.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            PostcodeService postcodeService = new PostcodeService();
            List<Postcodes> postcodes = postcodeService.FindAllPostcodes();
            postcodeComboBox.ItemsSource = postcodes;
            postcodeComboBox.DisplayMemberPath = "PostcodeGemeente";

            BedrijfServices service = new BedrijfServices();
            Bedrijven = service.FindActive();
            bedrijfViewSource.Source = Bedrijven;
            if (Bedrijven.Count > 0)
                bedrijfDataGrid.SelectedIndex = 0;

            goUpdate();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveChanges();
        }

        private void SlaWijzigingenOp()
        {
            RegisterChanges();
            //if (gewijzigdeBedrijven.Count > 0)
            //{
            //    BedrijfServices service = new BedrijfServices();
            //    foreach (Bedrijf b in gewijzigdeBedrijven)
            //    {
            //        b.Gewijzigd_door = Username;
            //        b.Gewijzigd = DateTime.Now;
            //        service.SaveBedrijfChanges(b);
            //    }
            //    gewijzigdeBedrijven.Clear();
            //    ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            //    LoadData();
            //}
            //else
            //    ConfocusMessageBox.Show("Geen wijzigingen gevonden.", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));

        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + ".gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IsInit = true;
            bedrijfViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("bedrijfViewSource")));
            LoadData();
            IsInit = false;
        }

        private void Reload()
        {
            int index = bedrijfDataGrid.SelectedIndex;
            BedrijfServices service = new BedrijfServices();
            Bedrijven = service.FindActive();
            bedrijfViewSource.Source = Bedrijven;
            if (Bedrijven.Count < index && Bedrijven.Count > 0)
                bedrijfDataGrid.SelectedIndex = index;
            else if (Bedrijven.Count >= index)
                bedrijfDataGrid.SelectedIndex = (Bedrijven.Count - 1);


        }

        private void goUpdate()
        {
            if (bedrijfDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (bedrijfViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (bedrijfViewSource.View.CurrentPosition == bedrijfDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                bedrijfDataGrid.SelectedIndex = 0;
            if (bedrijfDataGrid.Items.Count != 0)
                bedrijfDataGrid.ScrollIntoView(bedrijfDataGrid.SelectedItem);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
            this.Close();
        }

        private void bedrijfDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            if (bedrijfDataGrid.SelectedIndex > -1)
            {
                datagridChange = true;
                Bedrijf bedrijf = (Bedrijf)bedrijfDataGrid.SelectedItem;
                currentBedrijf = bedrijf;
                postcodeComboBox.Text = bedrijf.strPostcodeEnPlaats;
                plaatsComboBox.Text = bedrijf.strGemeente;
                provincieComboBox.Text = bedrijf.strProvincie;
            }
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdate();
                IsScroll = false;
            }
            datagridChange = false;
        }

        private void CheckIsChanged()
        {
            if (IsChanged)
            {
                RegisterChanges();
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private Bedrijf GetBedrijfFromForm(Bedrijf bedrijf)
        {
            if (IsBedrijfValid())
            {
                bedrijf.Naam = naamTextBox.Text;
                bedrijf.Ondernemersnummer = Helper.CleanVatnumber(ondernemersnummerTextBox.Text);
                bedrijf.Adres = adresTextBox.Text;
                bedrijf.Telefoon = telefoonTextBox.Text;
                bedrijf.Fax = faxTextBox.Text;
                bedrijf.Website = websiteTextBox.Text;
                bedrijf.Email = emailTextBox.Text;
                bedrijf.Notities = notitiesTextBox.Text;
                bedrijf.Actief = actiefCheckBox.IsChecked == true;
                bedrijf.Gewijzigd = DateTime.Now;
                bedrijf.Gewijzigd_door = currentUser.Naam;
                bedrijf.Postcode = "";
                bedrijf.Plaats = "";
                bedrijf.Provincie = "";
                bedrijf.Land = "";
                bedrijf.BedrijfsnaamFonetisch = DBHelper.Simplify(bedrijf.Naam);
                if (postcodeComboBox.SelectedIndex > -1)
                {
                    Postcodes postcode = (Postcodes)postcodeComboBox.SelectedItem;
                    bedrijf.Postcode = postcode.Postcode;
                    bedrijf.Zipcode = postcode.ID;
                    if (plaatsComboBox.SelectedIndex > -1)
                        bedrijf.Plaats = ((Postcodes)plaatsComboBox.SelectedItem).Gemeente;
                    if (provincieComboBox.SelectedIndex > -1)
                        bedrijf.Provincie = ((ComboBoxItem)provincieComboBox.SelectedItem).Content.ToString();
                    bedrijf.Land = landTextBox.Text;
                }

                return bedrijf;
            }
            else
                return null;
        }

        private bool IsBedrijfValid()
        {
            bool valid = true;
            String errortext = "";
            if (naamTextBox.Text == "")
            {
                errortext += "De bedrijfsnaam is leeg! dit is verplicht.\n";
                valid = false;
            }

            if (!valid)
                ConfocusMessageBox.Show(errortext, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (IsChanged)
            {
                RegisterChanges();
            }
            //if (gewijzigdeBedrijven.Count > 0)
            //{
            //    if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            //    {
            //        SlaWijzigingenOp();
            //    }
            //}
        }

        private void postcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (postcodeComboBox.SelectedIndex > -1)
            {
                Postcodes selected = (Postcodes)postcodeComboBox.SelectedItem;
                PostcodeService service = new PostcodeService();
                List<Postcodes> gemeenten = service.GetGemeentesByPostcode(selected.Postcode);
                if (gemeenten.Count > 0)
                {
                    plaatsComboBox.ItemsSource = gemeenten;
                    plaatsComboBox.DisplayMemberPath = "Gemeente";
                    plaatsComboBox.SelectedIndex = 0;
                }
                provincieComboBox.Text = selected.Provincie;
                landTextBox.Text = selected.Land;
            }
            else
            {
                plaatsComboBox.ItemsSource = new List<Postcodes>();
                provincieComboBox.SelectedIndex = -1;
                landTextBox.Text = "";
            }
        }

        private void plaatsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (plaatsComboBox.SelectedIndex > -1)
            {
                Postcodes postcode = (Postcodes)plaatsComboBox.SelectedItem;
                landTextBox.Text = postcode.Land;
            }
        }

        private void RegisterChanges()
        {
            if (!IsInit && !datagridChange)
            {
                if (IsChanged)
                {
                    if (MessageBox.Show("Er zijn wijzigingen geregistreerd. Wil je deze opslaan?", "Wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        SaveChanges();
                    }
                    else
                    {
                        IsChanged = false;
                        bedrijfDataGrid.IsEnabled = true;
                    }

                }
            }
        }

        private void SaveChanges()
        {
            if (Helper.CheckEmail(emailTextBox.Text) || string.IsNullOrEmpty(emailTextBox.Text))
            {
                if (bedrijfDataGrid.SelectedIndex > -1 && !datagridChange)
                {
                    //Bedrijf bedrijf = (Bedrijf)bedrijfDataGrid.SelectedItem;
                    int index = bedrijfDataGrid.SelectedIndex;
                    currentBedrijf = GetBedrijfFromForm(currentBedrijf);
                    if (currentBedrijf != null)
                    {
                        try
                        {
                            BedrijfServices service = new BedrijfServices();
                            service.SaveBedrijfChanges(currentBedrijf);
                            IsChanged = false;
                            bedrijfDataGrid.IsEnabled = true;
                        }
                        catch (CompanyConcurrencyException cex)
                        {
                            ConfocusMessageBox.Show("Fout bij het opslaan!\n" + cex.Message, ConfocusMessageBox.Kleuren.Rood);
                            ReloadBedrijven();
                            ZoekInLijst();
                            bedrijfDataGrid.SelectedIndex = index;
                            IsChanged = false;
                            bedrijfDataGrid.IsEnabled = true;
                        }
                        catch (Exception ex)
                        {
                            ConfocusMessageBox.Show("Fout bij het opslaan!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                        }
                    }
                }
            }
            else
                ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }

        private bool CheckForChanges()
        {
            bool isgewijzigd = false;
            try
            {
                if (bedrijfDataGrid.SelectedIndex > -1)
                {
                    Bedrijf bedrijf = new Bedrijf();
                    bedrijf = GetBedrijfFromForm(bedrijf);
                    Bedrijf oldbedrijf = (Bedrijf)bedrijfDataGrid.SelectedItem;
                    string naam = oldbedrijf.Naam == null ? "" : oldbedrijf.Naam;
                    if (bedrijf.Naam != naam)
                        isgewijzigd = true;
                    string ondernemersnr = oldbedrijf.Ondernemersnummer == null ? "" : oldbedrijf.Ondernemersnummer;
                    if (bedrijf.Ondernemersnummer != ondernemersnr)
                        isgewijzigd = true;
                    string adres = oldbedrijf.Adres == null ? "" : oldbedrijf.Adres;
                    if (bedrijf.Adres != adres)
                        isgewijzigd = true;
                    string postcode = oldbedrijf.Postcode == null ? "" : oldbedrijf.Postcode;
                    if (bedrijf.Postcode != postcode)
                        isgewijzigd = true;
                    string plaats = oldbedrijf.Plaats == null ? "" : oldbedrijf.Plaats;
                    if (bedrijf.Plaats != plaats)
                        isgewijzigd = true;
                    string land = oldbedrijf.Land == null ? "" : oldbedrijf.Land;
                    if (bedrijf.Land != land)
                        isgewijzigd = true;
                    string provincie = oldbedrijf.Provincie == null ? "" : oldbedrijf.Provincie;
                    if (bedrijf.Provincie != provincie)
                        isgewijzigd = true;
                    string telefoon = oldbedrijf.Telefoon == null ? "" : oldbedrijf.Telefoon;
                    if (bedrijf.Telefoon != telefoon)
                        isgewijzigd = true;
                    string fax = oldbedrijf.Fax == null ? "" : oldbedrijf.Fax;
                    if (bedrijf.Fax != fax)
                        isgewijzigd = true;
                    string email = oldbedrijf.Email == null ? "" : oldbedrijf.Email;
                    if (bedrijf.Email != email)
                        isgewijzigd = true;
                    string website = oldbedrijf.Website == null ? "" : oldbedrijf.Website;
                    if (bedrijf.Website != website)
                        isgewijzigd = true;
                    string notities = oldbedrijf.Notities == null ? "" : oldbedrijf.Notities;
                    if (bedrijf.Notities != notities)
                        isgewijzigd = true;
                    if (bedrijf.Actief != oldbedrijf.Actief)
                        isgewijzigd = true;
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                isgewijzigd = false;
            }

            return isgewijzigd;
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekInLijst();
        }

        private void ZoekInLijst()
        {
            if (ZoekTextBox.Text != "")
            {
                try
                {
                    string zoektekst = DBHelper.Simplify(ZoekTextBox.Text).ToLower();
                    gevondenBedrijven.Clear();

                    gevondenBedrijven = (from b in Bedrijven
                                         where b.BedrijfsnaamFonetisch.ToLower().Contains(zoektekst)
                                         || (b.Adres != null && b.Adres.Contains(zoektekst))
                                         select b).ToList();

                    bedrijfViewSource.Source = gevondenBedrijven;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Er is een probleem met het zoeken van het bedrijf!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    bedrijfViewSource.Source = Bedrijven;
                }
            }
            else
                bedrijfViewSource.Source = Bedrijven;

            goUpdate();

        }

        private void OpenLinkButton_Click(object sender, RoutedEventArgs e)
        {
            string url = websiteTextBox.Text;
            if (url != "")
            {
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void provincieComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datagridChange == false && !IsInit)
                IsChanged = CheckForChanges();
        }

        private void ZoekTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IsChanged)
                RegisterChanges();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (bedrijfDataGrid.SelectedIndex > -1)
            {
                IsChanged = CheckForChanges();
                bedrijfDataGrid.IsEnabled = !IsChanged;
            }
            else
                ConfocusMessageBox.Show("Om een nieuw bedrijf toe te voegen moet je de nieuw knop in de taakbalk gebruiken.", ConfocusMessageBox.Kleuren.Rood);
        }

        private void ComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (bedrijfDataGrid.SelectedIndex > -1)
            {
                IsChanged = CheckForChanges();
                bedrijfDataGrid.IsEnabled = !IsChanged;
            }
            else
                ConfocusMessageBox.Show("Om een nieuw bedrijf toe te voegen moet je de nieuw knop in de taakbalk gebruiken.", ConfocusMessageBox.Kleuren.Rood);
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (bedrijfDataGrid.SelectedIndex > -1)
            {
                IsChanged = CheckForChanges();
                bedrijfDataGrid.IsEnabled = !IsChanged;
            }
            else
                ConfocusMessageBox.Show("Om een nieuw bedrijf toe te voegen moet je de nieuw knop in de taakbalk gebruiken.", ConfocusMessageBox.Kleuren.Rood);
        }

        private void bedrijfDataGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void bedrijfDataGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //RegisterChanges();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            BedrijfServices bService = new BedrijfServices();
            List<Bedrijf> bedrijven = new BedrijfServices().GetWhereZipcodeIsNull();
            foreach (Bedrijf item in bedrijven)
            {

                PostcodeService pService = new PostcodeService();
                Postcodes pc = pService.GetPostcodeByPostcode(item.Postcode.Trim());//GetPostcodeByIDAndCity(item.Postcode, item.Plaats);
                if (pc != null)
                {
                    item.Zipcode = pc.ID;
                    bService.SaveBedrijfChanges(item);
                }
                //item.BedrijfsnaamFonetisch = DBHelper.Simplify(item.Naam);            
            }
        }

        private void gridTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (bedrijfDataGrid.SelectedIndex > -1)
            {
                if (e.Key == Key.Enter || e.Key == Key.Tab)
                {
                    TextBox text = (TextBox)sender;
                    Bedrijf bedrijf = (Bedrijf)bedrijfDataGrid.SelectedItem;
                    switch (text.Name)
                    {
                        case "gridNaamTextBox":
                            bedrijf.Naam = text.Text;
                            break;
                        case "gridBtwnummerTextbox":
                            bedrijf.Ondernemersnummer = text.Text;
                            break;
                        case "gridAdresTextBox":
                            bedrijf.Adres = text.Text;
                            break;
                        case "gridTelefoonTextbox":
                            bedrijf.Telefoon = text.Text;
                            break;
                        case "gridFaxTextBox":
                            bedrijf.Fax = text.Text;
                            break;
                        case "gridEmailTextBox":
                            bedrijf.Email = text.Text;
                            break;
                        case "gridWebsiteTextBox":
                            bedrijf.Website = text.Text;
                            break;
                        case "gridNotitiesTextBox":
                            bedrijf.Notities = text.Text;
                            break;
                    }
                    try
                    {
                        BedrijfServices service = new BedrijfServices();
                        service.SaveBedrijfChanges(bedrijf);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
            else
                ConfocusMessageBox.Show("Om een nieuw bedrijf toe te voegen moet je de nieuw knop in de taakbalk gebruiken.", ConfocusMessageBox.Kleuren.Rood);
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (bedrijfDataGrid.SelectedIndex > -1)
            {
                CheckBox check = (CheckBox)sender;
                Bedrijf bedrijf = (Bedrijf)bedrijfDataGrid.SelectedItem;
                bool reload = false, isCancel = false;
                if (check.IsChecked == false)
                {
                    decimal bedrijfsid = ((Bedrijf)bedrijfDataGrid.SelectedItem).ID;
                    BedrijfConflictenWindow BC = new BedrijfConflictenWindow(currentUser, bedrijfsid);
                    if (BC.ShowDialog() == true)
                    {
                        reload = true;
                    }
                    else
                        isCancel = true;
                }
                if (!isCancel)
                {
                    try
                    {
                        BedrijfServices service = new BedrijfServices();
                        service.SaveBedrijfChanges(bedrijf);
                        if (reload)
                        {
                            ReloadBedrijven();
                            ZoekInLijst();
                        }
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                else
                    actiefCheckBox.IsChecked = true;
            }
            else
                ConfocusMessageBox.Show("Om een nieuw bedrijf toe te voegen moet je de nieuw knop in de taakbalk gebruiken.", ConfocusMessageBox.Kleuren.Rood);
        }

        private void ReloadBedrijven()
        {
            BedrijfServices service = new BedrijfServices();
            Bedrijven = service.FindActive();
            bedrijfViewSource.Source = Bedrijven;
            if (Bedrijven.Count > 0)
                bedrijfDataGrid.SelectedIndex = 0;

            goUpdate();
        }
    }
}

﻿using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SprekerBeheer.xaml
    /// </summary>
    public partial class SprekerBeheer : Window
    {
        private CollectionViewSource sprekerViewSource;
        private List<Spreker> sprekers = new List<Spreker>();
        private List<Spreker> gevondenSprekers = new List<Spreker>();
        private List<Spreker> gewijzigdeSprekers = new List<Spreker>();
        private Boolean IsScroll = false, IsButtonClick = false;
        private Gebruiker currentUser;

        public SprekerBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Spreker beheer : Ingelogd als " + currentUser.Naam;
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            sprekerViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            sprekerViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            sprekerViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            sprekerViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweSpreker NS = new NieuweSpreker(currentUser);
            if (NS.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();            
        }

        private void SaveWijzigingen()
        {
            if (gewijzigdeSprekers.Count > 0)
            {
                SprekerServices service = new SprekerServices();
                foreach (Spreker sp in gewijzigdeSprekers)
                {
                    sp.Gewijzigd = DateTime.Now;
                    sp.Gewijzigd_door = currentUser.Naam;
                    service.SaveSprekerWijzigingen(sp);
                }
                gewijzigdeSprekers.Clear();
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            }
            else
                ConfocusMessageBox.Show("Geen wijzigingen gevonden.", ConfocusMessageBox.Kleuren.Blauw);
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void goUpdate()
        {
            if (sprekerDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (sprekerViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (sprekerViewSource.View.CurrentPosition == sprekerDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                sprekerDataGrid.SelectedIndex = 0;
            if (sprekerDataGrid.Items.Count != 0)
                sprekerDataGrid.ScrollIntoView(sprekerDataGrid.SelectedItem);
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "search":
                    img = searchImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + "_hover.gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));

        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "search":
                    img = searchImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + ".gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            sprekerViewSource = ((CollectionViewSource)(this.FindResource("sprekerViewSource")));
            LoadData();
        }

        private void LoadData()
        {
            first.IsEnabled = false;
            next.IsEnabled = false;
            back.IsEnabled = false;
            last.IsEnabled = false;

            BedrijfServices bedrijfService = new BedrijfServices();
            bedrijfComboBox.ItemsSource = bedrijfService.FindAll();
            bedrijfComboBox.DisplayMemberPath = "Naam";

            ContactServices contactService = new ContactServices();
            contactComboBox.ItemsSource = contactService.FindAll();
            contactComboBox.DisplayMemberPath = "VolledigeNaam";

            Niveau1Services n1Service = new Niveau1Services();
            niveau1ComboBox.ItemsSource = n1Service.FindAll();
            niveau1ComboBox.DisplayMemberPath = "Naam";

            Niveau2Services n2Service = new Niveau2Services();
            niveau2ComboBox.ItemsSource = n2Service.FindAll();
            niveau2ComboBox.DisplayMemberPath = "Naam";

            Niveau3Services n3Service = new Niveau3Services();
            niveau3ComboBox.ItemsSource = n3Service.FindAll();
            niveau3ComboBox.DisplayMemberPath = "Naam";

            ErkenningServices erService = new ErkenningServices();
            erkenningComboBox.ItemsSource = erService.FindAll();
            erkenningComboBox.DisplayMemberPath = "Naam";

            SprekerServices sprekerService = new SprekerServices();
            sprekers = sprekerService.FindAll();
            sprekerViewSource.Source = sprekers;

            goUpdate();          
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Control_LostFocus(object sender, RoutedEventArgs e)
        {
            RegisterChanges();
        }

        private void sprekerDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sprekerDataGrid.Items.Count > 1)
            {
                Spreker selected = (Spreker)sprekerDataGrid.SelectedItem;
                LoadSpreker(selected);

            }
        }

        private void LoadSpreker(Spreker selected)
        {
            BedrijfServices bedrijfService = new BedrijfServices();
            Bedrijf bedrijf = bedrijfService.GetBedrijfByID(selected.Bedrijf_ID.Value);
            bedrijfComboBox.Text = bedrijf.Naam;

            ContactServices contactService = new ContactServices();
            Contact contact = contactService.GetContactByID(selected.Contact_ID);
            contactComboBox.Text = contact.VolledigeNaam;

            Niveau1Services n1Service = new Niveau1Services();
            Niveau1 niveau1 = n1Service.GetNiveau1ByID(selected.Niveau1_ID);
            niveau1ComboBox.Text = niveau1.Naam;

            if (selected.Niveau2_ID != null)
            {
                Niveau2Services n2Service = new Niveau2Services();
                Niveau2 niveau2 = n2Service.GetNiveau2ByID(selected.Niveau2_ID);
                niveau2ComboBox.Text = niveau2.Naam;
            }

            if (selected.Niveau3_ID != null)
            {
                Niveau3Services n3Service = new Niveau3Services();
                Niveau3 niveau3 = n3Service.GetNiveau3ByID(selected.Niveau3_ID);
                niveau3ComboBox.Text = niveau3.Naam;
            }

            if (selected.Erkenning_ID != null)
            {
                ErkenningServices erService = new ErkenningServices();
                Erkenning er = erService.GetErkenningByID(selected.Erkenning_ID);
                erkenningComboBox.Text = er.Naam;
            }

        }

        private void OpenCVButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog OpenCV = new OpenFileDialog();
            if (OpenCV.ShowDialog() == true)
            {
                cVTextBox.Text = OpenCV.FileName;
            }
        }

        private void cVTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (cVTextBox.Text != "")
            {
                try
                {
                    Process wordProcess = new Process();
                    wordProcess.StartInfo.FileName = cVTextBox.Text;
                    wordProcess.StartInfo.UseShellExecute = true;
                    wordProcess.Start();
                }
                catch (Exception)
                {
                    ConfocusMessageBox.Show("Kan Word niet openen.", ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void contactComboBox_DropDownClosed(object sender, EventArgs e)
        {
            RegisterChanges();
        }

        private void RegisterChanges()
        {
            if (sprekerDataGrid.Items.Count > 1)
            {
                Spreker sp = (Spreker)sprekerDataGrid.SelectedItem;
                Contact contact = (Contact)contactComboBox.SelectedItem;
                if (contact != null)
                    sp.Contact_ID = contact.ID;
                Bedrijf bedrijf = (Bedrijf)bedrijfComboBox.SelectedItem;
                if (bedrijf != null)
                    sp.Bedrijf_ID = bedrijf.ID;
                if (erkenningComboBox.SelectedIndex > -1)
                {
                    Erkenning erkenning = (Erkenning)erkenningComboBox.SelectedItem;
                    if (erkenning != null)
                        sp.Erkenning_ID = erkenning.ID;
                }
                if (niveau1ComboBox.SelectedIndex > -1)
                {
                    Niveau1 n1 = (Niveau1)niveau1ComboBox.SelectedItem;
                    if (n1 != null)
                        sp.Niveau1_ID = n1.ID;
                }
                if (niveau2ComboBox.SelectedIndex > -1)
                {
                    Niveau2 n2 = (Niveau2)niveau2ComboBox.SelectedItem;
                    if (n2 != null)
                        sp.Niveau2_ID = n2.ID;
                }
                if (niveau3ComboBox.SelectedIndex > -1)
                {
                    Niveau3 n3 = (Niveau3)niveau3ComboBox.SelectedItem;
                    if (n3 != null)
                        sp.Niveau3_ID = n3.ID;
                }
                sp.Tarief = tariefTextBox.Text;

                if (gewijzigdeSprekers.Contains(sp))
                {
                    int pos = 0;
                    foreach (Spreker spreker in gewijzigdeSprekers)
                    {
                        if (spreker.ID == sp.ID)
                            break;
                        else
                            pos++;

                        gewijzigdeSprekers.RemoveAt(pos);
                        gewijzigdeSprekers.Add(sp);
                    }
                }
                else
                    gewijzigdeSprekers.Add(sp);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gewijzigdeSprekers.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil u deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveWijzigingen();
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ZoekInLijst();
            }
        }

        private void ZoekInLijst()
        {
            gevondenSprekers.Clear();

            gevondenSprekers = (from s in sprekers
                                where s.Onderwerp.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                select s).ToList();

            if (gevondenSprekers.Count > 0)
            {
                sprekerViewSource.Source = gevondenSprekers;
            }
            else
                sprekerViewSource.Source = sprekers;
        }
    }
}

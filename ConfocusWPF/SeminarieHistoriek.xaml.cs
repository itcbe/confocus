﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using ITCLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for SeminarieHistoriek.xaml
    /// </summary>
    public partial class SeminarieHistoriek : Window
    {
        private CollectionViewSource seminarieViewSource;
        private CollectionViewSource sessieViewSource;
        //private CollectionViewSource inschrijvingViewSource;
        private CollectionViewSource deelnemerViewSource;
        private CollectionViewSource seminarie_DoelgroepViewSource;
        private CollectionViewSource seminarie_MarketingViewSource;
        private CollectionViewSource inschrijvingenViewSource;
        private CollectionViewSource seminarie_ErkenningenViewSource;
        private CollectionViewSource sessie_SprekerViewSource;

        private Gebruiker currentUser;

        public Seminarie currentSeminarie { get; private set; }
        public Seminarie_Doelgroep currentDoelgroep { get; private set; }
        public Sessie currentSessie { get; private set; }

        public SeminarieHistoriek(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            seminarieViewSource = ((CollectionViewSource)(this.FindResource("seminarieViewSource")));
            sessieViewSource = ((CollectionViewSource)(this.FindResource("sessieViewSource")));
            //inschrijvingViewSource = ((CollectionViewSource)(this.FindResource("inschrijvingViewSource")));
            deelnemerViewSource = ((CollectionViewSource)(this.FindResource("deelnemerViewSource")));
            seminarie_DoelgroepViewSource = ((CollectionViewSource)(this.FindResource("seminarie_DoelgroepViewSource")));
            seminarie_MarketingViewSource = ((CollectionViewSource)(FindResource("seminarie_MarketingViewSource")));
            inschrijvingenViewSource = ((CollectionViewSource)(FindResource("inschrijvingenViewSource")));
            seminarie_ErkenningenViewSource = ((CollectionViewSource)(FindResource("seminarie_ErkenningenViewSource")));
            sessie_SprekerViewSource = ((CollectionViewSource)(FindResource("sessie_SprekerViewSource")));

            LoadData();
        }

        private void LoadData()
        {
            GebruikerService gService = new GebruikerService();
            List<Gebruiker> gebruikers = gService.FindActive();
            Gebruiker gebruiker = new Gebruiker();
            gebruiker.Naam = "Kies";
            gebruikers.Insert(0, gebruiker);
            verantwoordelijke_IDComboBox.ItemsSource = gebruikers;
            verantwoordelijke_IDComboBox.DisplayMemberPath = "Naam";
            verantwoordelijke_IDComboBox.SelectedIndex = 0;
            terplaatseComboBox.ItemsSource = gebruikers;
            terplaatseComboBox.DisplayMemberPath = "Naam";
            terplaatseComboBox.SelectedIndex = 0;
            sessieVerantwoordelijkeComboBox.ItemsSource = gebruikers;
            sessieVerantwoordelijkeComboBox.DisplayMemberPath = "Naam";
            sessieVerantwoordelijkeComboBox.SelectedIndex = 0;

            sessieVerantwoordelijkeFilterComboBox.ItemsSource = gebruikers;
            sessieVerantwoordelijkeFilterComboBox.DisplayMemberPath = "Naam";
            sessieVerantwoordelijkeFilterComboBox.SelectedIndex = 0;

            TerplaatseFilterComboBox.ItemsSource = gebruikers;
            TerplaatseFilterComboBox.DisplayMemberPath = "Naam";
            TerplaatseFilterComboBox.SelectedIndex = 0;

            ErkenningServices eService = new ErkenningServices();
            List<Erkenning> erkenningen = eService.FindActive();
            Erkenning erk = new Erkenning();
            erk.Naam = "Kies";
            erkenningen.Insert(0, erk);
            erkenningComboBox.ItemsSource = erkenningen;
            erkenningComboBox.DisplayMemberPath = "Naam";
            erkenningComboBox.SelectedIndex = 0;

            LocatieServices lService = new LocatieServices();
            List<Locatie> locaties = lService.FindActive();
            Locatie locatie = new Locatie();
            locatie.Naam = "Kies";
            locaties.Insert(0, locatie);
            locatieComboBox.ItemsSource = locaties;
            locatieComboBox.DisplayMemberPath = "Naam";
            locatieComboBox.SelectedIndex = 0;

            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> N1Lijst = n1Service.FindActive();
            Niveau1 n1 = new Niveau1();
            n1.Naam = "Kies";
            N1Lijst.Insert(0, n1);
            niveau1ComboBox.ItemsSource = N1Lijst;
            niveau1ComboBox.DisplayMemberPath = "Naam";

            TaalServices tService = new TaalServices();
            List<Taal> talen = tService.FindAll();
            detailTaalComboBox.ItemsSource = talen;
            detailTaalComboBox.DisplayMemberPath = "Naam";
            Taal taal = new Taal();
            taal.Naam = "Kies";
            talen.Insert(0, taal);
            languageComboBox.ItemsSource = talen;
            languageComboBox.DisplayMemberPath = "Naam";
            languageComboBox.SelectedIndex = 0;

            VanDatePicker.SelectedDate = new DateTime(2015, 1, 1);
            totDatePicker.SelectedDate = DateTime.Today;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            List<Seminarie> seminaries = GetSeminaries();
            seminarieViewSource.Source = seminaries;
        }

        private List<Seminarie> GetSeminaries()
        {
            HistoriekZoekTerm zoekterm = new HistoriekZoekTerm();
            if (VanDatePicker.SelectedDate != null)
            {
                zoekterm.StartDate = (DateTime)VanDatePicker.SelectedDate;
            }
            else zoekterm.StartDate = new DateTime(2013, 1, 1);
            if (totDatePicker.SelectedDate != null)
            {
                zoekterm.EndDate = (DateTime)totDatePicker.SelectedDate;
            }
            else zoekterm.EndDate = DateTime.Today;

            foreach (ConfocusCheckBoxListBoxItem item in seminarieComboBox.Items)
            {
                if (item.IsChecked)
                {
                    zoekterm.SeminarieIDs.Add(item.ID);
                }
            }

            if (StatusComboBox.SelectedIndex > 0)
                zoekterm.Status = StatusComboBox.Text;

            SeminarieServices sService = new SeminarieServices();
            return sService.GetSeminarieHistoriek(zoekterm);
        }

        private void seminarieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seminarieDataGrid.SelectedIndex > -1)
            {
                currentSeminarie = (Seminarie)seminarieDataGrid.SelectedItem;
                statusComboBox.Text = currentSeminarie.Status;
                typeComboBox.Text = currentSeminarie.Type;
                if (currentSeminarie.Taal_ID != null)
                {
                    Taal taal = new TaalServices().GetTaalByID((decimal)currentSeminarie.Taal_ID);
                    detailTaalComboBox.Text = taal.Naam;
                }
                else
                    detailTaalComboBox.SelectedIndex = -1;

                MarketingSeminarieLabel.Content = currentSeminarie.TitelEnDatum;
                GebruikerService gService = new GebruikerService();
                Gebruiker gebruiker = gService.GetUserByID(currentSeminarie.Verantwoordelijke_ID);
                if (gebruiker != null)
                    verantwoordelijke_IDComboBox.Text = gebruiker.Naam;
                ReloadDoelgroepen();
                LoadSeminarieMarketing();
                LoadSeminarie_ErkenningList();
                SetSessies(currentSeminarie.ID);
                LoadSprekers();
            }
        }

        private void SetSessies(decimal seminarieId)
        {
            SessieServices sService = new SessieServices();
            List<Sessie> sessies = sService.GetSessiesBySeminarieIDHistoriek(seminarieId);
            if (StatusComboBox.SelectedIndex > 0)
            {
                string status = StatusComboBox.Text;
                sessies = (from s in sessies where s.Status == status select s).ToList();
            }
            sessieViewSource.Source = sessies;
        }

        private void sessieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sessieDataGrid.SelectedIndex > -1)
            {
                currentSessie = (Sessie)sessieDataGrid.SelectedItem;
                //beamerComboBox.Text = currentSessie.Beamer;
                //laptopComboBox.Text = currentSessie.Laptop;
                statusComboBox1.Text = currentSessie.Status;
                status_LocatieComboBox.Text = currentSessie.Status_Locatie;
                if (currentSessie.Locatie_ID != null)
                {
                    LocatieServices lService = new LocatieServices();
                    Locatie locatie = lService.GetLocatieByID((decimal)currentSessie.Locatie_ID);
                    locatieComboBox.Text = locatie.Naam;
                }
                GebruikerService gService = new GebruikerService();
                if (currentSessie.Verantwoordelijke_Sessie_ID != null)
                {
                    Gebruiker sessieverant = gService.GetUserByID((int)currentSessie.Verantwoordelijke_Sessie_ID);
                    sessieVerantwoordelijkeComboBox.Text = sessieverant.Naam;
                }
                if (currentSessie.Verantwoordelijke_Terplaatse_ID != null)
                {
                    Gebruiker terplaatse = gService.GetUserByID((int)currentSessie.Verantwoordelijke_Terplaatse_ID);
                    terplaatseComboBox.Text = terplaatse.Naam;
                }
                HaalInschrijvingenOp();
            }
            else
                ClearSessieData();
        }

        private void SetInschrijvingen(decimal id)
        {
            //InschrijvingServices iService = new InschrijvingServices();
            //List<Inschrijving> inschrijvingen = iService.GetInschrijvingBySeminarieID(id);
            //inschrijvingViewSource.GroupDescriptions.Clear();
            //inschrijvingViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SeminarieNaam"));
            //inschrijvingViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SessieNaam"));
            //inschrijvingViewSource.Source = inschrijvingen;
            //InschrijvingSessieLabel.Content = "Sessie : " + sessie.Naam;
        }

        public void SetDeelnemers(Decimal id)
        {
            DeelnemerServices dService = new DeelnemerServices();
            List<Deelnemer> deelnemers = dService.GetFromSessie(id);
            deelnemerViewSource.GroupDescriptions.Clear();
            deelnemerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SessieNaam"));
            deelnemerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("Bedrijfsnaam"));
            deelnemerViewSource.Source = deelnemers;

        }

        private void ClearSessieData()
        {
            //beamerComboBox.SelectedIndex = -1;
            //laptopComboBox.SelectedIndex = -1;
            statusComboBox1.SelectedIndex = -1;
            status_LocatieComboBox.SelectedIndex = -1;
            locatieComboBox.SelectedIndex = 0;
            sessieVerantwoordelijkeComboBox.SelectedIndex = 0;
            terplaatseComboBox.SelectedIndex = 0;
        }


        /// <summary>
        /// Exporteren van de inschrijvingen vanuit de seminarie historiek
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportInschrijvingenButton_Click(object sender, RoutedEventArgs e)
        {
            if (inschrijvingenDataGrid.Items.Count > 0)
            {
                String[] koppen = new String[8] { "Bedrijf", "Datum inschrijving", "Factuurnummer", "Notitie", "Notitie factuur", "Status", "Via", "Gefactureerd" };
                String[][] data = new String[inschrijvingenDataGrid.Items.Count][];
                for (int i = 0; i < inschrijvingenDataGrid.Items.Count; i++)
                {
                    Inschrijving inschrijving = (Inschrijving)inschrijvingenDataGrid.Items[i];
                    String[] row = new String[8];
                    row[0] = inschrijving.Bedrijf.Naam;
                    row[1] = inschrijving.Datum_inschrijving.ToString();
                    row[2] = inschrijving.Factuurnummer;
                    row[3] = inschrijving.Notitie;
                    row[4] = inschrijving.Notitie_factuur;
                    row[5] = inschrijving.Status;
                    row[6] = inschrijving.Via;
                    row[7] = inschrijving.Gefactureerd == true ? "Ja" : "Nee";
                    data[i] = row;
                }
                ExcelDocument excel = new ExcelDocument();
                excel.SetColumnHeaders(koppen, 1, true, true, true);
                excel.InsertList(data, true, 2, new int[0], 20);

            }
            else
            {
                ConfocusMessageBox.Show("Er zijn geen gegevens om te exporteren!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > 0)
            {
                //IsChanged = true;
                Decimal N2ID = ((Niveau2)(combo.SelectedItem)).ID;
                Niveau3Services n3Service = new Niveau3Services();
                List<Niveau3> N3Lijst = n3Service.GetSelected(N2ID);
                Niveau3 kies = new Niveau3();
                kies.ID = 0;
                kies.Naam = "Kies";
                N3Lijst.Insert(0, kies);
                niveau3ComboBox.ItemsSource = N3Lijst;
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
            else
            {
                niveau3ComboBox.ItemsSource = new List<Niveau3>();
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex > 0)
            {
                //IsChanged = true;
                Decimal N1ID = ((Niveau1)(combo.SelectedItem)).ID;
                Niveau2Services n2Service = new Niveau2Services();
                List<Niveau2> N2Lijst = n2Service.GetSelected(N1ID);
                Niveau2 kies = new Niveau2();
                kies.ID = 0;
                kies.Naam = "Kies";
                N2Lijst.Insert(0, kies);
                niveau2ComboBox.ItemsSource = N2Lijst;
                niveau2ComboBox.DisplayMemberPath = "Naam";
            }
            else
            {
                niveau2ComboBox.ItemsSource = new List<Niveau2>();
                niveau2ComboBox.DisplayMemberPath = "Naam";

            }
        }

        private void totDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime? begindatum = VanDatePicker.SelectedDate;
            DateTime? einddatum = ((DatePicker)sender).SelectedDate;
            if (begindatum != null && einddatum > begindatum)
            {
                SeminarieServices sService = new SeminarieServices();
                List<Seminarie> seminaries = sService.GetSeminarieBetweenDates(begindatum, einddatum);
                List<ConfocusCheckBoxListBoxItem> items = new List<ConfocusCheckBoxListBoxItem>();
                foreach (Seminarie sem in seminaries)
                {
                    ConfocusCheckBoxListBoxItem boxItem = new ConfocusCheckBoxListBoxItem();
                    boxItem.ID = sem.ID;
                    boxItem.Name = sem.Titel;
                    boxItem.IsChecked = false;
                    items.Add(boxItem);
                }
                seminarieComboBox.ItemsSource = items;
            }
        }

        private void zoekContactButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekFunctieWindow ZC = new ZoekFunctieWindow();
            ZC.IsHistoriek = true;
            if (ZC.ShowDialog() == true)
            {
                if (ZC.Functie != null)
                {
                    HistoriekZoekTerm zoekterm = new HistoriekZoekTerm();
                    if (VanDatePicker.SelectedDate != null)
                    {
                        zoekterm.StartDate = (DateTime)VanDatePicker.SelectedDate;
                    }
                    else zoekterm.StartDate = new DateTime(2013, 1, 1);
                    if (totDatePicker.SelectedDate != null)
                    {
                        zoekterm.EndDate = (DateTime)totDatePicker.SelectedDate;
                    }
                    else zoekterm.EndDate = DateTime.Today;

                    foreach (ConfocusCheckBoxListBoxItem item in seminarieComboBox.Items)
                    {
                        if (item.IsChecked)
                        {
                            zoekterm.SeminarieIDs.Add(item.ID);
                        }
                    }
                    Functies fullFunctie = new FunctieServices().GetFunctieByID(ZC.Functie.ID);
                    zoekterm.FunctieID = fullFunctie.ID;
                    zoekterm.ContactID = fullFunctie.Contact_ID;
                    InschrijvingenService iService = new InschrijvingenService();
                    List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = iService.GetInschrijvingenBySearchTerms(zoekterm);
                    if (inschrijvingen.Count > 0)
                    {
                        ContactHistoriek CH = new ContactHistoriek(inschrijvingen);
                        CH.Show();
                    }
                    else
                        ConfocusMessageBox.Show("Geen inschrijvingen gevonden!", ConfocusMessageBox.Kleuren.Blauw);

                }
            }
        }

        private void zoekSprekerButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekSprekerWindow ZS = new ZoekSprekerWindow();
            if (ZS.ShowDialog() == true)
            {
                if (ZS.Spreker != null)
                {
                    HistoriekZoekTerm zoekterm = new HistoriekZoekTerm();
                    if (VanDatePicker.SelectedDate != null)
                    {
                        zoekterm.StartDate = (DateTime)VanDatePicker.SelectedDate;
                    }
                    else zoekterm.StartDate = new DateTime(2013, 1, 1);
                    if (totDatePicker.SelectedDate != null)
                    {
                        zoekterm.EndDate = (DateTime)totDatePicker.SelectedDate;
                    }
                    else zoekterm.EndDate = DateTime.Today;
                    foreach (ConfocusCheckBoxListBoxItem item in seminarieComboBox.Items)
                    {
                        if (item.IsChecked)
                        {
                            zoekterm.SeminarieIDs.Add(item.ID);
                        }
                    }
                    Spreker fullSpreker = new SprekerServices().GetSprekerById(ZS.Spreker.ID);
                    zoekterm.SprekerID = fullSpreker.ID;
                    zoekterm.ContactID = fullSpreker.Contact_ID;
                    SessieSprekerServices sService = new SessieSprekerServices();
                    List<Sessie_Spreker> sprekers = sService.GetSessieSprekersBySearchterm(zoekterm);
                    //seminarieViewSource.Source = seminaries;

                    if (sprekers.Count > 0)
                    {
                        SprekerHistoriek SH = new SprekerHistoriek(sprekers);
                        SH.Show();
                    }
                    else
                        ConfocusMessageBox.Show("Geen resultaten gevonden!", ConfocusMessageBox.Kleuren.Blauw);
                }
            }
        }

        #region Doelgroepen

        private void doelgroepNiveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ComboBox combo = (ComboBox)sender;
            //if (combo.SelectedIndex > -1)
            //{
            //    //IsChanged = true;
            //    Decimal N1ID = ((Niveau1)(combo.SelectedItem)).ID;
            //    Niveau2Services n2Service = new Niveau2Services();
            //    List<Niveau2> N2Lijst = n2Service.GetSelected(N1ID);
            //    doelgroepNiveau2ComboBox.ItemsSource = N2Lijst;
            //    doelgroepNiveau2ComboBox.DisplayMemberPath = "Naam";
            //}
        }

        private void onderwerpVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            MarketingTab.IsSelected = true;
        }

        private void ReloadDoelgroepen()
        {
            if (currentSeminarie != null)
            {
                Seminarie_DoelgroepServices sdService = new Seminarie_DoelgroepServices();
                List<Seminarie_Doelgroep> doelgroepen = sdService.GetDoelgroepenBySeminarieID(currentSeminarie.ID);
                seminarie_DoelgroepViewSource.Source = doelgroepen;
                if (doelgroepen.Contains(currentDoelgroep))
                {
                    seminarie_DoelgroepDataGrid.SelectedItem = currentDoelgroep;
                }
            }
        }

        private void seminarie_DoelgroepDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (seminarie_DoelgroepDataGrid.SelectedIndex > -1)
            //{
            //    Seminarie_Doelgroep doelgroep = (Seminarie_Doelgroep)seminarie_DoelgroepDataGrid.SelectedItem;
            //    doelgroepNiveau1ComboBox.Text = doelgroep.Niveau1.Naam;
            //    if (doelgroep.Niveau2 != null)
            //        doelgroepNiveau2ComboBox.Text = doelgroep.Niveau2.Naam;
            //    if (doelgroep.Niveau3 != null)
            //        doelgroepNiveau3ComboBox.Text = doelgroep.Niveau3.Naam;
            //    doelgroepActiefCheckBox.IsChecked = doelgroep.Actief;
            //}
        }

        private void doelgroepNiveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ComboBox combo = (ComboBox)sender;
            //if (combo.SelectedIndex > -1)
            //{
            //    //IsChanged = true;
            //    Decimal N2ID = ((Niveau2)(combo.SelectedItem)).ID;
            //    Niveau3Services n3Service = new Niveau3Services();
            //    List<Niveau3> N3Lijst = n3Service.GetSelected(N2ID);
            //    doelgroepNiveau3ComboBox.ItemsSource = N3Lijst;
            //    doelgroepNiveau3ComboBox.DisplayMemberPath = "Naam";
            //}
        }

        #endregion

        #region Marketingkanalen

        private void seminarie_MarketingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (seminarie_MarketingDataGrid.SelectedIndex > -1)
            {
                Seminarie_Marketing marketing = (Seminarie_Marketing)seminarie_MarketingDataGrid.SelectedItem;
                NieuwMarketingKanaal NM = new NieuwMarketingKanaal(currentUser, marketing.Marketing_ID);
                if (NM.ShowDialog() == true)
                {
                    LoadSeminarieMarketing();
                }
            }
        }

        private void LoadSeminarieMarketing()
        {
            if (currentSeminarie != null)
            {
                //data.Seminarie seminarie = (data.Seminarie)SeminarieComboBox.SelectedItem;
                Seminarie_MarketingServices smService = new Seminarie_MarketingServices();
                List<Seminarie_Marketing> seminarieMarketing = smService.GetMarketingBySeminarie(currentSeminarie.ID);
                if (seminarieMarketing.Count > 0)
                {
                    seminarie_MarketingViewSource.Source = seminarieMarketing;
                }
            }
        }

        private void MarketingVolgendeButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void seminarie_MarketingDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedIndex > -1)
            {
                Seminarie_Marketing mark = (Seminarie_Marketing)grid.SelectedItem;
                MarketingKanalenListBox.Text = mark.Marketingkanalen.Marketing_kanaal;
                opvolgdatumDatePicker1.SelectedDate = mark.Opvolgdatum;
                statusTextBox1.Text = mark.Status;
                OpmerkingTextBox1.Text = mark.Opmerking;
            }
        }

        #endregion

        #region Inschrijvingen

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.DefaultExt = ".csv";
            savedialog.Filter = "Excel|*.csv";

            //WeergaveInputWindow WI = new WeergaveInputWindow();
            if (savedialog.ShowDialog() == true)
            {
                string fileName = savedialog.FileName;//@"C:/Confocus Data/" + WI.Naam + ".csv";

                try
                {
                    InschrijvingenService iService = new InschrijvingenService();
                    SessieSprekerServices sService = new SessieSprekerServices();
                    decimal sessieid = 0;
                    ConfocusDBLibrary.Inschrijvingen selectedinschrijving;
                    if (inschrijvingenDataGrid.SelectedIndex > -1)
                    { 
                        selectedinschrijving = (ConfocusDBLibrary.Inschrijvingen)inschrijvingenDataGrid.SelectedItem;
                        sessieid = (decimal)selectedinschrijving.Sessie_ID;
                    }
                    else
                        sessieid = currentSessie.ID;
                    List<Seminarie_Erkenningen> erkenningen = new Seminarie_ErkenningenService().GetBySessieID(sessieid);
                    Sessie sessie = new SessieServices().GetSessieByID(sessieid);


                    CSVDocument doc = new CSVDocument(fileName);
                    List<string> headers = new List<string>()
                {
                    "Seminarienaam",
                    "Sessienaam",
                    "Datum Sessie",
                    "Locatie Sessie",
                    "Status Sessie",
                    "Erkenningsnummer Sessie",
                    "Achternaam",
                    "Voornaam",
                    "Oorspronkelijke titel",
                    "Telefoon contact",
                    "Email contact",
                    "Bedrijf",
                    "Telefoon bedrijf",
                    "Email bedrijf",
                    "Erkenningsnummer deelnemer",
                    "Type erkenning",
                    "Type",
                    "Status",
                    "Aanwezig",
                    "Datum inschrijving",
                    "Via",
                    "Dagtype",
                    "Aanwezigheid"
                };
                    doc.AddRow(headers);
                    foreach (Sessie_Spreker item in sessie_SprekerDataGrid.Items)
                    {
                        if (item.Sessie_ID == sessieid)
                        {
                            Sessie_Spreker spreker = sService.GetById(item.ID);
                            List<string> sprekers = new List<string>();
                            sprekers.Add(Helper.CleanForExcel(spreker.Sessie.SeminarieTitel));
                            sprekers.Add(Helper.CleanForExcel(spreker.Sessie.NaamDatumLocatie));
                            sprekers.Add(Helper.CleanForExcel(spreker.Sessie.Datum.ToString()));
                            sprekers.Add(Helper.CleanForExcel(spreker.Sessie.LocatieNaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Sessie.Status));
                            string erkenningsnummer = "";
                            foreach (Seminarie_Erkenningen erkenning in erkenningen)
                            {
                                if (erkenning.Erkenning.AttestType == spreker.Spreker.AttestType_ID)
                                {
                                    erkenningsnummer = erkenning.Nummer;
                                    break;
                                }
                            }
                            sprekers.Add(Helper.CleanForExcel(erkenningsnummer));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Achternaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Voornaam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Oorspronkelijke_Titel));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Telefoon));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Email));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Naam));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Telefoon));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Email));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Erkenningsnummer));
                            sprekers.Add(Helper.CleanForExcel(spreker.Spreker.AttestType1 == null ? "" : spreker.Spreker.AttestType1.Naam));
                            sprekers.Add("Spreker");
                            doc.AddRow(sprekers);
                        }
                    }

                    foreach (ConfocusDBLibrary.Inschrijvingen item in inschrijvingenDataGrid.Items)
                    {
                        if (item.Sessie_ID == sessieid)
                        {
                            ConfocusDBLibrary.Inschrijvingen inschrijving = iService.GetInschrijvingByID(item.ID);
                            List<string> deelnemer = new List<string>();
                            deelnemer.Add(inschrijving.Sessie.SeminarieTitel);
                            deelnemer.Add(inschrijving.Sessie.NaamDatumLocatie);
                            deelnemer.Add(inschrijving.Sessie.Datum.ToString());
                            deelnemer.Add(inschrijving.Sessie.LocatieNaam);
                            deelnemer.Add(inschrijving.Sessie.Status);
                            string erkenningsnummer = "";
                            foreach (Seminarie_Erkenningen erkenning in erkenningen)
                            {
                                if (erkenning.Erkenning.AttestType == inschrijving.Functies.Attesttype_ID)
                                {
                                    erkenningsnummer = erkenning.Nummer;
                                    break;
                                }
                            }
                            deelnemer.Add(erkenningsnummer);
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Contact.Achternaam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Contact.Voornaam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Oorspronkelijke_Titel);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Telefoon);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Email);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Bedrijf.Naam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Bedrijf.Telefoon);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Bedrijf.Email);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Erkenningsnummer);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.AttestType1 == null ? "" : inschrijving.Functies.AttestType1.Naam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }

                            deelnemer.Add("Klant");
                            deelnemer.Add(inschrijving.Status);
                            deelnemer.Add(inschrijving.Aanwezig == true ? "Ja" : "Nee");
                            deelnemer.Add(inschrijving.Datum_inschrijving.ToString());
                            deelnemer.Add(inschrijving.Via);
                            deelnemer.Add(inschrijving.Dagtype);
                            if (inschrijving.Aanwezig == true)
                                deelnemer.Add("Aanwezig");
                            else
                                deelnemer.Add("Afwezig");
                            doc.AddRow(deelnemer);
                        }
                    }

                    doc.Save();
                    Helper.OpenUrl(fileName);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void inschrijvingenVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            SprekerTabItem.IsSelected = true;
        }

        private void inschrijvingenDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedIndex > -1)
            {
                ConfocusDBLibrary.Inschrijvingen current = (ConfocusDBLibrary.Inschrijvingen)grid.SelectedItem;
                NieuwContact NC = new NieuwContact(currentUser, current.Functies);
                if (NC.ShowDialog() == true)
                {

                }
                //Inschrijvingen IW = new Inschrijvingen(Username, current.ID);
                //if (IW.ShowDialog() == true)
                //{
                //    //SeminarieReload();
                //}
            }
        }

        private void HaalInschrijvingenOp()
        {
            //IsInit = true;
            ConfocusDBLibrary.SessieServices sService = new ConfocusDBLibrary.SessieServices();
            List<ConfocusDBLibrary.Sessie> sessies = sService.GetSessiesBySeminarieIDHistoriek(currentSeminarie.ID);
            if (sessies.Count > 0)
            {
                currentSessie = sessies[0];
                InschrijvingenService iService = new InschrijvingenService();
                ObservableCollection<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = new ObservableCollection<ConfocusDBLibrary.Inschrijvingen>();
                foreach (Sessie mySessie in sessies)
                {
                    var tempInschrijvingen = iService.GetInschrijvingBySessieID(mySessie.ID);
                    foreach (var item in tempInschrijvingen)
                    {
                        inschrijvingen.Add(item as ConfocusDBLibrary.Inschrijvingen);
                    }
                }
                try
                {
                    inschrijvingenDataGrid.CancelEdit(DataGridEditingUnit.Row);
                    inschrijvingenViewSource.GroupDescriptions.Clear();
                    //inschrijvingenViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SeminarieNaam"));
                    inschrijvingenViewSource.GroupDescriptions.Add(new PropertyGroupDescription("SessieNaam"));
                    inschrijvingenViewSource.Source = inschrijvingen;
                }
                catch (Exception ex)
                {


                }
                int aantal = currentSessie.AantalInschrijvingen;
                //inschrijvingTotaalLabel.Content = aantal;
                //SeminarieInschrijvingenTotaalLabel.Content = currentSeminarie.AantalInschrijvingen;
                //sessieInschrijvingenTotaalLabel.Content = aantal;

            }
            else
                inschrijvingenViewSource.Source = new List<ConfocusDBLibrary.Inschrijvingen>();
            //IsInit = false;
        }


        private void inschrijvingenDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedIndex > -1)
            {
                ConfocusDBLibrary.Inschrijvingen myInschrijving = (ConfocusDBLibrary.Inschrijvingen)grid.SelectedItem;
                //attesttypeComboBox.Text = myInschrijving.Attesttype;
                //statusComboBox1.Text = myInschrijving.Status;
                //viaComboBox.Text = myInschrijving.Via;
                inschrijvingTotaalLabel.Content = GetAantalInschrijvingenBySessie(myInschrijving.Sessie_ID);
            }
        }

        private object GetAantalInschrijvingenBySessie(decimal? sessie_ID)
        {
            if (sessie_ID != null)
            {
                InschrijvingenService iService = new InschrijvingenService();
                int aantal = iService.GetAantalEffectieveInschrijvingenFromSessie((decimal)sessie_ID);
                return aantal.ToString();
            }
            else
                return "0";
        }

        #endregion

        #region Erkenningen

        private void ErkenningVolgendeButton_Click(object sender, RoutedEventArgs e)
        {
            SessieTabItem.IsSelected = true;
        }

        private void seminarie_ErkenningenDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (seminarie_ErkenningenDataGrid.SelectedIndex > -1)
            {
                Erkenning erkenning = ((Erkenning)((Seminarie_Erkenningen)seminarie_ErkenningenDataGrid.SelectedItem).Erkenning);
                ErkenningBeheer EB = new ErkenningBeheer(currentUser, erkenning.ID);
                if (EB.ShowDialog() == true)
                {

                }
            }

        }

        private void seminarie_ErkenningenDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seminarie_ErkenningenDataGrid.SelectedIndex > -1)
            {
                Seminarie_Erkenningen erkenning = (Seminarie_Erkenningen)seminarie_ErkenningenDataGrid.SelectedItem;
                seminarieErkenningComboBox.Text = erkenning.Erkenning.Naam;
                SetSeminarieErkenning(erkenning);

            }
        }

        private void SetSeminarieErkenning(Seminarie_Erkenningen erkenning)
        {
            datumDatePicker1.SelectedDate = erkenning.Datum;
            erkenningStatusComboBox.Text = erkenning.Status;
            //erkenningTypeComboBox.Text = erkenning.Type;
            aantalTextBox.Text = erkenning.Aantal.ToString();
            nummerTextBox.Text = erkenning.Nummer;
            //if (erkenning.Sessie == null)
            //{
            //    erkenningSessieComboBox.SelectedIndex = -1;
            //}
            //else
            //    erkenningSessieComboBox.Text = erkenning.Sessie.NaamDatumLocatie;
        }

        private void LoadSeminarie_ErkenningList()
        {
            if (currentSeminarie != null)
            {
                Seminarie_ErkenningenService eService = new Seminarie_ErkenningenService();
                List<Seminarie_Erkenningen> erkenningen = eService.GetBySeminarieID(currentSeminarie.ID);
                seminarie_ErkenningenViewSource.Source = erkenningen;
            }
        }

        #endregion

        #region Sprekers

        private void sessie_SprekerDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid grid = (DataGrid)sender;
            if (grid.SelectedIndex > -1)
            {
                Sessie_Spreker spreker = (Sessie_Spreker)grid.SelectedItem;
                NieuwContact NC = new NieuwContact(currentUser, spreker.Spreker);
                if (NC.ShowDialog() == true)
                {

                }
            }
        }

        private void sessie_SprekerDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ClearSessieSpreker()
        {
            sessieSprekerComboBox.Content = "";
            sprekerSessieComboBox.Content = "";
            sprekerAttesttypeComboBox.Content = "";
            sprekerOpmerkingTextBox.Text = "";
        }

        private void LoadSprekers()
        {
            if (currentSessie != null)
            {
                SessieSprekerServices sService = new SessieSprekerServices();
                List<Sessie_Spreker> sprekers = sService.GetSprekersBySeminarieID(currentSeminarie.ID);
                sessie_SprekerDataGrid.CancelEdit(DataGridEditingUnit.Row);
                sessie_SprekerViewSource.GroupDescriptions.Clear();
                sessie_SprekerViewSource.GroupDescriptions.Add(new PropertyGroupDescription("Sessie.NaamDatumLocatie"));
                sessie_SprekerViewSource.Source = sprekers;

                int sessieSprekersAantal = sService.GetAantalSprekersFromSessie(currentSessie.ID);
                totaalSprekerLabel.Content = "Totaal sprekers: " + sprekers.Count;
                deelnemerSprekerTotaalLabel.Content = sessieSprekersAantal.ToString();
                totaalSprekerLabel.Content = "Totaal sprekers: " + sessieSprekersAantal.ToString();
                //BindSelectedSessieSpreker();
            }
        }

        #endregion

        private void ZoekErkenningButton_Click(object sender, RoutedEventArgs e)
        {
            if (erkenningComboBox.SelectedIndex > 0)
            {
                HistoriekZoekTerm zoekterm = new HistoriekZoekTerm();
                if (VanDatePicker.SelectedDate != null)
                {
                    zoekterm.StartDate = (DateTime)VanDatePicker.SelectedDate;
                }
                else zoekterm.StartDate = new DateTime(2013, 1, 1);
                if (totDatePicker.SelectedDate != null)
                {
                    zoekterm.EndDate = (DateTime)totDatePicker.SelectedDate;
                }
                else zoekterm.EndDate = DateTime.Today;

                Erkenning erkenning = (Erkenning)erkenningComboBox.SelectedItem;
                zoekterm.ErkenningID = erkenning.ID;
                SeminarieServices sService = new SeminarieServices();
                List<Seminarie> seminaries = sService.GetSeminariesByErkenning(zoekterm);
                seminarieViewSource.Source = seminaries;
            }
        }

        private void zoekOpDoelgroepButton_Click(object sender, RoutedEventArgs e)
        {
            if (niveau1ComboBox.SelectedIndex > -1)
            {
                HistoriekZoekTerm zoekterm = new HistoriekZoekTerm();
                if (VanDatePicker.SelectedDate != null)
                {
                    zoekterm.StartDate = (DateTime)VanDatePicker.SelectedDate;
                }
                else zoekterm.StartDate = new DateTime(2013, 1, 1);
                if (totDatePicker.SelectedDate != null)
                {
                    zoekterm.EndDate = (DateTime)totDatePicker.SelectedDate;
                }
                else zoekterm.EndDate = DateTime.Today;
                Niveau1 n1 = (Niveau1)niveau1ComboBox.SelectedItem;
                zoekterm.Niveau1ID = n1.ID;
                if (niveau2ComboBox.SelectedIndex > 0)
                {
                    Niveau2 n2 = (Niveau2)niveau2ComboBox.SelectedItem;
                    zoekterm.Niveau2ID = n2.ID;
                    if (niveau3ComboBox.SelectedIndex > 0)
                    {
                        Niveau3 n3 = (Niveau3)niveau3ComboBox.SelectedItem;
                        zoekterm.Niveau3ID = n3.ID;
                    }
                    else
                        zoekterm.Niveau3ID = 0;
                }
                else
                {
                    zoekterm.Niveau2ID = 0;
                    zoekterm.Niveau3ID = 0;
                }

                if (languageComboBox.SelectedIndex > 0)
                    zoekterm.TaalID = ((Taal)languageComboBox.SelectedItem).ID;

                SeminarieServices sService = new SeminarieServices();
                List<Seminarie> seminaries = sService.GetSeminarieByDoelgroep(zoekterm);
                seminarieViewSource.Source = seminaries;
            }
        }

        private void filterVerantxoordelijkeButton_Click(object sender, RoutedEventArgs e)
        {
            HistoriekZoekTerm searchterms = new HistoriekZoekTerm();
            if (VanDatePicker.SelectedDate != null)
            {
                searchterms.StartDate = (DateTime)VanDatePicker.SelectedDate;
            }
            else searchterms.StartDate = new DateTime(2013, 1, 1);
            if (totDatePicker.SelectedDate != null)
            {
                searchterms.EndDate = (DateTime)totDatePicker.SelectedDate;
            }
            else searchterms.EndDate = DateTime.Today;
            if (sessieVerantwoordelijkeFilterComboBox.SelectedIndex > 0)
            {
                searchterms.VerantwoordelijkeSessie_ID = ((Gebruiker)sessieVerantwoordelijkeFilterComboBox.SelectedItem).ID;
            }
            if (TerplaatseFilterComboBox.SelectedIndex > 0)
            {
                searchterms.VerantwoordelijkeTerplaatse_ID = ((Gebruiker)TerplaatseFilterComboBox.SelectedItem).ID;
            }
            foreach (ConfocusCheckBoxListBoxItem item in seminarieComboBox.Items)
            {
                if (item.IsChecked)
                {
                    searchterms.SeminarieIDs.Add(item.ID);
                }
            }

            SeminarieServices sService = new SeminarieServices();
            List<Seminarie> seminaries = sService.GetSeminariesByVerantwoordelijken(searchterms);
            seminarieViewSource.Source = seminaries;
        }


        private void ExportSprekersButton_Click(object sender, RoutedEventArgs e)
        {
            if (sessie_SprekerDataGrid.HasItems)
            {
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.DefaultExt = ".csv";
                savedialog.Filter = "Excel|*.csv";
                Sessie sessie = (Sessie)sessieDataGrid.SelectedItem;


                //WeergaveInputWindow WI = new WeergaveInputWindow();
                if (savedialog.ShowDialog() == true)
                {
                    string fileName = savedialog.FileName;//@"C:/Confocus Data/" + WI.Naam + ".csv";
                    try
                    {
                        SessieSprekerServices sService = new SessieSprekerServices();
                        CSVDocument doc = new CSVDocument(fileName);
                        List<string> headers = new List<string>() {
                        "Seminarienaam",
                        "Sessienaam",
                        "Datum Sessie",
                        "Locatie Sessie",
                        "Status Sessie",
                        "Voornaam",
                        "Achternaam",
                        "Oorspronkelijke titel",
                        "Bedrijf", "Attesttype",
                        "Telefoonnummer",
                        "GSM-nummer",
                        "E-mailadres"
                    };
                        doc.AddRow(headers);
                        foreach (Sessie_Spreker item in sessie_SprekerDataGrid.Items)
                        {
                            if (item.Sessie_ID == sessie.ID)
                            {
                                Sessie_Spreker spreker = sService.GetById(item.ID);
                                List<string> sprekers = new List<string>();
                                sprekers.Add(Helper.CleanForExcel(spreker.Sessie.SeminarieTitel));
                                sprekers.Add(Helper.CleanForExcel(spreker.Sessie.NaamDatumLocatie));
                                sprekers.Add(Helper.CleanForExcel(spreker.Sessie.Datum.ToString()));
                                sprekers.Add(Helper.CleanForExcel(spreker.Sessie.LocatieNaam));
                                sprekers.Add(Helper.CleanForExcel(spreker.Sessie.Status));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Voornaam));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Achternaam));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Oorspronkelijke_Titel));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Naam));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Attesttype));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Telefoon));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Mobiel));
                                sprekers.Add(Helper.CleanForExcel(spreker.Spreker.Email));
                                doc.AddRow(sprekers);

                            }
                        }
                        doc.Save();
                        Helper.OpenUrl(fileName);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show("Fout bij het exporteren van de sprekers!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private void SeminarieInschrijvingenExportButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.DefaultExt = ".csv";
            savedialog.Filter = "Excel|*.csv";

            //WeergaveInputWindow WI = new WeergaveInputWindow();
            if (savedialog.ShowDialog() == true)
            {
                string fileName = savedialog.FileName;//@"C:/Confocus Data/" + WI.Naam + ".csv";
                SessieServices sService = new SessieServices();
                List<Sessie> sessies = new List<Sessie>();
                foreach (Seminarie sem in seminarieDataGrid.Items)
                {
                    List<Sessie> tempsessies = sService.GetSessiesBySeminarieIDHistoriek(sem.ID);
                    foreach (Sessie ses in tempsessies)
                    {
                        sessies.Add(ses);
                    }
                }

                try
                {
                    InschrijvingenService iService = new InschrijvingenService();
                    SessieSprekerServices spService = new SessieSprekerServices();
                    //SessieSprekerServices sprService = new SessieSprekerServices();


                    CSVDocument doc = new CSVDocument(fileName);
                    List<string> headers = new List<string>()
                    {
                        "Seminarienaam",
                        "Sessienaam",
                        "Datum Sessie",
                        "Locatie Sessie",
                        "Status Sessie",
                        "Erkenningsnummer Sessie",
                        "Voornaam",
                        "Achternaam",
                        "Oorspronkelijke titel",
                        "Telefoon contact",
                        "Email Contact",
                        "Bedrijf",
                        "Telefoon bedrijf",
                        "Email bedrijf",
                        "Erkenningsnummer deelnemer",
                        "Type erkenning",
                        "Type",
                        "Status",
                        "Aanwezig",
                        "Datum inschrijving",
                        "Via",
                        "Dagtype",
                        "Aanwezigheid"
                    };
                    doc.AddRow(headers);
                    foreach (Sessie ses in sessies)
                    {
                        List<Sessie_Spreker> sprekers = spService.GetSprekersBySessieID(ses.ID);
                        List<Seminarie_Erkenningen> erkenningen = new Seminarie_ErkenningenService().GetBySessieID(ses.ID);
                        foreach (Sessie_Spreker item in sprekers)
                        {
                            if (item.Sessie_ID == ses.ID)
                            {
                                Sessie_Spreker spreker = spService.GetById(item.ID);
                                List<string> sessprekers = new List<string>();
                                sessprekers.Add(Helper.CleanForExcel(spreker.Sessie.SeminarieTitel));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Sessie.NaamDatumLocatie));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Sessie.Datum.ToString()));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Sessie.LocatieNaam));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Sessie.Status));
                                string erkenningsnummer = "";
                                foreach (Seminarie_Erkenningen erkenning in erkenningen)
                                {
                                    if (erkenning.Erkenning.AttestType == spreker.Spreker.AttestType_ID)
                                    {
                                        erkenningsnummer = erkenning.Nummer;
                                        break;
                                    }
                                }
                                sessprekers.Add(Helper.CleanForExcel(erkenningsnummer));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Achternaam));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Voornaam));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Oorspronkelijke_Titel));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Telefoon));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Email));                                
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Naam));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Telefoon));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Email));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.Erkenningsnummer));
                                sessprekers.Add(Helper.CleanForExcel(spreker.Spreker.AttestType1 == null ? "" : spreker.Spreker.AttestType1.Naam));
                                sessprekers.Add("Spreker");
                                doc.AddRow(sessprekers);
                            }
                        }
                    }
                    foreach (Sessie ses in sessies)
                    {
                        List<Seminarie_Erkenningen> erkenningen = new Seminarie_ErkenningenService().GetBySessieID(ses.ID);

                        foreach (ConfocusDBLibrary.Inschrijvingen item in ses.Inschrijvingen)
                        {
                            ConfocusDBLibrary.Inschrijvingen inschrijving = iService.GetInschrijvingByID(item.ID);
                            List<string> deelnemer = new List<string>();
                            deelnemer.Add(inschrijving.Sessie.SeminarieTitel);
                            deelnemer.Add(inschrijving.Sessie.NaamDatumLocatie);
                            deelnemer.Add(inschrijving.Sessie.Datum.ToString());
                            deelnemer.Add(inschrijving.Sessie.LocatieNaam);
                            deelnemer.Add(inschrijving.Sessie.Status);
                            string erkenningsnummer = "";
                            foreach (Seminarie_Erkenningen erkenning in erkenningen)
                            {
                                if (erkenning.Erkenning.AttestType == inschrijving.Functies.Attesttype_ID)
                                {
                                    erkenningsnummer = erkenning.Nummer;
                                    break;
                                }
                            }
                            deelnemer.Add(erkenningsnummer);

                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Contact.Voornaam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Contact.Achternaam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Oorspronkelijke_Titel);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Telefoon);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Email);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Bedrijf.Naam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Bedrijf.Telefoon);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Bedrijf.Email);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.Erkenningsnummer);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }
                            try
                            {
                                deelnemer.Add(inschrijving.Functies.AttestType1 == null ? "" : inschrijving.Functies.AttestType1.Naam);
                            }
                            catch (Exception)
                            {
                                deelnemer.Add("");
                            }

                            deelnemer.Add("Klant");
                            deelnemer.Add(inschrijving.Status);
                            deelnemer.Add(inschrijving.Aanwezig == true ? "Ja" : "Nee");
                            deelnemer.Add(inschrijving.Datum_inschrijving.ToString());
                            deelnemer.Add(inschrijving.Via);
                            deelnemer.Add(inschrijving.Dagtype);
                            if (inschrijving.Aanwezig == true)
                                deelnemer.Add("Aanwezig");
                            else
                                deelnemer.Add("Afwezig");
                            doc.AddRow(deelnemer);
                        }

                    }
                    doc.Save();
                    Helper.OpenUrl(fileName);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void SeminariesSprekersExportButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.DefaultExt = ".csv";
            savedialog.Filter = "Excel|*.csv";

            //WeergaveInputWindow WI = new WeergaveInputWindow();
            if (savedialog.ShowDialog() == true)
            {
                string fileName = savedialog.FileName;//@"C:/Confocus Data/" + WI.Naam + ".csv";
                SessieSprekerServices sprService = new SessieSprekerServices();
                //SessieServices sService = new SessieServices();
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                foreach (Seminarie sem in seminarieDataGrid.Items)
                {
                    List<Sessie_Spreker> tempsprekers = sprService.GetSprekersBySeminarieID(sem.ID);
                    foreach (Sessie_Spreker spr in tempsprekers)
                    {
                        sprekers.Add(spr);
                    }
                }


                if (sprekers.Count > 0)
                {

                    try
                    {

                        CSVDocument doc = new CSVDocument(fileName);
                        List<string> headers = new List<string>() {
                        "Seminarienaam",
                        "Sessienaam",
                        "Datum Sessie",
                        "Locatie Sessie",
                        "Status Sessie",
                        "Voornaam",
                        "Achternaam",
                        "Oorspronkelijke titel",
                        "Bedrijf", "Attesttype",
                        "Telefoonnummer",
                        "GSM-nummer",
                        "E-mailadres"
                    };
                        doc.AddRow(headers);
                        foreach (Sessie_Spreker item in sprekers)
                        {
                            Sessie_Spreker spreker = sprService.GetById(item.ID);
                            List<string> excelsprekers = new List<string>();
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Sessie.SeminarieTitel));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Sessie.NaamDatumLocatie));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Sessie.Datum.ToString()));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Sessie.LocatieNaam));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Sessie.Status));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Voornaam));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Contact.Achternaam));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Oorspronkelijke_Titel));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Bedrijf.Naam));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Attesttype));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Telefoon));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Mobiel));
                            excelsprekers.Add(Helper.CleanForExcel(spreker.Spreker.Email));
                            doc.AddRow(excelsprekers);
                        }
                        doc.Save();
                        Helper.OpenUrl(fileName);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show("Fout bij het exporteren van de sprekers!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetFilter();
        }

        private void ResetFilter()
        {
            LoadData();
        }

        private void StatusComboBox_GotStylusCapture(object sender, StylusEventArgs e)
        {

        }

        private void InschrijvingactiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void exportDoelgroepenButton_Click(object sender, RoutedEventArgs e)
        {
            if (seminarie_DoelgroepDataGrid.Items.Count > 0)
            {
                try
                {
                    int totaalrijen = seminarie_DoelgroepDataGrid.Items.Count;
                    List<PrintableSeminarieDeelnemer> deelnemerlist = new List<PrintableSeminarieDeelnemer>();
                    string[][] arrDoelgroepen = new string[totaalrijen + 1][];
                    arrDoelgroepen[0] = new string[4] {"Seminarie", "Niveau 1","Niveau 2", "Niveau 3"};
                    List<Seminarie_Doelgroep> doelgroepen = new List<Seminarie_Doelgroep>();
                    int i = 1;
                    foreach (Seminarie_Doelgroep doelgroep in seminarie_DoelgroepDataGrid.Items)
                    {
                        doelgroepen.Add(doelgroep);
                        string seminarie = doelgroep.Seminarie.Titel;
                        string n1 = doelgroep.Niveau1.Naam;
                        string n2 = "";
                        if (doelgroep.Niveau2 != null)
                            n2 = doelgroep.Niveau2.Naam;
                        string n3 = "";
                        if (doelgroep.Niveau3 != null)
                            n3 = doelgroep.Niveau3.Naam;
                        string[] arrdoelgroep = new string[4] { seminarie, n1, n2, n3 };
                        arrDoelgroepen[i] = arrdoelgroep;
                        i++;
                    }

                    int[] widths = new int[4] { 50, 50, 50, 50 };
                    ExcelDocument excel = new ExcelDocument();
                    excel.InsertList(arrDoelgroepen, false, 1, widths, 20);

                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void seminarieDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid grid = (DataGrid)sender;
                if (grid.SelectedItem is Seminarie)
                {
                    Seminarie seminarie = (Seminarie)grid.SelectedItem;
                    List<Sessie> sessies = new SessieServices().GetSessiesBySeminarieID(seminarie.ID);

                    NieuweSeminarie NS = new NieuweSeminarie(currentUser, seminarie, null, sessies[0], true);
                    if (NS.ShowDialog() == true)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void TextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TextBox box = (TextBox)sender;
                Helper.OpenUrl(box.Text);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }
    }
}

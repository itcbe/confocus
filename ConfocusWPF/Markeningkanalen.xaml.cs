﻿using ConfocusClassLibrary;
using data = ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for Markeningkanalen.xaml
    /// </summary>
    public partial class Markeningkanalen : Window
    {
        private data.Gebruiker currentUser;
        private CollectionViewSource marketingkanaalViewSource;
        private List<data.Marketingkanalen> kanalen = new List<data.Marketingkanalen>();
        private List<data.Marketingkanalen> gevondenKanalen = new List<data.Marketingkanalen>();
        private data.Marketingkanalen currentKanaal;
        private Boolean IsScroll = true, IsButtonClick = true, datagridChange = true;

        public Markeningkanalen(data.Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "filter":
                    img = filterImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            if (marketingkanaalDataGrid.IsEnabled == true)
            {
                IsScroll = true;
                IsButtonClick = true;
                datagridChange = true;
                marketingkanaalViewSource.View.MoveCurrentToFirst();
                goUpdate();
                IsScroll = false;
                IsButtonClick = false;
                datagridChange = false;
            }

        }

        private void goUpdate()
        {
            if (marketingkanaalDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (marketingkanaalViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (marketingkanaalViewSource.View.CurrentPosition == marketingkanaalDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                marketingkanaalDataGrid.SelectedIndex = 0;
            if (marketingkanaalDataGrid.Items.Count != 0)
                marketingkanaalDataGrid.ScrollIntoView(marketingkanaalDataGrid.SelectedItem);
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            if (marketingkanaalDataGrid.IsEnabled == true)
            {
                IsScroll = true;
                IsButtonClick = true;
                datagridChange = true;
                marketingkanaalViewSource.View.MoveCurrentToPrevious();
                goUpdate();
                IsScroll = false;
                IsButtonClick = false;
                datagridChange = false;
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (marketingkanaalDataGrid.IsEnabled == true)
            {
                IsScroll = true;
                IsButtonClick = true;
                datagridChange = true;
                marketingkanaalViewSource.View.MoveCurrentToNext();
                goUpdate();
                IsScroll = false;
                IsButtonClick = false;
                datagridChange = false;
            }
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            if (marketingkanaalDataGrid.IsEnabled == true)
            {
                IsScroll = true;
                IsButtonClick = true;
                datagridChange = true;
                marketingkanaalViewSource.View.MoveCurrentToLast();
                goUpdate();
                IsScroll = false;
                IsButtonClick = false;
                datagridChange = false;
            }

        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwMarketingKanaal NM = new NieuwMarketingKanaal(currentUser, null);
            if (NM.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();

        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            marketingkanaalViewSource = ((CollectionViewSource)(this.FindResource("marketingkanaalViewSource")));
            data.MarketingkanalenServices kService = new data.MarketingkanalenServices();
            kanalen = kService.FindAll();
            // Load data by setting the CollectionViewSource.Source property:
            marketingkanaalViewSource.Source = kanalen;
            SetResults(kanalen.Count);
            goUpdate();
        }

        private void SetResults(int amount)
        {
            resultLabel.Content = amount + " resultaten";
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
                ZoekInLijst();
        }

        private void ZoekInLijst()
        {
            if (ZoekTextBox.Text != "" || statusZoekBox.Text != "" || taalZoekBox.Text != "")
            {
                gevondenKanalen.Clear();

                var query = from k in kanalen
                            select k;

                if (ZoekTextBox.Text != "")
                {
                    query = query.AsQueryable().Where(k => k.Marketing_kanaal.ToLower().Contains(ZoekTextBox.Text.ToLower()) || k.Sector.ToLower().Contains(ZoekTextBox.Text.ToLower()));
                }

                if (statusZoekBox.Text != "")
                {
                    query = query.AsQueryable().Where(k => k.Status.ToLower().Contains(statusZoekBox.Text.ToLower()));
                }

                if (taalZoekBox.Text != "")
                {
                    query = query.AsQueryable().Where(k => k.Taal.ToLower().Contains(taalZoekBox.Text.ToLower()));
                }

                //if (SectorCheckBox.IsChecked == true && StatusCheckBox.IsChecked != true && TaalCheckBox.IsChecked != true)
                //    query = query.AsQueryable().OrderBy(k => k.Sector);

                //if (StatusCheckBox.IsChecked == true && SectorCheckBox.IsChecked != true && TaalCheckBox.IsChecked != true)
                //    query = query.AsQueryable().OrderBy(k => k.Status);

                //if (SectorCheckBox.IsChecked != true && StatusCheckBox.IsChecked != true && TaalCheckBox.IsChecked == true)
                //    query = query.AsQueryable().OrderBy(k => k.Taal);


                //if (SectorCheckBox.IsChecked == true && StatusCheckBox.IsChecked == true && TaalCheckBox.IsChecked != true)
                //    query = query.AsQueryable().OrderBy(k => k.Sector).ThenBy(k => k.Status);

                //if (SectorCheckBox.IsChecked == true && StatusCheckBox.IsChecked != true && TaalCheckBox.IsChecked == true)
                //    query = query.AsQueryable().OrderBy(k => k.Sector).ThenBy(k => k.Taal);

                //if (StatusCheckBox.IsChecked == true && SectorCheckBox.IsChecked == true && TaalCheckBox.IsChecked == true)
                //    query = query.AsQueryable().OrderBy(k => k.Status).ThenBy(k => k.Taal);


                //if (SectorCheckBox.IsChecked == true && StatusCheckBox.IsChecked == true && TaalCheckBox.IsChecked != true)
                //    query = query.AsQueryable().OrderBy(k => k.Sector).ThenBy(k => k.Status).ThenBy(k => k.Taal);



                gevondenKanalen = query.ToList();

                marketingkanaalViewSource.Source = gevondenKanalen;
                SetResults(gevondenKanalen.Count);
            }
            else
            {
                marketingkanaalViewSource.Source = kanalen;
                SetResults(kanalen.Count);
            }

            goUpdate();

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBlock_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Tab || e.Key == Key.Enter)
            //{
            //marketingkanaalDataGrid.IsEnabled = true;
            TextBox box = (TextBox)sender;
            //    if (marketingkanaalDataGrid.SelectedIndex > -1)
            //    {
            //        data.Marketingkanalen kanaal = (data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem;
            switch (box.Name)
            {
                case "marketing_kanaalTextBlock":
                    if (currentKanaal.Marketing_kanaal != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "beschrijvingTextBlock":
                    if (currentKanaal.Beschrijving != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "bereikTextBlock":
                    if (currentKanaal.Bereik != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "sectorTextBlock":
                    if (currentKanaal.Sector != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "contactpersoonTextBlock":
                    if (currentKanaal.Contactpersoon != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "functieTextBlock":
                    if (currentKanaal.Functie != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "overeenkomstTextBlock":
                    if (currentKanaal.Overeenkomst != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "adverterenTextBlock":
                    if (currentKanaal.Adverteren != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "opmerkingenTextBlock":
                    if (currentKanaal.Opmerkingen != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "contacthistoriekTextBlock":
                    if (currentKanaal.Contacthistoriek != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "telefoonTextBox":
                    if (currentKanaal.Telefoon != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "emailTextBox":
                    if (currentKanaal.Email != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "websiteTextBox":
                    if (currentKanaal.Website != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "aankondiging_vanTextBlock":
                    if (currentKanaal.Aankondiging_van != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "feedbackTextBlock":
                    if (currentKanaal.Feedback != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "statusTextBlock":
                    if (currentKanaal.Status != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "mobielNummerTextBox":
                    if (currentKanaal.MobielNummers != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                default:
                    break;
            }

            //        SaveKanaalChanges(kanaal);
            //    }
            //}
        }

        private void SaveKanaalChanges(data.Marketingkanalen kanaal)
        {
            data.MarketingkanalenServices kService = new data.MarketingkanalenServices();
            try
            {
                kService.UpdateMarketingkanaal(kanaal);
                int index = marketingkanaalDataGrid.SelectedIndex;
                LoadData();
                ZoekInLijst();
                marketingkanaalDataGrid.SelectedIndex = index;
                marketingkanaalDataGrid.ScrollIntoView((data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            marketingkanaalDataGrid.IsEnabled = false;
            //CheckBox box = (CheckBox)sender;
            //data.Marketingkanalen kanaal = (data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem;
            //kanaal.Actief = box.IsChecked == true;

            //SaveKanaalChanges(kanaal);
        }

        private void marketingkanaalDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (marketingkanaalDataGrid.SelectedIndex > -1)
            {
                data.Marketingkanalen kanaal = (data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem;
                taalComboBox.Text = kanaal.Taal;
                currentKanaal = kanaal;
            }
        }

        private void taalComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (taalComboBox.Text != currentKanaal.Taal)
                marketingkanaalDataGrid.IsEnabled = false;
            //if (marketingkanaalDataGrid.SelectedIndex > -1)
            //{
            //    ComboBox box = (ComboBox)sender;
            //    data.Marketingkanalen kanaal = (data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem;
            //    kanaal.Taal = box.Text;

            //    SaveKanaalChanges(kanaal);
            //}
        }

        private void SectorCheckBox_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void hideDetails_Click(object sender, RoutedEventArgs e)
        {
            Button knop = (Button)sender;
            if (knop.Content.ToString() == "Toon details")
            {
                knop.Content = "Verberg details";
                detailsPanel.Height = new GridLength(380);
            }
            else
            {
                knop.Content = "Toon details";
                detailsPanel.Height = new GridLength(25);
            }
        }

        private void filterButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveKanaal();
            marketingkanaalDataGrid.IsEnabled = true;
        }

        private void SaveKanaal()
        {
            data.Marketingkanalen kanaal = (data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem;
            kanaal = GetKanaalFromForm(kanaal);
            SaveKanaalChanges(kanaal);
        }

        private data.Marketingkanalen GetKanaalFromForm(data.Marketingkanalen kanaal)
        {
            kanaal.Aankondiging_van = aankondiging_vanTextBlock.Text;
            kanaal.Marketing_kanaal = marketing_kanaalTextBlock.Text;
            kanaal.Sector = sectorTextBlock.Text;
            kanaal.Contacthistoriek = contacthistoriekTextBlock.Text;
            kanaal.Overeenkomst = overeenkomstTextBlock.Text;
            kanaal.Website = websiteTextBox.Text;
            kanaal.Contactpersoon = contactpersoonTextBlock.Text;
            kanaal.Telefoon = telefoonTextBox.Text;
            kanaal.Beschrijving = beschrijvingTextBlock.Text;
            kanaal.Bereik = bereikTextBlock.Text;
            kanaal.Feedback = feedbackTextBlock.Text;
            kanaal.Adverteren = adverterenTextBlock.Text;
            kanaal.Taal = taalComboBox.Text;
            kanaal.Functie = functieTextBlock.Text;
            kanaal.MobielNummers = mobielNummerTextBox.Text;
            kanaal.Opmerkingen = opmerkingenTextBlock.Text;
            kanaal.Status = statusTextBlock.Text;
            kanaal.Email = emailTextBox.Text;
            kanaal.Actief = actiefCheckBox.IsChecked == true;
            return kanaal;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (marketingkanaalDataGrid.IsEnabled == false)
            {
                if (MessageBox.Show("Er zijn nog onopgeslagen wijzigingen. wil je toch afsluiten?","Onopgeslagen wijzigen.", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
                
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            //marketingkanaalDataGrid.IsEnabled = false;
            //marketingkanaalDataGrid.IsEnabled = true;
            TextBox box = (TextBox)sender;
            //if (marketingkanaalDataGrid.SelectedIndex > -1)
            //{
            //    data.Marketingkanalen kanaal = (data.Marketingkanalen)marketingkanaalDataGrid.SelectedItem;
            switch (box.Name)
            {
                case "marketing_kanaalTextBlock":
                    if (currentKanaal.Marketing_kanaal != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "beschrijvingTextBlock":
                    if (currentKanaal.Beschrijving != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "bereikTextBlock":
                    if (currentKanaal.Bereik != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "sectorTextBlock":
                    if (currentKanaal.Sector != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "contactpersoonTextBlock":
                    if (currentKanaal.Contactpersoon != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "functieTextBlock":
                    if (currentKanaal.Functie != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "overeenkomstTextBlock":
                    if (currentKanaal.Overeenkomst != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "adverterenTextBlock":
                    if (currentKanaal.Adverteren != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "opmerkingenTextBlock":
                    if (currentKanaal.Opmerkingen != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "contacthistoriekTextBlock":
                    if (currentKanaal.Contacthistoriek != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "telefoonTextBox":
                    if (currentKanaal.Telefoon != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "emailTextBox":
                    if (currentKanaal.Email != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "websiteTextBox":
                    if (currentKanaal.Website != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "aankondiging_vanTextBlock":
                    if (currentKanaal.Aankondiging_van != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "feedbackTextBlock":
                    if (currentKanaal.Feedback != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "statusTextBlock":
                    if (currentKanaal.Status != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                case "mobielNummerTextBox":
                    if (currentKanaal.MobielNummers != box.Text)
                        marketingkanaalDataGrid.IsEnabled = false;
                    break;
                default:
                    break;
            }

            //    SaveKanaalChanges(kanaal);
            //}
        }

        private void websiteTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TextBox box = (TextBox)sender;
                Helper.OpenUrl(box.Text);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }
    }
}

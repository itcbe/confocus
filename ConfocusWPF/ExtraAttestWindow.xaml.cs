﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using data = ConfocusDBLibrary;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusClassLibrary;
using outlook = Microsoft.Office.Interop;
using ITCLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ExtraAttestWindow.xaml
    /// </summary>
    public partial class ExtraAttestWindow : Window
    {
        private decimal _id;
        private data.Inschrijvingen inschrijving;
        private data.Sessie_Spreker spreker;
        private string _kind = "Inschrijving";
        private string _dataFolder = "";
        private bool _rechtsdag = false;
        private data.Gebruiker _currentUser;
        public ExtraAttestWindow(decimal id, string kind, bool rechtsdag, data.Gebruiker user)
        {
            InitializeComponent();
            _id = id;
            _kind = kind;
            _rechtsdag = rechtsdag;
            _currentUser = user;
        }

        private void MailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_kind == "Inschrijving")
                    MailAttest();
                else
                    MailSprekerAttest();

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            _dataFolder = new data.Instelling_Service().Find().DataFolder;
            List<data.AttestType> attesten = new data.Attesttype_Services().FindActive();
            AttestTypeComboBox.ItemsSource = attesten;
            AttestTypeComboBox.DisplayMemberPath = "Naam";

            if (_kind == "Inschrijving")
            {
                inschrijving = new data.InschrijvingenService().GetInschrijvingByID(_id);
                List<data.Functies> functies = new data.FunctieServices().GetFunctieByContact((decimal)inschrijving.Functies.Contact_ID);

                FunctieComboBox.ItemsSource = functies;
                FunctieComboBox.DisplayMemberPath = "Functienaam";
                FunctieComboBox.Text = inschrijving.Functies.Functienaam;
                if (FunctieComboBox.SelectedIndex > -1)
                    AttestTypeComboBox.Text = ((data.Functies)FunctieComboBox.SelectedItem).Attesttype;

                firstNameLabel.Content = inschrijving.Functies.Contact.Voornaam;
                lastNameLabel.Content = inschrijving.Functies.Contact.Achternaam;
                emailLabel.Content = inschrijving.Functies.Email;
                CCEmailLabel.Content = inschrijving.CC_Email;
                //puntenTextBox.Text = (inschrijving.Aantal / 100).ToString();
                AttestTypeComboBox_SelectionChanged(AttestTypeComboBox, null);

            }
            else
            {
                spreker = new data.SessieSprekerServices().GetSessieSprekerByID(_id);
                List<data.Spreker> sprekers = new data.SprekerServices().GetSprekersByContact((decimal)spreker.Sessie_Spreker_Contact.ID);

                FunctieComboBox.ItemsSource = sprekers;
                FunctieComboBox.DisplayMemberPath = "SprekerNaamEnTitel";
                FunctieComboBox.Text = spreker.Spreker.SprekerNaamEnTitel;
                if (FunctieComboBox.SelectedIndex > -1)
                    AttestTypeComboBox.Text = ((data.Spreker)FunctieComboBox.SelectedItem).Attesttype;

                firstNameLabel.Content = spreker.Spreker.Contact.Voornaam;
                lastNameLabel.Content = spreker.Spreker.Contact.Achternaam;
                emailLabel.Content = spreker.Spreker.Email;
                ccLabel.Visibility = Visibility.Hidden;
                functieLabel.Content = "Spreker:";
                puntenLabel.Content = "Uren:";
                //CCEmailLabel.Content = inschrijving.CC_Email;
                puntenTextBox.Text = spreker.Uren;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MailAttest()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                data.Sessie sessie = new data.SessieServices().GetSessieByID((decimal)inschrijving.Sessie_ID);
                if (inschrijving.Aanwezig == true)
                {
                    string mailbody = StelMailBodyOpPlanning(sessie, inschrijving);
                    data.TaalServices tService = new data.TaalServices();
                    data.Taal taal = tService.GetTaalByID((decimal)inschrijving.Functies.Contact.Taal_ID);
                    data.Functies functie = GetFunctieFromDeelnemer(inschrijving);
                    string Mailto = functie.Email;
                    if (!string.IsNullOrEmpty(Mailto))
                    {
                        try
                        {
                            ConfocusClassLibrary.Attachment attest = GetAanwezigheidsAttest(inschrijving);
                            if (attest != null)
                            {
                                outlook.Outlook.Application oApp = new outlook.Outlook.Application();
                                outlook.Outlook._MailItem oMailItem = (outlook.Outlook._MailItem)oApp.CreateItem(outlook.Outlook.OlItemType.olMailItem);
                                DateTime mydate = new DateTime(2012, 01, 01);
                                oMailItem.Attachments.Add(attest.Link, outlook.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                                if (taal.Code == "FR")
                                    oMailItem.Subject = "Votre présence á la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                else
                                    oMailItem.Subject = "Uw aanwezigheid: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                                oMailItem.To = Mailto;
                                if (!string.IsNullOrEmpty(inschrijving.CC_Email))
                                    oMailItem.CC = inschrijving.CC_Email;
                                oMailItem.HTMLBody = mailbody;
                                oMailItem.Importance = outlook.Outlook.OlImportance.olImportanceHigh;
                                oMailItem.Display(false);

                                // Update inschrijving en zet attestverstuurd op ja.
                                inschrijving.Gewijzigd = DateTime.Now;
                                inschrijving.Gewijzigd_door = _currentUser.Naam;
                                new data.InschrijvingenService().SetAttestverstuurdOpJa(inschrijving);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(functie.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private string StelMailBodyOpPlanning(data.Sessie sessie, data.Inschrijvingen inschrijving)
        {
            string body = "";
            if (_rechtsdag)
            {
                data.Sessie fullSessie = new data.SessieServices().GetSessieByID((decimal)inschrijving.Sessie_ID);
                string filename = GetCorrectFilename(inschrijving);
                string naam = inschrijving.Functies.Contact.Achternaam;
                string aanspreking = GetAanspreking(inschrijving);
                body = System.IO.File.ReadAllText(filename);
                body = body.Replace("#aanspreking#", aanspreking);
                body = body.Replace("#naam#", naam);
                //body = body.Replace("#beginuur_sessie#", fullSessie.Beginuur);
                //body = body.Replace("#einduur_sessie#", fullSessie.Einduur);
                //body = body.Replace("#url_sessie#", fullSessie.URL);
                //body = body.Replace("#naam_sessie#", fullSessie.Naam);
                body = body.Replace("#datum_sessie#", ((DateTime)fullSessie.Datum).ToShortDateString());
                //body = body.Replace("#Periode#", "Voorjaar 2016");
                body = body.Replace("#Extra#", "");
                body = body.Replace("#locatie_sessie#", fullSessie.Locatie.Naam);
                //body = body.Replace("#naam_seminarie#", fullSessie.SeminarieTitel);
                body = body.Replace("#seminarie#", fullSessie.SeminarieTitel);
                //body = body.Replace("#naam_sessieverantwoordelijke#", verantwoordelijke);
                body = ITCHelper.TranslateToHTML(body);
                return body;
            }
            else
            {
                data.Sessie fullSessie = new data.SessieServices().GetSessieByID((decimal)inschrijving.Sessie_ID);
                if (fullSessie != null || fullSessie.ID != 0)
                {
                    

                    //todo Get correct file name.

                    string filename = GetCorrectFilename(inschrijving);
                    string naam = inschrijving.Functies.Contact.Achternaam;
                    string aanspreking = GetAanspreking(inschrijving);
                    string verantwoordelijke = (new data.GebruikerService().GetUserByID(fullSessie.Verantwoordelijke_Sessie_ID)).Naam;
                    string verantwoordelijkeEmail = (new data.GebruikerService().GetUserByID(fullSessie.Verantwoordelijke_Sessie_ID)).Email;
                    body = System.IO.File.ReadAllText(filename);
                    body = body.Replace("#aanspreking#", aanspreking);
                    body = body.Replace("#naam#", naam);
                    body = body.Replace("#beginuur_sessie#", fullSessie.Beginuur);
                    body = body.Replace("#einduur_sessie#", fullSessie.Einduur);
                    body = body.Replace("#url_sessie#", fullSessie.URL);
                    body = body.Replace("#naam_sessie#", fullSessie.Naam);
                    body = body.Replace("#datum_sessie#", ((DateTime)fullSessie.Datum).ToShortDateString());
                    //body = body.Replace("#Periode#", "Voorjaar 2016");
                    body = body.Replace("#Extra#", "");
                    body = body.Replace("#locatie_sessie#", fullSessie.Locatie.Naam);
                    body = body.Replace("#naam_seminarie#", fullSessie.SeminarieTitel);
                    body = body.Replace("#seminarie#", fullSessie.SeminarieTitel);
                    body = body.Replace("#naam_sessieverantwoordelijke#", verantwoordelijke);
                    body = body.Replace("#e-mailverantwoordelijkesessie#", verantwoordelijkeEmail);
                    body = ITCHelper.TranslateToHTML(body);
                    return body;
                }
                else
                    throw new System.Exception(data.ErrorMessages.SessieIsNull);

            }
        }

        private string GetSessieVerantwoordelijke(int? ID)
        {
            if (ID != null)
            {
                data.GebruikerService gService = new data.GebruikerService();
                data.Gebruiker geb = gService.GetUserByID((int)ID);
                return geb.Email;
            }
            else return "";

        }

        private string GetAanspreking(data.Inschrijvingen inschrijving)
        {
            var geslacht = inschrijving.Functies.Contact.Aanspreking;
            data.AansprekingServices aService = new data.AansprekingServices();
            data.Aanspreking aanspreking = aService.GetByLanguage((decimal)inschrijving.Functies.Contact.Taal_ID);

            if (geslacht.ToLower() == "v")
                return aanspreking.Aanspreking_vrouw;
            else
                return aanspreking.Aanspreking_man;
        }

        private string GetCorrectFilename(data.Inschrijvingen inschrijving)
        {
            String name = "Aanwezig";
            String language = new data.TaalServices().GetTaalByID((decimal)inschrijving.Functies.Contact.Taal_ID).Code;
            if (!_rechtsdag)
                name += " " + language;
            else
                name += "Rechtsdag " + language;
            data.AttestTemplate template = new data.AttestTemplate_Service().GetByName(name)[0];
            String filename = template.Uri;
            return filename;
        }

        private ConfocusClassLibrary.Attachment GetAanwezigheidsAttest(data.Inschrijvingen deelnemer)
        {
            if (deelnemer != null)
            {
                try
                {
                    string dagtype = GetDagTypeString(deelnemer);
                    object oMissing = System.Reflection.Missing.Value;
                    object template = GetTemplate(deelnemer);
                    string filename = deelnemer.Functies.Contact.VolledigeNaam + GetCleanDate((DateTime)deelnemer.Sessie.Datum) + "_";
                    data.Attesttype_Services aService = new data.Attesttype_Services();
                    data.AttestType type = aService.GetByID((decimal)((data.AttestType)AttestTypeComboBox.SelectedItem).ID);
                    filename += type.Naam + ".pdf";

                    string path = _dataFolder + @"\Aanwezigheids Attesten\" + filename;
                    data.Sessie sessie = new data.SessieServices().GetSessieByID((decimal)deelnemer.Sessie_ID);
                    data.Seminarie_ErkenningenService sService = new data.Seminarie_ErkenningenService();
                    List<data.Seminarie_Erkenningen> semErkenningen = sService.GetBySeminarieID((decimal)sessie.Seminarie_ID);
                    data.Functies currentFuntie;

                    if (FunctieComboBox.SelectedIndex > -1)
                    {
                        currentFuntie = new data.FunctieServices().GetFunctieByID(((data.Functies)FunctieComboBox.SelectedItem).ID);
                    }                   
                    else
                        currentFuntie = deelnemer.Functies;

                    WordDocument doc;
                    try
                    {
                        doc = new WordDocument(template, "Staand");
                    }
                    catch (System.Exception ex)
                    {
                        throw new System.Exception("Er is geen template beschikbaar voor deze erkenning!");
                    }

                    //doc.save(path);
                    List<String> fields = new List<string>();
                    //1
                    fields.Add(currentFuntie.Contact.Voornaam);
                    //2
                    fields.Add(currentFuntie.Contact.Achternaam);
                    //3
                    fields.Add(currentFuntie.Oorspronkelijke_Titel);
                    //4
                    fields.Add(currentFuntie.Bedrijf.Naam);
                    //5
                    fields.Add(inschrijving.Sessie.SeminarieTitel);
                    //6
                    if (sessie != null)
                        fields.Add(sessie.Naam);
                    else
                        fields.Add("");
                    //7
                    if (sessie.Locatie != null)
                        fields.Add(sessie.Locatie.Naam);
                    else
                        fields.Add("");
                    //8
                    fields.Add(((DateTime)sessie.Datum).ToShortDateString());
                    //9
                    fields.Add(sessie.Beginuur);
                    //10
                    fields.Add(sessie.Einduur);
                    //11
                    fields.Add(dagtype);
                    //12
                    fields.Add(currentFuntie.Erkenningsnummer);

                    string erknummersessie = "";
                    //string erknummercontact = "";
                    string erkAantal = "";
                    data.Functies functie = (data.Functies)FunctieComboBox.SelectedItem;
                    foreach (data.Seminarie_Erkenningen erk in semErkenningen)
                    {
                        if (erk.Sessie_ID == sessie.ID && functie.Attesttype == erk.Erkenning.Attest)
                        {
                            erknummersessie = erk.Nummer;
                            if (!string.IsNullOrEmpty(erk.TAantal) && erk.TAantal == deelnemer.TAantal)
                                erkAantal = erk.Aantal.ToString();
                            else if (!string.IsNullOrEmpty(deelnemer.TAantal))
                                erkAantal = deelnemer.TAantal;
                            else
                                erkAantal = erk.TAantal;
                            break;
                        }
                        else
                        {
                            if (currentFuntie.Attesttype == erk.Erkenning.Attest)
                            {
                                erknummersessie = erk.Nummer;
                                //erkAantal = deelnemer.DecimalAantal.ToString();
                            }
                        }

                    }
                    

                    //13
                    fields.Add(erknummersessie);
                    //14
                    if (string.IsNullOrEmpty(puntenTextBox.Text))
                        fields.Add(erkAantal);
                    else
                        fields.Add(puntenTextBox.Text);
                    //15
                    fields.Add(DateTime.Now.ToShortDateString());
                    //16
                    if (_rechtsdag)
                    {
                        List<string> sessiefields = GetSessieInfo(deelnemer);
                        foreach (string field in sessiefields)
                        {
                            fields.Add(field);
                        }
                    }
                    else
                    {
                        //16
                        fields.Add("");
                        //17
                        fields.Add("");
                        //18
                        fields.Add("");
                        //19
                        fields.Add("");
                    }
                    //20
                    fields.Add(currentFuntie.Type);
                    doc.Activate();
                    doc.ChangeFields(fields);
                    doc.SaveAsPDF(path);
                    //doc.save(path);
                    doc.quit();

                    ConfocusClassLibrary.Attachment attachment = new ConfocusClassLibrary.Attachment();
                    attachment.Naam = "Aanwezigheids attest";
                    attachment.Link = path;

                    return attachment;

                }
                catch (System.Exception ex)
                {
                    throw new System.Exception(ex.Message);
                }
            }
            else
                return null;
        }

        private List<string> GetSessieInfo(data.Inschrijvingen deelnemer)
        {
            List<string> results = new List<string>();
            data.SessieServices sService = new data.SessieServices();
            string sessie1 = "";
            string sessie2 = "";
            string sessie3 = "";
            string sessie4 = "";
            //Seminarie seminarie = new SeminarieServices().GetSeminarieByID((decimal)deelnemer.Sessie.Seminarie_ID);
            List<ConfocusDBLibrary.Inschrijvingen> inschrijvingen = new data.InschrijvingenService().GetInschrijvingBySeminarieIDAndFunctieID((decimal)deelnemer.Sessie.Seminarie_ID, deelnemer.Functies.Contact_ID);
            int i = 1;
            foreach (ConfocusDBLibrary.Inschrijvingen reg in inschrijvingen)
            {
                data.Sessie sess = sService.GetSessieByID((decimal)reg.Sessie_ID);
                if (!sess.Naam.ToLower().Contains("lunch"))
                {
                    switch (i)
                    {
                        case 1:
                            sessie1 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                        case 2:
                            sessie2 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                        case 3:
                            sessie3 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                        case 4:
                            sessie4 = sess.Naam + " - " + (reg.Aanwezig == true ? "aanwezig" : "niet aanwezig");
                            break;
                    }
                    i++;
                }
            }
            if (i == 3)
            {
                sessie3 = "";
                sessie4 = "";
            }
            else if (i == 4)
                sessie4 = "";

            results.Add(sessie1);
            results.Add(sessie2);
            results.Add(sessie3);
            results.Add(sessie4);
            return results;
        }

        private data.Functies GetFunctieFromDeelnemer(data.Inschrijvingen deelnemer)
        {
            try
            {
                data.FunctieServices fService = new data.FunctieServices();
                data.Functies theFunctie = new data.Functies();
                data.Functies functie = fService.GetFunctieByID((decimal)deelnemer.Functie_ID);

                return functie;
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }
        }

        private string GetDagTypeString(data.Inschrijvingen deelnemer)
        {
            try
            {
                data.DagType_Services dService = new data.DagType_Services();
                data.DagType dagtype = dService.GetByName(deelnemer.Dagtype);
                if (deelnemer.Functies.Contact.Taal_ID != null)
                {
                    data.Taal taal = new data.TaalServices().GetTaalByID((decimal)deelnemer.Functies.Contact.Taal_ID);
                    if (taal.Code == "NL")
                        return dagtype.Nederlands;
                    else
                        return dagtype.Frans;
                }
                else
                    return "volledige sessie";
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                return "volledige sessie";
            }
        }

        private object GetTemplate(data.Inschrijvingen deelnemer)
        {
            string template = _dataFolder + @"\Templates\";
            string filename = "";
            string taal = new data.TaalServices().GetTaalByID((decimal)deelnemer.Functies.Contact.Taal_ID).Code;
            data.AttestType attest = null;

            attest = (data.AttestType)AttestTypeComboBox.SelectedItem;
            //string contactAttest = deelnemer.Functies.Contact.Attesttype;
            if (attest != null)
            {
                if (_rechtsdag)
                    filename = attest.Naam +  "_RECHTSDAG_" + taal + ".docx";
                else
                    filename = attest.Naam + "_" + taal + ".docx";
            }
            else
            {
                if (_rechtsdag)
                    filename = "Administratief_" + "_RECHTSDAG_" + taal + ".docx"; 
                else
                    filename = "Administratief_" + taal + ".docx";
            }

            return template + filename;
        }

        private string GetCleanDate(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day;
        }


        // Spreker attest mail optie

        private void MailSprekerAttest()
        {
            try
            {
                try
                {
                    Helper.StartOutlook();
                }
                catch (System.Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
                List<data.Sessie_Spreker> sprekers = new List<data.Sessie_Spreker>();
                data.Sessie sessie = spreker.Sessie;
                string mailbody = StelSprekerMailBodyOpPlanning(sessie, spreker);
                data.TaalServices tService = new data.TaalServices();
                data.Taal taal = tService.GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID);
                string Mailto = spreker.Spreker.Email;
                if (!string.IsNullOrEmpty(Mailto))
                {
                    try
                    {
                        ConfocusClassLibrary.Attachment attest = GetSprekerAanwezigheidsAttest();
                        if (attest != null)
                        {
                            outlook.Outlook.Application oApp = new outlook.Outlook.Application();
                            outlook.Outlook._MailItem oMailItem = (outlook.Outlook._MailItem)oApp.CreateItem(outlook.Outlook.OlItemType.olMailItem);
                            DateTime mydate = new DateTime(2012, 01, 01);
                            oMailItem.Attachments.Add(attest.Link, outlook.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                            if (taal.Code == "FR")
                                oMailItem.Subject = "Votre présence á la formation Confocus: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                            else
                                oMailItem.Subject = "Uw aanwezigheid: Confocus opleiding: " + sessie.SeminarieTitel + ": " + sessie.Naam + " (" + ((DateTime)sessie.Datum).ToShortDateString() + " - " + sessie.Locatie.Naam + ")";
                            oMailItem.To = Mailto;
                            oMailItem.HTMLBody = mailbody;
                            oMailItem.Importance = outlook.Outlook.OlImportance.olImportanceHigh;
                            oMailItem.Display(false);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show(spreker.Spreker.Contact.VolledigeNaam + " heeft geen email adres.", "Opgelet!!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (System.Exception ex)
            {
                ConfocusMessageBox.Show("Fout: " + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private Attachment GetSprekerAanwezigheidsAttest()
        {
            if (spreker != null)
            {
                try
                {
                    object oMissing = System.Reflection.Missing.Value;
                    object template = GetSprekerTemplate(spreker);
                    string filename = spreker.Spreker.Contact.VolledigeNaam + GetCleanDate((DateTime)spreker.Sessie.Datum) + "_";
                    data.Attesttype_Services aService = new data.Attesttype_Services();
                    data.AttestType type = aService.GetByID(((data.AttestType)AttestTypeComboBox.SelectedItem).ID);
                    filename += type.Naam + ".pdf";

                    string path = _dataFolder + @"\Aanwezigheids Attesten\" + filename;
                    data.Sessie sessie = new data.SessieServices().GetSessieByID((decimal)spreker.Sessie_ID);
                    data.Seminarie_ErkenningenService sService = new data.Seminarie_ErkenningenService();
                    List<data.Seminarie_Erkenningen> semErkenningen = sService.GetBySeminarieID((decimal)sessie.Seminarie_ID);

                    WordDocument doc;
                    try
                    {
                        doc = new WordDocument(template, "Staand");
                    }
                    catch (System.Exception ex)
                    {
                        throw new System.Exception("Er is geen template beschikbaar voor deze erkenning!");
                    }

                    data.Spreker currentSpreker;
                    if (FunctieComboBox.SelectedIndex > -1)
                    {
                        currentSpreker = new data.SprekerServices().GetSprekerById(((data.Spreker)FunctieComboBox.SelectedItem).ID);
                    }
                    else
                        currentSpreker = spreker.Spreker;
                    //doc.save(path);
                    List<String> fields = new List<string>();
                    //1
                    fields.Add(currentSpreker.Contact.Voornaam);
                    //2
                    fields.Add(currentSpreker.Contact.Achternaam);
                    //3
                    fields.Add(currentSpreker.Oorspronkelijke_Titel);
                    //4
                    fields.Add(currentSpreker.Bedrijf.Naam);
                    //5
                    fields.Add(spreker.Sessie.SeminarieTitel);
                    //6
                    if (sessie != null)
                        fields.Add(sessie.Naam);
                    else
                        fields.Add("");
                    //7
                    if (sessie.Locatie != null)
                        fields.Add(sessie.Locatie.Naam);
                    else
                        fields.Add("");
                    //8
                    fields.Add(((DateTime)sessie.Datum).ToShortDateString());
                    //9
                    fields.Add(sessie.Beginuur);
                    //10
                    fields.Add(sessie.Einduur);
                    //11
                    fields.Add("");
                    //12
                    fields.Add(currentSpreker.Erkenningsnummer);

                    string erknummersessie = "";
                    //string erknummercontact = "";
                    //string erkAantal = "";
                    foreach (data.Seminarie_Erkenningen erk in semErkenningen)
                    {
                        if (erk.Sessie_ID == sessie.ID && spreker.Spreker.Attesttype == erk.Erkenning.Attest)
                        {
                            erknummersessie = erk.Nummer;
                            break;
                        }
                        else
                        {
                            if (spreker.Spreker.Attesttype == erk.Erkenning.Naam)
                            {
                                erknummersessie = erk.Nummer;
                            }
                        }

                    }
                    //13
                    fields.Add(erknummersessie);
                    //14
                    fields.Add("");
                    //15
                    if (puntenTextBox.Text != "")
                        fields.Add(puntenTextBox.Text);
                    else
                        fields.Add("");
                    //16
                    fields.Add(DateTime.Now.ToShortDateString());
                    //17
                    fields.Add(currentSpreker.Type);                   
                    //18
                    fields.Add("");
                    //19
                    fields.Add("");
                    //20
                    fields.Add("");
                    //21
                    fields.Add(spreker.Uren);

                    doc.Activate();
                    doc.ChangeFields(fields);
                    doc.SaveAsPDF(path);
                    //doc.save(path);
                    doc.quit();

                    ConfocusClassLibrary.Attachment attachment = new ConfocusClassLibrary.Attachment();
                    attachment.Naam = "Aanwezigheids attest";
                    attachment.Link = path;

                    return attachment;

                }
                catch (System.Exception ex)
                {
                    throw new System.Exception(ex.Message);
                }
            }
            else
                return null;
        }

        private object GetSprekerTemplate(data.Sessie_Spreker spreker)
        {
            string template = _dataFolder + @"\Templates\";
            string filename = "";
            string taal = new data.TaalServices().GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID).Code;
            data.AttestType attest = (data.AttestType)AttestTypeComboBox.SelectedItem;
            //if (spreker.Spreker.AttestType_ID != null)
            //{
            //    data.Attesttype_Services aService = new data.Attesttype_Services();
            //    attest = aService.GetByID((decimal)spreker.Spreker.AttestType_ID);
            //}
            //string contactAttest = deelnemer.Functies.Contact.Attesttype;
            if (attest != null)
            {
                if (attest.Naam != "Geen")
                {
                    List<data.AttestType> attesten = new data.Attesttype_Services().FindActive();
                    foreach (var myattest in attesten)
                    {
                        if (myattest.Naam == attest.Naam)
                        {
                            filename = myattest.Naam + "_Spreker_" + taal + ".docx";
                            break;
                        }
                    }
                    if (filename == "")
                        filename = "Administratief_Spreker" + taal + ".docx";
                }
                else
                    filename = "Administratief_Spreker" + taal + ".docx";
            }
            else
            {
                filename = "Administratief_Spreker" + taal + ".docx";
            }

            return template + filename;
        }

        private string StelSprekerMailBodyOpPlanning(data.Sessie sessie, data.Sessie_Spreker spreker)
        {
            data.Sessie fullSessie = new data.SessieServices().GetSessieByID((decimal)spreker.Sessie_ID);
            if (fullSessie != null || fullSessie.ID != 0)
            {
                string body = "";

                //todo Get correct file name.

                string filename = GetCorrectSprekerFilename(spreker);
                string naam = spreker.Spreker.Contact.Achternaam;
                string aanspreking = GetSprekerAanspreking(spreker);
                string verantwoordelijke = GetSessieVerantwoordelijke(fullSessie.Verantwoordelijke_Sessie_ID);

                //string agenda = SprekerEditor.ContentHtml;
                body = System.IO.File.ReadAllText(filename);
                body = body.Replace("#aanspreking#", aanspreking);
                body = body.Replace("#naam#", naam);
                body = body.Replace("#achternaam_spreker#", naam);
                body = body.Replace("#beginuur_sessie#", fullSessie.Beginuur);
                body = body.Replace("#einduur_sessie#", fullSessie.Einduur);
                body = body.Replace("#url_sessie#", fullSessie.URL);
                body = body.Replace("#naam_sessie#", fullSessie.Naam);
                body = body.Replace("#datum_sessie#", ((DateTime)fullSessie.Datum).ToShortDateString());
                body = body.Replace("#locatie_sessie#", fullSessie.Locatie.Naam);
                body = body.Replace("#naam_seminarie#", fullSessie.SeminarieTitel);
                body = body.Replace("#seminarie#", fullSessie.SeminarieTitel);
                body = body.Replace("#naam_sessieverantwoordelijke#", verantwoordelijke);
                body = body.Replace("#Extra#", "");

                return body;
            }
            else
                throw new System.Exception(data.ErrorMessages.SessieIsNull);
        }

        private string GetSprekerAanspreking(data.Sessie_Spreker spreker)
        {
            var geslacht = spreker.Spreker.Contact.Aanspreking;
            data.AansprekingServices aService = new data.AansprekingServices();
            data.Aanspreking aanspreking = aService.GetByLanguage((decimal)spreker.Spreker.Contact.Taal_ID);

            if (geslacht.ToLower() == "v")
                return aanspreking.Aanspreking_vrouw;
            else
                return aanspreking.Aanspreking_man;
        }

        private string GetCorrectSprekerFilename(data.Sessie_Spreker spreker)
        {
            string name = "";
            if (_kind == "Inschrijving")
                name = "Attest";
            else
                name = "AttestSpreker";
            string language = new data.TaalServices().GetTaalByID((decimal)spreker.Spreker.Contact.Taal_ID).Code;
            name += " " + language;
            data.AttestTemplate template = new data.AttestTemplate_Service().GetByName(name)[0];
            string filename = template.Uri;
            return filename;
        }

        private void FunctieComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (_kind == "Inschrijving")
            {
                if (FunctieComboBox.SelectedIndex > -1)
                    AttestTypeComboBox.Text = ((data.Functies)FunctieComboBox.SelectedItem).Attesttype;

            }
            else
                    if (FunctieComboBox.SelectedIndex > -1)
                    AttestTypeComboBox.Text = ((data.Spreker)FunctieComboBox.SelectedItem).Attesttype;
        }

        private void AttestTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AttestTypeComboBox.SelectedIndex > -1)
            {
                data.AttestType attesttype = (data.AttestType)AttestTypeComboBox.SelectedItem;
                decimal sesid = 0;
                if (_kind == "Inschrijving")
                    sesid = (decimal)inschrijving.Sessie_ID;
                else
                {
                    sesid = (decimal)spreker.Sessie_ID;
                }
                List<data.Seminarie_Erkenningen> erkenningen = new data.Seminarie_ErkenningenService().GetBySessieID(sesid);
                foreach (data.Seminarie_Erkenningen erkenning in erkenningen)
                {
                    if (erkenning.Erkenning.AttestType == attesttype.ID)
                    {
                        this.puntenTextBox.Text = erkenning.TAantal;
                        break;
                    }
                }
            }
        }
    }
}

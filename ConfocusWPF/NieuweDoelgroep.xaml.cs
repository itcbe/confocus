﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ConfocusDBLibrary;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuweDoelgroep.xaml
    /// </summary>
    public partial class NieuweDoelgroep : Window
    {
        private Gebruiker currentUser;

        public NieuweDoelgroep(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> n1List = n1Service.FindActive();
            niveau1ComboBox.ItemsSource = n1List;
            niveau1ComboBox.DisplayMemberPath = "Naam";
        }
      
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (niveau1ComboBox.SelectedIndex > -1)
            {
                Niveau1 n1 = (Niveau1)niveau1ComboBox.SelectedItem;
                Niveau2Services n2Service = new Niveau2Services();
                List<Niveau2> n2List = n2Service.GetSelected(n1.ID);
                niveau2ComboBox.ItemsSource = n2List;
                niveau2ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (niveau2ComboBox.SelectedIndex > -1)
            {
                Niveau2 n2 = (Niveau2)niveau2ComboBox.SelectedItem;
                Niveau3Services n3Service = new Niveau3Services();
                List<Niveau3> n3List = n3Service.GetSelected(n2.ID);
                niveau3ComboBox.ItemsSource = n3List;
                niveau3ComboBox.DisplayMemberPath = "Naam";
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (isValid())
            {
                Doelgroep doelgroep = new Doelgroep();
                doelgroep.Naam = naamTextBox.Text;
                doelgroep.Niveau1_ID = (niveau1ComboBox.SelectedItem as Niveau1).ID;
                if (niveau2ComboBox.SelectedIndex > -1)
                {
                    doelgroep.Niveau2_ID = (niveau2ComboBox.SelectedItem as Niveau2).ID;
                }
                if (niveau3ComboBox.SelectedIndex > -1)
                {
                    doelgroep.Niveau3_ID = (niveau3ComboBox.SelectedItem as Niveau3).ID;
                }
                doelgroep.Actief = true;
                doelgroep.Aangemaakt = DateTime.Now;
                doelgroep.Aangemaakt_door = currentUser.Naam;

                DoelgroepServices dService = new DoelgroepServices();
                dService.SaveDoelgroep(doelgroep);
                DialogResult = true;
            }
        }

        private bool isValid()
        {
            String errorText = "";
            bool valid = true;
            if (naamTextBox.Text == "")
            {
                errorText += "* Er is geen naam aan de doelgroep gegeven!\n";
                valid = false;
            }
            if (niveau1ComboBox.SelectedIndex < 0)
            {
                errorText += "* Niveau 1 is verplicht!";
                valid = false;
            }

            if (errorText != "")
            {
                ConfocusMessageBox.Show(errorText, ConfocusMessageBox.Kleuren.Rood);
            }

            return valid;
        }
    }
}

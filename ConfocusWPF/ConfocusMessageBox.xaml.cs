﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ConfocusMessageBox.xaml
    /// </summary>
    public partial class ConfocusMessageBox : Window
    {
        public ConfocusMessageBox()
        {
            InitializeComponent();
        }

        public enum Kleuren
        {
            Rood ,
            Blauw 
        };

        public static bool? Show(String message, Kleuren kleur)
        {
            ConfocusMessageBox msgBox = new ConfocusMessageBox();
            if (kleur.Equals(Kleuren.Rood))
                msgBox.Message.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            else
                msgBox.Message.Foreground = new SolidColorBrush(Color.FromRgb(0,2,255));
            msgBox.Message.Text = message;
            return msgBox.ShowDialog();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                DialogResult = true;
        }
    }
}

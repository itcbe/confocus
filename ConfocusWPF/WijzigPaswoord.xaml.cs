﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for WijzigPaswoord.xaml
    /// </summary>
    public partial class WijzigPaswoord : Window
    {
        private Gebruiker currentUser;
        private Gebruiker UserToUpdate;

        public WijzigPaswoord(Gebruiker user, Gebruiker toupdaten)
        {
            InitializeComponent();
            this.currentUser = user;
            UserToUpdate = toupdaten;
            gebruikerLabel.Content = UserToUpdate.Naam;
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsFormValid())
            {
                Encriptor encript = new Encriptor();
                GebruikerService service = new GebruikerService();
                UserToUpdate.Paswoord = encript.EncodePassword(nieuwPasswordBox.Password);
                try
                {
                    service.SaveGebruikerWijzigingen(UserToUpdate);
                    DialogResult = true;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate passErrorTemplate = (ControlTemplate)(this.FindResource("PasswordBoxErrorControlTemplate"));
            ControlTemplate passNormalTemplate = (ControlTemplate)(this).FindResource("PasswordBoxControlTemplate");
            if (nieuwPasswordBox.Password != herhaalPasswordBox.Password)
            {
                valid = false;
                Helper.SetPassTextBoxInError(herhaalPasswordBox, passErrorTemplate);
                errorMessage += "De paswoorden komen niet overeen!\n";
            }
            else
                Helper.SetPassTextBoxToNormal(herhaalPasswordBox, passNormalTemplate);

            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);
            return valid;
        }
    }
}

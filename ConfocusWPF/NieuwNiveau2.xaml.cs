﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for NieuwNiveau2.xaml
    /// </summary>
    public partial class NieuwNiveau2 : Window
    {
        private Gebruiker currentUser;

        public NieuwNiveau2(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            naamTextBox.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Niveau1Services service = new Niveau1Services();
            List<Niveau1> N1Lijst = service.FindAll();
            naamComboBox.ItemsSource = N1Lijst;
            naamComboBox.DisplayMemberPath = "Naam";

        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Niveau2 n2 = GetNiveau2FromForm();
            Niveau2Services service = new Niveau2Services();
            if (n2 != null)
            {
                service.SaveNiveau2(n2);
                DialogResult = true;
            }        
        }

        private Niveau2 GetNiveau2FromForm()
        {
            if (IsFormValid())
            {
                Niveau2 n2 = new Niveau2();
                n2.Naam = naamTextBox.Text;
                n2.Naam_FR = beschrijvingTextBox.Text;
                if (naamComboBox.SelectedIndex > -1)
                    n2.Niveau1_ID = ((Niveau1)naamComboBox.SelectedItem).ID;
                n2.Actief = actiefCheckBox.IsChecked == true;
                n2.Aangemaakt = DateTime.Now;
                n2.Aangemaakt_door = currentUser.Naam;

                return n2;
            }
            else
                return null;
        }

        private bool IsFormValid()
        {
            Boolean valid = true;
            String errorMessage = "";
            ControlTemplate errorTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseErrorControlTemplate"));
            ControlTemplate normalTemplate = (ControlTemplate)(this.FindResource("TextBoxBaseControlTemplate"));
            if (naamTextBox.Text == "")
            {
                errorMessage += "Er is geen naam ingegeven!\n";
                Helper.SetTextBoxInError(naamTextBox, errorTemplate);
                valid = false;
            }
            else
                Helper.SetTextBoxToNormal(naamTextBox, normalTemplate);
            if (naamComboBox.Text == "")
            {
                errorMessage += "Er is geen niveau 1 gekozen!\n";
                valid = false;
            }
            if (!valid)
                ConfocusMessageBox.Show(errorMessage, ConfocusMessageBox.Kleuren.Rood);

            return valid;
        }
    }
}

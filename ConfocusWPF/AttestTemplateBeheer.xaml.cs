﻿using ConfocusDBLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for AttestTemplateBeheer.xaml
    /// </summary>
    public partial class AttestTemplateBeheer : Window
    {
        private CollectionViewSource attestTemplateViewSource;
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private List<AttestTemplate> templates = new List<AttestTemplate>();
        private List<AttestTemplate> gevondenTemplates = new List<AttestTemplate>();
        private Gebruiker currentUser;
        private string _dataFolder = "";

        public AttestTemplateBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _dataFolder = new Instelling_Service().Find().DataFolder;
            attestTemplateViewSource = ((CollectionViewSource)(this.FindResource("attestTemplateViewSource")));
            LoadAttestTemplates();
        }

        private void LoadAttestTemplates()
        {
            templates = (new AttestTemplate_Service()).FindActive();
            // Load data by setting the CollectionViewSource.Source property:
            attestTemplateViewSource.Source = templates;
            if (templates.Count == 0)
            {
                DisableControls();
            }
            else
                EnableControls();
        }

        private void EnableControls()
        {
            uriTextBox.IsEnabled = true;
            actiefCheckBox.IsEnabled = true;
            browseButton.IsEnabled = true;
        }

        private void DisableControls()
        {
            uriTextBox.IsEnabled = false;
            actiefCheckBox.IsEnabled = false;
            browseButton.IsEnabled = false;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTemplateViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTemplateViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTemplateViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            attestTemplateViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweAttestTemplate NA = new NieuweAttestTemplate(currentUser);
            if (NA.ShowDialog() == true)
            {
                LoadAttestTemplates();
            }
        }


        private void GoUpdate()
        {
            if (attestTemplateDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (attestTemplateViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (attestTemplateViewSource.View.CurrentPosition == attestTemplateDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                attestTemplateDataGrid.SelectedIndex = 0;
            if (attestTemplateDataGrid.Items.Count != 0)
                attestTemplateDataGrid.ScrollIntoView(attestTemplateDataGrid.SelectedItem);
        }

        private void naamTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                if (attestTemplateDataGrid.SelectedIndex > -1)
                {
                    String naam = ((TextBox)sender).Text;
                    AttestTemplate attest = (AttestTemplate)attestTemplateDataGrid.SelectedItem;
                    attest.Naam = naam;
                    attest.Gewijzigd_door = currentUser.Naam;
                    attest.Gewijzigd = DateTime.Now;
                    AttestTemplate_Service aService = new AttestTemplate_Service();
                    try
                    {
                        aService.Update(attest);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
            }
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (attestTemplateDataGrid.SelectedIndex > -1)
            {
                AttestTemplate attest = (AttestTemplate)attestTemplateDataGrid.SelectedItem;
                attest.Actief = ((CheckBox)sender).IsChecked == true;
                attest.Gewijzigd_door = currentUser.Naam;
                attest.Gewijzigd = DateTime.Now;
                AttestTemplate_Service aService = new AttestTemplate_Service();
                try
                {
                    aService.Update(attest);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            String zoektext = ((TextBox)sender).Text;
            if (zoektext != "")
            {
                gevondenTemplates.Clear();

                gevondenTemplates = (from a in templates
                                    where a.Naam.ToLower().Contains(zoektext.ToLower())
                                    select a).ToList();

                attestTemplateViewSource.Source = gevondenTemplates;
            }
            else
            {
                attestTemplateViewSource.Source = templates;
            }
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog FD = new OpenFileDialog();
            FD.InitialDirectory = _dataFolder +  "Templates\\";
            if (FD.ShowDialog() == true)
            {
                uriTextBox.Text = FD.FileName;
            }
        }

        private void uriTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (attestTemplateDataGrid.SelectedIndex > -1)
            {
                String uri = ((TextBox)sender).Text;
                AttestTemplate attest = (AttestTemplate)attestTemplateDataGrid.SelectedItem;
                attest.Uri = uri;
                attest.Gewijzigd_door = currentUser.Naam;
                attest.Gewijzigd = DateTime.Now;
                AttestTemplate_Service aService = new AttestTemplate_Service();
                try
                {
                    aService.Update(attest);
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
        }
    }
}

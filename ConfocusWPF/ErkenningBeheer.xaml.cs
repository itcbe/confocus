﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ErkenningBeheer.xaml
    /// </summary>
    public partial class ErkenningBeheer : Window
    {
        private CollectionViewSource erkenningViewSource;
        private List<Erkenning> Erkenningen = new List<Erkenning>();
        private List<Erkenning> gevondenErkenningnen = new List<Erkenning>();
        private List<Erkenning> gewijzigdeErkenningen = new List<Erkenning>();
        private Decimal? ID;
        private Boolean IsScroll = false, IsButtonClick = false;
        private Gebruiker currentUser;

        public ErkenningBeheer(Gebruiker user, Decimal? id)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Erkenning beheer : Ingelogd als " + currentUser.Naam;
            if (id != null)
            {
                this.ID = id;
            }
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            erkenningViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            erkenningViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            erkenningViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            erkenningViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweErkenning NE = new NieuweErkenning(currentUser);
            if (NE.ShowDialog() == true)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            Attesttype_Services aService = new Attesttype_Services();
            List<AttestType> attesten = aService.FindActive();
            attestComboBox.ItemsSource = attesten;
            attestComboBox.DisplayMemberPath = "Naam";

            LoadErkenningen();
            if (ID != null)
            {
                int index = 0;
                foreach (var item in erkenningDataGrid.Items)
                {
                    Erkenning er = (Erkenning)item;
                    if (er.ID == ID)
                    {
                        erkenningDataGrid.SelectedIndex = index;
                        break;
                    }
                    index++;
                }
            }
            else
                goUpdate();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();
        }

        private void SaveWijzigingen()
        {
            if (gewijzigdeErkenningen.Count > 0)
            {
                ErkenningServices service = new ErkenningServices();
                foreach (Erkenning er in gewijzigdeErkenningen)
                {
                    er.Gewijzigd = DateTime.Now;
                    er.Gewijzigd_door = currentUser.Naam;
                    try
                    {
                        service.SaveErkenningWijzigingen(er);
                    }
                    catch (Exception ex)
                    {
                        ConfocusMessageBox.Show("Erkenning niet opgeslagen wegens een fout!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                    }
                }
                gewijzigdeErkenningen.Clear();
                ConfocusMessageBox.Show("Wijzigingen opgeslagen.", ConfocusMessageBox.Kleuren.Blauw);
            }
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            ZoekInLijst();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));

        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                //case "search":
                //    img = searchImage;
                //    break;
                case "new":
                    img = newImage;
                    break;
                //case "save":
                //    img = saveImage;
                //    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            erkenningViewSource = ((CollectionViewSource)(this.FindResource("erkenningViewSource")));
            // Load data by setting the CollectionViewSource.Source property
            LoadData();
        }

        private void goUpdate()
        {

            if (erkenningDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (erkenningViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (erkenningViewSource.View.CurrentPosition == erkenningDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                erkenningDataGrid.SelectedIndex = 0;
            if (erkenningDataGrid.Items.Count != 0)
                erkenningDataGrid.ScrollIntoView(erkenningDataGrid.SelectedItem);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void erkenningDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsButtonClick)
            {
                IsScroll = true;
                goUpdate();
                IsScroll = false;
            }
        }

        private void Control_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Helper.CheckEmail(emailTextBox.Text) || String.IsNullOrEmpty(emailTextBox.Text))
            {
                Erkenning ER = (Erkenning)erkenningDataGrid.SelectedItem;
                ER.Naam = naamTextBox.Text;
                ER.Instantie = instantieTextBox.Text;
                ER.Contactpersoon = contactpersoonTextBox.Text;
                ER.Telefoonnummer = telefoonnummerTextBox.Text;
                ER.Email = emailTextBox.Text;
                ER.Attest = attestComboBox.Text;
                ER.Erkend = erkendCheckBox.IsChecked == true;
                ER.Website = websiteTextBox.Text;
                ER.Notitie = notitieTextBox.Text;
                ER.Actief = actiefCheckBox.IsChecked == true;
                ER.Gewijzigd = DateTime.Now;
                ER.Gewijzigd_door = currentUser.Naam;

                ErkenningServices service = new ErkenningServices();
                try
                {
                    service.SaveErkenningWijzigingen(ER);
                    int index = erkenningDataGrid.SelectedIndex;
                    LoadErkenningen();
                    erkenningDataGrid.SelectedIndex = index;
                }
                catch (Exception ex)
                {
                    ConfocusMessageBox.Show("Erkenning niet opgeslagen wegens een fout!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }
            }
            else
                ConfocusMessageBox.Show(ErrorMessages.EmailInvalid, ConfocusMessageBox.Kleuren.Rood);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gewijzigdeErkenningen.Count > 0)
            {
                if (MessageBox.Show("Er zijn nog wijzigingen die niet zijn opgeslaan!\nWil je deze nog opslaan?", "Onopgeslagen wijzigingen", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SaveWijzigingen();
                }
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekInLijst();
        }

        private void ZoekInLijst()
        {
            gevondenErkenningnen.Clear();

            gevondenErkenningnen = (from er in Erkenningen
                                    where er.Naam.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                    select er).ToList();

            if (gevondenErkenningnen.Count > 0)
                erkenningViewSource.Source = gevondenErkenningnen;
            else
                erkenningViewSource.Source = Erkenningen;

            goUpdate();
        }

        private void gridTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                Erkenning erkenning = (Erkenning)erkenningDataGrid.SelectedItem;
                bool valid = true;
                switch (text.Name)
                {
                    case "gridNaamTextBox":
                        erkenning.Naam = text.Text;
                        break;
                    case "gridInstantieTextBox":
                        erkenning.Instantie = text.Text;
                        break;
                    case "gridContactTextBox":
                        erkenning.Contactpersoon = text.Text;
                        break;
                    case "gridTelefoonTextBox":
                        erkenning.Telefoonnummer = text.Text;
                        break;
                    case "gridEmailTextBox":
                        erkenning.Email = text.Text;
                        break;
                    case "gridWebsiteTextBox":
                        erkenning.Website = text.Text;
                        break;
                    case "gridAttestTextBox":
                        erkenning.Attest = text.Text;
                        break;
                    case "gridNotitieTextBox":
                        erkenning.Notitie = text.Text;
                        break;
                }
                if (valid)
                {
                    SaveGridWijzigingen(erkenning);
                }
                else
                    ConfocusMessageBox.Show("Er is geen geldig getal ingevuld!", ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void gridErkendCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            Erkenning erkenning = (Erkenning)erkenningDataGrid.SelectedItem;
            erkenning.Erkend = box.IsChecked == true;
            SaveGridWijzigingen(erkenning);
        }

        private void attestComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > 0)
            {
                Erkenning erkenning = (Erkenning)erkenningDataGrid.SelectedItem;
                erkenning.Attest = box.Text;
                erkenning.AttestType = ((AttestType)box.SelectedItem).ID;
                SaveGridWijzigingen(erkenning);
            }
        }

        private void SaveGridWijzigingen(Erkenning erkenning)
        {
            try
            {
                ErkenningServices service = new ErkenningServices();              
                service.SaveErkenningWijzigingen(erkenning);
                int index = erkenningDataGrid.SelectedIndex;
                LoadErkenningen();
                erkenningDataGrid.SelectedIndex = index;
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void LoadErkenningen()
        {
            ErkenningServices service = new ErkenningServices();
            Erkenningen = service.FindActive();
            erkenningViewSource.Source = Erkenningen;
        }
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for RegioBeheer.xaml
    /// </summary>
    public partial class RegioBeheer : Window
    {
        private System.Windows.Data.CollectionViewSource regioViewSource;
        private bool IsScroll = false, IsButtonClick = false, datagridChange = false;
        private List<Regio> Regios = new List<Regio>();
        private List<Regio> gevondenRegios = new List<Regio>();
        private Gebruiker currentUser;

        public RegioBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Regio beheer : Ingelogd als " + currentUser.Naam;
        }

        private void GoUpdate()
        {
            if (regioDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (regioViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (regioViewSource.View.CurrentPosition == regioDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                regioDataGrid.SelectedIndex = 0;
            if (regioDataGrid.Items.Count != 0)
                if (regioDataGrid.SelectedItem != null)
                    regioDataGrid.ScrollIntoView(regioDataGrid.SelectedItem);
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            regioViewSource.View.MoveCurrentToFirst();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            regioViewSource.View.MoveCurrentToPrevious();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            regioViewSource.View.MoveCurrentToLast();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuweRegio NR = new NieuweRegio(currentUser);
            if (NR.ShowDialog() == true)
            {
                LoadRegios();
            }
        }

        private void ZoekTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ZoekRegio();
        }

        private void ZoekRegio()
        {
            if (ZoekTextBox.Text != "")
            {
                gevondenRegios.Clear();
                gevondenRegios = (from r in Regios
                                  where r.Regio1.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                  select r).ToList();

                regioViewSource.Source = gevondenRegios;
            }
            else
                regioViewSource.Source = Regios;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            datagridChange = true;
            regioViewSource.View.MoveCurrentToNext();
            GoUpdate();
            IsScroll = false;
            IsButtonClick = false;
            datagridChange = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            regioViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("regioViewSource")));
            LoadRegios();
        }

        private void actiefCheckBox_Click(object sender, RoutedEventArgs e)
        {
            SaveWijzigingen();
        }

        private void regio1TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab || e.Key == Key.Return)
            {
                SaveWijzigingen();
            }
        }

        private void SaveWijzigingen()
        {
            try
            {
                if (!string.IsNullOrEmpty(regio1TextBox.Text))
                {
                    Regio regio = (Regio)regioDataGrid.SelectedItem;
                    regio.Regio1 = regio1TextBox.Text;
                    regio.Actief = actiefCheckBox.IsChecked == true;
                    regio.Gewijzigd = DateTime.Now;
                    regio.Gewijzigd_door = currentUser.Naam;
                    new RegioService().Update(regio);
                    ReloadRegios();
                }
                else
                    ConfocusMessageBox.Show("Het regio veld mag niet leeg zijn!", ConfocusMessageBox.Kleuren.Rood);
            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show("Fout bij het opslaan van de regio!!\n" + ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private void ReloadRegios()
        {
            int index = regioDataGrid.SelectedIndex;
            LoadRegios();
            regioDataGrid.SelectedIndex = index;
        }

        private void LoadRegios()
        {
            Regios = new RegioService().GetAll();
            regioViewSource.Source = Regios;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for InschrijvingDetail.xaml
    /// </summary>
    public partial class InschrijvingDetail : Window
    {
        private Gebruiker currentUser;
        private Inschrijving myInschrijving;

        public InschrijvingDetail(Gebruiker user, Inschrijving inschrijving)
        {
            InitializeComponent();
            this.currentUser = user;
            this.myInschrijving = inschrijving;
            LoadData();
            setInschrijvingToForm();

        }

        private void LoadData()
        {
            BedrijfServices bService = new BedrijfServices();
            List<Bedrijf> bedrijven = bService.FindActive();
            bedrijfComboBox.ItemsSource = bedrijven;
            bedrijfComboBox.DisplayMemberPath = "NaamEnBtwNummer";

            SeminarieServices sService = new SeminarieServices();
            List<Seminarie> seminaries = sService.FindActive();
            SeminarieComboBox.ItemsSource = seminaries;
            SeminarieComboBox.DisplayMemberPath = "Naam";
        }

        private void setInschrijvingToForm()
        {
            if (myInschrijving != null)
            {
                bedrijfComboBox.Text = myInschrijving.Bedrijf.NaamEnBtwNummer;
                SeminarieComboBox.Text = myInschrijving.Sessie.Naam;
                datum_inschrijvingDatePicker.SelectedDate = myInschrijving.Datum_inschrijving;
                viaTextBox.Text = myInschrijving.Via;
                statusComboBox.Text = myInschrijving.Status;
                notitieTextBox.Text = myInschrijving.Notitie;
                gefactureerdCheckBox.IsChecked = myInschrijving.Gefactureerd;
                factuurnummerTextBox.Text = myInschrijving.Factuurnummer;
                notitie_factuurTextBox.Text = myInschrijving.Notitie_factuur;
                actiefCheckBox.IsChecked = myInschrijving.Actief;
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}

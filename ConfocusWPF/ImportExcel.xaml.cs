﻿using ConfocusDBLibrary;
using ITCLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ImportExcel.xaml
    /// </summary>
    public partial class ImportExcel : Window
    {
        private CollectionViewSource excelContactViewSource;
        private List<ExcelContact> contacten = new List<ExcelContact>();
        private bool IsScroll = false, IsButtonClick = false;
        private Gebruiker currentUser;

        public ImportExcel(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Niveau1Services n1Service = new Niveau1Services();
            List<Niveau1> n1Lijst = n1Service.FindActive();
            niveau1ComboBox.ItemsSource = n1Lijst;
            niveau1ComboBox.DisplayMemberPath = "Naam";

            excelContactViewSource = ((CollectionViewSource)(this.FindResource("excelContactViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // excelContactViewSource.Source = [generic data source]
            save.IsEnabled = false;
            excel.IsEnabled = false;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OpenExcelButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog OD = new OpenFileDialog();
            OD.Filter = "Excel (.xlsx)|*.xlsx";
            if (OD.ShowDialog() == true)
            {
                String filename = OD.FileName;
                new Thread(delegate() { LoadContacten(filename); }).Start();
                //goUpdate();
            }
        }

        private void LoadContacten(string filename)
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);

                ImportContact im = new ImportContact(currentUser);
                contacten = im.readExcel(filename);
                List<ExcelContact> cleaned = CleanContacten(contacten);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { save.SetValue(Button.IsEnabledProperty, true); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { excelContactViewSource.SetValue(CollectionViewSource.SourceProperty, cleaned); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { goUpdate(); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ConfocusMessageBox.Show("Excel lijst ingeladen.", ConfocusMessageBox.Kleuren.Blauw); }, null);

            }
            catch (Exception ex)
            {
                ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
            }
        }

        private List<ExcelContact> CleanContacten(List<ExcelContact> contacten)
        {
            try
            {
                foreach (ExcelContact contact in contacten)
                {
                    if (contact.Aanspreking == "De heer" || contact.Aanspreking == "Monsieur")
                        contact.Aanspreking = "M";
                    else if (contact.Aanspreking == "Mevrouw" || contact.Aanspreking == "Madame")
                        contact.Aanspreking = "V";
                    if (contact.Bedrijf == null)
                    {
                        contact.Bedrijf = contact.Achternaam + " " + contact.Voornaam;
                    }

                    if (contact.Postcode != null)
                    {
                        String gemeente = contact.Plaats;
                        PostcodeService pService = new PostcodeService();
                        Postcodes postcode = pService.GetPostcodeByIDAndCity(contact.Postcode, gemeente);
                        if (postcode != null)
                        {
                            contact.Plaats = postcode.Gemeente;
                            contact.Provincie = postcode.Provincie;

                        }
                    }
                    else
                    {
                        contact.Postcode = "";
                        contact.Plaats = "";
                        contact.Provincie = "";
                        contact.Land = "";
                    }
                }
                return contacten;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (contacten.Count > 0)
            {
                Thread t = new Thread(delegate() { SaveContacten(contacten); });
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
            }
        }

        private void SaveContacten(List<ExcelContact> contacten)
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Visible); }, null);
                ImportContact import = new ImportContact(currentUser);
                contacten = import.saveExcelToDB(contacten);
                if (contacten.Count > 0)
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { excel.SetValue(Button.IsEnabledProperty, true); }, null);

                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { excelContactViewSource.SetValue(CollectionViewSource.SourceProperty, contacten); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadingGrid.SetValue(Grid.VisibilityProperty, System.Windows.Visibility.Hidden); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ConfocusMessageBox.Show("Contacten lijst opgeslagen.\nAls er nog Contacten in de lijst staan is zijn deze niet opgeslagen wegens een fout in de gegevens.", ConfocusMessageBox.Kleuren.Blauw); }, null);
            }
            catch (Exception ex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Blauw); }, null);
            }
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "import":
                    img = importImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "excel":
                    img = exportImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + "_hover.gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "import":
                    img = importImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                case "excel":
                    img = exportImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            excelContactViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            excelContactViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            excelContactViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            excelContactViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void ToExcel(int lengte)
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { excel.SetValue(Button.IsEnabledProperty, false); }, null);
                //excel.IsEnabled = false;
                
                string[] headers = {"Niveau1","Niveau2","Niveau3", "Oorspronkelijke titel", "Bedrijf","Website","Adres","Postcode","Plaats","Provincie","Land","Telefoon bedrijf", "Telefoon contact", "Mobiel","Fax","Email bedrijf", "Email contact", "Aanspreking","Achternaam","Voornaam","Taal", "Type"};
                string[][] contacten = new string[lengte][];
                int teller = 0;
                foreach (object item in excelContactDataGrid.Items)
                {
                    if (item is ExcelContact)
                    {
                        ExcelContact contact = item as ExcelContact;

                        string[] lid = {  
                                          contact.Niveau1, 
                                          contact.Niveau2, 
                                          contact.Niveau3,
                                          contact.Oorspronkelijke_titel,
                                          contact.Bedrijf, 
                                          contact.Webpagina, 
                                          contact.Adres, 
                                          contact.Postcode, 
                                          contact.Plaats, 
                                          contact.Provincie, 
                                          contact.Land, 
                                          contact.TelefoonBedrijf, 
                                          contact.TelefoonContact,
                                          contact.Mobiel, 
                                          contact.Fax, 
                                          contact.EmailBedrijf,
                                          contact.EmailContact,
                                          contact.Aanspreking, 
                                          contact.Achternaam, 
                                          contact.Voornaam, 
                                          contact.Taal, 
                                          contact.Type
                                       };
                        contacten[teller] = lid;
                        teller++;
                    }
                }

                ExcelDocument Xl = new ExcelDocument();
                Xl.SetColumnHeaders(headers, 1, true, true, true);
                Xl.InsertList(contacten, true, 2, new int[0], 10);
                object misValue = System.Reflection.Missing.Value;
                //Xl.Save();
                //SendMail(Xl.fileName);
                //Xl.Close();

            }
            catch (Exception ex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate
                {
                    ConfocusMessageBox.Show(ex.Message, ConfocusMessageBox.Kleuren.Rood);
                }, null);
            }
        }

        private void goUpdate()
        {
            if (excelContactDataGrid.Items.Count <= 1)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (excelContactViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (excelContactViewSource.View.CurrentPosition == excelContactDataGrid.Items.Count - 1)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                excelContactDataGrid.SelectedIndex = 0;
            if (excelContactDataGrid.Items.Count != 0)
                excelContactDataGrid.ScrollIntoView(excelContactDataGrid.SelectedItem);
        }

        private void gridTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                TextBox text = (TextBox)sender;
                ExcelContact contact = (ExcelContact)excelContactDataGrid.SelectedItem;
                int pos = contacten.IndexOf(contact);
                switch (text.Name)
                {
                    case "gridVoornaamTextBox":
                        contact.Voornaam = text.Text;
                        break;
                    case "gridAchternaamTextBox":
                        contact.Achternaam = text.Text;
                        break;
                    case "gridTaalTextBox":
                        contact.Taal = text.Text;
                        break;
                    case "gridAansprekingTextBox":
                        contact.Aanspreking = text.Text;
                        break;
                    case "gridOorspronkelijkeTitelTextBox":
                        contact.Oorspronkelijke_titel = text.Text;
                        break;
                    case "gridBedrijfTextBox":
                        contact.Bedrijf = text.Text;
                        break;
                    case "gridAdresTextBox":
                        contact.Adres = text.Text;
                        break;
                    case "gridPostcodeTextBox":
                        contact.Postcode = text.Text;
                        break;
                    case "gridPlaatsTextBox":
                        contact.Plaats = text.Text;
                        break;
                    case "gridProvincieTextBox":
                        contact.Provincie = text.Text;
                        break;
                    case "gridLandTextBox":
                        contact.Land = text.Text;
                        break;
                    case "gridEmailTextBox":
                        contact.EmailBedrijf = text.Text;
                        break;
                    case "gridContactEmailTextBox":
                        contact.EmailContact = text.Text;
                        break;
                    case "gridTelefoonTextBox":
                        contact.TelefoonBedrijf = text.Text;
                        break;
                    case "gridTelefoonContactTextBox":
                        contact.TelefoonContact = text.Text;
                        break;
                    case "gridFaxTextBox":
                        contact.Fax = text.Text;
                        break;
                    case "gridWebsiteTextBox":
                        contact.Webpagina = text.Text;
                        break;
                    case "gridMobielTextBox":
                        contact.Mobiel = text.Text;
                        break;
                    case "gridNiveau1TextBox":
                        contact.Niveau1 = text.Text;
                        break;
                    case "gridNiveau2TextBox":
                        contact.Niveau2 = text.Text;
                        break;
                    case "gridNiveau3TextBox":
                        contact.Niveau3 = text.Text;
                        break;
                }

                contacten.RemoveAt(pos);
                contacten.Insert(pos, contact);
                excelContactViewSource.Source = contacten;
                excelContactDataGrid.SelectedIndex = pos;
                goUpdate();
                
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox text = (TextBox)sender;
            ExcelContact contact = (ExcelContact)excelContactDataGrid.SelectedItem;
            int pos = contacten.IndexOf(contact);
            switch (text.Name)
            {
                case "voornaamTextBox":
                    contact.Voornaam = text.Text;
                    break;
                case "achternaamTextBox":
                    contact.Achternaam = text.Text;
                    break;
                case "taalTextBox":
                    contact.Taal = text.Text;
                    break;
                case "aansprekingTextBox":
                    contact.Aanspreking = text.Text;
                    break;
                case "OorspronkelijkeTitelTextBox":
                    contact.Oorspronkelijke_titel = text.Text;
                    break;
                case "bedrijfTextBox":
                    contact.Bedrijf = text.Text;
                    break;
                case "adresTextBox":
                    contact.Adres = text.Text;
                    break;
                case "postcodeTextBox":
                    contact.Postcode = text.Text;
                    break;
                case "plaatsTextBox":
                    contact.Plaats = text.Text;
                    break;
                case "provincieTextBox":
                    contact.Provincie = text.Text;
                    break;
                case "landTextBox":
                    contact.Land = text.Text;
                    break;
                case "emailTextBox":
                    contact.EmailBedrijf = text.Text;
                    break;
                case "ContactEmailTextBox":
                    contact.EmailContact = text.Text;
                    break;
                case "telefoonTextBox":
                    contact.TelefoonBedrijf = text.Text;
                    break;
                case "TelefoonContactTextBox":
                    contact.TelefoonContact = text.Text;
                    break;
                case "faxTextBox":
                    contact.Fax = text.Text;
                    break;
                case "webpaginaTextBox":
                    contact.Webpagina = text.Text;
                    break;
                case "mobielTextBox":
                    contact.Mobiel = text.Text;
                    break;
                case "niveau1TextBox":
                    contact.Niveau1 = text.Text;
                    break;
                case "niveau2TextBox":
                    contact.Niveau2 = text.Text;
                    break;
                case "niveau3TextBox":
                    contact.Niveau3 = text.Text;
                    break;
            }

            contacten.RemoveAt(pos);
            contacten.Insert(pos, contact);
            excelContactViewSource.Source = contacten;
            excelContactDataGrid.SelectedIndex = pos;
            goUpdate();
                
        }

        private void niveau1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            ExcelContact contact = (ExcelContact)excelContactDataGrid.SelectedItem;
            if (box.SelectedIndex > -1)
            {
                Niveau1 n1 = (Niveau1)box.SelectedItem;
                Niveau2Services n2Service = new Niveau2Services();
                List<Niveau2> n2Lijst = n2Service.GetSelected(n1.ID);
                niveau2ComboBox.ItemsSource = n2Lijst;
                niveau2ComboBox.DisplayMemberPath = "Naam";
                //if (contact != null)
                //    niveau2ComboBox.Text = contact.Niveau2;
            }
        }

        private void niveau2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             ComboBox box = (ComboBox)sender;
            ExcelContact contact = (ExcelContact)excelContactDataGrid.SelectedItem;
            if (box.SelectedIndex > -1)
            {
                Niveau2 n2 = (Niveau2)box.SelectedItem;
                Niveau3Services n3Service = new Niveau3Services();
                List<Niveau3> n3Lijst = n3Service.GetSelected(n2.ID);
                niveau3ComboBox.ItemsSource = n3Lijst;
                niveau3ComboBox.DisplayMemberPath = "Naam";
                //if (contact != null)
                //    niveau3ComboBox.Text = contact.Niveau3;
            }
        }

        private void excelContactDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (excelContactDataGrid.Items.Count > 0)
            {
                ExcelContact contact = (ExcelContact)excelContactDataGrid.SelectedItem;
                if (contact.Niveau1 != null)
                    niveau1ComboBox.Text = contact.Niveau1;
                else
                    niveau1ComboBox.SelectedIndex = -1;
                if (contact.Niveau2 != null)
                    niveau2ComboBox.Text = contact.Niveau2;
                else
                    niveau2ComboBox.SelectedIndex = -1;
                if (contact.Niveau1 != null)
                    niveau3ComboBox.Text = contact.Niveau3;
                else
                    niveau3ComboBox.SelectedIndex = -1;
            }
        }

        private void niveau1ComboBox_DropDownClosed(object sender, EventArgs e)
        {

        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            ExcelContact contact = (ExcelContact)excelContactDataGrid.SelectedItem;
            switch (box.Name)
            {
                case "niveau1ComboBox":
                    contact.Niveau1 = box.Text;
                    contact.Niveau2 = "";
                    contact.Niveau3 = "";
                    break;
                case "niveau2ComboBox":
                    contact.Niveau2 = box.Text;
                    contact.Niveau3 = "";
                    break;
                case "niveau3ComboBox":
                    contact.Niveau3 = box.Text;
                    break;

            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            int lengte = excelContactDataGrid.Items.Count;
            Task.Factory.StartNew(() => { ToExcel(lengte); });
        }
    }
}

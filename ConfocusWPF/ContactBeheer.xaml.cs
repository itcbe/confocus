﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for ContactBeheer.xaml
    /// </summary>
    public partial class ContactBeheer : Window
    {
        private CollectionViewSource contactViewSource;
        private List<Contact> Contacten = new List<Contact>();
        private List<Contact> gevondenContacten = new List<Contact>();
        private List<Contact> gewijzigdeContacten = new List<Contact>();
        private List<Taal> Talen = new List<Taal>();
        private Boolean IsScroll = false, IsButtonClick = false;
        private Gebruiker currentUser;

        public ContactBeheer(Gebruiker user)
        {
            InitializeComponent();
            this.currentUser = user;
            this.Title = "Contact beheer : Ingelogd als " + currentUser.Naam;
        }

        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToFirst();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToPrevious();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToNext();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            IsScroll = true;
            IsButtonClick = true;
            contactViewSource.View.MoveCurrentToLast();
            goUpdate();
            IsScroll = false;
            IsButtonClick = false;
        }

        private void ZoekButton_Click(object sender, RoutedEventArgs e)
        {
            if (ZoekTextBox.Text != "")
            {
                gevondenContacten.Clear();

                gevondenContacten = (from c in Contacten
                                     where c.Achternaam.ToLower().Contains(ZoekTextBox.Text.ToLower())
                                     select c).ToList();
                contactViewSource.Source = gevondenContacten;
                goUpdate();
            }
            else
            {
                contactViewSource.Source = Contacten;
                goUpdate();
            }
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NieuwContact nc = new NieuwContact(currentUser, "Functie");
            if (nc.ShowDialog() == true)
            {
                ReloadContacten();
            }
        }

        private void ReloadContacten()
        {
            ContactServices contactServie = new ContactServices();
            Contacten = contactServie.FindAll();
            contactViewSource.Source = Contacten;
            goUpdate();

        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (gewijzigdeContacten.Count > 0)
            {
                ContactServices service = new ContactServices();
                foreach (Contact contact in gewijzigdeContacten)
                {
                    service.SaveContactChanges(contact);
                }
                gewijzigdeContacten.Clear();
                LoadData();
            }
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "search":
                    img = searchImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + "_hover.gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + "_hover.gif"));

        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button knop = (Button)sender;
            String naam = knop.Name;
            Image img;
            switch (naam)
            {
                case "first":
                    img = firstImage;
                    break;
                case "back":
                    img = backImage;
                    break;
                case "next":
                    img = nextImage;
                    break;
                case "last":
                    img = lastImage;
                    break;
                case "search":
                    img = searchImage;
                    break;
                case "new":
                    img = newImage;
                    break;
                case "save":
                    img = saveImage;
                    break;
                default:
                    img = firstImage;
                    break;
            }
            //img.Source = new BitmapImage(new Uri(@"C:\Users\Stefan.Kelchtermans\Documents\Visual Studio 2012\Projects\ConfocusSolutions\confocus\ConfocusWPF\Images\" + naam + ".gif"));
            img.Source = new BitmapImage(new Uri("pack://application:,,,/ConfocusWPF;component/Images/" + naam + ".gif"));

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            geboortedatumDatePicker.Text = DateTime.Today.ToShortDateString();
            contactViewSource = ((CollectionViewSource)(this.FindResource("contactViewSource")));
            LoadData();           
        }

        private void LoadData()
        {
            back.IsEnabled = false;
            first.IsEnabled = false;
            next.IsEnabled = false;
            last.IsEnabled = false;

            TaalServices taalService = new TaalServices();
            Talen = taalService.FindAll();
            naamComboBox.ItemsSource = Talen;
            naamComboBox.DisplayMemberPath = "Naam";

            ContactServices service = new ContactServices();
            Contacten = service.FindAll();
            if (Contacten.Count > 0)
            {
                contactViewSource.Source = Contacten;
                goUpdate();
            }

            BindBedrijf();
        }

        private void goUpdate()
        {
            if (contactDataGrid.Items.Count <= 2)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else if (contactViewSource.View.CurrentPosition == 0)
            {
                back.IsEnabled = false;
                first.IsEnabled = false;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            else if (contactViewSource.View.CurrentPosition == contactDataGrid.Items.Count - 2)
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = false;
                last.IsEnabled = false;
            }
            else
            {
                back.IsEnabled = true;
                first.IsEnabled = true;
                next.IsEnabled = true;
                last.IsEnabled = true;
            }
            if (!IsScroll)
                contactDataGrid.SelectedIndex = 0;
            if (contactDataGrid.Items.Count != 0)
                contactDataGrid.ScrollIntoView(contactDataGrid.SelectedItem);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void contactDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (contactDataGrid.Items.Count > 1)
            {
                if (contactDataGrid.SelectedItem is Contact)
                {
                    Contact myContact = (Contact)contactDataGrid.SelectedItem;
                    naamComboBox.Text = myContact.Taal.Naam;
                    if (!IsButtonClick)
                    {
                        IsScroll = true;
                        goUpdate();
                        IsScroll = false;
                    }
                }
            }
        }

        private void functiesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BindBedrijf();
        }

        private void functiesDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void BindBedrijf()
        {
            Bedrijf myBedrijf = new Bedrijf();
            Functies functie = new Functies();
            if (functiesDataGrid.Items.Count > 0)
            {
                functie = (Functies)functiesDataGrid.SelectedItem;
                BedrijfServices bedrijfService = new BedrijfServices();

                myBedrijf = bedrijfService.GetBedrijfByID(functie.Bedrijf_ID);
                if (myBedrijf != null)
                {
                    naamTextBox.Text = myBedrijf.Naam;
                    adresTextBox.Text = myBedrijf.Adres;
                    plaatsTextBox.Text = myBedrijf.Plaats;
                    landTextBox.Text = myBedrijf.Land;
                    telefoonTextBox.Text = myBedrijf.Telefoon;
                    faxTextBox.Text = myBedrijf.Fax;
                    emailTextBox.Text = myBedrijf.Email;
                    websiteTextBox.Text = myBedrijf.Website;
                }

            }
        }

        private void naamComboBox_DropDownClosed(object sender, EventArgs e)
        {
            registerChanges();
        }

        private void registerChanges()
        {
            if (contactDataGrid.Items.Count > 1)
            {
                Contact contact = (Contact)contactDataGrid.SelectedItem;
                Taal taal = (Taal)naamComboBox.SelectedItem;
                if (taal != null)
                    contact.Taal_ID = taal.ID;
                contact.Geboortedatum = geboortedatumDatePicker.SelectedDate;
                if (gewijzigdeContacten.Contains(contact))
                {
                    int pos = 0;
                    foreach (Contact c in gewijzigdeContacten)
                    {
                        if (c.ID == contact.ID)
                            break;
                        else
                            pos++;
                    }
                    gewijzigdeContacten.RemoveAt(pos);
                    gewijzigdeContacten.Add(contact);
                }
                else
                    gewijzigdeContacten.Add(contact);
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            registerChanges();
        }

        private void geboortedatumDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            registerChanges();
        }
    }
}

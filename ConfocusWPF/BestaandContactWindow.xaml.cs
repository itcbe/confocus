﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfocusWPF
{
    /// <summary>
    /// Interaction logic for BestaandContactWindow.xaml
    /// </summary>
    public partial class BestaandContactWindow : Window
    {
        private List<Functies> contacten = new List<Functies>();
        public Functies myContact;

        public BestaandContactWindow(List<Functies> cont)
        {
            InitializeComponent();
            this.contacten = cont;
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            contactenListBox.ItemsSource = contacten;
            contactenListBox.DisplayMemberPath = "NaamBedrijfFunctie";
        }

        private void AnnuleerButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void gebruikbestaandeButton_Click(object sender, RoutedEventArgs e)
        {
            if (contactenListBox.SelectedIndex != -1)
            {
                myContact = (Functies)contactenListBox.SelectedItem;
                DialogResult = true;
            }
            else
                ConfocusMessageBox.Show("Geen contact geselecteerd!", ConfocusMessageBox.Kleuren.Rood);
        }
    }
}

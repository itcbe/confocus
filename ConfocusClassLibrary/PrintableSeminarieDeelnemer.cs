﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class PrintableSeminarieDeelnemer
    {
        public string Sessie { get; set; }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public string Bedrijf { get; set; }
        public string Functie { get; set; }
        public string Type { get; set; }
        public string Beginuur { get; set; }
        public string Einduur { get; set; }
        public bool Cursief { get; set; }
        public bool Toevoegen { get; set; }

        public PrintableSeminarieDeelnemer()
        {

        }

        public PrintableSeminarieDeelnemer(string sessie, string achternaam, string voornaam, string bedrijf, string functie, string beginuur, string einduur, string type,  bool cursief)
        {
            this.Sessie = sessie;
            this.Voornaam = voornaam;
            this.Achternaam = achternaam;
            this.Bedrijf = bedrijf;
            this.Functie = functie;
            this.Type = type;
            this.Beginuur = beginuur;
            this.Einduur = einduur;
            this.Cursief = cursief;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Controls;
using System.Xml;
using ConfocusDBLibrary;
using System.Text.RegularExpressions;
using System.Diagnostics;
using ITCLibrary;

namespace ConfocusClassLibrary
{
    public static class Helper
    {

        public enum ExtraAttestKind
        {
            Deelnemer,
            Spreker
        };

        public static void SetTextBoxToNormal(System.Windows.Controls.TextBox myTextBox, ControlTemplate template)
        {
            myTextBox.Template = template;
        }

        public static void SetTextBoxInError(System.Windows.Controls.TextBox myTextBox, ControlTemplate template)
        {
            myTextBox.Template = template;
        }

        public static void SetTextBoxReadOnly(System.Windows.Controls.TextBox myTextBox, ControlTemplate template)
        {
            myTextBox.Template = template;
        }

        public static void SetComboBoxInError(System.Windows.Controls.ComboBox myComboBox)
        {
            myComboBox.BorderThickness = new Thickness(2, 2, 1, 1);
            myComboBox.BorderBrush = new SolidColorBrush(Colors.Red);
        }

        public static void SetComboBoxToNormal(System.Windows.Controls.ComboBox myComboBox)
        {
            myComboBox.BorderThickness = new Thickness(0);
            myComboBox.BorderBrush = new SolidColorBrush(Colors.Black);

        }

        public static void SetPassTextBoxInError(PasswordBox paswoordTextBox, ControlTemplate template)
        {
            paswoordTextBox.Template = template;
        }

        public static void SetPassTextBoxToNormal(PasswordBox paswoordTextBox, ControlTemplate template)
        {
            paswoordTextBox.Template = template;
        }

        public static void SaveContactConfig(string Filename, ContactConfig myConfig)
        {
            try
            {
                XmlDocument instellingen = new XmlDocument();
                instellingen.Load(Filename);
                // Contact
                XmlNode IDNode = instellingen.SelectSingleNode("//ContactHeaders/ID");
                IDNode.InnerXml = myConfig.ID == true ? "1" : "0";
                XmlNode VoornaamNode = instellingen.SelectSingleNode("//ContactHeaders/Voornaam");
                VoornaamNode.InnerXml = myConfig.Voornaam == true ? "1" : "0";
                XmlNode AchternaamNode = instellingen.SelectSingleNode("//ContactHeaders/Achternaam");
                AchternaamNode.InnerXml = myConfig.Achternaam == true ? "1" : "0";
                XmlNode GeboortedatumNode = instellingen.SelectSingleNode("//ContactHeaders/Geboortedatum");
                GeboortedatumNode.InnerXml = myConfig.Geboortedatum == true ? "1" : "0";
                XmlNode AansprekingNode = instellingen.SelectSingleNode("//ContactHeaders/Aanspreking");
                AansprekingNode.InnerXml = myConfig.Aanspreking == true ? "1" : "0";
                XmlNode TaalNode = instellingen.SelectSingleNode("//ContactHeaders/Taal");
                TaalNode.InnerXml = myConfig.Taal == true ? "1" : "0";
                XmlNode ContactnotitiesNode = instellingen.SelectSingleNode("//ContactHeaders/Contactnotities");
                ContactnotitiesNode.InnerXml = myConfig.ContactNotities == true ? "1" : "0";
                XmlNode ContactActiefNode = instellingen.SelectSingleNode("//ContactHeaders/Contactactief");
                ContactActiefNode.InnerXml = myConfig.ContactActief == true ? "1" : "0";


                // Bedrijf
                XmlNode BedrijfNode = instellingen.SelectSingleNode("//ContactHeaders/Bedrijf");
                BedrijfNode.InnerXml = myConfig.Bedrijf == true ? "1" : "0";
                XmlNode BtwnummerNode = instellingen.SelectSingleNode("//ContactHeaders/Btwnummer");
                BtwnummerNode.InnerXml = myConfig.BTWnummer == true ? "1" : "0";
                XmlNode TelefoonNode = instellingen.SelectSingleNode("//ContactHeaders/Telefoon");
                TelefoonNode.InnerXml = myConfig.Telefoon == true ? "1" : "0";
                XmlNode FaxNode = instellingen.SelectSingleNode("//ContactHeaders/Fax");
                FaxNode.InnerXml = myConfig.Fax == true ? "1" : "0";
                XmlNode EmailNode = instellingen.SelectSingleNode("//ContactHeaders/Email");
                EmailNode.InnerXml = myConfig.Email == true ? "1" : "0";
                XmlNode WebsiteNode = instellingen.SelectSingleNode("//ContactHeaders/Website");
                WebsiteNode.InnerXml = myConfig.Website == true ? "1" : "0";
                XmlNode AdresNode = instellingen.SelectSingleNode("//ContactHeaders/Adres");
                AdresNode.InnerXml = myConfig.Adres == true ? "1" : "0";
                XmlNode PostcodeNode = instellingen.SelectSingleNode("//ContactHeaders/Postcode");
                PostcodeNode.InnerXml = myConfig.Postcode == true ? "1" : "0";
                XmlNode GemeenteNode = instellingen.SelectSingleNode("//ContactHeaders/Gemeente");
                GemeenteNode.InnerXml = myConfig.Gemeente == true ? "1" : "0";
                XmlNode NotitiesNode = instellingen.SelectSingleNode("//ContactHeaders/Bedrijfsnotities");
                NotitiesNode.InnerXml = myConfig.BedrijfsNotities == true ? "1" : "0";
                XmlNode BedrijfactiefNode = instellingen.SelectSingleNode("//ContactHeaders/Bedrijfactief");
                BedrijfactiefNode.InnerXml = myConfig.BedrijfActief == true ? "1" : "0";
                XmlNode GewijzigdNode = instellingen.SelectSingleNode("//ContactHeaders/Gewijzigd");
                GewijzigdNode.InnerXml = myConfig.Gewijzigd == true ? "1" : "0";
                XmlNode GewijzigdDoorNode = instellingen.SelectSingleNode("//ContactHeaders/GewijzigdDoor");
                GewijzigdDoorNode.InnerXml = myConfig.GewijzigdDoor == true ? "1" : "0";
                XmlNode AangemaaktNode = instellingen.SelectSingleNode("//ContactHeaders/Aangemaakt");
                AangemaaktNode.InnerXml = myConfig.Aangemaakt == true ? "1" : "0";
                XmlNode AangemaaktDoorNode = instellingen.SelectSingleNode("//ContactHeaders/AangemaaktDoor");
                AangemaaktDoorNode.InnerXml = myConfig.AangemaaktDoor == true ? "1" : "0";
                // Functie 
                XmlNode Niveau1Node = instellingen.SelectSingleNode("//ContactHeaders/Niveau1");
                Niveau1Node.InnerXml = myConfig.Niveau1 == true ? "1" : "0";
                XmlNode Niveau2Node = instellingen.SelectSingleNode("//ContactHeaders/Niveau2");
                Niveau2Node.InnerXml = myConfig.Niveau2 == true ? "1" : "0";
                XmlNode Niveau3Node = instellingen.SelectSingleNode("//ContactHeaders/Niveau3");
                Niveau3Node.InnerXml = myConfig.Niveau3 == true ? "1" : "0";
                XmlNode AttesttypeNode = instellingen.SelectSingleNode("//ContactHeaders/Attesttype");
                AttesttypeNode.InnerXml = myConfig.Attesttype == true ? "1" : "0";
                XmlNode ErkenningsnummerNode = instellingen.SelectSingleNode("//ContactHeaders/Erkenningsnummer");
                ErkenningsnummerNode.InnerXml = myConfig.Erkenningsnummer == true ? "1" : "0";
                XmlNode StartdatumNode = instellingen.SelectSingleNode("//ContactHeaders/Startdatum");
                StartdatumNode.InnerXml = myConfig.Startdatum == true ? "1" : "0";
                XmlNode EinddatumNode = instellingen.SelectSingleNode("//ContactHeaders/Einddatum");
                EinddatumNode.InnerXml = myConfig.Einddatum == true ? "1" : "0";
                XmlNode TypeNode = instellingen.SelectSingleNode("//ContactHeaders/Type");
                TypeNode.InnerXml = myConfig.Type == true ? "1" : "0";
                XmlNode ContactEmailNode = instellingen.SelectSingleNode("//ContactHeaders/ContactEmail");
                ContactEmailNode.InnerXml = myConfig.ContactEmail == true ? "1" : "0";
                XmlNode ContactTelefoonNode = instellingen.SelectSingleNode("//ContactHeaders/ContactTelefoon");
                ContactTelefoonNode.InnerXml = myConfig.ContactTelefoon == true ? "1" : "0";
                XmlNode NieuwsbriefMailNode = instellingen.SelectSingleNode("//ContactHeaders/NieuwsbriefMail");
                NieuwsbriefMailNode.InnerXml = myConfig.NieuwsbriefMail == true ? "1" : "0";
                XmlNode NieuwsbriefFaxNode = instellingen.SelectSingleNode("//ContactHeaders/NieuwsbriefFax");
                NieuwsbriefFaxNode.InnerXml = myConfig.NieuwsbriefFax == true ? "1" : "0";
                XmlNode FunctieActiefNode = instellingen.SelectSingleNode("//ContactHeaders/Functieactief");
                FunctieActiefNode.InnerXml = myConfig.FunctieActief == true ? "1" : "0";

                instellingen.Save(Filename);

            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het lezen van het configuratie document!");
            }
        }

        public static ContactConfig GetContactConfigFromXml(string Filename)
        {
            try
            {
                ContactConfig contactconfig = new ContactConfig();
                XmlDocument instellingen = new XmlDocument();
                instellingen.Load(Filename);
                // Contact
                XmlNode IDNode = instellingen.SelectSingleNode("//ContactHeaders/ID");
                contactconfig.ID = IDNode.InnerXml == "1" ? true : false;
                XmlNode VoornaamNode = instellingen.SelectSingleNode("//ContactHeaders/Voornaam");
                contactconfig.Voornaam = VoornaamNode.InnerXml == "1" ? true : false;
                XmlNode AchternaamNode = instellingen.SelectSingleNode("//ContactHeaders/Achternaam");
                contactconfig.Achternaam = AchternaamNode.InnerXml == "1" ? true : false;
                XmlNode GeboortedatumNode = instellingen.SelectSingleNode("//ContactHeaders/Geboortedatum");
                contactconfig.Geboortedatum = GeboortedatumNode.InnerXml == "1" ? true : false;
                XmlNode AansprekingNode = instellingen.SelectSingleNode("//ContactHeaders/Aanspreking");
                contactconfig.Aanspreking = AansprekingNode.InnerXml == "1" ? true : false;
                XmlNode TaalNode = instellingen.SelectSingleNode("//ContactHeaders/Taal");
                contactconfig.Taal = TaalNode.InnerXml == "1" ? true : false;
                XmlNode ContactNotitieNode = instellingen.SelectSingleNode("//ContactHeaders/Contactnotities");
                contactconfig.ContactNotities = ContactNotitieNode.InnerXml == "1" ? true : false;
                XmlNode ContactActiefNode = instellingen.SelectSingleNode("//ContactHeaders/Contactactief");
                contactconfig.ContactActief = ContactActiefNode.InnerXml == "1" ? true : false;

                // Bedrijf
                XmlNode BedrijfNode = instellingen.SelectSingleNode("//ContactHeaders/Bedrijf");
                contactconfig.Bedrijf = BedrijfNode.InnerXml == "1" ? true : false;
                XmlNode BtwnummerNode = instellingen.SelectSingleNode("//ContactHeaders/Btwnummer");
                contactconfig.BTWnummer = BtwnummerNode.InnerXml == "1" ? true : false;
                XmlNode TelefoonNode = instellingen.SelectSingleNode("//ContactHeaders/Telefoon");
                contactconfig.Telefoon = TelefoonNode.InnerXml == "1" ? true : false;
                XmlNode FaxNode = instellingen.SelectSingleNode("//ContactHeaders/Fax");
                contactconfig.Fax = FaxNode.InnerXml == "1" ? true : false;
                XmlNode EmailNode = instellingen.SelectSingleNode("//ContactHeaders/Email");
                contactconfig.Email = EmailNode.InnerXml == "1" ? true : false;
                XmlNode AdresNode = instellingen.SelectSingleNode("//ContactHeaders/Adres");
                contactconfig.Adres = AdresNode.InnerXml == "1" ? true : false;
                XmlNode PostcodeNode = instellingen.SelectSingleNode("//ContactHeaders/Postcode");
                contactconfig.Postcode = PostcodeNode.InnerXml == "1" ? true : false;
                XmlNode GemeenteNode = instellingen.SelectSingleNode("//ContactHeaders/Gemeente");
                contactconfig.Gemeente = GemeenteNode.InnerXml == "1" ? true : false;
                XmlNode WebsiteNode = instellingen.SelectSingleNode("//ContactHeaders/Website");
                contactconfig.Website = WebsiteNode.InnerXml == "1" ? true : false;
                XmlNode NotitiesNode = instellingen.SelectSingleNode("//ContactHeaders/Bedrijfsnotities");
                contactconfig.BedrijfsNotities = NotitiesNode.InnerXml == "1" ? true : false;
                XmlNode BedijfActiefNode = instellingen.SelectSingleNode("//ContactHeaders/Bedrijfactief");
                contactconfig.BedrijfActief = BedijfActiefNode.InnerXml == "1" ? true : false;
                XmlNode GewijzigdNode = instellingen.SelectSingleNode("//ContactHeaders/Gewijzigd");
                contactconfig.Gewijzigd = GewijzigdNode.InnerXml == "1" ? true : false;
                XmlNode GewijzigdDoorNode = instellingen.SelectSingleNode("//ContactHeaders/GewijzigdDoor");
                contactconfig.GewijzigdDoor = GewijzigdDoorNode.InnerXml == "1" ? true : false;
                XmlNode AangemaaktNode = instellingen.SelectSingleNode("//ContactHeaders/Aangemaakt");
                contactconfig.Aangemaakt = AangemaaktNode.InnerXml == "1" ? true : false;
                XmlNode AangemaaktDoorNode = instellingen.SelectSingleNode("//ContactHeaders/AangemaaktDoor");
                contactconfig.AangemaaktDoor = AangemaaktDoorNode.InnerXml == "1" ? true : false;
                // Functie
                XmlNode Niveau1Node = instellingen.SelectSingleNode("//ContactHeaders/Niveau1");
                contactconfig.Niveau1 = Niveau1Node.InnerXml == "1" ? true : false;
                XmlNode Niveau2Node = instellingen.SelectSingleNode("//ContactHeaders/Niveau2");
                contactconfig.Niveau2 = Niveau2Node.InnerXml == "1" ? true : false;
                XmlNode Niveau3Node = instellingen.SelectSingleNode("//ContactHeaders/Niveau3");
                contactconfig.Niveau3 = Niveau3Node.InnerXml == "1" ? true : false;
                XmlNode AttesttypeNode = instellingen.SelectSingleNode("//ContactHeaders/Attesttype");
                contactconfig.Attesttype = AttesttypeNode.InnerXml == "1" ? true : false;
                XmlNode ErkenningsnummerNode = instellingen.SelectSingleNode("//ContactHeaders/Erkenningsnummer");
                contactconfig.Erkenningsnummer = ErkenningsnummerNode.InnerXml == "1" ? true : false;
                XmlNode StartdatumNode = instellingen.SelectSingleNode("//ContactHeaders/Startdatum");
                contactconfig.Startdatum = StartdatumNode.InnerXml == "1" ? true : false;
                XmlNode EinddatumNode = instellingen.SelectSingleNode("//ContactHeaders/Einddatum");
                contactconfig.Einddatum = EinddatumNode.InnerXml == "1" ? true : false;
                XmlNode NieuwsbriefMailNode = instellingen.SelectSingleNode("//ContactHeaders/NieuwsbriefMail");
                contactconfig.NieuwsbriefMail = NieuwsbriefMailNode.InnerXml == "1" ? true : false;
                XmlNode NieuwsbriefFaxNode = instellingen.SelectSingleNode("//ContactHeaders/NieuwsbriefFax");
                contactconfig.NieuwsbriefFax = NieuwsbriefFaxNode.InnerXml == "1" ? true : false;
                XmlNode TypeNode = instellingen.SelectSingleNode("//ContactHeaders/Type");
                contactconfig.Type = TypeNode.InnerXml == "1" ? true : false;
                XmlNode ContactEmailNode = instellingen.SelectSingleNode("//ContactHeaders/ContactEmail");
                contactconfig.ContactEmail = ContactEmailNode.InnerXml == "1" ? true : false;
                XmlNode ContactTelefoonNode = instellingen.SelectSingleNode("//ContactHeaders/ContactTelefoon");
                contactconfig.ContactTelefoon = ContactTelefoonNode.InnerXml == "1" ? true : false;
                XmlNode FunctieActiefNode = instellingen.SelectSingleNode("//ContactHeaders/Functieactief");
                contactconfig.FunctieActief = FunctieActiefNode.InnerXml == "1" ? true : false;



                return contactconfig;

            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het lezen van het configuratie document!");
            }
        }

        public static String CreateConfigXml(String user)
        {
            String FileName = "C:\\Confocus Data\\" + user + ".xml";
            XmlDocument doc = new XmlDocument();

            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //(2) string.Empty makes cleaner code
            XmlElement ContactHeadersElement = doc.CreateElement(string.Empty, "ContactHeaders", string.Empty);
            doc.AppendChild(ContactHeadersElement);

            XmlElement IdElement = doc.CreateElement(string.Empty, "ID", string.Empty);
            XmlText Idwaarde = doc.CreateTextNode("1");
            IdElement.AppendChild(Idwaarde);
            ContactHeadersElement.AppendChild(IdElement);

            XmlElement VoornaamElement = doc.CreateElement(string.Empty, "Voornaam", string.Empty);
            XmlText VNwaarde = doc.CreateTextNode("1");
            VoornaamElement.AppendChild(VNwaarde);
            ContactHeadersElement.AppendChild(VoornaamElement);

            XmlElement AchternaamElement = doc.CreateElement(string.Empty, "Achternaam", string.Empty);
            XmlText ANwaarde = doc.CreateTextNode("1");
            AchternaamElement.AppendChild(ANwaarde);
            ContactHeadersElement.AppendChild(AchternaamElement);

            XmlElement GeboortedatumElement = doc.CreateElement(string.Empty, "Geboortedatum", string.Empty);
            XmlText GDwaarde = doc.CreateTextNode("1");
            GeboortedatumElement.AppendChild(GDwaarde);
            ContactHeadersElement.AppendChild(GeboortedatumElement);

            XmlElement GeslachtElement = doc.CreateElement(string.Empty, "Geslacht", string.Empty);
            XmlText GSwaarde = doc.CreateTextNode("1");
            GeslachtElement.AppendChild(GSwaarde);
            ContactHeadersElement.AppendChild(GeslachtElement);

            XmlElement AansprekingElement = doc.CreateElement(string.Empty, "Aanspreking", string.Empty);
            XmlText ASwaarde = doc.CreateTextNode("1");
            AansprekingElement.AppendChild(ASwaarde);
            ContactHeadersElement.AppendChild(AansprekingElement);

            XmlElement TypeElement = doc.CreateElement(string.Empty, "Type", string.Empty);
            XmlText TYwaarde = doc.CreateTextNode("1");
            TypeElement.AppendChild(TYwaarde);
            ContactHeadersElement.AppendChild(TypeElement);

            XmlElement ContactEmailElement = doc.CreateElement(string.Empty, "ContactEmail", string.Empty);
            XmlText CEwaarde = doc.CreateTextNode("1");
            ContactEmailElement.AppendChild(CEwaarde);
            ContactHeadersElement.AppendChild(ContactEmailElement);

            XmlElement ContactTelefoonElement = doc.CreateElement(string.Empty, "ContactTelefoon", string.Empty);
            XmlText CTwaarde = doc.CreateTextNode("1");
            ContactTelefoonElement.AppendChild(CTwaarde);
            ContactHeadersElement.AppendChild(ContactTelefoonElement);

            XmlElement TaalElement = doc.CreateElement(string.Empty, "Taal", string.Empty);
            XmlText Taalwaarde = doc.CreateTextNode("1");
            TaalElement.AppendChild(Taalwaarde);
            ContactHeadersElement.AppendChild(TaalElement);

            XmlElement ContactNotitieElement = doc.CreateElement(string.Empty, "Contactnotities", string.Empty);
            XmlText ContactNotitiewaarde = doc.CreateTextNode("1");
            ContactNotitieElement.AppendChild(ContactNotitiewaarde);
            ContactHeadersElement.AppendChild(ContactNotitieElement);

            XmlElement ContactActiefElement = doc.CreateElement(string.Empty, "Contactactief", string.Empty);
            XmlText ContactActiefwaarde = doc.CreateTextNode("1");
            ContactActiefElement.AppendChild(ContactActiefwaarde);
            ContactHeadersElement.AppendChild(ContactActiefElement);

            XmlElement BtwnummerElement = doc.CreateElement(string.Empty, "Btwnummer", string.Empty);
            XmlText Btwnummerwaarde = doc.CreateTextNode("1");
            BtwnummerElement.AppendChild(Btwnummerwaarde);
            ContactHeadersElement.AppendChild(BtwnummerElement);

            XmlElement Niveau2Element = doc.CreateElement(string.Empty, "Niveau2", string.Empty);
            XmlText N2waarde = doc.CreateTextNode("1");
            Niveau2Element.AppendChild(N2waarde);
            ContactHeadersElement.AppendChild(Niveau2Element);

            XmlElement Niveau3Element = doc.CreateElement(string.Empty, "Niveau3", string.Empty);
            XmlText N3waarde = doc.CreateTextNode("1");
            Niveau3Element.AppendChild(N3waarde);
            ContactHeadersElement.AppendChild(Niveau3Element);

            XmlElement AttesttypeElement = doc.CreateElement(string.Empty, "Attesttype", string.Empty);
            XmlText Attesttypewaarde = doc.CreateTextNode("1");
            AttesttypeElement.AppendChild(Attesttypewaarde);
            ContactHeadersElement.AppendChild(AttesttypeElement);

            XmlElement ErkenningsnummerElement = doc.CreateElement(string.Empty, "Erkenningsnummer", string.Empty);
            XmlText Erkenningsnummerwaarde = doc.CreateTextNode("1");
            ErkenningsnummerElement.AppendChild(Erkenningsnummerwaarde);
            ContactHeadersElement.AppendChild(ErkenningsnummerElement);

            XmlElement StartdatumElement = doc.CreateElement(string.Empty, "Startdatum", string.Empty);
            XmlText Startdatumnummerwaarde = doc.CreateTextNode("1");
            StartdatumElement.AppendChild(Startdatumnummerwaarde);
            ContactHeadersElement.AppendChild(StartdatumElement);

            XmlElement EinddatumElement = doc.CreateElement(string.Empty, "Einddatum", string.Empty);
            XmlText Einddatumnummerwaarde = doc.CreateTextNode("1");
            EinddatumElement.AppendChild(Einddatumnummerwaarde);
            ContactHeadersElement.AppendChild(EinddatumElement);

            XmlElement BedrijfElement = doc.CreateElement(string.Empty, "Bedrijf", string.Empty);
            XmlText BDwaarde = doc.CreateTextNode("1");
            BedrijfElement.AppendChild(BDwaarde);
            ContactHeadersElement.AppendChild(BedrijfElement);

            XmlElement WebsiteElement = doc.CreateElement(string.Empty, "Website", string.Empty);
            XmlText Websitewaarde = doc.CreateTextNode("1");
            WebsiteElement.AppendChild(Websitewaarde);
            ContactHeadersElement.AppendChild(WebsiteElement);

            XmlElement TelefoonElement = doc.CreateElement(string.Empty, "Telefoon", string.Empty);
            XmlText TELwaarde = doc.CreateTextNode("1");
            BedrijfElement.AppendChild(TELwaarde);
            ContactHeadersElement.AppendChild(TelefoonElement);

            XmlElement FaxElement = doc.CreateElement(string.Empty, "Fax", string.Empty);
            XmlText FAXwaarde = doc.CreateTextNode("1");
            BedrijfElement.AppendChild(FAXwaarde);
            ContactHeadersElement.AppendChild(FaxElement);

            XmlElement EmailElement = doc.CreateElement(string.Empty, "Email", string.Empty);
            XmlText EMwaarde = doc.CreateTextNode("1");
            EmailElement.AppendChild(EMwaarde);
            ContactHeadersElement.AppendChild(EmailElement);

            XmlElement AdresElement = doc.CreateElement(string.Empty, "Adres", string.Empty);
            XmlText ADwaarde = doc.CreateTextNode("1");
            EmailElement.AppendChild(ADwaarde);
            ContactHeadersElement.AppendChild(AdresElement);

            XmlElement PostcodeElement = doc.CreateElement(string.Empty, "Postcode", string.Empty);
            XmlText PCwaarde = doc.CreateTextNode("1");
            EmailElement.AppendChild(PCwaarde);
            ContactHeadersElement.AppendChild(PostcodeElement);

            XmlElement GemeenteElement = doc.CreateElement(string.Empty, "Gemeente", string.Empty);
            XmlText GEwaarde = doc.CreateTextNode("1");
            EmailElement.AppendChild(GEwaarde);
            ContactHeadersElement.AppendChild(GemeenteElement);

            XmlElement BedrijfsNotitiesElement = doc.CreateElement(string.Empty, "Notities", string.Empty);
            XmlText Bedrijfsnotitiewaarde = doc.CreateTextNode("1");
            BedrijfsNotitiesElement.AppendChild(Bedrijfsnotitiewaarde);
            ContactHeadersElement.AppendChild(BedrijfsNotitiesElement);

            XmlElement BedrijfActiefElement = doc.CreateElement(string.Empty, "Bedrijfactief", string.Empty);
            XmlText BedrijfActiefwaarde = doc.CreateTextNode("1");
            BedrijfActiefElement.AppendChild(BedrijfActiefwaarde);
            ContactHeadersElement.AppendChild(BedrijfActiefElement);

            XmlElement NieuwsbriefMailElement = doc.CreateElement(string.Empty, "NieuwsbriefMail", string.Empty);
            XmlText NMwaarde = doc.CreateTextNode("1");
            NieuwsbriefMailElement.AppendChild(NMwaarde);
            ContactHeadersElement.AppendChild(NieuwsbriefMailElement);

            XmlElement NieuwsbriefFaxElement = doc.CreateElement(string.Empty, "NieuwsbriefFax", string.Empty);
            XmlText NFwaarde = doc.CreateTextNode("1");
            NieuwsbriefFaxElement.AppendChild(NFwaarde);
            ContactHeadersElement.AppendChild(NieuwsbriefFaxElement);

            XmlElement GewijzigdElement = doc.CreateElement(string.Empty, "Gewijzigd", string.Empty);
            XmlText GWwaarde = doc.CreateTextNode("0");
            GewijzigdElement.AppendChild(GWwaarde);
            ContactHeadersElement.AppendChild(GewijzigdElement);

            XmlElement GewijzigdDoorElement = doc.CreateElement(string.Empty, "GewijzigdDoor", string.Empty);
            XmlText GWDwaarde = doc.CreateTextNode("0");
            GewijzigdDoorElement.AppendChild(GWDwaarde);
            ContactHeadersElement.AppendChild(GewijzigdDoorElement);

            XmlElement AangemaaktElement = doc.CreateElement(string.Empty, "Aangemaakt", string.Empty);
            XmlText AMwaarde = doc.CreateTextNode("0");
            AangemaaktElement.AppendChild(AMwaarde);
            ContactHeadersElement.AppendChild(AangemaaktElement);

            XmlElement AangemaaktDoorElement = doc.CreateElement(string.Empty, "AangemaaktDoor", string.Empty);
            XmlText AMDwaarde = doc.CreateTextNode("0");
            AangemaaktDoorElement.AppendChild(AMDwaarde);
            ContactHeadersElement.AppendChild(AangemaaktDoorElement);

            XmlElement FunctieactiefElement = doc.CreateElement(string.Empty, "Functieactief", string.Empty);
            XmlText Functieactiefwaarde = doc.CreateTextNode("0");
            FunctieactiefElement.AppendChild(Functieactiefwaarde);
            ContactHeadersElement.AppendChild(FunctieactiefElement);

            doc.Save(FileName);
            return FileName;
        }

        public static bool CheckLocatie(System.Windows.Controls.ComboBox box, Locatie locatie)
        {
            bool result = true;
            if (box.Name == "beamerComboBox")
            {
                if ((((ComboBoxItem)box.SelectedItem).Content.ToString()) == "Locatie")
                {
                    if (locatie != null)
                    {
                        if (locatie.Beamer == false)
                            result = false;
                    }
                }
            }
            else if (box.Name == "laptopComboBox")
            {
                if (box.Text == "Locatie")
                {
                    if (locatie != null)
                    {
                        if (locatie.Laptop == false)
                            result = false;
                    }
                }
            }
            return result;
        }

        public static bool deelnemerExcists(Deelnemer deelnemer)
        {
            DeelnemerServices dService = new DeelnemerServices();
            return dService.CheckIfExcists(deelnemer);
        }

        public static bool CheckEmail(String email)
        {
            try
            {
                return Regex.IsMatch(email,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static void OpenUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception)
                {
                    throw new Exception(ErrorMessages.UrlFout);
                }
            }

        }

        public static T FindChild<T>(DependencyObject parent, string childName)
            where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;
            T foundChild = null;
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);
                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }
            return foundChild;
        }

        public static string CleanForExcel(string s)
        {
            string result = "";
            if (!string.IsNullOrEmpty(s))
            {
                Regex regx = new Regex(";");
                result = regx.Replace(s, ",");
                result = result.Replace("\r\n", " ");
                result = result.Replace("\n", " ");
            }

            return result;
        }

        public static string NextCSVPath(string p, int i)
        {
            string path = "";
            string temp = p.Substring(0, p.Length - 4);
            if (temp.Contains('('))
                temp = temp.Substring(0, temp.IndexOf('('));
            path = temp + "(" + i + ").csv";
            return path;
        }


        public static string CleanPhonenumber(string number)
        {
            string tempnumber = number.Replace("+", "00");
            Regex rgx = new Regex("[^0-9]");
            string result = rgx.Replace(tempnumber, "");
            if (result.StartsWith("32"))
            {
                result = "00" + result;
            }
            else if (!result.StartsWith("0032"))
            {
                if (result.Length > 0)
                    result = "0032" + result.Substring(1, result.Length - 1);
                else
                    result = "0032";
            }
            return result;
        }

        public static string CleanVatnumber(string number)
        {
            Regex rgx = new Regex("[^0-9]");
            string tempvat = "BE" + rgx.Replace(number, "");
            return tempvat;
        }

        public static void StartOutlook()
        {
            Process[] pName = Process.GetProcessesByName("OUTLOOK");
            if (pName.Length == 0)
            {
                try
                {
                    Process.Start(@"C:\Program Files\Microsoft Office 15\root\office15\outlook.exe");
                }
                catch (Exception)
                {
                    try
                    {
                        Process.Start(@"C:\Program Files\Microsoft Office\Office15\OUTLOOK.EXE");
                    }
                    catch (Exception)
                    {
                        throw new Exception("Kan outlook niet starten!\nAls outlook nog niet gestart is start deze en klik dan op OK.");
                    }
                }
            }
        }

        public static void SetAantalByInschrijvingen(Seminarie_Erkenningen erkenning, string user, bool update)
        {
            InschrijvingenService iService = new InschrijvingenService();
            List<Inschrijvingen> inschrijvingen = new List<Inschrijvingen>();
            Erkenning fullerkenning = new ErkenningServices().GetErkenningByID(erkenning.Erkenning_ID);
            if (erkenning.Sessie_ID != null)
            {
                inschrijvingen = iService.GetInschrijvingBySessieID((decimal)erkenning.Sessie_ID);
            }
            else
            {
                inschrijvingen = iService.GetInschrijvingBySeminarieID(erkenning.Seminarie_ID);
            }

            foreach (Inschrijvingen inschrijving in inschrijvingen)
            {
                try
                {
                    if (inschrijving.Functies.Attesttype_ID == fullerkenning.AttestType)
                    {
                        if (update)
                        {
                            inschrijving.TAantal = erkenning.TAantal;
                            inschrijving.Gewijzigd = DateTime.Now;
                            inschrijving.Gewijzigd_door = user;
                            iService.SaveInschrijvingCHanges(inschrijving);
                        }
                        else if (inschrijving.TAantal == null || inschrijving.TAantal == "")
                        {
                            //inschrijving.DecimalAantal = erkenning.Aantal;
                            inschrijving.TAantal = "";
                            inschrijving.Gewijzigd = DateTime.Now;
                            inschrijving.Gewijzigd_door = user;
                            iService.SaveInschrijvingCHanges(inschrijving);
                        }
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        public static void ToExcel(string url, string[][] list)
        {
            // Naar CSV
            CSVDocument doc = new CSVDocument(url);
            foreach (string[] data in list)
            {
                List<string> conta = new List<string>();
                foreach (string waarde in data)
                {
                    conta.Add(waarde);
                }
                doc.AddRow(conta);
            }
            doc.Save();
            System.Diagnostics.Process.Start(url);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class FtpItem
    {
        public string Size { get; set; }
        public string Filename { get; set; }
        public DateTime Modified { get; set; }
        public string Strdate { get; set; }
        public string FullLine { get; set; }

        public FtpItem()
        {

        }

        public FtpItem(string s)
        {
            string temp = s.Substring(28).Trim();
            int month = 0, year = DateTime.Today.Year;
            string day = "";

            FullLine = s;
            Size = temp.Substring(0, temp.IndexOf(' '));
            temp = temp.Substring(temp.IndexOf(' ')).Trim();
            month = GetMonth(temp.Substring(0, temp.IndexOf(' ')));

            temp = temp.Substring(temp.IndexOf(' ')).Trim();
            day = temp.Substring(0, temp.IndexOf(' '));
            temp = temp.Substring(temp.IndexOf(' ')).Trim();
            string stryear = temp.Substring(0, temp.IndexOf(' '));
            if (stryear.IndexOf(':') == -1)
                year = int.Parse(stryear);

            Modified = new DateTime(year, month, int.Parse(day));
            temp = temp.Substring(temp.IndexOf(' ')).Trim();
            Filename = temp;
        }

        private int GetMonth(string m)
        {
            int month = 0;
            switch (m)
            {
                case "Jan":
                    month = 1;
                    break;
                case "Feb":
                    month = 2;
                    break;
                case "Mar":
                    month = 3;
                    break;
                case "Apr":
                    month = 4;
                    break;
                case "May":
                    month = 5;
                    break;
                case "Jun":
                    month = 6;
                    break;
                case "Jul":
                    month = 7;
                    break;
                case "Aug":
                    month = 8;
                    break;
                case "Sep":
                    month = 9;
                    break;
                case "Oct":
                    month = 10;
                    break;
                case "Nov":
                    month = 11;
                    break;
                case "Dec":
                    month = 12;
                    break;
                default:
                    break;
            }
            return month;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class ConfocusCheckBoxListBoxItem
    {
        public decimal ID { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }
}

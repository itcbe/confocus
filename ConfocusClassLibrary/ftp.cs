﻿using ConfocusDBLibrary;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace ConfocusClassLibrary
{
    public class ftp
    {
        private string _baseUri;
        private NetworkCredential _credentials;
        private string _username;
        private string _password;
        private string _downloadedFtpPath;
        private string _backupPath;
        private string _sessieFolder = "Sessies";

        public string DownloadedFtpPath
        {
            get { return _downloadedFtpPath; }
            set { _downloadedFtpPath = value; }
        }


        public string BaseUri
        {
            get { return _baseUri; }
            set { _baseUri = value; }
        }

        public NetworkCredential Credentials
        {
            get { return _credentials; }
            set { _credentials = value; }
        }
        /// <summary>
        /// Constructor 
        /// </summary>
        public ftp()
        {
            //Verzamelen van de logingegevens voor de FTP server
            Instelling_Service iService = new Instelling_Service();
            Instelling inst = iService.Find();
            _baseUri = inst.FTPHost;
            _credentials = new NetworkCredential(inst.FTPGebruikersnaam, inst.FTPPaswoord);
            _username = inst.FTPGebruikersnaam;
            _password = inst.FTPPaswoord;
            _downloadedFtpPath = inst.NieuweInschrijvingenPad;
            _backupPath = inst.BackupFolder;
        }

        /// <summary>
        /// Zorgt voor het afhalen en verplaatsen van de XML's naar
        /// 1. De map F:\Nieuwe FTP
        /// 2. Naar de map op de FTP server Verwerkte FTP
        /// </summary>
        public void GetFTP()
        {
            try
            {
                // Haal de bestandsnamen op voor het downloaden van de XML's
                string[] files = GetFileList();
                if (files != null)
                {
                    foreach (string file in files)
                    {
                        if (file != _backupPath && file != _sessieFolder && file != "Verwerkte FTP_oud")
                        { 
                            // Download het bestand van de FTP server naar de Map "F:Nieuwe FTP"
                            Download(file);
                            // Hernoemt het gedownloade bestand op de ftp server naar de map Verwerkte FTP
                            MoveFile(file);
                        }
                    }
                }
                else
                    throw new Exception(ErrorMessages.GeenFTPInschrijvingenGevonden);
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessages.FoutBijHetLadenVanDeFTP + "\n" + ex.Message);
            }
        }

        public void GetSFTP()
        {
            try
            {
                using (var client = new SftpClient(_baseUri, _username, _password))
                {
                    client.Connect();
                    var files = client.ListDirectory("");//confocus-FTP/
                    int movedfiles = 0;
                    foreach (SftpFile file in files)
                    {
                        Console.WriteLine(file.Name);
                        if (file.Name != "Sessies" && file.Name != "Verwerkte FTP" && file.Name != "Verwerkte FTP_oud" && file.Name != "." && file.Name != "..")
                        {
                            using (Stream stream = File.OpenWrite(_downloadedFtpPath + "\\" + file.Name))
                            {
                                client.DownloadFile(file.FullName, stream);
                            }
                            List<SftpFile> backupfiles = client.ListDirectory("/Verwerkte FTP/") as List<SftpFile>;
                            string fullbackupfilename = _backupPath + "//" + file.Name;
                            SftpFile backupfile = (from f in backupfiles where f.Name == file.Name select f).FirstOrDefault();
                            if (backupfile != null)
                                client.DeleteFile(file.FullName);
                            else
                                client.RenameFile(file.FullName, fullbackupfilename);
                            movedfiles++;
                        }
                    }
                    if (movedfiles == 0)
                        throw new Exception(ErrorMessages.GeenFTPInschrijvingenGevonden);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessages.FoutBijHetLadenVanDeFTP + "\n" + ex.Message);
            }
        }

        public void GetVerwerkteFTP()
        {
            try
            {
                // Haalt de bestandsnamen op van de FTP Map Verwerkte FTP om te downloaden
                List<FtpItem> files = GetVerwerkteFileList();
                if (files != null)
                {
                    foreach (FtpItem file in files)
                    {
                        string downloadPath = @"F:\Controlexlm\ ";
                        // als het bestand nog niet aanwezig is in de Map download de xml dan
                        if (!File.Exists(downloadPath + file.Filename))
                        {
                            DownloadVerwerkte(" " + file.Filename);
                        }
                    }
                }
                else
                    throw new Exception(ErrorMessages.GeenFTPInschrijvingenGevonden);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ErrorMessages.FoutBijHetLadenVanDeFTP + "\n" + ex.Message);
            }
        }

        /// <summary>
        /// Method voor het downloaden van de xml's voor het controlescript 
        /// </summary>
        /// <param name="file"></param>
        private void DownloadVerwerkte(string file)
        {
            try
            {
                string uri = @"ftp://confocus-ftp.itc.be/Verwerkte%20FTP/" + file;
                Uri serverUri = new Uri(uri);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return;
                }
                FtpWebRequest reqFTP;
                reqFTP = (FtpWebRequest)WebRequest.Create(uri);
                reqFTP.Credentials = _credentials;
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream responseStream = response.GetResponseStream();
                FileStream writeStream = new FileStream(@"C:\Controlexlm\" + file, FileMode.Create);
                int Length = 2048;
                Byte[] buffer = new Byte[Length];
                int bytesRead = responseStream.Read(buffer, 0, Length);
                while (bytesRead > 0)
                {
                    writeStream.Write(buffer, 0, bytesRead);
                    bytesRead = responseStream.Read(buffer, 0, Length);
                }
                writeStream.Close();
                response.Close();
            }
            catch (WebException wEx)
            {
                throw new Exception(wEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Method om de bestandsnamen op te halen van de FTP server om dan te gebruiken om te downloaden.
        /// </summary>
        /// <returns>Array van bestandsnamen</returns>
        public string[] GetFileList()
        {
            string[] downloadFiles;
            StringBuilder result = new StringBuilder();
            WebResponse response = null;
            StreamReader reader = null;
            try
            {
                FtpWebRequest reqFTP;
                reqFTP = (FtpWebRequest)WebRequest.Create(new Uri(_baseUri));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = _credentials;
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.Proxy = null;
                reqFTP.KeepAlive = false;
                reqFTP.UsePassive = false;
                response = reqFTP.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                // to remove the tailing '\n'
                result.Remove(result.ToString().LastIndexOf('\n'), 1);
                return result.ToString().Split('\n');
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                }
                downloadFiles = null;
                return downloadFiles;
            }
        }

        /// <summary>
        /// Method om de bestandsnamen op te halen van de FTP server om dan te gebruiken om te downloaden en te controleren door het controlescript.
        /// </summary>
        /// <returns>List van FtpItems</returns>
        public List<FtpItem> GetVerwerkteFileList()
        {
            List<FtpItem> xmlfiles = new List<FtpItem>();
            WebResponse response = null;
            StreamReader reader = null;
            int counter = 0;
            try
            {
                FtpWebRequest reqFTP;
                Uri uri = new Uri(@"ftp://confocus-ftp.itc.be/Verwerkte%20FTP/");
                reqFTP = (FtpWebRequest)WebRequest.Create(uri);
                reqFTP.Credentials = _credentials;
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                response = reqFTP.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    FtpItem f = new FtpItem(line);
                    xmlfiles.Add(f);
                    line = reader.ReadLine();
                    counter++;
                }
                return xmlfiles;
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                }
                
                return new List<FtpItem>();
            }
        }

        public List<FtpItem> GetVerwerkteFilesFromSFTP(string controleuri)
        {
            List<FtpItem> xmlfiles = new List<FtpItem>();
            try
            {
                using (var client = new SftpClient(_baseUri, _username, _password))
                {
                    client.Connect();
                    var files = client.ListDirectory("/Verwerkte FTP/");
                    //int movedfiles = 0;
                    foreach (SftpFile file in files)
                    {
                        if (file.Name != "." && file.Name != "..")
                        {
                            FtpItem fitem = new FtpItem();
                            fitem.Filename = file.Name;
                            fitem.Modified = file.LastWriteTime;
                            xmlfiles.Add(fitem);
                            if (!File.Exists(controleuri + "\\" + file.Name))
                            {
                                using (Stream stream = File.OpenWrite(controleuri + "\\" + file.Name))
                                {
                                    client.DownloadFile(file.FullName, stream);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessages.FoutBijHetLadenVanDeFTP + "\n" + ex.Message);
            }
            return xmlfiles;
        }

        /// <summary>
        /// Method voor het werkelijk downloaden van de XML.
        /// </summary>
        /// <param name="file"></param>
        private void Download(string file)
        {
            try
            {
                string uri = _baseUri + file;
                Uri serverUri = new Uri(uri);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return;
                }
                FtpWebRequest reqFTP;
                reqFTP = (FtpWebRequest)WebRequest.Create(new Uri(_baseUri + file));
                reqFTP.Credentials = _credentials;
                reqFTP.KeepAlive = false;
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Proxy = null;
                reqFTP.UsePassive = false;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream responseStream = response.GetResponseStream();
                FileStream writeStream = new FileStream(_downloadedFtpPath + "\\" + file, FileMode.Create);
                int Length = 2048;
                Byte[] buffer = new Byte[Length];
                int bytesRead = responseStream.Read(buffer, 0, Length);
                while (bytesRead > 0)
                {
                    writeStream.Write(buffer, 0, bytesRead);
                    bytesRead = responseStream.Read(buffer, 0, Length);
                }
                writeStream.Close();
                response.Close();
            }
            catch (WebException wEx)
            {
                throw new Exception(wEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Method voor het hernoemen van de de gedownloade bestanden op de FTP server.
        /// Klaarmaken voor het verplaatsen van de Root naar Verwerkte FTP
        /// </summary>
        /// <param name="file"></param>
        private void MoveFile(string file)
        {
            bool saved = false;
            int version = 0;
            while (!saved)
            {
                string filename = "";
                if (version == 0)
                    filename = file.Substring(0, file.Length - 4);
                else if (version == 1)
                    filename = file.Substring(0, file.Length - 4) + "(" + version + ")";
                else
                    filename = file.Substring(0, (file.Length - 4 - 2 - version.ToString().Length)) + "(" + version + ")";
                filename += ".xml";
                try
                {
                    RenameFile(filename);
                    saved = true;
                }
                catch (WebException)
                {
                    version++;
                }
                catch (Exception)
                {
                    version++;
                }
            }
        }

        /// <summary>
        /// De werkelijk verplaatsing
        /// </summary>
        /// <param name="file"></param>
        private void RenameFile(string file)
        {
            try
            {
                FtpWebRequest requestFileMove = (FtpWebRequest)WebRequest.Create(new Uri(_baseUri + file));
                requestFileMove.Credentials = _credentials;
                requestFileMove.Method = WebRequestMethods.Ftp.Rename;
                requestFileMove.RenameTo = _backupPath + "/ " + file;
                requestFileMove.GetResponse();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method voor het uploaden van de sessie gegevens naar de FTP server voor de website
        /// </summary>
        /// <param name="file"></param>
        public void UploadSessieXmlFile(string file)
        {
            var fileparts = file.Split('\\');
            string filename = fileparts[(fileparts.Length - 1)];
            FtpWebRequest requestFileUpload = (FtpWebRequest)WebRequest.Create(new Uri(_baseUri + "/Sessies/" + filename));
            requestFileUpload.Credentials = _credentials;
            requestFileUpload.Method = WebRequestMethods.Ftp.UploadFile;

            StreamReader sourceStream = new StreamReader(file);
            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            requestFileUpload.ContentLength = fileContents.Length;

            Stream requestStream = requestFileUpload.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)requestFileUpload.GetResponse();

            if (response.StatusCode != FtpStatusCode.CommandOK)
                throw new Exception(response.StatusCode.ToString());

        }

        public void UploadSessieXMLFileToSFTP(string file)
        {
            try
            {
                using (var client = new SftpClient(_baseUri, _username, _password))//hostingftp.itc.be
                {
                    client.Connect();
                    var fileparts = file.Split('\\');
                    string filename = fileparts[(fileparts.Length - 1)];
                    using (Stream stream = File.OpenRead(file))
                    {
                        client.UploadFile(stream, "Sessies/" + filename);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het uploaden van de XML!!\n" + ex.Message);
            }
        }
    }
}

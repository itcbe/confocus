﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class Attachment
    {
        public String Naam { get; set; }
        public String Link { get; set; }

        public Attachment() { }
    }
}

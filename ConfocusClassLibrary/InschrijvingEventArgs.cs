﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = ConfocusDBLibrary;

namespace ConfocusClassLibrary
{
    public class InschrijvingEventArgs : EventArgs
    {
        public data.Inschrijvingen Inschrijving { get; set; }
        public InschrijvingEventArgs(data.Inschrijvingen inschrijving)
        {
            Inschrijving = inschrijving;
        }
    }
}

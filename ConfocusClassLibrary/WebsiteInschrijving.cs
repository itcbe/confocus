﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class WebsiteInschrijving
    {
        private Functies _personeel;
        private Functies _opleiding;
        public string Bedrijfsnaam { get; set; }
        public string BTWNummer { get; set; }
        public string Adres { get; set; }
        public string Postcode { get; set; }
        public string Gemeente { get; set; }
        public string Telefoon { get; set; }
        public string Fax { get; set; }
        public bool Identiek
        {
            get
            {
                return Bedrijfsnaam == FacturatieBedrijfsNaam;
            }
        }
        public string FacturatieBedrijfsNaam { get; set; }
        public string FacturatieBTWNummer { get; set; }
        public string FacturatieAdres { get; set; }
        public string FacturatiePostcode { get; set; }
        public string FacturatieGemeente { get; set; }
        public string FacturatieTelefoon { get; set; }
        public string FacturatieFax { get; set; }
        public string Bestelbonnummer { get; set; }
        public string CommunicatieVoornaam { get; set; }
        public string CommunicatieAchternaam { get; set; }
        public string CommunicatieFunctie { get; set; }
        public string CommunicatieEmail { get; set; }
        public string VerantwoordelijkeOpleiding { get; set; }
        public string VerantwoordelijkeOpleidingMail { get; set; }
        public string VerantwoordelijkePersoneel { get; set; }
        public string VerantwoordelijkePersoneelMail { get; set; }
        public string Opmerking { get; set; }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public string FunctieNaam { get; set; }
        public string Geslacht { get; set; }
        public string Email { get; set; }
        public string Taal { get; set; }
        public string Tav { get; set; }
        public string KmoPortefeuille { get; set; }
        public DateTime Startdatum { get; set; }
        public Functies Personeel
        {
            get
            {
                return _personeel;
            }
        }
        public Functies Opleiding
        {
            get
            {
                return _opleiding;
            }
        }
        public DateTime InschrijvingsDatum { get; set; }
        public List<WebSessie> websessies { get; set; }
        public Contact Contact { get; set; }
        public Bedrijf Bedrijf { get; set; }
        public Bedrijf FacturatieBedrijf { get; set; }
        public Functies Functie { get; set; }
        public List<Sessie> Sessies { get; set; }
        public string Erkenningsnummer { get; set; }

        public WebsiteInschrijving()
        {
            websessies = new List<WebSessie>();
        }

        public List<Contact> GetContact()
        {
            List<Contact> contacten = new List<Contact>();
            ContactServices cService = new ContactServices();
            contacten = cService.GetContactenByNamen(Voornaam, Achternaam);

            return contacten;
        }

        public List<Bedrijf> GetBedrijf()
        {
            List<Bedrijf> bedrijven = new List<Bedrijf>();
            BedrijfServices bService = new BedrijfServices();
            bedrijven = bService.GetBedrijfByName(Bedrijfsnaam);
            return bedrijven;
        }

        public  List<Bedrijf> GetFacturatieOp()
        {
            List<Bedrijf> bedrijven = new List<Bedrijf>();
            BedrijfServices bService = new BedrijfServices();
            bedrijven = bService.GetBedrijfByName(FacturatieBedrijfsNaam);
            return bedrijven;
        }

        public void SetVerantwoordelijkePersoneel(Bedrijf bedrijf)
        {
            FunctieServices fService = new FunctieServices();
            //_personeel = fService.GetByNameAndEmail(VerantwoordelijkePersoneel, VerantwoordelijkePersoneelMail);
            _personeel = fService.GetByNameAndEmailAndCompany(VerantwoordelijkePersoneel, VerantwoordelijkePersoneelMail, bedrijf);
        }

        public void SetVerantwoordelijkeOpleiding(Bedrijf bedrijf)
        {
            FunctieServices fService = new FunctieServices();
            //_opleiding = fService.GetByNameAndEmail(VerantwoordelijkeOpleiding, VerantwoordelijkeOpleidingMail);
            //_opleiding = fService.GetByNameAndEmailAndCompany(VerantwoordelijkeOpleiding, VerantwoordelijkeOpleidingMail, bedrijf);
            _opleiding = fService.GetCommunicatiePersoon(CommunicatieVoornaam, CommunicatieAchternaam, CommunicatieEmail, bedrijf);
        }

        public Sessie GetSessie(string sessieId)
        {
            decimal id = 0;
            if (decimal.TryParse(sessieId, out id))
            {
                SessieServices sService = new SessieServices();
                Sessie sessie = sService.GetSessieByID(id);
                return sessie;
            }
            else
                return null;
        }

        public Inschrijvingen GetInschrijving(decimal sessieId)
        {
            try
            {
                Inschrijvingen inschrijving = new Inschrijvingen();
                if (Functie != null)
                {
                    inschrijving.Functie_ID = Functie.ID;
                    inschrijving.Via = "Website";
                    if (Sessies != null)
                        inschrijving.Sessie_ID = sessieId;

                }

                return inschrijving;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Functies> GetFuncties()
        {
            List<Functies> functies = new List<Functies>();
            FunctieServices fService = new FunctieServices();
            functies = fService.GetFunctieByContactNameAndCompany(Voornaam, Achternaam, Bedrijfsnaam);
            return functies;
        }
    }
}

﻿using ConfocusDBLibrary;
using System;
using System.Collections;
using System.Xml;

namespace ConfocusClassLibrary
{
    public static class XMLReader
    {
        private static string _downloadedFtpPath = new Instelling_Service().Find().DataFolder + "\\Nieuwe FTP\\";
        public static WebsiteInschrijving GetInschrijvingFromXml(string Filename)
        {
            try
            {
                string file = _downloadedFtpPath + Filename;
                WebsiteInschrijving inschrijving = new WebsiteInschrijving();
                XmlDocument webinschijving = new XmlDocument();
                webinschijving.Load(file);
                inschrijving.InschrijvingsDatum = GetInschrijvingsDatumFromFile(Filename);
                XmlNode VoornaamNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Voornaam");
                inschrijving.Voornaam = VoornaamNode.InnerXml;
                XmlNode AchternaamNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Naam");
                inschrijving.Achternaam = AchternaamNode.InnerXml;
                XmlNode GeboortedatumNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Functie");
                inschrijving.FunctieNaam = ReplaceAmp(GeboortedatumNode.InnerXml);
                XmlNode GeslachtNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Geslacht");
                try
                {                    
                    inschrijving.Geslacht = GeslachtNode.InnerXml;
                }
                catch (Exception)
                {
          
                }
                XmlNode EmailNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Email");
                inschrijving.Email = ReplaceAmp(EmailNode.InnerXml);
                XmlNode ErkenningsnummerNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Erkenningsnummer");
                inschrijving.Erkenningsnummer  = ErkenningsnummerNode.InnerXml;
                inschrijving.Startdatum = DateTime.Now;
                XmlNode BedrijfNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Bedrijfsnaam");
                string bedrijfsnaam = BedrijfNode.InnerXml;
                inschrijving.Bedrijfsnaam = ReplaceAmp(bedrijfsnaam);
                XmlNode btwnummerNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/BtwNummer");
                inschrijving.BTWNummer = Helper.CleanVatnumber(btwnummerNode.InnerXml);
                XmlNode TelefoonNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Telefoon");
                inschrijving.Telefoon = Helper.CleanPhonenumber(TelefoonNode.InnerXml);
                XmlNode FaxNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Fax");
                inschrijving.Fax = Helper.CleanPhonenumber(FaxNode.InnerXml);
                XmlNode AdresNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Adres");
                inschrijving.Adres = ReplaceAmp(AdresNode.InnerXml);
                XmlNode PostcodeNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Postcode");
                inschrijving.Postcode = PostcodeNode.InnerXml;
                XmlNode GemeenteNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Stad");
                inschrijving.Gemeente = GemeenteNode.InnerXml;
                XmlNode FacturatieAdresNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Adres");
                inschrijving.FacturatieAdres = ReplaceAmp(FacturatieAdresNode.InnerXml);
                XmlNode FacturatieBedrijfNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Bedrijfsnaam");
                string facturatieBedrijfsNaam = FacturatieBedrijfNode.InnerXml;
                inschrijving.FacturatieBedrijfsNaam = ReplaceAmp(facturatieBedrijfsNaam);
                XmlNode FacturatieBTWNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/BtwNummer");
                inschrijving.FacturatieBTWNummer = Helper.CleanVatnumber(FacturatieBTWNode.InnerXml);
                XmlNode FacturatiePostcodeNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Postcode");
                inschrijving.FacturatiePostcode = ReplaceAmp(FacturatiePostcodeNode.InnerXml);
                XmlNode FacturatieGemeenteNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Stad");
                inschrijving.FacturatieGemeente = FacturatieGemeenteNode.InnerXml;
                XmlNode TAVNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Tav");
                inschrijving.Tav = ReplaceAmp(TAVNode.InnerXml);
                XmlNode CommunicatieVoornaamNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Voornaam");
                inschrijving.CommunicatieVoornaam = CommunicatieVoornaamNode.InnerXml;
                XmlNode CommunicatieAchternaamNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Naam");
                inschrijving.CommunicatieAchternaam = CommunicatieAchternaamNode.InnerXml;
                XmlNode CommunicatieFunctieNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Functie");
                inschrijving.CommunicatieFunctie = CommunicatieFunctieNode.InnerXml;
                XmlNode CommunicatieEmailNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Email");
                inschrijving.CommunicatieEmail = CommunicatieEmailNode.InnerXml;
                XmlNode InschrijvingsdatumNode = webinschijving.SelectSingleNode("//Inschrijving/Datum");
                DateTime inschrijvingsdatum;
                if (DateTime.TryParse(InschrijvingsdatumNode.InnerXml, out inschrijvingsdatum))
                {
                    inschrijving.InschrijvingsDatum = inschrijvingsdatum;
                }
                else
                {
                    inschrijving.InschrijvingsDatum = DateTime.Now;
                }
                XmlNode OpmerkingNode = webinschijving.SelectSingleNode("//Inschrijving/Opmerkingen");
                inschrijving.Opmerking = ReplaceAmp(OpmerkingNode.InnerXml);
                try
                {
                    // single sessie
                    WebSessie websessie = new WebSessie();
                    XmlNode nameNode = webinschijving.SelectSingleNode("//Inschrijving/Sessie/Naam");
                    websessie.SessieNaam = ReplaceAmp(nameNode.InnerXml);
                    XmlNode idNode = webinschijving.SelectSingleNode("//Inschrijving/Sessie/ID");
                    websessie.Sessie_Id = idNode.InnerXml;
                    inschrijving.websessies.Add(websessie);

                }
                catch (Exception)
                {
                    // multiple sessies
                    XmlNodeList sessies = webinschijving.SelectNodes("//Inschrijving/Sessies/Sessie");
                    IEnumerator ienum = sessies.GetEnumerator();
                    while (ienum.MoveNext())
                    {
                        WebSessie sessie = new WebSessie();

                        XmlNode nameNode1 = ((XmlNode)ienum.Current).FirstChild;//.SelectSingleNode("//Sessie/Naam");
                        sessie.SessieNaam = ReplaceAmp(nameNode1.InnerXml);
                        XmlNode idNode1 = ((XmlNode)ienum.Current).LastChild;//SelectSingleNode("//Sessie/ID");
                        sessie.Sessie_Id = idNode1.InnerXml;
                        inschrijving.websessies.Add(sessie);
                    }
                }
                return inschrijving;
            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het lezen van het XML document!\n" + ex.Message);
            }
        }

        public static WebsiteInschrijving GetControleInschrijvingFromXml(string Filename)
        {
            try
            {
                string file = @"C:\Controlexlm\" + Filename;
                WebsiteInschrijving inschrijving = new WebsiteInschrijving();
                XmlDocument webinschijving = new XmlDocument();
                webinschijving.Load(file);
                inschrijving.InschrijvingsDatum = GetInschrijvingsDatumFromFile(Filename);
                XmlNode VoornaamNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Voornaam");
                inschrijving.Voornaam = VoornaamNode.InnerXml;
                XmlNode AchternaamNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Naam");
                inschrijving.Achternaam = AchternaamNode.InnerXml;
                XmlNode GeboortedatumNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Functie");
                inschrijving.FunctieNaam = ReplaceAmp(GeboortedatumNode.InnerXml);
                XmlNode GeslachtNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Geslacht");
                try
                {
                    inschrijving.Geslacht = GeslachtNode.InnerXml;
                }
                catch (Exception)
                {

                }
                XmlNode EmailNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Email");
                inschrijving.Email = ReplaceAmp(EmailNode.InnerXml);
                XmlNode ErkenningsnummerNode = webinschijving.SelectSingleNode("//Inschrijving/Deelnemer/Erkenningsnummer");
                inschrijving.Erkenningsnummer = ErkenningsnummerNode.InnerXml;
                inschrijving.Startdatum = DateTime.Now;
                XmlNode BedrijfNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Bedrijfsnaam");
                string bedrijfsnaam = BedrijfNode.InnerXml;
                inschrijving.Bedrijfsnaam = ReplaceAmp(bedrijfsnaam);
                XmlNode btwnummerNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/BtwNummer");
                inschrijving.BTWNummer = Helper.CleanVatnumber(btwnummerNode.InnerXml);
                XmlNode TelefoonNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Telefoon");
                inschrijving.Telefoon = Helper.CleanPhonenumber(TelefoonNode.InnerXml);
                XmlNode FaxNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Fax");
                inschrijving.Fax = Helper.CleanPhonenumber(FaxNode.InnerXml);
                XmlNode AdresNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Adres");
                inschrijving.Adres = ReplaceAmp(AdresNode.InnerXml);
                XmlNode PostcodeNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Postcode");
                inschrijving.Postcode = PostcodeNode.InnerXml;
                XmlNode GemeenteNode = webinschijving.SelectSingleNode("//Inschrijving/Bedrijf/Stad");
                inschrijving.Gemeente = GemeenteNode.InnerXml;
                XmlNode FacturatieAdresNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Adres");
                inschrijving.FacturatieAdres = ReplaceAmp(FacturatieAdresNode.InnerXml);
                XmlNode FacturatieBedrijfNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Bedrijfsnaam");
                string facturatieBedrijfsNaam = FacturatieBedrijfNode.InnerXml;
                inschrijving.FacturatieBedrijfsNaam = ReplaceAmp(facturatieBedrijfsNaam);
                XmlNode FacturatieBTWNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/BtwNummer");
                inschrijving.FacturatieBTWNummer = Helper.CleanVatnumber(FacturatieBTWNode.InnerXml);
                XmlNode FacturatiePostcodeNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Postcode");
                inschrijving.FacturatiePostcode = ReplaceAmp(FacturatiePostcodeNode.InnerXml);
                XmlNode FacturatieGemeenteNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Stad");
                inschrijving.FacturatieGemeente = FacturatieGemeenteNode.InnerXml;
                XmlNode TAVNode = webinschijving.SelectSingleNode("//Inschrijving/Facturatie/Tav");
                inschrijving.Tav = ReplaceAmp(TAVNode.InnerXml);
                XmlNode CommunicatieVoornaamNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Voornaam");
                inschrijving.CommunicatieVoornaam = CommunicatieVoornaamNode.InnerXml;
                XmlNode CommunicatieAchternaamNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Naam");
                inschrijving.CommunicatieAchternaam = CommunicatieAchternaamNode.InnerXml;
                XmlNode CommunicatieFunctieNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Functie");
                inschrijving.CommunicatieFunctie = CommunicatieFunctieNode.InnerXml;
                XmlNode CommunicatieEmailNode = webinschijving.SelectSingleNode("//Inschrijving/Communicatie/Email");
                inschrijving.CommunicatieEmail = CommunicatieEmailNode.InnerXml;
                XmlNode InschrijvingsdatumNode = webinschijving.SelectSingleNode("//Inschrijving/Datum");
                DateTime inschrijvingsdatum;
                if (DateTime.TryParse(InschrijvingsdatumNode.InnerXml, out inschrijvingsdatum))
                {
                    inschrijving.InschrijvingsDatum = inschrijvingsdatum;
                }
                else
                {
                    inschrijving.InschrijvingsDatum = DateTime.Now;
                }
                XmlNode OpmerkingNode = webinschijving.SelectSingleNode("//Inschrijving/Opmerkingen");
                inschrijving.Opmerking = ReplaceAmp(OpmerkingNode.InnerXml);
                try
                {
                    // single sessie
                    WebSessie websessie = new WebSessie();
                    XmlNode nameNode = webinschijving.SelectSingleNode("//Inschrijving/Sessie/Naam");
                    websessie.SessieNaam = ReplaceAmp(nameNode.InnerXml);
                    XmlNode idNode = webinschijving.SelectSingleNode("//Inschrijving/Sessie/ID");
                    websessie.Sessie_Id = idNode.InnerXml;
                    inschrijving.websessies.Add(websessie);

                }
                catch (Exception)
                {
                    // multiple sessies
                    XmlNodeList sessies = webinschijving.SelectNodes("//Inschrijving/Sessies/Sessie");
                    IEnumerator ienum = sessies.GetEnumerator();
                    while (ienum.MoveNext())
                    {
                        WebSessie sessie = new WebSessie();

                        XmlNode nameNode1 = ((XmlNode)ienum.Current).FirstChild;//.SelectSingleNode("//Sessie/Naam");
                        sessie.SessieNaam = ReplaceAmp(nameNode1.InnerXml);
                        XmlNode idNode1 = ((XmlNode)ienum.Current).LastChild;//SelectSingleNode("//Sessie/ID");
                        sessie.Sessie_Id = idNode1.InnerXml;
                        inschrijving.websessies.Add(sessie);
                    }
                }
                return inschrijving;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fout bij het lezen van het XML document!\n" + ex.Message);
                return null;
            }
        }


        private static string ReplaceAmp(string t)
        {
            string result = t.Replace("&amp;", "&");
            return result;
        }

        private static DateTime GetInschrijvingsDatumFromFile(string filename)
        {
            try
            {
                string[] filearray = filename.Split('_');
                string strdatum = filearray[filearray.Length - 1];
                int jaar = int.Parse(strdatum.Substring(0, 4));
                int maand = int.Parse(strdatum.Substring(4, 2));
                int dag = int.Parse(strdatum.Substring(6, 2));
                int uur = int.Parse(strdatum.Substring(8, 2));
                int minuut = int.Parse(strdatum.Substring(10, 2));
                int sec = int.Parse(strdatum.Substring(12, 2));
                return new DateTime(jaar, maand, dag, uur, minuut, sec);

            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }
    }
}

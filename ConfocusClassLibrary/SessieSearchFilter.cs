﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class SessieSearchFilter
    {
        public string Zoektekst { get; set; } = "";
        public string Organisatieweek { get; set; } = "";
        public string InschrijvingStatus { get; set; } = "";
        public Locatie Locatie { get; set; }
        public int AantalDeelnemers { get; set; } = 0;
        public Gebruiker VSessie { get; set; }
        public Gebruiker VTerplaatse { get; set; }
        public List<decimal> Erkenningen { get; set; } = new List<decimal>();
        public string Erkenningstatus { get; set; } = "";
        public string Taalcode { get; set; } = "";
        public string Seminarietype { get; set; } = "";

        public void SetAantalDeelnemers(string t)
        {
            if (!string.IsNullOrEmpty(t))
            {
                int aantal;
                if (int.TryParse(t, out aantal))
                    AantalDeelnemers = aantal;
                else
                    AantalDeelnemers = -1;
            }
            else
                AantalDeelnemers = -1;
        }

    }
}

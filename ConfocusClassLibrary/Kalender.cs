﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class Kalender
    {
        public class Dag
        {
            public int dagNummer { get; set; }
            public int weekdag { get; set; }
            public String Afspraak { get; set; }
            public decimal seminarieNummer { get; set; }

            public Dag(int dagnummer, int weekdag, String afspraak, decimal seminarienr) 
            {
                this.dagNummer = dagnummer;
                this.weekdag = weekdag;
                this.Afspraak = afspraak;
                this.seminarieNummer = seminarienr;
            }
        }

        public class Maand
        {
            public int intMaand { get; set; }
            public String MaandNaam { get; set; }
            public int AantalDagen { get; set; }

            public Maand(DateTime datum)
            {
                this.MaandNaam = datum.ToString("MMMM");
                this.intMaand = datum.Month;
                this.AantalDagen = DateTime.DaysInMonth(datum.Year, datum.Month);
            }
        }

        public DateTime Datum { get; set; }
        public Int16 intWeek { get; set; }
        public List<Dag> Dagen { get; set; }
        public Maand myMaand { get; set; }

        public Kalender(DateTime datum) 
        {
            this.Datum = datum;
            this.myMaand = new Maand(datum);
            this.Dagen = new List<Dag>();
        }

        public void AddDag(DateTime datum, String afspraak, decimal nummer)
        {
            int dag = datum.Day;
            int week = (int)datum.DayOfWeek;
            Dagen.Add(new Dag(dag, week, afspraak, nummer));
        }
    }
}

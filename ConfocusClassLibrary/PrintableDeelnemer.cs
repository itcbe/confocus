﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class PrintableDeelnemer
    {
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public string Bedrijf { get; set; }
        public string Functie { get; set; }
        public string Type { get; set; }
        public bool Cursief { get; set; }
        public bool Toevoegen { get; set; }

        public PrintableDeelnemer() { }

        public PrintableDeelnemer(string achternaam, string voornaam, string bedrijf, string functie, string type, bool cursief)
        {
            this.Voornaam = voornaam;
            this.Achternaam = achternaam;
            this.Bedrijf = bedrijf;
            this.Functie = functie;
            this.Type = type;
            this.Cursief = cursief;
        }
    }
}

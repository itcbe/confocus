﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSFTPConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var client = new SftpClient("hostingftp.itc.be", "empuls-conf", "N6y0F959uE"))
                {
                    client.Connect();
                    Console.WriteLine("Connectie geslaagd.");

                    string file = @"C:\Users\stefa\Documents\test\ 20181206_marc_rummens_1531383098_1395_1.xml";
                    var fileparts = file.Split('\\');
                    string filename = fileparts[(fileparts.Length - 1)];
                    using (Stream stream = File.OpenRead(file))
                    {
                        client.UploadFile(stream, "/Test/" + filename);
                    }

                    //var files = client.ListDirectory("Test/");
                    //foreach (SftpFile file in files)
                    //{
                    //    Console.WriteLine(file.Name);
                    //    if (file.Name != "Sessies" && file.Name != "Verwerkte FTP" && file.Name != "Verwerkte FTP_oud" && file.Name != "." && file.Name != "..")
                    //    {
                    //        using (Stream stream = File.OpenWrite(@"C:\Users\stefa\Documents\test\" + file.Name))//new System.IO.FileStream(@"C:\Users\stefa\Documents\test\" + file.Name, FileMode.Create);
                    //        {
                    //            client.DownloadFile(file.FullName, stream);
                    //        }
                    //        //client.RenameFile(file.FullName, "/Verwerkte FTP/" + file.Name);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fout met de connectie!!\n" + ex.Message);
            }
            Console.ReadLine();
        }
    }
}

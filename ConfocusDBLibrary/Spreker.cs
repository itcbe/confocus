//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConfocusDBLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class Spreker
    {
        public Spreker()
        {
            this.Sessie_Spreker = new HashSet<Sessie_Spreker>();
        }
    
        public decimal ID { get; set; }
        public Nullable<decimal> Bedrijf_ID { get; set; }
        public decimal Contact_ID { get; set; }
        public string Tarief { get; set; }
        public string Onderwerp { get; set; }
        public Nullable<decimal> Waardering { get; set; }
        public Nullable<decimal> Niveau1_ID { get; set; }
        public Nullable<decimal> Niveau2_ID { get; set; }
        public Nullable<decimal> Niveau3_ID { get; set; }
        public Nullable<decimal> Erkenning_ID { get; set; }
        public string CV { get; set; }
        public Nullable<bool> Actief { get; set; }
        public string Aangemaakt_door { get; set; }
        public Nullable<System.DateTime> Aangemaakt { get; set; }
        public string Gewijzigd_door { get; set; }
        public Nullable<System.DateTime> Gewijzigd { get; set; }
        public string Telefoon { get; set; }
        public string Mobiel { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> Startdatum { get; set; }
        public Nullable<System.DateTime> Einddatum { get; set; }
        public string Oorspronkelijke_Titel { get; set; }
        public string Erkenningsnummer { get; set; }
        public string Attesttype { get; set; }
        public string Type { get; set; }
        public Nullable<decimal> AttestType_ID { get; set; }
        public byte[] Modified { get; set; }
    
        public virtual Bedrijf Bedrijf { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual Erkenning Erkenning { get; set; }
        public virtual Niveau1 Niveau1 { get; set; }
        public virtual Niveau2 Niveau2 { get; set; }
        public virtual Niveau3 Niveau3 { get; set; }
        public virtual ICollection<Sessie_Spreker> Sessie_Spreker { get; set; }
        public virtual AttestType AttestType1 { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class SprekerAttestType_Service
    {
        public void Add(SprekerAttestType type)
        {
            using (var entities = new ConfocusEntities5())
            {
                SprekerAttestType gevonden = (from sa in entities.SprekerAttestType
                                              where sa.SprekerID == type.SprekerID
                                              && sa.AttesttypeID == type.AttesttypeID
                                              select sa).FirstOrDefault();
                if (gevonden == null)
                {
                    entities.SprekerAttestType.Add(type);
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Spreker attesttype bestaat reeds!");
            }
        }

        public void Delete(SprekerAttestType type)
        {
            using (var entities = new ConfocusEntities5())
            {
                SprekerAttestType gevonden = (from sa in entities.SprekerAttestType
                                              where sa.ID == type.ID                                              
                                              select sa).FirstOrDefault();
                if (gevonden != null)
                {
                    entities.SprekerAttestType.Remove(gevonden);
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Spreker attesttype niet gevonden om te verwijderen!");
            }
        }

        public List<SprekerAttestType> GetBySprekerID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from sa in entities.SprekerAttestType
                        where sa.SprekerID == id
                        select sa).ToList();
            }
        }
    }
}

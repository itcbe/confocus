﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class MarketingkanalenServices
    {
        public List<Marketingkanalen> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from kanaal in confocusEntities.Marketingkanalen
                        where kanaal.Actief == true
                        orderby kanaal.Marketing_kanaal
                        select kanaal).ToList();
            }
        }

        public void AddMarketingkanaal(Marketingkanalen kanaal)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenkanaal = (from k in confocusEntities.Marketingkanalen
                                     where k.Marketing_kanaal.ToLower() == kanaal.Marketing_kanaal.ToLower()
                                     select k).FirstOrDefault();

                if (gevondenkanaal != null)
                {
                    throw new Exception(ErrorMessages.MarketingkanaalBestaatReeds);
                }
                else
                {
                    confocusEntities.Marketingkanalen.Add(kanaal);                 
                }
                confocusEntities.SaveChanges();
            }
        }

        public void UpdateMarketingkanaal(Marketingkanalen kanaal)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenkanaal = (from k in confocusEntities.Marketingkanalen
                                      where k.ID == kanaal.ID
                                      select k).FirstOrDefault();

                if (gevondenkanaal != null)
                {
                    //if (!gevondenkanaal.Modified.SequenceEqual(kanaal.Modified))
                    //    throw new MarketingConcurrencyException();

                    gevondenkanaal.Marketing_kanaal = kanaal.Marketing_kanaal;
                    gevondenkanaal.Sector = kanaal.Sector;
                    gevondenkanaal.Beschrijving = kanaal.Beschrijving;
                    gevondenkanaal.Bereik = kanaal.Bereik;
                    gevondenkanaal.Contactpersoon = kanaal.Contactpersoon;
                    gevondenkanaal.Functie = kanaal.Functie;
                    gevondenkanaal.Email = kanaal.Email;
                    gevondenkanaal.Website = kanaal.Website;
                    gevondenkanaal.Telefoon = kanaal.Telefoon;
                    gevondenkanaal.Mobiel = kanaal.Mobiel;
                    gevondenkanaal.Overeenkomst = kanaal.Overeenkomst;
                    gevondenkanaal.Adverteren = kanaal.Adverteren;
                    gevondenkanaal.Opmerkingen = kanaal.Opmerkingen;
                    gevondenkanaal.Contacthistoriek = kanaal.Contacthistoriek;
                    gevondenkanaal.Feedback = kanaal.Feedback;
                    gevondenkanaal.Aankondiging_van = kanaal.Aankondiging_van;
                    gevondenkanaal.Taal = kanaal.Taal;
                    gevondenkanaal.Status = kanaal.Status;
                    gevondenkanaal.Feedback_vervolg = kanaal.Feedback_vervolg;
                    gevondenkanaal.Beschrijving_vervolg = kanaal.Beschrijving_vervolg;
                    gevondenkanaal.Gemaakt = kanaal.Gemaakt;
                    gevondenkanaal.Gewijzigd = kanaal.Gewijzigd;
                    gevondenkanaal.Actief = kanaal.Actief;

                    confocusEntities.SaveChanges();
                }
            }
        }

        public Marketingkanalen GetKanaalByID(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenkanaal = (from k in confocusEntities.Marketingkanalen
                                      where k.ID == id
                                      select k).FirstOrDefault();

                return gevondenkanaal;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;

namespace ConfocusDBLibrary
{
    public class ImportMarketingKanalen
    {
        private List<Marketingkanalen> fouteKanalen = new List<Marketingkanalen>();
        private Gebruiker currentUser;

        public ImportMarketingKanalen(Gebruiker user)
        {
            this.currentUser = user;
        }

        public List<Marketingkanalen> readExcel(String filename)
        {

            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook workbook;
            Excel.Worksheet worksheet;
            Excel.Range range;
            workbook = excelApp.Workbooks.Open(filename);
            worksheet = (Excel.Worksheet)workbook.Sheets[1];
            List<Marketingkanalen> myKanalen = new List<Marketingkanalen>();

            try
            {

                int column = 0, row = 0;

                range = worksheet.UsedRange;
                for (row = 2; row <= range.Rows.Count; row++)
                {
                    try
                    {
                        Marketingkanalen myKanaal = new Marketingkanalen();

                        myKanaal.Marketing_kanaal = (range.Cells[row, 1] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 1] as Excel.Range).Value2.ToString();
                        myKanaal.Sector = (range.Cells[row, 2] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 2] as Excel.Range).Value2.ToString();
                        myKanaal.Beschrijving = (range.Cells[row, 3] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 3] as Excel.Range).Value2.ToString();
                        string beschrijvingVervolg = (range.Cells[row, 4] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 4] as Excel.Range).Value2.ToString();
                        if(!string.IsNullOrEmpty(beschrijvingVervolg))
                            myKanaal.Beschrijving += "\n" + beschrijvingVervolg;
                        myKanaal.Bereik = (range.Cells[row, 5] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 5] as Excel.Range).Value2.ToString();
                        myKanaal.Contactpersoon = (range.Cells[row, 6] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 6] as Excel.Range).Value2.ToString();
                        myKanaal.Functie = (range.Cells[row, 7] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 7] as Excel.Range).Value2.ToString();
                        myKanaal.Email = (range.Cells[row, 8] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 8] as Excel.Range).Value2.ToString();
                        string telefoonnummers = (range.Cells[row, 9] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 9] as Excel.Range).Value2.ToString();
                        string mobiel = GetMobielenummers(telefoonnummers);
                        string telefoon = GetTelefoonnummers(telefoonnummers);
                        myKanaal.Telefoon = telefoon;
                        myKanaal.MobielNummers = mobiel;
                        myKanaal.Website = (range.Cells[row, 10] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 10] as Excel.Range).Value2.ToString();
                        myKanaal.Overeenkomst = (range.Cells[row, 11] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 11] as Excel.Range).Value2.ToString();
                        myKanaal.Adverteren = (range.Cells[row, 12] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 12] as Excel.Range).Value2.ToString();
                        myKanaal.Opmerkingen = (range.Cells[row, 13] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 13] as Excel.Range).Value2.ToString();
                        myKanaal.Contacthistoriek = (range.Cells[row, 14] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 14] as Excel.Range).Value2.ToString();
                        myKanaal.Feedback = (range.Cells[row, 15] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 15] as Excel.Range).Value2.ToString();
                        string FeedbackVervolg = (range.Cells[row, 16] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 16] as Excel.Range).Value2.ToString();
                        if (!string.IsNullOrEmpty(FeedbackVervolg))
                            myKanaal.Feedback += "\n" + FeedbackVervolg;
                        myKanaal.Aankondiging_van = (range.Cells[row, 17] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 17] as Excel.Range).Value2.ToString();
                        myKanaal.Taal = (range.Cells[row, 18] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 18] as Excel.Range).Value2.ToString();
                        myKanaal.Status = (range.Cells[row, 19] as Excel.Range).Value2 == null ? "" : (range.Cells[row, 19] as Excel.Range).Value2.ToString();

                        myKanalen.Add(myKanaal);
                    }
                    catch (Exception nex)
                    {                        
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Fout bij het importeren van de Excel!");
            }
            finally
            {
                workbook.Close(true, Missing.Value, Missing.Value);
                excelApp.Quit();
            }
            return myKanalen;
        }

        private string GetTelefoonnummers(string telefoonnummers)
        {
            string telefoon = "";
            string[] nummers;
            if (telefoonnummers.Contains(";") || telefoonnummers.Contains('/'))
            {

                nummers = telefoonnummers.Split(new char[] {';', '/' });
                foreach (string nummer in nummers)
                {
                    if (!nummer.StartsWith("00324") && !nummer.StartsWith(" 00324"))
                        telefoon += nummer + ";";
                }
            }
            else 
            {
                if (!telefoonnummers.StartsWith("00324") && !telefoonnummers.StartsWith(" 00324"))
                    telefoon += telefoonnummers;
            }

            return telefoon;
        }

        private string GetMobielenummers(string telefoonnummers)
        {
            string telefoon = "";
            string[] nummers;
            if (telefoonnummers.Contains(";") || telefoonnummers.Contains('/'))
            {

                nummers = telefoonnummers.Split(new char[] { ';', '/' });
                foreach (string nummer in nummers)
                {
                    if (nummer.StartsWith("00324") || nummer.StartsWith(" 00324"))
                        telefoon += nummer + ";";
                }
            }
            else
            {
                if (telefoonnummers.StartsWith("00324") || telefoonnummers.StartsWith(" 00324"))
                    telefoon += telefoonnummers;
            }


            return telefoon;
        }
    }
}

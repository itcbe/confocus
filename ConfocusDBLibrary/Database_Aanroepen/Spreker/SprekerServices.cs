﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfocusClassLibrary;
using ConfocusDBLibrary.Exceptions;

namespace ConfocusDBLibrary
{
    public class SprekerServices
    {
        public List<Spreker> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("Erkenning").Include("AttestType1")
                        select s).ToList();
            }
        }

        public Spreker SaveSpreker(Spreker sp)
        {
            try
            {
                using (var confocusEntities = new ConfocusEntities5())
                {
                    var query = from s in confocusEntities.Spreker
                                where s.Contact_ID == sp.Contact_ID
                                && s.Bedrijf_ID == sp.Bedrijf_ID
                                && s.Niveau1_ID == sp.Niveau1_ID
                                && s.Email == sp.Email
                                select s;

                    Spreker gevonden = query.FirstOrDefault();

                    if (gevonden != null)
                    {
                        if (sp.Niveau2_ID != null)
                            query = query.AsQueryable().Where(s => s.Niveau2_ID == sp.Niveau2_ID);
                        if (sp.Niveau3_ID != null)
                            query = query.AsQueryable().Where(s => s.Niveau3_ID == sp.Niveau3_ID);

                        gevonden = query.FirstOrDefault();
                    }

                    if (gevonden == null)
                    {
                        confocusEntities.Spreker.Add(sp);
                        confocusEntities.SaveChanges();
                        return sp;
                    }
                    else
                        throw new Exception(ErrorMessages.SprekerBestaatReeds);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Spreker> GetContactName(string zoektext)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                //string vnaam = zoektext, anaam = zoektext;
                //if (zoektext.Contains(' '))
                //{
                //    string[] zoeknamen = zoektext.Split(' ');
                //    if (zoeknamen.Length > 1)
                //    {
                //        vnaam = zoeknamen[0];
                //        anaam = zoeknamen[1];
                //    }

                //}

                string naam = zoektext;
                if (!string.IsNullOrEmpty(zoektext))
                {
                    var query = from s in confocusEntities.Spreker.Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("Erkenning").Include("AttestType1") where s.Actief == true && (s.Einddatum == null || s.Einddatum >= DateTime.Now) select s;

                    query = query.AsQueryable().Where(s => (s.Contact.Voornaam.ToLower() + " " + s.Contact.Achternaam.ToLower()).Contains(zoektext.ToLower()) || (s.Contact.Achternaam.ToLower() + " " + s.Contact.Voornaam.ToLower()).Contains(zoektext.ToLower()));
                    return query.ToList();
                }
                return new List<Spreker>();
                //if (!string.IsNullOrEmpty(zoektext))
                //{
                //    var query = from s in confocusEntities.Spreker.Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("Erkenning").Include("AttestType1") where s.Actief == true select s;

                //    if (vnaam == anaam)
                //        query = query.AsQueryable().Where(s => s.Contact.Voornaam.ToLower().Contains(vnaam.ToLower()) || s.Contact.Achternaam.ToLower().Contains(vnaam.ToLower()));
                //    else
                //    {
                //        query = query.AsQueryable().Where(s => s.Contact.Voornaam.ToLower().Contains(vnaam.ToLower()) && s.Contact.Achternaam.ToLower().Contains(anaam.ToLower()));
                //        if (query.ToList().Count == 0)
                //        {
                //            query = from s in confocusEntities.Spreker.Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("Erkenning").Include("AttestType1") where s.Actief == true select s;
                //            query = query.AsQueryable().Where(s => s.Contact.Voornaam.ToLower().Contains(anaam.ToLower()) && s.Contact.Achternaam.ToLower().Contains(vnaam.ToLower()));
                //        }
                //    }

                //    query = query.AsQueryable().OrderBy(s => s.Contact.Achternaam);

                //    return query.ToList();
                //}
                //else
                //{
                //    return (from s in confocusEntities.Spreker.Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("Erkenning").Include("AttestType1") where s.Actief == true orderby s.Contact.Achternaam select s).ToList();
                //}
            }
        }

        public Spreker ActivateFunction(Spreker spreker)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var query = from s in confocusEntities.Spreker
                            where s.Contact_ID == spreker.Contact_ID
                            && s.Bedrijf_ID == spreker.Bedrijf_ID
                            && s.Niveau1_ID == spreker.Niveau1_ID
                            && s.Email == spreker.Email
                            select s;

                if (spreker.Niveau2_ID != null)
                    query = query.AsQueryable().Where(s => s.Niveau2_ID == spreker.Niveau2_ID);
                if (spreker.Niveau3_ID != null)
                    query = query.AsQueryable().Where(s => s.Niveau3_ID == spreker.Niveau3_ID);

                Spreker gevondenSpreker = query.FirstOrDefault();

                if (gevondenSpreker != null)
                {
                    gevondenSpreker.Bedrijf_ID = spreker.Bedrijf_ID;
                    gevondenSpreker.Contact_ID = spreker.Contact_ID;
                    gevondenSpreker.Tarief = spreker.Tarief;
                    gevondenSpreker.Onderwerp = spreker.Onderwerp;
                    gevondenSpreker.Waardering = spreker.Waardering;
                    gevondenSpreker.Niveau1_ID = spreker.Niveau1_ID;
                    gevondenSpreker.Niveau2_ID = spreker.Niveau2_ID;
                    gevondenSpreker.Niveau3_ID = spreker.Niveau3_ID;
                    //gevondenSpreker.Erkenning_ID = spreker.Erkenning_ID;
                    gevondenSpreker.Attesttype = spreker.Attesttype;
                    gevondenSpreker.AttestType_ID = spreker.AttestType_ID;
                    gevondenSpreker.CV = spreker.CV;
                    gevondenSpreker.Email = spreker.Email;
                    gevondenSpreker.Telefoon = spreker.Telefoon;
                    gevondenSpreker.Mobiel = spreker.Mobiel;
                    gevondenSpreker.Startdatum = spreker.Startdatum;
                    gevondenSpreker.Einddatum = spreker.Einddatum;
                    gevondenSpreker.Oorspronkelijke_Titel = spreker.Oorspronkelijke_Titel;
                    gevondenSpreker.Erkenningsnummer = spreker.Erkenningsnummer;
                    gevondenSpreker.Type = spreker.Type;
                    gevondenSpreker.Actief = spreker.Actief;
                    gevondenSpreker.Gewijzigd = spreker.Gewijzigd;
                    gevondenSpreker.Gewijzigd_door = spreker.Gewijzigd_door;

                    confocusEntities.SaveChanges();
                    return gevondenSpreker;
                }
                else
                    throw new Exception("Spreker niet gevonden!");
            }
        }


        public Spreker SaveSprekerWijzigingen(Spreker sp)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenSpreker = (from s in confocusEntities.Spreker
                                       where s.ID == sp.ID
                                       select s).FirstOrDefault();
                if (gevondenSpreker != null)
                {
                    //if (!gevondenSpreker.Modified.SequenceEqual(sp.Modified))
                    //    throw new SprekerConcurrencyException();

                    gevondenSpreker.Bedrijf_ID = sp.Bedrijf_ID;
                    gevondenSpreker.Contact_ID = sp.Contact_ID;
                    gevondenSpreker.Tarief = sp.Tarief;
                    gevondenSpreker.Onderwerp = sp.Onderwerp;
                    gevondenSpreker.Waardering = sp.Waardering;
                    gevondenSpreker.Niveau1_ID = sp.Niveau1_ID;
                    gevondenSpreker.Niveau2_ID = sp.Niveau2_ID;
                    gevondenSpreker.Niveau3_ID = sp.Niveau3_ID;
                    //gevondenSpreker.Erkenning_ID = sp.Erkenning_ID;
                    gevondenSpreker.Attesttype = sp.Attesttype;
                    gevondenSpreker.AttestType_ID = sp.AttestType_ID;
                    gevondenSpreker.CV = sp.CV;
                    gevondenSpreker.Email = sp.Email;
                    gevondenSpreker.Telefoon = sp.Telefoon;
                    gevondenSpreker.Mobiel = sp.Mobiel;
                    gevondenSpreker.Startdatum = sp.Startdatum;
                    gevondenSpreker.Einddatum = sp.Einddatum;
                    gevondenSpreker.Oorspronkelijke_Titel = sp.Oorspronkelijke_Titel;
                    gevondenSpreker.Erkenningsnummer = sp.Erkenningsnummer;
                    gevondenSpreker.Type = sp.Type;
                    gevondenSpreker.Actief = sp.Actief;
                    gevondenSpreker.Gewijzigd = sp.Gewijzigd;
                    gevondenSpreker.Gewijzigd_door = sp.Gewijzigd_door;

                    confocusEntities.SaveChanges();
                    return gevondenSpreker;
                }
                else
                    return null;
            }
        }

        public List<Spreker> GetReplaceEmail(string t)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Contact").Include("Bedrijf")
                        where (s.Email.ToLower().Contains(t.ToLower())
                        || s.Bedrijf.Email.ToLower().Contains(t.ToLower()))
                        && s.Actief == true
                        && s.Einddatum == null
                        select s).ToList();
            }
        }

        public Spreker GetSprekerByContactBedrijf(decimal contactid, decimal bedrijfid)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker
                        where s.Contact_ID == contactid && s.Bedrijf_ID == bedrijfid
                        select s).FirstOrDefault();
            }
        }

        public List<Spreker> GertSprekersByOnderwerp(string o)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker
                        where s.Onderwerp == o
                        select s).ToList();
            }
        }

        public List<Spreker> GetSprekersByContact(decimal contact)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                DateTime? vandaag = DateTime.Today;
                List<Spreker> sprekers = (from s in confocusEntities.Spreker.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Contact").Include("Bedrijf").Include("AttestType1")
                                          where s.Contact.ID == contact
                                          && s.Actief == true
                                          && (s.Einddatum == null || s.Einddatum >= vandaag)
                                          select s).ToList();

                return sprekers;
            }
        }

        public List<Spreker> GetSprekersByContactBedrijf(decimal contactid, decimal bedrijfid)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("Contact").Include("AttestType1")
                        where s.Contact_ID == contactid && s.Bedrijf_ID == bedrijfid
                        && s.Actief == true
                        && s.Einddatum == null
                        select s).ToList();
            }
        }

        public List<Spreker> GetSprekersInBedrijf(decimal bedrijfid)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("Contact").Include("AttestType1")
                        where s.Bedrijf_ID == bedrijfid
                        && s.Actief == true
                        && s.Einddatum == null
                        select s).ToList();
            }
        }

        public List<Spreker> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("AttestType1")
                        where s.Contact.Actief == true
                        orderby s.Contact.Achternaam, s.Contact.Voornaam
                        select s).ToList();
            }
        }

        public Spreker GetSprekerById(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("AttestType1")
                        where s.ID == id
                        select s).FirstOrDefault();
            }
        }

        public List<Spreker> GetSprekersByZoekterm(ContactZoekTerm zoektext)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var query = from s in confocusEntities.Spreker.Include("Contact").Include("Bedrijf").Include("AttestType1") select s;
                if (!string.IsNullOrEmpty(zoektext.Achternaam))
                {
                    if (zoektext.Achternaam[0] == '*')
                    {
                        string achternaam = zoektext.Achternaam.Substring(1, zoektext.Achternaam.Length - 1);
                        query = query.AsQueryable().Where(s => s.Contact.Achternaam.ToLower().Contains(achternaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(s => s.Contact.Achternaam.ToLower().StartsWith(zoektext.Achternaam.ToLower()));
                }
                if (!string.IsNullOrEmpty(zoektext.Voornaam))
                {
                    if (zoektext.Voornaam[0] == '*')
                    {
                        string voornaam = zoektext.Voornaam.Substring(1, zoektext.Voornaam.Length - 1);
                        query = query.AsQueryable().Where(s => s.Contact.Voornaam.ToLower().Contains(voornaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(s => s.Contact.Voornaam.ToLower().StartsWith(zoektext.Voornaam.ToLower()));
                }
                query = query.AsQueryable().OrderBy(s => s.Contact.Achternaam).ThenBy(s => s.Contact.Voornaam);

                return query.ToList();
            }
        }


        public Contact GetSprekerContact(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Contact").Include("AttestType1")
                        where s.ID == id
                        select s.Contact).FirstOrDefault();
            }
        }

        public Bedrijf GetSprekerBedrijf(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from s in confocusEntities.Spreker.Include("Bedrijf").Include("AttestType1")
                        where s.ID == id
                        select s.Bedrijf).FirstOrDefault();
            }
        }

        public int ReplaceEmail(ReplaceEmail mail)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var gevonden = (from f in entities.Spreker
                                where f.Email.ToLower() == mail.searchText.ToLower()
                                select f).ToList();

                foreach (Spreker spreker in gevonden)
                {
                    spreker.Email = mail.replaceText;
                }
                aantal = gevonden.Count;

                entities.SaveChanges();
                return aantal;
            }
        }

        public List<Spreker> GetBySearch(ContactSearchFilter filter)
        {
            using (var entities = new ConfocusEntities5())
            {

                var query = from s in entities.Spreker.Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("Erkenning").Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("AttestType1") where s.Actief == true select s;

                if (!string.IsNullOrEmpty(filter.Achternaam))
                {
                    if (filter.Achternaam[0] == '*')
                    {
                        string achternaam = filter.Achternaam.Substring(1, filter.Achternaam.Length - 1);
                        query = query.AsQueryable().Where(s => s.Contact.AchternaamFonetisch.ToLower().Contains(achternaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(s => s.Contact.AchternaamFonetisch.ToLower().StartsWith(filter.Achternaam.ToLower()));
                }
                if (!string.IsNullOrEmpty(filter.Voornaam))
                {
                    if (filter.Voornaam[0] == '*')
                    {
                        string voornaam = filter.Voornaam.Substring(1, filter.Voornaam.Length - 1);
                        query = query.AsQueryable().Where(s => s.Contact.VoornaamFonetisch.ToLower().Contains(filter.Voornaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(s => s.Contact.VoornaamFonetisch.ToLower().StartsWith(filter.Voornaam.ToLower()));
                }

                if (filter.Bedrijf_ID != 0)
                {
                    query = query.AsQueryable().Where(s => s.Bedrijf_ID == filter.Bedrijf_ID);
                }

                if (filter.Email == "Leeg")
                {
                    query = query.AsQueryable().Where(s => s.Email == "");
                }
                else if (filter.Email != "")
                    query = query.AsQueryable().Where(s => s.Email.ToLower().Contains(filter.Email.ToLower()));

                if (filter.Niveau1_ID != 0)
                {
                    query = query.AsQueryable().Where(s => s.Niveau1_ID == filter.Niveau1_ID);
                }

                if (filter.Niveau2_ID != 0)
                {
                    query = query.AsQueryable().Where(s => s.Niveau2_ID == filter.Niveau2_ID);
                }

                if (filter.Niveau3_ID != 0)
                {
                    query = query.AsQueryable().Where(s => s.Niveau3_ID == filter.Niveau3_ID);
                }
                if (filter.Gemeente != "")
                {
                    query = query.AsQueryable().Where(s => s.Bedrijf.Plaats.ToLower() == filter.Gemeente.ToLower());
                }

                if (filter.ContactType != "")
                {
                    query = query.AsQueryable().Where(s => s.Type.ToLower() == filter.ContactType.ToLower());
                }

                if (filter.Provincie != "")
                {
                    query = query.AsQueryable().Where(s => s.Bedrijf.Provincie == filter.Provincie);
                }

                if (filter.Aangemaakt > new DateTime(2014, 12, 1))
                {
                    string datum = ((DateTime)filter.Aangemaakt).ToShortDateString();
                    query = query.AsQueryable().Where(c => ((DateTime)c.Aangemaakt).ToShortDateString() == datum);
                }
                if (!string.IsNullOrEmpty(filter.Door))
                {
                    query = query.AsQueryable().Where(c => c.Aangemaakt_door == filter.Door || c.Gewijzigd_door == filter.Door);
                }
                if (filter.Taal_ID != 0)
                {
                    query = query.AsQueryable().Where(s => s.Contact.Taal_ID == filter.Taal_ID);
                }
                if (filter.Faxnummer != "")
                    query = query.AsQueryable().Where(s => s.Bedrijf.Fax.Replace(" ", "").Contains(filter.Faxnummer.Replace(" ", "")));

                query = query.AsQueryable().Where(s => s.Einddatum == null || (DateTime)s.Einddatum >= DateTime.Today).OrderBy(c => c.Contact.Achternaam).ThenBy(c => c.Contact.Voornaam);

                return query.ToList();
            }

        }

        public List<Sessie_Spreker> GetSprekersBySeminarieID(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                //List<Sessie_Spreker> results = new List<Sessie_Spreker>();
                return (from s in entities.Sessie_Spreker.Include("Spreker")
                        where s.Sessie.Seminarie_ID == iD
                        select s).ToList();

                //return results;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Spreker
    {
        public String Sprekernaam 
        {
            get
            {
                Niveau1Services n1service = new Niveau1Services();
                Niveau1 n1 = n1service.GetNiveau1ByID(Niveau1_ID);
                String naam = n1.Naam;
                if (Niveau2_ID != null)
                {
                    Niveau2Services n2service = new Niveau2Services();
                    Niveau2 n2 = n2service.GetNiveau2ByID(Niveau2_ID);
                    naam += " " + n2.Naam;
                }
                if (Niveau3_ID != null)
                {
                    Niveau3Services n3service = new Niveau3Services();
                    Niveau3 n3 = n3service.GetNiveau3ByID(Niveau3_ID);
                    naam += " " + n3.Naam;
                }
                naam +=  " " + Onderwerp;
                return naam;
            }
        }

        public String Functie 
        { 
            get
            {
                Niveau1Services n1service = new Niveau1Services();
                Niveau1 n1 = n1service.GetNiveau1ByID(Niveau1_ID);
                String functie = n1.Naam;
                if (Niveau2_ID != null)
                {
                    Niveau2Services n2service = new Niveau2Services();
                    Niveau2 n2 = n2service.GetNiveau2ByID(Niveau2_ID);
                    functie += " " + n2.Naam;
                }
                if (Niveau3_ID != null)
                {
                    Niveau3Services n3service = new Niveau3Services();
                    Niveau3 n3 = n3service.GetNiveau3ByID(Niveau3_ID);
                    functie += " " + n3.Naam;
                }
                return functie;
            } 
        }

        public string VolledigeNaam
        {
            get
            {
                return this.Contact.VolledigeNaam;
            }
        }

        public string Niveau1naam
        {
            get
            {
                Niveau1 n1 = new Niveau1Services().GetNiveau1ByID(Niveau1_ID);
                return n1.Naam;
            }
        }

        public string Niveau2naam
        {
            get
            {
                Niveau2 n2 = new Niveau2Services().GetNiveau2ByID(Niveau2_ID);
                if (n2 != null)
                    return n2.Naam;
                return "";
            }
        }

        public string Niveau3naam
        {
            get
            {
                Niveau3 n3 = new Niveau3Services().GetNiveau3ByID(Niveau3_ID);
                if (n3 != null)
                    return n3.Naam;
                return "";
            }
        }

        public string NaamEnNiveaus
        {
            get
            {
                
                Bedrijf bedrijf = new BedrijfServices().GetBedrijfByID((decimal)this.Bedrijf_ID);
                string naam = this.Contact.VolledigeNaam + ": " + bedrijf.Naam;
                Niveau1Services n1service = new Niveau1Services();
                Niveau1 n1 = n1service.GetNiveau1ByID(Niveau1_ID);
                naam += ": " + n1.Naam;
                if (Niveau2_ID != null)
                {
                    Niveau2Services n2service = new Niveau2Services();
                    Niveau2 n2 = n2service.GetNiveau2ByID(Niveau2_ID);
                    naam += ", " + n2.Naam;
                }
                if (Niveau3_ID != null)
                {
                    Niveau3Services n3service = new Niveau3Services();
                    Niveau3 n3 = n3service.GetNiveau3ByID(Niveau3_ID);
                    naam += ", " + n3.Naam;
                }

                return naam;
            }
        }

        public string SprekerNaamEnTitel
        {
            get
            {
                return this.Contact.VolledigeNaam + ": " + this.Oorspronkelijke_Titel;
            }
        }

        public string HasCV
        {
            get
            {
                return CV == "" ? "Nee" : "Ja";
            }
        }

        public bool IsEmpty()
        {
            return (this.Niveau1_ID == 0 && this.Contact_ID == 0 && this.Bedrijf_ID == 0);
        }

        public override bool Equals(object obj)
        {
            if (obj is Spreker)
            {
                if (ID == ((Spreker)obj).ID)
                    return true;
                else
                    return false;
            }
            else
                return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}

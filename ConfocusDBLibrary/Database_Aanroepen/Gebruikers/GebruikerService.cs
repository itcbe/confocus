﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class GebruikerService
    {
        public List<Gebruiker> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from g in confocusEntities.Gebruiker.Include("UserRole")
                        select g).ToList();
            }
        }

        public void SaveGebruiker(Gebruiker g)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Gebruiker.Add(g);
                confocusEntities.SaveChanges();
            }
        }

        public void SaveGebruikerWijzigingen(Gebruiker g)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenGebruiker = (from G in confocusEntities.Gebruiker
                                         where G.ID == g.ID
                                         select G).FirstOrDefault();

                if (gevondenGebruiker != null)
                {
                    //if (!gevondenGebruiker.Modified.SequenceEqual(g.Modified))
                    //    throw new UserConcurrencyException();

                    gevondenGebruiker.Naam = g.Naam;
                    gevondenGebruiker.Email = g.Email;
                    gevondenGebruiker.Paswoord = g.Paswoord;
                    gevondenGebruiker.Rol = g.Rol;
                    gevondenGebruiker.Actief = g.Actief;
                    gevondenGebruiker.Gewijzigd = g.Gewijzigd;
                    gevondenGebruiker.Gewijzigd_door = g.Gewijzigd_door;
                    
                    confocusEntities.SaveChanges();
                }
            }
        }

        public Gebruiker GetUserWithEmailAndPassword(string email, string password)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Gebruiker G = (from g in confocusEntities.Gebruiker.Include("UserRole")
                               where g.Email == email && g.Paswoord == password
                               //&& g.Actief == true
                               select g).FirstOrDefault();

                return G;
            }
        }

        public Gebruiker GetUserWithUserNameAndPassword(string UserName, string pass)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Gebruiker G = (from g in confocusEntities.Gebruiker.Include("UserRole")
                               where g.Naam == UserName && g.Paswoord == pass
                               select g).FirstOrDefault();

                return G;
            }
        }

        public string GetConfigFileName(string UserName)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Gebruiker gebruiker = (from g in confocusEntities.Gebruiker.Include("UserRole")
                                       where g.Naam == UserName
                                       select g).FirstOrDefault();
                if (gebruiker.ColumnHeaders != null)
                    return gebruiker.ColumnHeaders;
                else
                    return "";
            }
        }

        public Gebruiker GetUserByName(string Username)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from g in confocusEntities.Gebruiker.Include("UserRole")
                        where g.Naam == Username
                        select g).FirstOrDefault();
            }
        }

        public List<Gebruiker> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from g in confocusEntities.Gebruiker.Include("UserRole")
                        where g.Actief == true
                        select g).ToList();
            }
        }

        public Gebruiker GetUserByID(int? id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from g in confocusEntities.Gebruiker.Include("UserRole")
                        where g.ID == id
                        //where g.Actief == true
                        //&& g.ID == id
                        select g).FirstOrDefault();
            }
        }

        public List<Gebruiker> GetAvtiveGebruikers()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from g in confocusEntities.Gebruiker.Include("UserRole")
                        where g.Actief == true && g.ID > 2
                        select g).ToList();
            }
        }
    }
}

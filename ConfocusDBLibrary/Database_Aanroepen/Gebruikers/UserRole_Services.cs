﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class UserRole_Services
    {
        public List<UserRole> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from r in entities.UserRole
                        where r.Actief == true
                        select r).ToList();
            }
        }

        public UserRole Save(UserRole role)
        {
            using (var entities = new ConfocusEntities5())
            {
                var found = (from r in entities.UserRole
                             where r.Rol == role.Rol
                             select r).FirstOrDefault();

                if (found == null)
                {
                    entities.UserRole.Add(role);
                    entities.SaveChanges();
                    return role;
                }
                else
                    throw new Exception(ErrorMessages.RoleBestaatReeds);

            }
        }

        public void SaveRoleChanges(UserRole role)
        {
            using (var entities = new ConfocusEntities5())
            {
                var found = (from r in entities.UserRole
                             where r.Rol == role.Rol
                             select r).FirstOrDefault();

                if (found == null)
                    throw new Exception(ErrorMessages.RoleNotFound);

                found.Rol = role.Rol;
                found.Actief = role.Actief;
                found.Gewijzigd = role.Gewijzigd;
                found.Gewijzigd_door = role.Gewijzigd_door;
                entities.SaveChanges();
            }
        }

        public void Delete(UserRole role)
        {
            role.Actief = false;
            this.SaveRoleChanges(role);
        }
    }
}

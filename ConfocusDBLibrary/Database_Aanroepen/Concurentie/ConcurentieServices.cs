﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class ConcurentieServices
    {
        public List<Concurentie> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from c in entities.Concurentie
                        select c).ToList();
            }
        }

        public void SaveConcurentie(Concurentie concu)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from c in entities.Concurentie
                                where c.Datum == concu.Datum
                                && c.Omschrijving == concu.Omschrijving
                                select c).FirstOrDefault();

                if (gevonden == null)
                {
                    entities.Concurentie.Add(concu);
                    entities.SaveChanges();
                }
            }
        }

        public Concurentie GetByID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from c in entities.Concurentie
                        where c.ID == id
                        select c).FirstOrDefault();
            }
        }

        public List<Concurentie> GetFromMonth(DateTime datum)
        {
            DateTime EersteDag = new DateTime(datum.Year, datum.Month, 1);
            DateTime LaatsteDag = new DateTime(datum.Year, datum.Month, DateTime.DaysInMonth(datum.Year, datum.Month));
            using (var entities = new ConfocusEntities5())
            {
                return (from c in entities.Concurentie
                        where c.Datum >= EersteDag
                        && c.Datum <= LaatsteDag
                        && c.Actief == true
                        select c).ToList();
            }
        }
    }
}

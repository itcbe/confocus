﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class TaalServices
    {
        public List<Taal> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from taal in confocusEntities.Taal
                        select taal).ToList();
            }
        }

        public void SaveTaal(Taal taal)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Taal.Add(taal);
                confocusEntities.SaveChanges();
            }
        }

        public void SaveTaalWijzigingen(Taal taal)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenTaal = (from t in confocusEntities.Taal
                                    where t.ID == taal.ID
                                    select t).FirstOrDefault();
                if (gevondenTaal != null)
                {
                    gevondenTaal.Naam = taal.Naam;
                    gevondenTaal.Code = taal.Code;

                    confocusEntities.SaveChanges();
                }
            }
        }

        public Taal GetTaalByID(decimal taalid)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from taal in confocusEntities.Taal
                        where taal.ID == taalid
                        select taal).FirstOrDefault();
            }
        }

        public Taal GetTaalByNameOrCode(string taalnaam)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from taal in confocusEntities.Taal
                        where taal.Naam.ToLower() == taalnaam.ToLower() || taal.Code.ToLower() == taalnaam.ToLower()
                        select taal).FirstOrDefault();
            }
        }

    }
}

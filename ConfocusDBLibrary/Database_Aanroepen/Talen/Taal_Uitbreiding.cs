﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Taal
    {
        public override bool Equals(object obj)
        {
            if (obj is Taal)
            {
                Taal deAndere = obj as Taal;
                if (this.ID == deAndere.ID)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
    }
}

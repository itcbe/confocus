﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class HistoriekZoekTerm
    {
        private string _status = "";
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<decimal> SeminarieIDs { get; set; }
        public decimal? SprekerID { get; set; }
        public decimal? FunctieID { get; set; }
        public decimal? ErkenningID { get; set; }
        public decimal? Niveau1ID { get; set; }
        public decimal? Niveau2ID { get; set; }
        public decimal? Niveau3ID { get; set; }
        public decimal? TaalID { get; set; }
        public decimal? ContactID { get; set; }
        public int? VerantwoordelijkeSessie_ID { get; set; }
        public int? VerantwoordelijkeTerplaatse_ID { get; set; }
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public HistoriekZoekTerm()
        {
            SeminarieIDs = new List<decimal>();
        }
    }
}

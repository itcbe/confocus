﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConfocusDBLibrary
{
    public class Instelling_Service
    {
        public Instelling Find()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Instelling select i).FirstOrDefault();
            }
        }

        public void Update(Instelling instelling)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from i in entities.Instelling select i).FirstOrDefault();

                if (gevonden == null)
                    throw new Exception(ErrorMessages.InstellingNietGevonden);
                //if (!gevonden.Modified.SequenceEqual(instelling.Modified))
                //    throw new SettingConcurrencyException();

                gevonden.FTPHost = instelling.FTPHost;
                gevonden.FTPGebruikersnaam = instelling.FTPGebruikersnaam;
                gevonden.FTPPaswoord = instelling.FTPPaswoord;
                gevonden.NieuweInschrijvingenPad = instelling.NieuweInschrijvingenPad;
                gevonden.BackupFolder = instelling.BackupFolder;
                gevonden.Flexmail_User_ID = instelling.Flexmail_User_ID;
                gevonden.Flexmail_User_Token = instelling.Flexmail_User_Token;
                gevonden.DataFolder = instelling.DataFolder;

                entities.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public static class DBHelper
    {
        public static string Simplify(string name)
        {
            string result = "";
            List<Speciaal_teken> tekens = new Speciale_Tekens_Services().GetAll();
            foreach (char kar in name)
            {
                char karakter = kar;
                foreach (Speciaal_teken teken in tekens)
                {
                    if (kar.ToString() == teken.Speciaal_karakter)
                    {
                        karakter = teken.Alternatief[0];
                        break;
                    }
                }
                result += karakter.ToString();
            }
            return result;
        }

        public static void CheckAantalPuntenBijInschrijvingen(Functies functie)
        {
            try
            {
                List<Inschrijvingen> inschrijvingen = new InschrijvingenService().GetInschrijvingByFunctionID(functie.ID);
                foreach (Inschrijvingen inschr in inschrijvingen)
                {
                    List<Seminarie_Erkenningen> erkenningen = new Seminarie_ErkenningenService().GetBySessieID((Decimal)inschr.Sessie_ID);
                    foreach (Seminarie_Erkenningen erkenning in erkenningen)
                    {
                        if (erkenning.Erkenning.AttestType1 == null)
                            continue;
                        if (erkenning.Erkenning.AttestType1.Naam == functie.Attesttype)
                        {
                            inschr.DecimalAantal = erkenning.Aantal;
                            new InschrijvingenService().SaveInschrijvingCHanges(inschr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Functies CheckFaxCollegesSettings(Functies f)
        {
            FunctieServices fService = new FunctieServices();
            List<Functies> functions = fService.GetFunctiesInBedrijf(f.Bedrijf_ID);
            foreach (Functies func in functions)
            {
                if (func.Fax == false)
                {
                    f.Fax = false;
                    f = fService.SaveFunctieWijzigingen(f);
                    break;
                }
            }
            return f;
        }

        public static bool CheckInschrijving(decimal iD, decimal? functieid, decimal contactid)
        {
            List<Inschrijvingen> inschr = new InschrijvingenService().GetLimitedInschrijvingenFromSessie(iD);
            int aantal = inschr.Where(i => i.Functies.Contact_ID == contactid).Count();
            return aantal > 0;
        }
    }
}

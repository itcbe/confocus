﻿namespace ConfocusDBLibrary
{
    public static class ErrorMessages
    {
        /// <summary>
        /// Error constanten
        /// </summary>
        public const string ContactBestaatReeds = "Het contact bestaat reeds!";
        public const string SessieBestaatReeds = "De sessie bestaat reeds voor dit seminarie!";
        public const string BedrijfBestaatReeds = "Het bedrijf bestaat reeds!";
        public const string FunctieBestaatReeds = "Deze functie bestaat reeds voor dit contact!";
        public const string SeminarieBestaatReeds = "De seminarie bestaat reeds!";
        public const string DoelgroepBestaatReeds = "Deze doelgroep is reeds toegevoegd aan het seminarie!";
        public const string DoelgroepNietGevonden = "Doegroep is niet gevonden. kan de wijzigingen niet doorvoeren!";
        public const string EmailInvalid = "Het E-mail adres is niet juist!";
        public const string UrlFout = "Fout bij het openen van de website!\nControleer of de URL correct is en probeer opnieuw.";
        public const string GewijzigdeDataOpen = "Er zijn nog onopgeslagen gegevens! Sla deze eerst op voor je verder gaat.";
        public const string GeenSessieIngegeven = "Er is nog geen sessie aangemaakt voor dit seminarie! Gelieve minstens één sessie aan te maken.";
        public const string SessieIsNull = "Sessie is null of bevat geen waarde!";
        public const string DeelnemerNietOpgeslagen = "Fout bij het opslaan van de deelnemer!";
        public const string MarketingkanaalBestaatReeds = "Het marketingkanaal bestaat reeds!";
        public const string SeminarieMarketingNietGevondenFout = "Er is geen marketing kanaal gevonden in de database om te verwijderen!";
        public const string SessieSprekerNietGevondenFout = "Er is geen spreker gevonden in de database om te verwijderen!";
        public const string DagTypeNietGevondenFout = "Dagtype is niet gevonden. kan de wijzigingen niet doorvoeren!";
        public const string WeergaveBestaatReeds = "Er bestaat reeds een weergave met deze naam!";
        public const string WeergaveNietGevonden = "Weergave niet gevonden!";
        public const string AttestTemplateBestaat = "Het attesttemplate bestaat reeds!";
        public const string GeenFTPInschrijvingenGevonden = "Er zijn geen inschrijvingen gevonden!";
        public const string FoutBijHetLadenVanDeFTP = "Fout bij het lezen van de ftp site!";
        public const string AttestTemplateNotFound = "Geen attesttemplate gevonden!";
        public const string DownLoadKlaar = "Downloaden van de inschrijvingen gelukt.";
        public const string InstellingNietGevonden = "Instelling niet gevonden!";
        public const string SprekerBestaatReeds = "De Spreker bestaat reeds!";
        public const string SprekerReedsToegevoegd = "De spreker is reeds toegevoegd aan deze Sessie!";
        public const string DataBaseBezet = "De database is momenteel bezet! Probeer het later opnieuw.";
        public const string RoleBestaatReeds = "De rol bestaat reeds! Gebruik een andere naam.";
        public const string RoleNotFound = "De rol is niet gevonden";
    }
}

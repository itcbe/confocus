﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class ReplaceEmail
    {
        public string searchText { get; set; }
        public string replaceText { get; set; }

        public ReplaceEmail() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class Level1ConcurrencyException: Exception
    {
        public Level1ConcurrencyException():base("Niveau 1 is ondertussen aangepast door iemand anders!")
        {
                
        }
    }
}

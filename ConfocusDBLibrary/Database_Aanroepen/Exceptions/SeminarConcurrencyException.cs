﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SeminarConcurrencyException: Exception
    {
        public SeminarConcurrencyException():base("Het seminarie is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

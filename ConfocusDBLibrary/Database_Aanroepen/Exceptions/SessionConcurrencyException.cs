﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SessionConcurrencyException : Exception
    {
        public SessionConcurrencyException() : base("De sessie is reeds aangepast door iemand anders!")
        {
                
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SeminarRecognitionConcurrencyException : Exception
    {
        public SeminarRecognitionConcurrencyException() : base("De (sessie) erkenning is ondertussen aangepast door iemand anders!")
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class TypeBestaatReedsException : Exception
    {
        public TypeBestaatReedsException(): base("Het seminarietype bestaat reeds!!!")
        {

        }
    }
}

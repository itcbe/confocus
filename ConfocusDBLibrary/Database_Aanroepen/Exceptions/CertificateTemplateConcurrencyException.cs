﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class CertificateTemplateConcurrencyException : Exception
    {
        public CertificateTemplateConcurrencyException() : base("AttestTemplate is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

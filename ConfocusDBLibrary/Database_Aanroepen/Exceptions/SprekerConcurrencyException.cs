﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SprekerConcurrencyException : Exception
    {
        public SprekerConcurrencyException(): base("De spreker is reeds aangepast door iemand anders!")
        {

        }
    }
}

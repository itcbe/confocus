﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SettingConcurrencyException: Exception
    {
        public SettingConcurrencyException():base("De instellingen is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

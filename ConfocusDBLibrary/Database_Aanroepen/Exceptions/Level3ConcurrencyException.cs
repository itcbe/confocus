﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class Level3ConcurrencyException: Exception
    {
        public Level3ConcurrencyException():base("Niveau 3 is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

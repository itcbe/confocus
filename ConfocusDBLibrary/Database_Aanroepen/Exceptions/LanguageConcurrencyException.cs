﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class LanguageConcurrencyException: Exception
    {
        public LanguageConcurrencyException():base("De taal is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class RecognitionConcurrencyException: Exception
    {
        public RecognitionConcurrencyException(): base("De erkenning is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

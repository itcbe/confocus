﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class ZipcodeConcurrencyException: Exception
    {
        public ZipcodeConcurrencyException(): base("De postcode is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class RegistrationConcurrencyException : Exception
    {
        public RegistrationConcurrencyException() : base("De inschrijving is reeds gewijzigd door iemand anders!")
        {

        }
    }
}

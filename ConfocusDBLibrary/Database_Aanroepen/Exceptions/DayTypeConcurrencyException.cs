﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class DayTypeConcurrencyException: Exception
    {
        public DayTypeConcurrencyException():base("Het dagtype is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

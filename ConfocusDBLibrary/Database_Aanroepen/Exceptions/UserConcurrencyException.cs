﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class UserConcurrencyException: Exception
    {
        public UserConcurrencyException():base("De gebruiker is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

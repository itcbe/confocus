﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class Level2ConcurrencyException: Exception
    {
        public Level2ConcurrencyException():base("Niveau 2 is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class MarketingConcurrencyException: Exception
    {
        public MarketingConcurrencyException():base("Het marketingkanaal is ondertussen aangepast door iemand anders!")
        {
                
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class TargetConcurrencyException: Exception
    {
        public TargetConcurrencyException():base("De doelgroep is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

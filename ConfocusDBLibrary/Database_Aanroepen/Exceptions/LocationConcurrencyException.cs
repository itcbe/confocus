﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class LocationConcurrencyException: Exception
    {
        public LocationConcurrencyException(): base("De locatie is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

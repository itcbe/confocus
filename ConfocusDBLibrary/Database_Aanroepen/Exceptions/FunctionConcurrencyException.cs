﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class FunctionConcurrencyException : Exception
    {
        public FunctionConcurrencyException() : base("De functie is reeds aangepast door iemand anders!")
        {

        }
    }
}

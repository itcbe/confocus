﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SeminarMarketingConcurrenyException: Exception
    {
        public SeminarMarketingConcurrenyException():base("De seminarie marketing is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SessionSpeakerConcurrencyException: Exception
    {
        public SessionSpeakerConcurrencyException():base("De sessie spreker is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

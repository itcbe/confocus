﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SeminarTargetConcurrencyException: Exception
    {
        public SeminarTargetConcurrencyException():base("De seminarie doelgroep is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

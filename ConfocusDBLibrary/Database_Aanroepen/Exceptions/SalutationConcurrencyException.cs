﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class SalutationConcurrencyException : Exception
    {
        public SalutationConcurrencyException(): base("De aanspreking is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class CustomerConcurrencyException : Exception
    {
        public CustomerConcurrencyException() : base("Het contact is reeds aangepast door iemand anders!")
        {

        }
    }
}

﻿using System;

namespace ConfocusClassLibrary
{
    public class CompanyConcurrencyException: Exception
    {
        public CompanyConcurrencyException():base("Bedrijf is ondertussen aangepast door iemand anders!")
        {

        }
    }
}

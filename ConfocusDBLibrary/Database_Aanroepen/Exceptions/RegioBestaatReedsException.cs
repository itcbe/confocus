﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary.Exceptions
{
    public class RegioBestaatReedsException : Exception
    {
        public RegioBestaatReedsException() : base("De regio bestaat reeds!!")
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Seminarie
    {
        public String TitelEnDatum {
            get
            {
                string titel = Titel;
                if (Startdatum_organisatie != null)
                {
                    DateTime? firstsessiedate = new SessieServices().GetFirstSessieDateFromSeminarie(ID);
                    if (firstsessiedate != null)
                        titel += " " + ((DateTime)firstsessiedate).ToShortDateString();

                }
                return titel;
            }
        }

        public int AantalDeelnemers 
        {
            get
            {
                SessieServices sService = new SessieServices();
                return sService.GetAantalDeelnemersFromSeminarie(this.ID);             
            }
        }

        public String AantalInschrijvingen 
        {
            get
            {
                SessieServices sService = new SessieServices();
                string aantal = sService.GetAantalInschrijvingenPerSeminarie(this.ID).ToString();
                if (aantal != "")
                    return aantal;
                else
                    return "0";
            }
        }

        public int AantalSessies
        {
            get
            {
                return (from s in this.Sessie where s.Actief == true select s).Count();
            }
        }

        public string Taalnaam
        {
            get
            {
                TaalServices tservice = new TaalServices();
                Taal taal = tservice.GetTaalByID((decimal)Taal_ID);
                return taal.Naam;
            }
        }

        public string Taalcode
        {
            get
            {
                if (Taal_ID != null)
                {
                    TaalServices tservice = new TaalServices();
                    Taal taal = tservice.GetTaalByID((decimal)Taal_ID);
                    return taal.Code;
                }
                else
                    return "NL";
            }
        }


        public override bool Equals(object obj)
        {
            if (obj is Seminarie)
            {
                Seminarie deAndere = (Seminarie)obj;
                if (this.ID == deAndere.ID)
                    return true;
                else
                    return false;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

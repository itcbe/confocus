﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class SeminarieTypeService
    {
        public List<Seminarietype> GetAll()
        {
            using(var confocusEntities = new ConfocusEntities5())
            {
                return (from t in confocusEntities.Seminarietype orderby t.Type select t).ToList();
            }
        }

        public List<Seminarietype> GetActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from t in confocusEntities.Seminarietype where t.Actief == true orderby t.Type select t).ToList();
            }
        }

        public void Save(Seminarietype type)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from t in confocusEntities.Seminarietype where t.Type.ToLower() == type.Type.ToLower() select t).FirstOrDefault();
                if (gevonden == null)
                {
                    confocusEntities.Seminarietype.Add(type);
                    confocusEntities.SaveChanges();
                }
                else
                    throw new Exceptions.TypeBestaatReedsException();
            }
        }

        public void Update(Seminarietype type)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from t in confocusEntities.Seminarietype where t.ID == type.ID select t).FirstOrDefault();
                if (gevonden != null)
                {
                    gevonden.Type = type.Type;
                    gevonden.Actief = type.Actief;
                    gevonden.Gewijzigd = type.Gewijzigd;
                    gevonden.Gewijzigd_door = type.Gewijzigd_door;

                    confocusEntities.SaveChanges();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfocusClassLibrary;
using ConfocusDBLibrary.Exceptions;

namespace ConfocusDBLibrary
{
    public class SeminarieServices
    {
        public List<Seminarie> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Seminarie.Include("Sessie").Include("Doelgroep").Include("Gebruiker").Include("Sessie.Locatie")
                        where s.Actief == true
                        && s.Status != "Afgesloten"
                        orderby s.Titel
                        select s).ToList();
            }
        }

        public List<Seminarie> GetSeminariesWithActiveSessies()
        {
            using (var entities = new ConfocusEntities5())
            {
                DateTime minDate = DateTime.Now.AddDays(-21);
                List<Seminarie> seminaries = (from s in entities.Sessie.Include("Seminarie").Include("Seminarie.Sessie").Include("Seminarie.Doelgroep").Include("Seminarie.Gebruiker").Include("Seminarie.Sessie.Locatie")
                                              where s.Actief == true && s.Datum > minDate
                                              orderby s.Seminarie.Titel
                                              select s.Seminarie).ToList();
                List<Seminarie> UniekeSeminaries = new List<Seminarie>();
                foreach (Seminarie sem in seminaries)
                {
                    bool gevonden = false;
                    foreach (Seminarie un in UniekeSeminaries)
                    {
                        if (sem.ID == un.ID)
                        {
                            gevonden = true;
                            break;
                        }
                    }
                    if (!gevonden)
                    {
                        UniekeSeminaries.Add(sem);
                    }
                }

                return UniekeSeminaries;
            }
        }

        public List<Seminarie> GetAllActiveForInschrijving()
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie> seminaries = (from s in entities.Seminarie.Include("Sessie").Include("Sessie.Locatie").Include("Sessie.Gebruiker1").Include("Sessie.Inschrijvingen").Include("Sessie.Inschrijvingen.Functies")
                                              where s.Actief == true
                                              && s.Status != "Afgesloten"
                                              orderby s.Titel
                                              select s).ToList();
                return seminaries;
            }
        }


        public List<Seminarie> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Seminarie.Include("Sessie").Include("Doelgroep").Include("Gebruiker").Include("Sessie.Locatie")
                        orderby s.Titel select s).ToList();
            }
        }

        public Seminarie SaveSeminarie(Seminarie seminarie)
        {
            using (var entities = new ConfocusEntities5())
            {
                entities.Seminarie.Add(seminarie);
                entities.SaveChanges();

                return (from s in entities.Seminarie.Include("Sessie").Include("Doelgroep").Include("Gebruiker").Include("Sessie.Locatie")
                        where s.ID == seminarie.ID
                        select s).FirstOrDefault();
            }
        }

        public Seminarie SaveSeminarieChanges(Seminarie seminarie)
        {
            using (var entities = new ConfocusEntities5())
            {
                Seminarie gevonden = (from s in entities.Seminarie
                                      where s.ID == seminarie.ID
                                      select s).FirstOrDefault();
                if (gevonden != null)
                {
                    //if (!gevonden.Modified.SequenceEqual(seminarie.Modified))
                    //    throw new SeminarConcurrencyException();

                    gevonden.Titel = seminarie.Titel;
                    gevonden.Type = seminarie.Type;
                    gevonden.Weeknummer = seminarie.Weeknummer;
                    gevonden.Startdatum_organisatie = seminarie.Startdatum_organisatie;
                    gevonden.Start_marketing = seminarie.Start_marketing;
                    gevonden.Deadline_organisatie = seminarie.Deadline_organisatie;
                    gevonden.Verantwoordelijke_ID = seminarie.Verantwoordelijke_ID;
                    gevonden.Doelgroep_ID = seminarie.Doelgroep_ID;
                    gevonden.Status = seminarie.Status;
                    gevonden.Projectnummer = seminarie.Projectnummer;
                    gevonden.Taal_ID = seminarie.Taal_ID;
                    gevonden.Actief = seminarie.Actief;
                    gevonden.Gewijzigd = seminarie.Gewijzigd;
                    gevonden.Gewijzigd_door = seminarie.Gewijzigd_door;
                    entities.SaveChanges();
                }

                return seminarie;
            }
        }

        public Seminarie GetSeminarieByID(Decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {

                return (from s in entities.Seminarie.Include("Sessie").Include("Doelgroep").Include("Gebruiker")
                        where s.ID == id
                        select s).FirstOrDefault();
            }
        }

        public void ZetSessiesOpNonActief(Decimal seminarieid)
        {
            using (var entities = new ConfocusEntities5())
            {
                var sessies = (from sessie in entities.Sessie
                               where sessie.Seminarie_ID == seminarieid
                               select sessie).ToList();

                foreach (Sessie ses in sessies)
                {
                    ses.Actief = false;
                    entities.SaveChanges();
                }
            }
        }

        public void ZetSessieOpActief(decimal seminarieid)
        {
            using (var entities = new ConfocusEntities5())
            {
                var sessies = (from sessie in entities.Sessie
                               where sessie.Seminarie_ID == seminarieid
                               select sessie).ToList();

                foreach (Sessie ses in sessies)
                {
                    ses.Actief = true;
                    entities.SaveChanges();
                }
            }
        }

        public List<Seminarie> GetSeminarieBetweenDates(DateTime? van, DateTime? tot)
        {
            using (var entities = new ConfocusEntities5())
            {
                var sessies = (from s in entities.Sessie.Include("Seminarie")
                               where s.Datum >= van
                               && s.Datum <= tot
                               select s).ToList();

                List<Seminarie> seminaries = new List<Seminarie>();
                if (sessies.Count > 0)
                {
                    foreach (Sessie sessie in sessies)
                    {
                        if (!seminaries.Contains(sessie.Seminarie))
                            seminaries.Add(sessie.Seminarie);
                    }
                }

                return (from sem in seminaries orderby sem.Titel select sem).ToList();
            }
        }


        public List<Seminarie> GetSeminarieHistoriek(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie> seminaries = new List<Seminarie>();
                List<Seminarie> seminariesOpID = new List<Seminarie>();
                var query = from s in entities.Sessie.Include("Seminarie") select s;

                query = query.AsQueryable().Where(s => s.Datum >= zoekterm.StartDate);
                query = query.AsQueryable().Where(s => s.Datum <= zoekterm.EndDate);

                if (zoekterm.Status != "")
                {
                    query = query.AsQueryable().Where(s => s.Status == zoekterm.Status);
                }
                var tempsem = query.AsQueryable().Select(s => s.Seminarie).ToList();
                foreach (Seminarie seminarie in tempsem)
                {
                    if (!seminaries.Contains(seminarie))
                    {
                        seminaries.Add(seminarie);
                    }
                }
                //seminaries = query.AsQueryable().Select(s => s.Seminarie).ToList();

                if (zoekterm.SeminarieIDs.Count > 0)
                {
                    seminariesOpID = new List<Seminarie>();
                    foreach (decimal id in zoekterm.SeminarieIDs)
                    {
                        var temp = (from s in seminaries where s.ID == id select s).ToList();
                        foreach (Seminarie sem in temp)
                        {
                            if (!seminariesOpID.Contains(sem))
                            {
                                seminariesOpID.Add(sem);
                            }
                        }
                    }
                }

                if (seminariesOpID.Count > 0)
                    return seminariesOpID;
                else
                    return seminaries;
            }
        }

        public List<Seminarie> GetSeminariesByFunction(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie> seminaries = new List<Seminarie>();

                var query = (from i in entities.Inschrijvingen.Include("Sessie").Include("Sessie.Seminarie")
                             where i.Functie_ID == zoekterm.FunctieID
                             && i.Sessie.Datum >= zoekterm.StartDate
                             && i.Sessie.Datum <= zoekterm.EndDate
                             select i).ToList();

                foreach (Inschrijvingen inschrijving in query)
                {
                    if (!seminaries.Contains(inschrijving.Sessie.Seminarie))
                        seminaries.Add(inschrijving.Sessie.Seminarie);
                }

                return seminaries;
            }
        }

        public List<Seminarie> GetSeminariesBySpreker(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie> seminaries = new List<Seminarie>();

                var query = (from s in entities.Sessie_Spreker.Include("Sessie").Include("Sessie.Seminarie")
                             where s.Spreker_ID == zoekterm.SprekerID
                             && s.Sessie.Datum >= zoekterm.StartDate
                             && s.Sessie.Datum <= zoekterm.EndDate
                             select s).ToList();

                foreach (Sessie_Spreker spreker in query)
                {
                    if (!seminaries.Contains(spreker.Sessie.Seminarie))
                        seminaries.Add(spreker.Sessie.Seminarie);
                }

                return seminaries;
            }
        }

        public List<Seminarie> GetSeminariesByErkenning(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie> seminaries = new List<Seminarie>();

                var query = (from s in entities.Seminarie_Erkenningen.Include("Sessie").Include("Sessie.Seminarie").Include("Seminarie")
                             where s.Erkenning_ID == zoekterm.ErkenningID
                             && s.Sessie.Datum >= zoekterm.StartDate
                             && s.Sessie.Datum <= zoekterm.EndDate
                             select s).ToList();

                foreach (Seminarie_Erkenningen erkenning in query)
                {

                    if (!seminaries.Contains(erkenning.Seminarie))
                        seminaries.Add(erkenning.Seminarie);
                }

                return seminaries;
            }
        }

        public List<Seminarie> GetSeminarieByDoelgroep(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie> seminariesDoelgroep = new List<Seminarie>();
                List<Seminarie> seminarieOpDatum = new List<Seminarie>();
                List<Seminarie> seminaries = new List<Seminarie>();
                var query = from s in entities.Sessie.Include("Seminarie") select s;

                query = query.AsQueryable().Where(s => s.Datum >= zoekterm.StartDate);
                query = query.AsQueryable().Where(s => s.Datum <= zoekterm.EndDate);
                var tempsem = query.AsQueryable().Select(s => s.Seminarie).ToList();
                foreach (Seminarie seminarie in tempsem)
                {
                    if (!seminarieOpDatum.Contains(seminarie))
                    {
                        seminarieOpDatum.Add(seminarie);
                    }
                }

                var query2 = from d in entities.Seminarie_Doelgroep.Include("Seminarie")
                             where d.Niveau1_ID == zoekterm.Niveau1ID
                             select d;

                if (zoekterm.Niveau2ID != 0)
                    query2 = query2.AsQueryable().Where(d => d.Niveau2_ID == zoekterm.Niveau2ID);
                if (zoekterm.Niveau3ID != 0)
                    query2 = query2.AsQueryable().Where(d => d.Niveau3_ID == zoekterm.Niveau3ID);

                var temp = query2.ToList();

                foreach (Seminarie_Doelgroep doelgroep in temp)
                {

                    if (!seminariesDoelgroep.Contains(doelgroep.Seminarie))
                        seminariesDoelgroep.Add(doelgroep.Seminarie);
                }

                foreach (Seminarie datumSem in seminarieOpDatum)
                {
                    foreach (Seminarie semdoelgroep in seminariesDoelgroep)
                    {
                        if (datumSem.ID == semdoelgroep.ID && !seminaries.Contains(semdoelgroep))
                        {
                            seminaries.Add(semdoelgroep);
                        }
                    }
                }

                if (zoekterm.TaalID != null)
                    return (from s in seminaries where s.Taal_ID == zoekterm.TaalID select s).ToList();

                return seminaries;
            }
        }

        public void ZetErkenningenOpActief(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie_Erkenningen> erkenningen = (from e in entities.Seminarie_Erkenningen
                                                           where e.Seminarie_ID == iD
                                                           select e).ToList();

                foreach (Seminarie_Erkenningen item in erkenningen)
                {
                    if (item.Actief == false)
                        item.Actief = true;
                }

                entities.SaveChanges();
            }
        }

        public void ZetErkenningenOpNonActief(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie_Erkenningen> erkenningen = (from e in entities.Seminarie_Erkenningen
                                                           where e.Seminarie_ID == iD
                                                           select e).ToList();

                foreach (Seminarie_Erkenningen item in erkenningen)
                {
                    if (item.Actief == true)
                        item.Actief = false;
                }

                entities.SaveChanges();
            }
        }

        public List<Seminarie> GetSeminariesByVerantwoordelijken(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                var query = from s in entities.Sessie.Include("Seminarie") select s;

                query = query.AsQueryable().Where(s => s.Datum >= zoekterm.StartDate);
                query = query.AsQueryable().Where(s => s.Datum <= zoekterm.EndDate);

                if (zoekterm.VerantwoordelijkeSessie_ID != null)
                {
                    query = query.AsQueryable().Where(s => s.Verantwoordelijke_Sessie_ID == zoekterm.VerantwoordelijkeSessie_ID);
                }
                if (zoekterm.VerantwoordelijkeTerplaatse_ID != null)
                {
                    query = query.AsQueryable().Where(s => s.Verantwoordelijke_Terplaatse_ID == zoekterm.VerantwoordelijkeTerplaatse_ID);
                }

                var sessies = query.ToList();

                List<Seminarie> seminaries = new List<Seminarie>();
                List<Seminarie> seminariesOpID = new List<Seminarie>();

                foreach (var item in sessies)
                {
                    if (!seminaries.Contains(item.Seminarie))
                        seminaries.Add(item.Seminarie);
                }

                if (zoekterm.SeminarieIDs.Count > 0)
                {
                    foreach (decimal id in zoekterm.SeminarieIDs)
                    {
                        var temp = (from s in seminaries where s.ID == id select s).ToList();
                        foreach (Seminarie sem in temp)
                        {
                            if (!seminariesOpID.Contains(sem))
                            {
                                seminariesOpID.Add(sem);
                            }
                        }
                    }
                }

                if (zoekterm.SeminarieIDs.Count > 0)
                    return seminariesOpID;
                else
                    return seminaries;
            }
        }

        public List<Inschrijvingen> GetAllRegistrations(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Inschrijvingen> temp = new InschrijvingenService().GetInschrijvingBySeminarieID(iD);
                temp = temp.Where(i => i.Status == "Inschrijving" || i.Status == "Gast").ToList();

                return temp;
            }
        }

        public void DisableActiveErkenningOfNonactiveSeminars()
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Seminarie_Erkenningen> erkenningen = (from e in entities.Seminarie_Erkenningen.Include("Seminarie")
                                                           where e.Actief == null
                                                           select e).ToList();

                foreach (Seminarie_Erkenningen item in erkenningen)
                {
                    item.Actief = true;
                }

                entities.SaveChanges();
            }
        }
    }
}

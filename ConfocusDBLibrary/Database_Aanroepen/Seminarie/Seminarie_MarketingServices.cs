﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Seminarie_MarketingServices
    {
        public List<Seminarie_Marketing> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from sm in confocusEntities.Seminarie_Marketing select sm).ToList();
            }
        }

        public List<Seminarie_Marketing> GetMarketingBySeminarie(decimal seminarieId)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from sm in confocusEntities.Seminarie_Marketing.Include("Seminarie" ).Include("Marketingkanalen")
                        where sm.Seminarie_ID == seminarieId
                        select sm).ToList();
            }
        }

        public void SaveSeminarieMarketing(Seminarie_Marketing marketing)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenItem = (from sm in confocusEntities.Seminarie_Marketing
                                    where sm.Seminarie_ID == marketing.Seminarie_ID
                                    && sm.Marketing_ID == marketing.Marketing_ID
                                    select sm).FirstOrDefault();

                if (gevondenItem != null)
                {
                    if (!gevondenItem.Modified.SequenceEqual(marketing.Modified))
                        throw new SeminarMarketingConcurrenyException();

                    gevondenItem.Status = marketing.Status;
                    gevondenItem.Opvolgdatum = marketing.Opvolgdatum;
                }
                else
                {
                    confocusEntities.Seminarie_Marketing.Add(marketing);
                }
                confocusEntities.SaveChanges();
            }
        }

        public void UpdateSeminarieMarketing(Seminarie_Marketing marketing)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenItem = (from sm in confocusEntities.Seminarie_Marketing
                                    where sm.ID == marketing.ID
                                    select sm).FirstOrDefault();

                if (gevondenItem != null)
                {
                    gevondenItem.Seminarie_ID = marketing.Seminarie_ID;
                    gevondenItem.Marketing_ID = marketing.Marketing_ID;
                    gevondenItem.Status = marketing.Status;
                    gevondenItem.Opmerking = marketing.Opmerking;
                    gevondenItem.Opvolgdatum = marketing.Opvolgdatum;
                    confocusEntities.SaveChanges();
                }
                
            }
        }

        public bool Delete(Seminarie_Marketing kanaal)
        {
            try
            {
                using (var entities = new ConfocusEntities5())
                {
                    var teverwijderen = (from sm in entities.Seminarie_Marketing
                                         where sm.ID == kanaal.ID
                                         select sm).FirstOrDefault();
                    if (teverwijderen != null)
                    {
                        entities.Seminarie_Marketing.Remove(teverwijderen);
                        entities.SaveChanges();
                    }
                    else
                        throw new Exception(ErrorMessages.SeminarieMarketingNietGevondenFout);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

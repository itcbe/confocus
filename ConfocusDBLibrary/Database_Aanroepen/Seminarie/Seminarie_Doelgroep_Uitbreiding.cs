﻿using System;

namespace ConfocusDBLibrary
{
    public partial class Seminarie_Doelgroep
    {
        public String Naam 
        {
            get
            {
                String naam = Niveau1.Naam;
                if (Niveau2 != null)
                    naam += " : " + Niveau2.Naam;
                if (Niveau3 != null)
                    naam += " : " + Niveau3.Naam;
                return naam;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Seminarie_Doelgroep)
            {
                Seminarie_Doelgroep deAndere = (Seminarie_Doelgroep)obj;
                if (deAndere.ID == ID)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

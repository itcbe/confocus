﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Seminarie_ErkenningenService
    {
        public List<Seminarie_Erkenningen> GetActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from e in entities.Seminarie_Erkenningen
                        where e.Actief == true
                        select e).ToList();
            }
        }

        public List<Seminarie_Erkenningen> GetBySeminarieID(Decimal semID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from e in entities.Seminarie_Erkenningen.Include("Seminarie").Include("Erkenning").Include("Sessie")
                        where e.Seminarie_ID == semID
                        && e.Actief == true
                        select e).ToList();
            }
        }

        public Seminarie_Erkenningen GetByID(Decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from e in entities.Seminarie_Erkenningen.Include("Seminarie").Include("Erkenning").Include("Sessie")
                        where e.ID == id
                        select e).FirstOrDefault();
            }
        }

        public Seminarie_Erkenningen SaveSeminarie_Erkenningen(Seminarie_Erkenningen erkenning)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from e in entities.Seminarie_Erkenningen
                                where e.Seminarie_ID == erkenning.Seminarie_ID
                                && e.Erkenning_ID == erkenning.Erkenning_ID
                                && (e.Sessie_ID == erkenning.Sessie_ID)
                                && e.Actief == true
                                select e).FirstOrDefault();

                if (gevonden == null)
                {
                    entities.Seminarie_Erkenningen.Add(erkenning);
                    entities.SaveChanges();
                    return erkenning;
                }
                else
                    throw new Exception("Erkenning is reeds aan het seminarie toegevoegd!");
            }
        }

        public void UpdateErkenning(Seminarie_Erkenningen erkenning)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from e in entities.Seminarie_Erkenningen
                                where e.ID == erkenning.ID
                                select e).FirstOrDefault();

                if (gevonden != null)
                {
                    //if (!gevonden.Modified.SequenceEqual(erkenning.Modified))
                    //    throw new SeminarRecognitionConcurrencyException();

                    gevonden.Nummer = erkenning.Nummer;
                    gevonden.Datum = erkenning.Datum;
                    gevonden.Status = erkenning.Status;
                    gevonden.Aantal = erkenning.Aantal;
                    gevonden.Type = erkenning.Type;
                    gevonden.Opmerking = erkenning.Opmerking;
                    gevonden.Actief = erkenning.Actief;
                    gevonden.Sessie_ID = erkenning.Sessie_ID;
                    gevonden.Gewijzigd = DateTime.Now;
                    gevonden.Gewijzigd_door = erkenning.Aangemaakt_door;
                    gevonden.TAantal = erkenning.TAantal;
                    entities.SaveChanges();
                }
            }
        }


        public void Remove(Seminarie_Erkenningen erkenning)
        {
            using (var entities = new ConfocusEntities5())
            {
                var teverwijderen = (from e in entities.Seminarie_Erkenningen
                                     where e.ID == erkenning.ID
                                     select e).FirstOrDefault();

                entities.Seminarie_Erkenningen.Remove(teverwijderen);
                entities.SaveChanges();
            }
        }

        public List<Seminarie_Erkenningen> GetBySessieID(decimal sessieId)
        {
            using (var entities = new ConfocusEntities5())
            {

                return (from e in entities.Seminarie_Erkenningen.Include("Erkenning").Include("Erkenning.AttestType1")
                        where e.Sessie_ID == sessieId
                        && e.Actief == true
                        select e).ToList();
            }
        }

        public List<Seminarie_Erkenningen> GetByErkenningID(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from se in entities.Seminarie_Erkenningen.Include("Seminarie").Include("Erkenning")
                        where se.Erkenning_ID == iD
                        select se).ToList();
            }
        }

        public List<Seminarie_Erkenningen> GetByErkenningIDs(List<decimal> erkenningIDs)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from se in entities.Seminarie_Erkenningen.Include("Seminarie").Include("Erkenning").Include("Sessie")
                        where erkenningIDs.Contains(se.Erkenning_ID)
                        && se.Actief == true
                        select se).ToList();
            }
        }

        public List<Seminarie_Erkenningen> GetAllErkenningen()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from se in entities.Seminarie_Erkenningen.Include("Seminarie").Include("Erkenning").Include("Sessie")
                        where se.Actief == true
                        select se).ToList();
            }
        }

        public List<Sessie> GetAllErkenningenSessies()
        {
            using (var entities = new ConfocusEntities5())
            {
                DateTime date = DateTime.Now.AddDays(-21);
                return (from se in entities.Seminarie_Erkenningen.Include("Seminarie").Include("Erkenning").Include("Sessie")
                        where se.Actief == true
                        && se.Sessie != null 
                        && se.Sessie.Datum > date
                        && se.Seminarie.Status != "Afgesloten"
                        && se.Seminarie.Actief == true
                        select se.Sessie).ToList();
            }
        }
    }
}

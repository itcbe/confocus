﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Seminarie_DoelgroepServices
    {
        public List<Seminarie_Doelgroep> GetDoelgroepenBySeminarieID(decimal seminarieID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from sd in entities.Seminarie_Doelgroep.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Seminarie")
                        where sd.Seminarie_ID == seminarieID
                        && sd.Actief == true
                        select sd).ToList();
            }
        }
        public Seminarie_Doelgroep SaveSeminarieDoelgroep(Seminarie_Doelgroep SM)
        {
            using (var entities = new ConfocusEntities5())
            {
                var query = from sd in entities.Seminarie_Doelgroep
                            where sd.Seminarie_ID == SM.Seminarie_ID
                            && sd.Niveau1_ID == SM.Niveau1_ID
                            select sd;
                query = query.AsQueryable().Where(sd => sd.Niveau1_ID == SM.Niveau1_ID);
                if (SM.Niveau2_ID != null)
                    query = query.AsQueryable().Where(sd => sd.Niveau2_ID == SM.Niveau2_ID);
                if (SM.Niveau3_ID != null)
                    query = query.AsQueryable().Where(sd => sd.Niveau3_ID == SM.Niveau3_ID);

                var gevonden = query.FirstOrDefault();

                if (gevonden == null)
                {
                    entities.Seminarie_Doelgroep.Add(SM);
                    entities.SaveChanges();
                }
                else
                    throw new Exception(ErrorMessages.DoelgroepBestaatReeds);

                return (from sd in entities.Seminarie_Doelgroep.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Seminarie")
                        where sd.ID == SM.ID
                        select sd).FirstOrDefault();
            }
        }

        public void SaveSeminarieDoelgroepChanges(Seminarie_Doelgroep SM)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from sd in entities.Seminarie_Doelgroep
                                where sd.ID == SM.ID
                                select sd).FirstOrDefault();

                if (gevonden != null)
                {
                    //if (!gevonden.Modified.SequenceEqual(SM.Modified))
                    //    throw new SeminarTargetConcurrencyException();

                    gevonden.Niveau1_ID = SM.Niveau1_ID;
                    gevonden.Niveau2_ID = SM.Niveau2_ID;
                    gevonden.Niveau3_ID = SM.Niveau3_ID;
                    gevonden.Opmerking = SM.Opmerking;
                    gevonden.Actief = SM.Actief;
                    gevonden.Gewijzigd_door = SM.Gewijzigd_door;
                    gevonden.Gewijzigd = SM.Gewijzigd;
                    entities.SaveChanges();
                }
                else
                    throw new Exception(ErrorMessages.DoelgroepNietGevonden);
            }
        }

        public bool Delete(Seminarie_Doelgroep doelgroep)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from sd in entities.Seminarie_Doelgroep
                                where sd.ID == doelgroep.ID
                                select sd).FirstOrDefault();

                if (gevonden != null)
                {
                    entities.Seminarie_Doelgroep.Remove(gevonden);
                    entities.SaveChanges();
                    return true;
                }
                else
                    throw new Exception(ErrorMessages.DoelgroepNietGevonden);

            }
        }
    }
}

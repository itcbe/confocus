﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Inschrijving
    {
        public String SessieNaam {
            get
            {
                using (var entities = new ConfocusEntities5())
                {
                    var sessie = (from s in entities.Sessie.Include("Locatie")
                                  where s.ID == (decimal)this.Sessie_ID
                                  select s).FirstOrDefault();
                    if (sessie != null)
                        return sessie.Naam + " : " + (sessie.Datum == null ? "" : ((DateTime)sessie.Datum).ToShortDateString()) + " : " + sessie.Locatie.Naam;
                    else
                        return "";
                }
            }
        }

        public String SeminarieNaam 
        { 
            get
            {
                String naam = "";
                using (var entities = new ConfocusEntities5())
                {
                    var sessie = (from s in entities.Sessie.Include("Seminarie")
                                  where s.ID == (decimal)this.Sessie_ID
                                  select s).FirstOrDefault();

                    var sessies = (from ses in entities.Sessie.Include("Seminarie")
                                   where ses.Seminarie_ID == sessie.Seminarie_ID
                                   select ses).ToList();

                    if (sessie != null)
                        naam =  sessie.Seminarie.Titel + " ( " + sessies.Count + " sessie(s) )";
                }
                return naam;
            } 
        }
    }
}

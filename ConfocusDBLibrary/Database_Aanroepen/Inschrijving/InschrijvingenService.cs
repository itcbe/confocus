﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfocusClassLibrary;
using ConfocusDBLibrary.Exceptions;

namespace ConfocusDBLibrary
{
    public class InschrijvingenService
    {
        public List<Inschrijvingen> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Actief == true
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetInschrijvingenVanGisteren(DateTime date)
        {
            using (var entities = new ConfocusEntities5())
            {
                DateTime smorgens = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
                DateTime savonds = new DateTime(date.Year, date.Month, date.Day, 23, 59, 50);
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact") where i.Aangemaakt > smorgens && i.Aangemaakt < savonds select i).ToList();
            }
        }

        internal int GetTotaalInschrijvingenFromSessie(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen where i.Sessie_ID == iD && i.Actief == true && (i.Status == "Inschrijving" || i.Status == "Gast") select i).Count();
            }
        }

        public Inschrijvingen GetInschrijvingBySessieAndName(decimal sessie, string voornaam, string naam)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Sessie_ID == sessie
                        && i.Functies.Contact.Voornaam.ToLower() == voornaam.ToLower()
                        && i.Functies.Contact.Achternaam.ToLower() == naam.ToLower()
                        select i).FirstOrDefault();
            }
        }

        public List<Inschrijvingen> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        select i).ToList();
            }
        }

        public Inschrijvingen GetInschrijvingByID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1").Include("FacturatieBedrijf").Include("Functies.Bedrijf")
                        where i.ID == id
                        select i).FirstOrDefault();
            }
        }

        public Inschrijvingen SaveInschrijving(Inschrijvingen inschrijving)
        {
            using (var entities = new ConfocusEntities5())
            {
                Inschrijvingen gevonden = (from i in entities.Inschrijvingen
                                           where i.Sessie_ID == inschrijving.Sessie_ID
                                           && i.Functie_ID == inschrijving.Functie_ID
                                           && i.Actief == true
                                           select i).FirstOrDefault();
                if (gevonden == null)
                {
                    entities.Inschrijvingen.Add(inschrijving);
                    try
                    {
                        entities.SaveChanges();
                        return inschrijving;
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                    
                }
                else
                    return gevonden;
            }
        }

        public Inschrijvingen SaveInschrijvingCHanges(Inschrijvingen inschrijving)
        {
            using (var entities = new ConfocusEntities5())
            {
                Inschrijvingen gevonden = (from i in entities.Inschrijvingen
                                           where i.ID == inschrijving.ID
                                           select i).FirstOrDefault();
                if (gevonden != null)
                {
                    //if (!gevonden.Modified.SequenceEqual(inschrijving.Modified) && !gevonden.Gewijzigd_door.Equals(inschrijving.Gewijzigd_door))
                    //    throw new RegistrationConcurrencyException();
                    gevonden.Functie_ID = inschrijving.Functie_ID;
                    gevonden.Sessie_ID = inschrijving.Sessie_ID;
                    gevonden.Datum_inschrijving = inschrijving.Datum_inschrijving;
                    gevonden.Via = inschrijving.Via;
                    gevonden.Status = inschrijving.Status;
                    gevonden.Notitie = inschrijving.Notitie;
                    gevonden.CC_Email = inschrijving.CC_Email;
                    gevonden.Gefactureerd = inschrijving.Gefactureerd;
                    gevonden.Factuurnummer = inschrijving.Factuurnummer;
                    gevonden.Notitie_factuur = inschrijving.Notitie_factuur;
                    gevonden.Facturatie_op_ID = inschrijving.Facturatie_op_ID;
                    gevonden.Dagtype = inschrijving.Dagtype;
                    gevonden.Aantal = inschrijving.Aantal;
                    gevonden.TAantal = inschrijving.TAantal;
                    gevonden.Aanwezig = inschrijving.Aanwezig;
                    gevonden.Attest_opmaken = inschrijving.Attest_opmaken;
                    gevonden.Attesttype = inschrijving.Attesttype;
                    gevonden.Mail_ontvangen = inschrijving.Mail_ontvangen;
                    gevonden.Evaluatie_ontvangen = inschrijving.Evaluatie_ontvangen;
                    gevonden.Reminder_Verstuurd = inschrijving.Reminder_Verstuurd;
                    gevonden.Annulatie_Verstuurd = inschrijving.Annulatie_Verstuurd;
                    gevonden.Verplaatsing_Verstuurd = inschrijving.Verplaatsing_Verstuurd;
                    gevonden.Attest_Verstuurd = inschrijving.Attest_Verstuurd;
                    if (inschrijving.VerantwoordelijkeOpleiding != null)
                        gevonden.VerantwoordelijkeOpleiding = inschrijving.VerantwoordelijkeOpleiding;
                    if (inschrijving.VerantwoordelijkePersoneel != null)
                        gevonden.VerantwoordelijkePersoneel = inschrijving.VerantwoordelijkePersoneel;
                    gevonden.Actief = inschrijving.Actief;
                    gevonden.Gewijzigd = inschrijving.Gewijzigd;
                    gevonden.Gewijzigd_door = inschrijving.Gewijzigd_door;
                    try
                    {
                        entities.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                }
                return gevonden;
            }
        }

        public void SetAttestverstuurdOpJa(Inschrijvingen inschrijving)
        {
            using (var entities = new ConfocusEntities5())
            {
                Inschrijvingen gevonden = (from i in entities.Inschrijvingen where i.ID == inschrijving.ID select i).FirstOrDefault();
                if (gevonden != null)
                {
                    gevonden.Attest_Verstuurd = true;
                    gevonden.Gewijzigd = inschrijving.Gewijzigd;
                    gevonden.Gewijzigd_door = inschrijving.Gewijzigd_door;
                    try
                    {
                        entities.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                }
            }
        }

        internal int GetAantalIngeschrevenSessies(Inschrijvingen inschrijving)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                Inschrijvingen dezeInschrijving = GetInschrijvingByID(inschrijving.ID);
                aantal = (from i in entities.Inschrijvingen
                          where i.Sessie.Seminarie_ID == inschrijving.Sessie.Seminarie_ID
                          && i.Functies.Contact_ID == inschrijving.Functies.Contact_ID
                          && (i.Status == "Inschrijving" || i.Status == "Gast")
                          && !i.Sessie.Naam.ToLower().Contains("lunch")
                          select i).Count();
                return aantal;
            }
        }

        public List<Inschrijvingen> GetInschrijvingBySeminarieID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Sessie.Locatie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Sessie.Seminarie_ID == id
                        && i.Actief == true
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetInschrijvingBySessieID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Sessie.Locatie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Sessie_ID == id
                        && i.Actief == true
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetLimitedInschrijvingenFromSessie(decimal sessieID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Functies").Include("Functies.Contact")
                    where i.Sessie_ID == sessieID
                    && i.Actief == true
                    select i).ToList();
            }
        }

        public List<Inschrijvingen> GetBlancoInschrijvingenByIDAndStatus(decimal id, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == id
                            && i.Actief == true
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();
                else
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == id && i.Status == status
                            && i.Actief == true
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();


            }
        }

        public int GetDeelnemersAantalBySessie(decimal sessieID)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var inschrijvingen = (from i in entities.Inschrijvingen
                                      where i.Sessie_ID == sessieID
                                      && i.Actief == true
                                      select i).ToList();

                foreach (Inschrijvingen inschrijving in inschrijvingen)
                {
                    var deelnemerlijst = (from d in entities.Deelnemer
                                          where d.Inschrijving_ID == inschrijving.ID
                                          && d.Actief == true
                                          select d).ToList();

                    aantal += deelnemerlijst.Count;
                }
                return aantal;
            }
        }

        public List<Inschrijvingen> GetInschrijvingenByBedrijf(decimal? bedrijfid)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (bedrijfid != null)
                {
                    var inschrijvingen = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                                          where i.Facturatie_op_ID == bedrijfid
                                          && i.Actief == true
                                          select i).ToList();
                    return inschrijvingen;
                }
                else
                    return new List<Inschrijvingen>();
            }
        }

        public List<Inschrijvingen> GetInschrijvingByFunctionID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Functies")
                        where i.Functie_ID == (decimal)id
                        && i.Actief == true
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetInschrijvingenOpFunction(decimal? id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Functies")
                        where i.Functies.Contact_ID == (decimal)id
                        && i.Actief == true
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetAanwezigeInschrijvingenBySessieID(decimal id, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Sessie_ID == id
                        && i.Aanwezig == true
                        orderby i.Functies.Contact.Achternaam
                        select i).ToList();
                else
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == id
                            && i.Aanwezig == true
                            && i.Status == status
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

            }
        }

        public List<Inschrijvingen> GetReminderInschrijvingenBySessieID(decimal id, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == id
                            && (i.Status == "Inschrijving" || i.Status == "Gast")
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();
                else
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == id
                            && (i.Status == status)
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

            }
        }

        public int GetAantalGastenFromSessie(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = (from i in entities.Inschrijvingen
                              where i.Sessie_ID == id
                              && i.Status == "Gast"
                              && i.Actief == true
                              select i).Count();

                return aantal;
            }
        }

        public int GetAantalEffectieveInschrijvingenFromSessie(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = (from i in entities.Inschrijvingen
                              where i.Sessie_ID == id
                              && (i.Status == "Inschrijving" || i.Status == "Gast")
                              && i.Actief == true
                              select i).Count();
                return aantal;
            }
        }

        public int GetAantalEffectieveInschrijvingenAndInteresseFromSessie(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = (from i in entities.Inschrijvingen
                              where i.Sessie_ID == id
                              && (i.Status == "Inschrijving" || i.Status == "Gast" || i.Status == "interesse")
                              && i.Actief == true
                              select i).Count();
                return aantal;
            }

        }


        public int GetAantalInschrijvingenFromSessie(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = (from i in entities.Inschrijvingen
                              where i.Sessie_ID == id
                              && (i.Status == "Inschrijving"
                              || i.Status == "Gast")
                              && i.Actief == true
                              select i).Count();
                return aantal;
            }
        }

        public List<Sessie> GetSessiesByFunctie(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {

                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies")
                        where i.Functies.Contact_ID == iD
                        && i.Sessie.Datum >= DateTime.Today
                        && i.Actief == true
                        orderby i.Sessie.Datum
                        select i.Sessie).ToList();
            }
        }

        public List<Inschrijvingen> GetAfwezigeInschrijvingenBySessieID(decimal iD, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == iD
                            && i.Aanwezig == false
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();
                else
                    return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie_ID == iD
                            && i.Aanwezig == false
                            && i.Status == status
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();


            }
        }

        public List<Inschrijvingen> GetInschrijvingenBySearchTerms(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                var query = from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Functies.Contact_ID == zoekterm.ContactID
                            && i.Sessie.Datum <= zoekterm.EndDate
                            select i;
                if (zoekterm.StartDate != null)
                    query = query.AsQueryable().Where(i => i.Sessie.Datum >= zoekterm.StartDate);
                if (zoekterm.EndDate != null)
                    query = query.AsQueryable().Where(i => i.Sessie.Datum <= zoekterm.EndDate);

                var result = query.ToList();
                List<Inschrijvingen> seminarieInschrijvingen = new List<Inschrijvingen>();
                if (zoekterm.SeminarieIDs.Count > 0)
                {
                    foreach (var id in zoekterm.SeminarieIDs)
                    {
                        foreach (var item in result)
                        {
                            if (item.Sessie.Seminarie_ID == id)
                            {
                                if (!seminarieInschrijvingen.Contains(item))
                                    seminarieInschrijvingen.Add(item);
                            }
                        }
                    }
                }
                if (zoekterm.SeminarieIDs.Count > 0)
                    return seminarieInschrijvingen;
                else
                    return result;
            }
        }

        public List<Inschrijvingen> GetBlancoInschrijvingenBySeminarieIDAndStatus(decimal iD, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                { 
                    var alledeelnemers =  (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }
                else
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && i.Status == status
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();
                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }

            }
        }

        public List<Inschrijvingen> GetAanwezigeInschrijvingenBySeminarieID(decimal iD, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && i.Aanwezig == true
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }
                else
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && i.Aanwezig == true
                            && i.Status == status
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }
            }
        }

        public void UpdateTaantal()
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Inschrijvingen> inschrijvingen = (from i in entities.Inschrijvingen where i.Aantal != null select i).ToList();

                foreach (Inschrijvingen item in inschrijvingen)
                {
                    decimal aant = decimal.Parse(item.Aantal.ToString());
                    aant = aant / 100;
                    string aantal = aant.ToString();
                    //aantal = aantal.Replace(',', '.');
                    item.TAantal = aantal;
                    SaveInschrijvingCHanges(item);
                }
            }
        }


        public List<Inschrijvingen> GetAfwezigeInschrijvingenBySeminarieID(decimal iD, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && i.Aanwezig == false
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }
                else
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && i.Aanwezig == false
                            && i.Status == status
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;

                }
            }
        }

        public List<Inschrijvingen> GetReminderInschrijvingenBySeminarieID(decimal iD, string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (status == "Alle")
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && (i.Status == "Inschrijving" || i.Status == "Gast")
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }
                else
                {
                    var alledeelnemers = (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                            where i.Sessie.Seminarie_ID == iD
                            && i.Status == status
                            orderby i.Functies.Contact.Achternaam
                            select i).ToList();

                    List<Inschrijvingen> unieke = new List<Inschrijvingen>();
                    foreach (Inschrijvingen item in alledeelnemers)
                    {
                        bool gevonden = false;
                        foreach (Inschrijvingen uniek in unieke)
                        {
                            if (item.Functies.Contact_ID == uniek.Functies.Contact_ID)
                            {
                                gevonden = true;
                            }
                        }

                        if (!gevonden)
                            unieke.Add(item);
                    }

                    return unieke;
                }
            }
        }

        public List<Inschrijvingen> GetInschrijvingBySeminarieIDAndFunctieID(decimal seminarie_ID, decimal? contact_ID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies")
                        where i.Sessie.Seminarie_ID == seminarie_ID
                        && i.Functies.Contact_ID == contact_ID
                        && (i.Status == "Inschrijving" || i.Status == "Gast")
                        orderby i.Sessie.Beginuur
                        select i).ToList();
            }
        }

        public Inschrijvingen GetInschrijvingByFunctieIDAndSessieID(decimal functieid, decimal sessieid)
        {
            using (var entities = new ConfocusEntities5())
            {
                Inschrijvingen inschrijving = (from i in entities.Inschrijvingen where i.Functie_ID == functieid && i.Sessie_ID == sessieid select i).FirstOrDefault();
                return inschrijving;
            }
        }

        public List<Inschrijvingen> GetInschrijvingenPerStatus(string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Sessie.Seminarie") where i.Status == status && i.Actief == true select i).ToList();
            }
        }

        public List<Inschrijvingen> GetInschrijvingenByContact(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies")
                        where i.Functies.Contact_ID == iD
                        && i.Sessie.Datum >= DateTime.Today
                        && i.Actief == true
                        orderby i.Sessie.Datum
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetSessiesByFunctieAndSemId(decimal functieid, decimal? seminarieid, DateTime datum)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Functies.ID == functieid
                        && i.Sessie.Seminarie_ID == seminarieid
                        && i.Sessie.Datum == datum
                        && (i.Status == "Inschrijving" || i.Status == "Gast")
                        && i.Actief == true
                        orderby i.Sessie.Beginuur
                        select i).ToList();
            }
        }

        public List<Inschrijvingen> GetSessiesByContactIDAndSeminarieID(decimal contact_ID, decimal seminarie_ID, DateTime datum)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijvingen.Include("Sessie").Include("Functies").Include("Functies.Bedrijf").Include("Functies.Contact").Include("Functies.AttestType1")
                        where i.Functies.Contact_ID == contact_ID
                        && i.Sessie.Seminarie_ID == seminarie_ID
                        && i.Sessie.Datum == datum
                        && (i.Status == "Inschrijving" || i.Status == "Gast")
                        && i.Actief == true
                        orderby i.Sessie.Beginuur
                        select i).ToList();
            }
        }

    }
}

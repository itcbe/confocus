﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class InschrijvingService
    {
        public List<Inschrijving> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijving.Include("Bedrijf").Include("Sessie")
                        where i.Actief == true
                        select i).ToList();
            }
        }

        public List<Inschrijving> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijving.Include("Bedrijf").Include("Sessie")
                        select i).ToList();
            }
        }

        public Inschrijving GetInschrijvingByID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijving.Include("Bedrijf").Include("Sessie")
                        where i.ID == id
                        select i).FirstOrDefault();
            }
        }

        public Inschrijving SaveInschrijving(Inschrijving inschrijving)
        {
            using (var entities = new ConfocusEntities5())
            {
                Inschrijving gevonden = (from i in entities.Inschrijving
                                         where i.Sessie_ID == inschrijving.Sessie_ID
                                         && i.Bedrijf_ID == inschrijving.Bedrijf_ID
                                         select i).FirstOrDefault();
                if (gevonden == null)
                {
                    entities.Inschrijving.Add(inschrijving);
                    entities.SaveChanges();
                    return inschrijving;
                }
                else
                    return gevonden;
            }
        }

        public Inschrijving SaveInschrijvingCHanges(Inschrijving inschrijving)
        {
            using (var entities = new ConfocusEntities5())
            {
                Inschrijving gevonden = (from i in entities.Inschrijving
                                         where i.ID == inschrijving.ID
                                         select i).FirstOrDefault();
                if (gevonden != null)
                {
                    gevonden.Bedrijf_ID = inschrijving.Bedrijf_ID;
                    gevonden.Sessie_ID = inschrijving.Sessie_ID;
                    gevonden.Datum_inschrijving = inschrijving.Datum_inschrijving;
                    gevonden.Via = inschrijving.Via;
                    gevonden.Status = inschrijving.Status;
                    gevonden.Notitie = inschrijving.Notitie;
                    gevonden.Gefactureerd = inschrijving.Gefactureerd;
                    gevonden.Factuurnummer = inschrijving.Factuurnummer;
                    gevonden.Notitie_factuur = inschrijving.Notitie_factuur;
                    gevonden.Actief = inschrijving.Actief;
                    gevonden.Gewijzigd = inschrijving.Gewijzigd;
                    gevonden.Gewijzigd_door = inschrijving.Gewijzigd_door;
                    entities.SaveChanges();
                }
                return gevonden;
            }
        }

        public List<Inschrijving> GetInschrijvingBySeminarieID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from i in entities.Inschrijving.Include("Bedrijf").Include("Sessie")
                        where i.Sessie.Seminarie_ID == id
                        select i).ToList();
            }
        }

        public List<Inschrijving> GetInschrijvingBySessieID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
              {
                return (from i in entities.Inschrijving.Include("Bedrijf").Include("Sessie")
                        where i.Sessie_ID == id
                        select i).ToList();
            }
        }

        public int GetDeelnemersAantalBySessie(decimal sessieID)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var inschrijvingen = (from i in entities.Inschrijving
                                      where i.Sessie_ID == sessieID
                                      && i.Actief == true
                                      select i).ToList();

                foreach (Inschrijving inschrijving in inschrijvingen)
                {
                    var deelnemerlijst = (from d in entities.Deelnemer
                                          where d.Inschrijving_ID == inschrijving.ID
                                          && d.Actief == true
                                          select d).ToList();

                    aantal += deelnemerlijst.Count;
                }
                return aantal;
            }
        }

        public List<Inschrijving> GetInschrijvingenByBedrijf(decimal? bedrijfid)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (bedrijfid != null)
                {
                    var inschrijvingen = (from i in entities.Inschrijving.Include("Sessie").Include("Bedrijf")
                                          where i.Bedrijf_ID == bedrijfid
                                          && i.Actief == true
                                          select i).ToList();
                    return inschrijvingen;
                }
                else
                    return new List<Inschrijving>();
            }
        }

    }
}

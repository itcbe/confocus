﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Inschrijvingen
    {
        public String SessieNaam
        {
            get
            {
                using (var entities = new ConfocusEntities5())
                {
                    var sessie = (from s in entities.Sessie.Include("Locatie")
                                  where s.ID == (decimal)this.Sessie_ID
                                  select s).FirstOrDefault();
                    if (sessie != null)
                        return sessie.Naam + " : " + (sessie.Datum == null ? "" : ((DateTime)sessie.Datum).ToShortDateString()) + " : " + sessie.Locatie.Naam;
                    else
                        return "";
                }
            }
        }

        public String SeminarieNaam
        {
            get
            {
                String naam = "";
                using (var entities = new ConfocusEntities5())
                {
                    var sessie = (from s in entities.Sessie.Include("Seminarie")
                                  where s.ID == (decimal)this.Sessie_ID
                                  select s).FirstOrDefault();

                    var sessies = (from ses in entities.Sessie.Include("Seminarie")
                                   where ses.Seminarie_ID == sessie.Seminarie_ID
                                   select ses).ToList();

                    if (sessie != null)
                        naam = sessie.Seminarie.Titel + " ( " + sessies.Count + " sessie(s) )";
                }
                return naam;
            }
        }

        public string SeminarieTitel
        {
            get
            {
                String naam = "";
                using (var entities = new ConfocusEntities5())
                {
                    var sessie = (from s in entities.Sessie.Include("Seminarie")
                                  where s.ID == (decimal)this.Sessie_ID
                                  select s).FirstOrDefault();

                    var sessies = (from ses in entities.Sessie.Include("Seminarie")
                                   where ses.Seminarie_ID == sessie.Seminarie_ID
                                   select ses).ToList();

                    if (sessie != null)
                        naam = sessie.Seminarie.Titel;
                }
                return naam;
            }
        }

        public string boolAanwezig
        {
            get
            {
                return Aanwezig == true ? "Ja" : "Nee";
            }
        }

        public string boolGefactureerd
        {
            get
            {
                return Gefactureerd == true ? "Ja" : "Nee";
            }
        }

        public string boolMailOntvangen
        {
            get
            {
                return Mail_ontvangen == true ? "Ja" : "Nee";
            }
        }

        public string boolEvalutieOntvangen
        {
            get
            {
                return Evaluatie_ontvangen == true ? "Ja" : "Nee";
            }
        }

        public string boolReminderVerstuurd
        {
            get
            {
                return Reminder_Verstuurd == true ? "Ja" : "Nee";
            }
        }

        public string boolAnnulatieVerstuurd
        {
            get
            {
                return Annulatie_Verstuurd == true ? "Ja" : "Nee";
            }
        }

        public string boolVerplaatsingVerstuurd
        {
            get
            {
                return Verplaatsing_Verstuurd == true ? "Ja" : "Nee";
            }
        }

        public string boolAttestVerstuurd
        {
            get
            {
                return Attest_Verstuurd == true ? "Ja" : "Nee";
            }
        }

        public string boolLocatiewijzigingVerstuurd
        {
            get
            {
                return Locatiewijziging_Verstuurd == true ? "Ja" : "Nee";
            }
        }

        public string boolInhouseVerstuurd
        {
            get
            {
                return Inhouse_Verstuurd == true ? "Ja" : "Nee";
            }
        }

        public string FacturatieOpNaam
        {
            get
            {
                if (Facturatie_op_ID != null)
                {
                    BedrijfServices bService = new BedrijfServices();
                    Bedrijf bedrijf = bService.GetBedrijfByID((decimal)Facturatie_op_ID);
                    return bedrijf.Naam;
                }
                else
                    return "";
            }
        }

        public decimal? DecimalAantal
        {
            get
            {
                if (Aantal == null)
                    return null;
                else
                    return (decimal)Aantal / 100;
            }
            set
            {
                if (value != null)
                    Aantal = (int)Math.Round((decimal)value * 100);
            }
        }

        public int AantalGevolgdeSessies
        {
            get
            {
                InschrijvingenService iService = new InschrijvingenService();
                int aantal = iService.GetAantalIngeschrevenSessies(this);
                return aantal;
            }
        }
    }
}

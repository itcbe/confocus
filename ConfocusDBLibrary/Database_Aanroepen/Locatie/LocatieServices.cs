﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class LocatieServices
    {
        public List<Locatie> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from l in confocusEntities.Locatie
                        where l.Actief == true
                        orderby l.Naam
                        select l).ToList();
            }
        }

        public Locatie SaveLocatie(Locatie locatie)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Locatie.Add(locatie);
                confocusEntities.SaveChanges();
                return locatie;
            }
        }

        public Locatie SaveLocatieChanges(Locatie locatie)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Locatie gevondenLocatie = (from l in confocusEntities.Locatie
                                           where l.ID == locatie.ID
                                           select l).FirstOrDefault();
                if (gevondenLocatie != null)
                {
                    //if (!gevondenLocatie.Modified.SequenceEqual(locatie.Modified))
                    //    throw new LocationConcurrencyException();

                    gevondenLocatie.Naam = locatie.Naam;
                    gevondenLocatie.Adres = locatie.Adres;
                    gevondenLocatie.Postcode = locatie.Postcode;
                    gevondenLocatie.Plaats = locatie.Plaats;
                    gevondenLocatie.Regio = locatie.Regio;
                    gevondenLocatie.Contact = locatie.Contact;
                    gevondenLocatie.Telefoon = locatie.Telefoon;
                    gevondenLocatie.Beamer = locatie.Beamer;
                    gevondenLocatie.Laptop = locatie.Laptop;
                    gevondenLocatie.Gewijzigd = locatie.Gewijzigd;
                    gevondenLocatie.Gewijzigd_door = locatie.Gewijzigd_door;
                    gevondenLocatie.Actief = locatie.Actief;
                    confocusEntities.SaveChanges();
                    return gevondenLocatie;
                }
                else
                    return null;
            }
        }

        public Locatie GetLocatieByID(Decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from l in confocusEntities.Locatie
                        where l.ID == id
                        select l).FirstOrDefault();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Locatie
    {
        public string FullAddress
        {
            get
            {
                return Adres + " - " +  Postcode + " " + Plaats;
            }
        }
    }
}

﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class AttestTemplate_Service
    {
        public List<AttestTemplate> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.AttestTemplate where a.Actief == true select a).ToList();
            }
        }

        public void Add(AttestTemplate template)
        {
            using (var entities  = new ConfocusEntities5())
            {
                var gevonden = (from t in entities.AttestTemplate
                                where t.Uri.ToLower() == template.Uri.ToLower()
                                || t.Naam.ToLower() == template.Naam.ToLower()
                                select t).FirstOrDefault();
                if (gevonden != null)
                    throw new Exception(ErrorMessages.AttestTemplateBestaat);

                
                try
                {
                    entities.AttestTemplate.Add(template);
                    entities.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    throw new Exception(ErrorMessages.DataBaseBezet);
                }
                catch (Exception ex)
                {
                    throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                }
            }
        }

        public void Update(AttestTemplate template)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from t in entities.AttestTemplate
                                where t.ID == template.ID
                                select t).FirstOrDefault();

                if (gevonden == null)
                    throw new Exception(ErrorMessages.AttestTemplateNotFound);
                //if (!gevonden.Modified.SequenceEqual(template.Modified))
                //    throw new CertificateTemplateConcurrencyException();

                gevonden.Naam = template.Naam;
                gevonden.Uri = template.Uri;
                gevonden.Gewijzigd = template.Gewijzigd;
                gevonden.Gewijzigd_door = template.Gewijzigd_door;
                gevonden.Actief = template.Actief;

                entities.SaveChanges();
                try
                {
                    entities.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    throw new Exception(ErrorMessages.DataBaseBezet);
                }
                catch (Exception ex)
                {
                    throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                }
            }
        }

        public List<AttestTemplate> GetByName(string template)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.AttestTemplate where a.Naam.ToLower().Contains(template.ToLower()) select a).ToList();
            }
        }
    }
}

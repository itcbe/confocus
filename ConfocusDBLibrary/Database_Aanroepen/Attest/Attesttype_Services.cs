﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Attesttype_Services
    {
        public List<AttestType> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.AttestType
                        where a.Actief == true
                        orderby a.Naam
                        select a).ToList();
            }
        }

        public void SaveAttesttype(AttestType attest)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from a in entities.AttestType
                                where a.Naam == attest.Naam
                                select a).FirstOrDefault();

                if (gevonden != null)
                {
                    gevonden.Actief = attest.Actief;
                    gevonden.Gewijzigd = attest.Gewijzigd;
                    gevonden.Gewijzigd_door = attest.Gewijzigd_door;
                }
                else
                {
                    entities.AttestType.Add(attest);
                }

                try
                {
                    entities.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    throw new Exception(ErrorMessages.DataBaseBezet);
                }
                catch (Exception ex)
                {
                    throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                }
            }
        }

        public void SaveAttesttypeChanges(AttestType attest)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from a in entities.AttestType
                                where a.ID == attest.ID
                                select a).FirstOrDefault();

                if (gevonden != null)
                {
                    if (!gevonden.Modified.SequenceEqual(attest.Modified))
                        throw new CertificateConcurrencyException();

                    gevonden.Naam = attest.Naam;
                    gevonden.Actief = attest.Actief;
                    gevonden.Gewijzigd = attest.Gewijzigd;
                    gevonden.Gewijzigd_door = attest.Gewijzigd_door;
                    try
                    {
                        entities.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                    }
                }
            }
        }

        public List<AttestType> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.AttestType
                        select a).ToList();
            }
        }

        public AttestType GetByID(decimal attesttype_ID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.AttestType
                        where a.ID == attesttype_ID
                        select a).FirstOrDefault();
            }
        }

        public AttestType GetAdminitratiefType()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.AttestType
                        where a.Naam == "Administratief"
                        select a).FirstOrDefault();
            }
        }
    }
}

﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class ErkenningServices
    {
        public List<Erkenning> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from e in confocusEntities.Erkenning.Include("AttestType1") orderby e.Naam select e).ToList();
            }
        }

        public void SaveErkenning(Erkenning erkenning)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Erkenning.Add(erkenning);
                confocusEntities.SaveChanges();
            }
        }

        public void SaveErkenningWijzigingen(Erkenning erkenning)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenErkenning = (from er in confocusEntities.Erkenning
                                         where er.ID == erkenning.ID
                                         select er).FirstOrDefault();
                if (gevondenErkenning != null)
                {
                    //if (!gevondenErkenning.Modified.SequenceEqual(erkenning.Modified))
                    //    throw new RecognitionConcurrencyException();

                    gevondenErkenning.Naam = erkenning.Naam;
                    gevondenErkenning.Instantie = erkenning.Instantie;
                    gevondenErkenning.Contactpersoon = erkenning.Contactpersoon;
                    gevondenErkenning.Telefoonnummer = erkenning.Telefoonnummer;
                    gevondenErkenning.Email = erkenning.Email;
                    gevondenErkenning.Website = erkenning.Website;
                    gevondenErkenning.Notitie = erkenning.Notitie;
                    gevondenErkenning.Attest = erkenning.Attest;
                    gevondenErkenning.AttestType = erkenning.AttestType;
                    gevondenErkenning.Erkend = erkenning.Erkend;
                    gevondenErkenning.Actief = erkenning.Actief;
                    gevondenErkenning.Gewijzigd = erkenning.Gewijzigd;
                    gevondenErkenning.Gewijzigd_door = erkenning.Gewijzigd_door;

                    confocusEntities.SaveChanges();
                }
            }
        }


        public Erkenning GetErkenningByID(decimal? id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from e in confocusEntities.Erkenning.Include("AttestType1")
                        where e.ID == id
                        select e).FirstOrDefault();
            }
        }

        public List<Erkenning> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from e in confocusEntities.Erkenning.Include("AttestType1")
                        where e.Actief == true
                        orderby e.Naam
                        select e).ToList();
            }
        }
    }
}

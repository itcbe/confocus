﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class DagType_Services
    {
        public List<DagType> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from a in entities.DagType
                        where a.Actief == true
                        orderby a.Naam
                        select a).ToList();
            }
        }

        public DagType Save(DagType type)
        {
            using (var entities = new ConfocusEntities5())
            {
                entities.DagType.Add(type);
                entities.SaveChanges();
                return type;
            }
        }

        public DagType Update(DagType type)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from d in entities.DagType
                                where d.ID == type.ID
                                select d).FirstOrDefault();
                if (gevonden != null)
                {
                    //if (!gevonden.Modified.SequenceEqual(type.Modified))
                    //    throw new DayTypeConcurrencyException();

                    gevonden.Naam = type.Naam;
                    gevonden.Nederlands = type.Nederlands;
                    gevonden.Frans = type.Frans;
                    gevonden.Gewijzigd = DateTime.Now;
                    gevonden.Gewijzigd_door = type.Gewijzigd_door;
                    gevonden.Actief = type.Actief;
                    entities.SaveChanges();
                    return type;
                }
                else
                    throw new Exception(ErrorMessages.DagTypeNietGevondenFout);
                
            }
        }

        public DagType GetByName(string dagtype)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from d in entities.DagType
                        where d.Naam == dagtype
                        select d).FirstOrDefault();
            }
        }
    }
}

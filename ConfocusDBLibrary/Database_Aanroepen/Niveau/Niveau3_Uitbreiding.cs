﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Niveau3
    {
        public string FullName
        {
            get
            {
                return Niveau2.Niveau1En2Namen;
            }

        }

        public override bool Equals(object obj)
        {
            if (obj is Niveau3)
            {
                Niveau3 deAndere = obj as Niveau3;
                if (deAndere.Naam == this.Naam)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

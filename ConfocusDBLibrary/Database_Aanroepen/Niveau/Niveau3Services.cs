﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Niveau3Services
    {
        public List<Niveau3> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n3 in confocusEntities.Niveau3.Include("Niveau2")
                        orderby n3.Naam
                        select n3).ToList();
            }
        }

        public List<Niveau3> GetSelected(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n3 in confocusEntities.Niveau3
                        where n3.Niveau2_ID == id
                        && n3.Actief == true
                        orderby n3.Naam
                        select n3).ToList();
            }
        }

        public Niveau3 SaveNiveau3(Niveau3 niveau3)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Niveau3.Add(niveau3);
                confocusEntities.SaveChanges();
                return niveau3;
            }
        }

        public void SaveNiveauWijzigingen3(Niveau3 N3)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenNiveau3 = (from niveau in confocusEntities.Niveau3
                                       where niveau.ID == N3.ID
                                       select niveau).FirstOrDefault();
                if (gevondenNiveau3 != null)
                {
                    //if (!gevondenNiveau3.Modified.SequenceEqual(N3.Modified))
                    //    throw new Level3ConcurrencyException();

                    gevondenNiveau3.Naam = N3.Naam;
                    gevondenNiveau3.Naam_FR = N3.Naam_FR;
                    gevondenNiveau3.Niveau2_ID = N3.Niveau2_ID;
                    gevondenNiveau3.Actief = N3.Actief;
                    gevondenNiveau3.Gewijzigd = N3.Gewijzigd;
                    gevondenNiveau3.Gewijzigd_door = N3.Gewijzigd_door;
                    confocusEntities.SaveChanges();
                }
            }

        }

        public Niveau3 GetNiveau3ByID(decimal? id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n3 in confocusEntities.Niveau3
                        where n3.ID == id
                        orderby n3.Naam
                        select n3).FirstOrDefault();
            }
        }

        public Niveau3 GetNiveau3ByName(string naam, decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n3 in confocusEntities.Niveau3
                        where n3.Naam.ToLower() == naam.ToLower()
                        && n3.Niveau2_ID == id
                        select n3).FirstOrDefault();
            }
        }

        public List<Niveau3> FindActice()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n3 in confocusEntities.Niveau3.Include("Niveau2")
                        where n3.Actief == true
                        orderby n3.Naam
                        select n3).Distinct().ToList();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Niveau2
    {
        public String Niveau1En2Namen 
        {
            get
            {
                using (var entities = new ConfocusEntities5())
                {
                    String naam = "";
                    Niveau2 n2 = (from n in entities.Niveau2.Include("Niveau1")
                                  where n.ID == this.ID
                                  select n).FirstOrDefault();
                    naam = n2.Niveau1.Naam + " : " + n2.Naam;
                    return naam;
                }
                
            }
        }
    }
}

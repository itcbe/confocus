﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Niveau2Services
    {
        public List<Niveau2> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n1 in confocusEntities.Niveau2.Include("Niveau1")
                        orderby n1.Naam
                        select n1).ToList();
            }
        }

        public List<Niveau2> FindAllComboBox()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n1 in confocusEntities.Niveau2.Include("Niveau1")
                        orderby n1.Niveau1.Naam, n1.Naam
                        select n1).ToList();
            }
        }

        public List<Niveau2> GetSelected(decimal N1ID)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n2 in confocusEntities.Niveau2
                        where n2.Niveau1_ID == N1ID
                        && n2.Actief == true
                        orderby n2.Naam
                        select n2).ToList();
            }
        }

        public Niveau2 SaveNiveau2(Niveau2 niveau2)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Niveau2.Add(niveau2);
                confocusEntities.SaveChanges();
                return niveau2;
            }
        }

        public void SaveNiveau2Wijzigingen(Niveau2 N2)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenNiveau2 = (from niveau in confocusEntities.Niveau2
                                       where niveau.ID == N2.ID
                                       select niveau).FirstOrDefault();
                if (gevondenNiveau2 != null)
                {
                    //if (!gevondenNiveau2.Modified.SequenceEqual(N2.Modified))
                    //    throw new Level2ConcurrencyException();

                    gevondenNiveau2.Naam = N2.Naam;
                    gevondenNiveau2.Naam_FR = N2.Naam_FR;
                    gevondenNiveau2.Niveau1_ID = N2.Niveau1_ID;
                    gevondenNiveau2.Actief = N2.Actief;
                    gevondenNiveau2.Gewijzigd = N2.Gewijzigd;
                    gevondenNiveau2.Gewijzigd_door = N2.Gewijzigd_door;
                    confocusEntities.SaveChanges();
                }
            }
        }

        public Niveau2 GetNiveau2ByID(decimal? id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n2 in confocusEntities.Niveau2
                        where n2.ID == id
                        orderby n2.Naam
                        select n2).FirstOrDefault();
            }
        }

        public Niveau2 GetNiveau2ByName(string naam, decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n2 in confocusEntities.Niveau2
                        where n2.Naam.ToLower() == naam.ToLower()
                        && n2.Niveau1_ID == id
                        select n2).FirstOrDefault();
            }
        }

        public List<Niveau2> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from n2 in confocusEntities.Niveau2.Include("Niveau1")
                        where n2.Actief == true
                        orderby n2.Naam
                        select n2).Distinct().ToList();
            }
        }
    }
}

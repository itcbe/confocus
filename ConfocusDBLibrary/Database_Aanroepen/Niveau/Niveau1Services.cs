﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Niveau1Services
    {
        public List<Niveau1> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from niveau in confocusEntities.Niveau1
                        orderby niveau.Naam
                        select niveau).ToList();
            }
        }

        public void SaveNiveau1(Niveau1 N1)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Niveau1.Add(N1);
                confocusEntities.SaveChanges();
            }
        }

        public void SaveNiveau1Wijzigingen(Niveau1 n1)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenNiveau1 = (from niveau in confocusEntities.Niveau1
                                       where niveau.ID == n1.ID
                                       select niveau).FirstOrDefault();
                if (gevondenNiveau1 != null)
                {
                    //if (!gevondenNiveau1.Modified.SequenceEqual(n1.Modified))
                    //    throw new Level1ConcurrencyException();

                    gevondenNiveau1.Naam = n1.Naam;
                    gevondenNiveau1.Naam_FR = n1.Naam_FR;
                    gevondenNiveau1.Actief = n1.Actief;
                    gevondenNiveau1.Gewijzigd = n1.Gewijzigd;
                    gevondenNiveau1.Gewijzigd_door = n1.Gewijzigd_door;
                    confocusEntities.SaveChanges();
                }
            }
        }

        public Niveau1 GetNiveau1ByID(decimal? id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from niveau in confocusEntities.Niveau1
                        where niveau.ID == id
                        select niveau).FirstOrDefault();
            }
        }

        public List<Niveau1> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from niveau in confocusEntities.Niveau1
                        where niveau.Actief == true
                        orderby niveau.Naam
                        select niveau).ToList();
            }
        }

        public Niveau1 GetNiveau1ByName(string n1)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from niveau in confocusEntities.Niveau1
                        where niveau.Naam.ToLower() == n1.ToLower()
                        orderby niveau.Naam
                        select niveau).FirstOrDefault();
            }
        }
    }
}

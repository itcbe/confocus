﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Regio
    {
        public string strActief
        {
            get { return Actief == true ? "Ja" : "Nee"; }
        }
    }
}

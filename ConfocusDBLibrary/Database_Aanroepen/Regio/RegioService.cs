﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class RegioService
    {
        public List<Regio> GetActief()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from r in confocusEntities.Regio where r.Actief == true orderby r.Regio1 select r).ToList();
            }
        }

        public void Save(Regio regio)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from r in confocusEntities.Regio where r.Regio1.ToLower() == regio.Regio1.ToLower() select r).FirstOrDefault();
                if (gevonden == null)
                {
                    confocusEntities.Regio.Add(regio);
                    confocusEntities.SaveChanges();
                }
                else
                    throw new Exceptions.RegioBestaatReedsException();
            }
        }

        public void Update(Regio regio)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from r in confocusEntities.Regio where r.ID == regio.ID select r).FirstOrDefault();
                if (gevonden != null)
                {
                    gevonden.Regio1 = regio.Regio1;
                    gevonden.Actief = regio.Actief;
                    gevonden.Gewijzigd = regio.Gewijzigd;
                    gevonden.Gewijzigd_door = regio.Gewijzigd_door;

                    confocusEntities.SaveChanges();  
                }
            }
        }

        public List<Regio> GetAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from r in confocusEntities.Regio orderby r.Regio1 select r).ToList();
            }
        }
    }
}

﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConfocusDBLibrary
{
    //[DataObject]
    public class AansprekingServices
    {
        //[DataObjectMethod(DataObjectMethodType.Select)]
        public List<Aanspreking> FindAll()
        {
            using (var Entities = new ConfocusEntities5())
            {
                return (from aanspreking in Entities.Aanspreking.Include("Taal")
                        
                        select aanspreking).ToList();
            }
        }

        public void SaveAanspreking(Aanspreking aanspreking)
        {
            using (var Entities = new ConfocusEntities5())
            {
                
                try
                {
                    Entities.Aanspreking.Add(aanspreking);
                    Entities.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    throw new Exception(ErrorMessages.DataBaseBezet);
                }
                catch (Exception ex)
                {
                    throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                }
            }
        }

        public void SaveAansprekingWijzigingen(Aanspreking aanspreking)
        {
            using (var ConfocusEntities = new ConfocusEntities5())
            {
                var gevondenAanspreking = (from a in ConfocusEntities.Aanspreking
                                           where a.ID == aanspreking.ID
                                           select a).FirstOrDefault();

                if (gevondenAanspreking != null)
                {
                    //if (!gevondenAanspreking.Modified.SequenceEqual(aanspreking.Modified))
                    //    throw new SalutationConcurrencyException();
                    gevondenAanspreking.Aanspreking_man = aanspreking.Aanspreking_man;
                    gevondenAanspreking.Aanspreking_vrouw = aanspreking.Aanspreking_vrouw;
                    gevondenAanspreking.Taal_ID = aanspreking.Taal_ID;
                    gevondenAanspreking.Gewijzigd = aanspreking.Gewijzigd;
                    gevondenAanspreking.Gewijzigd_door = aanspreking.Gewijzigd_door;

                    try
                    {
                        ConfocusEntities.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                    }
                }
            }
        }

        public Aanspreking GetByLanguage(decimal id)
        {
            using (var Entities = new ConfocusEntities5())
            {              
                var aanspreking = (from a in Entities.Aanspreking
                                   where a.Taal_ID == id
                                   select a).FirstOrDefault();
                return aanspreking;
            }
        }
    }
}

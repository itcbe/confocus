﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ConfocusClassLibrary;

namespace ConfocusDBLibrary
{
    public class BedrijfServices
    {
        public List<Bedrijf> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        orderby b.Naam
                        select b).ToList();
            }
        }

        public List<Bedrijf> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.Actief == true
                        orderby b.Naam
                        select b).ToList();
            }
        }

        public Bedrijf SaveBedrijf(Bedrijf bedrijf)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                try
                {
                    Bedrijf gevonden = (from b in confocusEntities.Bedrijf
                                        where b.Naam.ToLower() == bedrijf.Naam.ToLower()
                                        select b).FirstOrDefault();

                    if (gevonden != null)
                    {
                        throw new Exception(ErrorMessages.BedrijfBestaatReeds);
                    }
                    else
                    {
                        var transactionOptions = new TransactionOptions()
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead
                        };
                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
                        {
                            confocusEntities.Bedrijf.Add(bedrijf);
                            confocusEntities.SaveChanges();
                            scope.Complete();
                            return bedrijf;
                        }
                    }
                }
                catch (DbUpdateException)
                {
                    throw new Exception(ErrorMessages.DataBaseBezet);
                }
                catch (Exception ex)
                {
                    throw new Exception("Opslaan bedrijf mislukt. Probeer later opnieuw.\n" + ex.Message);
                }
            }
        }

        public List<Bedrijf> GetAvtiveBySearchTerm(ContactZoekTerm naam)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                if (!string.IsNullOrEmpty(naam.Bedrijfsnaam) && !string.IsNullOrEmpty(naam.Bedrijfsadres))
                {
                    return (from b in confocusEntities.Bedrijf
                            where b.BedrijfsnaamFonetisch.ToLower().Contains(naam.Bedrijfsnaam.ToLower())
                            && b.Adres.ToLower().Contains(naam.Bedrijfsadres.ToLower())
                            && b.Actief == true
                            orderby b.Naam
                            select b).ToList();
                }
                else if(string.IsNullOrEmpty(naam.Bedrijfsnaam) && !string.IsNullOrEmpty(naam.Bedrijfsadres))
                {
                    return (from b in confocusEntities.Bedrijf
                            where b.Adres.ToLower().Contains(naam.Bedrijfsadres.ToLower())
                            && b.Actief == true
                            orderby b.Naam
                            select b).ToList();
                }
                else if(!string.IsNullOrEmpty(naam.Bedrijfsnaam) && string.IsNullOrEmpty(naam.Bedrijfsadres))
                {
                    return (from b in confocusEntities.Bedrijf
                            where b.BedrijfsnaamFonetisch.ToLower().Contains(naam.Bedrijfsnaam.ToLower())
                            && b.Actief == true
                            orderby b.Naam
                            select b).ToList();
                }
                return null;
            }
        }

        public Bedrijf GetBedrijfByID(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.ID == id
                        select b).FirstOrDefault();
            }
        }

        public List<Bedrijf> GetBySearchTerm(ContactZoekTerm naam)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.Naam.ToLower().Contains(naam.Bedrijfsnaam.ToLower())
                        || (b.Adres != null && b.Adres.Contains(naam.Bedrijfsnaam))
                        //&& b.Actief == true
                        orderby b.Naam
                        select b).ToList();
            }
        }

        public void SaveBedrijfChanges(Bedrijf b)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                if (b == null)
                    throw new Exception("Bedrijf is leeg! Kan dit niet opslaan.");

                var gevondenBedrijf = (from bedrijf in confocusEntities.Bedrijf
                                       where bedrijf.ID == b.ID
                                       select bedrijf).FirstOrDefault();

                if (gevondenBedrijf != null)
                {
                    //if (!gevondenBedrijf.Modified.SequenceEqual(b.Modified))
                    //    throw new CompanyConcurrencyException();
                    gevondenBedrijf.Naam = b.Naam;
                    gevondenBedrijf.Ondernemersnummer = b.Ondernemersnummer;
                    gevondenBedrijf.Adres = b.Adres;
                    gevondenBedrijf.Postcode = b.Postcode;
                    gevondenBedrijf.Plaats = b.Plaats;
                    gevondenBedrijf.Provincie = b.Provincie;
                    gevondenBedrijf.Land = b.Land;
                    gevondenBedrijf.Telefoon = b.Telefoon;
                    gevondenBedrijf.Fax = b.Fax;
                    gevondenBedrijf.Website = b.Website;
                    gevondenBedrijf.Email = b.Email;
                    gevondenBedrijf.Notities = b.Notities;
                    gevondenBedrijf.Actief = b.Actief;
                    gevondenBedrijf.BedrijfsnaamFonetisch = b.BedrijfsnaamFonetisch;
                    gevondenBedrijf.Zipcode = b.Zipcode;
                    if (b.Gewijzigd != null)
                        gevondenBedrijf.Gewijzigd = b.Gewijzigd;
                    if (!string.IsNullOrEmpty(b.Gewijzigd_door))
                        gevondenBedrijf.Gewijzigd_door = b.Gewijzigd_door;
                    try
                    {
                        confocusEntities.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {

                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Opslaan bedrijf mislukt. Probeer het later opnieuw.\n" + ex.Message);
                    }
                }
            }
        }

        public List<Bedrijf> GetBedrijfByName(string bedrijf)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.Naam.ToLower() == bedrijf
                        && b.Actief == true
                        orderby b.Naam
                        select b).ToList();
            }
        }

        public Bedrijf GetBestaandBedrijfByName(string bedrijf)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.Naam.ToLower() == bedrijf
                        orderby b.Naam
                        select b).FirstOrDefault();
            }
        }

        public List<Bedrijf> GetBedrijfBySearchTerm(string searchterm)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.Naam.ToLower().Contains(searchterm.ToLower())
                        && b.Actief == true
                        orderby b.Naam
                        select b).ToList();
            }
        }

        public List<Bedrijf> GetWherePhonetischIsNull()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf where b.BedrijfsnaamFonetisch == null select b).ToList();
            }
        }

        public List<Bedrijf> GetWhereZipcodeIsNull()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from b in confocusEntities.Bedrijf
                        where b.Zipcode == null
                        && b.Postcode != ""
                        select b).ToList();
            }
        }
    }
}

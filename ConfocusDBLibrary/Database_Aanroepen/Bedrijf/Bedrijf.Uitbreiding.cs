﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Bedrijf
    {
        public String NaamEnBtwNummer 
        {
            get
            {
                return Naam + " " + Ondernemersnummer;
            }

        }

        public String NaamEnAdres 
        {
            get
            {
                string adres = "";
                if (!string.IsNullOrEmpty(Adres))
                    adres = " (" + Adres + ", " + Postcode + ", " + Plaats + " )";
                return Naam + adres;
            }
        }

        public string FullAdres
        {
            get
            {
                string adres = "";
                if (!string.IsNullOrEmpty(Adres))
                    adres = Adres + " " + Postcode + " " + Plaats.ToUpper();
                return adres;
            }
        }

        public string strPostcode
        {
            get
            {
                string psc = "";
                if (this.Zipcode != null)
                    psc = new PostcodeService().GetByID(Zipcode).Postcode;
                return psc;
            }
        }

        public string strGemeente
        {
            get
            {
                string gem = "";
                if (this.Zipcode != null)
                    gem = new PostcodeService().GetByID(Zipcode).Gemeente;
                return gem;
            }
        }

        public string strProvincie
        {
            get
            {
                string pro = "";
                if (this.Zipcode != null)
                    pro = new PostcodeService().GetByID(Zipcode).Provincie;
                return pro;
            }
        }

        public string strLand
        {
            get
            {
                string lan = "";
                if (this.Zipcode != null)
                    lan = new PostcodeService().GetByID(Zipcode).Land;
                return lan;
            }
        }

        public string strPostcodeEnPlaats
        {
            get
            {
                string PP = "";
                if (this.Zipcode != null)
                {
                    Postcodes code = new PostcodeService().GetByID(Zipcode);
                    PP = code.PostcodeGemeente;
                }
                    
                return PP;
            }
        }
    }
}

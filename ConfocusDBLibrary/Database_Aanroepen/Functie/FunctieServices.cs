﻿using ConfocusClassLibrary;
using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Transactions;

namespace ConfocusDBLibrary
{
    public class FunctieServices
    {
        public List<Functies> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from f in confocusEntities.Functies.Include("Contact").Include("Contact.Taal").Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("AttestType1")
                        select f).ToList();
            }
        }

        public ObservableCollection<Functies> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from f in confocusEntities.Functies.Include("Contact").Include("Contact.Taal").Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("AttestType1")
                                where f.Contact.Actief == true
                       && ((f.Einddatum == null) || ((DateTime)f.Einddatum >= DateTime.Today))
                                select f).ToList();
                return new ObservableCollection<Functies>(gevonden);
            }
        }

        public Functies SaveFunctie(Functies functie)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var query = from f in confocusEntities.Functies
                            where f.Niveau1_ID == functie.Niveau1_ID
                            && f.Contact_ID == functie.Contact_ID
                            && f.Bedrijf_ID == functie.Bedrijf_ID
                            && f.Email == functie.Email
                            select f;

                if (functie.Niveau2_ID != null)
                    query = query.AsQueryable().Where(f => f.Niveau2_ID == functie.Niveau2_ID);
                if (functie.Niveau3_ID != null)
                    query = query.AsQueryable().Where(f => f.Niveau3_ID == functie.Niveau3_ID);

                Functies gevonden = query.FirstOrDefault();

                if (gevonden != null)
                {
                    throw new Exception(ErrorMessages.FunctieBestaatReeds);
                }
                else
                {
                    try
                    {
                        confocusEntities.Functies.Add(functie);
                        confocusEntities.SaveChanges();
                        return (from f in confocusEntities.Functies.Include("Contact").Include("Bedrijf").Include("Contact.Taal").Include("AttestType1") where f.ID == functie.ID select f).FirstOrDefault();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                }
            }
        }

        public List<Functies> GetAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from f in entities.Functies select f).ToList();
            }
        }

        public List<Functies> getHRMFuncties()
        {
            using (var entities = new ConfocusEntities5())
            {
                List<decimal> hrms = new List<decimal>() { 55, 57, 71, 83, 93, 110, 136, 147, 159, 166, 169, 178, 185, 188 };
                var gevonden = (from f in entities.Functies.Include("Niveau1").Include("Niveau2").Include("Niveau3")
                                where hrms.Contains((decimal)f.Niveau2_ID)
                                || (f.Niveau1_ID != 20 && f.Niveau2.Naam.ToLower().StartsWith("verantwoordelijke"))
                                select f).ToList();

                //var gevonden2 = (from f in entities.Functies.Include("Niveau1").Include("Niveau2").Include("Niveau3")
                //                 where f.Niveau2.Naam.ToLower().Contains("verantwoordelijke")

                return gevonden;
            }
        }

        public List<Functies> GetReplaceEmail(string text)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from f in entities.Functies.Include("Contact").Include("Bedrijf")
                        where f.Email.ToLower().Contains(text.ToLower())
                        || f.Bedrijf.Email.ToLower().Contains(text.ToLower())
                        orderby f.Contact.Achternaam
                        select f).ToList();
            }
        }

        public Functies GetCommunicatiePersoon(string voornaam, string naam, string email, Bedrijf bedrijf)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (string.IsNullOrEmpty(naam) && string.IsNullOrEmpty(email))
                    return null;
                //string[] names = name.Split(' ');
                string firstName = voornaam;//DBHelper.Simplify(names[0].ToLower());
                string lastName = naam;
                if (naam.Length > 1)
                    lastName = DBHelper.Simplify(naam);

                var query2 = (from f in entities.Functies.Include("Contact").Include("Bedrijf")
                             where f.Bedrijf.Naam.ToLower().Contains(bedrijf.Naam.ToLower())
                             select f).ToList();
                //List<Functies> tempquery = query2.ToList();
                var query3 = from f in query2 select f;
                if (!string.IsNullOrEmpty(firstName))
                {
                    query3 = query2.AsQueryable().Where(f => f.Contact.VoornaamFonetisch == firstName || f.Contact.AchternaamFonetisch == firstName);
                    query3 = query3.AsQueryable().Where(f => f.Contact.AchternaamFonetisch == lastName || f.Contact.VoornaamFonetisch == lastName);
                }

                if (query3.ToList().Count < 1)
                {
                    query3 = query2.AsQueryable().Where(f => f.Email.ToLower() == email.ToLower());
                }

                try
                {
                    return query3.FirstOrDefault();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<Functies> GetActiveFunctieByContactName(ContactZoekTerm contact)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var query = from f in confocusEntities.Functies.Include("Contact").Include("Bedrijf").Include("AttestType1") where f.Actief == true select f;
                if (!string.IsNullOrEmpty(contact.Achternaam))
                {
                    if (contact.Achternaam[0] == '*')
                    {
                        string achternaam = contact.Achternaam.Substring(1, contact.Achternaam.Length - 1);
                        query = query.AsQueryable().Where(f => f.Contact.AchternaamFonetisch.ToLower().Contains(achternaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(f => f.Contact.AchternaamFonetisch.ToLower().StartsWith(contact.Achternaam.ToLower()));
                }
                if (!string.IsNullOrEmpty(contact.Voornaam))
                {
                    if (contact.Voornaam[0] == '*')
                    {
                        string voornaam = contact.Voornaam.Substring(1, contact.Voornaam.Length - 1);
                        query = query.AsQueryable().Where(f => f.Contact.VoornaamFonetisch.ToLower().Contains(voornaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(f => f.Contact.VoornaamFonetisch.ToLower().StartsWith(contact.Voornaam.ToLower()));
                }
                query = query.AsQueryable().Where(f => f.Einddatum == null || (DateTime)f.Einddatum >= DateTime.Today);
                query = query.AsQueryable().OrderBy(f => f.Contact.Achternaam).ThenBy(f => f.Contact.Voornaam);
                return query.ToList();
            }
        }

        public Functies GetByNameAndEmailAndCompany(string name, string email, Bedrijf bedrijf)
        {
            using (var entities = new ConfocusEntities5())
            {
                if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(email))
                    return null;
                string[] names = name.Split(' ');
                string firstName = DBHelper.Simplify(names[0].ToLower());
                string lastName = "";
                if (names.Length > 1)
                    lastName = DBHelper.Simplify(names[1].ToLower());

                var query2 = from f in entities.Functies.Include("Contact").Include("Bedrijf")
                             where f.Bedrijf.Naam.ToLower().Contains(bedrijf.Naam.ToLower())
                             select f;

                var query3 = from f in query2.ToList() select f;
                if (!string.IsNullOrEmpty(name))
                {
                    query3 = query2.AsQueryable().Where(f => f.Contact.VoornaamFonetisch == firstName || f.Contact.AchternaamFonetisch == firstName);
                    query3 = query3.AsQueryable().Where(f => f.Contact.AchternaamFonetisch == lastName || f.Contact.VoornaamFonetisch == lastName);
                }

                if (query3.ToList().Count < 1)
                {
                    query3 = query2.AsQueryable().Where(f => f.Email.ToLower() == email.ToLower());
                }

                return query3.FirstOrDefault();


                //var query = from f in entities.Functies.Include("Contact")
                //            where (f.Contact.VoornaamFonetisch == firstName || f.Contact.VoornaamFonetisch == lastName)
                //            && (f.Contact.AchternaamFonetisch == lastName || f.Contact.AchternaamFonetisch == firstName)
                //            || f.Email.ToLower() == email.ToLower()
                //            select f;
                ////if (!string.IsNullOrEmpty(email))
                ////    query = query.AsQueryable().Where(f => f.Email.ToLower() == email.ToLower());

                //if (bedrijf != null)
                //    if (!string.IsNullOrEmpty(bedrijf.Naam))
                //        query = query.AsQueryable().Where(f => f.Bedrijf.Naam.ToLower().Contains(bedrijf.Naam.ToLower()));

                //return query.FirstOrDefault();
            }
        }

        public Functies GetByNameAndEmail(string name, string email)
        {
            using (var entities = new ConfocusEntities5())
            {
                string[] names = name.Split(' ');
                string firstName = names[0].ToLower();
                string lastName = "";
                if (names.Length > 1)
                    lastName = names[1].ToLower();

                var query = from f in entities.Functies.Include("Contact")
                            where (f.Contact.Voornaam.ToLower() == firstName || f.Contact.Voornaam.ToLower() == lastName)
                            && (f.Contact.Achternaam.ToLower() == lastName || f.Contact.Achternaam.ToLower() == firstName)
                            select f;
                if (!string.IsNullOrEmpty(email))
                    query = query.AsQueryable().Where(f => f.Email.ToLower() == email.ToLower());

                return query.FirstOrDefault();
            }
        }

        public List<Functies> GetFunctieByContactName(ContactZoekTerm contact)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var query = from f in confocusEntities.Functies.Include("Contact").Include("Bedrijf").Include("AttestType1") select f;
                if (!string.IsNullOrEmpty(contact.Achternaam))
                {
                    if (contact.Achternaam[0] == '*')
                    {
                        string achternaam = DBHelper.Simplify(contact.Achternaam).Substring(1, contact.Achternaam.Length - 1);
                        query = query.AsQueryable().Where(f => f.Contact.AchternaamFonetisch.ToLower().Contains(achternaam.ToLower()));
                    }
                    else
                    {
                        string achternaam = DBHelper.Simplify(contact.Achternaam).ToLower();
                        query = query.AsQueryable().Where(f => f.Contact.AchternaamFonetisch.ToLower().StartsWith(achternaam));
                    }
                }
                if (!string.IsNullOrEmpty(contact.Voornaam))
                {
                    if (contact.Voornaam[0] == '*')
                    {
                        string voornaam = DBHelper.Simplify(contact.Voornaam).Substring(1, contact.Voornaam.Length - 1);
                        query = query.AsQueryable().Where(f => f.Contact.VoornaamFonetisch.ToLower().Contains(voornaam.ToLower()));
                    }
                    else
                    {
                        string voornaam = DBHelper.Simplify(contact.Voornaam).ToLower();
                        query = query.AsQueryable().Where(f => f.Contact.VoornaamFonetisch.ToLower().StartsWith(voornaam));
                    }
                }
                query = query.AsQueryable().OrderBy(f => f.Contact.Achternaam).ThenBy(f => f.Contact.Voornaam);
                return query.ToList();
            }
        }

        public List<Functies> GetFunctieByContact(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from f in confocusEntities.Functies.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("Contact").Include("AttestType1")
                        where f.Contact_ID == id
                        && f.Actief == true
                        && (f.Einddatum == null || (DateTime)f.Einddatum >= DateTime.Today)
                        select f).ToList();
            }
        }

        public List<Functies> GetFunctieByTitel(string functie)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from f in entities.Functies where f.Oorspronkelijke_Titel.ToLower() == functie.ToLower() select f).ToList();
            }
        }

        public Functies SaveFunctieWijzigingen(Functies functie)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Functies gevondenFunctie = (from f in confocusEntities.Functies
                                            where f.ID == functie.ID
                                            select f).FirstOrDefault(); ;

                if (gevondenFunctie != null)
                {
                    //if (!gevondenFunctie.Modified.SequenceEqual(functie.Modified))
                    //    throw new FunctionConcurrencyException();
                    gevondenFunctie.Bedrijf_ID = functie.Bedrijf_ID;
                    gevondenFunctie.Contact_ID = functie.Contact_ID;
                    gevondenFunctie.Niveau1_ID = functie.Niveau1_ID;
                    gevondenFunctie.Niveau2_ID = functie.Niveau2_ID;
                    gevondenFunctie.Niveau3_ID = functie.Niveau3_ID;
                    gevondenFunctie.Attesttype_ID = functie.Attesttype_ID;
                    gevondenFunctie.Oorspronkelijke_Titel = functie.Oorspronkelijke_Titel;
                    gevondenFunctie.Email = functie.Email;
                    gevondenFunctie.Telefoon = functie.Telefoon;
                    gevondenFunctie.Mobiel = functie.Mobiel;
                    //gevondenFunctie.Erkenning_ID = functie.Erkenning_ID;
                    gevondenFunctie.Attesttype = functie.Attesttype;
                    gevondenFunctie.Erkenningsnummer = functie.Erkenningsnummer;
                    gevondenFunctie.Mail = functie.Mail;
                    gevondenFunctie.Fax = functie.Fax;
                    gevondenFunctie.Einddatum = functie.Einddatum;
                    gevondenFunctie.Startdatum = functie.Startdatum;
                    gevondenFunctie.Type = functie.Type;
                    gevondenFunctie.Actief = functie.Actief;
                    gevondenFunctie.Gewijzigd = functie.Gewijzigd;
                    gevondenFunctie.Gewijzigd_door = functie.Gewijzigd_door;

                    try
                    {
                        confocusEntities.SaveChanges();
                        return gevondenFunctie;
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                }
                else
                    return null;
            }
        }

        public List<Functies> GetFunctieByContactNameAndCompany(string voornaam, string achternaam, string bedrijf)
        {
            using (var entities = new ConfocusEntities5())
            {
                string voornaamfon = DBHelper.Simplify(voornaam);
                string achternaamfon = DBHelper.Simplify(achternaam);

                var query = (from f in entities.Functies.Include("Contact").Include("Bedrijf").Include("AttestType1")
                             where (f.Contact.VoornaamFonetisch.ToLower() == voornaamfon.ToLower() || f.Contact.Voornaam.ToLower() == voornaam.ToLower())
                             && (f.Contact.AchternaamFonetisch.ToLower() == achternaamfon.ToLower() || f.Contact.Achternaam.ToLower() == achternaam.ToLower())
                             //&& f.Bedrijf.Naam == bedrijf
                             && f.Actief == true
                             && (f.Einddatum == null || f.Einddatum > DateTime.Today)
                             select f);

                var tempquery = query.AsQueryable().Where(f => f.Bedrijf.Naam.ToLower().Contains(bedrijf.ToLower()));

                if (tempquery.ToList().Count > 0)
                    return tempquery.ToList();

                return query.ToList();

            }
        }

        public List<Functies> GetFunctiesByContactBedrijf(decimal contactid, decimal bedrijfid)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from f in confocusEntities.Functies.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Erkenning").Include("Bedrijf").Include("Contact").Include("AttestType1")
                        where f.Contact_ID == contactid
                        && f.Bedrijf_ID == bedrijfid
                        && f.Actief == true
                        && (f.Einddatum == null || (DateTime)f.Einddatum >= DateTime.Today)
                        select f).ToList();
            }
        }

        public Functies GetFunctieByID(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from f in confocusEntities.Functies.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Erkenning").Include("Bedrijf").Include("Contact").Include("Contact.Taal").Include("AttestType1")
                        where f.ID == id
                        select f).FirstOrDefault();
            }
        }

        public List<Functies> GetFunctiesInBedrijf(decimal bedrijfId)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from f in confocusEntities.Functies.Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Erkenning").Include("Bedrijf").Include("Contact").Include("AttestType1")
                        where f.Bedrijf_ID == bedrijfId
                        select f).ToList();
            }
        }

        public int ReplaceEmail(ReplaceEmail mail)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var gevonden = (from f in entities.Functies
                                where f.Email.ToLower().Contains(mail.searchText.ToLower())
                                select f).ToList();

                foreach (Functies functie in gevonden)
                {
                    functie.Email = functie.Email.Replace(mail.searchText, mail.replaceText);
                }

                aantal = gevonden.Count;

                entities.SaveChanges();
                return aantal;
            }
        }

        public Functies GetFunctieByContactBedrijfNiveaus(Functies functie)
        {
            using (var entities = new ConfocusEntities5())
            {
                Functies gevonden = (from f in entities.Functies
                                     where f.Niveau1_ID == functie.Niveau1_ID
                                     && ((f.Niveau2_ID == functie.Niveau2_ID) || (f.Niveau2_ID == null))
                                     && ((f.Niveau3_ID == functie.Niveau3_ID) || (f.Niveau3_ID == null))
                                     && f.Contact_ID == functie.Contact_ID
                                     && f.Bedrijf_ID == functie.Bedrijf_ID
                                     select f).FirstOrDefault();
                return gevonden;
            }
        }

        public ObservableCollection<Functies> FindByFilter(ContactSearchFilter filter)
        {
            using (var entities = new ConfocusEntities5())
            {
                ObservableCollection<Functies> gevonden = new ObservableCollection<Functies>();
                try
                {
                    var transactionSettings = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadUncommitted
                    };
                    using (var transscope = new TransactionScope(TransactionScopeOption.Required, transactionSettings))
                    {
                        DateTime vandaag = DateTime.Today;
                        var query = from f in entities.Functies.Include("Contact").Include("Contact.Taal").Include("Niveau1").Include("Niveau2").Include("Niveau3").Include("Bedrijf").Include("AttestType1") where f.Actief == true select f;

                        if (!string.IsNullOrEmpty(filter.Achternaam))
                        {
                            if (filter.Achternaam[0] == '*')
                            {
                                string achternaam = filter.Achternaam.Substring(1, filter.Achternaam.Length - 1);
                                query = query.AsQueryable().Where(f => f.Contact.AchternaamFonetisch.ToLower().Contains(achternaam.ToLower()));
                            }
                            else
                                query = query.AsQueryable().Where(f => f.Contact.AchternaamFonetisch.ToLower().StartsWith(filter.Achternaam.ToLower()));
                        }
                        if (!string.IsNullOrEmpty(filter.Voornaam))
                        {
                            if (filter.Voornaam[0] == '*')
                            {
                                string voornaam = filter.Voornaam.Substring(1, filter.Voornaam.Length - 1);
                                query = query.AsQueryable().Where(f => f.Contact.VoornaamFonetisch.ToLower().Contains(filter.Voornaam.ToLower()));
                            }
                            else
                                query = query.AsQueryable().Where(f => f.Contact.VoornaamFonetisch.ToLower().StartsWith(filter.Voornaam.ToLower()));
                        }
                        if (filter.Bedrijf_ID > 0)
                            query = query.AsQueryable().Where(f => f.Bedrijf_ID == filter.Bedrijf_ID);
                        if (filter.Email == "Leeg")
                            query = query.AsQueryable().Where(f => f.Email == null);
                        else if (filter.Email != "")
                            query = query.AsQueryable().Where(f => f.Email.ToLower().Contains(filter.Email.ToLower()));
                        if (filter.Niveau1_ID > 0)
                            query = query.AsQueryable().Where(f => f.Niveau1_ID == filter.Niveau1_ID);
                        if (filter.Niveau2_ID > 0)
                            query = query.AsQueryable().Where(f => f.Niveau2_ID == filter.Niveau2_ID);
                        if (filter.Niveau3_ID > 0)
                            query = query.AsQueryable().Where(f => f.Niveau3_ID == filter.Niveau3_ID);
                        if (filter.Gemeente != "")
                            query = query.AsQueryable().Where(f => f.Bedrijf.Plaats == filter.Gemeente);
                        if (filter.ContactType != "")
                            query = query.AsQueryable().Where(f => f.Type.ToLower() == filter.ContactType.ToLower());
                        if (filter.Door != "")
                            query = query.AsQueryable().Where(f => f.Gewijzigd_door == filter.Door || f.Aangemaakt_door == filter.Door);
                        if (filter.Provincie != "")
                            query = query.AsQueryable().Where(f => f.Bedrijf.Provincie == filter.Provincie);
                        if (filter.Aangemaakt > new DateTime(2014, 12, 1))
                            query = query.AsQueryable().Where(f => SqlFunctions.DateDiff("DAY", f.Contact.Aangemaakt, filter.Aangemaakt) == 0);//(DateTime)f.Contact.Aangemaakt).ToShortDateString() == filter.Aangemaakt.ToShortDateString()
                        if (filter.Taal_ID != 0)
                            query = query.AsQueryable().Where(f => f.Contact.Taal_ID == filter.Taal_ID || f.Contact.Secundaire_Taal_ID == filter.Taal_ID);
                        if (filter.Faxnummer != "")
                            query = query.AsQueryable().Where(f => f.Bedrijf.Fax.Replace(" ", "").Contains(filter.Faxnummer.Replace(" ", "")));
                        query = query.AsQueryable().Where(f => f.Einddatum == null || (DateTime)f.Einddatum > vandaag);

                        query = query.AsQueryable().OrderBy(f => f.Contact.Achternaam).ThenBy(f => f.Contact.Voornaam);

                        try
                        {
                            gevonden = new ObservableCollection<Functies>(query.ToList());
                            transscope.Complete();
                        }
                        catch (System.TimeoutException tex)
                        {
                            throw new Exception("Er is een timeout op de database gebeurt!\nProbeer de gegevens opnieuw op te vragen.");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                }
                catch (Exception nex)
                {
                    throw nex;
                }
                return gevonden;
            }
        }

        public void SetProspect()
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from f in entities.Functies
                                where f.Contact.Type == "Spreker"
                                select f).ToList();

                if (gevonden.Count > 0)
                {
                    foreach (Functies functie in gevonden)
                    {
                        functie.Type = "Klant";
                        functie.Gewijzigd = DateTime.Now;
                        functie.Gewijzigd_door = functie.Aangemaakt_door;
                        SaveFunctieWijzigingen(functie);
                    }
                }
            }
        }

        public Functies ActivateFunction(Functies functie)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var query = from f in confocusEntities.Functies
                            where f.Niveau1_ID == functie.Niveau1_ID
                            && f.Contact_ID == functie.Contact_ID
                            && f.Bedrijf_ID == functie.Bedrijf_ID
                            && f.Email == functie.Email
                            select f;

                if (functie.Niveau2_ID != null)
                    query = query.AsQueryable().Where(f => f.Niveau2_ID == functie.Niveau2_ID);
                if (functie.Niveau3_ID != null)
                    query = query.AsQueryable().Where(f => f.Niveau3_ID == functie.Niveau3_ID);

                Functies gevonden = query.FirstOrDefault();

                if (gevonden != null)
                {
                    gevonden.Bedrijf_ID = functie.Bedrijf_ID;
                    gevonden.Contact_ID = functie.Contact_ID;
                    gevonden.Niveau1_ID = functie.Niveau1_ID;
                    gevonden.Niveau2_ID = functie.Niveau2_ID;
                    gevonden.Niveau3_ID = functie.Niveau3_ID;
                    gevonden.Attesttype_ID = functie.Attesttype_ID;
                    gevonden.Oorspronkelijke_Titel = functie.Oorspronkelijke_Titel;
                    gevonden.Email = functie.Email;
                    gevonden.Telefoon = functie.Telefoon;
                    gevonden.Mobiel = functie.Mobiel;
                    //gevondenFunctie.Erkenning_ID = functie.Erkenning_ID;
                    gevonden.Attesttype = functie.Attesttype;
                    gevonden.Erkenningsnummer = functie.Erkenningsnummer;
                    gevonden.Mail = functie.Mail;
                    gevonden.Fax = functie.Fax;
                    gevonden.Einddatum = functie.Einddatum;
                    gevonden.Startdatum = functie.Startdatum;
                    gevonden.Type = functie.Type;
                    gevonden.Actief = functie.Actief;
                    gevonden.Gewijzigd = functie.Gewijzigd;
                    gevonden.Gewijzigd_door = functie.Gewijzigd_door;

                    try
                    {
                        confocusEntities.SaveChanges();
                        return (from f in confocusEntities.Functies.Include("Contact").Include("Bedrijf").Include("Contact.Taal").Include("AttestType1") where f.ID == functie.ID select f).FirstOrDefault();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                }
                else
                {
                    throw new Exception("Functie niet gevonden!");
                }
            }
        }

        public Functies GetExcistingFunction(Functies functie)
        {
            using (var entities = new ConfocusEntities5())
            {
                var query = from f in entities.Functies.Include("Contact").Include("Bedrijf").Include("Contact.Taal").Include("AttestType1")
                            where f.Niveau1_ID == functie.Niveau1_ID
                            && f.Contact_ID == functie.Contact_ID
                            && f.Bedrijf_ID == functie.Bedrijf_ID
                            && f.Email == functie.Email
                            select f;

                if (functie.Niveau2_ID != null)
                    query = query.AsQueryable().Where(f => f.Niveau2_ID == functie.Niveau2_ID);
                if (functie.Niveau3_ID != null)
                    query = query.AsQueryable().Where(f => f.Niveau3_ID == functie.Niveau3_ID);

                return query.FirstOrDefault();
            }
        }

        public List<Functies> GetFaxList()
        {
            throw new NotImplementedException();
        }

        public List<Functies> GetActiveFuncties()
        {
            throw new NotImplementedException();
        }

        public List<Functies> GetActiveFunctiesByFaxnumber(string fax)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var result = (from f in confocusEntities.Functies.Include("Bedrijf") where f.Bedrijf != null && f.Bedrijf.Fax == fax select f).ToList();
                return result;
            }
        }
    }
}

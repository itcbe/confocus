﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Functies
    {
        public String Functienaam 
        {
            get
            {
                Niveau1Services n1service = new Niveau1Services();
                Niveau1 n1 = n1service.GetNiveau1ByID(Niveau1_ID);
                String naam = n1.Naam;
                if (Niveau2_ID != null)
                {
                    Niveau2Services n2service = new Niveau2Services();
                    Niveau2 n2 = n2service.GetNiveau2ByID(Niveau2_ID);
                    naam += " " + n2.Naam;
                }
                if (Niveau3_ID != null)
                {
                    Niveau3Services n3service = new Niveau3Services();
                    Niveau3 n3 = n3service.GetNiveau3ByID(Niveau3_ID);
                    naam += " " + n3.Naam;
                }
                return naam;
            }
        }

        public string NaamEnNiveaus
        {
            get
            {
                string naam = "";
                ContactServices cService = new ContactServices();
                Contact contact = cService.GetContactByID(this.Contact_ID);
                naam = contact.Achternaam + " " + contact.Voornaam;
                BedrijfServices bService = new BedrijfServices();
                Bedrijf bedrijf = bService.GetBedrijfByID(this.Bedrijf_ID);
                naam += ": " + bedrijf.Naam;

                Niveau1Services n1service = new Niveau1Services();
                Niveau1 n1 = n1service.GetNiveau1ByID(Niveau1_ID);
                naam += ": " + n1.Naam;
                if (Niveau2_ID != null)
                {
                    Niveau2Services n2service = new Niveau2Services();
                    Niveau2 n2 = n2service.GetNiveau2ByID(Niveau2_ID);
                    naam += ", " + n2.Naam;
                }
                if (Niveau3_ID != null)
                {
                    Niveau3Services n3service = new Niveau3Services();
                    Niveau3 n3 = n3service.GetNiveau3ByID(Niveau3_ID);
                    naam += ", " + n3.Naam;
                }

                return naam;
            }
        }

        public String NaamEnFunctie
        {
            get
            {
                ContactServices cService = new ContactServices();
                Contact contact = cService.GetContactByID(this.Contact_ID);
                return contact.VolledigeNaam + ", " + this.Oorspronkelijke_Titel + ": " + this.Email;
            }
        }

        public string NaamBedrijfFunctie
        {
            get
            {
                ContactServices cService = new ContactServices();
                Contact contact = cService.GetContactByID(this.Contact_ID);
                BedrijfServices bService = new BedrijfServices();
                Bedrijf bedrijf = bService.GetBedrijfByID(this.Bedrijf_ID);
                return contact.VolledigeNaam + ", " + bedrijf.Naam + " : " + this.Oorspronkelijke_Titel + " " + this.Email;
            }
        }

        public string MailAsText 
        {
            get
            {
                return Mail == true ? "Ja" : "Nee";
            }
        }

        public string FaxAsText 
        {
            get
            {
                return Fax == true ? "Ja" : "Nee";
            }
        }

        public bool EqualsContact(object obj)
        {
            if (obj is Functies)
            {
                if (this.Contact_ID == ((Functies)obj).Contact_ID)
                    return true;
                else
                    return false;

            }
            else
                return false;
        }

        public bool IsEmpty()
        {
            return (this.Niveau1_ID == 0 && this.Contact_ID == 0 && this.Bedrijf_ID == 0);
        }

        public override bool Equals(object obj)
        {
            if (obj is Functies)
            {
                if (this.ID == ((Functies)obj).ID)
                    return true;
                else
                    return false;
            }
            else
                return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool ContainsContact(object list)
        {
            List<Functies> functies = (List<Functies>)list;
            bool found = false;
            foreach (Functies func in functies)
            {
                if (func.EqualsContact(this))
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
    }
}

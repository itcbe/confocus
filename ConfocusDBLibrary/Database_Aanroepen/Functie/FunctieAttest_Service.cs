﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class FunctieAttest_Service
    {
        public FunctieAttest_Service()
        {

        }

        public void Add(FunctieAttestType type)
        {
            using (var entities = new ConfocusEntities5())
            {
                var found = (from fa in entities.FunctieAttestType where fa.FunctieID == type.FunctieID && fa.AttesttypeID == type.AttesttypeID select fa).FirstOrDefault();

                if (found == null)
                {
                    entities.FunctieAttestType.Add(type);
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Deze functie heeft dit attesttype reeds!");
            }
        }

        public void Update(FunctieAttestType type)
        {
            using (var entities = new ConfocusEntities5())
            {

            }
        }

        public void Delete(FunctieAttestType type)
        {
            using (var entities = new ConfocusEntities5())
            {

            }
        }

        public List<FunctieAttestType> GetByFunctieID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from fa in entities.FunctieAttestType
                        where fa.FunctieID == id
                        select fa).ToList();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Deelnemer
    {
        //private int myprop
        public String Bedrijfsnaam 
        {
            get
            {
                String naam = "";
                using (var confocusEntities = new ConfocusEntities5())
                {
                    var inschrijving = (from i in confocusEntities.Inschrijving.Include("Bedrijf")
                                        where i.ID == this.Inschrijving_ID
                                        select i).FirstOrDefault();
                    naam = inschrijving.Bedrijf.Naam;
                }
                return naam;
            }
        }

        public String ContactNaam 
        {
            get
            {
                String naam = "";
                using (var confocusEntities = new ConfocusEntities5())
                {
                    var contact = (from c in confocusEntities.Contact
                                   where c.ID == this.Contact_ID
                                   select c).FirstOrDefault();
                    if (contact != null)
                        naam = contact.Voornaam + " " + contact.Achternaam;
                }
                return naam;
            }
        }

        public String SessieNaam 
        { 
            get
            {
                String naam = "";
                using (var confocusEntities = new ConfocusEntities5())
                {

                    var inschrijving = (from i in confocusEntities.Inschrijving.Include("Sessie")
                                        where i.ID == this.Inschrijving_ID
                                        select i).FirstOrDefault();

                    var inschrijvingen = (from ins in confocusEntities.Inschrijving.Include("Sessie").Include("Sessie.Locatie")
                                          where ins.Sessie_ID == inschrijving.Sessie_ID
                                          select ins).ToList();


                    if (inschrijving != null)
                        naam = inschrijving.Sessie.Naam + " : " + (inschrijving.Sessie.Datum == null ? "" : ((DateTime)inschrijving.Sessie.Datum).ToShortDateString()) + " : " + inschrijving.Sessie.Locatie.Naam + " (" + inschrijvingen.Count + " inschrijving(en))";
                }
                return naam;
            } 
        }

        public String FacturatieNaam {
            get
            {
                String naam = "";
                if (this.FaturatieOp_ID != null)
                {
                    BedrijfServices bService = new BedrijfServices();
                    Bedrijf bedrijf = bService.GetBedrijfByID((Decimal)this.FaturatieOp_ID);
                    naam = bedrijf.Naam;
                }
                return naam;
            }
        }

        public String Achternaam 
        {
            get
            {
                if (this.Contact != null)
                    return this.Contact.Achternaam;
                else
                {
                    DeelnemerServices dService = new DeelnemerServices();
                    Deelnemer fulldeelnemer = dService.GetDeelnemerByID(this.ID);
                    if (fulldeelnemer.Contact == null)
                        return "z";
                    else
                        return fulldeelnemer.Contact.Achternaam;
                }
            }
        }
    }
}

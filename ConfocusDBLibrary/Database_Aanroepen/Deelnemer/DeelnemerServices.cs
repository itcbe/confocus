﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class DeelnemerServices
    {
        public List<Deelnemer> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from d in entities.Deelnemer.Include("Inschrijving").Include("Contact").Include("Bedrijf")
                        where d.Actief == true
                        select d).ToList();
            }
        }

        public List<Deelnemer> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from d in entities.Deelnemer.Include("Inschrijving").Include("Contact").Include("Bedrijf")
                        select d).ToList();
            }
        }

        public Deelnemer GetDeelnemerByID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from d in entities.Deelnemer.Include("Inschrijving").Include("Contact").Include("Bedrijf")
                        where d.ID == id
                        select d).FirstOrDefault();
            }
        }

        public Deelnemer SaveDeelnemer(Deelnemer deelnemer)
        {
            using (var entities = new ConfocusEntities5())
            {
                Deelnemer gevonden = (from d in entities.Deelnemer
                                      where d.Contact_ID == deelnemer.Contact_ID
                                      && d.Inschrijving_ID == deelnemer.Inschrijving_ID
                                      select d).FirstOrDefault();

                if (gevonden == null)
                {
                    deelnemer.Inschrijving = null;
                    entities.Deelnemer.Add(deelnemer);
                    entities.SaveChanges();
                    return deelnemer;
                }
                else
                {
                    gevonden.Inschrijving_ID = deelnemer.Inschrijving_ID;
                    gevonden.Contact_ID = deelnemer.Contact_ID;
                    gevonden.Functie = deelnemer.Functie;
                    gevonden.Mail_ontvangen = deelnemer.Mail_ontvangen;
                    gevonden.Evaluatie_ontvangen = deelnemer.Evaluatie_ontvangen;
                    gevonden.Aanwezig = deelnemer.Aanwezig;
                    gevonden.Attest_opmaken = deelnemer.Attest_opmaken;
                    gevonden.CCEmail = deelnemer.CCEmail;
                    gevonden.FaturatieOp_ID = deelnemer.FaturatieOp_ID;
                    gevonden.Status = deelnemer.Status;
                    gevonden.KMO_nummer = deelnemer.KMO_nummer;
                    gevonden.Attesttype = deelnemer.Attesttype;
                    gevonden.Actief = deelnemer.Actief;
                    gevonden.Gewijzigd = deelnemer.Gewijzigd;
                    gevonden.Gewijzigd_door = deelnemer.Gewijzigd_door;

                    entities.SaveChanges();

                    return gevonden;
                }
            }
        }

        public Deelnemer SaveDeelnemerChanges(Deelnemer deelnemer)
        {
            if (deelnemer != null)
            {
                using (var entities = new ConfocusEntities5())
                {
                    Deelnemer gevonden = (from d in entities.Deelnemer
                                          where d.ID == deelnemer.ID
                                          select d).FirstOrDefault();
                    if (gevonden != null)
                    {
                        gevonden.Inschrijving_ID = deelnemer.Inschrijving_ID;
                        gevonden.Contact_ID = deelnemer.Contact_ID;
                        gevonden.Functie = deelnemer.Functie;
                        gevonden.Mail_ontvangen = deelnemer.Mail_ontvangen;
                        gevonden.Evaluatie_ontvangen = deelnemer.Evaluatie_ontvangen;
                        gevonden.Aanwezig = deelnemer.Aanwezig;
                        gevonden.Attest_opmaken = deelnemer.Attest_opmaken;
                        gevonden.CCEmail = deelnemer.CCEmail;
                        gevonden.FaturatieOp_ID = deelnemer.FaturatieOp_ID;
                        gevonden.Status = deelnemer.Status;
                        gevonden.KMO_nummer = deelnemer.KMO_nummer;
                        gevonden.Attesttype = deelnemer.Attesttype;
                        gevonden.Actief = deelnemer.Actief;
                        gevonden.Gewijzigd = deelnemer.Gewijzigd;
                        gevonden.Gewijzigd_door = deelnemer.Gewijzigd_door;

                        entities.SaveChanges();
                    }
                    return gevonden;
                }
            }
            else
                return null;
        }

        public List<Deelnemer> GetDeelmenersByInschrijvingID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from d in entities.Deelnemer.Include("Inschrijving").Include("Contact")
                        where d.Inschrijving_ID == id
                        && d.Actief == true
                        orderby d.Contact.Achternaam
                        select d).ToList();
            }
        }

        public List<Deelnemer> GetFromSessie(decimal sessieid)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from d in entities.Deelnemer.Include("Inschrijving").Include("Contact")
                        where d.Inschrijving.Sessie.ID == sessieid
                        && d.Actief == true
                        orderby d.Contact.Achternaam
                        select d).ToList();
            }
        }

        public void SetDeelnemerOpNonActief(Deelnemer deelnemer)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Deelnemer> gevonden = (from d in entities.Deelnemer
                                      where (d.Contact_ID == deelnemer.Contact_ID || d.Contact_ID == null)
                                      && d.Inschrijving_ID == deelnemer.Inschrijving_ID
                                      select d).ToList();

                if (gevonden.Count > 0)
                {
                    foreach (Deelnemer de in gevonden)
                    {
                        if (de.Actief != false)
                        {
                            de.Actief = false;
                            entities.SaveChanges();
                            break;
                        }
                    }
                }
            }
        }

        public bool CheckIfExcists(Deelnemer deelnemer)
        {
            using (var entities = new ConfocusEntities5())
            {
                Deelnemer gevonden = (from d in entities.Deelnemer
                                      where (d.Contact_ID == deelnemer.Contact_ID || d.Contact_ID == null)
                                      && d.Inschrijving_ID == deelnemer.Inschrijving_ID
                                      select d).FirstOrDefault();

                if (gevonden != null)
                    return true;
                else
                    return false;
            }
        }
    }
}

﻿namespace ConfocusDBLibrary
{
    public class ContactZoekTerm
    {
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public string Bedrijfsnaam { get; set; }
        public string Bedrijfsadres { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Contact
    {
        public String VolledigeNaam 
        {
            get
            {
                return Achternaam + " " + Voornaam;
            }       
        }

        public String NaamEnGeboortedatum
        {
            get
            {
                return Achternaam + " " + Voornaam  + " - " + Geboortedatum;
            }
        }

        public string SimpleVoornaam
        {
            get
            {
                return DBHelper.Simplify(Voornaam);
            }
        }

        public string SimpleAchternaam
        {
            get
            {
                return DBHelper.Simplify(Achternaam);
            }
        }
    }
}

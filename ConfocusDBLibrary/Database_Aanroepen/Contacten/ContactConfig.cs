﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class ContactConfig
    {
        // Contact
        public Boolean ID { get; set; }
        public Boolean Voornaam { get; set; }
        public Boolean Achternaam { get; set; }
        public Boolean Geboortedatum { get; set; }
        public Boolean Aanspreking { get; set; }
        public Boolean Geslacht { get; set; }
        public Boolean ContactNotities { get; set; }
        public Boolean Taal { get; set; }
        public Boolean ContactActief { get; set; }
        //public Boolean Communicatie_inschrijving { get; set; }
        // Bedrijf
        public Boolean Bedrijf { get; set; }
        public Boolean BTWnummer { get; set; }
        public Boolean Telefoon { get; set; }
        public Boolean Fax { get; set; }
        public Boolean Email { get; set; }
        public Boolean Adres { get; set; }
        public Boolean Postcode { get; set; }
        public Boolean Gemeente { get; set; }
        public Boolean BedrijfsNotities { get; set; }
        public Boolean Website { get; set; }
        public Boolean BedrijfActief { get; set; }
        public Boolean Gewijzigd { get; set; }
        public Boolean GewijzigdDoor { get; set; }
        public Boolean Aangemaakt { get; set; }
        public Boolean AangemaaktDoor { get; set; }
        //public Boolean Facturatie_inschrijvingen { get; set; }
        // Functie
        public Boolean Niveau1 { get; set; }
        public Boolean Niveau2 { get; set; }
        public Boolean Niveau3 { get; set; }
        public Boolean Attesttype { get; set; }
        public Boolean Erkenningsnummer { get; set; }
        public Boolean Type { get; set; }
        public Boolean ContactEmail { get; set; }
        public Boolean ContactTelefoon { get; set; }
        public Boolean Startdatum { get; set; }
        public Boolean Einddatum { get; set; }
        public Boolean NieuwsbriefMail { get; set; }
        public Boolean NieuwsbriefFax { get; set; }
        public Boolean FunctieActief { get; set; }



        public ContactConfig() { }
    }
}

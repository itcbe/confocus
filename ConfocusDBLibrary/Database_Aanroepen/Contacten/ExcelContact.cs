﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class ExcelContact
    {
        public String Niveau1 { get; set; }
        public String Niveau2 { get; set; }
        public String Niveau3 { get; set; }
        public String Oorspronkelijke_titel { get; set; }
        public String Bedrijf { get; set; }
        public String Webpagina { get; set; }
        public String Adres { get; set; }
        public String Postcode { get; set; }
        public String Plaats { get; set; }
        public String Provincie { get; set; }
        public String Land { get; set; }
        public String TelefoonBedrijf { get; set; }
        public String TelefoonContact { get; set; }
        public String Mobiel { get; set; }
        public String Fax { get; set; }
        public String EmailBedrijf { get; set; }
        public String EmailContact { get; set; }
        public String Aanspreking { get; set; }
        public String Voornaam { get; set; }
        public String Achternaam { get; set; }
        public String Taal { get; set; }
        public String Type { get; set; }

        public ExcelContact() { }
    }
}

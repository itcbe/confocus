﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace ConfocusDBLibrary
{
    //[DataObject()]
    public class ContactServices
    {
        //[DataObjectMethod(DataObjectMethodType.Select)]
        public List<Contact> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from contact in confocusEntities.Contact.Include("Taal").Include("Functies")
                        where contact.VoornaamFonetisch == null
                        orderby contact.Achternaam
                        select contact).ToList();
            }
        }

        public List<Contact> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from contact in confocusEntities.Contact.Include("Taal").Include("Functies")
                        where contact.Actief == true
                        select contact).ToList();
            }
        }

        public Contact SaveContact(Contact contact, bool controle)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                if (controle)
                {
                    Contact gevonden = (from c in confocusEntities.Contact
                                        where c.Voornaam.ToLower() == contact.Voornaam.ToLower()
                                        && c.Achternaam.ToLower() == contact.Achternaam.ToLower()
                                        select c).FirstOrDefault();

                    if (gevonden != null)
                    {
                        throw new Exception(ErrorMessages.ContactBestaatReeds);
                    }
                    else
                    {
                        contact.VoornaamFonetisch = DBHelper.Simplify(contact.Voornaam);
                        contact.AchternaamFonetisch = DBHelper.Simplify(contact.Achternaam);
                        confocusEntities.Contact.Add(contact);
                        confocusEntities.SaveChanges();
                        return contact;
                    }
                }
                else
                {
                    contact.VoornaamFonetisch = DBHelper.Simplify(contact.Voornaam);
                    contact.AchternaamFonetisch = DBHelper.Simplify(contact.Achternaam);
                    confocusEntities.Contact.Add(contact);
                    confocusEntities.SaveChanges();
                    return contact;
                }
            }
        }

        public Contact GetContactByID(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from contact in confocusEntities.Contact.Include("Taal").Include("Erkenning")
                        where contact.ID == id
                        select contact).FirstOrDefault();
            }
        }

        public void SaveContactChanges(Contact contact)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenContact = (from c in confocusEntities.Contact
                                       where c.ID == contact.ID
                                       select c).FirstOrDefault();

                if (gevondenContact != null)
                {
                    //if (!gevondenContact.Modified.SequenceEqual(contact.Modified))
                    //    throw new CustomerConcurrencyException();
                    gevondenContact.Voornaam = contact.Voornaam;
                    gevondenContact.Achternaam = contact.Achternaam;
                    gevondenContact.Geboortedatum = contact.Geboortedatum;
                    gevondenContact.Aanspreking = contact.Aanspreking;
                    gevondenContact.Taal_ID = contact.Taal_ID;
                    gevondenContact.Secundaire_Taal_ID = contact.Secundaire_Taal_ID;
                    //gevondenContact.Attesttype = contact.Attesttype;
                    gevondenContact.Type = contact.Type;
                    gevondenContact.Notities = contact.Notities;
                    gevondenContact.Actief = contact.Actief;
                    gevondenContact.Gewijzigd = contact.Gewijzigd;
                    gevondenContact.Gewijzigd_door = contact.Gewijzigd_door;
                    gevondenContact.VoornaamFonetisch = DBHelper.Simplify(contact.Voornaam);
                    gevondenContact.AchternaamFonetisch = DBHelper.Simplify(contact.Achternaam);

                    try
                    {
                        confocusEntities.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        throw new Exception(ErrorMessages.DataBaseBezet);
                    }
                                        
                }
            }
        }

        public List<Contact> GetActiveBySearchTerm(ContactZoekTerm searchterm)
        {
            using (var entities = new ConfocusEntities5())
            {

                var query = from c in entities.Contact where c.Actief == true select c;

                if (!string.IsNullOrEmpty(searchterm.Achternaam))
                {
                    if (searchterm.Achternaam[0] == '*')
                    {
                        string achternaam = searchterm.Achternaam.Substring(1, searchterm.Achternaam.Length - 1);
                        query = query.AsQueryable().Where(c => c.AchternaamFonetisch.ToLower().Contains(achternaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(c => c.AchternaamFonetisch.ToLower().StartsWith(searchterm.Achternaam.ToLower()));
                }
                if (!string.IsNullOrEmpty(searchterm.Voornaam))
                {
                    if (searchterm.Voornaam[0] == '*')
                    {
                        string voornaam = searchterm.Voornaam.Substring(1, searchterm.Voornaam.Length - 1);
                        query = query.AsQueryable().Where(c => c.VoornaamFonetisch.ToLower().Contains(voornaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(c => c.VoornaamFonetisch.ToLower().StartsWith(searchterm.Voornaam.ToLower()));
                }

                query = query.AsQueryable().OrderBy(c => c.Achternaam).ThenBy(c => c.Voornaam);

                var gevonden = query.ToList();

                return gevonden;
            }
        }

        public List<Contact> GetContactenByName(string voornaam, string achternaam)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from c in confocusEntities.Contact.Include("Taal")
                        where c.Voornaam.ToLower() == voornaam.ToLower()
                        && c.Achternaam.ToLower() == achternaam.ToLower()
                        select c).ToList();
            }
        }

        public List<Contact> GetContactenByBedrijf(decimal? bedrijf_id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from f in confocusEntities.Functies.Include("Bedrijf").Include("Contact")
                        where f.Bedrijf_ID == bedrijf_id
                        select f.Contact).ToList();
            }
        }

        public Contact GetContactByName(Contact contact)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from c in confocusEntities.Contact
                        where c.Voornaam.ToLower() == contact.Voornaam.ToLower()
                        && c.Achternaam.ToLower() == contact.Achternaam.ToLower()
                        select c).FirstOrDefault();
            }
        }

        public List<string> GetDistinctFamilienamen()
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from c in entities.Contact
                                where c.Actief == true
                                orderby c.Achternaam
                                select c.Achternaam).ToList().Distinct();

                return gevonden.ToList();
            }
        }

        public List<String> GetDictinctVoornaamByAchternaam(string achternaam)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from c in entities.Contact
                                where c.Actief == true
                                && c.Achternaam == achternaam
                                orderby c.Voornaam
                                select c.Voornaam).ToList().Distinct();

                return gevonden.ToList();
            }
        }

        public List<Contact> GetContactenByNamen(string voornaam, string naam)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from c in entities.Contact
                                where c.Actief == true
                                && c.Voornaam == voornaam
                                && c.Achternaam == naam
                                select c).ToList();

                return gevonden;
            }
        }

        public void SetContactTypeToKlant(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                var contact = (from c in entities.Contact
                               where c.ID == id
                               select c).FirstOrDefault();

                if (contact != null)
                {
                    contact.Type = "Klant";
                    entities.SaveChanges();
                }
            }
        }

        public List<Contact> GetBySearchTerm(ContactZoekTerm searchterm)
        {
            using (var entities = new ConfocusEntities5())
            {

                var query = from c in entities.Contact where c.Actief == true select c;

                if (!string.IsNullOrEmpty(searchterm.Achternaam))
                { 
                    if (searchterm.Achternaam[0] == '*')
                    {
                        string achternaam = searchterm.Achternaam.Substring(1, searchterm.Achternaam.Length - 1);
                        query = query.AsQueryable().Where(c => c.AchternaamFonetisch.ToLower().Contains(achternaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(c => c.AchternaamFonetisch.ToLower().StartsWith(searchterm.Achternaam.ToLower()));
                }
                if (!string.IsNullOrEmpty(searchterm.Voornaam))
                {
                    if (searchterm.Voornaam[0] == '*')
                    {
                        string voornaam = searchterm.Voornaam.Substring(1, searchterm.Voornaam.Length - 1);
                        query = query.AsQueryable().Where(c => c.VoornaamFonetisch.ToLower().Contains(voornaam.ToLower()));
                    }
                    else
                        query = query.AsQueryable().Where(c => c.VoornaamFonetisch.ToLower().StartsWith(searchterm.Voornaam.ToLower()));
                }

                query = query.AsQueryable().OrderBy(c => c.Achternaam).ThenBy(c => c.Voornaam);

                var gevonden = query.ToList();

                return gevonden;
            }
        }
    }
}

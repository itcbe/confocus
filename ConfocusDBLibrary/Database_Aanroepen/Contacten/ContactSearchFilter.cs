﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusClassLibrary
{
    public class ContactSearchFilter
    {
        private string _firstname = "";

        public string Voornaam
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        private string _achternaam;

        public string Achternaam
        {
            get { return _achternaam; }
            set { _achternaam = value; }
        }


        private decimal _companyId = 0;

        public decimal Bedrijf_ID
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        private string _bedrijfsnaam = "";

        public string BedrijfsNaam
        {
            get { return _bedrijfsnaam; }
            set { _bedrijfsnaam = value; }
        }

        private string _email = "";

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _type = "";

        public string ContactType
        {
            get { return _type; }
            set { _type = value; }
        }

        private decimal _level1 = 0;

        public decimal Niveau1_ID
        {
            get { return _level1; }
            set { _level1 = value; }
        }

        private string _niveau1Naam = "";

        public string Niveau1Naam
        {
            get { return _niveau1Naam; }
            set { _niveau1Naam = value; }
        }

        private decimal _level2 = 0;

        public decimal Niveau2_ID
        {
            get { return _level2; }
            set { _level2 = value; }
        }

        private string _niveau2Naam = "";

        public string Niveau2Naam
        {
            get { return _niveau2Naam; }
            set { _niveau2Naam = value; }
        }

        private decimal _level3 = 0;

        public decimal Niveau3_ID
        {
            get { return _level3; }
            set { _level3 = value; }
        }

        private string _niveau3Naam = "";

        public string Niveau3Naam
        {
            get { return _niveau3Naam; }
            set { _niveau3Naam = value; }
        }


        private string _city = "";

        public string Gemeente
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _County = "";

        public string Provincie
        {
            get { return _County; }
            set { _County = value; }
        }

        private decimal _taal_ID = 0;

        public decimal Taal_ID
        {
            get { return _taal_ID; }
            set { _taal_ID = value; }
        }

        private string _taalNaam = "";

        public string TaalNaam
        {
            get { return _taalNaam; }
            set { _taalNaam = value; }
        }

        private string _secundairetaalNaam = "";

        public string SecundaireTaalNaam
        {
            get { return _secundairetaalNaam; }
            set { _secundairetaalNaam = value; }
        }


        private string _faxnummer = "";

        public string Faxnummer
        {
            get
            {
                return _faxnummer;
            }
            set
            {
                _faxnummer = value;
            }
        }

        private DateTime _created = new DateTime(2014,12,1);

        public DateTime Aangemaakt
        {
            get { return _created; }
            set { _created = value; }
        }

        public string Door { get; set; }

        public ContactSearchFilter()
        {
            Door = "";
        }
    }
}

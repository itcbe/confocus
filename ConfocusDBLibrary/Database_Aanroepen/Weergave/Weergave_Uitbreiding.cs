﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Weergave
    {
        public string GebruikerAsString
        {
            get
            {
                Gebruiker gebruiker = new GebruikerService().GetUserByID(this.User_ID);
                if (gebruiker != null)
                    return gebruiker.Naam;
                return "";
            }
        }
    }
}

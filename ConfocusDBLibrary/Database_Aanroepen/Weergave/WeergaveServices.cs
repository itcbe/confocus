﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class WeergaveServices
    {
        public List<Weergave> GetWeergavesByUserId(int uID)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from w in confocusEntities.Weergave
                        where w.User_ID == uID
                        select w).ToList();
            }
        }

        public List<Weergave> GetWeergaveById(int id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from w in confocusEntities.Weergave
                        where w.ID == id
                        select w).ToList();
            }
        }

        public Weergave SaveWeergave(Weergave weergave)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                confocusEntities.Weergave.Add(weergave);
                confocusEntities.SaveChanges();
                return weergave;
            }
        }

        public Weergave SaveWeergaveChanges(Weergave weergave)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Weergave gevondenWeergave = (from w in confocusEntities.Weergave
                                             where w.ID == weergave.ID
                                             select w).FirstOrDefault();
                if (gevondenWeergave != null)
                {
                    gevondenWeergave.Naam = weergave.Naam;
                    gevondenWeergave.Bedrijf = weergave.Bedrijf;
                    gevondenWeergave.Email = weergave.Email;
                    gevondenWeergave.Type = weergave.Type;
                    gevondenWeergave.Niveau1 = weergave.Niveau1;
                    gevondenWeergave.Niveau2 = weergave.Niveau2;
                    gevondenWeergave.Niveau3 = weergave.Niveau3;
                    gevondenWeergave.Gemeente = weergave.Gemeente;
                    gevondenWeergave.Provincie = weergave.Provincie;
                    gevondenWeergave.Datum = weergave.Datum;
                    gevondenWeergave.Zoeknaam = weergave.Zoeknaam;
                }
                confocusEntities.SaveChanges();
                return gevondenWeergave;
            }
        }

        public List<Weergave> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from w in confocusEntities.Weergave orderby w.Naam select w).ToList();
            }
        }

        public void Delete(Weergave selected)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Weergave found = (from w in confocusEntities.Weergave where w.ID == selected.ID select w).FirstOrDefault();
                if (found != null)
                {
                    confocusEntities.Weergave.Remove(found);
                    confocusEntities.SaveChanges();
                }
            }
        }
    }
}

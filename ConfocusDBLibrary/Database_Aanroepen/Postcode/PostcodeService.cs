﻿using ConfocusDBLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class PostcodeService
    {
        public List<Postcodes> FindAllPostcodes()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from p in confocusEntities.Postcodes orderby p.Postcode select p).ToList();
            }
        }

        public List<Postcodes> FindAllGemeentes()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from p in confocusEntities.Postcodes orderby p.Gemeente select p).ToList();
            }
        }

        internal Postcodes GetByID(decimal? zipcode)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from p in entities.Postcodes where p.ID == zipcode select p).FirstOrDefault();
            }
        }

        public void SavePostcode(Postcodes postcode)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from p in confocusEntities.Postcodes
                                where p.Postcode == postcode.Postcode
                                && p.Gemeente.ToLower() == postcode.Gemeente.ToLower()
                                select p).FirstOrDefault();
                if (gevonden == null)
                {
                    confocusEntities.Postcodes.Add(postcode);
                    confocusEntities.SaveChanges();
                }
                else
                    throw new Exception("Postcode en gemeente bestaat reeds!");

            }
        }

        public void ChangePostcode(Postcodes postcode)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevondenPostcode = (from p in confocusEntities.Postcodes
                                        where p.ID == postcode.ID
                                        select p).FirstOrDefault();

                if (gevondenPostcode != null)
                {
                    //if (!gevondenPostcode.Modified.SequenceEqual(postcode.Modified))
                    //    throw new ZipcodeConcurrencyException();

                    gevondenPostcode.Postcode = postcode.Postcode;
                    gevondenPostcode.Gemeente = postcode.Gemeente;
                    gevondenPostcode.Provincie = postcode.Provincie;
                    gevondenPostcode.Land = postcode.Land;

                    confocusEntities.SaveChanges();
                }
            }
        }

        public List<Postcodes> GetGemeentesByPostcode(string postcode)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from p in confocusEntities.Postcodes
                        where p.Postcode == postcode
                        select p).ToList();
            }
        }

        public Postcodes GetPostcodeByIDAndCity(string code, string gemeente)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Postcodes  gevonden = (from p in confocusEntities.Postcodes
                                             where p.Postcode == code
                                             && gemeente.ToLower().Contains(p.Gemeente.ToLower())
                                             select p).FirstOrDefault();

                if (gevonden == null)
                    gevonden = (from p in confocusEntities.Postcodes
                                where p.Postcode == code
                                select p).FirstOrDefault();

                return gevonden;
            }
        }

        public Postcodes GetPostcodeByPostcode(string code)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from p in confocusEntities.Postcodes
                        where p.Postcode == code
                        //&& gemeente.ToLower().Contains(p.Gemeente.ToLower())
                        select p).FirstOrDefault();
            }
        }
    }
}

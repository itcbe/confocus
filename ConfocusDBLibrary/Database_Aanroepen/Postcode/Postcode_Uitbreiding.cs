﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Postcodes
    {
        public string PostcodeGemeente
        {
            get
            {
                return this.Postcode + " " + this.Gemeente;
            }
        }
    }
}

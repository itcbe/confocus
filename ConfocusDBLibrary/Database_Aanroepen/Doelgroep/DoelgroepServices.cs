﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class DoelgroepServices
    {
        public List<Doelgroep> FindActive()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from d in confocusEntities.Doelgroep
                        where d.Actief == true
                        select d).ToList();
            }
        }

        public List<Doelgroep> FindAll()
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from d in confocusEntities.Doelgroep
                        select d).ToList();
            }
        }

        public Doelgroep SaveDoelgroep(Doelgroep doelgroep)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                var gevonden = (from d in confocusEntities.Doelgroep
                                where d.Naam == doelgroep.Naam
                                select d).FirstOrDefault();
                if (gevonden == null)
                {
                    confocusEntities.Doelgroep.Add(doelgroep);
                    confocusEntities.SaveChanges();
                }
                return doelgroep;
            }
        }

        public Doelgroep SaveDoelgroepChanges(Doelgroep doelgroep)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                Doelgroep gevondenDoelgroep =  (from d in confocusEntities.Doelgroep
                                where d.ID == doelgroep.ID
                                select d).FirstOrDefault();

                if (gevondenDoelgroep != null)
                {
                    gevondenDoelgroep.Naam = doelgroep.Naam;
                    gevondenDoelgroep.Niveau1_ID = doelgroep.Niveau1_ID;
                    gevondenDoelgroep.Niveau2_ID = doelgroep.Niveau2_ID;
                    gevondenDoelgroep.Niveau3_ID = doelgroep.Niveau3_ID;
                    gevondenDoelgroep.Actief = doelgroep.Actief;
                    gevondenDoelgroep.Gewijzigd = doelgroep.Gewijzigd;
                    gevondenDoelgroep.Gewijzigd_door = doelgroep.Gewijzigd_door;
                    confocusEntities.SaveChanges();
                    return gevondenDoelgroep;
                }
                else
                    return null;
            }
        }
        
        public Doelgroep GetDoelgroepByID(decimal id)
        {
            using (var confocusEntities = new ConfocusEntities5())
            {
                return (from d in confocusEntities.Doelgroep
                            where d.ID == id
                            select d).FirstOrDefault();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITCLibrary;
using System.Data.Objects.SqlClient;
using ConfocusDBLibrary.Exceptions;

namespace ConfocusDBLibrary
{
    public class SessieServices
    {
        public List<Sessie> FindActive()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Actief == true
                        //&& s.Datum >= DateTime.Today
                        && s.Seminarie.Status != "Afgesloten"
                        orderby s.Sorteerdatum
                        select s).ToList();
            }
        }

        public List<Sessie> GetAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie select s).ToList();
            }
        }

        public List<Sessie> GetAllEmpty()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie where s.Sorteerdatum == null select s).ToList();
            }
        }

        internal DateTime? GetFirstSessieDateFromSeminarie(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                DateTime? date = (from s in entities.Sessie where s.Seminarie_ID == iD select s.Sorteerdatum).FirstOrDefault();
                return date;
            }
        }

        public List<Sessie> GetActueleSessies()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1")
                        where s.Actief == true
                        && s.Sorteerdatum >= DateTime.Today
                        && s.Seminarie.Status != "Afgesloten"
                        orderby s.Sorteerdatum
                        select s).ToList();
            }
        }

        public List<Sessie> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        select s).ToList();
            }
        }

        public Sessie GetSessieByID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen").Include("Inschrijvingen.Functies")
                        where s.ID == id
                        select s).FirstOrDefault();
            }
        }

        public List<Sessie> GetSessiesByDates(DateTime van, DateTime tot)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie
                        where s.Sorteerdatum >= van
                        && s.Sorteerdatum <= tot
                        && s.Actief == true
                        && s.Status == "Actief"
                        select s).ToList();
            }
        }

        public Sessie SaveSessie(Sessie sessie)
        {
            using (var entities = new ConfocusEntities5())
            {
                Sessie gevonden = (from s in entities.Sessie
                                   where s.Naam == sessie.Naam
                                   && s.Seminarie_ID == sessie.Seminarie_ID
                                   && s.Locatie_ID == sessie.Locatie_ID
                                   && SqlFunctions.DateDiff("DAY", s.Sorteerdatum, sessie.Sorteerdatum) == 0
                                   //&& ((DateTime)s.Datum).ToShortDateString() == ((DateTime)sessie.Datum).ToShortDateString()
                                   select s).FirstOrDefault();
                if (gevonden == null)
                {
                    entities.Sessie.Add(sessie);
                    entities.SaveChanges();
                }
                else
                    throw new Exception(ErrorMessages.SessieBestaatReeds);
                return sessie;
            }
        }

        public Sessie SaveSessieChanges(Sessie sessie)
        {
            using (var entities = new ConfocusEntities5())
            {
                Sessie gevonden = (from s in entities.Sessie
                                   where s.ID == sessie.ID
                                   select s).FirstOrDefault();
                if (gevonden != null)
                {
                    //if (!gevonden.Modified.SequenceEqual(sessie.Modified))
                    //    throw new SessionConcurrencyException();

                    gevonden.Naam = sessie.Naam;
                    gevonden.Seminarie_ID = sessie.Seminarie_ID;
                    gevonden.Locatie_ID = sessie.Locatie_ID;
                    gevonden.Datum = sessie.Datum;
                    gevonden.Sorteerdatum = sessie.Sorteerdatum;
                    gevonden.Beginuur = sessie.Beginuur;
                    gevonden.Einduur = sessie.Einduur;
                    gevonden.Status = sessie.Status;
                    gevonden.Status_info = sessie.Status_info;
                    gevonden.Zaal = sessie.Zaal;
                    gevonden.URL = sessie.URL;
                    gevonden.Standaard_prijs = sessie.Standaard_prijs;
                    gevonden.Prijs_Non_Profit = sessie.Prijs_Non_Profit;
                    gevonden.Prijs_Profit_Prive = sessie.Prijs_Profit_Prive;
                    gevonden.Target = sessie.Target;
                    gevonden.Kortingsprijs = sessie.Kortingsprijs;
                    gevonden.Erkenning_ID = sessie.Erkenning_ID;
                    gevonden.Erkenningsnummer = sessie.Erkenningsnummer;
                    gevonden.Opstelling_zaal = sessie.Opstelling_zaal;
                    gevonden.Catering_Lunch = sessie.Catering_Lunch;
                    gevonden.Status_Locatie = sessie.Status_Locatie;
                    gevonden.Status_Datum = sessie.Status_Datum;
                    gevonden.KMO_Port_Uren = sessie.KMO_Port_Uren;
                    gevonden.Verantwoordelijke_Terplaatse_ID = sessie.Verantwoordelijke_Terplaatse_ID;
                    gevonden.Verantwoordelijke_Sessie_ID = sessie.Verantwoordelijke_Sessie_ID;
                    gevonden.Beamer = sessie.Beamer;
                    gevonden.Laptop = sessie.Laptop;
                    gevonden.Opmerking = sessie.Opmerking;
                    gevonden.Actief = sessie.Actief;
                    gevonden.Gewijzigd = sessie.Gewijzigd;
                    gevonden.Gewijzigd_door = sessie.Gewijzigd_door;

                    entities.SaveChanges();
                }
                return gevonden;
            }
        }

        public void UpdateSeminarieSessiesTimeStamp(decimal iD, Gebruiker currentUser)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Sessie> sessies = (from s in entities.Sessie where s.Seminarie_ID == iD select s).ToList();
                foreach (Sessie sessie in sessies)
                {
                    UpdateSessieTimeStamp(sessie.ID, currentUser);
                }
            }

        }

        public void UpdateSessieTimeStamp(decimal id, Gebruiker user)
        {
            Sessie sessie = GetSessieByID(id);
            sessie.Gewijzigd_door = user.Naam;
            sessie.Gewijzigd = DateTime.Now;
            SaveSessieTimetampChanges(sessie);
        }

        public void SaveSessieTimetampChanges(Sessie sessie)
        {
            using (var entities = new ConfocusEntities5())
            {
                Sessie gevonden = (from s in entities.Sessie
                                   where s.ID == sessie.ID
                                   select s).FirstOrDefault();
                if (gevonden != null)
                {             
                    gevonden.Gewijzigd = sessie.Gewijzigd;
                    gevonden.Gewijzigd_door = sessie.Gewijzigd_door;
                    entities.SaveChanges();
                }
            }
        }

        public List<Sessie> GetSessiesBySeminarieIDAndSessieNaam(Sessie sessie)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Seminarie_ID == sessie.Seminarie_ID
                        && s.Naam == sessie.Naam
                        && s.Locatie_ID != sessie.Locatie_ID
                        && s.Actief == true
                        orderby s.Sorteerdatum
                        select s).ToList();
            }
        }

        public List<Sessie> FindResent(DateTime filterdate)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Actief == true
                        && s.Seminarie.Status != "Afgesloten"
                        && s.Sorteerdatum > filterdate
                        orderby s.Sorteerdatum, s.Beginuur
                        select s).ToList();
            }
        }

        public List<Sessie> GetSessiesBySeminarieID(decimal sID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Seminarie_ID == sID
                        && s.Actief == true
                        orderby s.Sorteerdatum
                        select s).ToList();
            }
        }

        public List<Sessie> GetSessiesBySeminarieIDHistoriek(decimal sID)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Seminarie_ID == sID
                        orderby s.Sorteerdatum
                        select s).ToList();
            }
        }

        public List<Sessie> GetSessiesFromMonth(DateTime datum)
        {
            DateTime EersteDag = new DateTime(datum.Year, datum.Month, 1);
            DateTime LaatsteDag = new DateTime(datum.Year, datum.Month, DateTime.DaysInMonth(datum.Year, datum.Month));
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Datum >= EersteDag
                        && s.Datum <= LaatsteDag
                        && s.Actief == true
                        //&& s.Seminarie.Status != "Afgesloten"
                        && s.Seminarie.Actief == true
                        select s).ToList();
            }
        }

        public List<Sessie> GetSessiesForWeek(DateTime maandag)
        {
            DateTime zondag = maandag.AddDays(6).AddHours(24);
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen")
                        where s.Datum >= maandag
                        && s.Datum <= zondag
                        && s.Actief == true
                        && s.Seminarie.Status != "Afgesloten"
                        && s.Seminarie.Actief == true
                        select s).ToList();
            }
        }

        public int GetAantalDeelnemersFromSeminarie(decimal seminarieId)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var Sessies = (from s in entities.Sessie.Include("Inschrijvingen")
                               where s.Seminarie_ID == seminarieId
                               && s.Actief == true
                               select s).ToList();

                foreach (Sessie sessie in Sessies)
                {
                    int toadd = (from i in sessie.Inschrijvingen
                                 where i.Status == "Inschrijving"
                                 || i.Status == "Gast"
                                 select i).Count();
                    aantal += toadd;
                }

                return aantal;
            }
        }

        public int GetAantalInschrijvingenPerSeminarie(decimal seminarieId)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var Sessies = (from s in entities.Sessie.Include("Inschrijvingen")
                               where s.Seminarie_ID == seminarieId
                               && s.Actief == true
                               select s).ToList();

                foreach (Sessie sessie in Sessies)
                {
                    int toadd = (from i in sessie.Inschrijvingen
                                 where (i.Status == "Inschrijving"
                                 || i.Status == "Gast")
                                 && i.Actief == true
                                 select i).Count();
                    aantal += toadd;
                }


                return aantal;
            }
        }

        public int GetAantalDeelnemersFromSessie(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                int aantal = 0;
                var Sessies = (from s in entities.Sessie.Include("Inschrijvingen")
                               where s.ID == id
                               select s).ToList();

                foreach (Sessie sessie in Sessies)
                {
                    foreach (Inschrijving inschrijving in sessie.Inschrijving)
                    {
                        foreach (Deelnemer deelnemer in inschrijving.Deelnemer)
                        {
                            if (deelnemer.Actief == true)
                                aantal++;
                        }
                    }

                }
                return aantal;
            }
        }


        internal int GetAanInschrijvingen(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                try
                {
                    int aantal = 0;
                    var sessie = (from s in entities.Sessie.Include("Inschrijvingen")
                                  where s.ID == id
                                  select s).FirstOrDefault();

                    int toadd = (from i in sessie.Inschrijvingen
                                 where i.Status == "Inschrijving"
                                 || i.Status == "Gast"
                                 select i).Count();
                    aantal += toadd;
                    return aantal;

                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public List<Sessie> GetSessieByDate(DateTime datum)
        {
            using (var entities = new ConfocusEntities5())
            {
                try
                {
                    var result = (from s in entities.Sessie.Include("Seminarie") where s.Sorteerdatum == datum && s.Actief == true select s).ToList();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }

            }
        }

        public List<Sessie> GetByIDS(List<decimal> sessies)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen").Include("Inschrijvingen.Functies")
                        where sessies.Contains(s.ID)
                        select s).ToList();

            }
        }

        public List<Sessie> GetSessiesExErkenningSessies(List<Sessie> semerkenningensessies)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<decimal> sesids = (from s in semerkenningensessies select s.ID).ToList();
                return (from s in entities.Sessie.Include("Seminarie").Include("Locatie").Include("Gebruiker1").Include("Inschrijvingen").Include("Inschrijvingen.Functies")
                        where !sesids.Contains(s.ID)
                        && s.Actief == true
                        && s.Sorteerdatum >= DateTime.Today
                        && s.Seminarie.Status != "Afgesloten"
                        orderby s.Sorteerdatum
                        select s).ToList();

            }
        }

        public List<Sessie> GetFullSessies(List<Sessie> sessies)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<decimal> sesids = new List<decimal>();
                foreach (Sessie ses in sessies)
                {
                    sesids.Add(ses.ID);

                }
                return GetByIDS(sesids);


            }
        }

        public List<Sessie> GetSessiesFromInschrijvingsStatus(string status)
        {
            using (var entities = new ConfocusEntities5())
            {
                var result = (from s in entities.GetSessiesFromInschrijvingStatus(status) select s).ToList();
                List<Sessie> sessies = new List<Sessie>();
                foreach (GetSessiesFromInschrijvingStatus_Result sessie in result)
                {
                    sessies.Add(new Sessie(sessie));
                }
                return sessies;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class SessieWeergaven
    {
        public string Locatienaam
        {
            get
            {
                if (this.Locatie_ID != null)
                    return new LocatieServices().GetLocatieByID((decimal)this.Locatie_ID).Naam;
                return "";
            }
        }

        public string Organisatornaam
        {
            get
            {
                if (this.Organisator_ID != null)
                    return new GebruikerService().GetUserByID(this.Organisator_ID).Naam;
                return "";
            }
        }

        public string VerantTerplaatseNaam
        {
            get
            {
                if (this.Verantwoordelijke_terplaatse_ID != null)
                    return new GebruikerService().GetUserByID(this.Verantwoordelijke_terplaatse_ID).Naam;
                return "";
            }
        }

        public string VerantSessieNaam
        {
            get
            {
                if (this.Verantwoordelijke_sessie_ID != null)
                    return new GebruikerService().GetUserByID(this.Verantwoordelijke_sessie_ID).Naam;
                return "";
            }
        }

        public string ErkenningNaam
        {
            get
            {
                if (this.Erkenning_ID != null)
                    return new ErkenningServices().GetErkenningByID(this.Erkenning_ID).Naam;
                return "";
            }
        }

        public string InschrijvingStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public partial class Sessie_Spreker
    {
        public Contact Sessie_Spreker_Contact
        {
            get
            {
                using (var entities = new ConfocusEntities5())
                {
                    Spreker spreker = (from s in entities.Spreker.Include("Contact")
                                       where s.ID == this.Spreker_ID
                                       select s).FirstOrDefault();
                    Contact contact = spreker.Contact;

                    return contact;

                }
            }
        }

        public string SprekerAttesttype
        {
            get
            {
                Attesttype_Services aService = new Attesttype_Services();
                if (this.Spreker.AttestType_ID != null)
                {
                    AttestType attest = aService.GetByID((decimal)this.Spreker.AttestType_ID);
                    return attest.Naam;
                }
                else
                    return "";
            }
        }

        public string SeminarieNaam
        {
            get
            {
                return this.Sessie.SeminarieTitel;
            }
        }
        public string SessieNaam
        {
            get
            { 
                return this.Sessie.Naam;
            }
        }
        public string SessieLocatie
        {
            get
            {
                return this.Sessie.LocatieNaam;
            }
        }
        public DateTime? SessieDatum
        {
            get
            {
                return this.Sessie.Datum;
            }

        }

        public string StrAttestVerstuurd
        {
            get
            {
                if (this.AttestVerstuurd == true)
                {
                    return "Ja";
                }
                return "Nee";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfocusClassLibrary;
using ConfocusDBLibrary.Exceptions;

namespace ConfocusDBLibrary
{
    public class SessieSprekerServices
    {
        public List<Sessie_Spreker> GetSprekersBySessieID(decimal sessieid)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from ss in entities.Sessie_Spreker.Include("Spreker").Include("Spreker.Contact").Include("Spreker.Bedrijf").Include("Sessie").Include("Spreker.Niveau1").Include("Spreker.AttestType1")
                        where ss.Sessie_ID == sessieid
                        && ss.Spreker.Actief == true
                        && (ss.Spreker.Einddatum == null || ss.Spreker.Einddatum > DateTime.Today)
                        orderby ss.Spreker.Contact.Achternaam
                        select ss).ToList();
            }
        }

        public void SaveSpreker(Sessie_Spreker spreker)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from ss in entities.Sessie_Spreker
                                where ss.Sessie_ID == spreker.Sessie_ID
                                && ss.Spreker_ID == spreker.Spreker_ID
                                select ss).FirstOrDefault();

                if (gevonden == null)
                {
                    entities.Sessie_Spreker.Add(spreker);
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception(ErrorMessages.SprekerReedsToegevoegd);
                }          
            }
        }

        public Sessie_Spreker GetById(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from ss in entities.Sessie_Spreker.Include("Spreker").Include("Spreker.Contact").Include("Spreker.Bedrijf").Include("Spreker.AttestType1").Include("Sessie")
                        where ss.ID == id
                        select ss).FirstOrDefault();
            }
        }

        public int GetAantalSprekersFromSessie(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from ss in entities.Sessie_Spreker.Include("Spreker").Include("Spreker.Contact").Include("Sessie")
                        where ss.Sessie_ID == id
                        select ss).Count();
            }
        }

        public bool Delete(Sessie_Spreker spreker)
        {
            try
            {
                using (var entities = new ConfocusEntities5())
                {
                    var teverwijderen = (from ss in entities.Sessie_Spreker
                                         where ss.ID == spreker.ID
                                         && ss.Sessie_ID == spreker.Sessie_ID
                                         select ss).FirstOrDefault();
                    if (teverwijderen != null)
                    {
                        entities.Sessie_Spreker.Remove(teverwijderen);
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        throw new Exception(ErrorMessages.SessieSprekerNietGevondenFout);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Sessie_Spreker GetSessieSprekerByID(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie_Spreker.Include("Sessie").Include("Sessie.Locatie").Include("Sessie.Seminarie").Include("Spreker").Include("Spreker.Contact").Include("Spreker.Bedrijf")
                        where s.ID == iD
                        select s).FirstOrDefault();
            }
        }

        public List<Sessie_Spreker> GetSprekersBySeminarieID(decimal id)
        {
            using (var entities = new ConfocusEntities5())
            {
                List<Sessie_Spreker> sprekers = new List<Sessie_Spreker>();
                var sessies = (from s in entities.Seminarie.Include("Sessie").Include("Sessie.Sessie_Spreker").Include("Sessie.Locatie").Include("Sessie.Seminarie").Include("Spreker").Include("Spreker.AttestType1").Include("Spreker.Contact").Include("Spreker.Bedrijf")
                               where s.ID == id
                               select s.Sessie).FirstOrDefault();

                foreach (Sessie sessie in sessies)
                {
                    foreach (var spreker in sessie.Sessie_Spreker)
                    {
                        sprekers.Add(spreker);
                    }
                }

                sprekers = sprekers.OrderBy(s=> s.Sessie.Datum).ThenBy(s => s.Spreker.Contact.Achternaam).ToList();

                return sprekers;
            }
        }

        public int GetSprekerCountBySessie(decimal iD)
        {
            throw new NotImplementedException();
        }

        public void Update(Sessie_Spreker spreker)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from ss in entities.Sessie_Spreker
                                where ss.ID == spreker.ID
                                select ss).FirstOrDefault();

                if (gevonden == null)
                {
                    throw new Exception("Spreker niet gevonden!");
                }
                else
                {
                    //if (!gevonden.Modified.SequenceEqual(spreker.Modified))
                    //    throw new SessionSpeakerConcurrencyException();

                    gevonden.Sessie_ID = spreker.Sessie_ID;
                    gevonden.Spreker_ID = spreker.Spreker_ID;
                    gevonden.Attesttype = spreker.Attesttype;
                    gevonden.Status = spreker.Status;
                    gevonden.Opmerking = spreker.Opmerking;
                    gevonden.Uren = spreker.Uren;
                    gevonden.AttestVerstuurd = spreker.AttestVerstuurd;

                    entities.SaveChanges();
                }
            }
        }

        public List<Sessie> GetSessiesBySprekerID(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Sessie_Spreker.Include("Sessie").Include("Sessie.Locatie").Include("Sessie.Seminarie").Include("Spreker")
                        where s.Spreker.Contact_ID == iD
                        && s.Sessie.Datum >= DateTime.Today
                        orderby s.Sessie.Datum
                        select s.Sessie).ToList();
            }
        }

        public List<Sessie_Spreker> GetSessieSprekersBySearchterm(HistoriekZoekTerm zoekterm)
        {
            using (var entities = new ConfocusEntities5())
            {
                var query = from s in entities.Sessie_Spreker.Include("Sessie").Include("Sessie.Locatie").Include("Sessie.Seminarie").Include("Spreker").Include("Spreker.Contact").Include("Spreker.Bedrijf")
                            where s.Spreker.Contact_ID == zoekterm.ContactID
                            select s;
                if (zoekterm.StartDate != null)
                    query = query.AsQueryable().Where(s => s.Sessie.Datum >= zoekterm.StartDate);
                if (zoekterm.EndDate != null)
                    query = query.AsQueryable().Where(s => s.Sessie.Datum <= zoekterm.EndDate);

                var result = query.ToList();
                List<Sessie_Spreker> seminarieSprekers = new List<Sessie_Spreker>();
                if (zoekterm.SeminarieIDs.Count > 0)
                {
                    foreach (var id in zoekterm.SeminarieIDs)
                    {
                        foreach (var spreker in result)
                        {
                            if (spreker.Sessie.Seminarie_ID == id)
                            {
                                if (!seminarieSprekers.Contains(spreker))
                                    seminarieSprekers.Add(spreker);
                            }
                        }
                    }
                }

                if (zoekterm.SeminarieIDs.Count > 0)
                    return seminarieSprekers;
                else
                    return result;
            
            }
        }

        public List<Sessie_Spreker> GetSessiesSprekerBySprekerID(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                var sprekers = (from s in entities.Sessie_Spreker.Include("Sessie")
                                where s.Spreker_ID == iD && s.Sessie.Datum > DateTime.Today
                                select s).ToList();

                return sprekers;
            }
        }

        public List<Sessie_Spreker> GetSessieSprekersByContact(decimal iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                var sprekers = (from s in entities.Sessie_Spreker.Include("Sessie")
                                where s.Spreker.Contact_ID == iD && s.Sessie.Datum > DateTime.Today
                                orderby s.Sessie.Datum
                                select s).ToList();

                return sprekers;
            }
        }
    }
}

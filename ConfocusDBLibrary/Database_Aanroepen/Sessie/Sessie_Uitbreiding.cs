﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ConfocusDBLibrary
{
    public partial class Sessie
    {
        public bool IsSelected { get; set; }

        public string AantalDeelnemers 
        {
            get
            {
                SessieServices sService = new SessieServices();
                int aantal = sService.GetAantalDeelnemersFromSessie(this.ID);
                return aantal.ToString();
            }
        }

        public int TotaalSessieInschrijvingen
        {
            get
            {
                return new InschrijvingenService().GetTotaalInschrijvingenFromSessie(this.ID);
            }
        }

        //public int AantalGasten
        //{
        //    get
        //    {
        //        return new InschrijvingenService().GetAantalGastenFromSessie(this.ID);
        //    }
        //}

        public int AantalEffectieveInschrijvingen
        {
            get
            {
                return new InschrijvingenService().GetAantalInschrijvingenFromSessie(this.ID);
            }
        }

        public bool HasInschrijvingen
        {
            get
            {
                int aantal = (from i in Inschrijvingen where i.Actief == true select i).Count();
                return aantal > 0;
            }
        }


        public string AantalSprekers 
        {
            get 
            {
                SessieSprekerServices sService = new SessieSprekerServices(); 
                int aantal = sService.GetAantalSprekersFromSessie(this.ID);
                return aantal.ToString();
            } 
        }

        public string NaamDatumLocatie 
        {
            get
            {
                SessieServices sService = new SessieServices();
                Sessie fullsessie = sService.GetSessieByID(this.ID);
                if (fullsessie != null)
                {
                    String naam = fullsessie.Naam;
                    if (fullsessie.Sorteerdatum != null)
                        naam += " : " + ((DateTime)fullsessie.Sorteerdatum).ToShortDateString();
                    if (fullsessie.Locatie != null)
                        naam += " : " + fullsessie.Locatie.Naam;
                    return naam;
                }
                else
                    return "-- Kies --";
            }
        }

        public string LocatieNaam
        {
            get
            {
                SessieServices sService = new SessieServices();
                Sessie fullsessie = sService.GetSessieByID(this.ID);
                if (fullsessie != null)
                {
                    return fullsessie.Locatie == null ?  "" : fullsessie.Locatie.Naam;
                }
                else
                    return "";
            }

        }

        public string LocatieAdres
        {
            get
            {
                SessieServices sService = new SessieServices();
                Sessie fullsessie = sService.GetSessieByID(this.ID);
                if (fullsessie != null)
                {
                    return fullsessie.Locatie == null ? "" : fullsessie.Locatie.FullAddress;
                }
                else
                    return "";
            }
        }

        public string SeminarieTitel
        {
            get
            {
                SessieServices sService = new SessieServices();
                Sessie fullsessie = sService.GetSessieByID(this.ID);
                if (fullsessie != null)
                {
                    return fullsessie.Seminarie.Titel;
                }
                else
                    return "";
            }
        }

        public string SeminarieTaalCode
        {
            get
            {
                SessieServices sService = new SessieServices();
                Sessie fullsessie = sService.GetSessieByID(this.ID);
                if (fullsessie != null)
                {
                    return fullsessie.Seminarie.Taalcode;
                }
                else
                    return "";
            }
        }

        public int AantalInschrijvingen
        {
            get
            {
                //return this.Inschrijvingen.Count();
                SessieServices sService = new SessieServices();
                int aantal = sService.GetAanInschrijvingen(this.ID);
                return aantal;
            }
        }

        public string SessieNaamEnDeelnemers 
        {
            get
            {
                return NaamDatumLocatie + ": inschrijvingen: " + AantalInschrijvingen;
            }
        }

        public string SeminarieSessieEnDeelnemers
        {
            get
            {
                return SeminarieTitel + ": " + NaamDatumLocatie + ": inschrijvingen: " + AantalEffectieveInschrijvingen;
            }
        }

        public string AantalGasten
        {
            get
            {
                int aantal = 0;
                InschrijvingenService iService = new InschrijvingenService();
                aantal = iService.GetAantalGastenFromSessie(this.ID);
                return aantal.ToString();
            }
        }

        public string AantalEchteInschrijvingen
        {
            get
            {
                int aantal = 0;
                InschrijvingenService iservice = new InschrijvingenService();
                aantal = iservice.GetAantalEffectieveInschrijvingenFromSessie(this.ID);
                return aantal.ToString();
            }
        }

        public int AantalEffectieveInschrijvingenAndInteresse
        {
            get
            {
                int aantal = 0;
                InschrijvingenService iservice = new InschrijvingenService();
                aantal = iservice.GetAantalEffectieveInschrijvingenAndInteresseFromSessie(this.ID);
                return aantal;
            }
        }


        public override bool Equals(object obj)
        {
            if (obj is Sessie)
            {
                Sessie deAndere = obj as Sessie;
                if (deAndere.ID == ID)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public int TeFacturerenInschrijvingen
        {
            get
            {
                int aantal = 0;
                foreach (Inschrijvingen inschrijving in this.Inschrijvingen)
                {
                    if (inschrijving.Actief == true)
                    {
                        if (string.IsNullOrWhiteSpace(inschrijving.Factuurnummer))
                            aantal++;
                    }
                }
                return aantal;
            }
        }

        public int OpTeVolgenKlanten
        {
            get
            {
                int aantal = 0;
                foreach (Inschrijvingen inschrijving in this.Inschrijvingen)
                {
                    if (inschrijving.Status == "Opvolgen")
                    {
                        aantal++;
                    }
                }
                return aantal;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string NaamVTerplaatse
        {
            get
            {  
                if (Verantwoordelijke_Terplaatse_ID != null)           
                    return new GebruikerService().GetUserByID(Verantwoordelijke_Terplaatse_ID).Naam;
                else
                    return "";
            }
        }

        public string NaamVSessie
        {
            get
            {
                return new GebruikerService().GetUserByID(Verantwoordelijke_Sessie_ID).Naam;
            }
        }

        public SolidColorBrush TextColor {
            get
            {
                SolidColorBrush color = new SolidColorBrush(Color.FromArgb(0, 0, 0, 255));
                switch (this.Status)
                {
                    case "Geannuleerd":
                        color = new SolidColorBrush(Color.FromArgb(0, 255, 0, 0));
                        break;
                    case "Verplaatst":
                        color = new SolidColorBrush(Color.FromArgb(0, 0, 255, 0));
                        break;
                    default:
                        color = new SolidColorBrush(Color.FromArgb(0, 0, 0, 255));
                        break;
                }
                return color;
            }
        }

        public string VoorNaamVTerplaatse
        {
            get
            {
                string naam = new GebruikerService().GetUserByID(Verantwoordelijke_Terplaatse_ID).Naam;
                string voornaam = naam.Split('.')[0];
                return voornaam;
            }
        }

        public Sessie(GetSessiesFromInschrijvingStatus_Result result)
        {
            this.ID = result.ID;
            this.Naam = result.Naam;
            this.Seminarie_ID = result.Seminarie_ID;
            this.Locatie_ID = result.Locatie_ID;
            this.Datum = result.Datum;
            this.Beginuur = result.Beginuur;
            this.Einduur = result.Einduur;
            this.Status = result.Status;
            this.Status_info = result.Status_info;
            this.Zaal = result.Zaal;
            this.URL = result.URL;
            this.Standaard_prijs = result.Standaard_prijs;
            this.Prijs_Profit_Prive = result.Prijs_Profit_Prive;
            this.Prijs_Non_Profit = result.Prijs_Non_Profit;
            this.Kortingsprijs = result.Kortingsprijs;
            this.Erkenning_ID = result.Erkenning_ID;
            this.Opstelling_zaal = result.Opstelling_zaal;
            this.Catering_Lunch = result.Catering_Lunch;
            this.Status_Locatie = result.Status_Locatie;
            this.Status_Datum = result.Status_Datum;
            this.KMO_Port_Uren = result.KMO_Port_Uren;
            this.Verantwoordelijke_Terplaatse_ID = result.Verantwoordelijke_Terplaatse_ID;
            this.Verantwoordelijke_Sessie_ID = result.Verantwoordelijke_Sessie_ID;
            this.Beamer = result.Beamer;
            this.Laptop = result.Laptop;
            this.Opmerking = result.Opmerking;
            this.Actief = result.Actief;
            this.Gewijzigd = result.Gewijzigd;
            this.Gewijzigd_door = result.Gewijzigd_door;
            this.Aangemaakt = result.Aangemaakt;
            this.Aangemaakt_door = result.Aangemaakt_door;
            this.Target = result.Target;
            this.Erkenningsnummer = result.Erkenningsnummer;
            this.Modified = result.Modified;
        }
    }
}

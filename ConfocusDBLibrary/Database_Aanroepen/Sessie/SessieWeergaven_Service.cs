﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class SessieWeergaven_Service
    {
        public List<SessieWeergaven> FindAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from w in entities.SessieWeergaven.Include("Gebruiker").Include("Locatie")
                        orderby w.Naam
                        select w).ToList();
            }
        }

        public void Save(SessieWeergaven weergave)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from w in entities.SessieWeergaven
                                where w.Naam == weergave.Naam
                                select w).FirstOrDefault();

                if (gevonden != null)
                    throw new Exception(ErrorMessages.WeergaveBestaatReeds);

                entities.SessieWeergaven.Add(weergave);
                entities.SaveChanges();
            }
        }

        public void Update(SessieWeergaven weergave)
        {
            using (var entities = new ConfocusEntities5())
            {
                var gevonden = (from w in entities.SessieWeergaven
                                where w.Naam == weergave.Naam
                                select w).FirstOrDefault();

                if (gevonden == null)
                    throw new Exception(ErrorMessages.WeergaveNietGevonden);

                gevonden.Locatie_ID = weergave.Locatie_ID;
                gevonden.User_ID = weergave.User_ID;
                //gevonden.Weeknummer = weergave.Weeknummer;
                gevonden.Taalcode = weergave.Taalcode;
                gevonden.Aantal_inschrijvingen = weergave.Aantal_inschrijvingen;
                gevonden.Verantwoordelijke_sessie_ID = weergave.Verantwoordelijke_sessie_ID;
                gevonden.Verantwoordelijke_terplaatse_ID = weergave.Verantwoordelijke_terplaatse_ID;
                gevonden.Organisator_ID = weergave.Organisator_ID;
                gevonden.Erkenning_ID = weergave.Erkenning_ID;
                gevonden.Erkenning_status = weergave.Erkenning_status;

                entities.SaveChanges();
            }
        }

        public List<SessieWeergaven> GetWeergavesByUserId(int iD)
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from w in entities.SessieWeergaven where w.User_ID == iD orderby w.Naam select w).ToList();
            }
        }

        public void Delete(SessieWeergaven weergave)
        {
            using (var entities = new ConfocusEntities5())
            {
                var todelete = (from w in entities.SessieWeergaven where w.ID == weergave.ID select w).FirstOrDefault();

                if (todelete != null)
                {
                    entities.SessieWeergaven.Remove(todelete);
                    entities.SaveChanges();
                }
            }
        }
    }
}

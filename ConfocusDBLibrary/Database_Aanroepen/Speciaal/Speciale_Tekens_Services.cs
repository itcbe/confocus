﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusDBLibrary
{
    public class Speciale_Tekens_Services
    {
        public void Add(Speciaal_teken teken)
        {
            using (var entities = new ConfocusEntities5())
            {
                try
                {
                    entities.Speciaal_teken.Add(teken);
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void Update(Speciaal_teken teken)
        {
            using (var entities = new ConfocusEntities5())
            {
                try
                {
                    var sign = (from s in entities.Speciaal_teken where s.ID == teken.ID select s).FirstOrDefault();
                    if (sign != null)
                    {
                        sign.Speciaal_karakter = teken.Speciaal_karakter;
                        sign.Alternatief = teken.Alternatief;

                        entities.SaveChanges();
                    }
                    else
                        throw new Exception("Teken niet gevonden om bij te werken!");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<Speciaal_teken> GetAll()
        {
            using (var entities = new ConfocusEntities5())
            {
                return (from s in entities.Speciaal_teken orderby s.Speciaal_karakter  select s).ToList();
            }
        }
    }
}

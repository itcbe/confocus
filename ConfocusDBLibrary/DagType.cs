//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConfocusDBLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class DagType
    {
        public int ID { get; set; }
        public string Naam { get; set; }
        public System.DateTime Aangemaakt { get; set; }
        public string Aangemaakt_door { get; set; }
        public Nullable<System.DateTime> Gewijzigd { get; set; }
        public string Gewijzigd_door { get; set; }
        public bool Actief { get; set; }
        public string Nederlands { get; set; }
        public string Frans { get; set; }
        public byte[] Modified { get; set; }
    }
}

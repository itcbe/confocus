﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
using Microsoft.Win32;
using Excel = Microsoft.Office.Interop.Excel;

namespace ITCLibrary
{
    public class ExcelDocument
    {
        public Excel.Worksheet xlWorkSheet;
        public Excel.Workbook xlWorkBook;
        public Excel.Application xlApp;
        public Excel.Range rangeToHoldHyperlink;
        public Excel.Range CellInstance;
        public String fileName;

        public Excel.XlSaveAsAccessMode xlSaveAsAccessmode;
        private object misValue = System.Reflection.Missing.Value;

        public ExcelDocument()
        {
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlApp.DisplayAlerts = false;
            xlApp.Visible = true;
            //Dummy initialisation to prevent errors.
            rangeToHoldHyperlink = xlWorkSheet.get_Range("A1", Type.Missing);
            CellInstance = xlWorkSheet.get_Range("A1", Type.Missing);
        }

        public ExcelDocument(string template)
        {
            xlApp = new Excel.Application();

            xlWorkBook = xlApp.Workbooks.Open(template);

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlApp.DisplayAlerts = false;
            xlApp.Visible = true;
            //Dummy initialisation to prevent errors.
            rangeToHoldHyperlink = xlWorkSheet.get_Range("A1", Type.Missing);
            CellInstance = xlWorkSheet.get_Range("A1", Type.Missing);
        }

        public void InsertText(string text, int column, int row, int cells, bool bold)
        {
            string cell1 = GetColumn(column) + row;
            string cell2 = GetColumn(column + cells) + row;
            //Excel.Range range = xlWorkSheet.get_Range(cell1, cell2);
            //range.Merge(Type.Missing);
            if (bold)
                xlWorkSheet.Cells[column, row].Font.Bold = true;
            xlWorkSheet.Cells[column, row].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[column, row] = text;
        }

        public void InsertTextWithBorder(string text, int column, int row, int cells)
        {
            string cell1 = GetColumn(column) + row;
            string cell2 = GetColumn(column + cells) + row;
            Excel.Range range = xlWorkSheet.get_Range(cell1, cell2);
            range.Merge(Type.Missing);
            xlWorkSheet.Cells[column, row].Font.Bold = true;
            xlWorkSheet.Cells[column, row].Borders.Color = System.Drawing.Color.Black;
            xlWorkSheet.Cells[column, row].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
            xlWorkSheet.Cells[column, row] = text;
        }

        public void InsertImage(string uri, int left, int top)
        {
            Excel.Pictures p = xlWorkSheet.Pictures(Type.Missing) as Excel.Pictures;
            Excel.Picture pic = null;
            pic = p.Insert(uri, Type.Missing);
            pic.Width = 150;
            pic.Height = 50;
            pic.Left = left;
            pic.Top = top;
        }

        private string GetColumn(int column)
        {
            string letter = "";
            switch (column)
            {
                case 1:
                    letter = "A";
                    break;
                case 2:
                    letter = "B";
                    break;
                case 3:
                    letter = "C";
                    break;
                case 4:
                    letter = "D";
                    break;
                case 5:
                    letter = "E";
                    break;
                case 6:
                    letter = "F";
                    break;
                case 7:
                    letter = "G";
                    break;
                case 8:
                    letter = "H";
                    break;
                case 9:
                    letter = "I";
                    break;
                case 10:
                    letter = "J";
                    break;
            }

            return letter;
        }

        public void SetColumnHeaders(String[] headers, int startpos, Boolean border, Boolean bold, Boolean altColor)
        {
            int pos = startpos;
            foreach (String header in headers)
            {
                if (altColor)
                    if ((pos % 2) == 1)
                        xlWorkSheet.Cells[1, pos].Interior.Color = System.Drawing.Color.LightBlue;
                if (bold)
                    xlWorkSheet.Cells[1, pos].Font.Bold = true;
                if (border)
                {
                    xlWorkSheet.Cells[1, pos].Borders.Color = System.Drawing.Color.Black;
                    xlWorkSheet.Cells[1, pos].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
                }
                xlWorkSheet.Cells[1, pos] = header;

                pos++;
            }
        }

        public void InsertList(String[][] items, Boolean border, int row, int[] columnwidths, int rowwidth)
        {
            try
            {
                System.Drawing.Color c = System.Drawing.Color.FromArgb(184, 204, 228);
                int colCounter = 1, rowcounter = row;
                int index = 0;
                int activeRows = 1;
                foreach (int width in columnwidths)
                {
                    xlWorkSheet.Columns[colCounter].ColumnWidth = width;
                    colCounter++;
                    if (width != 0)
                        activeRows++;
                }
                colCounter = 1;
                foreach (String[] myItem in items)
                {
                    if (rowcounter != row)
                    {
                        xlWorkSheet.Rows[rowcounter].RowHeight = rowwidth;
                        xlWorkSheet.Rows[rowcounter].VerticalAlignment = Excel.Constants.xlTop;
                        xlWorkSheet.Rows[rowcounter].WrapText = true;
                    }
                    if (myItem != null)
                    {
                        foreach (String item in myItem)
                        {
                            if (item == "True")
                            {
                                xlWorkSheet.Rows[rowcounter].Font.Italic = true;
                                continue;
                            }
                            else if (item == "False") continue;

                            if (item == "1")
                            {
                                for (int i = 1; i < activeRows; i++)
                                {
                                    xlWorkSheet.Cells[rowcounter, i].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                }
                                //xlWorkSheet.Cells[rowcounter, 1].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                //xlWorkSheet.Cells[rowcounter, 2].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                //xlWorkSheet.Cells[rowcounter, 3].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                //xlWorkSheet.Cells[rowcounter, 4].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                //if (activeRows > 4)
                                //{
                                //    xlWorkSheet.Cells[rowcounter, 5].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                //    xlWorkSheet.Cells[rowcounter, 6].Interior.Color = System.Drawing.ColorTranslator.ToOle(c);
                                //}
                                continue;
                            }
                            else if (item == "0")
                                continue;
                            if (rowcounter == row && item == "")
                                continue;

                            if (index == 0)
                            {
                                xlWorkSheet.Cells[rowcounter, colCounter].Font.Bold = true;
                            }

                            if (border)
                            {
                                xlWorkSheet.Cells[rowcounter, colCounter].Borders.Color = System.Drawing.Color.Black;
                                xlWorkSheet.Cells[rowcounter, colCounter].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
                            }
                            xlWorkSheet.Cells[rowcounter, colCounter] = item;
                            xlWorkSheet.Cells[rowcounter, colCounter].NumberFormat = "@";

                            //xlWorkSheet.Cells[1, colCounter].EntireColumn.AutoFit();
                            //xlWorkSheet.Cells[rowcounter, 6].EntireColumn.NumberFormat = "d/mm/jjjj;@";
                            colCounter++;
                        }
                        colCounter = 1;
                        rowcounter++;
                    }
                    index++;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Save()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Nieuwe scheet";
            dlg.DefaultExt = ".xls";
            dlg.Filter = "Excelscheets | *.xls";

            if (dlg.ShowDialog() == true)
            {
                this.xlWorkBook.SaveAs(dlg.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                fileName = dlg.FileName;
            }
        }

        public void Close()
        {
            this.xlWorkBook.Close();
        }
    }
}

USE [Confocus]
GO

SELECT [Naam]
      ,[Seminarie_ID]
      ,[Datum]
	  ,COUNT(*)
FROM [dbo].[Sessie]
GROUP BY Naam, Seminarie_ID, Datum
HAVING (COUNT(*) > 1) and Datum > '2020-06-01'
GO



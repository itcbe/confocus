﻿using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusUnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("+++++++++++ Sessie verborgen datum gelijkstellen aan de sessie datum +++++++++++");
            Console.WriteLine("Start sessies ophalen");
            SessieServices sService = new SessieServices();
            try
            {
                List<Sessie> sessies = sService.GetAllEmpty();
                int aangepastesessies = 0, nietaangepastesessies = 0;
                Console.WriteLine(sessies.Count() + " sessies gevonden.");
                foreach (Sessie sessie in sessies)
                {
                    if (sessie.Datum != null)
                    {
                        sessie.Sorteerdatum = sessie.Datum;
                        try
                        {
                            sService.SaveSessieChanges(sessie);
                            aangepastesessies++;

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Fout bij het opslaan van sessie " + sessie.ID + "!!\n" + ex.Message);
                            nietaangepastesessies++;
                        }
                    }
                    else
                        nietaangepastesessies++;
                }
                Console.WriteLine(aangepastesessies + " sessies aangepast.");
                Console.WriteLine(nietaangepastesessies + " sessies niet aangepast");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}

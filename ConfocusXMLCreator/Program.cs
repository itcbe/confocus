﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConfocusXMLCreator
{
    class Program
    {
        private static string file = @"C:\SessieFTP\ConfocusSessiesXml.xml";
        static void Main(string[] args)
        {
            try
            {
                // haal alle actuele sessies op. 
                List<Sessie> ActieveSessies = new SessieServices().GetActueleSessies();
                // Maak de XML
                XmlDocument xml = CreateXML(ActieveSessies);
                xml.Save(file);
                // Plaats de nieuwe XML op de FTP server in de map Sessies
                ftp myftp = new ftp();
                myftp.UploadSessieXMLFileToSFTP(file);
            }
            catch (Exception ex)
            {
                echo(ex.Message);
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Functie die de tekst enerzijds toont in de console en anderzijds wegschrijft naar het logbestand
        /// </summary>
        /// <param name="t"></param>
        private static void echo(String t)
        {
            Console.WriteLine(t);
            String[] tekst = t.Split('\n');
            // Als er al een logfile bestaat voor vandaag gebruik deze anders maak een nieuw logbestand
            if (System.IO.File.Exists(@"C:\Confocus XML Creator\Logs\" + DateTime.Now.ToString("yyyy-MM-dd") + "XmlCreatorLog.log"))
            {
                using (FileStream log = new FileStream(@"C:\Confocus XML Creator\Logs\" + DateTime.Now.ToString("yyyy-MM-dd") + "XmlCreatorLog.log", FileMode.Append))
                {
                    StreamWriter sw = new StreamWriter(log);
                    foreach (String myT in tekst)
                    {
                        sw.WriteLine("- " + DateTime.Now.ToShortTimeString() + ": " + myT);
                    }
                    sw.Close();
                }
            }
            else
            {
                using (FileStream log = new FileStream(@"C:\Confocus XML Creator\Logs\" + DateTime.Now.ToString("yyyy-MM-dd") + "XmlCreatorLog.log", FileMode.OpenOrCreate))
                {
                    StreamWriter sw = new StreamWriter(log);
                    foreach (String myT in tekst)
                    {
                        sw.WriteLine("- " + DateTime.Now.ToShortTimeString() + ": " + myT);
                    }
                    sw.Close();
                }
            }
        }

        /// <summary>
        /// Functie die de XML creeerd voor de website met de sessies en alle toebehoren
        /// Seminarie, Sessie, erkenningen, doelgroepen, marketingkanalen, sprekers en locatie
        /// Deze XML wordt door de website ingelezen om deze gegevens te tonen .
        /// </summary>
        /// <param name="sessies"></param>
        /// <returns></returns>
        private static XmlDocument CreateXML(List<Sessie> sessies)
        {
            try
            {
                //Sessie ses = sessies[0];
                XmlDocument doc = new XmlDocument();
                XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.LoadXml("<Sessies></Sessies>");
                XmlElement root = doc.DocumentElement;
                foreach (Sessie ses in sessies)
                {

                    XmlNode sessiesnode = doc.CreateNode(XmlNodeType.Element, "Sessie", "");

                    //Basis sessie gegevens
                    XmlNode SessieIdNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                    SessieIdNode.InnerText = ses.ID.ToString();
                    sessiesnode.AppendChild(SessieIdNode);
                    XmlNode SessieNaamNode = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                    SessieNaamNode.InnerText = ses.Naam;
                    sessiesnode.AppendChild(SessieNaamNode);
                    XmlNode SessieDatumNode = doc.CreateNode(XmlNodeType.Element, "Datum", "");
                    SessieDatumNode.InnerText = ses.Datum == null ? "" : ((DateTime)ses.Datum).ToString();
                    sessiesnode.AppendChild(SessieDatumNode);
                    XmlNode SorteerDatumNode = doc.CreateNode(XmlNodeType.Element, "VerborgenDatum", "");
                    SorteerDatumNode.InnerText = ses.Sorteerdatum == null ? "" : ReverseDate((DateTime)ses.Sorteerdatum);
                    sessiesnode.AppendChild(SorteerDatumNode);
                    XmlNode SessieBeginuurNode = doc.CreateNode(XmlNodeType.Element, "Beginuur", "");
                    SessieBeginuurNode.InnerText = string.IsNullOrEmpty(ses.Beginuur) ? "" : ses.Beginuur;
                    sessiesnode.AppendChild(SessieBeginuurNode);
                    XmlNode SessieEinduurNode = doc.CreateNode(XmlNodeType.Element, "Einduur", "");
                    SessieEinduurNode.InnerText = string.IsNullOrEmpty(ses.Einduur) ? "" : ses.Einduur;
                    sessiesnode.AppendChild(SessieEinduurNode);
                    XmlNode CleverCastIDNode = doc.CreateNode(XmlNodeType.Element, "ID_Clevercast", "");
                    CleverCastIDNode.InnerText = ses.Target;
                    sessiesnode.AppendChild(CleverCastIDNode);
                    XmlNode SessieStatusNode = doc.CreateNode(XmlNodeType.Element, "Status", "");
                    SessieStatusNode.InnerText = ses.Status;
                    sessiesnode.AppendChild(SessieStatusNode);
                    XmlNode SessiePrijsNode = doc.CreateNode(XmlNodeType.Element, "StandaardPrijs", "");
                    SessiePrijsNode.InnerText = ses.Standaard_prijs;
                    sessiesnode.AppendChild(SessiePrijsNode);
                    XmlNode SessiePrijsNonProfitdNode = doc.CreateNode(XmlNodeType.Element, "PrijsNonProfit", "");
                    SessiePrijsNonProfitdNode.InnerText = ses.Prijs_Non_Profit;
                    sessiesnode.AppendChild(SessiePrijsNonProfitdNode);
                    XmlNode SessiePrijsProfitNode = doc.CreateNode(XmlNodeType.Element, "PrijsProfit", "");
                    SessiePrijsProfitNode.InnerText = ses.Prijs_Profit_Prive;
                    sessiesnode.AppendChild(SessiePrijsProfitNode);
                    XmlNode SessieKortingsprijsNode = doc.CreateNode(XmlNodeType.Element, "Kortingsprijs", "");
                    SessieKortingsprijsNode.InnerText = ses.Kortingsprijs;
                    sessiesnode.AppendChild(SessieKortingsprijsNode);
                    XmlNode SessieOpmerkingNode = doc.CreateNode(XmlNodeType.Element, "Opmerking", "");
                    SessieOpmerkingNode.InnerText = ses.Opmerking;
                    sessiesnode.AppendChild(SessieOpmerkingNode);
                    XmlNode SessieVerantwoordelijkeNode = doc.CreateNode(XmlNodeType.Element, "Verantwoordelijke_ID", "");
                    SessieVerantwoordelijkeNode.InnerText = ses.Verantwoordelijke_Sessie_ID == null ? "" : ses.Verantwoordelijke_Sessie_ID.ToString();
                    sessiesnode.AppendChild(SessieVerantwoordelijkeNode);
                    XmlNode SessieVerantwoordelijkeNaamNode = doc.CreateNode(XmlNodeType.Element, "Verantwoordelijke_Naam", "");
                    SessieVerantwoordelijkeNaamNode.InnerText = ses.Gebruiker == null ? "" : ses.Gebruiker.Naam;
                    sessiesnode.AppendChild(SessieVerantwoordelijkeNaamNode);
                    XmlNode SessieVerantwoordelijkeEmailNode = doc.CreateNode(XmlNodeType.Element, "Verantwoordelijke_Email", "");
                    SessieVerantwoordelijkeEmailNode.InnerText = ses.Gebruiker == null ? "" : ses.Gebruiker.Email;
                    sessiesnode.AppendChild(SessieVerantwoordelijkeEmailNode);
                    string Sessietime = "";
                    foreach (byte b in ses.Modified)
                    {
                        Sessietime += b.ToString();
                    }
                    XmlNode SessieGewijzigdNode = doc.CreateNode(XmlNodeType.Element, "SessieTimeStamp", "");
                    SessieGewijzigdNode.InnerText = Sessietime;
                    sessiesnode.AppendChild(SessieGewijzigdNode);

                    //Locatie gegevens                    

                    XmlNode LocatieNode = doc.CreateNode(XmlNodeType.Element, "Locatie", "");
                    XmlNode LocatieIDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                    LocatieIDNode.InnerText = ses.Locatie_ID.ToString();
                    LocatieNode.AppendChild(LocatieIDNode);
                    XmlNode LocatieNaamNode = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                    LocatieNaamNode.InnerText = ses.Locatie == null ? "" : ses.Locatie.Naam;
                    LocatieNode.AppendChild(LocatieNaamNode);
                    XmlNode LocatieAdresNode = doc.CreateNode(XmlNodeType.Element, "Adres", "");
                    LocatieAdresNode.InnerText = ses.Locatie == null ? "" : ses.Locatie.Adres;
                    LocatieNode.AppendChild(LocatieAdresNode);
                    XmlNode LocatiePostcodeNode = doc.CreateNode(XmlNodeType.Element, "Postcode", "");
                    LocatiePostcodeNode.InnerText = ses.Locatie == null ? "" : ses.Locatie.Postcode;
                    LocatieNode.AppendChild(LocatiePostcodeNode);
                    XmlNode LocatiePlaatsNode = doc.CreateNode(XmlNodeType.Element, "Plaats", "");
                    LocatiePlaatsNode.InnerText = ses.Locatie == null ? "" : ses.Locatie.Plaats;
                    LocatieNode.AppendChild(LocatiePlaatsNode);
                    XmlNode LocatieRegioNode = doc.CreateNode(XmlNodeType.Element, "Regio", "");
                    LocatieRegioNode.InnerText = ses.Locatie == null ? "" : ses.Locatie.Regio;
                    LocatieNode.AppendChild(LocatieRegioNode);
                    XmlNode LocatieGewijzigdNode = doc.CreateNode(XmlNodeType.Element, "LocatieTimeStamp", "");
                    if (ses.Locatie != null)
                    {
                        string Locatietime = "";
                        foreach (byte b in ses.Locatie.Modified)
                        {
                            Locatietime += b.ToString();
                        }

                        LocatieGewijzigdNode.InnerText = Locatietime;
                    }
                    LocatieNode.AppendChild(LocatieGewijzigdNode);

                    sessiesnode.AppendChild(LocatieNode);

                    //Seminarie gegevens
                    XmlNode SeminarieNode = doc.CreateNode(XmlNodeType.Element, "Seminarie", "");
                    XmlNode IDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                    IDNode.InnerText = ses.Seminarie_ID.ToString();
                    SeminarieNode.AppendChild(IDNode);
                    XmlNode TitleNode = doc.CreateNode(XmlNodeType.Element, "Titel", "");
                    TitleNode.InnerText = ses.Seminarie.Titel;
                    SeminarieNode.AppendChild(TitleNode);
                    XmlNode TypeNode = doc.CreateNode(XmlNodeType.Element, "Type", "");
                    TypeNode.InnerText = ses.Seminarie.Type;
                    SeminarieNode.AppendChild(TypeNode);
                    XmlNode TaalNode = doc.CreateNode(XmlNodeType.Element, "Taal", "");
                    TaalNode.InnerText = ses.Seminarie.Taalcode;
                    SeminarieNode.AppendChild(TaalNode);
                    XmlNode StatusNode = doc.CreateNode(XmlNodeType.Element, "Status", "");
                    StatusNode.InnerText = ses.Seminarie.Status;
                    SeminarieNode.AppendChild(StatusNode);
                    XmlNode VerantwoordelijkeNode = doc.CreateNode(XmlNodeType.Element, "Verantwoordelijke_ID", "");
                    VerantwoordelijkeNode.InnerText = ses.Seminarie.Verantwoordelijke_ID.ToString();
                    SeminarieNode.AppendChild(VerantwoordelijkeNode);
                    string Seminarietime = "";
                    foreach (byte b in ses.Seminarie.Modified)
                    {
                        Seminarietime += b.ToString();
                    }
                    XmlNode SeminarieGewijzigdNode = doc.CreateNode(XmlNodeType.Element, "SeminarieTimeStamp", "");
                    SeminarieGewijzigdNode.InnerText = Seminarietime;
                    SeminarieNode.AppendChild(SeminarieGewijzigdNode);
                    sessiesnode.AppendChild(SeminarieNode);
                    //Doelgroepen
                    // Get doelgroepen per SeminarieId
                    List<Seminarie_Doelgroep> doelgroepen = new Seminarie_DoelgroepServices().GetDoelgroepenBySeminarieID((decimal)ses.Seminarie_ID);
                    if (doelgroepen.Count > 0)
                    {
                        XmlNode groepenNode = doc.CreateNode(XmlNodeType.Element, "Doelgroepen", "");
                        foreach (Seminarie_Doelgroep groep in doelgroepen)
                        {
                            XmlNode groepNode = doc.CreateNode(XmlNodeType.Element, "Doelgroep", "");
                            XmlNode groepIDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                            groepIDNode.InnerText = groep.ID.ToString();
                            groepNode.AppendChild(groepIDNode);

                            //Niveau1 groep
                            XmlNode groepN1Node = doc.CreateNode(XmlNodeType.Element, "Niveau1", "");
                            XmlNode n1IDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                            n1IDNode.InnerText = groep.Niveau1_ID.ToString();
                            groepN1Node.AppendChild(n1IDNode);

                            XmlNode n1NodeNL = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                            n1NodeNL.InnerText = groep.Niveau1.Naam;
                            XmlAttribute n1TaalNLAtt = doc.CreateAttribute("taal");
                            n1TaalNLAtt.InnerText = "NL";
                            n1NodeNL.Attributes.Append(n1TaalNLAtt);
                            groepN1Node.AppendChild(n1NodeNL);

                            XmlNode n1NodeFR = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                            n1NodeFR.InnerText = groep.Niveau1.Naam_FR;
                            XmlAttribute n1TaalFRAtt = doc.CreateAttribute("taal");
                            n1TaalFRAtt.InnerText = "FR";
                            n1NodeFR.Attributes.Append(n1TaalFRAtt);
                            groepN1Node.AppendChild(n1NodeFR);

                            groepNode.AppendChild(groepN1Node);

                            //Niveau2 groep
                            if (groep.Niveau2 != null)
                            {
                                XmlNode groepN2Node = doc.CreateNode(XmlNodeType.Element, "Niveau2", "");
                                XmlNode n2IDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                                XmlNode n2NodeNL = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                                XmlNode n2NodeFR = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                                n2IDNode.InnerText = groep.Niveau2_ID.ToString();
                                groepN2Node.AppendChild(n2IDNode);
                                n2NodeNL.InnerText = groep.Niveau2.Naam;
                                XmlAttribute n2TaalNLAtt = doc.CreateAttribute("taal");
                                n2TaalNLAtt.InnerText = "NL";
                                n2NodeNL.Attributes.Append(n2TaalNLAtt);
                                groepN2Node.AppendChild(n2NodeNL);

                                n2NodeFR.InnerText = groep.Niveau2.Naam_FR;
                                XmlAttribute n2TaalFRAtt = doc.CreateAttribute("taal");
                                n2TaalFRAtt.InnerText = "FR";
                                n2NodeFR.Attributes.Append(n2TaalFRAtt);
                                groepN2Node.AppendChild(n2NodeFR);
                                groepNode.AppendChild(groepN2Node);
                            }

                            if (groep.Niveau3 != null)
                            {
                                XmlNode groepN3Node = doc.CreateNode(XmlNodeType.Element, "Niveau3", "");
                                XmlNode n3IDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                                XmlNode n3NodeNL = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                                XmlNode n3NodeFR = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                                n3IDNode.InnerText = groep.Niveau3_ID.ToString();
                                groepN3Node.AppendChild(n3IDNode);
                                n3NodeNL.InnerText = groep.Niveau3.Naam;
                                XmlAttribute n3TaalNLAtt = doc.CreateAttribute("taal");
                                n3TaalNLAtt.InnerText = "NL";
                                n3NodeNL.Attributes.Append(n3TaalNLAtt);
                                groepN3Node.AppendChild(n3NodeNL);
                                n3NodeFR.InnerText = groep.Niveau2.Naam_FR;
                                XmlAttribute n3TaalFRAtt = doc.CreateAttribute("taal");
                                n3TaalFRAtt.InnerText = "FR";
                                n3NodeFR.Attributes.Append(n3TaalFRAtt);
                                groepN3Node.AppendChild(n3NodeFR);
                                groepNode.AppendChild(groepN3Node);
                            }

                            XmlNode OpmerkingNode = doc.CreateNode(XmlNodeType.Element, "Opmerking", "");
                            OpmerkingNode.InnerText = groep.Opmerking;
                            groepNode.AppendChild(OpmerkingNode);
                            string time = "";
                            foreach (byte b in groep.Modified)
                            {
                                time += b.ToString();
                            }
                            XmlNode DoelgroepGewijzigdNode = doc.CreateNode(XmlNodeType.Element, "DoelgroepTimeStamp", "");
                            DoelgroepGewijzigdNode.InnerText = time;
                            groepNode.AppendChild(DoelgroepGewijzigdNode);
                            groepenNode.AppendChild(groepNode);
                        }
                        sessiesnode.AppendChild(groepenNode);
                    }
                    //Erkenningen
                    // Get erkenningen per sessieID
                    List<Seminarie_Erkenningen> erkenningen = new Seminarie_ErkenningenService().GetBySessieID(ses.ID);
                    if (erkenningen.Count > 0)
                    {
                        XmlNode ErkenningenNode = doc.CreateNode(XmlNodeType.Element, "Erkenningen", "");
                        foreach (Seminarie_Erkenningen erk in erkenningen)
                        {
                            XmlNode ErkNode = doc.CreateNode(XmlNodeType.Element, "Erkenning", "");
                            XmlNode ErkIDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                            ErkIDNode.InnerText = erk.ID.ToString();
                            ErkNode.AppendChild(ErkIDNode);
                            XmlNode ErkErkenningIDNode = doc.CreateNode(XmlNodeType.Element, "Erkenning_ID", "");
                            ErkErkenningIDNode.InnerText = erk.Erkenning_ID.ToString();
                            ErkNode.AppendChild(ErkErkenningIDNode);
                            XmlNode NaamNode = doc.CreateNode(XmlNodeType.Element, "Erkenning_Naam", "");
                            NaamNode.InnerText = erk.Erkenning.Naam;
                            ErkNode.AppendChild(NaamNode);
                            XmlNode NummerNode = doc.CreateNode(XmlNodeType.Element, "Erkenningsnummer", "");
                            NummerNode.InnerText = erk.Nummer;
                            ErkNode.AppendChild(NummerNode);
                            XmlNode AantalNode = doc.CreateNode(XmlNodeType.Element, "Aantal", "");
                            AantalNode.InnerText = erk.TAantal;
                            ErkNode.AppendChild(AantalNode);
                            XmlNode StatusErkNode = doc.CreateNode(XmlNodeType.Element, "Status", "");
                            StatusErkNode.InnerText = erk.Status;
                            ErkNode.AppendChild(StatusErkNode);
                            XmlNode WebsiteOpmNode = doc.CreateNode(XmlNodeType.Element, "Opmerkingen", "");
                            WebsiteOpmNode.InnerText = erk.Opmerking;
                            ErkNode.AppendChild(WebsiteOpmNode);
                            string time = "";
                            foreach (byte b in erk.Modified)
                            {
                                time += b.ToString();
                            }
                            XmlNode ErkGewijzigdNode = doc.CreateNode(XmlNodeType.Element, "ErkenningTimeStamp", "");
                            ErkGewijzigdNode.InnerText = time;
                            ErkNode.AppendChild(ErkGewijzigdNode);
                            ErkenningenNode.AppendChild(ErkNode);
                        }
                        sessiesnode.AppendChild(ErkenningenNode);
                    }
                    //Marketingkanalen
                    // Get Marketingkanalen by SeminarieID
                    List<Seminarie_Marketing> marketingkanalen = new Seminarie_MarketingServices().GetMarketingBySeminarie((decimal)ses.Seminarie_ID);
                    if (marketingkanalen.Count > 0)
                    {
                        XmlNode KanalenNode = doc.CreateNode(XmlNodeType.Element, "Marketingkanalen", "");
                        foreach (Seminarie_Marketing marketing in marketingkanalen)
                        {
                            XmlNode kanaalNode = doc.CreateNode(XmlNodeType.Element, "Marketingkanaal", "");
                            XmlNode KanaalIDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                            KanaalIDNode.InnerText = marketing.ID.ToString();
                            kanaalNode.AppendChild(KanaalIDNode);
                            XmlNode MarketingKanaalIDNode = doc.CreateNode(XmlNodeType.Element, "MarketingKanaal_ID", "");
                            MarketingKanaalIDNode.InnerText = marketing.Marketing_ID.ToString();
                            kanaalNode.AppendChild(MarketingKanaalIDNode);
                            XmlNode MarketingNaamNode = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                            MarketingNaamNode.InnerText = marketing.Marketingkanalen.Marketing_kanaal;
                            kanaalNode.AppendChild(MarketingNaamNode);
                            XmlNode OpvolgingNode = doc.CreateNode(XmlNodeType.Element, "Opvolgdatum", "");
                            OpvolgingNode.InnerText = marketing.Opvolgdatum.ToString();
                            kanaalNode.AppendChild(OpvolgingNode);
                            XmlNode KanaalStatusNode = doc.CreateNode(XmlNodeType.Element, "Status", "");
                            KanaalStatusNode.InnerText = marketing.Status;
                            kanaalNode.AppendChild(KanaalStatusNode);
                            XmlNode OpmerkingNode = doc.CreateNode(XmlNodeType.Element, "Opmerking", "");
                            OpmerkingNode.InnerText = marketing.Opmerking;
                            kanaalNode.AppendChild(OpmerkingNode);
                            XmlNode WebsiteOpmerkingNode = doc.CreateNode(XmlNodeType.Element, "Opmerking_Website", "");
                            WebsiteOpmerkingNode.InnerText = marketing.Marketingkanalen.Adverteren;
                            kanaalNode.AppendChild(WebsiteOpmerkingNode);
                            string time = "";
                            foreach (byte b in marketing.Modified)
                            {
                                time += b.ToString();
                            }
                            XmlNode KanaalTimestampNode = doc.CreateNode(XmlNodeType.Element, "KanaalTimeStamp", "");
                            KanaalTimestampNode.InnerText = time;
                            kanaalNode.AppendChild(KanaalTimestampNode);
                            KanalenNode.AppendChild(kanaalNode);
                        }
                        sessiesnode.AppendChild(KanalenNode);
                    }
                    //Sprekers
                    // Get Sprekers per SessieID
                    List<Sessie_Spreker> sprekers = new SessieSprekerServices().GetSprekersBySessieID(ses.ID);
                    if (sprekers.Count > 0)
                    {
                        XmlNode SprekersNode = doc.CreateNode(XmlNodeType.Element, "Sprekers", "");
                        foreach (Sessie_Spreker spreker in sprekers)
                        {
                            XmlNode SprekerNode = doc.CreateNode(XmlNodeType.Element, "Spreker", "");
                            XmlNode SprekerIDNode = doc.CreateNode(XmlNodeType.Element, "ID", "");
                            SprekerIDNode.InnerText = spreker.Spreker_ID.ToString();
                            SprekerNode.AppendChild(SprekerIDNode);
                            XmlNode VoornaamNode = doc.CreateNode(XmlNodeType.Element, "Voornaam", "");
                            VoornaamNode.InnerText = spreker.Spreker.Contact.Voornaam;
                            SprekerNode.AppendChild(VoornaamNode);
                            XmlNode NaamNode = doc.CreateNode(XmlNodeType.Element, "Naam", "");
                            NaamNode.InnerText = spreker.Spreker.Contact.Achternaam;
                            SprekerNode.AppendChild(NaamNode);
                            XmlNode EmailNode = doc.CreateNode(XmlNodeType.Element, "Email", "");
                            EmailNode.InnerText = spreker.Spreker.Email;
                            SprekerNode.AppendChild(EmailNode);
                            XmlNode SprekerOpmerkingNode = doc.CreateNode(XmlNodeType.Element, "Status", "");
                            SprekerOpmerkingNode.InnerText = spreker.Opmerking;
                            SprekerNode.AppendChild(SprekerOpmerkingNode);
                            XmlNode SprekerGewijzigdNode = doc.CreateNode(XmlNodeType.Element, "SprekerTimeStamp", "");
                            string time = "";
                            foreach (byte b in spreker.Modified)
                            {
                                time += b.ToString();
                            }
                            SprekerGewijzigdNode.InnerText = time;
                            SprekerNode.AppendChild(SprekerGewijzigdNode);
                            SprekersNode.AppendChild(SprekerNode);
                        }
                        sessiesnode.AppendChild(SprekersNode);
                    }
                    root.AppendChild(sessiesnode);
                }

                Console.WriteLine(doc.OuterXml);
                return doc;
            }
            catch (Exception ex)
            {
                echo(ex.Message);
                throw ex;
            }
        }

        private static string ReverseDate(DateTime date)
        {
            string datum = date.Year + "-" 
                + (date.Month < 10 ? "0" + date.Month : date.Month.ToString())
                + "-" + (date.Day < 10 ? "0" + date.Day : date.Day.ToString())
                + " " + (date.Hour < 10 ? "0" + date.Hour : date.Hour.ToString())
                + ":" + (date.Minute < 10 ? "0" + date.Minute : date.Minute.ToString()) + ":00" ;

            return datum;
        }
    }
}

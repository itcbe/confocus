﻿using ConfocusClassLibrary;
using ConfocusDBLibrary;
using ITCLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ConfocusInschrijvingControle
{
    class Program
    {
        private static string LogUri = @"C:\Confocus Data\Controle\";
        private static string ContoleUri = @"C:\Controlexlm";
        /// <summary>
        /// De verwerkte inschrijvingen op de FTP server worden naar de map F:\Controlexlm gecopieerd als backup
        /// Van hieruit worden de inschrijvingen van de laatste dagen nagekeken in de database op aanwezigheid.
        /// Er wordt een verslag en een logbestand aangemaakt en verzonden.8
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            ftp ftpservice = new ftp();
            List<FtpItem> ftpxmls = ftpservice.GetVerwerkteFilesFromSFTP(ContoleUri); //
            //ftpservice.GetVerwerkteFTP();
            List<string> xmllist = GetXMLSByDate(DateTime.Today.AddDays(-1));
            echo("Er zijn " + ftpxmls.Count() + " xml's op de ftpserver en " + xmllist.Count() + " locale xml's");
            var tecontroleren = (from f in ftpxmls where f.Modified > DateTime.Today.AddDays(-1) select f).ToList();
            List<WebsiteInschrijving> nietGevondenInschrijvingen = new List<WebsiteInschrijving>();
            foreach (FtpItem f in tecontroleren)
            {
                WebsiteInschrijving webInschrijving = XMLReader.GetControleInschrijvingFromXml(f.Filename);
                if (webInschrijving != null)
                { 
                    foreach (WebSessie sessie in webInschrijving.websessies)
                    {
                        List<Inschrijvingen> inschrijvingen = new InschrijvingenService().GetInschrijvingBySessieID(decimal.Parse(sessie.Sessie_Id));
                        echo("Zoeken naar: " + webInschrijving.Voornaam + " " + webInschrijving.Achternaam);
                        echo("");
                        string voornaam = DBHelper.Simplify(webInschrijving.Voornaam.ToLower().Trim());
                        string achternaam = DBHelper.Simplify(webInschrijving.Achternaam.ToLower().Trim());
                        Inschrijvingen inschrijving = (from i in inschrijvingen
                                                       where (i.Functies.Contact.SimpleVoornaam.ToLower().Trim() == voornaam
                                                       && i.Functies.Contact.SimpleAchternaam.ToLower().Trim() == achternaam)
                                                       || (i.Functies.Contact.SimpleVoornaam.ToLower().Trim() == achternaam
                                                       && i.Functies.Contact.SimpleAchternaam.ToLower().Trim() == voornaam)
                                                       select i).FirstOrDefault();
                        if (inschrijving == null)
                        { 
                            nietGevondenInschrijvingen.Add(webInschrijving);
                            echo("Niet gevonden");
                            echo("");
                        }
                        else
                        {
                            echo("Gevonden");
                            echo("");
                        }
                    }
                }
            }
            StringBuilder body = new StringBuilder();
            body.Append("<html><h1>Controle op de inschrijvingen op FTP server en in de database.</h1>");
            Console.WriteLine("Er zijn " + nietGevondenInschrijvingen.Count + " inschrijvingen niet gevonden in het systeem!");
            body.Append("<p>Er zijn " + nietGevondenInschrijvingen.Count + " inschrijvingen niet gevonden in het systeem!<p>");
            body.Append("<table><thead><tr><th>Deelnemer</th><th>Sessie ID</th><th>Sessie naam</th></tr></thead></tbody>");
            foreach (WebsiteInschrijving insch in nietGevondenInschrijvingen)
            {

                echo(insch.Voornaam + " " + insch.Achternaam);
                body.Append("<tr><td>" + insch.Voornaam + " " + insch.Achternaam + "</td>");
                foreach (WebSessie ses in insch.websessies)
                {
                    echo("  - " + ses.Sessie_Id + " : " + ses.SessieNaam + " - " + ses.Sessiedatum);
                    body.Append("<td>" + ses.Sessie_Id + "</td><td>" + ses.SessieNaam + " - " + ses.Sessiedatum + "</td></tr>");
                    echo("");
                }
            }
            body.Append("</tbody></table>");
            body.Append("<p>Veel voorkomende oorzaken kunnen vb.<br /><ul>");
            body.Append("<li>De XML is nog niet verwerkt. Hij staat nog in de map Nieuwe FTP.</li>");
            body.Append("<li>De voor en achternaam zijn verwisseld.</li>");
            body.Append("<li>De sessie is geannuleerd.</li>");
            body.Append("<ul>");
            Mail(body);
            //Console.ReadLine();
        }

        /// <summary>
        /// Functie om het verslag te mailen naar Els Duflos en Stefan Kelchtermans. Later kan dit via het instellingenscript geregeld worden.
        /// </summary>
        /// <param name="body"></param>
        private static void Mail(StringBuilder body)
        {
            string subject = "";
            subject = "Verslag controle op de inschrijvingen";

            string mailbody = body.ToString();
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            //message.To.Add("Els.Duflos@confocus.be");
            //message.To.Add("barbara.castermans@confocus.be");
            message.To.Add("stefan.kelchtermans@itc.be");
            message.Subject = subject;
            message.From = new System.Net.Mail.MailAddress("administratie@confocus.be", "Confocus opleidingen");
            message.IsBodyHtml = true;
            message.Body = mailbody;
            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.outlook.office365.com", 587))
            {
                smtp.Credentials = new System.Net.NetworkCredential("administratie@confocus.be", "Lewis2021");
                smtp.EnableSsl = true;

                echo("try send");
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    smtp.Send(message);
                    echo("Mail verzonden.");
                }
                catch (Exception ex)
                {
                    echo("Fout bij het verzenden van de email: ");
                    echo(ex.Message);
                    if (ex.InnerException != null)
                        echo(ex.InnerException.Message);
                }

            }
        }

        private static List<string> GetXMLSByDate(DateTime today)
        {
            List<string> files = new List<string>();
            foreach (string file in Directory.EnumerateFiles(LogUri, "*.xml"))
            {
                files.Add(file);
            }
            return files;
        }

        /// <summary>
        /// Deze functie schrijft de tekst in t naar het logbestand en de console.
        /// </summary>
        /// <param name="t"></param>
        private static void echo(String t)
        {
            Console.WriteLine(t);
            String[] tekst = t.Split('\n');
            string uri = LogUri + "Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + "Inschrijvingen.log";
            if (System.IO.File.Exists(uri))
            {
                using (FileStream log = new FileStream(uri, FileMode.Append))
                {
                    StreamWriter sw = new StreamWriter(log);
                    foreach (String myT in tekst)
                    {
                        sw.WriteLine(myT);
                    }
                    sw.Close();
                }
            }
            else
            {
                using (FileStream log = new FileStream(uri, FileMode.OpenOrCreate))
                {
                    StreamWriter sw = new StreamWriter(log);
                    foreach (String myT in tekst)
                    {
                        sw.WriteLine(myT);
                    }
                    sw.Close();
                }
            }
        }

    }
}
